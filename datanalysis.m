(* ::Package:: *)

BeginPackage["datanalysis`"];


(* ::Subsection:: *)
(*Function usage*)


scalar::usage = 
		"scalar[h1,h2,noise,fin,fend] computes the weighted inner product between two waveforms for a given PSD and freqeuncy range.";

scalarHP::usage = 
		"scalar[h1,h2,noise,fin,fend] computes the weighted inner product between two waveforms for a given PSD and freqeuncy range.";
		
overlap::usage = 
		"overlap[h1,h2,noise,fmin,fmax] computes the overlap between two waveforms maximised over the time and phase, for a given PSD and freqeuncy range.";
		
Cov::usage = 
		"Cov[\[CapitalGamma],k], computes the covariance matrix from the SVD with number of pivot k.";
		
fisher::usage = 
		"fisher[h,dh,noise,fin,fend] computes the covariance matrix for the waveform h with derivative dh, for a given PSD and freqeuncy range.";

snr::usage = 
		"snr[h,noise,fin,fend] computes the signal to noise ratio of the template h for a given PSD and frequency range.";


Begin["`Private`"];


(* ::Subsection:: *)
(*Integration routines*)


(* ::Subsubsection::Closed:: *)
(*Scalar product over the waveform and noise integrals*)


scalar[h1_,h2_,noise_,fin_,fend_]:=Block[{cc=299792,prod,f},

prod=Re[NIntegrate[SetPrecision[h1[f]*Conjugate[h2[f]]/(noise[f]),70],{f,fin,fend},
		MaxRecursion->50,PrecisionGoal->25,AccuracyGoal->25,WorkingPrecision->30]];

Return[4/cc^2*prod]

]


(* ::Subsubsection::Closed:: *)
(*Scalar product over the waveform and noise integrals*)


scalarHP[h1_,h2_,noise_,fin_,fend_]:=Block[{cc=299792,prod,f},

prod=Re[NIntegrate[SetPrecision[h1[f]*Conjugate[h2[f]]/(noise[f]),70],{f,fin,fend},
		Method->{"GlobalAdaptive","SymbolicProcessing"->0,"MaxErrorIncreases"->2000},
		MaxRecursion->20,WorkingPrecision->60,PrecisionGoal->12,AccuracyGoal->Infinity]];

Return[4/cc^2*prod]

]


(* ::Subsubsection::Closed:: *)
(*Overlap maximized over time and phase*)


overlap[h1_,h2_,noise_,fmin_,fmax_]:=Block[{cc=299792,n,\[Delta]f,\[CapitalDelta]f,\[ScriptCapitalO]1,\[ScriptCapitalO]2,\[ScriptCapitalO]max,Noise,\[ScriptH]1,\[ScriptH]2,psd},

n = 1000;

{\[Delta]f,\[CapitalDelta]f} = {(fmax-fmin)/n,Range[fmin,fmax,\[Delta]f]};

{\[ScriptH]1,\[ScriptH]2,psd} = {h1[#],h2[#],noise[#]}&/@\[CapitalDelta]f;

\[ScriptCapitalO]1 = Abs[Total[4 (\[ScriptH]1 Conjugate[\[ScriptH]1])/(psd cc^2)]];
\[ScriptCapitalO]2 = Abs[Total[4 (\[ScriptH]2 Conjugate[\[ScriptH]2])/(psd cc^2)]];

\[ScriptCapitalO]max = Max[4Abs[InverseFourier[PadRight[(\[ScriptH]1 Conjugate[\[ScriptH]2])/(psd cc^2),100 n],FourierParameters->{-1,-1}]]]
			/Sqrt[\[ScriptCapitalO]1 \[ScriptCapitalO]2];

Return[\[ScriptCapitalO]max];

]


(* ::Subsection:: *)
(*Statistical tools*)


(* ::Subsubsection:: *)
(*Covariance from singular value decomposition*)


Cov[\[CapitalGamma]_,k_]:=Module[{U,S,V,dim,\[CapitalSigma]},

dim = Length[\[CapitalGamma]];

{U,S,V} = SingularValueDecomposition[\[CapitalGamma],dim-k,Tolerance->0];

\[CapitalSigma] = V.If[PositiveDefiniteMatrixQ[S],LinearSolve[S,IdentityMatrix[dim-k],Method->"Cholesky"],
		Inverse[\[CapitalGamma]]].ConjugateTranspose[V];

Return[\[CapitalSigma]];

]


(* ::Subsubsection::Closed:: *)
(*Signal to noise ratio*)


snr[h_,noise_,fin_,fend_]:=Block[{cc=299792,\[Rho],f},

\[Rho] = 4/cc^2Re[NIntegrate[SetPrecision[Abs[h[f]]^2/(noise[f]),50],{f,fin,fend},
	MaxRecursion->50,PrecisionGoal->25,AccuracyGoal->25,WorkingPrecision->40]];

Return[Sqrt[\[Rho]]]

]


(* ::Subsubsection:: *)
(*Fisher Matrix*)


fisher[h_,dh_,noise_,fin_,fend_]:=Block[{\[CapitalSigma],\[CapitalGamma],\[CapitalGamma]0,\[Rho],f,dim,i,j,dh1,dh2,k},

dim = Length[dh[f]];

\[Rho] = snr[h,noise,fin,fend];

{\[CapitalGamma]0,\[CapitalSigma],\[CapitalGamma]} = {ConstantArray[0,{dim,dim}],ConstantArray[0,{dim,dim}],ConstantArray[0,{dim,dim}]};

(* ---- Compute the Fisher and Covariance Matrix ---- *)

Print["SNR = ",\[Rho]];

For[i=1,i<=dim,i++,
	For[j=1,j<=i,j++,
	
		dh1[f_] = dh[f][[i]];
		dh2[f_] = dh[f][[j]];
		
		\[CapitalGamma][[i,j]] = If[i==j,scalar[dh1,dh2,noise,fin,fend],
								2scalar[dh1,dh2,noise,fin,fend]];
		]
	];

\[CapitalGamma] = Normal[Symmetrize[\[CapitalGamma]]]+\[CapitalGamma]0;

\[CapitalSigma] = If[PositiveDefiniteMatrixQ[\[CapitalGamma]],LinearSolve[\[CapitalGamma],IdentityMatrix[dim],Method->"Cholesky"],Inverse[\[CapitalGamma]]];

Return[{\[Rho],\[CapitalGamma],\[CapitalSigma]}]

]


(* ::Subsection:: *)
(*End*)


End[];

EndPackage[];
