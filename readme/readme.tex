\documentclass[aps,twocolumn,showpacs,preprintnumbers,nofootinbib,prd,superscriptaddress,groupedaddress,10pt]{revtex4-1}

\makeatletter
\def\l@subsubsection#1#2{}
\def\l@subsubsubsection#1#2{}
\makeatother

\setcounter{secnumdepth}{4}

\usepackage{graphicx,amssymb,amsmath,amsthm,amsfonts,epsfig,epsf,fixmath}
\usepackage[usenames]{color}
\usepackage{epstopdf}

\usepackage{aas_macros}
\usepackage{bm}
\usepackage{dcolumn}
\usepackage{latexsym}
\usepackage{rotating}
\usepackage{longtable}
\def\arraystretch{1.25}
\setlength{\tabcolsep}{12pt}
\usepackage{enumerate}
\usepackage{tensor,multirow}
\usepackage{url}
\usepackage[linktocpage]{hyperref}
\usepackage[T1]{fontenc}

\def\nn{\nonumber}

\DeclareMathOperator{\sech}{sech}
\DeclareMathOperator\arctanh{tanh^{-1}}

\newcommand{\pp}[1]{{\textcolor{red}{\sf{[PP: #1]}} }}
\newcommand{\eb}[1]{{\textcolor{blue}{\sf{[EB: #1]}} }}
\newcommand{\leo}[1]{{\textcolor{magenta}{\sf{[LEO: #1]}} }}
\newcommand{\am}[1]{{\textcolor{cyan}{\sf{[AN: #1]}} }}

\newcommand{\msun}{M_\odot}
\newcommand{\hz}{\textnormal{Hz}}
\newcommand{\km}{\textnormal{km}}

\def\mf{\mathfrak}



\begin{document}
\title{
README: code's repository of the Gravity Theory Group @ Sapienza
}
% \\
% }.}
\author{Tiziano Abdelsalhin$^1$}
\author{Andrea Maselli$^1$}
\author{Paolo Pani$^1$}
\author{Gabriel Andres Piovano$^1$}
\author{Guilherme Raposo$^1$}

\affiliation{$^{1}$ Dipartimento di Fisica, ``Sapienza'' Universit\`a di Roma, Piazzale 
Aldo Moro 5, 00185, Roma, Italy}

\maketitle

\tableofcontents

%--------------------------------------------------------------------------------------
\section{Summary}
%--------------------------------------------------------------------------------------
Webpage: \url{https://web.uniroma1.it/gmunu/}


Each of the following sections provides a basic description of a specific package, 
code, and all the routines included within. The modules can be called using the 
mathematica standard syntax \texttt{<<name\_of\_the\_package\`}

%--------------------------------------------------------------------------------------
\section{\texttt{waveform.m}}
%--------------------------------------------------------------------------------------
This package contains routines to obtain a variety of GW templates in the 
frequency domain, as well as modules that returns orbital variables of 
compact binary systems.

\begin{itemize}
\item \texttt{kerrisco[$\chi$]}: radius of the Kerr ISCO, normalised to the black 
hole mass $M$, as a function of the spin dimensionless parameter $\chi=J/M^2$, 
with $J$ being the BH angular momentum \cite{Bardeen:1972fi}.
\item \texttt{kerrSF[$M/\msun,\eta,\chi_1,\chi_2$]}: frequency in Hz at the location 
of the Kerr ISCO, corrected with self force terms. The function depends on the total 
mass $M$, on the symmetric mass ratio $\eta$, and on the spin variables of the two 
bodies $\chi_{1,2}$ \cite{Favata:2010ic}. 
\item \texttt{finalbh[$m_1,m_2,\chi_1,\chi_2$]}: provides the mass and spin of the 
final BH formed by a binary coalescence using numerical relativity fits in GR \cite{Healy:2014yta}.
\item \texttt{pattern[$\theta,\phi,\psi$]}: returns the pattern functions $(F_+,F_\times)$ 
as a function of the  polar, azimuthal and polarization angle of the binary \cite{Sathyaprakash:2009xs}.
\item \texttt{phenomB[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{t_c}{s},\phi_c$,mode]}: this routine provides the phenomB template for 
spinning non-precessing BBHs, for a given choice of the masses, of the spins, and 
of the source distance \cite{Ajith:2009bn}. The parameter mode can assume two values: 
\texttt{N} and \texttt{PN}, which select a Newtonian or a expanded post-Newtonian 
amplitude for the GW signal. The routine relies on 2 sub-modules 
\texttt{ampB[$f/\hz,{\cal M}/\km,\eta,\chi$]} and \texttt{phaseB[$f/\hz,{\cal M}/\km,\eta,\chi,t_c/\km,\phi_c$]} 
such that:
$$
\texttt{phenomB}[\_]=\texttt{ampB}[\_]e^{-i\ \texttt{phaseB[\_]}}\ ,
$$
where $\chi=(m_1 \chi_1+m_2\chi_2)/(m_1+m_2)$ is the effective spin parameter. Note 
that \texttt{amp}$[\_]$ and \texttt{phaseB}$[\_]$ can be called independently. The amplitude 
is normalised to the Newtonian term with average sky localization.
\item \texttt{dphenomB[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{t_c}{s},\phi_c$,mode]}: returns a 5D vector with the derivative of the 
phenomB template assuming the amplitude to be constant:
$$
\left[{\cal M}\frac{\partial}{\partial {\cal M}}, \eta\frac{\partial}{\partial \eta},
\frac{\partial}{\partial \chi},\frac{\partial}{\partial t_c},
\frac{\partial}{\partial \phi_c}\right]\texttt{phenomB[\_]}\ .
$$
The routine has the same properties of the previously defined \texttt{phenomB}$[\_]$.
\item \texttt{phaseppE[$f/\hz,{\cal M}/\km,\eta,\beta,b]$}: ppE correction to the GW 
inspiral phase, with constant amplitude $\beta$ and exponent $b$ \cite{Yunes:2009ke}. 
Note that the pN order of the modification is equal to $n=(5+b)/2$.
\item \texttt{phenomBppE[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{t_c}{s},\phi_c,\beta,p,\texttt{mode}$]}: phenomB template with ppE correction to the 
inspiral phase only, controlled by the constant amplitude $\beta$ and exponent $b$. This 
routine relies on the previously defined \texttt{phenomB}$[\_]$ and on 
\texttt{phaseppE}$[\_]$:
$$
\texttt{phenomBppE}[\_]=\texttt{phenomB}[\_]e^{-i\ \texttt{phaseBppE[\_]}}\ ,
$$
\item \texttt{dphenomppE[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{t_c}{s},\phi_c,\beta,p,$,mode]}: returns a 6D vector with the derivative of the 
\texttt{phenomBppE}$[\_]$ template assuming the amplitude to be constant:
$$
\left[{\cal M}\frac{\partial}{\partial {\cal M}}, \eta\frac{\partial}{\partial \eta},
\frac{\partial}{\partial \chi},\frac{\partial}{\partial t_c},
\frac{\partial}{\partial \phi_c},\frac{\partial}{\partial \beta}\right]\texttt{phenomBppE[\_]}\ .
$$
\item \texttt{TaylorF2[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{\lambda}{\km^{5}},\frac{t_c}{s},\phi_c$,mode]}: TaylorF2 template in the frequency 
domain including spin corrections and tidal terms trough the Love number. The calling is 
equal to \texttt{phenomB[$\_$]}, with the structure given by
$$
\texttt{TaylorF2}[\_]=\texttt{ampF2}[\_]e^{-i\ \texttt{phaseF2[\_]}}\ ,
$$
The phase of the signal is given by the module  
\texttt{phaseF2[$\frac{f}{\hz},\frac{{\cal M}}{\km},\eta,\chi_s,\chi_a,\frac{\lambda}{\km^5},\frac{t_c}{\km},\phi_c$]}
which takes into account 3pN point-particle corrections, linear spin-orbit up to the 3.5pN, 
and quadratic terms up to the 2pN \cite{Khan:2015jqa}, with $\chi_{s,a}=\frac{\chi_1\pm\chi_2}{2}$.
Tidal corrections up to the 6pN order \cite{Vines:2011ud,Bini:2012gu}. The routine 
\texttt{ampF2[$\frac{f}{\hz},\frac{{\cal M}}{\km},\eta,\chi_s,\chi_a$]} provides the 3pN post-Newtonian 
expanded amplitude \cite{Khan:2015jqa}, normalized to the Newtonian component with average sky 
localization. As for the phenomB, the phase and the amplitude routines can be called independently. 
\item \texttt{dTaylorF2[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{\lambda}{\km^{5}},\frac{t_c}{s},\phi_c$,mode]}: returns a 7D vector with the derivative of the 
\texttt{TaylorF2}$[\_]$ template assuming the amplitude to be constant:
$$
\left[{\cal M}\frac{\partial}{\partial {\cal M}}, \eta\frac{\partial}{\partial \eta},
\frac{\partial}{\partial \chi_s},\frac{\partial}{\partial \chi_a},\frac{\partial}{\partial \lambda},\frac{\partial}{\partial t_c},
\frac{\partial}{\partial \phi_c}\right]\texttt{TaylorF2[\_]}\ .
$$
\item \texttt{TaylorF2ppE[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{t_c}{s},\phi_c,\beta,p,\texttt{mode}$]}: TaylorF2 template with ppE correction to 
the inspiral phase only, controlled by the constant amplitude $\beta$ and exponent $b$. This 
routine relies on the previously defined \texttt{TaylorF2}$[\_]$ without the Love number correction,  
and on \texttt{phaseppE}$[\_]$:
$$
\texttt{TaylorF2ppE}[\_]=\texttt{TaylorF2}[\_]e^{-i\ \texttt{phaseBppE[\_]}}\ ,
$$
\item \texttt{dTaylorF2ppE[$\frac{f}{\hz},\frac{m_1}{\msun},\frac{m_2}{\msun},\chi_1,\chi_2,
\frac{d}{\textnormal{Mpc}},\frac{t_c}{s},\phi_c,\beta,p,$,mode]}: returns a 7D vector with the derivative of the 
\texttt{TaylorF2ppE}$[\_]$ template assuming the amplitude to be constant:
$$
\left[{\cal M}\frac{\partial}{\partial {\cal M}}, \eta\frac{\partial}{\partial \eta},
\frac{\partial}{\partial \chi_s},\frac{\partial}{\partial \chi_a},\frac{\partial}{\partial t_c},
\frac{\partial}{\partial \phi_c},\frac{\partial}{\partial \beta}\right]\texttt{TaylorF2ppE[\_]}\ .
$$



\end{itemize}

%--------------------------------------------------------------------------------------
\section{\texttt{datanalysis.m}}
%--------------------------------------------------------------------------------------
This package contains numerical routines to compute the signal-to-noise ratio and the errors of 
a GW signal for a given noise spectral density, and integration modules to numerically 
compute the overlap between templates in the frequency domain.

\begin{itemize}
\item \texttt{scalar$[h_1,h_2,\frac{\textnormal{noise}}{\hz^{-1}},\frac{f_{in}}{\hz},\frac{f_{end}}{\hz}]$}: 
weighted scalar product in the frequency domain between two waveform, for a 
given PSD and frequency range $[f_{in},f_{end}]$ \cite{Sathyaprakash:2009xs}. This routine 
has a high-precision (but slower) version called \texttt{scalarHP[]}.
\item \texttt{overlap$[h_1,h_2,\frac{\textnormal{noise}}{\hz^{-1}},\frac{f_{in}}{\hz},\frac{f_{end}}{\hz}]$}: 
computes the overlap between two waveforms in the frequency domain maximised over the time and 
phase at the coalescence \cite{Sathyaprakash:2009xs}.
\item \texttt{Cov[$\Gamma_{\alpha\beta},k$]}: provides the covariance matrix for a given Fisher matrix 
$\Gamma_{\alpha\beta}$ using the singular value decomposition with $k$ pivot.
\item \texttt{fisher$[h,dh,\frac{\textnormal{noise}}{\hz^{-1}},\frac{f_{in}}{\hz},\frac{f_{end}}{\hz}]$}:
this functions return the Fisher and the Covariance matrix for a given template in the 
frequency domain, with derivative vector $dh$, detector noise and frequency range 
\cite{Vallisneri:2007ev}.
\item \texttt{snr$[h,\frac{\textnormal{noise}}{\hz^{-1}},\frac{f_{in}}{\hz},\frac{f_{end}}{\hz}]$}:
this functions return the signal-to-noise ratio of a GW signal specified by the template 
$h$, for a given noise spectral density and frequency range \cite{Sathyaprakash:2009xs}. 
\end{itemize}

In addition we provide:
\begin{itemize}
 \item \texttt{RTLNs/emcee.f90}:  Fortran code implementing a MCMC using the emcee algorithm~\cite{emcee}.
\end{itemize}

 


%--------------------------------------------------------------------------------------
\section{\texttt{stellar.m}}
%--------------------------------------------------------------------------------------
Routines to integrate the TOV equation for a given equation of state, and build 
the stellar structure of slowly rotating NS. This repository includes a set of realistic 
EoS within the folder \texttt{eos\_tabulated}. 

\begin{itemize}
\item \texttt{tov[eos,$\frac{p_0}{\km^2}$,poly,K,$\gamma$,profile]}: solve the Tolman Oppenheimer 
Volkoff equations for a central pressure $p_0$ and a given EoS. The flag \texttt{poly} selects 
if the EoS is tabulated \texttt{F} or polytropic \texttt{F}. in the former the table is passed to 
the routine through \texttt{eos}, while in the latter the energy density profile is specified 
by the constant $K$ and $\gamma$, such that $p=K\rho^\gamma$. Note that we consider 
energy-polytropes\footnote{For mass polytropes change this to 
$\epsilon=\rho+\frac{p}{\gamma-1}$.}, such that $\epsilon=\rho$. The initial pressure and 
the differential equations use geometrized units.
The code solves the stellar structure including linear order correction in the rotation rate 
\cite{Hartle:1967he}, and tidal deformations \cite{Hinderer:2007mb}. 
For tabulated EoS the integration uses the module \texttt{interpeos[eos]} to interpolate the energy 
density profiles. The interpolation returns an array with $\epsilon$ as a function of the logarithm 
of the pressure, in geometrized units. The routine is written to load an array which specifies the 
tabulated EoS in the form $[\textnormal{void},\rho_i(\textnormal{eV/fm}^{3}),\epsilon_i(\textnormal{g/cm}^{3}),p_i(\textnormal{dyn/fm}^{2})$]. For different input units  change accordingly.
If the flag \texttt{profile} is equal to $F$ the routine returns and array with the mass, the radius, 
the moment of inertia and the love number of the star. If \texttt{profile}=\texttt{T} the code 
also yields a table which contains the values of the radial coordinate, the pressure, the  mass density 
and the energy density profiles inside the star, from the center to the radius.
\item \texttt{tovPW[$\frac{p_0}{\km^2},p_1,\gamma_1,\gamma_2,\gamma_3$,profile]}: TOV solver 
for piecewise polytropic EoS \cite{Read:2008iy}, specified by the parameters $(p_1,\gamma_{1,2,3})$ 
and central pressure $p_0$. The routine uses the module 
\texttt{pweos[$\frac{p}{\km^2},,p_1,\gamma_1,\gamma_2,\gamma_3$]} to build the equation of state. 
It returns and array with the mass, the radius, the moment of inertia and the love number of the star. 
\end{itemize}


%--------------------------------------------------------------------------------------
\section{\texttt{ringdown.m}}
%--------------------------------------------------------------------------------------
Module with templates and functions used to describe the post-merger phase 
of a binary coalescence.

\begin{itemize}
\item\texttt{spheroidalkerr[$s,l,m,\chi$]}: solve the differential equation for the 
spheroidal function of Kerr, for a given set of quantum numbers $(s,l,m)$ and 
spin parameter $\chi$ \cite{Berti:2005gp}. The routine returns the frequency, the 
angular separation constant, and the (not normalized) angular eigenfunctions 
$_{s}S_{lm}(x)$, where $x=\cos\theta$.
\item \texttt{ringwave[$f/\hz,A_+,\phi_+,\tau, \omega,\tau,S_{lm}$]}: returns an array with 
the $(h_+,h_\times)$ component of the BH ringdown signal in the frequency domain, 
as the function of the $+$ amplitude and phase, of the mode frequency and damping 
time, and of the spheroidal function. We follow the notation of \cite{Berti:2005ys} such 
that the phase and the amplitude of the $\times$ polarization are proportional to the 
$+$ component. Frequency and damping times are normalised such that the the 
BH mass is 1.
\item \texttt{pattern[$f,\theta,\phi,\psi$]}: same routine of the module 
\texttt{waveform.m}.
\item \texttt{dringwave[$f/\hz,A_+,\phi_+,\tau, \omega,\tau,S_{lm}$]}: returns an 4D array 
with the derivatives of the ringdown templates:
$$
\left[ A_+\frac{\partial}{\partial A_+}, \frac{\partial}{\partial \phi_+},
\frac{\partial}{\partial \omega},\frac{\partial}{\partial \tau}\right]\texttt{ringwave[\_]}\ .
$$
Note that the spheroidal function is kept fixed.
\end{itemize}

%--------------------------------------------------------------------------------------
\section{Constants}
%--------------------------------------------------------------------------------------
Name and values of constants used throughout the repository.

\begin{itemize}
\item \texttt{msun}=1.4768: $1\msun$ in \km.
\item \texttt{cc}=299792 km/s: speed of light.
\item \texttt{pc}=$3.085677 10^{13}$ km: 1 parsec in km.
\item \texttt{mB}=938.27  MeV: mass of the proton in electronvolt.
\item $\rho_{fact}=1.322\times10^{-12}$: changes density in eV/fm$^3$ to km$^{-2}$.
\item $\epsilon_{fact}=7.42427\times10^{-19}$: changes energy density in g/cm$^3$ to km$^{-2}$.
\item $p_{fact}=8.2611 \times10^{-40}$: changes pressure in dyn/cm$^2$ to km$^{-2}$.

\end{itemize}

%--------------------------------------------------------------------------------------
\section{\texttt{QNMs}}
%--------------------------------------------------------------------------------------
This subfolder contains various routines to find the quasinormal modes of a black hole in general relativity and in 
other theories of gravity:

\begin{itemize}
 \item \texttt{CF\_matrix\_3terms.nb}:  scalar, electromagnetic and gravitational QNMs of a
Schwarzschild BH computed with a matrix-valued continued fraction method. 
 \item \texttt{DirectIntegration\_CD.nb}: Computation of QNMs with direct integration
 \item \texttt{series\_method\_DCS.nb}:  Gravito-scalar QNMs of Schwarzschild–AdS BH in DCS gravity computed with a 
matrix-valued series method.
 \item \texttt{DCS\_pert\_eqs.nb}: Derivation of the gravito-scalar perturbation equations of  with a matrix-valued 
direct integration.
 \item \texttt{DCS\_DI.nb}: Gravito-scalar QNMs of Schwarzschild BHs in DCS gravity computed  with a matrix-valued 
continued fraction method.
\end{itemize}


%--------------------------------------------------------------------------------------
\section{\texttt{GW echoes}}
%--------------------------------------------------------------------------------------
This folder contains two notebooks:
%
\begin{itemize}
 \item \texttt{echo\_template\_spinning.nb}: analytical template in the frequency domain using low-frequency 
approximation of Teukolsky's equation~\cite{Maggio:2019zyv}
 \item \texttt{echo\_template\_nonspinning.nb}: analytical template in the frequency domain, using Poschl-Teller 
approximation~\cite{Testa:2018bzd}
\end{itemize}
%%%
It also contains a subfolder called ``catalogue'' which is an extended version of the echo catalogue (waveforms plus 
audio files) in \url{https://www.darkgra.org/gw-echo-catalogue.html}.


%--------------------------------------------------------------------------------------
\section{\texttt{soft\_ECO\_repository}}
%--------------------------------------------------------------------------------------
Particular solutions of exotic compact objects with soft hair~\cite{Raposo:2018xkf}.
%
The prefix in the file name denotes the solution according to the adopted nomenclature. For example, file JS2M2-solution 
contains the solution $[JS_2M_2]$ presented in~\cite{Raposo:2018xkf}.

Each file contains a Mathematica table containing the form of the covariant metric of the respective solution up to a 
given order. The coordinates are respectively $(t,r,\theta, \phi)$

Nomenclature:

\begin{itemize}
 \item "Mass": Physical Mass of the object;
 \item $J$: Physical Angular momentum of the object;
 \item $M_2$:  Physical mass quadrupole of the object;
 \item $S_2$:  Physical current quadrupole of the object;
 \item $M_\ell$:  Physical mass $\ell$-pole of the object;
 \item $S_\ell$: Physical current $\ell$-pole of the object;
 \item $\epsilon_p$:  Bookeeping parameter that denotes the order in order in perturbation theory;  
\end{itemize}


%--------------------------------------------------------------------------------------
\section{\texttt{RTLNs}: Rotational Tidal Love Numbers}
%--------------------------------------------------------------------------------------
(For more information, see \texttt{RTLNs/RTLN\_notes.pdf}.

\begin{itemize}
 \item \texttt{RTLN\_EOM.nb}: This notebook contains the computations to obtain the equations of motion, the orbital 
energy and the GW flux of a gravitating two-body system modeled as two point-like spinning masses endowed with a series 
of multipole moments~\cite{Abdelsalhin:2018reg}.
Only quadrupole and octupole (both mass and current) moments are retained. Tidal effects are taken into account by 
using the adiabatic relations~\cite{Vines:2010ca}.
%%%%
 \item\texttt{taylorF2\_GW\_phase.nb}: This notebook contains the TaylorF2 GW phase emitted by an inspiralling compact 
binary, computed from the expressions of orbital energy and GW flux~\cite{Vines:2011ud}.
Black holes terms are up to 1.5 PN order (spin terms), whereas tidal terms are up to 6.5~PN order, including the 
spin-TLNs coupling and the effect of RTLNs~\cite{Abdelsalhin:2018reg}.
\end{itemize}










%--------------------------------------------------------------------------------------
\section{\texttt{BH\_solutions}}
%--------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------
\section{\texttt{SlowRot}}
%--------------------------------------------------------------------------------------

%--------------------------------------------------------------------------------------
\section{\texttt{Superradiance}}
%--------------------------------------------------------------------------------------

\begin{itemize}
 \item \texttt{Superradiance\_Stars.nb}: Superradiance in spinning conducting stars~\cite{Cardoso:2017kgn}.
\end{itemize}


%--------------------------------------------------------------------------------------
\section{\texttt{EMRI spinning secondary CEO}}
%--------------------------------------------------------------------------------------
{\bf Section to be completed}

Fluxes and phases data for a spinning particle with spin $\chi$ in circular equatorial
orbit (CEO) in Kerr space time. See ["Model independent tests of the Kerr bound with extreme mass ratio inspirals", G. 
A. Piovano, A. Maselli, P. Pani (2020)]
["Extreme mass ratio inspirals with spinning secondary:a detailed study of equatorial circular motion", G. A. Piovano, 
A. Maselli, P. Pani (2020)]



\bibliographystyle{utphys}
\bibliography{References}

\end{document}
