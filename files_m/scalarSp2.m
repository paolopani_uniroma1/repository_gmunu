{{0, 0}, 
 {-(\[Sigma]^2*((-3*\[ScriptCapitalM]^3*
       (r*(40 + 29*\[ScriptL] + 7*\[ScriptL]^2)*\[ScriptCapitalM] - 
        32*\[ScriptCapitalM]^2 + 2*r^4*\[Omega]^2)*Q[1 + \[ScriptL], m]*
       Q[2 + \[ScriptL], m])/(r^6*(r - 2*\[ScriptCapitalM])) - 
     (\[ScriptCapitalM]^3*\[Zeta]^2*(-350*r^3*(477800 + 160453*\[ScriptL] + 
          38175*\[ScriptL]^2)*\[ScriptCapitalM]^6 + 
        2800*r^2*(178984 + 63651*\[ScriptL] + 15673*\[ScriptL]^2)*
         \[ScriptCapitalM]^7 - 98000*r*(8800 + 1677*\[ScriptL] + 
          423*\[ScriptL]^2)*\[ScriptCapitalM]^8 + 
        580160000*\[ScriptCapitalM]^9 + 62482*r^11*\[Omega]^2 + 
        187446*r^10*\[ScriptCapitalM]*\[Omega]^2 + 
        399225*r^9*\[ScriptCapitalM]^2*\[Omega]^2 + r^6*\[ScriptCapitalM]^3*
         (7122456 + 3436445*\[ScriptL] + 798519*\[ScriptL]^2 - 
          4869200*\[ScriptCapitalM]^2*\[Omega]^2) - 
        42*r^7*\[ScriptCapitalM]^2*(10384 + 6490*\[ScriptL] + 
          1298*\[ScriptL]^2 + 11875*\[ScriptCapitalM]^2*\[Omega]^2) - 
        6*r^8*\[ScriptCapitalM]*(45430 + 22715*\[ScriptL] + 
          4543*\[ScriptL]^2 + 125690*\[ScriptCapitalM]^2*\[Omega]^2) - 
        20*r^4*\[ScriptCapitalM]^5*(-2989496 - 809715*\[ScriptL] - 
          198105*\[ScriptL]^2 + 803600*\[ScriptCapitalM]^2*\[Omega]^2) + 
        5*r^5*\[ScriptCapitalM]^4*(-2673415*\[ScriptL] - 
          665317*\[ScriptL]^2 + 56*(-98009 + 28490*\[ScriptCapitalM]^2*
             \[Omega]^2)))*Q[1 + \[ScriptL], m]*Q[2 + \[ScriptL], m])/
      (98000*r^12*(r - 2*\[ScriptCapitalM])^2) - 
     (\[ScriptCapitalM]^3*\[Zeta]^3*
       (-28224000*r^3*(79718476 + 7990045*\[ScriptL] + 2020123*\[ScriptL]^2)*
         \[ScriptCapitalM]^9 + 1267728000*r^2*(1341208 + 229445*\[ScriptL] + 
          54175*\[ScriptL]^2)*\[ScriptCapitalM]^10 - 135224320000*r*
         (21672 + 6290*\[ScriptL] + 1585*\[ScriptL]^2)*\[ScriptCapitalM]^11 + 
        3612517708800000*\[ScriptCapitalM]^12 + 171696544404*r^14*
         \[Omega]^2 + 515089633212*r^13*\[ScriptCapitalM]*\[Omega]^2 + 
        1114949414700*r^12*\[ScriptCapitalM]^2*\[Omega]^2 - 
        940800*r^4*\[ScriptCapitalM]^8*(-1770942597 - 557137490*\[ScriptL] - 
          138981838*\[ScriptL]^2 + 95942000*\[ScriptCapitalM]^2*\[Omega]^2) - 
        22400*r^5*\[ScriptCapitalM]^7*(24719467610 + 9742264105*\[ScriptL] + 
          2336830349*\[ScriptL]^2 + 882127400*\[ScriptCapitalM]^2*
           \[Omega]^2) - 154*r^10*\[ScriptCapitalM]^2*(7285108104 + 
          4553192565*\[ScriptL] + 910638513*\[ScriptL]^2 + 
          5000382500*\[ScriptCapitalM]^2*\[Omega]^2) + 
        33*r^11*\[ScriptCapitalM]*(-21248231970 - 10624115985*\[ScriptL] - 
          2124823197*\[ScriptL]^2 + 17787721240*\[ScriptCapitalM]^2*
           \[Omega]^2) - 44*r^9*\[ScriptCapitalM]^3*(-161890236828 - 
          64610238285*\[ScriptL] - 15273940347*\[ScriptL]^2 + 
          140307823600*\[ScriptCapitalM]^2*\[Omega]^2) + 
        40*r^7*\[ScriptCapitalM]^5*(543627404606 - 42088238335*\[ScriptL] - 
          14983126895*\[ScriptL]^2 + 321897189600*\[ScriptCapitalM]^2*
           \[Omega]^2) - 60*r^8*\[ScriptCapitalM]^4*(259387032674 + 
          130325560915*\[ScriptL] + 34117743077*\[ScriptL]^2 + 
          404474859040*\[ScriptCapitalM]^2*\[Omega]^2) - 
        15400*r^6*\[ScriptCapitalM]^6*(-1297320291*\[ScriptL] - 
          292743573*\[ScriptL]^2 + 40*(-78881443 + 82083918*\[ScriptCapitalM]^
              2*\[Omega]^2)))*Q[1 + \[ScriptL], m]*Q[2 + \[ScriptL], m])/
      (380318400000*r^15*(r - 2*\[ScriptCapitalM])^2) - 
     (\[ScriptCapitalM]^3*\[Zeta]^4*(783151649280000*r^3*
         (95282748560152 + 19668330472539*\[ScriptL] + 4841037319377*
           \[ScriptL]^2)*\[ScriptCapitalM]^13 - 383744308147200000*r^2*
         (370590165888 + 64369473311*\[ScriptL] + 16060513469*\[ScriptL]^2)*
         \[ScriptCapitalM]^14 + 6036297967155456000000*r*
         (29137968 + 2823665*\[ScriptL] + 711715*\[ScriptL]^2)*
         \[ScriptCapitalM]^15 - 93304747841752686919680000000*
         \[ScriptCapitalM]^16 + 81049063336212857729844*r^18*\[Omega]^2 + 
        81049063336212857729844*r^17*\[ScriptCapitalM]*\[Omega]^2 + 
        40583794899252851096136*r^16*\[ScriptCapitalM]^2*\[Omega]^2 + 
        2738292480000*r^4*\[ScriptCapitalM]^12*(-12533224111569256 - 
          2555849711811583*\[ScriptL] - 627622359662221*\[ScriptL]^2 + 
          664494999168000*\[ScriptCapitalM]^2*\[Omega]^2) - 
        60850944000*r^5*\[ScriptCapitalM]^11*(-258719477176191553 - 
          54312543985485105*\[ScriptL] - 13513398216540715*\[ScriptL]^2 + 
          27145608488598600*\[ScriptCapitalM]^2*\[Omega]^2) + 
        4704000*r^6*\[ScriptCapitalM]^10*(-1404869906615978202332 - 
          382160274243487718685*\[ScriptL] - 94130030719557170079*
           \[ScriptL]^2 + 198532514009942592480*\[ScriptCapitalM]^2*
           \[Omega]^2) - 1881600*r^7*\[ScriptCapitalM]^9*
         (-876744148079685832878 - 321842012411320373735*\[ScriptL] - 
          77416217961293021077*\[ScriptL]^2 + 202441633876576294200*
           \[ScriptCapitalM]^2*\[Omega]^2) + 89600*r^8*\[ScriptCapitalM]^8*
         (-484817820348997018754 + 91287292664372901005*\[ScriptL] + 
          25991200386549857641*\[ScriptL]^2 + 1742841068058120561300*
           \[ScriptCapitalM]^2*\[Omega]^2) - 429*r^15*\[ScriptCapitalM]*
         (742232901774080240090 + 371116450887040120045*\[ScriptL] + 
          74223290177408024009*\[ScriptL]^2 + 1753003826701162679320*
           \[ScriptCapitalM]^2*\[Omega]^2) - 572*r^14*\[ScriptCapitalM]^2*
         (-222669870532224072027 + 2165323889889208792420*\[ScriptCapitalM]^2*
           \[Omega]^2) - 572*r^13*\[ScriptCapitalM]^3*
         (-7249286747566279857132 - 3380369987474068622655*\[ScriptL] - 
          760117123316396832711*\[ScriptL]^2 + 5681128998249094443200*
           \[ScriptCapitalM]^2*\[Omega]^2) + 104*r^12*\[ScriptCapitalM]^4*
         (-97472198862283396015161 - 40169791723895854425570*\[ScriptL] - 
          10133618576082228298104*\[ScriptL]^2 + 9019028591926007429600*
           \[ScriptCapitalM]^2*\[Omega]^2) + 520*r^11*\[ScriptCapitalM]^5*
         (39636553767126432220016 + 11927776875751249462895*\[ScriptL] + 
          2897066397500884787167*\[ScriptL]^2 + 23630617389554690907840*
           \[ScriptCapitalM]^2*\[Omega]^2) + 80*r^10*\[ScriptCapitalM]^6*
         (-800718413971412789661666 - 257345636362960109077685*\[ScriptL] - 
          61016069829975888128215*\[ScriptL]^2 + 395390739150081513963600*
           \[ScriptCapitalM]^2*\[Omega]^2) - 11200*r^9*\[ScriptCapitalM]^7*
         (213047245821100242149*\[ScriptL] + 16501831755848499580*
           \[ScriptL]^2 + 90*(-41770493694562241167 + 99201372216422718088*
             \[ScriptCapitalM]^2*\[Omega]^2)))*Q[1 + \[ScriptL], m]*
       Q[2 + \[ScriptL], m])/(181088939014663680000000*r^18*
       (r - 2*\[ScriptCapitalM])^3))), 
  -(\[Sigma]^2*((48*\[ScriptCapitalM]^4*Q[1 + \[ScriptL], m]*
       Q[2 + \[ScriptL], m])/r^5 - (\[ScriptCapitalM]^4*
       (13629*r^6 + 68145*r^5*\[ScriptCapitalM] - 
        281910*r^4*\[ScriptCapitalM]^2 + 759850*r^3*\[ScriptCapitalM]^3 - 
        1691200*r^2*\[ScriptCapitalM]^4 + 5071500*r*\[ScriptCapitalM]^5 - 
        18130000*\[ScriptCapitalM]^6)*\[Zeta]^2*Q[1 + \[ScriptL], m]*
       Q[2 + \[ScriptL], m])/(12250*r^11) + 
     (\[ScriptCapitalM]^4*(-70119165501*r^9 - 350595827505*r^8*
         \[ScriptCapitalM] + 38868188040*r^7*\[ScriptCapitalM]^2 - 
        1074194502150*r^6*\[ScriptCapitalM]^3 + 898189014800*r^5*
         \[ScriptCapitalM]^4 + 15196791733200*r^4*\[ScriptCapitalM]^5 - 
        35340820368000*r^3*\[ScriptCapitalM]^6 + 115455792144000*r^2*
         \[ScriptCapitalM]^7 + 81540264960000*r*\[ScriptCapitalM]^8 + 
        225782356800000*\[ScriptCapitalM]^9)*\[Zeta]^3*Q[1 + \[ScriptL], m]*
       Q[2 + \[ScriptL], m])/(95079600000*r^14) - 
     (\[ScriptCapitalM]^4*(31841791486108042299861*r^12 + 
        159208957430540211499305*r^11*\[ScriptCapitalM] + 
        8412228766139457247560*r^10*\[ScriptCapitalM]^2 + 
        167000378077951616848650*r^9*\[ScriptCapitalM]^3 - 
        1715759510541126192942800*r^8*\[ScriptCapitalM]^4 - 
        674741103006751790209200*r^7*\[ScriptCapitalM]^5 + 
        5976755417397071890128000*r^6*\[ScriptCapitalM]^6 + 
        41985948901069384146936000*r^5*\[ScriptCapitalM]^7 - 
        111189600129161610524160000*r^4*\[ScriptCapitalM]^8 + 
        176982863536533084750720000*r^3*\[ScriptCapitalM]^9 - 
        575225786722959045273600000*r^2*\[ScriptCapitalM]^10 + 
        733124987930439808704000000*r*\[ScriptCapitalM]^11 - 
        2915773370054771466240000000*\[ScriptCapitalM]^12)*\[Zeta]^4*
       Q[1 + \[ScriptL], m]*Q[2 + \[ScriptL], m])/(45272234753665920000000*
       r^17)))}}
