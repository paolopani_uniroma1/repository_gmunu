(* ::Package:: *)

BeginPackage["waveform`"];


(* ::Subsubsection::Closed:: *)
(*Usage*)


kerrisco::usage =
 	"kerrisco[\[Chi]] computes the Isco for Kerr BH as afunction of the spin parameter.";

kerrSF::usage =
 	"kerrSF[M,\[Eta],\[Chi]1,\[Chi]2] computes the Isco frequency (in Hz) for Kerr BH with Self Force corrections.";
 	
finalbh::usage =
 	"finalbh[m1,m2,\[Chi]1,\[Chi]2] computes the final mass and spin for binary BH in GR using NR fits.";
 	
pattern::usage=
  "pattern[\[Theta],\[Phi],\[Psi]] returns the GW pattern functions (fx,fp) as a function of the polar, azimuthal and polarization angle";
  
coeffD::usage =
     "Coefficients for the phenomD template";
     
ampD::usage =
     "Amplitude of the phenomD template";
     
phaseD::usage =
     "phase of the phenomD template";
     
phenomD::usage =
     "phenomD[f,m1,m2,\[Chi]1,\[Chi]2,d,tc,\[Phi]c,mode] gives the PhenomD template. Mode = N or PN for Newtonian/post Newtonian amplitude.";
         
ampB::usage =
     "ampB[f,\[ScriptCapitalM],\[Eta],\[Chi]] amplitude of the phenomB template.";
     
phaseB::usage =
     "phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c] phase of the phenomB template.";
     
phenomB::usage =
     "phenomB[f,m1,m2,\[Chi]1,\[Chi]2,d,tc,\[Phi]c,mode] gives the full PhenomB template. Mode = N/PN for Newtonian/post Newtonian amplitude.";

dphenomB::usage =
     "dphenomB[f,m1,m2,\[Chi]1,\[Chi]2,d,tc,\[Phi]c,mode] derivative of the PhenomB template. Mode = N/PN for Newtonian/post Newtonian amplitude. The overall amplitude is kept constant.";
     
phaseF2::usage =
     "phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c] pN expanded phase of the TaylorF2 template.";

ampF2::usage =
     "ampF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a] pN expanded amplitude of the TaylorF2 template.";
     
TaylorF2::usage =
     "TaylorF2[f,m1,m2,\[Chi]1,\[Chi]2,d,\[Lambda],tc,\[Phi]c,mode] returns the TaylorF2 template in the frequency domain including tidal corrections.";

dTaylorF2::usage =
     "dTaylorF2[f,m1,m2,\[Chi]1,\[Chi]2,d,\[Lambda],tc,\[Phi]c,mode] derivative of the TaylorF2";
     
phaseppE::usage =
     "phaseppE[f,\[ScriptCapitalM],\[Eta],\[Beta],b] ppE phase with constant amplitude \[Beta] and exponent b. ";

phenomBppE::usage =
     "phenomBppE[f,m1,m2,\[Chi]1,\[Chi]2,d,tc,\[Phi]c,\[Beta],b,mode] phenomB template with ppE correction to the inspiral phase.";
          
dphenomBppE::usage =
     "dphenomBppE[f,m1,m2,\[Chi]1,\[Chi]2,d,tc,\[Phi]c,\[Beta],b,mode] derivative of the phenomBppE template with ppE correction to the inspiral phase.";     
     
TaylorF2ppE::usage=
  "TaylorF2ppE[f,m1,m2,\[Chi]1,\[Chi]2,d,tc,\[Phi]c,\[Beta],b,mode] phenomB template with ppE correction and no Love number.";

dTaylorF2ppE::usage=
  "dTaylorF2ppE[f,m1,m2,\[Chi]1,\[Chi]2,d,tc,\[Phi]c,\[Beta],b,mode] derivative of the TaylorF2 template with ppE correction.";


Begin["`Private`"];


(* ::Subsubsection::Closed:: *)
(*Kerr ISCO radius*)


kerrisco[\[Chi]_]:=Block[{Z1,Z2},

Z1=1+(1-\[Chi]^2)^(1/3) ((1+\[Chi])^(1/3)+(1-\[Chi])^(1/3));
Z2=Sqrt[3\[Chi]^2+Z1^2];

Return[3+Z2-Sign[\[Chi]]Sqrt[(3-Z1)(3+Z1+2Z2)]];

]


(* ::Subsubsection::Closed:: *)
(*Kerr ISCO frequency with SF corrections*)


kerrSF[M_,\[Eta]_,\[Chi]1_,\[Chi]2_]:=Block[{cGSF,cGSF0,cspin,cspin0,cc=299792,msun=1.4768},

cGSF={{-0.99,1.1903},{-0.9,1.1961},{-0.8,1.2043},{-0.7,1.2148},{-0.6,1.2282},
		{-0.5,1.2453},{-0.4,1.2670},{-0.3,1.2948},{-0.2,1.3303},{-0.1,1.3759},
			{0,1.4349},{0.1,1.5116},{0.2,1.6118},{0.3,1.7434},{0.4,1.9167},
				{0.5,2.1441},{0.6,2.4388},{0.7,2.8098} ,{0.8,3.2524},
					{0.9,3.7337},{0.999,4.1440}};

cGSF0=ListInterpolation[cGSF[[All,2]],cGSF[[All,1]],Method->"Spline"];

cspin={{-0.99,0.1945},{-0.9,0.2020},{-0.8,0.2110},{-0.7,0.2205},{-0.6,0.2308},
		{-0.5,0.2417},{-0.4,0.2534},{-0.3,0.2657},{-0.2,0.2788},{-0.1,0.2923},
			{0,0.3062},{0.1,0.3199},{0.2,0.3328},{0.3,0.3435},{0.4,0.3502},
				{0.5,0.3499},{0.6,0.3382},{0.7,0.3097} ,{0.8,0.2590},
					{0.9,0.1837},{0.999,0.0983}};

cspin0=ListInterpolation[cspin[[All,2]],cspin[[All,1]],Method->"Spline"];

Return[cc/(\[Pi] M msun)/(kerrisco[\[Chi]1]^(3/2) + \[Chi]1)(1 + \[Eta] cGSF0[\[Chi]1] + \[Eta] \[Chi]2 cspin0[\[Chi]1])];

]


(* ::Subsubsection::Closed:: *)
(*NR fits for final mass and spin*)


finalbh[m1_,m2_,\[Chi]1_,\[Chi]2_]:=Block[{M0,K1,K2a,K2b,K2c,K2d,K3a,K3b,K3c,K3d,K4a,K4b,K4c,K4d,K4e,K4f,K4g,K4h,K4i,L0,L1,L2a,L2b,L2c,L2d,L3a,L3b,L3c,L3d,L4a,L4b,L4c,L4d,L4e,L4f,L4g,L4h,L4i,\[ScriptCapitalE],\[ScriptCapitalJ],\[Chi],M,\[Eta],s,\[Delta]m,\[CapitalDelta],Mf,\[Chi]f},

{M,\[Eta]}={m1+m2,(m1 m2)/(m1+m2)^2};
{s,\[Delta]m,\[CapitalDelta]}={(\[Chi]1 m1^2+\[Chi]2 m2^2)/M,(m1-m2),\[Chi]2 m2-\[Chi]1 m1}/M;

{M0,K1,K2a,K2b,K2c,K2d,K3a,K3b,K3c,K3d,K4a,K4b,K4c,K4d,K4e,K4f,K4g,K4h,K4i}={0.951507,\[Minus]0.051379,\[Minus]0.004804,\[Minus]0.054522,-0.000022,1.995246,0.007064,-0.017599,-0.119175,0.025000,-0.068981,-0.011383,-0.002284,-0.165658,0.019403,2.980990,0.020250,-0.004091,0.078441};

{L0,L1,L2a,L2b,L2c,L2d,L3a,L3b,L3c,L3d,L4a,L4b,L4c,L4d,L4e,L4f,L4g,L4h,L4i}={0.686710,0.613247,-0.145427,-0.115689,-0.005254,0.801838,-0.073839,0.004759,-0.078377,1.585809,-0.003050,-0.002968,0.004364,-0.047204,-0.053099,0.953458,-0.067998,0.001629,-0.066693};

\[ScriptCapitalE][\[Chi]_]:=(1-2/kerrisco[\[Chi]]+\[Chi]/kerrisco[\[Chi]]^(3/2))/Sqrt[1-3/kerrisco[\[Chi]]+2\[Chi]/kerrisco[\[Chi]]^(3/2)];\[ScriptCapitalJ][\[Chi]_]:=2/Sqrt[3 kerrisco[\[Chi]]] (3 kerrisco[\[Chi]]^(1/2)-2 \[Chi]);

\[Chi]f=FindRoot[(4\[Eta])^2 (L0+L1 s+L2a \[CapitalDelta] \[Delta]m +L2b s^2+L2c \[CapitalDelta]^2+L2d \[Delta]m^2+L3a \[CapitalDelta] s \[Delta]m+L3b s \[CapitalDelta]^2+L3c s^3+L3d s \[Delta]m^2+L4a s^2 \[CapitalDelta] \[Delta]m+L4b \[CapitalDelta]^3 \[Delta]m+L4c \[CapitalDelta]^4+L4d s^4+L4e \[CapitalDelta]^2 s^2+L4f \[Delta]m^4+L4g \[CapitalDelta] \[Delta]m^3+L4h \[CapitalDelta]^2 \[Delta]m^2+L4i s^2 \[Delta]m^2+s(1+8\[Eta])\[Delta]m^4+\[Eta] \[ScriptCapitalJ][\[Chi]] \[Delta]m^6)==\[Chi],{\[Chi],0.5}][[1,2]];

Mf=M (4\[Eta])^2 (M0+K1 s+K2a \[CapitalDelta] \[Delta]m +K2b s^2+K2c \[CapitalDelta]^2+K2d \[Delta]m^2+K3a \[CapitalDelta] s \[Delta]m+K3b s \[CapitalDelta]^2+K3c s^3+K3d s \[Delta]m^2+K4a s^2 \[CapitalDelta] \[Delta]m+K4b \[CapitalDelta]^3 \[Delta]m+K4c \[CapitalDelta]^4+K4d s^4+K4e \[CapitalDelta]^2 s^2+K4f \[Delta]m^4+K4g \[CapitalDelta] \[Delta]m^3+K4h \[CapitalDelta]^2 \[Delta]m^2+K4i s^2 \[Delta]m^2+(1+\[Eta](\[ScriptCapitalE][\[Chi]f]+11))\[Delta]m^6);

Return[{Mf,\[Chi]f}];

]


(* ::Subsubsection::Closed:: *)
(*GW pattern function*)


pattern[\[Theta]_,\[Phi]_,\[Psi]_]:=Block[{fp,fx},

fp = 1/2 (1+Cos[\[Theta]]^2)Cos[2\[Phi]]Cos[2\[Psi]]-Cos[\[Theta]]Sin[2\[Phi]]Sin[2\[Psi]];

fx = 1/2 (1+Cos[\[Theta]]^2)Cos[2\[Phi]]Sin[2\[Psi]]+Cos[\[Theta]]Sin[2\[Phi]]Cos[2\[Psi]];

Return[{fp,fx}]

]


(* ::Subsection::Closed:: *)
(*PhenomD*)


(* ::Subsubsection::Closed:: *)
(*PhenomD coefficients*)


coeffD[\[Eta]_,\[Chi]_]:=Block[{\[Lambda]00,\[Lambda]10,\[Lambda],k,i,j},

\[Lambda]00={2096.55,-10114.1,22933.7,\[Minus]14621.7,3931.9,\[Minus]40105.5,83208.4,43.3151,\[Minus]0.0702021,9.59881,\[Minus]0.0298949,0.997441,97.8975,\[Minus]3.2827,\[Minus]2.51564*10^-5,0.0069274,1.01034,1.30816,0.814984};

\[Lambda]10={1463.75,-44631,230960,\[Minus]377813,\[Minus]17395.8,112253,\[Minus]191238,638.633,\[Minus]0.162698,\[Minus]397.054,1.40221,\[Minus]0.00788445,\[Minus]42.6597,\[Minus]9.05138,1.97503 10^-5,0.0302047,0.000899312,\[Minus]0.00553773,2.57476};

\[Lambda]={{{1312.55,18307.3,-43534.1},{-833.289,32047.3,-108609},{452.251,8353.44,-44531.3}},{{\[Minus]6541.31,\[Minus]266959,686328},{3405.64,\[Minus]437508,1.63182 10^6},{\[Minus]7462.65,\[Minus]114585,674402}},{{14961.1,1.19402 10^6,\[Minus]3.10422 10^6},{\[Minus]3038.17,1.87203 10^6,\[Minus]7.30915 10^6},{42738.2,467502.,\[Minus]3.06485*10^6}},
{{\[Minus]9608.68,\[Minus]1.71089*10^6,4.33292 10^6},{\[Minus]22366.7,\[Minus]2.50197*10^6,1.02745 10^7},{\[Minus]85360.3,\[Minus]570025,4.39684 10^6}},{{3132.38,343966,\[Minus]1.21626*10^6},{\[Minus]70698,1.38391 10^6,\[Minus]3.96628*10^6},{\[Minus]60017.5,803515,\[Minus]2.09171*10^6}},{{23561.7,\[Minus]3.47618*10^6,1.13759 10^7},{754313,\[Minus]1.30848*10^7,3.64446 10^7},{596227.,\[Minus]7.42779*10^6,1.8929 10^7}},{{\[Minus]210916,8.71798 10^6,\[Minus]2.69149*10^7},{\[Minus]1.98898*10^6,3.0888 10^7,\[Minus]8.39087*10^7},{\[Minus]1.4535*10^6,1.70635 10^7,\[Minus]4.27487*10^7}},
{{\[Minus]32.8577,2415.89,\[Minus]5766.88},{\[Minus]61.8546,2953.97,\[Minus]8986.29},{\[Minus]21.5714,981.216,\[Minus]3239.57}},{{\[Minus]0.187251,1.13831,\[Minus]2.83342},{\[Minus]0.17138,1.71975,\[Minus]4.53972},{\[Minus]0.0499834,0.606207,\[Minus]1.68277}},{{16.2021,\[Minus]1574.83,3600.34},{27.0924,\[Minus]1786.48,5152.92},{11.1757,\[Minus]577.8,1808.73}},{{\[Minus]0.0735605,0.833701,0.2240010},{\[Minus]0.0552029,0.566719,0.718693},{\[Minus]0.0155074,0.157503,0.210768}},{{\[Minus]0.0590469,1.39587,\[Minus]4.51663},{\[Minus]0.0558534,1.75166,\[Minus]5.99021},{\[Minus]0.0179453,0.59651,\[Minus]2.06089}},{{153.484,\[Minus]1417.06,2752.86},{138.741,\[Minus]1433.66,2857.74},{41.0251,\[Minus]423.681,850.359}},{{\[Minus]12.4154,55.4716,\[Minus]106.051},{\[Minus]11.953,76.807,\[Minus]155.332},{\[Minus]3.41293,25.5724,\[Minus]54.408}},{{\[Minus]1.83707*10^-5,2.18863 10^-5,8.25024 10^-5},{7.15737 10^-6,\[Minus]5.578*10^-5,1.91421 10^-4},{5.44717 10^-6,\[Minus]3.22061*10^-5,7.97402 10^-5}},
{{0.00630802,\[Minus]0.120741,0.262716},{0.00341518,\[Minus]0.107793,0.27099},{0.000737419,\[Minus]0.0274962,0.0733151}},{{0.283949,\[Minus]4.04975,13.2078},{0.103963,\[Minus]7.02506,24.7849},{0.030932,\[Minus]2.6924,9.60937}},{{\[Minus]0.0678292,\[Minus]0.668983,3.40315},{\[Minus]0.0529658,\[Minus]0.9923794,4.82068},{\[Minus]0.00613414,\[Minus]0.384293,1.75618}},{{1.16102,\[Minus]2.36278,6.77104},{0.757078,\[Minus]2.72569,7.11404},{0.176693,\[Minus]0.797869,2.11624}}};

Return[Table[\[Lambda]00[[k]]+\[Lambda]10[[k]]\[Eta]+Sum[(\[Chi]-1)^j \[Eta]^i \[Lambda][[k]][[j,i+1]],{i,0,2},{j,1,3}],{k,1,19}]];

]


(* ::Subsubsection::Closed:: *)
(*PhenomD amplitude*)


ampD[F_,m1_,m2_,\[Chi]1_,\[Chi]2_]:=Block[{msun=1.4768,cc=299792,pc=3.085677 10^13,i,M,\[ScriptCapitalM],\[Eta],\[Delta],\[Chi]s,\[Chi]a,\[Chi]pn,\[Delta]0,\[Delta]1,\[Delta]2,\[Delta]3,\[Delta]4,
ampEI,ampLI,ampI,ampIN,ampMR,ampNetw,fRD,fdamp,\[Rho]1,\[Rho]2,\[Rho]3,\[Gamma]1,\[Gamma]2,\[Gamma]3,v2,f1,f2,f3,fpeak,\[Delta]A,\[Theta]p,\[Theta]m,x,x0,ampIMR,f,mf,\[Chi]f,M\[Omega]22,\[Tau]\[Omega]22},

M\[Omega]22=Interpolation[Import["n1l2m2.dat"][[All,{1,2}]],Method->"Spline"];
\[Tau]\[Omega]22=Interpolation[Import["n1l2m2.dat"][[All,{1,3}]],Method->"Spline"];

\[Theta]p[x_,x0_]:=1/2 (1+Piecewise[{{-1,x<x0},{1,x>=x0}}]);
\[Theta]m[x_,x0_]:=1/2 (1-Piecewise[{{-1,x<x0},{1,x>=x0}}]);

{M,\[ScriptCapitalM],\[Eta]}={(m1+m2)msun,(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun,m1 m2/(m1+m2)^2};
{\[Chi]s,\[Chi]a}={(\[Chi]1+\[Chi]2),(\[Chi]1-\[Chi]2)}/2;

\[Delta]=If[\[Eta]==1/4,0,Sqrt[1-4 \[Eta]]];
\[Chi]pn=1/(1-76/113 \[Eta]) (\[Chi]s+\[Delta] \[Chi]a-76/113 \[Eta] \[Chi]s);

{mf,\[Chi]f}=finalbh[m1,m2,\[Chi]1,\[Chi]2];

{\[Rho]1,\[Rho]2,\[Rho]3,\[Gamma]1,\[Gamma]2,\[Gamma]3,v2}=coeffD[\[Eta],\[Chi]pn][[{5,6,7,16,17,18,19}]];

(* early inpsiral *)
ampEI={1,0,451/168 \[Eta]-323/224,27/8 \[Delta] \[Chi]a+(27/8-11/6 \[Eta])\[Chi]s,105271/24192 \[Eta]^2-1975055/338688 \[Eta]-27312085/8128512+(-(81/32)+8\[Eta])\[Chi]a^2-81/16 \[Delta] \[Chi]a \[Chi]s+(-(81/32)+17/8 \[Eta])\[Chi]s^2,
(85\[Pi])/64 (4\[Eta]-1)+\[Delta](285197/16128-1579/4032 \[Eta])\[Chi]a+(285197/16128-15317/672 \[Eta]-2227/1008 \[Eta]^2)\[Chi]s,
34473079/6386688 \[Eta]^3-3248849057/178827264 \[Eta]^2+545384828789/5007163392 \[Eta]-205/48 \[Eta] \[Pi]^2-177520268561/8583708672+(1614569/64512-1873643/16128 \[Eta]+2167/42 \[Eta]^2)\[Chi]a^2+(31/12 \[Pi]-7/3 \[Pi] \[Eta])\[Chi]s+(1614569/64512-61391/1344 \[Eta]+57451/4032 \[Eta]^2)\[Chi]s^2+\[Delta] \[Chi]a(31/12 \[Pi]+(1614569/32256-165961/2688 \[Eta])\[Chi]s)};

(* late inpsiral *)
ampLI=\[Rho]1 f^(7/3)+\[Rho]2 f^(8/3)+\[Rho]3 f^(9/3);

(* total inpsiral *)
ampI=(Sum[ampEI[[i]](\[Pi] f)^((i-1)/3),{i,1,7}]+ampLI);

(* intermediate *)
ampIN=(\[Delta]0+\[Delta]1 f+\[Delta]2 f^2+\[Delta]3 f^3+\[Delta]4 f^4);

(* merger-ringdown *)
ampMR=\[Gamma]1 (\[Gamma]3 fdamp)/((f-fRD)^2+(\[Gamma]3 fdamp)^2) Exp[-0 (\[Gamma]2(f-fRD))/(\[Gamma]3 fdamp)];

ampNetw= (\[ScriptCapitalM] msun)^(5/6)/(10^6 pc \[Pi]^(2/3) );

{fRD,fdamp,fpeak}={M\[Omega]22[\[Chi]f]/(2\[Pi]),-1/(\[Tau]\[Omega]22[\[Chi]f]2 \[Pi]),Abs[fRD+0fdamp \[Gamma]3/\[Gamma]2 (Sqrt[1-\[Gamma]2^2]-1)]};

{f1,f2,f3}={0.014,(f1+f3)/2,fpeak};

\[Delta]A=NSolve[{(ampIN-ampI//.f->f1)==0,(ampIN-v2 //.f->f2)==0,(ampIN-ampMR //.f->f3)==0,
(D[ampIN,f]-D[ampI,f]//.f->f1)==0,(D[ampIN,f]-D[ampMR,f]//.f->f3)==0},{\[Delta]0,\[Delta]1,\[Delta]2,\[Delta]3,\[Delta]4}][[1]];

ampIMR=(ampI \[Theta]m[f,f1]+\[Theta]p[f,f1]\[Theta]m[f,fpeak](ampIN//.\[Delta]A)+\[Theta]p[f,fpeak]ampMR//.f->F M/cc);

Return[ampIMR];

]


(* ::Subsubsection::Closed:: *)
(*PhenomD phase*)


phaseD[F_,m1_,m2_,\[Chi]1_,\[Chi]2_]:=Block[{msun=1.4768,\[Gamma]E=0.57721,cc=299792,\[Eta],\[Chi]s,\[Chi]a,\[Delta],f,M,\[ScriptCapitalM],tc=0,\[Phi]c=0,\[Chi]pn,phaseEI,phaseI,phaseIN,i,\[Sigma]1,\[Sigma]2,\[Sigma]3,\[Sigma]4,\[Alpha]0,\[Alpha]1,\[Alpha]2,\[Alpha]3,\[Alpha]4,\[Alpha]5,\[Beta]0,\[Beta]1,\[Beta]2,\[Beta]3,fRD,fdamp,phaseMR,f1,f2,\[Delta]phaseI,\[Delta]phaseMR,phaseIMR,M\[Omega]22,\[Tau]\[Omega]22,\[Theta]p,\[Theta]m,x,x0,mf,\[Chi]f},

M\[Omega]22=Interpolation[Import["n1l2m2.dat"][[All,{1,2}]],Method->"Spline"];
\[Tau]\[Omega]22=Interpolation[Import["n1l2m2.dat"][[All,{1,3}]],Method->"Spline"];

\[Theta]p[x_,x0_]:=1/2 (1+Piecewise[{{-1,x<x0},{1,x>=x0}}]);
\[Theta]m[x_,x0_]:=1/2 (1-Piecewise[{{-1,x<x0},{1,x>=x0}}]);

{M,\[ScriptCapitalM],\[Eta]}={(m1+m2)msun,(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun,m1 m2/(m1+m2)^2};
{\[Chi]s,\[Chi]a}={(\[Chi]1+\[Chi]2),(\[Chi]1-\[Chi]2)}/2;

\[Delta]=If[\[Eta]==1/4,0,Sqrt[1-4 \[Eta]]];

\[Chi]pn=1/(1-76/113 \[Eta]) (\[Chi]s+\[Delta] \[Chi]a-76/113 \[Eta] \[Chi]s);

{mf,\[Chi]f}=finalbh[m1,m2,\[Chi]1,\[Chi]2];

{\[Sigma]1,\[Sigma]2,\[Sigma]3,\[Sigma]4,\[Alpha]2,\[Alpha]3,\[Alpha]4,\[Alpha]5,\[Beta]2,\[Beta]3}=coeffD[\[Eta],\[Chi]pn][[{1,2,3,4,9,10,11,12,14,15}]];

(* early inspiral *)
phaseEI= {1,0,3715/756+55/9 \[Eta],-16\[Pi]+113 /3 \[Delta] \[Chi]a+(113/3-76/3 \[Eta])\[Chi]s,15293365/508032+27145/504 \[Eta]+3085/72 \[Eta]^2
+(-(405/8)+200\[Eta])\[Chi]a^2-405/4 \[Chi]a \[Chi]s \[Delta]+(-(405/8)+5/2 \[Eta])\[Chi]s^2,(1+Log[\[Pi] f])(38645/756\[Pi]-65/9 \[Eta] \[Pi]+\[Delta](-(732985/2268)-140/9 \[Eta])\[Chi]a+(-(732985/2268)+24260/81 \[Eta]+340/9 \[Eta]^2)\[Chi]s),
11583231236531/4694215680-(640\[Pi]^2)/3-6848/21 \[Gamma]E-6848/63 Log[64 \[Pi] f]+(2255/12 \[Pi]^2-15737765635/3048192)\[Eta]+76055/1728 \[Eta]^2-127825/1296 \[Eta]^3+2270/3 \[Pi] \[Delta] \[Chi]a+(2270/3 \[Pi]-520\[Pi] \[Eta])\[Chi]s,(77096675/254016+378515/1512 \[Eta]-74045/756 \[Eta]^2)\[Pi]+\[Delta](-(25150083775/3048192)+26804935/6048 \[Eta]-1985/48 \[Eta]^2)\[Chi]a+(-(25150083775/3048192)+10566655595/762048 \[Eta]-1042165/3024 \[Eta]^2+5345/36 \[Eta]^3)\[Chi]s};

(* total inspiral *)
phaseI=2\[Pi] f tc-\[Phi]c-\[Pi]/4+3/(128\[Eta] (\[Pi] f)^(5/3)) Sum[phaseEI[[i]](\[Pi] f)^((i-1)/3),{i,1,8}]+1/\[Eta] (3/4 \[Sigma]2 f^(4/3)+3/5 \[Sigma]3 f^(5/3)+1/2 \[Sigma]4 f^2);

(* intermediate *)
phaseIN=\[Beta]0+\[Beta]1 f+\[Beta]2 Log[f]-\[Beta]3/3 f^-3;

(* merger ringdown *)
phaseMR=1/\[Eta] (\[Alpha]0+\[Alpha]1 f-\[Alpha]2 f^-1+4/3 \[Alpha]3 f^(3/4)+\[Alpha]4 ArcTan[(f-\[Alpha]5 fRD)/fdamp]);

{fRD,fdamp}={M\[Omega]22[\[Chi]f]/(2\[Pi]),-1/(2\[Pi] \[Tau]\[Omega]22[\[Chi]f])};
Print[{fRD,fdamp}];
{f1,f2}={0.018,0.5fRD};

\[Delta]phaseI=NSolve[{(phaseI-phaseIN//.f->f1)==0,(D[phaseI,f]-D[phaseIN,f])//.f->f1},{\[Beta]0,\[Beta]1}][[1]];

\[Delta]phaseMR=NSolve[{(phaseIN-phaseMR//.Join[\[Delta]phaseI,{f->f2}])==0,(D[phaseIN,f]-D[phaseMR,f]//.Join[\[Delta]phaseI,{f->f2}])==0},{\[Alpha]0,\[Alpha]1}][[1]];

phaseIMR=phaseI \[Theta]m[f,f1]+\[Theta]p[f,f1]\[Theta]m[f,f2](phaseIN//.\[Delta]phaseI)+\[Theta]p[f,f2](phaseMR//.\[Delta]phaseMR)//.f->F M/cc;

Return[phaseIMR]

]


(* ::Subsubsection::Closed:: *)
(*Full PhenomD waveform*)


phenomD[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_]:=Block[{msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],ampNetw},

\[ScriptCapitalM]=(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun;

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d) (f/cc)^(-7/6);

Return[ampNetw ampD[f,m1,m2,\[Chi]1,\[Chi]2]Exp[I phaseD[f,m1,m2,\[Chi]1,\[Chi]2]]]

]


(* ::Subsection:: *)
(*PhenomB*)


(* ::Subsubsection::Closed:: *)
(*PhenomB phase*)


(* Note that frequency is in Hz while masses are in km *)


phaseB[F_,\[ScriptCapitalM]_,\[Eta]_,\[Chi]_,tc_,\[Phi]c_]:=Block[{eps=10^-30,f,M,msun=1.4768,cc=299792,\[Psi]0,phasePN,phaseHYB,phaseIMR,k,i,l,\[ScriptX]},

\[ScriptX]={{{-920.9,492.1,135},{6742,-1053,0},{-1.34*10^4,0,0}},
{{1.702 10^4,-9566,-2182},{-1.214*10^5,2.075 10^4,0},{2.386 10^5,0,0}},
{{-1.254*10^5,7.507 10^4,1.338 10^4},{8.735 10^5,-1.657*10^5,0},{-1.694*10^6,0,0}},
{{0,0,0},{0,0,0},{0,0,0}},{{-8.898*10^5,6.31 10^5,5.068 10^4},{5.981 10^6,-1.415*10^6,0},{-1.128*10^7,0,0}},{{8.696*10^5,-6.71*10^5,-3.008*10^4},{-5.838*10^6,1.514*10^6,0},{1.089*10^7,0,0}}};

M = \[ScriptCapitalM] \[Eta]^(-3/5);

phasePN={3715/756,-16\[Pi]+113/3 \[Chi],15293365/508032-405/8 \[Chi]^2,0,0,0};

phaseHYB[k_]:=phasePN[[k]]+Sum[\[ScriptX][[k,i,j+1]]\[Eta]^i Re[\[Chi]^(j+eps)],{i,1,3},{j,0,Min[3-i,2]}];

phaseIMR=2\[Pi] f tc+\[Phi]c+3/(128 \[Eta] (\[Pi] M f)^(5/3)) (1+Sum[(\[Pi] M f)^(l/3) phaseHYB[l-1],{l,2,7}])//.f->F/cc;

Return[phaseIMR]

]


(* ::Subsubsection::Closed:: *)
(*PhenomB amplitude*)


(* Note that frequency is in Hz while masses are in km *)


ampB[F_,\[ScriptCapitalM]_,\[Eta]_,\[Chi]_]:=Block[{eps=10^-30,f,M,msun=1.4768,cc=299792,\[ScriptY],\[Mu]0,i,j,\[Alpha],\[Epsilon],\[Mu],\[ScriptF]1,\[ScriptF]2,\[ScriptF]3,\[Sigma]Lor,ampI,ampM,ampR,wm,wr,ampIMR},

\[ScriptY]={{{0.6437,0.827,-0.2706},{-0.05822,-3.935,0},{-7.092,0,0}},{{0.1469,-0.1228,-0.02609},
{-0.0249,0.1701,0},{2.325,0,0}},{{-0.4098,-0.03523,0.1008},{1.829,-0.02017,0},
{-2.87,0,0}},{{-0.1331,-0.08172,0.1451},{-0.2714,0.1279,0},{4.922,0,0}}};

M = \[ScriptCapitalM] \[Eta]^(-3/5);

\[Mu]0={1-4.455(1-\[Chi])^0.217+3.521(1-\[Chi])^0.26,(1-0.63(1-\[Chi])^0.3)/2,((1-0.63(1-\[Chi])^0.3) (1-\[Chi])^0.45)/4,0.3236+0.04894\[Chi]+0.01346\[Chi]^2};

\[Alpha]={-(323/224)+451/168 \[Eta],(27/8-11/6 \[Eta])\[Chi]}; (* \[Alpha]2 and \[Alpha]3 *)
\[Epsilon]={1.4547\[Chi]-1.8897,-1.8153\[Chi]+1.6557}; (* \[Epsilon]1 and \[Epsilon]2 *)

\[Mu][k_]:=\[Mu]0[[k]]+Sum[\[ScriptY][[k,i,j+1]]\[Eta]^i Re[\[Chi]^(j+eps)],{i,1,3},{j,0,Min[3-i,2]}];

{\[ScriptF]1,\[ScriptF]2,\[ScriptF]3,\[Sigma]Lor}={\[Mu][1],\[Mu][2],\[Mu][4],\[Mu][3]}/(M \[Pi]);

ampI=(f/\[ScriptF]1)^(-7/6) (1+Sum[\[Alpha][[i-1]](\[Pi] M f)^(i/3),{i,2,3}]);

ampM=(f/\[ScriptF]1)^(-2/3) (1+Sum[\[Epsilon][[i]](\[Pi] M f)^(i/3),{i,1,2}]);

ampR=1/(2\[Pi]) \[Sigma]Lor/((f-\[ScriptF]2)^2+\[Sigma]Lor^2/4);

wm=ampI/ampM//.f->\[ScriptF]1;

wr=wm ampM/ampR//.f->\[ScriptF]2;

ampIMR = \[ScriptF]1^(-7/6) Which[f<\[ScriptF]1,ampI,f>=\[ScriptF]1 && f<\[ScriptF]2, wm*ampM,f>=\[ScriptF]2 && f<\[ScriptF]3, wr*ampR,f>=\[ScriptF]3,0]//.f->F/cc;

Return[ampIMR]

]


(* ::Subsubsection::Closed:: *)
(*Full PhenomB waveform*)


(* Note that frequency is in Hz while masses are in units of solar masses *)


phenomB[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[Tau]_,\[Phi]c_,mode_]:=Block[{tc,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],\[Chi],ampNetw,amplitude},

{\[ScriptCapitalM],\[Eta],\[Chi],tc}={(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun,m1 m2/(m1+m2)^2,(m1 \[Chi]1+m2 \[Chi]2)/(m1+m2),\[Tau] cc};

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d);

Which[mode=="N",amplitude = ampNetw (f/cc)^(-7/6),mode=="PN",amplitude=ampNetw ampB[f,\[ScriptCapitalM],\[Eta],\[Chi]]];

Return[amplitude Exp[-I phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c]]]

]


(* ::Subsubsection::Closed:: *)
(*Derivative of the PhenomB template*)


dphenomB[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[Tau]_,\[Phi]_,mode_]:=Block[{tc,\[Phi]c,dh,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],\[Chi],ampNetw,amplitude},

dh={\[ScriptCapitalM] D[phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c],\[ScriptCapitalM]],\[Eta] D[phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c],\[Eta]],
	D[phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c],\[Chi]],D[phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c],tc],D[phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c],\[Phi]c]};

{\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c}={(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun,m1 m2/(m1+m2)^2,(m1 \[Chi]1+m2 \[Chi]2)/(m1+m2),\[Tau] cc,\[Phi]};

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d);

Which[mode=="N",amplitude = ampNetw (f/cc)^(-7/6),mode=="PN",amplitude=ampNetw ampB[f,\[ScriptCapitalM],\[Eta],\[Chi]]];

Return[-amplitude I dh]

]


(* ::Subsubsection::Closed:: *)
(*ppE phase*)


phaseppE[f_,\[ScriptCapitalM]_,\[Eta]_,\[Beta]_,b_]:=Block[{M,msun=1.4768,cc=299792,ppE},

M = \[ScriptCapitalM] \[Eta]^(-3/5);

ppE = 3/(128 (\[Pi] \[ScriptCapitalM] f/cc)^(5/3)) \[Beta] (\[Pi] M f/cc)^(b/3);

Return[ppE]

]


(* ::Subsubsection::Closed:: *)
(*Full PhenomBppE waveform*)


phenomBppE[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[Tau]_,\[Phi]c_,\[Beta]_,b_,mode_]:=Block[{tc,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],\[Chi],ampNetw,amplitude},

{\[ScriptCapitalM],\[Eta],\[Chi],tc}={(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun,m1 m2/(m1+m2)^2,(m1 \[Chi]1+m2 \[Chi]2)/(m1+m2),\[Tau] cc};

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d);

Which[mode=="N",amplitude = ampNetw (f/cc)^(-7/6),mode=="PN",amplitude=ampNetw ampB[f,\[ScriptCapitalM],\[Eta],\[Chi]]];

Return[amplitude Exp[-I (phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c]+phaseppE[f,\[ScriptCapitalM],\[Eta],\[Beta],b])]]

]


(* ::Subsubsection::Closed:: *)
(*Derivative of the PhenomBppE template*)


dphenomBppE[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[Tau]_,\[Phi]_,ppE\[Beta]_,b_,mode_]:=Block[{\[Beta],phasetot,tc,\[Phi]c,dh,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],\[Chi],ampNetw,amplitude},

phasetot = phaseB[f,\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c]+phaseppE[f,\[ScriptCapitalM],\[Eta],\[Beta],b];

dh={\[ScriptCapitalM] D[phasetot,\[ScriptCapitalM]],\[Eta] D[phasetot,\[Eta]],D[phasetot,\[Chi]],
		D[phasetot,tc],D[phasetot,\[Phi]c],D[phasetot,\[Beta]]};

{\[ScriptCapitalM],\[Eta],\[Chi],tc,\[Phi]c,\[Beta]}={(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun,m1 m2/(m1+m2)^2,(m1 \[Chi]1+m2 \[Chi]2)/(m1+m2),\[Tau] cc,\[Phi],ppE\[Beta]};

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d);

Which[mode=="N",amplitude = ampNetw (f/cc)^(-7/6),mode=="PN",amplitude = ampNetw ampB[f,\[ScriptCapitalM],\[Eta],\[Chi]]];

Return[-amplitude I dh]

]


(* ::Subsection::Closed:: *)
(*Taylor F2*)


(* ::Subsubsection::Closed:: *)
(*Taylor F2 phase*)


(* Note that frequency is in Hz while masses are in km *)


phaseF2[F_,\[ScriptCapitalM]_,\[Eta]_,\[Chi]s_,\[Chi]a_,\[Lambda]_,tc_,\[Phi]c_]:=Block[{\[Gamma]E=0.57721,cc=299792,\[Delta],f,M,coeffT2,phaseI,phaseT,i},

M = \[ScriptCapitalM] \[Eta]^(-3/5);

\[Delta]=If[\[Eta]==1/4,0,Sqrt[1-4 \[Eta]]];

coeffT2 = {1,0,3715/756+55/9 \[Eta],-16\[Pi]+113 /3 \[Delta] \[Chi]a+(113/3-76/3 \[Eta])\[Chi]s,15293365/508032+27145/504 \[Eta]+3085/72 \[Eta]^2
+(-(405/8)+200\[Eta])\[Chi]a^2-405/4 \[Chi]a \[Chi]s \[Delta]+(-(405/8)+5/2 \[Eta])\[Chi]s^2,(1+Log[M \[Pi] f])(38645/756\[Pi]-65/9 \[Eta] \[Pi]+\[Delta](-(732985/2268)-140/9 \[Eta])\[Chi]a+(-(732985/2268)+24260/81 \[Eta]
+340/9 \[Eta]^2)\[Chi]s),11583231236531/4694215680-(640\[Pi]^2)/3-6848/21 \[Gamma]E-6848/63 Log[64 M \[Pi] f]+(2255/12 \[Pi]^2-15737765635/3048192)\[Eta]
+76055/1728 \[Eta]^2-127825/1296 \[Eta]^3+2270/3 \[Pi] \[Delta] \[Chi]a+(2270/3 \[Pi]-520\[Pi] \[Eta])\[Chi]s,(77096675/254016+378515/1512 \[Eta]-74045/756 \[Eta]^2)\[Pi]
+\[Delta](-(25150083775/3048192)+26804935/6048 \[Eta]-1985/48 \[Eta]^2)\[Chi]a+(-(25150083775/3048192)+10566655595/762048 \[Eta]-1042165/3024 \[Eta]^2+5345/36 \[Eta]^3)\[Chi]s};

(* point - particle inspiral *)
phaseI = 2\[Pi] f tc-\[Phi]c-\[Pi]/4+3/(128\[Eta] (M \[Pi] f)^(5/3)) Sum[coeffT2[[i]](M \[Pi] f)^((i-1)/3),{i,1,8}];

(* tidal inspiral *)
phaseT = -(\[Lambda] 117)/(8 \[Eta] M^5)*(M \[Pi] f)^(5/3)(1+3115/1248 (M \[Pi] f)^(2/3));(*+3/128/\[Eta](6595/364*Sqrt[1-4\[Eta]]\[Delta]\[Lambda] 0)(M \[Pi] f)^(7/3)^(7/2)*);
				(*-\[Pi] (M \[Pi] f)+(23073805/3302208+20/81 6)(M \[Pi] f)^(4/3)- 4283/1092 \[Pi] (M \[Pi] f)^(5/3)*)

Return[phaseI+phaseT//.f -> F/cc]

]


(* ::Subsubsection::Closed:: *)
(*Taylor F2 amplitude*)


(* Note that frequency is in Hz while masses are in km *)


ampF2[f_,\[ScriptCapitalM]_,\[Eta]_,\[Chi]s_,\[Chi]a_]:=Block[{cc=299792,pc=3.085677 10^13,i,M,\[Delta],coeffampI,ampI},

M = \[ScriptCapitalM] \[Eta]^(-3/5);

\[Delta]=If[\[Eta]==1/4,0,Sqrt[1-4 \[Eta]]];

coeffampI={1,0,451/168 \[Eta]-323/224,27/8 \[Delta] \[Chi]a+(27/8-11/6 \[Eta])\[Chi]s,105271/24192 \[Eta]^2-1975055/338688 \[Eta]-27312085/8128512
+(-(81/32)+8\[Eta])\[Chi]a^2-81/16 \[Delta] \[Chi]a \[Chi]s+(-(81/32)+17/8 \[Eta])\[Chi]s^2,(85\[Pi])/64 (4\[Eta]-1)+\[Delta](285197/16128-1579/4032 \[Eta])\[Chi]a
+(285197/16128-15317/672 \[Eta]-2227/1008 \[Eta]^2)\[Chi]s,34473079/6386688 \[Eta]^3-3248849057/178827264 \[Eta]^2+545384828789/5007163392 \[Eta]
-205/48 \[Eta] \[Pi]^2-177520268561/8583708672+(1614569/64512-1873643/16128 \[Eta]+2167/42 \[Eta]^2)\[Chi]a^2+(31/12 \[Pi]-7/3 \[Pi] \[Eta])\[Chi]s
+(1614569/64512-61391/1344 \[Eta]+57451/4032 \[Eta]^2)\[Chi]s^2+\[Delta] \[Chi]a(31/12 \[Pi]+(1614569/32256-165961/2688 \[Eta])\[Chi]s)};

ampI = Sum[coeffampI[[i]](M \[Pi] f/cc)^((i-1)/3),{i,1,7}];

Return[ampI];

]


(* ::Subsubsection::Closed:: *)
(*Taylor F2 template*)


(* Note that frequency is in Hz while masses are in units of solar masses *)


TaylorF2[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[Lambda]_,\[Tau]_,\[Phi]c_,mode_]:=Block[{tc,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],ampNetw,\[Chi]s,\[Chi]a,amplitude},

{\[ScriptCapitalM],\[Eta],tc} = {(m1 m2/(m1+m2)^2)^(3/5) (m1+m2) msun,m1 m2/(m1+m2)^2,\[Tau] cc};

{\[Chi]s,\[Chi]a}={(\[Chi]1+\[Chi]2),(\[Chi]1-\[Chi]2)}/2;

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d) (f/cc)^(-7/6);

Which[mode=="N",amplitude = ampNetw,mode=="PN",amplitude=ampNetw ampF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a]];

Return[amplitude Exp[-I phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c]]]

]


(* ::Subsubsection::Closed:: *)
(*Taylor F2 ppE*)


TaylorF2ppE[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[Tau]_,\[Phi]c_,\[Beta]_,b_,mode_]:=Block[{tc,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],ampNetw,\[Chi]s,\[Chi]a,amplitude},

{\[ScriptCapitalM],\[Eta],tc} = {(m1 m2/(m1+m2)^2)^(3/5) (m1+m2)msun,m1 m2/(m1+m2)^2,\[Tau] cc};

{\[Chi]s,\[Chi]a}={(\[Chi]1+\[Chi]2),(\[Chi]1-\[Chi]2)}/2;

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d) (f/cc)^(-7/6);

Which[mode=="N",amplitude = ampNetw,mode=="PN",amplitude=ampNetw ampF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a]];

Return[amplitude Exp[-I (phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,0,tc,\[Phi]c]+phaseppE[f,\[ScriptCapitalM],\[Eta],\[Beta],b])]]

]


(* ::Subsubsection::Closed:: *)
(*Derivative of the Taylor F2 template*)


dTaylorF2[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[CapitalLambda]_,\[Tau]_,\[Phi]_,mode_]:=Block[{\[Lambda],tc,\[Phi]c,dh,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],ampNetw,\[Chi]s,\[Chi]a,amplitude},

dh={\[ScriptCapitalM] D[phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c],\[ScriptCapitalM]],\[Eta] D[phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c],\[Eta]],
	D[phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c],\[Chi]s],D[phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c],\[Chi]a],
	D[phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c],\[Lambda]],D[phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c],tc],D[phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,\[Lambda],tc,\[Phi]c],\[Phi]c]};
	
{\[ScriptCapitalM],\[Eta],tc,\[Phi]c,\[Lambda]} = {(m1 m2/(m1+m2)^2)^(3/5) (m1+m2) msun,m1 m2/(m1+m2)^2,\[Tau] cc,\[Phi],\[CapitalLambda]};
{\[Chi]s,\[Chi]a}={(\[Chi]1+\[Chi]2),(\[Chi]1-\[Chi]2)}/2;

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d) (f/cc)^(-7/6);

Which[mode=="N",amplitude = ampNetw,mode=="PN",amplitude=ampNetw ampF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a]];

Return[-I dh amplitude]

]


(* ::Subsubsection::Closed:: *)
(*Derivative of the TaylorF2ppE template*)


dTaylorF2ppE[f_,m1_,m2_,\[Chi]1_,\[Chi]2_,d_,\[Tau]_,\[Phi]_,ppE\[Beta]_,b_,mode_]:=Block[{\[Chi]s,\[Chi]a,\[Beta],phasetot,tc,\[Phi]c,dh,msun=1.4768,cc=299792,pc=3.085677 10^13,\[ScriptCapitalM],\[Eta],\[Chi],ampNetw,amplitude},

phasetot = phaseF2[f,\[ScriptCapitalM],\[Eta],\[Chi]s,\[Chi]a,0,tc,\[Phi]c]+phaseppE[f,\[ScriptCapitalM],\[Eta],\[Beta],b];

dh={\[ScriptCapitalM] D[phasetot,\[ScriptCapitalM]],\[Eta] D[phasetot,\[Eta]],D[phasetot,\[Chi]s],D[phasetot,\[Chi]a],
	D[phasetot,tc],D[phasetot,\[Phi]c],D[phasetot,\[Beta]]};

{\[ScriptCapitalM],\[Eta],tc,\[Phi]c,\[Beta]}={(m1 m2/(m1+m2)^2)^(3/5) (m1+m2) msun,m1 m2/(m1+m2)^2,\[Tau] cc,\[Phi],ppE\[Beta]};

{\[Chi]s,\[Chi]a}={(\[Chi]1+\[Chi]2),(\[Chi]1-\[Chi]2)}/2;

ampNetw = 2/5 (5/24)^(1/2) \[ScriptCapitalM]^(5/6)/(10^6 pc \[Pi]^(2/3) d);

Which[mode=="N",amplitude = ampNetw (f/cc)^(-7/6),mode=="PN",amplitude=ampNetw ampB[f,\[ScriptCapitalM],\[Eta],\[Chi]]];

Return[-amplitude I dh]

]


(* ::Subsection::Closed:: *)
(*End*)


End[];

EndPackage[];
