{((-I)*\[Omega]*H1[r]*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][\[CurlyPhi]][r]))/(r^2*A[r]) + 
  (Derivative[1][H0][r]*(-2*r + E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][\[CurlyPhi]][r]))/(2*r^2) + 
  (H2[r]*(-2 + (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]*
       Derivative[1][\[CurlyPhi]][r])/A[r]))/(2*r^2*B[r]) + 
  (K[r]*(2*r^2*\[Omega]^2 + 2*A[r] - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (2*r*\[Omega]^2 + Derivative[1][A][r])*Derivative[1][\[CurlyPhi]][r]))/
   (2*r^2*A[r]*B[r]) + (Derivative[1][K][r]*
    (2 + (Derivative[1][A][r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]))/A[r]))/(2*r) + 
  (\[ScriptL]*(1 + \[ScriptL])*((E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
       Derivative[1][A][r])/(r^4*A[r]) + 
     (H0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
          r]))/(r^3*B[r]) + 
     (K[r]*(-2/B[r] + (E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
          Derivative[1][\[CurlyPhi]][r])/A[r]))/(2*r^2)))/2 + 
  (\[CurlyPhi]1[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*\[Omega]^2 - 
     3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]*
      (-1 + r*Derivative[1][\[CurlyPhi]][r]) + 
     B[r]*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
        (-1 + r*Derivative[1][\[CurlyPhi]][r]) + 
       r*(-2*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2 + 
         r*A[r]*Derivative[1][\[CurlyPhi]][r]))))/(2*r^4*A[r]*B[r]) - 
  (((E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*Derivative[1][A][r])/A[r] + 
     r^2*Derivative[1][\[CurlyPhi]][r])*Derivative[1][\[CurlyPhi]1][r])/
   (2*r^3) + 
  a^2*((H0[r]*(4*r*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][r] + 
       6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[1][hi[2][2]][r] + 
       (r^4*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
            r])*Derivative[1][wi[1][1]][r]^2)/A[r] + (12*r*hi[2][2][r])/
        B[r] - 2*(r*(2*r + E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
         6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          hi[2][2][r])))/(4*r^3) + 
    (H2[r]*(2*A[r]*(3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
          Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][hi[2][0]][r] - 
           Derivative[1][hi[2][2]][r]) + 6*hi[2][2][r] + 
         4*(ki[2][0][r] + ki[2][2][r])) + 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]^2 + 
         Derivative[1][A][r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][
               r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
             \[CurlyPhi]i[2][2][r])))))/(4*r^2*A[r]*B[r]) + 
    (Derivative[1][H0][r]*(-4*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][0]][r] + 
       2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
           r])*Derivative[1][ki[2][2]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]i[2][0]][r] + 6*E^\[CurlyPhi][r]*r*\[Alpha]*
        B[r]*Derivative[1][\[CurlyPhi]i[2][0]][r] + E^\[CurlyPhi][r]*r*
        \[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
       3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][2]][
         r] + 8*r^2*hi[2][0][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*hi[2][0][r] - 12*E^\[CurlyPhi][r]*r*
        \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*hi[2][0][r] - 
       4*r^2*hi[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 6*E^\[CurlyPhi][r]*r*
        \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 
       4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
        ki[2][0][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] - 12*E^\[CurlyPhi][r]*r*
        \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
       6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        pi[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
       6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        \[CurlyPhi]i[2][0][r] + 6*E^\[CurlyPhi][r]*\[Alpha]*
        \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] - 
       3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        \[CurlyPhi]i[2][2][r]))/(4*r^3) - 
    ((I/2)*\[Omega]*H1[r]*(4*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][0]][r] - 
       2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
           r])*Derivative[1][ki[2][2]][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]i[2][0]][r] - 6*E^\[CurlyPhi][r]*r*\[Alpha]*
        B[r]*Derivative[1][\[CurlyPhi]i[2][0]][r] - E^\[CurlyPhi][r]*r*
        \[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
       3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][2]][
         r] - 8*r^2*hi[2][0][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*hi[2][0][r] + 12*E^\[CurlyPhi][r]*r*
        \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*hi[2][0][r] + 
       4*r^2*hi[2][2][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 6*E^\[CurlyPhi][r]*r*
        \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 
       4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
        ki[2][0][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] + 12*E^\[CurlyPhi][r]*r*
        \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] - 
       6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        pi[2][2][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] - 
       6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        \[CurlyPhi]i[2][0][r] - 6*E^\[CurlyPhi][r]*\[Alpha]*
        \[CurlyPhi]i[2][2][r] - E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
       3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        \[CurlyPhi]i[2][2][r]))/(r^3*A[r]) + 
    (Derivative[1][\[CurlyPhi]1][r]*
      (A[r]*(-4*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
          Derivative[1][hi[2][0]][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          (-1 + 3*B[r])*Derivative[1][hi[2][2]][r] - 
         2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
         r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 12*E^\[CurlyPhi][r]*
          \[Alpha]*hi[2][2][r]) - E^\[CurlyPhi][r]*r*\[Alpha]*
        (3*B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
            (4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][
               r] - 4*pi[2][0][r] + 2*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
             \[CurlyPhi]i[2][2][r])) + Derivative[1][A][r]*
          (4*ki[2][0][r] + 4*ki[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r]))))/(4*r^4*A[r]) + 
    (Derivative[1][K][r]*
      (4*A[r]*(2*r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][r] - 
         r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
             r])*Derivative[1][hi[2][2]][r] + 2*r^2*Derivative[1][ki[2][0]][
           r] - r^2*Derivative[1][ki[2][2]][r] - 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 4*r*ki[2][0][r] + 
         2*r*ki[2][2][r]) - 3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (r*Derivative[1][\[CurlyPhi]][r]*(r^2*Derivative[1][wi[1][1]][r]^2 - 
           4*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 4*wi[1][1][r]^2) + 
         Derivative[1][A][r]*(4*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(4*r*Derivative[1][ki[2][0]][r] - 
             2*r*Derivative[1][ki[2][2]][r] - 8*ki[2][0][r] + 4*ki[2][2][r] - 
             8*pi[2][0][r] + 4*pi[2][2][r] + 4*\[CurlyPhi]i[2][0][r] - 
             2*\[CurlyPhi]i[2][2][r]))) - 
       2*(2*r^2*wi[1][1][r]*(r^2*Derivative[1][wi[1][1]][r] + 
           (2*r + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r])*
            wi[1][1][r]) + Derivative[1][A][r]*(4*r^2*ki[2][0][r] - 
           2*r^2*ki[2][2][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][
             r]))))/(8*r^2*A[r]) - 
    (\[CurlyPhi]1[r]*(4*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
        (-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][r] - 
       2*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
        (-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] - 
       2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
       r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 12*E^\[CurlyPhi][r]*
        \[Alpha]*hi[2][2][r] + 12*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 
       (E^\[CurlyPhi][r]*r*\[Alpha]*(2*r*\[Omega]^2*(4*hi[2][0][r] - 
            2*hi[2][2][r] + 4*ki[2][0][r] + 4*ki[2][2][r] - 4*pi[2][0][r] + 
            2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][
             r]) + B[r]*(8*r^2*\[Omega]^2*Derivative[1][ki[2][0]][r] - 
            4*r^2*\[Omega]^2*Derivative[1][ki[2][2]][r] - 
            2*r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
            r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
            8*r*\[Omega]^2*hi[2][0][r] + 4*r*\[Omega]^2*hi[2][2][r] - 
            4*Derivative[1][A][r]*ki[2][0][r] + 4*r*Derivative[1][A][r]*
             Derivative[1][\[CurlyPhi]][r]*ki[2][0][r] - 
            4*Derivative[1][A][r]*ki[2][2][r] + 4*r*Derivative[1][A][r]*
             Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] + 4*r*\[Omega]^2*
             \[CurlyPhi]i[2][0][r] + 2*Derivative[1][A][r]*\[CurlyPhi]i[2][0][
              r] - 2*r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
             \[CurlyPhi]i[2][0][r] - 2*r*\[Omega]^2*\[CurlyPhi]i[2][2][r] + 
            5*Derivative[1][A][r]*\[CurlyPhi]i[2][2][r] + 
            r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
             \[CurlyPhi]i[2][2][r]) + 3*B[r]^2*
           (r^3*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
             Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
             (4*r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][
                 ki[2][0]][r] - 2*r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
               Derivative[1][ki[2][2]][r] + 2*r*Derivative[1][\[CurlyPhi]i[2][
                  0]][r] - r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
              4*pi[2][0][r] - 4*r*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] - 
              2*pi[2][2][r] + 2*r*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 
              2*\[CurlyPhi]i[2][0][r] + 2*r*Derivative[1][\[CurlyPhi]][r]*
               \[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r] - 
              r*Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]))))/
        (A[r]*B[r])))/(4*r^5) + (\[ScriptL]*(1 + \[ScriptL])*
      (H0[r]*((-2*hi[2][0][r] + hi[2][2][r] - 2*ki[2][0][r] + ki[2][2][r] + 
           2*pi[2][0][r] - pi[2][2][r])/(r^2*B[r]) - 
         (E^\[CurlyPhi][r]*\[Alpha]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
            Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
              r]*(2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][
                r] - 4*hi[2][0][r] + 2*hi[2][2][r] - 4*ki[2][0][r] + 
              2*ki[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][
               r])))/(2*r^3)) + (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
         (A[r]*(4*Derivative[1][hi[2][0]][r] - 2*Derivative[1][hi[2][2]][
              r]) + r^3*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
           (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
            4*ki[2][0][r] + 2*ki[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
            \[CurlyPhi]i[2][2][r])))/(2*r^4*A[r]) + 
       (K[r]*(2*A[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*
             Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][hi[2][0]][r] - 
              Derivative[1][hi[2][2]][r]) + 2*(4*ki[2][0][r] - 
              2*ki[2][2][r] - 2*pi[2][0][r] + pi[2][2][r])) + 
          4*r^2*wi[1][1][r]^2 + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           (-4*r*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]*
             (r*Derivative[1][wi[1][1]][r] + wi[1][1][r]) + 
            Derivative[1][A][r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
              Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][
                 \[CurlyPhi]][r]*(8*ki[2][0][r] - 4*ki[2][2][r] - 
                2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])))))/
        (4*r^2*A[r]*B[r])))/2 + 
    (K[r]*(-2*A[r]^2*(B[r]*(2*r^2*(2*Derivative[1][ki[2][0]][r] - 
             Derivative[1][ki[2][2]][r]) + E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][hi[2][0]][r] - 
             r*Derivative[1][hi[2][2]][r] - 6*hi[2][2][r])) + 
         2*r*(3*hi[2][2][r] + 4*ki[2][0][r] + 4*ki[2][2][r] - 2*pi[2][0][r] + 
           pi[2][2][r])) + 2*r^3*B[r]*Derivative[1][A][r]*
        (2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]^2 + 
       A[r]*(-8*r^3*\[Omega]^2*hi[2][0][r] + 4*r^3*\[Omega]^2*hi[2][2][r] - 
         8*r^3*\[Omega]^2*ki[2][0][r] + 4*r^3*\[Omega]^2*ki[2][2][r] + 
         8*r^3*\[Omega]^2*pi[2][0][r] - 4*r^3*\[Omega]^2*pi[2][2][r] + 
         4*r^3*wi[1][1][r]^2 + 3*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*
          Derivative[1][\[CurlyPhi]][r]*(Derivative[1][A][r]*
            (4*Derivative[1][ki[2][0]][r] - 2*Derivative[1][ki[2][2]][r]) + 
           (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])^2) - 
         6*E^\[CurlyPhi][r]*r*\[Alpha]*\[Omega]^2*\[CurlyPhi]i[2][2][r] + 
         B[r]*(-(r^2*(r^3*Derivative[1][wi[1][1]][r]^2 + 4*E^\[CurlyPhi][r]*
               \[Alpha]*\[Omega]^2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
              2*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*Derivative[1][
                 \[CurlyPhi]i[2][2]][r] + 8*r^2*Derivative[1][wi[1][1]][r]*
               wi[1][1][r] + 8*r*wi[1][1][r]^2 + 2*E^\[CurlyPhi][r]*\[Alpha]*
               Derivative[1][\[CurlyPhi]][r]*(2*r*\[Omega]^2*
                 Derivative[1][ki[2][0]][r] - r*\[Omega]^2*Derivative[1][
                   ki[2][2]][r] - 4*\[Omega]^2*hi[2][0][r] + 2*\[Omega]^2*
                 hi[2][2][r] - 4*\[Omega]^2*ki[2][0][r] + 2*\[Omega]^2*
                 ki[2][2][r] + 2*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
                4*wi[1][1][r]^2 + 2*\[Omega]^2*\[CurlyPhi]i[2][0][r] - 
                \[Omega]^2*\[CurlyPhi]i[2][2][r]))) + Derivative[1][A][r]*
            (-4*r^3*Derivative[1][ki[2][0]][r] + 2*r^3*Derivative[1][
                ki[2][2]][r] - E^\[CurlyPhi][r]*\[Alpha]*
              (2*r*Derivative[1][\[CurlyPhi]i[2][0]][r] - r*
                Derivative[1][\[CurlyPhi]i[2][2]][r] - 8*r*Derivative[1][
                  \[CurlyPhi]][r]*ki[2][0][r] - 8*r*Derivative[1][
                  \[CurlyPhi]][r]*ki[2][2][r] + 2*r*Derivative[1][
                  \[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] - 6*
                \[CurlyPhi]i[2][2][r] - r*Derivative[1][\[CurlyPhi]][r]*
                \[CurlyPhi]i[2][2][r]))))))/(4*r^3*A[r]^2*B[r])), 
 (2*a*\[ScriptL]*(1 + \[ScriptL])*h0[r]*
   (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
   wi[1][1][r])/(r^3*A[r]*B[r]), 
 a^2*(H0[r]*((3*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
         Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r])/(2*r^2) - 
     (r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
       Derivative[1][wi[1][1]][r]^2)/(4*A[r]) - 
     (9*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
       hi[2][2][r])/(r^3*B[r])) - (((3*I)/2)*\[Omega]*H1[r]*
     (2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
          r])*Derivative[1][ki[2][2]][r] - E^\[CurlyPhi][r]*r*\[Alpha]*
       (-1 + 3*B[r])*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
      4*r^2*hi[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
       Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 6*E^\[CurlyPhi][r]*r*
       \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 
      4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
       ki[2][2][r] + 6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 6*E^\[CurlyPhi][r]*
       \[Alpha]*\[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*
       Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] - 
      3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
       \[CurlyPhi]i[2][2][r]))/(r^3*A[r]) - 
   (3*Derivative[1][H0][r]*(2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][2]][r] - 
      E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
       Derivative[1][\[CurlyPhi]i[2][2]][r] - 4*r^2*hi[2][2][r] - 
      2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
       hi[2][2][r] + 6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 4*E^\[CurlyPhi][r]*r*
       \[Alpha]*Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] + 
      6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
       pi[2][2][r] + 6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] + 
      E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
       \[CurlyPhi]i[2][2][r] - 3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]))/(4*r^3) + 
   (H2[r]*(6*A[r]*(3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
         Derivative[1][\[CurlyPhi]][r]*Derivative[1][hi[2][2]][r] - 
        6*hi[2][2][r] - 4*ki[2][2][r]) - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
       (r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]^2 - 
        3*Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
          Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][ki[2][2]][r] - 
            4*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/(4*r^2*A[r]*B[r]) + 
   (3*Derivative[1][\[CurlyPhi]1][r]*
     (A[r]*(-2*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
         Derivative[1][hi[2][2]][r] - r^3*Derivative[1][\[CurlyPhi]i[2][2]][
          r] + 12*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r]) + 
      E^\[CurlyPhi][r]*r*\[Alpha]*(Derivative[1][A][r]*(4*ki[2][2][r] + 
          \[CurlyPhi]i[2][2][r]) + B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 - 
          3*Derivative[1][A][r]*(2*r*Derivative[1][ki[2][2]][r] - 
            2*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/(4*r^4*A[r]) + 
   (Derivative[1][K][r]*
     (12*A[r]*(r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
        r^2*Derivative[1][ki[2][2]][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 2*r*ki[2][2][r]) + 
      4*r^2*wi[1][1][r]*(r^2*Derivative[1][wi[1][1]][r] + 
        (2*r + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r])*
         wi[1][1][r]) + 2*Derivative[1][A][r]*(-6*r^2*ki[2][2][r] + 
        9*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
      3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]][r]*
         (r^2*Derivative[1][wi[1][1]][r]^2 - 4*r*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] - 4*wi[1][1][r]^2) - 6*Derivative[1][A][r]*
         (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
            r]*(r*Derivative[1][ki[2][2]][r] - 2*ki[2][2][r] - 
            2*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/(8*r^2*A[r]) + 
   (3*\[CurlyPhi]1[r]*(-2*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
       (-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
      r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] + 12*E^\[CurlyPhi][r]*\[Alpha]*
       (-1 + r*Derivative[1][\[CurlyPhi]][r])*hi[2][2][r] + 
      (E^\[CurlyPhi][r]*r*\[Alpha]*
        (B[r]^2*(r^3*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
            Derivative[1][wi[1][1]][r]^2 - 3*Derivative[1][A][r]*
            (2*r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][
                ki[2][2]][r] + r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
             (-1 + r*Derivative[1][\[CurlyPhi]][r])*(2*pi[2][2][r] - 
               \[CurlyPhi]i[2][2][r]))) + 2*r*\[Omega]^2*(-2*hi[2][2][r] + 
           4*ki[2][2][r] + 2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]) + 
         B[r]*(-4*r^2*\[Omega]^2*Derivative[1][ki[2][2]][r] + 
           2*r*\[Omega]^2*(2*hi[2][2][r] - \[CurlyPhi]i[2][2][r]) + 
           Derivative[1][A][r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             4*(-1 + r*Derivative[1][\[CurlyPhi]][r])*ki[2][2][r] + 
             (5 + r*Derivative[1][\[CurlyPhi]][r])*\[CurlyPhi]i[2][2][r]))))/
       (A[r]*B[r])))/(4*r^5) + (\[ScriptL]*(1 + \[ScriptL])*
     (-1/2*(E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
         (-6*A[r]*(r*B[r]*Derivative[1][hi[2][2]][r] - 2*hi[2][2][r]) + 
          r*B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 - 3*Derivative[1][A][r]*
             (r*Derivative[1][ki[2][2]][r] - 2*ki[2][2][r] + \[CurlyPhi]i[2][
                2][r]))))/(r^5*A[r]*B[r]) - 
      (3*H0[r]*(2*(r^2*hi[2][2][r] + r^2*ki[2][2][r] - r^2*pi[2][2][r] - 
           E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
         E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]i[2][2]][
            r] + Derivative[1][\[CurlyPhi]][r]*
            (r*Derivative[1][ki[2][2]][r] - 2*hi[2][2][r] - 2*ki[2][2][r] + 
             \[CurlyPhi]i[2][2][r]))))/(2*r^4*B[r]) + 
      (K[r]*(6*A[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][hi[2][2]][r] + 
           4*ki[2][2][r] - 2*pi[2][2][r]) - 4*r^2*wi[1][1][r]^2 + 
         E^\[CurlyPhi][r]*\[Alpha]*B[r]*(4*r*Derivative[1][\[CurlyPhi]][r]*
            wi[1][1][r]*(r*Derivative[1][wi[1][1]][r] + wi[1][1][r]) + 
           3*Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             Derivative[1][\[CurlyPhi]][r]*(-4*ki[2][2][r] + \[CurlyPhi]i[2][
                 2][r])))))/(4*r^2*A[r]*B[r])))/2 + 
   (K[r]*(-6*A[r]^2*(B[r]*(2*r^2*Derivative[1][ki[2][2]][r] + 
          E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
           (r*Derivative[1][hi[2][2]][r] + 6*hi[2][2][r])) - 
        2*r*(3*hi[2][2][r] + 4*ki[2][2][r] + pi[2][2][r])) - 
      2*r^3*B[r]*Derivative[1][A][r]*(2*r - E^\[CurlyPhi][r]*\[Alpha]*
         (-1 + 3*B[r])*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]^2 + 
      A[r]*(-3*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*
         Derivative[1][\[CurlyPhi]][r]*(-6*Derivative[1][A][r]*
           Derivative[1][ki[2][2]][r] + (r*Derivative[1][wi[1][1]][r] + 
            2*wi[1][1][r])^2) - 6*r*(2*r^2*\[Omega]^2*hi[2][2][r] + 
          2*r^2*\[Omega]^2*ki[2][2][r] - 2*r^2*\[Omega]^2*pi[2][2][r] + 
          2*r^2*wi[1][1][r]^2 - 3*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*
           \[CurlyPhi]i[2][2][r]) + 
        B[r]*(r^2*(r^3*Derivative[1][wi[1][1]][r]^2 - 6*E^\[CurlyPhi][r]*
             \[Alpha]*\[Omega]^2*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
            8*r^2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
            8*r*wi[1][1][r]^2 - 2*E^\[CurlyPhi][r]*\[Alpha]*
             Derivative[1][\[CurlyPhi]][r]*(3*r*\[Omega]^2*Derivative[1][
                 ki[2][2]][r] - 6*\[Omega]^2*hi[2][2][r] - 6*\[Omega]^2*
               ki[2][2][r] - 2*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
              8*wi[1][1][r]^2 + 3*\[Omega]^2*\[CurlyPhi]i[2][2][r])) - 
          3*Derivative[1][A][r]*(2*r^3*Derivative[1][ki[2][2]][r] + 
            E^\[CurlyPhi][r]*\[Alpha]*(r*Derivative[1][\[CurlyPhi]i[2][2]][
                r] + 6*\[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][
                r]*(8*ki[2][2][r] + \[CurlyPhi]i[2][2][r])))))))/
    (4*r^3*A[r]^2*B[r])), 
 a*((H1[r]*(r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
      2*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
         Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]))/(2*r^2*A[r]) - 
   (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
     (-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/
    (r^3*A[r]*B[r]) - ((I/2)*\[Omega]*K[r]*(-2*r*wi[1][1][r] + 
      E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
       (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(r*A[r]*B[r])), 
 a^2*(((3*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*(-2 + B[r])*h0[r]*
     \[CurlyPhi]i[2][2][r])/(r^4*A[r]*B[r]) - 
   (3*h1[r]*(4*Derivative[1][A][r]*(r^2*hi[2][2][r] - r^2*pi[2][2][r] + 
        E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
      2*A[r]*(4*r*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
        4*r^2*Derivative[1][ki[2][2]][r] + 4*E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 2*E^\[CurlyPhi][r]*
         \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 
        4*r*pi[2][2][r] + r^2*Derivative[1][\[CurlyPhi]][r]*
         \[CurlyPhi]i[2][2][r]) + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (r^4*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]^2 - 
        2*Derivative[1][A][r]*(2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
          \[CurlyPhi]i[2][2][r] + 2*r*Derivative[1][\[CurlyPhi]][r]*
           (r*Derivative[1][ki[2][2]][r] + hi[2][2][r] - 3*pi[2][2][r] + 
            \[CurlyPhi]i[2][2][r])))))/(4*r^4*A[r])), 
 a^2*(-1/4*(E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
      (r^2*B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])^2 - 
       4*(3*A[r]*hi[2][2][r] + r^2*wi[1][1][r]^2)))/(r^5*A[r]*B[r]) - 
   (3*E^\[CurlyPhi][r]*\[Alpha]*H0[r]*\[CurlyPhi]i[2][2][r])/(2*r^4*B[r])), 
 -1/2*(a*\[ScriptL]*(1 + \[ScriptL])*h0[r]*(-2*r*wi[1][1][r] + 
      E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
       (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(r^3*A[r]*B[r]) - 
  a*(((I/2)*\[Omega]*h1[r]*(r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
       2*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]))/(r^2*A[r]) + 
    (Derivative[1][h0][r]*(r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
       2*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]))/(2*r^2*A[r]) + 
    (h0[r]*(r*A[r]*Derivative[1][wi[1][1]][r] - Derivative[1][A][r]*
        (2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]))/(r^2*A[r]^2)), 
 -(a^2*((K[r]*(-6*A[r]*ki[2][2][r] + 2*r^2*wi[1][1][r]^2 + 
       E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        (3*Derivative[1][A][r]*ki[2][2][r] - r*wi[1][1][r]*
          (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]))))/(r^2*A[r]*B[r]) + 
    (H0[r]*(6*r*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*hi[2][2][r] + 
       3*E^\[CurlyPhi][r]*\[Alpha]*(-2 + B[r])*\[CurlyPhi]i[2][2][r]))/
     (2*r^4*B[r]) + (\[CurlyPhi]1[r]*
      (6*A[r]*(4*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] + 
         r^2*\[CurlyPhi]i[2][2][r]) - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (r^4*Derivative[1][wi[1][1]][r]^2 + 12*(A[r]*hi[2][2][r] + 
           r*Derivative[1][A][r]*\[CurlyPhi]i[2][2][r]))))/
     (4*r^5*A[r]*B[r]))), 
 -(a^2*(((-3*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*h0[r]*
      \[CurlyPhi]i[2][2][r])/(r^4*A[r]*B[r]) + 
    (h1[r]*(3*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
         r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])^2 - 
       2*(6*E^\[CurlyPhi][r]*\[Alpha]*A[r]*Derivative[1][\[CurlyPhi]][r]*
          hi[2][2][r] + 2*r^4*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
         4*r^3*wi[1][1][r]^2 + 2*E^\[CurlyPhi][r]*r^2*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]^2 + 3*E^\[CurlyPhi][r]*
          \[Alpha]*Derivative[1][A][r]*\[CurlyPhi]i[2][2][r])))/
     (4*r^4*A[r])))}
