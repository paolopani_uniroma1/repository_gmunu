{{0, -1}, 
 {\[Sigma]*((-4*m*\[ScriptCapitalM]^2*\[Omega])/
      (r*(r - 2*\[ScriptCapitalM])^2) + 
     (m*\[ScriptCapitalM]^4*(39*r^5 + 162*r^4*\[ScriptCapitalM] + 
        590*r^3*\[ScriptCapitalM]^2 + 156*r^2*\[ScriptCapitalM]^3 - 
        400*r*\[ScriptCapitalM]^4 - 3680*\[ScriptCapitalM]^5)*\[Zeta]^2*
       \[Omega])/(120*r^7*(r - 2*\[ScriptCapitalM])^3) + 
     (m*\[ScriptCapitalM]^4*(657657*r^8 + 1720026*r^7*\[ScriptCapitalM] + 
        6894030*r^6*\[ScriptCapitalM]^2 + 10539936*r^5*\[ScriptCapitalM]^3 + 
        13021624*r^4*\[ScriptCapitalM]^4 + 3830464*r^3*\[ScriptCapitalM]^5 - 
        93951648*r^2*\[ScriptCapitalM]^6 - 90341440*r*\[ScriptCapitalM]^7 - 
        102502400*\[ScriptCapitalM]^8)*\[Zeta]^3*\[Omega])/
      (3326400*r^10*(r - 2*\[ScriptCapitalM])^3) + 
     (m*\[ScriptCapitalM]^4*(7537376847*r^12 + 2397967572*r^11*
         \[ScriptCapitalM] + 29327742158*r^10*\[ScriptCapitalM]^2 - 
        42272429056*r^9*\[ScriptCapitalM]^3 - 96768529000*r^8*
         \[ScriptCapitalM]^4 - 324477808512*r^7*\[ScriptCapitalM]^5 - 
        574508975456*r^6*\[ScriptCapitalM]^6 + 18657049216*r^5*
         \[ScriptCapitalM]^7 + 4203680337920*r^4*\[ScriptCapitalM]^8 - 
        3418664711168*r^3*\[ScriptCapitalM]^9 + 1741607755776*r^2*
         \[ScriptCapitalM]^10 - 20285735608320*r*\[ScriptCapitalM]^11 + 
        38046328320000*\[ScriptCapitalM]^12)*\[Zeta]^4*\[Omega])/
      (48432384000*r^13*(r - 2*\[ScriptCapitalM])^4)) + 
   (-(r^2*\[ScriptL]*(1 + \[ScriptL])) + 2*r*(-1 + \[ScriptL] + \[ScriptL]^2)*
      \[ScriptCapitalM] + 4*\[ScriptCapitalM]^2 + r^4*\[Omega]^2)/
    (r^2*(r - 2*\[ScriptCapitalM])^2) - 
   (\[ScriptCapitalM]^2*\[Zeta]^2*(12*r^3*(-159 + \[ScriptL] + \[ScriptL]^2)*
       \[ScriptCapitalM]^4 + 16*r^2*(541 + 121*\[ScriptL] + 121*\[ScriptL]^2)*
       \[ScriptCapitalM]^5 - 160*r*(191 + 23*\[ScriptL] + 23*\[ScriptL]^2)*
       \[ScriptCapitalM]^6 + 26880*\[ScriptCapitalM]^7 + 15*r^9*\[Omega]^2 + 
      20*r^8*\[ScriptCapitalM]*\[Omega]^2 + 10*r^4*\[ScriptCapitalM]^3*
       (241 + 49*\[ScriptL] + 49*\[ScriptL]^2 - 224*\[ScriptCapitalM]^2*
         \[Omega]^2) - 15*r^7*(1 + \[ScriptL] + \[ScriptL]^2 - 
        26*\[ScriptCapitalM]^2*\[Omega]^2) + 3*r^6*\[ScriptCapitalM]*
       (5 + 5*\[ScriptL] + 5*\[ScriptL]^2 + 32*\[ScriptCapitalM]^2*
         \[Omega]^2) + 2*r^5*\[ScriptCapitalM]^2*(-355 - 115*\[ScriptL] - 
        115*\[ScriptL]^2 + 72*\[ScriptCapitalM]^2*\[Omega]^2)))/
    (240*r^8*(r - 2*\[ScriptCapitalM])^3) - 
   (\[ScriptCapitalM]^2*\[Zeta]^3*(-1440*r^3*(41791 + 2927*\[ScriptL] + 
        2927*\[ScriptL]^2)*\[ScriptCapitalM]^7 - 
      2240*r^2*(-8785 + 1151*\[ScriptL] + 1151*\[ScriptL]^2)*
       \[ScriptCapitalM]^8 - 143360*r*(254 + 65*\[ScriptL] + 65*\[ScriptL]^2)*
       \[ScriptCapitalM]^9 + 96768000*\[ScriptCapitalM]^10 + 
      22995*r^12*\[Omega]^2 + 30660*r^11*\[ScriptCapitalM]*\[Omega]^2 - 
      22995*r^10*(1 + \[ScriptL] + \[ScriptL]^2 - 14*\[ScriptCapitalM]^2*
         \[Omega]^2) - 9600*r^4*\[ScriptCapitalM]^6*(-2501 - 527*\[ScriptL] - 
        527*\[ScriptL]^2 + 560*\[ScriptCapitalM]^2*\[Omega]^2) + 
      63*r^9*\[ScriptCapitalM]*(365 + 365*\[ScriptL] + 365*\[ScriptL]^2 + 
        8472*\[ScriptCapitalM]^2*\[Omega]^2) - 448*r^5*\[ScriptCapitalM]^5*
       (1329 - 471*\[ScriptL] - 471*\[ScriptL]^2 + 9170*\[ScriptCapitalM]^2*
         \[Omega]^2) + 42*r^8*\[ScriptCapitalM]^2*(-12775 - 4015*\[ScriptL] - 
        4015*\[ScriptL]^2 + 15248*\[ScriptCapitalM]^2*\[Omega]^2) + 
      30*r^7*\[ScriptCapitalM]^3*(26551 + 2695*\[ScriptL] + 
        2695*\[ScriptL]^2 + 32096*\[ScriptCapitalM]^2*\[Omega]^2) - 
      36*r^6*\[ScriptCapitalM]^4*(-26411 - 8491*\[ScriptL] - 
        8491*\[ScriptL]^2 + 124280*\[ScriptCapitalM]^2*\[Omega]^2)))/
    (604800*r^11*(r - 2*\[ScriptCapitalM])^3) + 
   (\[ScriptCapitalM]^2*\[Zeta]^4*
     (-129024*r^3*(112535129 + 8703159*\[ScriptL] + 8703159*\[ScriptL]^2)*
       \[ScriptCapitalM]^11 + 1720320*r^2*(23596975 + 1838557*\[ScriptL] + 
        1838557*\[ScriptL]^2)*\[ScriptCapitalM]^12 - 
      33116160*r*(2171599 + 88375*\[ScriptL] + 88375*\[ScriptL]^2)*
       \[ScriptCapitalM]^13 + 45859258368000*\[ScriptCapitalM]^14 - 
      222999315*r^16*\[Omega]^2 + 148666210*r^15*\[ScriptCapitalM]*
       \[Omega]^2 + 385*r^14*(579219 + 579219*\[ScriptL] + 
        579219*\[ScriptL]^2 - 5145974*\[ScriptCapitalM]^2*\[Omega]^2) + 
      693*r^13*\[ScriptCapitalM]*(-965365 - 965365*\[ScriptL] - 
        965365*\[ScriptL]^2 + 2005284*\[ScriptCapitalM]^2*\[Omega]^2) + 
      308*r^12*\[ScriptCapitalM]^2*(14890165 + 5543965*\[ScriptL] + 
        5543965*\[ScriptL]^2 + 9896172*\[ScriptCapitalM]^2*\[Omega]^2) - 
      43008*r^4*\[ScriptCapitalM]^10*(-210857487 - 10382157*\[ScriptL] - 
        10382157*\[ScriptL]^2 + 36883000*\[ScriptCapitalM]^2*\[Omega]^2) + 
      110*r^11*\[ScriptCapitalM]^3*(-142263499 - 32522539*\[ScriptL] - 
        32522539*\[ScriptL]^2 + 86757568*\[ScriptCapitalM]^2*\[Omega]^2) + 
      5376*r^5*\[ScriptCapitalM]^9*(-951092329 - 86572189*\[ScriptL] - 
        86572189*\[ScriptL]^2 + 180230120*\[ScriptCapitalM]^2*\[Omega]^2) - 
      704*r^8*\[ScriptCapitalM]^6*(-21045023 + 6385177*\[ScriptL] + 
        6385177*\[ScriptL]^2 + 260509060*\[ScriptCapitalM]^2*\[Omega]^2) + 
      88*r^9*\[ScriptCapitalM]^5*(-166697209 - 23779609*\[ScriptL] - 
        23779609*\[ScriptL]^2 + 286972160*\[ScriptCapitalM]^2*\[Omega]^2) + 
      88*r^10*\[ScriptCapitalM]^4*(136291631 - 115969*\[ScriptL] - 
        115969*\[ScriptL]^2 + 308807330*\[ScriptCapitalM]^2*\[Omega]^2) - 
      192*r^6*\[ScriptCapitalM]^8*(-5811223275 - 968071115*\[ScriptL] - 
        968071115*\[ScriptL]^2 + 909587168*\[ScriptCapitalM]^2*\[Omega]^2) + 
      32*r^7*\[ScriptCapitalM]^7*(1316325175 + 593332135*\[ScriptL] + 
        593332135*\[ScriptL]^2 + 4668337744*\[ScriptCapitalM]^2*\[Omega]^2)))/
    (7451136000*r^14*(r - 2*\[ScriptCapitalM])^4) + 
   \[Sigma]^2*((\[ScriptCapitalM]^3*(24*r^3*\[ScriptCapitalM] + 
        4*m^2*r^3*\[ScriptCapitalM] + 9*r^3*\[ScriptL]*\[ScriptCapitalM] + 
        9*r^3*\[ScriptL]^2*\[ScriptCapitalM] - 124*r^2*\[ScriptCapitalM]^2 - 
        8*m^2*r^2*\[ScriptCapitalM]^2 - 32*r^2*\[ScriptL]*
         \[ScriptCapitalM]^2 - 32*r^2*\[ScriptL]^2*\[ScriptCapitalM]^2 + 
        216*r*\[ScriptCapitalM]^3 + 28*r*\[ScriptL]*\[ScriptCapitalM]^3 + 
        28*r*\[ScriptL]^2*\[ScriptCapitalM]^3 - 128*\[ScriptCapitalM]^4 + 
        2*r^6*\[Omega]^2 - 12*r^5*\[ScriptCapitalM]*\[Omega]^2 + 
        8*r^4*\[ScriptCapitalM]^2*\[Omega]^2 - 3*(r - 2*\[ScriptCapitalM])^2*
         (r*(10 + \[ScriptL] + 7*\[ScriptL]^2)*\[ScriptCapitalM] - 
          32*\[ScriptCapitalM]^2 + 2*r^4*\[Omega]^2)*Q[\[ScriptL], m]^2 - 
        3*(r - 2*\[ScriptCapitalM])^2*(r*(16 + 13*\[ScriptL] + 
            7*\[ScriptL]^2)*\[ScriptCapitalM] - 32*\[ScriptCapitalM]^2 + 
          2*r^4*\[Omega]^2)*Q[1 + \[ScriptL], m]^2))/
      (r^6*(r - 2*\[ScriptCapitalM])^3) + 
     (\[ScriptCapitalM]^2*\[Zeta]^2*(-18375*r^12 - 18375*r^12*\[ScriptL] - 
        18375*r^12*\[ScriptL]^2 + 55125*r^11*\[ScriptCapitalM] + 
        55125*r^11*\[ScriptL]*\[ScriptCapitalM] + 55125*r^11*\[ScriptL]^2*
         \[ScriptCapitalM] - 683564*r^10*\[ScriptCapitalM]^2 - 
        226016*r^10*\[ScriptL]*\[ScriptCapitalM]^2 - 226016*r^10*\[ScriptL]^2*
         \[ScriptCapitalM]^2 + 2993942*r^9*\[ScriptCapitalM]^3 + 
        684782*r^9*\[ScriptL]*\[ScriptCapitalM]^3 + 684782*r^9*\[ScriptL]^2*
         \[ScriptCapitalM]^3 + 3491584*r^8*\[ScriptCapitalM]^4 - 
        235200*m^2*r^8*\[ScriptCapitalM]^4 + 1055602*r^8*\[ScriptL]*
         \[ScriptCapitalM]^4 + 1055602*r^8*\[ScriptL]^2*\[ScriptCapitalM]^4 - 
        67799212*r^7*\[ScriptCapitalM]^5 - 921200*m^2*r^7*
         \[ScriptCapitalM]^5 - 13918450*r^7*\[ScriptL]*\[ScriptCapitalM]^5 - 
        13918450*r^7*\[ScriptL]^2*\[ScriptCapitalM]^5 + 
        275538656*r^6*\[ScriptCapitalM]^6 + 823200*m^2*r^6*
         \[ScriptCapitalM]^6 + 41023032*r^6*\[ScriptL]*\[ScriptCapitalM]^6 + 
        41023032*r^6*\[ScriptL]^2*\[ScriptCapitalM]^6 - 
        683024040*r^5*\[ScriptCapitalM]^7 + 3332000*m^2*r^5*
         \[ScriptCapitalM]^7 - 68469980*r^5*\[ScriptL]*\[ScriptCapitalM]^7 - 
        68469980*r^5*\[ScriptL]^2*\[ScriptCapitalM]^7 + 
        1621016960*r^4*\[ScriptCapitalM]^8 + 6507200*m^2*r^4*
         \[ScriptCapitalM]^8 + 171593200*r^4*\[ScriptL]*\[ScriptCapitalM]^8 + 
        171593200*r^4*\[ScriptL]^2*\[ScriptCapitalM]^8 - 
        4476264800*r^3*\[ScriptCapitalM]^9 + 3449600*m^2*r^3*
         \[ScriptCapitalM]^9 - 469372400*r^3*\[ScriptL]*\[ScriptCapitalM]^9 - 
        469372400*r^3*\[ScriptL]^2*\[ScriptCapitalM]^9 + 
        9290892800*r^2*\[ScriptCapitalM]^10 - 28224000*m^2*r^2*
         \[ScriptCapitalM]^10 + 648211200*r^2*\[ScriptL]*
         \[ScriptCapitalM]^10 + 648211200*r^2*\[ScriptL]^2*
         \[ScriptCapitalM]^10 - 10397408000*r*\[ScriptCapitalM]^11 - 
        331632000*r*\[ScriptL]*\[ScriptCapitalM]^11 - 
        331632000*r*\[ScriptL]^2*\[ScriptCapitalM]^11 + 
        4641280000*\[ScriptCapitalM]^12 + 18375*r^14*\[Omega]^2 + 
        112714*r^13*\[ScriptCapitalM]*\[Omega]^2 + 
        83286*r^12*\[ScriptCapitalM]^2*\[Omega]^2 - 
        627562*r^11*\[ScriptCapitalM]^3*\[Omega]^2 - 
        2967312*r^10*\[ScriptCapitalM]^4*\[Omega]^2 + 
        9513220*r^9*\[ScriptCapitalM]^5*\[Omega]^2 - 
        7763520*r^8*\[ScriptCapitalM]^6*\[Omega]^2 + 
        34904800*r^7*\[ScriptCapitalM]^7*\[Omega]^2 - 
        102457600*r^6*\[ScriptCapitalM]^8*\[Omega]^2 + 
        160249600*r^5*\[ScriptCapitalM]^9*\[Omega]^2 - 
        128576000*r^4*\[ScriptCapitalM]^10*\[Omega]^2 - 
        6*(r - 2*\[ScriptCapitalM])^2*\[ScriptCapitalM]*
         (-350*r^3*(309594 + 7753*\[ScriptL] + 38175*\[ScriptL]^2)*
           \[ScriptCapitalM]^6 + 2800*r^2*(114374 + 959*\[ScriptL] + 
            15673*\[ScriptL]^2)*\[ScriptCapitalM]^7 - 
          98000*r*(7138 - 15*\[ScriptL] + 423*\[ScriptL]^2)*
           \[ScriptCapitalM]^8 + 580160000*\[ScriptCapitalM]^9 + 
          62482*r^11*\[Omega]^2 + 187446*r^10*\[ScriptCapitalM]*\[Omega]^2 + 
          399225*r^9*\[ScriptCapitalM]^2*\[Omega]^2 + r^6*\[ScriptCapitalM]^3*
           (3443642 + 242369*\[ScriptL] + 798519*\[ScriptL]^2 - 
            4869200*\[ScriptCapitalM]^2*\[Omega]^2) - 
          42*r^7*\[ScriptCapitalM]^2*(2596 + 1298*\[ScriptL] + 
            1298*\[ScriptL]^2 + 11875*\[ScriptCapitalM]^2*\[Omega]^2) - 
          6*r^8*\[ScriptCapitalM]*(18172 + 4543*\[ScriptL] + 
            4543*\[ScriptL]^2 + 125690*\[ScriptCapitalM]^2*\[Omega]^2) - 
          20*r^4*\[ScriptCapitalM]^5*(-2162486 - 17295*\[ScriptL] - 
            198105*\[ScriptL]^2 + 803600*\[ScriptCapitalM]^2*\[Omega]^2) + 
          5*r^5*\[ScriptCapitalM]^4*(-2802942 - 12147*\[ScriptL] - 
            665317*\[ScriptL]^2 + 1595440*\[ScriptCapitalM]^2*\[Omega]^2))*
         Q[\[ScriptL], m]^2 - 6*(r - 2*\[ScriptCapitalM])^2*\[ScriptCapitalM]*
         (-350*r^3*(340016 + 68597*\[ScriptL] + 38175*\[ScriptL]^2)*
           \[ScriptCapitalM]^6 + 2800*r^2*(129088 + 30387*\[ScriptL] + 
            15673*\[ScriptL]^2)*\[ScriptCapitalM]^7 - 
          98000*r*(7576 + 861*\[ScriptL] + 423*\[ScriptL]^2)*
           \[ScriptCapitalM]^8 + 580160000*\[ScriptCapitalM]^9 + 
          62482*r^11*\[Omega]^2 + 187446*r^10*\[ScriptCapitalM]*\[Omega]^2 + 
          399225*r^9*\[ScriptCapitalM]^2*\[Omega]^2 + r^6*\[ScriptCapitalM]^3*
           (3999792 + 1354669*\[ScriptL] + 798519*\[ScriptL]^2 - 
            4869200*\[ScriptCapitalM]^2*\[Omega]^2) - 
          42*r^7*\[ScriptCapitalM]^2*(2596 + 1298*\[ScriptL] + 
            1298*\[ScriptL]^2 + 11875*\[ScriptCapitalM]^2*\[Omega]^2) - 
          6*r^8*\[ScriptCapitalM]*(18172 + 4543*\[ScriptL] + 
            4543*\[ScriptL]^2 + 125690*\[ScriptCapitalM]^2*\[Omega]^2) - 
          20*r^4*\[ScriptCapitalM]^5*(-2343296 - 378915*\[ScriptL] - 
            198105*\[ScriptL]^2 + 803600*\[ScriptCapitalM]^2*\[Omega]^2) + 
          5*r^5*\[ScriptCapitalM]^4*(-3456112 - 1318487*\[ScriptL] - 
            665317*\[ScriptL]^2 + 1595440*\[ScriptCapitalM]^2*\[Omega]^2))*
         Q[1 + \[ScriptL], m]^2))/(588000*r^12*(r - 2*\[ScriptCapitalM])^4) + 
     (\[ScriptCapitalM]^2*\[Zeta]^3*(-29563813125*r^15 - 
        29563813125*r^15*\[ScriptL] - 29563813125*r^15*\[ScriptL]^2 + 
        88691439375*r^14*\[ScriptCapitalM] + 88691439375*r^14*\[ScriptL]*
         \[ScriptCapitalM] + 88691439375*r^14*\[ScriptL]^2*
         \[ScriptCapitalM] - 769146189504*r^13*\[ScriptCapitalM]^2 - 
        259287953001*r^13*\[ScriptL]*\[ScriptCapitalM]^2 - 
        259287953001*r^13*\[ScriptL]^2*\[ScriptCapitalM]^2 + 
        2219390897262*r^12*\[ScriptCapitalM]^3 + 434291802252*r^12*\[ScriptL]*
         \[ScriptCapitalM]^3 + 434291802252*r^12*\[ScriptL]^2*
         \[ScriptCapitalM]^3 + 4247488975224*r^11*\[ScriptCapitalM]^4 - 
        277632432000*m^2*r^11*\[ScriptCapitalM]^4 + 988343353272*r^11*
         \[ScriptL]*\[ScriptCapitalM]^4 + 988343353272*r^11*\[ScriptL]^2*
         \[ScriptCapitalM]^4 - 34078617926232*r^10*\[ScriptCapitalM]^5 - 
        393312612000*m^2*r^10*\[ScriptCapitalM]^5 - 5840007488700*r^10*
         \[ScriptL]*\[ScriptCapitalM]^5 - 5840007488700*r^10*\[ScriptL]^2*
         \[ScriptCapitalM]^5 + 97416694692416*r^9*\[ScriptCapitalM]^6 - 
        402865848000*m^2*r^9*\[ScriptCapitalM]^6 + 11882135687752*r^9*
         \[ScriptL]*\[ScriptCapitalM]^6 + 11882135687752*r^9*\[ScriptL]^2*
         \[ScriptCapitalM]^6 - 147625914636640*r^8*\[ScriptCapitalM]^7 + 
        1397217360000*m^2*r^8*\[ScriptCapitalM]^7 - 7363387001080*r^8*
         \[ScriptL]*\[ScriptCapitalM]^7 - 7363387001080*r^8*\[ScriptL]^2*
         \[ScriptCapitalM]^7 - 171095834843840*r^7*\[ScriptCapitalM]^8 + 
        2305393552000*m^2*r^7*\[ScriptCapitalM]^8 - 41017691561600*r^7*
         \[ScriptL]*\[ScriptCapitalM]^8 - 41017691561600*r^7*\[ScriptL]^2*
         \[ScriptCapitalM]^8 + 2116363087955200*r^6*\[ScriptCapitalM]^9 + 
        12838704032000*m^2*r^6*\[ScriptCapitalM]^9 + 281658252741600*r^6*
         \[ScriptL]*\[ScriptCapitalM]^9 + 281658252741600*r^6*\[ScriptL]^2*
         \[ScriptCapitalM]^9 - 7542543530726400*r^5*\[ScriptCapitalM]^10 + 
        21413034496000*m^2*r^5*\[ScriptCapitalM]^10 - 714839747328000*r^5*
         \[ScriptL]*\[ScriptCapitalM]^10 - 714839747328000*r^5*\[ScriptL]^2*
         \[ScriptCapitalM]^10 + 13781871935596800*r^4*\[ScriptCapitalM]^11 - 
        30404793216000*m^2*r^4*\[ScriptCapitalM]^11 + 737402478153600*r^4*
         \[ScriptL]*\[ScriptCapitalM]^11 + 737402478153600*r^4*\[ScriptL]^2*
         \[ScriptCapitalM]^11 - 14346913068544000*r^3*\[ScriptCapitalM]^12 - 
        32198754560000*m^2*r^3*\[ScriptCapitalM]^12 - 560449026368000*r^3*
         \[ScriptL]*\[ScriptCapitalM]^12 - 560449026368000*r^3*\[ScriptL]^2*
         \[ScriptCapitalM]^12 + 15987223150976000*r^2*\[ScriptCapitalM]^13 - 
        59498700800000*m^2*r^2*\[ScriptCapitalM]^13 + 1029268363200000*r^2*
         \[ScriptL]*\[ScriptCapitalM]^13 + 1029268363200000*r^2*\[ScriptL]^2*
         \[ScriptCapitalM]^13 - 22740403445760000*r*\[ScriptCapitalM]^14 - 
        857322188800000*r*\[ScriptL]*\[ScriptCapitalM]^14 - 
        857322188800000*r*\[ScriptL]^2*\[ScriptCapitalM]^14 + 
        14450070835200000*\[ScriptCapitalM]^15 + 29563813125*r^17*
         \[Omega]^2 + 151987335654*r^16*\[ScriptCapitalM]*\[Omega]^2 + 
        33219801846*r^15*\[ScriptCapitalM]^2*\[Omega]^2 - 
        302240707032*r^14*\[ScriptCapitalM]^3*\[Omega]^2 - 
        1896589771032*r^13*\[ScriptCapitalM]^4*\[Omega]^2 + 
        2013451648120*r^12*\[ScriptCapitalM]^5*\[Omega]^2 - 
        312151284720*r^11*\[ScriptCapitalM]^6*\[Omega]^2 + 
        5103311287200*r^10*\[ScriptCapitalM]^7*\[Omega]^2 + 
        60745001072000*r^9*\[ScriptCapitalM]^8*\[Omega]^2 - 
        160759125833600*r^8*\[ScriptCapitalM]^9*\[Omega]^2 + 
        197822455040000*r^7*\[ScriptCapitalM]^10*\[Omega]^2 - 
        170828180992000*r^6*\[ScriptCapitalM]^11*\[Omega]^2 + 
        202227970560000*r^5*\[ScriptCapitalM]^12*\[Omega]^2 - 
        361048934400000*r^4*\[ScriptCapitalM]^13*\[Omega]^2 - 
        3*(r - 2*\[ScriptCapitalM])^2*\[ScriptCapitalM]*
         (-28224000*r^3*(71818878 - 90447*\[ScriptL] + 2020123*\[ScriptL]^2)*
           \[ScriptCapitalM]^9 + 1267728000*r^2*(1099018 + 12745*\[ScriptL] + 
            54175*\[ScriptL]^2)*\[ScriptCapitalM]^10 - 135224320000*r*
           (15432 - 50*\[ScriptL] + 1585*\[ScriptL]^2)*\[ScriptCapitalM]^11 + 
          3612517708800000*\[ScriptCapitalM]^12 + 171696544404*r^14*
           \[Omega]^2 + 515089633212*r^13*\[ScriptCapitalM]*\[Omega]^2 + 
          1114949414700*r^12*\[ScriptCapitalM]^2*\[Omega]^2 - 
          940800*r^4*\[ScriptCapitalM]^8*(-1212594969 - 1210138*\[ScriptL] - 
            138981838*\[ScriptL]^2 + 95942000*\[ScriptCapitalM]^2*
             \[Omega]^2) - 22400*r^5*\[ScriptCapitalM]^7*(14582260796 + 
            394942709*\[ScriptL] + 2336830349*\[ScriptL]^2 + 
            882127400*\[ScriptCapitalM]^2*\[Omega]^2) - 
          15400*r^6*\[ScriptCapitalM]^6*(-1731591430 - 126345999*\[ScriptL] - 
            292743573*\[ScriptL]^2 + 3283356720*\[ScriptCapitalM]^2*
             \[Omega]^2) - 154*r^10*\[ScriptCapitalM]^2*(1821277026 + 
            910638513*\[ScriptL] + 910638513*\[ScriptL]^2 + 
            5000382500*\[ScriptCapitalM]^2*\[Omega]^2) + 
          33*r^11*\[ScriptCapitalM]*(-8499292788 - 2124823197*\[ScriptL] - 
            2124823197*\[ScriptL]^2 + 17787721240*\[ScriptCapitalM]^2*
             \[Omega]^2) - 44*r^9*\[ScriptCapitalM]^3*(-93765521646 - 
            3514476897*\[ScriptL] - 15273940347*\[ScriptL]^2 + 
            140307823600*\[ScriptCapitalM]^2*\[Omega]^2) + 
          40*r^7*\[ScriptCapitalM]^5*(567871373696 + 17844269245*\[ScriptL] - 
            14983126895*\[ScriptL]^2 + 321897189600*\[ScriptCapitalM]^2*
             \[Omega]^2) - 60*r^8*\[ScriptCapitalM]^4*(135206883152 - 
            6145411393*\[ScriptL] + 34117743077*\[ScriptL]^2 + 
            404474859040*\[ScriptCapitalM]^2*\[Omega]^2))*
         Q[\[ScriptL], m]^2 - 3*(r - 2*\[ScriptCapitalM])^2*\[ScriptCapitalM]*
         (-28224000*r^3*(73929448 + 4130693*\[ScriptL] + 
            2020123*\[ScriptL]^2)*\[ScriptCapitalM]^9 + 1267728000*r^2*
           (1140448 + 95605*\[ScriptL] + 54175*\[ScriptL]^2)*
           \[ScriptCapitalM]^10 - 135224320000*r*(17067 + 3220*\[ScriptL] + 
            1585*\[ScriptL]^2)*\[ScriptCapitalM]^11 + 3612517708800000*
           \[ScriptCapitalM]^12 + 171696544404*r^14*\[Omega]^2 + 
          515089633212*r^13*\[ScriptCapitalM]*\[Omega]^2 + 
          1114949414700*r^12*\[ScriptCapitalM]^2*\[Omega]^2 - 
          940800*r^4*\[ScriptCapitalM]^8*(-1350366669 - 
            276753538*\[ScriptL] - 138981838*\[ScriptL]^2 + 
            95942000*\[ScriptCapitalM]^2*\[Omega]^2) - 
          22400*r^5*\[ScriptCapitalM]^7*(16524148436 + 4278717989*
             \[ScriptL] + 2336830349*\[ScriptL]^2 + 882127400*
             \[ScriptCapitalM]^2*\[Omega]^2) - 15400*r^6*\[ScriptCapitalM]^6*
           (-1897989004 - 459141147*\[ScriptL] - 292743573*\[ScriptL]^2 + 
            3283356720*\[ScriptCapitalM]^2*\[Omega]^2) - 
          154*r^10*\[ScriptCapitalM]^2*(1821277026 + 910638513*\[ScriptL] + 
            910638513*\[ScriptL]^2 + 5000382500*\[ScriptCapitalM]^2*
             \[Omega]^2) + 33*r^11*\[ScriptCapitalM]*(-8499292788 - 
            2124823197*\[ScriptL] - 2124823197*\[ScriptL]^2 + 
            17787721240*\[ScriptCapitalM]^2*\[Omega]^2) - 
          44*r^9*\[ScriptCapitalM]^3*(-105524985096 - 27033403797*
             \[ScriptL] - 15273940347*\[ScriptL]^2 + 140307823600*
             \[ScriptCapitalM]^2*\[Omega]^2) + 40*r^7*\[ScriptCapitalM]^5*
           (535043977556 - 47810523035*\[ScriptL] - 14983126895*
             \[ScriptL]^2 + 321897189600*\[ScriptCapitalM]^2*\[Omega]^2) - 
          60*r^8*\[ScriptCapitalM]^4*(175470037622 + 74380897547*\[ScriptL] + 
            34117743077*\[ScriptL]^2 + 404474859040*\[ScriptCapitalM]^2*
             \[Omega]^2))*Q[1 + \[ScriptL], m]^2))/(1140955200000*r^15*
       (r - 2*\[ScriptCapitalM])^4) + (\[ScriptCapitalM]^2*\[Zeta]^4*
       (-14823585066992845893750*r^19 - 14823585066992845893750*r^19*
         \[ScriptL] - 14823585066992845893750*r^19*\[ScriptL]^2 + 
        74117925334964229468750*r^18*\[ScriptCapitalM] + 
        74117925334964229468750*r^18*\[ScriptL]*\[ScriptCapitalM] + 
        74117925334964229468750*r^18*\[ScriptL]^2*\[ScriptCapitalM] - 
        445946689926989944636944*r^17*\[ScriptCapitalM]^2 - 
        209091069138429154037361*r^17*\[ScriptL]*\[ScriptCapitalM]^2 - 
        209091069138429154037361*r^17*\[ScriptL]^2*\[ScriptCapitalM]^2 + 
        1861095336396901869984720*r^16*\[ScriptCapitalM]^3 + 
        477055779802702743586944*r^16*\[ScriptL]*\[ScriptCapitalM]^3 + 
        477055779802702743586944*r^16*\[ScriptL]^2*\[ScriptCapitalM]^3 - 
        1060491598682124315220800*r^15*\[ScriptCapitalM]^4 - 
        104057692626125478240000*m^2*r^15*\[ScriptCapitalM]^4 - 
        112476558265071426463752*r^15*\[ScriptL]*\[ScriptCapitalM]^4 - 
        112476558265071426463752*r^15*\[ScriptL]^2*\[ScriptCapitalM]^4 - 
        13806696474466557350025480*r^14*\[ScriptCapitalM]^5 + 
        110961932990716047240000*m^2*r^14*\[ScriptCapitalM]^5 - 
        2621799443470012288495584*r^14*\[ScriptL]*\[ScriptCapitalM]^5 - 
        2621799443470012288495584*r^14*\[ScriptL]^2*\[ScriptCapitalM]^5 + 
        52320902865930388510929680*r^13*\[ScriptCapitalM]^6 + 
        114933320444406784320000*m^2*r^13*\[ScriptCapitalM]^6 + 
        7347590025519241449680872*r^13*\[ScriptL]*\[ScriptCapitalM]^6 + 
        7347590025519241449680872*r^13*\[ScriptL]^2*\[ScriptCapitalM]^6 - 
        135774573998950003103235392*r^12*\[ScriptCapitalM]^7 + 
        722515904243950536720000*m^2*r^12*\[ScriptCapitalM]^7 - 
        15109066976477065643875824*r^12*\[ScriptL]*\[ScriptCapitalM]^7 - 
        15109066976477065643875824*r^12*\[ScriptL]^2*\[ScriptCapitalM]^7 + 
        289777156378746114991427840*r^11*\[ScriptCapitalM]^8 + 
        262563768168215754240000*m^2*r^11*\[ScriptCapitalM]^8 + 
        25796441822022812873560160*r^11*\[ScriptL]*\[ScriptCapitalM]^8 + 
        25796441822022812873560160*r^11*\[ScriptL]^2*\[ScriptCapitalM]^8 - 
        372879556162349209296968320*r^10*\[ScriptCapitalM]^9 + 
        1272536355761606295360000*m^2*r^10*\[ScriptCapitalM]^9 - 
        11538148432130286272571200*r^10*\[ScriptL]*\[ScriptCapitalM]^9 - 
        11538148432130286272571200*r^10*\[ScriptL]^2*\[ScriptCapitalM]^9 + 
        1082594054540678568090899200*r^9*\[ScriptCapitalM]^10 - 
        3001486430718861845760000*m^2*r^9*\[ScriptCapitalM]^10 + 
        89295846676560867418924800*r^9*\[ScriptL]*\[ScriptCapitalM]^10 + 
        89295846676560867418924800*r^9*\[ScriptL]^2*\[ScriptCapitalM]^10 - 
        8025959681427667765653414400*r^8*\[ScriptCapitalM]^11 - 
        19310019933019645589760000*m^2*r^8*\[ScriptCapitalM]^11 - 
        853210096326082466487174400*r^8*\[ScriptL]*\[ScriptCapitalM]^11 - 
        853210096326082466487174400*r^8*\[ScriptL]^2*\[ScriptCapitalM]^11 + 
        33555799435258907317109222400*r^7*\[ScriptCapitalM]^12 - 
        18295555406554764898560000*m^2*r^7*\[ScriptCapitalM]^12 + 
        2824995122176381873403596800*r^7*\[ScriptL]*\[ScriptCapitalM]^12 + 
        2824995122176381873403596800*r^7*\[ScriptL]^2*\[ScriptCapitalM]^12 - 
        90924030918095171100519936000*r^6*\[ScriptCapitalM]^13 + 
        103726131954924552353280000*m^2*r^6*\[ScriptCapitalM]^13 - 
        5939860293114433850876544000*r^6*\[ScriptL]*\[ScriptCapitalM]^13 - 
        5939860293114433850876544000*r^6*\[ScriptL]^2*\[ScriptCapitalM]^13 + 
        197902312176975403010162688000*r^5*\[ScriptCapitalM]^14 - 
        45671201606454672568320000*m^2*r^5*\[ScriptCapitalM]^14 + 
        11589477931765874602344960000*r^5*\[ScriptL]*\[ScriptCapitalM]^14 + 
        11589477931765874602344960000*r^5*\[ScriptL]^2*\[ScriptCapitalM]^14 - 
        409821497690462617645670400000*r^4*\[ScriptCapitalM]^15 + 
        130565992905699818987520000*m^2*r^4*\[ScriptCapitalM]^15 - 
        23690195447347493338982400000*r^4*\[ScriptL]*\[ScriptCapitalM]^15 - 
        23690195447347493338982400000*r^4*\[ScriptL]^2*\[ScriptCapitalM]^15 + 
        792634651149605822146068480000*r^3*\[ScriptCapitalM]^16 - 
        709136508548197752422400000*m^2*r^3*\[ScriptCapitalM]^16 + 
        39676521074096801394339840000*r^3*\[ScriptL]*\[ScriptCapitalM]^16 + 
        39676521074096801394339840000*r^3*\[ScriptL]^2*\[ScriptCapitalM]^16 - 
        1155834589483708775075635200000*r^2*\[ScriptCapitalM]^17 + 
        769748716771663749120000000*m^2*r^2*\[ScriptCapitalM]^17 - 
        40135747530313855887667200000*r^2*\[ScriptL]*\[ScriptCapitalM]^17 - 
        40135747530313855887667200000*r^2*\[ScriptL]^2*\[ScriptCapitalM]^17 + 
        1002885948895619640563712000000*r*\[ScriptCapitalM]^18 + 
        17184495230776181468160000000*r*\[ScriptL]*\[ScriptCapitalM]^18 + 
        17184495230776181468160000000*r*\[ScriptL]^2*\[ScriptCapitalM]^18 - 
        373218991367010747678720000000*\[ScriptCapitalM]^19 + 
        14823585066992845893750*r^21*\[Omega]^2 + 41519503157565268679844*
         r^20*\[ScriptCapitalM]*\[Omega]^2 - 129389251220377268152032*r^19*
         \[ScriptCapitalM]^2*\[Omega]^2 - 220189372858068888423864*r^18*
         \[ScriptCapitalM]^3*\[Omega]^2 - 480339608582314141823448*r^17*
         \[ScriptCapitalM]^4*\[Omega]^2 + 1903789487283789832293424*r^16*
         \[ScriptCapitalM]^5*\[Omega]^2 - 996718818300441926106560*r^15*
         \[ScriptCapitalM]^6*\[Omega]^2 + 9066232545433388617943040*r^14*
         \[ScriptCapitalM]^7*\[Omega]^2 - 5194589443369130948998400*r^13*
         \[ScriptCapitalM]^8*\[Omega]^2 - 22457403186942865601465600*r^12*
         \[ScriptCapitalM]^9*\[Omega]^2 - 140857078403736761270668800*r^11*
         \[ScriptCapitalM]^10*\[Omega]^2 + 578048619321507443870208000*r^10*
         \[ScriptCapitalM]^11*\[Omega]^2 - 1185131706008120007008256000*r^9*
         \[ScriptCapitalM]^12*\[Omega]^2 + 2488716104293814267980800000*r^8*
         \[ScriptCapitalM]^13*\[Omega]^2 - 5379450411665508666808320000*r^7*
         \[ScriptCapitalM]^14*\[Omega]^2 + 10044732408143841531002880000*r^6*
         \[ScriptCapitalM]^15*\[Omega]^2 - 12710885934452119968153600000*r^5*
         \[ScriptCapitalM]^16*\[Omega]^2 + 7278326636877362626560000000*r^4*
         \[ScriptCapitalM]^17*\[Omega]^2 - 3*(r - 2*\[ScriptCapitalM])^2*
         \[ScriptCapitalM]*(783151649280000*r^3*(75310236892582 + 
            304181195031*\[ScriptL] + 4841037319377*\[ScriptL]^2)*
           \[ScriptCapitalM]^13 - 383744308147200000*r^2*(306093273142 + 
            127419435*\[ScriptL] + 16060513469*\[ScriptL]^2)*
           \[ScriptCapitalM]^14 + 6036297967155456000000*r*
           (26337498 - 23195*\[ScriptL] + 711715*\[ScriptL]^2)*
           \[ScriptCapitalM]^15 - 93304747841752686919680000000*
           \[ScriptCapitalM]^16 + 81049063336212857729844*r^18*\[Omega]^2 + 
          81049063336212857729844*r^17*\[ScriptCapitalM]*\[Omega]^2 + 
          40583794899252851096136*r^16*\[ScriptCapitalM]^2*\[Omega]^2 + 
          2738292480000*r^4*\[ScriptCapitalM]^12*(-9932014126594974 - 
            45360273162699*\[ScriptL] - 627622359662221*\[ScriptL]^2 + 
            664494999168000*\[ScriptCapitalM]^2*\[Omega]^2) - 
          60850944000*r^5*\[ScriptCapitalM]^11*(-204147982071384203 - 
            258951119322245*\[ScriptL] - 13513398216540715*\[ScriptL]^2 + 
            27145608488598600*\[ScriptCapitalM]^2*\[Omega]^2) + 
          4704000*r^6*\[ScriptCapitalM]^10*(-1017069481007231445278 - 
            5640151365259038369*\[ScriptL] - 94130030719557170079*
             \[ScriptL]^2 + 198532514009942592480*\[ScriptCapitalM]^2*
             \[Omega]^2) - 1881600*r^7*\[ScriptCapitalM]^9*
           (-542724995102217169716 - 12177140566148289427*\[ScriptL] - 
            77416217961293021077*\[ScriptL]^2 + 202441633876576294200*
             \[ScriptCapitalM]^2*\[Omega]^2) + 89600*r^8*\[ScriptCapitalM]^8*
           (-563427604131543390200 - 12677508881826529559*\[ScriptL] + 
            25991200386549857641*\[ScriptL]^2 + 1742841068058120561300*
             \[ScriptCapitalM]^2*\[Omega]^2) - 429*r^15*\[ScriptCapitalM]*
           (296893160709632096036 + 74223290177408024009*\[ScriptL] + 
            74223290177408024009*\[ScriptL]^2 + 1753003826701162679320*
             \[ScriptCapitalM]^2*\[Omega]^2) - 572*r^14*\[ScriptCapitalM]^2*
           (-222669870532224072027 + 2165323889889208792420*\[ScriptCapitalM]^
              2*\[Omega]^2) - 572*r^13*\[ScriptCapitalM]^3*
           (-3529015265883729942666 - 339901494208481291811*\[ScriptL] - 
            760117123316396832711*\[ScriptL]^2 + 5681128998249094443200*
             \[ScriptCapitalM]^2*\[Omega]^2) - 11200*r^9*\[ScriptCapitalM]^7*
           (-4119431597129408191008 + 147039918797706243829*\[ScriptL] + 
            16501831755848499580*\[ScriptL]^2 + 8928123499478044627920*
             \[ScriptCapitalM]^2*\[Omega]^2) + 104*r^12*\[ScriptCapitalM]^4*
           (-57667089718820600356437 + 364682580433058766846*\[ScriptL] - 
            10133618576082228298104*\[ScriptL]^2 + 9019028591926007429600*
             \[ScriptCapitalM]^2*\[Omega]^2) + 520*r^11*\[ScriptCapitalM]^5*
           (27369265605627472442894 + 339511285747710314227*\[ScriptL] + 
            2897066397500884787167*\[ScriptL]^2 + 23630617389554690907840*
             \[ScriptCapitalM]^2*\[Omega]^2) + 80*r^10*\[ScriptCapitalM]^6*
           (-530091420565396124019156 - 13281357043056556564825*\[ScriptL] - 
            61016069829975888128215*\[ScriptL]^2 + 395390739150081513963600*
             \[ScriptCapitalM]^2*\[Omega]^2))*Q[\[ScriptL], m]^2 - 
        3*(r - 2*\[ScriptCapitalM])^2*\[ScriptCapitalM]*
         (783151649280000*r^3*(79847093016928 + 9377893443723*\[ScriptL] + 
            4841037319377*\[ScriptL]^2)*\[ScriptCapitalM]^13 - 
          383744308147200000*r^2*(322026367176 + 31993607503*\[ScriptL] + 
            16060513469*\[ScriptL]^2)*\[ScriptCapitalM]^14 + 
          6036297967155456000000*r*(27072408 + 1446625*\[ScriptL] + 
            711715*\[ScriptL]^2)*\[ScriptCapitalM]^15 - 
          93304747841752686919680000000*\[ScriptCapitalM]^16 + 
          81049063336212857729844*r^18*\[Omega]^2 + 81049063336212857729844*
           r^17*\[ScriptCapitalM]*\[Omega]^2 + 40583794899252851096136*r^16*
           \[ScriptCapitalM]^2*\[Omega]^2 + 2738292480000*r^4*
           \[ScriptCapitalM]^12*(-10514276213094496 - 1209884446161743*
             \[ScriptL] - 627622359662221*\[ScriptL]^2 + 664494999168000*
             \[ScriptCapitalM]^2*\[Omega]^2) - 60850944000*r^5*
           \[ScriptCapitalM]^11*(-217402429168602673 - 26767845313759185*
             \[ScriptL] - 13513398216540715*\[ScriptL]^2 + 27145608488598600*
             \[ScriptCapitalM]^2*\[Omega]^2) + 4704000*r^6*
           \[ScriptCapitalM]^10*(-1105559360361529576988 - 
            182619910073855301789*\[ScriptL] - 94130030719557170079*
             \[ScriptL]^2 + 198532514009942592480*\[ScriptCapitalM]^2*
             \[Omega]^2) - 1881600*r^7*\[ScriptCapitalM]^9*
           (-607964072497361901366 - 142655295356437752727*\[ScriptL] - 
            77416217961293021077*\[ScriptL]^2 + 202441633876576294200*
             \[ScriptCapitalM]^2*\[Omega]^2) + 89600*r^8*\[ScriptCapitalM]^8*
           (-524758894863167003000 + 64659909654926244841*\[ScriptL] + 
            25991200386549857641*\[ScriptL]^2 + 1742841068058120561300*
             \[ScriptCapitalM]^2*\[Omega]^2) - 429*r^15*\[ScriptCapitalM]*
           (296893160709632096036 + 74223290177408024009*\[ScriptL] + 
            74223290177408024009*\[ScriptL]^2 + 1753003826701162679320*
             \[ScriptCapitalM]^2*\[Omega]^2) - 572*r^14*\[ScriptCapitalM]^2*
           (-222669870532224072027 + 2165323889889208792420*\[ScriptCapitalM]^
              2*\[Omega]^2) - 572*r^13*\[ScriptCapitalM]^3*
           (-3949230894991645483566 - 1180332752424312373611*\[ScriptL] - 
            760117123316396832711*\[ScriptL]^2 + 5681128998249094443200*
             \[ScriptCapitalM]^2*\[Omega]^2) - 11200*r^9*\[ScriptCapitalM]^7*
           (-4249969684171265935257 - 114036255286009244669*\[ScriptL] + 
            16501831755848499580*\[ScriptL]^2 + 8928123499478044627920*
             \[ScriptCapitalM]^2*\[Omega]^2) + 104*r^12*\[ScriptCapitalM]^4*
           (-68165390875335887421387 - 20631919732597515363054*\[ScriptL] - 
            10133618576082228298104*\[ScriptL]^2 + 9019028591926007429600*
             \[ScriptCapitalM]^2*\[Omega]^2) + 520*r^11*\[ScriptCapitalM]^5*
           (29926820717380646915834 + 5454621509254059260107*\[ScriptL] + 
            2897066397500884787167*\[ScriptL]^2 + 23630617389554690907840*
             \[ScriptCapitalM]^2*\[Omega]^2) + 80*r^10*\[ScriptCapitalM]^6*
           (-577826133352315455582546 - 108750782616895219691605*\[ScriptL] - 
            61016069829975888128215*\[ScriptL]^2 + 395390739150081513963600*
             \[ScriptCapitalM]^2*\[Omega]^2))*Q[1 + \[ScriptL], m]^2))/
      (543266817043991040000000*r^18*(r - 2*\[ScriptCapitalM])^5)), 
  (2*\[ScriptCapitalM])/(r^2 - 2*r*\[ScriptCapitalM]) - 
   (\[ScriptCapitalM]^2*(15*r^6 + 15*r^5*\[ScriptCapitalM] + 
      740*r^4*\[ScriptCapitalM]^2 - 930*r^3*\[ScriptCapitalM]^3 + 
      48*r^2*\[ScriptCapitalM]^4 - 8560*r*\[ScriptCapitalM]^5 + 
      13440*\[ScriptCapitalM]^6)*\[Zeta]^2)/
    (240*r^7*(r - 2*\[ScriptCapitalM])^2) - 
   (\[ScriptCapitalM]^2*(22995*r^9 + 22995*r^8*\[ScriptCapitalM] + 
      582540*r^7*\[ScriptCapitalM]^2 + 368550*r^6*\[ScriptCapitalM]^3 - 
      213696*r^5*\[ScriptCapitalM]^4 + 168000*r^4*\[ScriptCapitalM]^5 - 
      23673600*r^3*\[ScriptCapitalM]^6 + 12831840*r^2*\[ScriptCapitalM]^7 + 
      5985280*r*\[ScriptCapitalM]^8 + 48384000*\[ScriptCapitalM]^9)*
     \[Zeta]^3)/(604800*r^10*(r - 2*\[ScriptCapitalM])^2) + 
   (\[ScriptCapitalM]^2*(-222999315*r^13 + 222999315*r^12*\[ScriptCapitalM] - 
      4140172190*r^11*\[ScriptCapitalM]^2 + 7368640510*r^10*
       \[ScriptCapitalM]^3 + 2743617492*r^9*\[ScriptCapitalM]^4 + 
      20156589376*r^8*\[ScriptCapitalM]^5 + 25497482560*r^7*
       \[ScriptCapitalM]^6 + 8872559520*r^6*\[ScriptCapitalM]^7 - 
      1098009749760*r^5*\[ScriptCapitalM]^8 + 2917052861184*r^4*
       \[ScriptCapitalM]^9 - 3234453078528*r^3*\[ScriptCapitalM]^10 + 
      8050826327040*r^2*\[ScriptCapitalM]^11 - 24492695377920*r*
       \[ScriptCapitalM]^12 + 22929629184000*\[ScriptCapitalM]^13)*\[Zeta]^4)/
    (7451136000*r^13*(r - 2*\[ScriptCapitalM])^3) + 
   \[Sigma]^2*((4*\[ScriptCapitalM]^4*(-6*r^2 + 19*r*\[ScriptCapitalM] - 
        16*\[ScriptCapitalM]^2 + 12*(r - 2*\[ScriptCapitalM])^2*
         Q[\[ScriptL], m]^2 + 12*(r - 2*\[ScriptCapitalM])^2*
         Q[1 + \[ScriptL], m]^2))/(r^5*(r - 2*\[ScriptCapitalM])^2) + 
     (\[ScriptCapitalM]^2*\[Zeta]^2*(18375*r^11 - 
        18375*r^10*\[ScriptCapitalM] + 646814*r^9*\[ScriptCapitalM]^2 - 
        1700314*r^8*\[ScriptCapitalM]^3 - 6892212*r^7*\[ScriptCapitalM]^4 + 
        54014788*r^6*\[ScriptCapitalM]^5 - 167509080*r^5*
         \[ScriptCapitalM]^6 + 348005880*r^4*\[ScriptCapitalM]^7 - 
        925005200*r^3*\[ScriptCapitalM]^8 + 2626254400*r^2*
         \[ScriptCapitalM]^9 - 4038384000*r*\[ScriptCapitalM]^10 + 
        2320640000*\[ScriptCapitalM]^11 - 48*(r - 2*\[ScriptCapitalM])^3*
         \[ScriptCapitalM]^2*(13629*r^6 + 68145*r^5*\[ScriptCapitalM] - 
          281910*r^4*\[ScriptCapitalM]^2 + 759850*r^3*\[ScriptCapitalM]^3 - 
          1691200*r^2*\[ScriptCapitalM]^4 + 5071500*r*\[ScriptCapitalM]^5 - 
          18130000*\[ScriptCapitalM]^6)*Q[\[ScriptL], m]^2 - 
        48*(r - 2*\[ScriptCapitalM])^3*\[ScriptCapitalM]^2*
         (13629*r^6 + 68145*r^5*\[ScriptCapitalM] - 281910*r^4*
           \[ScriptCapitalM]^2 + 759850*r^3*\[ScriptCapitalM]^3 - 
          1691200*r^2*\[ScriptCapitalM]^4 + 5071500*r*\[ScriptCapitalM]^5 - 
          18130000*\[ScriptCapitalM]^6)*Q[1 + \[ScriptL], m]^2))/
      (588000*r^11*(r - 2*\[ScriptCapitalM])^3) + 
     (\[ScriptCapitalM]^2*\[Zeta]^3*(29563813125*r^14 - 
        29563813125*r^13*\[ScriptCapitalM] + 710018563254*r^12*
         \[ScriptCapitalM]^2 - 799353770754*r^11*\[ScriptCapitalM]^3 - 
        5846196516732*r^10*\[ScriptCapitalM]^4 + 22386224892768*r^9*
         \[ScriptCapitalM]^5 - 52644244906880*r^8*\[ScriptCapitalM]^6 + 
        42337424822880*r^7*\[ScriptCapitalM]^7 + 255770684489600*r^6*
         \[ScriptCapitalM]^8 - 1604821718976000*r^5*\[ScriptCapitalM]^9 + 
        4332900092774400*r^4*\[ScriptCapitalM]^10 - 5116071750048000*r^3*
         \[ScriptCapitalM]^11 + 4114769568448000*r^2*\[ScriptCapitalM]^12 - 
        7757684014080000*r*\[ScriptCapitalM]^13 + 7225035417600000*
         \[ScriptCapitalM]^14 - 12*(r - 2*\[ScriptCapitalM])^3*
         \[ScriptCapitalM]^2*(70119165501*r^9 + 350595827505*r^8*
           \[ScriptCapitalM] - 38868188040*r^7*\[ScriptCapitalM]^2 + 
          1074194502150*r^6*\[ScriptCapitalM]^3 - 898189014800*r^5*
           \[ScriptCapitalM]^4 - 15196791733200*r^4*\[ScriptCapitalM]^5 + 
          35340820368000*r^3*\[ScriptCapitalM]^6 - 115455792144000*r^2*
           \[ScriptCapitalM]^7 - 81540264960000*r*\[ScriptCapitalM]^8 - 
          225782356800000*\[ScriptCapitalM]^9)*Q[\[ScriptL], m]^2 - 
        12*(r - 2*\[ScriptCapitalM])^3*\[ScriptCapitalM]^2*
         (70119165501*r^9 + 350595827505*r^8*\[ScriptCapitalM] - 
          38868188040*r^7*\[ScriptCapitalM]^2 + 1074194502150*r^6*
           \[ScriptCapitalM]^3 - 898189014800*r^5*\[ScriptCapitalM]^4 - 
          15196791733200*r^4*\[ScriptCapitalM]^5 + 35340820368000*r^3*
           \[ScriptCapitalM]^6 - 115455792144000*r^2*\[ScriptCapitalM]^7 - 
          81540264960000*r*\[ScriptCapitalM]^8 - 225782356800000*
           \[ScriptCapitalM]^9)*Q[1 + \[ScriptL], m]^2))/
      (1140955200000*r^14*(r - 2*\[ScriptCapitalM])^3) + 
     (\[ScriptCapitalM]^2*\[Zeta]^4*(7411792533496422946875*r^18 - 
        22235377600489268840625*r^17*\[ScriptCapitalM] + 
        178502589762516434637222*r^16*\[ScriptCapitalM]^2 - 
        573542488673418065717916*r^15*\[ScriptCapitalM]^3 - 
        616839178005773973825432*r^14*\[ScriptCapitalM]^4 + 
        5669669881221730727361876*r^13*\[ScriptCapitalM]^5 - 
        14821111670521732800741088*r^12*\[ScriptCapitalM]^6 + 
        38245063658431535950135520*r^11*\[ScriptCapitalM]^7 - 
        68398450872509985595442880*r^10*\[ScriptCapitalM]^8 + 
        49642876336154633457598400*r^9*\[ScriptCapitalM]^9 - 
        442011274598030017130252800*r^8*\[ScriptCapitalM]^10 + 
        3128957291517773848566201600*r^7*\[ScriptCapitalM]^11 - 
        10519985134593905961422208000*r^6*\[ScriptCapitalM]^12 + 
        24422045189859773627415552000*r^5*\[ScriptCapitalM]^13 - 
        50107065708768154250250240000*r^4*\[ScriptCapitalM]^14 + 
        104696617427695000322334720000*r^3*\[ScriptCapitalM]^15 - 
        186924090719412910428364800000*r^2*\[ScriptCapitalM]^16 + 
        204069113303028566681088000000*r*\[ScriptCapitalM]^17 - 
        93304747841752686919680000000*\[ScriptCapitalM]^18 - 
        6*(r - 2*\[ScriptCapitalM])^4*\[ScriptCapitalM]^2*
         (31841791486108042299861*r^12 + 159208957430540211499305*r^11*
           \[ScriptCapitalM] + 8412228766139457247560*r^10*
           \[ScriptCapitalM]^2 + 167000378077951616848650*r^9*
           \[ScriptCapitalM]^3 - 1715759510541126192942800*r^8*
           \[ScriptCapitalM]^4 - 674741103006751790209200*r^7*
           \[ScriptCapitalM]^5 + 5976755417397071890128000*r^6*
           \[ScriptCapitalM]^6 + 41985948901069384146936000*r^5*
           \[ScriptCapitalM]^7 - 111189600129161610524160000*r^4*
           \[ScriptCapitalM]^8 + 176982863536533084750720000*r^3*
           \[ScriptCapitalM]^9 - 575225786722959045273600000*r^2*
           \[ScriptCapitalM]^10 + 733124987930439808704000000*r*
           \[ScriptCapitalM]^11 - 2915773370054771466240000000*
           \[ScriptCapitalM]^12)*Q[\[ScriptL], m]^2 - 
        6*(r - 2*\[ScriptCapitalM])^4*\[ScriptCapitalM]^2*
         (31841791486108042299861*r^12 + 159208957430540211499305*r^11*
           \[ScriptCapitalM] + 8412228766139457247560*r^10*
           \[ScriptCapitalM]^2 + 167000378077951616848650*r^9*
           \[ScriptCapitalM]^3 - 1715759510541126192942800*r^8*
           \[ScriptCapitalM]^4 - 674741103006751790209200*r^7*
           \[ScriptCapitalM]^5 + 5976755417397071890128000*r^6*
           \[ScriptCapitalM]^6 + 41985948901069384146936000*r^5*
           \[ScriptCapitalM]^7 - 111189600129161610524160000*r^4*
           \[ScriptCapitalM]^8 + 176982863536533084750720000*r^3*
           \[ScriptCapitalM]^9 - 575225786722959045273600000*r^2*
           \[ScriptCapitalM]^10 + 733124987930439808704000000*r*
           \[ScriptCapitalM]^11 - 2915773370054771466240000000*
           \[ScriptCapitalM]^12)*Q[1 + \[ScriptL], m]^2))/
      (271633408521995520000000*r^17*(r - 2*\[ScriptCapitalM])^4))}}
