{((-2*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*(-1 + B[r])*B[r]*
    Derivative[1][H1][r])/(r^2*A[r]) - 
  (B[r]*Derivative[1][H2][r]*(E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][A][r] + r^2*A[r]*Derivative[1][\[CurlyPhi]][r]))/
   (2*r^2*A[r]) - (I*\[Omega]*H1[r]*(E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][B][r] + r^2*B[r]*Derivative[1][\[CurlyPhi]][r]))/
   (r^2*A[r]) + (Derivative[1][H0][r]*(-2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      Derivative[1][A][r] + E^\[CurlyPhi][r]*\[Alpha]*A[r]*
      Derivative[1][B][r] + B[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][A][r] - A[r]*(3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r] + r^2*Derivative[1][\[CurlyPhi]][r]))))/
   (2*r^2*A[r]) + ((B[r]*Derivative[1][A][r] + A[r]*Derivative[1][B][r])*
    Derivative[1][\[CurlyPhi]1][r])/(2*r*A[r]) + 
  (E^\[CurlyPhi][r]*\[Alpha]*K[r]*
    (A[r]*(4*r*\[Omega]^2 + 2*Derivative[1][A][r])*Derivative[1][B][r] - 
     2*B[r]*(Derivative[1][A][r]^2 - 2*A[r]*Derivative[2][A][r])))/
   (4*r^2*A[r]^2) + (B[r]*Derivative[1][K][r]*
    (-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2) + 
     2*r^2*A[r]^2*Derivative[1][\[CurlyPhi]][r] + E^\[CurlyPhi][r]*\[Alpha]*
      A[r]*(3*r*Derivative[1][A][r]*Derivative[1][B][r] + 
       2*B[r]*(2*Derivative[1][A][r] + r*Derivative[2][A][r]))))/
   (2*r^2*A[r]^2) - (\[CurlyPhi]1[r]*(E^\[CurlyPhi][r]*\[Alpha]*(-1 + B[r])*
      B[r]*Derivative[1][A][r]^2 + r*A[r]^2*Derivative[1][B][r] + 
     A[r]*(-2*r^2*\[Omega]^2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
        Derivative[1][B][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        Derivative[2][A][r] + B[r]*(Derivative[1][A][r]*
          (r - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]) + 
         2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]))))/(2*r^3*A[r]^2) + 
  (\[ScriptL]*(1 + \[ScriptL])*((-2*\[CurlyPhi]1[r])/r^3 - 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*Derivative[1][A][r])/(r^3*A[r]) + 
     (E^\[CurlyPhi][r]*\[Alpha]*H0[r]*Derivative[1][B][r])/r^3 + 
     (E^\[CurlyPhi][r]*\[Alpha]*K[r]*
       (-(A[r]*Derivative[1][A][r]*Derivative[1][B][r]) + 
        B[r]*(Derivative[1][A][r]^2 - 2*A[r]*Derivative[2][A][r])))/
      (2*r^2*A[r]^2)))/2 - (E^\[CurlyPhi][r]*\[Alpha]*(-1 + B[r])*B[r]*
    Derivative[2][H0][r])/r^2 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
    Derivative[1][A][r]*Derivative[2][K][r])/(r*A[r]) + 
  (H2[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*(-1 + 2*B[r])*
      Derivative[1][A][r]^2 + 
     A[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*(2*\[Omega]^2 - Derivative[1][A][r]*
           Derivative[1][B][r])) - 4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        Derivative[2][A][r] + 
       B[r]*(-(Derivative[1][A][r]*(6*E^\[CurlyPhi][r]*r*\[Alpha]*
             Derivative[1][B][r] + r^3*Derivative[1][\[CurlyPhi]][r])) + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*(\[Omega]^2 + Derivative[2][A][r]))) - 
     r^2*A[r]^2*(r*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*B[r]*(2*Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[2][\[CurlyPhi]][r]))))/(2*r^3*A[r]^2) + 
  (B[r]*Derivative[2][\[CurlyPhi]1][r])/r, 
 (H0[r]*(-(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*Derivative[1][A][r]*
       Derivative[1][wi[1][1]][r]^2) + E^\[CurlyPhi][r]*r*\[Alpha]*A[r]*B[r]*
      (4*(-1 + B[r])*Derivative[1][A][r]*(2*Derivative[1][hi[2][0]][r] - 
         Derivative[1][hi[2][2]][r]) + r^2*Derivative[1][wi[1][1]][r]*
        ((6 + 6*B[r] + 3*r*Derivative[1][B][r])*Derivative[1][wi[1][1]][r] + 
         4*r*B[r]*Derivative[2][wi[1][1]][r])) + 
     A[r]^2*(2*B[r]*(r^3*Derivative[1][\[CurlyPhi]][r]*
          (2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][r]) + 
         E^\[CurlyPhi][r]*\[Alpha]*(-4*r*Derivative[2][hi[2][0]][r] + 
           4*r*B[r]*Derivative[2][hi[2][0]][r] + 2*r*Derivative[2][hi[2][2]][
             r] - 2*r*B[r]*Derivative[2][hi[2][2]][r])) + 
       E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        (4*r*(-1 + 3*B[r])*Derivative[1][hi[2][0]][r] + 
         (2*r - 6*r*B[r])*Derivative[1][hi[2][2]][r] + 12*hi[2][2][r]))))/
   (4*r^3*A[r]^2) + (B[r]*Derivative[2][\[CurlyPhi]1][r]*
    (-2*pi[2][0][r] + pi[2][2][r]))/r + 
  (Derivative[1][\[CurlyPhi]1][r]*(B[r]*Derivative[1][A][r]*
      (-2*pi[2][0][r] + pi[2][2][r]) + 
     A[r]*(B[r]*(2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][r] + 
         4*Derivative[1][ki[2][0]][r] - 2*Derivative[1][ki[2][2]][r] - 
         2*Derivative[1][pi[2][0]][r] + Derivative[1][pi[2][2]][r]) + 
       Derivative[1][B][r]*(-2*pi[2][0][r] + pi[2][2][r]))))/(2*r*A[r]) + 
  (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[2][K][r]*
    (4*A[r]*(r*B[r]*(2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][
          r]) + 3*hi[2][2][r]) + r*(4*r*wi[1][1][r]^2 + 
       B[r]*(r*(r^2*Derivative[1][wi[1][1]][r]^2 - 
           4*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 4*wi[1][1][r]^2) + 
         2*Derivative[1][A][r]*(2*r*Derivative[1][ki[2][0]][r] - 
           r*Derivative[1][ki[2][2]][r] - 4*ki[2][0][r] + 2*ki[2][2][r] - 
           8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r])))))/(4*r^2*A[r]) - 
  (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][H1][r]*
    (4*hi[2][0][r] - 2*hi[2][2][r] + 4*ki[2][0][r] + 4*ki[2][2][r] + 
     4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
     B[r]*(4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][r] - 
       4*hi[2][0][r] + 2*hi[2][2][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
       2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]) + 
     \[CurlyPhi]i[2][2][r]))/(r^2*A[r]) - 
  (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[2][H0][r]*
    (4*hi[2][0][r] - 2*hi[2][2][r] + 4*ki[2][0][r] + 4*ki[2][2][r] + 
     4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
     B[r]*(4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][r] - 
       4*hi[2][0][r] + 2*hi[2][2][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
       2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]) + 
     \[CurlyPhi]i[2][2][r]))/(2*r^2) - 
  (B[r]*Derivative[1][H2][r]*
    (A[r]*(4*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
        Derivative[1][hi[2][0]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        (-1 + 3*B[r])*Derivative[1][hi[2][2]][r] + 
       2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
       r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] + 12*E^\[CurlyPhi][r]*
        \[Alpha]*hi[2][2][r] - 8*r^3*Derivative[1][\[CurlyPhi]][r]*
        pi[2][0][r] + 4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r]) + 
     E^\[CurlyPhi][r]*r*\[Alpha]*(3*B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 + 
         Derivative[1][A][r]*(4*r*Derivative[1][ki[2][0]][r] - 
           2*r*Derivative[1][ki[2][2]][r] - 12*pi[2][0][r] + 6*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r])) + 
       Derivative[1][A][r]*(4*ki[2][0][r] + 4*ki[2][2][r] + 8*pi[2][0][r] - 
         4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r]))))/
   (4*r^3*A[r]) - ((I/2)*\[Omega]*H1[r]*(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*
      Derivative[1][wi[1][1]][r]^2 + 
     A[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (8*Derivative[1][ki[2][0]][r] - 4*Derivative[1][ki[2][2]][r] - 
         6*Derivative[1][pi[2][0]][r] + 3*Derivative[1][pi[2][2]][r] + 
         4*r*Derivative[2][ki[2][0]][r] - 2*r*Derivative[2][ki[2][2]][r]) + 
       B[r]*(4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][pi[2][0]][r] - 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][pi[2][2]][r] + 
         2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         4*r^3*Derivative[1][\[CurlyPhi]][r]*hi[2][0][r] + 
         2*r^3*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 
         4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
         12*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] + 
         2*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          (4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][r] - 
           4*hi[2][0][r] + 2*hi[2][2][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r])) + 
       E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
        (4*hi[2][0][r] - 2*hi[2][2][r] + 4*ki[2][0][r] + 4*ki[2][2][r] + 
         4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
         \[CurlyPhi]i[2][2][r]))))/(r^3*A[r]^2) + 
  (Derivative[1][H0][r]*
    (-(A[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
         (-4*Derivative[1][hi[2][0]][r] + 2*Derivative[1][hi[2][2]][r] + 
          8*Derivative[1][ki[2][0]][r] - 4*Derivative[1][ki[2][2]][r] - 
          6*Derivative[1][pi[2][0]][r] + 3*Derivative[1][pi[2][2]][r] + 
          4*r*Derivative[2][ki[2][0]][r] - 2*r*Derivative[2][ki[2][2]][r]) + 
        E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
         (4*hi[2][0][r] - 2*hi[2][2][r] + 4*ki[2][0][r] + 4*ki[2][2][r] + 
          4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
          \[CurlyPhi]i[2][2][r]) + B[r]*(8*E^\[CurlyPhi][r]*r*\[Alpha]*
           Derivative[1][hi[2][0]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
           Derivative[1][hi[2][2]][r] + 12*E^\[CurlyPhi][r]*r^2*\[Alpha]*
           Derivative[1][B][r]*Derivative[1][ki[2][0]][r] - 
          6*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][ki[2][2]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
           Derivative[1][pi[2][0]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
           Derivative[1][pi[2][2]][r] + 
          2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
          r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 12*E^\[CurlyPhi][r]*r*
           \[Alpha]*Derivative[1][B][r]*hi[2][0][r] - 
          4*r^3*Derivative[1][\[CurlyPhi]][r]*hi[2][0][r] + 
          6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*hi[2][2][r] + 
          2*r^3*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 
          24*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*pi[2][0][r] - 
          4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
          12*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] + 12*E^\[CurlyPhi][r]*r*
           \[Alpha]*Derivative[1][B][r]*pi[2][2][r] + 
          2*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
          6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
           \[CurlyPhi]i[2][0][r] - 3*E^\[CurlyPhi][r]*r*\[Alpha]*
           Derivative[1][B][r]*\[CurlyPhi]i[2][2][r]))) + 
     E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*(-2*Derivative[1][A][r]*
        (4*hi[2][0][r] - 2*hi[2][2][r] + 4*ki[2][0][r] + 4*ki[2][2][r] + 
         4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
         \[CurlyPhi]i[2][2][r]) + B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 + 
         2*Derivative[1][A][r]*(-4*r*Derivative[1][ki[2][0]][r] + 
           2*r*Derivative[1][ki[2][2]][r] + 4*hi[2][0][r] - 2*hi[2][2][r] + 
           8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r])))))/(4*r^3*A[r]) + 
  (Derivative[1][K][r]*(4*A[r]^2*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (4*Derivative[1][hi[2][0]][r] - 2*Derivative[1][hi[2][2]][r] + 
         2*r*Derivative[2][hi[2][0]][r] - r*Derivative[2][hi[2][2]][r]) + 
       3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*hi[2][2][r] + 
       B[r]*(3*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
          (2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][r]) + 
         2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] + 12*E^\[CurlyPhi][r]*
          \[Alpha]*hi[2][2][r] - 4*r^3*Derivative[1][\[CurlyPhi]][r]*
          ki[2][0][r] + 2*r^3*Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] - 
         4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
         2*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r])) + 
     E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*Derivative[1][A][r]*
      (-8*r*wi[1][1][r]^2 + B[r]*(r*(-(r^2*Derivative[1][wi[1][1]][r]^2) + 
           4*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 8*wi[1][1][r]^2) + 
         2*Derivative[1][A][r]*(-2*r*Derivative[1][ki[2][0]][r] + 
           r*Derivative[1][ki[2][2]][r] + 4*ki[2][0][r] - 2*ki[2][2][r] + 
           8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r]))) + 
     r*A[r]*(4*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
        wi[1][1][r]^2 + 
       B[r]*(r*(4*wi[1][1][r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][
                wi[1][1]][r] + (8*E^\[CurlyPhi][r]*\[Alpha] - r^3*
                Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) + 
           3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            (r^2*Derivative[1][wi[1][1]][r]^2 - 4*r*Derivative[1][wi[1][1]][
               r]*wi[1][1][r] - 4*wi[1][1][r]^2)) + 6*E^\[CurlyPhi][r]*
          \[Alpha]*Derivative[1][A][r]*(2*pi[2][2][r] + r*Derivative[1][B][r]*
            (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
             4*ki[2][0][r] + 2*ki[2][2][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
             2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]))) + 
       4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][A][r]*
          (4*r*Derivative[1][hi[2][0]][r] - 2*r*Derivative[1][hi[2][2]][r] + 
           4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][r] - 
           6*r*Derivative[1][pi[2][0]][r] + 3*r*Derivative[1][pi[2][2]][r] + 
           2*r^2*Derivative[2][ki[2][0]][r] - r^2*Derivative[2][ki[2][2]][
             r] - 8*ki[2][0][r] + 4*ki[2][2][r] - 16*pi[2][0][r] + 
           8*pi[2][2][r] + 4*\[CurlyPhi]i[2][0][r] - 
           2*\[CurlyPhi]i[2][2][r]) + r*(2*r*Derivative[1][ki[2][0]][r]*
            Derivative[2][A][r] - r*Derivative[1][ki[2][2]][r]*
            Derivative[2][A][r] + r^3*Derivative[1][wi[1][1]][r]*
            Derivative[2][wi[1][1]][r] - 4*Derivative[2][A][r]*ki[2][0][r] + 
           2*Derivative[2][A][r]*ki[2][2][r] - 8*Derivative[2][A][r]*
            pi[2][0][r] + 4*Derivative[2][A][r]*pi[2][2][r] - 
           14*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
           2*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] - 8*wi[1][1][r]^2 + 
           2*Derivative[2][A][r]*\[CurlyPhi]i[2][0][r] - Derivative[2][A][r]*
            \[CurlyPhi]i[2][2][r])))))/(8*r^3*A[r]^2) + 
  (H2[r]*(A[r]^2*(-8*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (2*Derivative[2][hi[2][0]][r] - Derivative[2][hi[2][2]][r]) + 
       Derivative[1][B][r]*(4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][hi[2][0]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][hi[2][2]][r] - 
         2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
         r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 12*E^\[CurlyPhi][r]*
          \[Alpha]*hi[2][2][r] + 8*r^3*Derivative[1][\[CurlyPhi]][r]*
          pi[2][0][r] - 4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r]) - 
       2*B[r]*(2*(6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][r] - 
         (6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
         r*(4*r*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 4*E^\[CurlyPhi][r]*
            \[Alpha]*Derivative[2][hi[2][0]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[2][hi[2][2]][r] + 2*r^2*Derivative[2][\[CurlyPhi]i[
                2][0]][r] - r^2*Derivative[2][\[CurlyPhi]i[2][2]][r] - 
           8*r^2*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] + 
           4*r^2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
           2*r*Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][ki[2][0]][
               r] - r*Derivative[1][ki[2][2]][r] - 2*r*Derivative[1][
                pi[2][0]][r] + r*Derivative[1][pi[2][2]][r] - 8*pi[2][0][r] + 
             4*pi[2][2][r])))) + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
      Derivative[1][A][r]*(2*B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 + 
         Derivative[1][A][r]*(4*r*Derivative[1][ki[2][0]][r] - 
           2*r*Derivative[1][ki[2][2]][r] - 12*pi[2][0][r] + 6*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r])) + 
       Derivative[1][A][r]*(4*ki[2][0][r] + 4*ki[2][2][r] + 8*pi[2][0][r] - 
         4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) - 
     A[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (6*r^2*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
          (8*Derivative[1][hi[2][0]][r] - 4*Derivative[1][hi[2][2]][r] + 
           16*Derivative[1][ki[2][0]][r] - 8*Derivative[1][ki[2][2]][r] - 
           18*Derivative[1][pi[2][0]][r] + 9*Derivative[1][pi[2][2]][r] + 
           8*r*Derivative[2][ki[2][0]][r] - 4*r*Derivative[2][ki[2][2]][r]) + 
         4*r^3*Derivative[1][wi[1][1]][r]*Derivative[2][wi[1][1]][r] + 
         2*Derivative[2][A][r]*(4*r*Derivative[1][ki[2][0]][r] - 
           2*r*Derivative[1][ki[2][2]][r] - 12*pi[2][0][r] + 6*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r])) - 
       E^\[CurlyPhi][r]*r*\[Alpha]*(8*\[Omega]^2*hi[2][0][r] - 
         4*\[Omega]^2*hi[2][2][r] + 8*\[Omega]^2*ki[2][0][r] - 
         4*Derivative[1][A][r]*Derivative[1][B][r]*ki[2][0][r] + 
         8*\[Omega]^2*ki[2][2][r] - 4*Derivative[1][A][r]*Derivative[1][B][r]*
          ki[2][2][r] + 8*\[Omega]^2*pi[2][0][r] - 8*Derivative[1][A][r]*
          Derivative[1][B][r]*pi[2][0][r] - 4*\[Omega]^2*pi[2][2][r] + 
         4*Derivative[1][A][r]*Derivative[1][B][r]*pi[2][2][r] - 
         4*\[Omega]^2*\[CurlyPhi]i[2][0][r] + 2*Derivative[1][A][r]*
          Derivative[1][B][r]*\[CurlyPhi]i[2][0][r] + 
         2*\[Omega]^2*\[CurlyPhi]i[2][2][r] - Derivative[1][A][r]*
          Derivative[1][B][r]*\[CurlyPhi]i[2][2][r]) + 
       B[r]*(Derivative[1][A][r]*(-8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][hi[2][0]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][hi[2][2]][r] + 24*E^\[CurlyPhi][r]*r^2*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][ki[2][0]][r] - 
           12*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][ki[2][2]][r] + 8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][pi[2][0]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][pi[2][2]][r] + 2*r^3*Derivative[1][\[CurlyPhi]i[
                2][0]][r] - r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
           72*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*pi[2][0][r] - 
           8*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
           24*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] + 36*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][B][r]*pi[2][2][r] + 
           4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
           12*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            \[CurlyPhi]i[2][0][r] - 6*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][B][r]*\[CurlyPhi]i[2][2][r]) + 
         E^\[CurlyPhi][r]*r*\[Alpha]*(-8*r*\[Omega]^2*Derivative[1][ki[2][0]][
             r] + 4*r*\[Omega]^2*Derivative[1][ki[2][2]][r] + 
           6*r^2*Derivative[1][wi[1][1]][r]^2 + 6*r^3*Derivative[1][B][r]*
            Derivative[1][wi[1][1]][r]^2 + 8*\[Omega]^2*hi[2][0][r] - 
           4*\[Omega]^2*hi[2][2][r] + 8*Derivative[2][A][r]*ki[2][0][r] + 
           8*Derivative[2][A][r]*ki[2][2][r] + 16*\[Omega]^2*pi[2][0][r] + 
           16*Derivative[2][A][r]*pi[2][0][r] - 8*\[Omega]^2*pi[2][2][r] - 
           8*Derivative[2][A][r]*pi[2][2][r] - 4*\[Omega]^2*
            \[CurlyPhi]i[2][0][r] - 4*Derivative[2][A][r]*\[CurlyPhi]i[2][0][
             r] + 2*\[Omega]^2*\[CurlyPhi]i[2][2][r] + 2*Derivative[2][A][r]*
            \[CurlyPhi]i[2][2][r])))))/(4*r^3*A[r]^2) + 
  (K[r]*(-8*E^\[CurlyPhi][r]*r^3*\[Alpha]*(-1 + B[r])*B[r]*
      Derivative[1][A][r]^2*wi[1][1][r]^2 - 
     2*A[r]^3*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        (-4*r*Derivative[1][hi[2][0]][r] + 2*r*Derivative[1][hi[2][2]][r] + 
         12*hi[2][2][r]) + 2*r*(B[r]*(2*r^2*Derivative[1][\[CurlyPhi]][r]*
            (2*Derivative[1][ki[2][0]][r] - Derivative[1][ki[2][2]][r]) - 
           2*E^\[CurlyPhi][r]*\[Alpha]*(2*Derivative[2][hi[2][0]][r] - 
             Derivative[2][hi[2][2]][r])) + 6*\[CurlyPhi]i[2][2][r])) + 
     A[r]^2*(-4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (7*r^2*Derivative[1][wi[1][1]][r]^2 + 4*r*Derivative[1][ki[2][0]][r]*
          Derivative[2][A][r] - 2*r*Derivative[1][ki[2][2]][r]*
          Derivative[2][A][r] + Derivative[1][A][r]*
          (8*Derivative[1][ki[2][0]][r] - 4*Derivative[1][ki[2][2]][r] + 
           4*r*Derivative[2][ki[2][0]][r] - 2*r*Derivative[2][ki[2][2]][r]) + 
         4*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] + 4*wi[1][1][r]^2 + 
         2*Derivative[1][wi[1][1]][r]*(r^3*Derivative[2][wi[1][1]][r] + 
           8*r*wi[1][1][r])) + E^\[CurlyPhi][r]*r*\[Alpha]*
        (24*\[Omega]^2*pi[2][2][r] + Derivative[1][B][r]*
          (8*r^2*\[Omega]^2*Derivative[1][ki[2][0]][r] - 4*r^2*\[Omega]^2*
            Derivative[1][ki[2][2]][r] - 16*r*\[Omega]^2*hi[2][0][r] + 
           8*r*\[Omega]^2*hi[2][2][r] - 16*r*\[Omega]^2*ki[2][0][r] - 
           16*Derivative[1][A][r]*ki[2][0][r] + 8*r*\[Omega]^2*ki[2][2][r] - 
           16*Derivative[1][A][r]*ki[2][2][r] - 16*r*\[Omega]^2*pi[2][0][r] - 
           8*Derivative[1][A][r]*pi[2][0][r] + 8*r*\[Omega]^2*pi[2][2][r] + 
           4*Derivative[1][A][r]*pi[2][2][r] + 8*r^2*Derivative[1][wi[1][1]][
             r]*wi[1][1][r] + 16*r*wi[1][1][r]^2 + 8*r*\[Omega]^2*
            \[CurlyPhi]i[2][0][r] + 4*Derivative[1][A][r]*\[CurlyPhi]i[2][0][
             r] - 4*r*\[Omega]^2*\[CurlyPhi]i[2][2][r] - 
           2*Derivative[1][A][r]*\[CurlyPhi]i[2][2][r])) - 
       2*B[r]*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
          (-8*r*Derivative[1][hi[2][0]][r] + 4*r*Derivative[1][hi[2][2]][r] + 
           12*r^2*Derivative[1][B][r]*Derivative[1][ki[2][0]][r] - 
           6*r^2*Derivative[1][B][r]*Derivative[1][ki[2][2]][r] + 
           4*r*Derivative[1][pi[2][0]][r] - 2*r*Derivative[1][pi[2][2]][r] + 
           12*pi[2][2][r]) + r*(-16*E^\[CurlyPhi][r]*r*\[Alpha]*\[Omega]^2*
            Derivative[1][ki[2][0]][r] + 8*E^\[CurlyPhi][r]*r*\[Alpha]*
            \[Omega]^2*Derivative[1][ki[2][2]][r] + 8*E^\[CurlyPhi][r]*r*
            \[Alpha]*\[Omega]^2*Derivative[1][pi[2][0]][r] - 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*\[Omega]^2*Derivative[1][pi[2][2]][
             r] + 4*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][wi[1][1]][r]^
             2 + 3*E^\[CurlyPhi][r]*r^3*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][wi[1][1]][r]^2 - 8*E^\[CurlyPhi][r]*r^2*\[Alpha]*
            \[Omega]^2*Derivative[2][ki[2][0]][r] + 4*E^\[CurlyPhi][r]*r^2*
            \[Alpha]*\[Omega]^2*Derivative[2][ki[2][2]][r] + 
           16*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*ki[2][0][r] + 
           16*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*ki[2][2][r] + 
           8*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*pi[2][0][r] - 
           4*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*pi[2][2][r] - 
           32*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + 12*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][
             r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
           4*r^4*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] - 8*E^\[CurlyPhi][r]*r^2*\[Alpha]*
            Derivative[2][wi[1][1]][r]*wi[1][1][r] - 8*E^\[CurlyPhi][r]*
            \[Alpha]*wi[1][1][r]^2 + 12*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][B][r]*wi[1][1][r]^2 + 
           4*r^3*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]^2 - 
           4*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*\[CurlyPhi]i[2][0][
             r] + 2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*
            \[CurlyPhi]i[2][2][r]))) + 
     r*A[r]*(-4*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][A][r]*
        Derivative[1][B][r]*wi[1][1][r]^2 + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        B[r]^2*(Derivative[1][A][r]^2*(4*Derivative[1][ki[2][0]][r] - 
           2*Derivative[1][ki[2][2]][r]) + 4*r*Derivative[2][A][r]*
          wi[1][1][r]^2 + Derivative[1][A][r]*
          (r^2*Derivative[1][wi[1][1]][r]^2 + 8*r*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + 8*wi[1][1][r]^2)) + 
       B[r]*(2*E^\[CurlyPhi][r]*r^4*\[Alpha]*\[Omega]^2*
          Derivative[1][wi[1][1]][r]^2 - 16*E^\[CurlyPhi][r]*r^2*\[Alpha]*
          Derivative[1][A][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
         4*r*Derivative[1][A][r]*(-4*E^\[CurlyPhi][r]*\[Alpha] + 
           3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]^2 - 
         8*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[2][A][r]*wi[1][1][r]^2 - 
         E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]^2*(-16*ki[2][0][r] - 
           16*ki[2][2][r] - 2*(4*pi[2][0][r] - 2*pi[2][2][r] - 
             2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r]))))))/
   (8*r^3*A[r]^3) + (\[ScriptL]*(1 + \[ScriptL])*
    ((\[CurlyPhi]1[r]*(4*ki[2][0][r] - 2*ki[2][2][r]))/r^3 + 
     (E^\[CurlyPhi][r]*\[Alpha]*H0[r]*(r^3*B[r]*Derivative[1][wi[1][1]][r]^
          2 + A[r]*(2*B[r]*(4*Derivative[1][ki[2][0]][r] - 
            2*Derivative[1][ki[2][2]][r] - 2*Derivative[1][pi[2][0]][r] + 
            Derivative[1][pi[2][2]][r] + 2*r*Derivative[2][ki[2][0]][r] - 
            r*Derivative[2][ki[2][2]][r]) + Derivative[1][B][r]*
           (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
            4*hi[2][0][r] + 2*hi[2][2][r] - 4*ki[2][0][r] + 2*ki[2][2][r] - 
            4*pi[2][0][r] + 2*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
            \[CurlyPhi]i[2][2][r]))))/(2*r^3*A[r]) - 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*
       (A[r]*(4*Derivative[1][hi[2][0]][r] - 2*Derivative[1][hi[2][2]][r]) + 
        r^3*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
         (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
          4*ki[2][0][r] + 2*ki[2][2][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
          2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r])))/(2*r^3*A[r]) + 
     (E^\[CurlyPhi][r]*\[Alpha]*K[r]*
       (A[r]^2*(Derivative[1][B][r]*(-4*Derivative[1][hi[2][0]][r] + 
            2*Derivative[1][hi[2][2]][r]) + 4*B[r]*
           (-2*Derivative[2][hi[2][0]][r] + Derivative[2][hi[2][2]][r])) - 
        B[r]*Derivative[1][A][r]*(4*r^2*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] + Derivative[1][A][r]*(8*ki[2][0][r] - 4*ki[2][2][r] + 
            4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
            \[CurlyPhi]i[2][2][r])) + 
        A[r]*(2*B[r]*(-(Derivative[1][A][r]*(4*Derivative[1][hi[2][0]][r] - 2*
                Derivative[1][hi[2][2]][r] - 2*Derivative[1][pi[2][0]][r] + 
               Derivative[1][pi[2][2]][r])) + 3*r^2*Derivative[1][wi[1][1]][
               r]^2 + 8*Derivative[2][A][r]*ki[2][0][r] - 
            4*Derivative[2][A][r]*ki[2][2][r] + 4*Derivative[2][A][r]*
             pi[2][0][r] - 2*Derivative[2][A][r]*pi[2][2][r] + 
            12*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
            4*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] - 
            2*Derivative[2][A][r]*\[CurlyPhi]i[2][0][r] + Derivative[2][A][r]*
             \[CurlyPhi]i[2][2][r]) + Derivative[1][B][r]*
           (4*r*wi[1][1][r]*(r*Derivative[1][wi[1][1]][r] + wi[1][1][r]) + 
            Derivative[1][A][r]*(8*ki[2][0][r] - 4*ki[2][2][r] + 
              4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
              \[CurlyPhi]i[2][2][r])))))/(4*r^2*A[r]^2)))/2 + 
  (\[CurlyPhi]1[r]*(-2*A[r]^2*(-2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (2*Derivative[2][hi[2][0]][r] - Derivative[2][hi[2][2]][r]) + 
       r*B[r]*(2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r])*
          Derivative[1][hi[2][0]][r] - (r - 3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r])*Derivative[1][hi[2][2]][r] + 
         4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][r] - 
         2*r*Derivative[1][pi[2][0]][r] + r*Derivative[1][pi[2][2]][r] + 
         4*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][hi[2][0]][r] - 
         2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][hi[2][2]][r]) + 
       Derivative[1][B][r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][hi[2][0]][r] - E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][hi[2][2]][r] - 6*E^\[CurlyPhi][r]*\[Alpha]*
          hi[2][2][r] - 2*r^2*pi[2][0][r] + r^2*pi[2][2][r])) - 
     E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]*
      (B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
          (4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][r] - 
           8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r])) + Derivative[1][A][r]*
        (4*ki[2][0][r] + 4*ki[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r] - 
         2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) + 
     A[r]*(-8*r^3*\[Omega]^2*hi[2][0][r] + 4*r^3*\[Omega]^2*hi[2][2][r] + 
       2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (3*r^2*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
          (4*Derivative[1][hi[2][0]][r] - 2*Derivative[1][hi[2][2]][r] + 
           8*Derivative[1][ki[2][0]][r] - 4*Derivative[1][ki[2][2]][r] - 
           6*Derivative[1][pi[2][0]][r] + 3*Derivative[1][pi[2][2]][r] + 
           4*r*Derivative[2][ki[2][0]][r] - 2*r*Derivative[2][ki[2][2]][r]) + 
         2*r^3*Derivative[1][wi[1][1]][r]*Derivative[2][wi[1][1]][r] + 
         Derivative[2][A][r]*(4*r*Derivative[1][ki[2][0]][r] - 
           2*r*Derivative[1][ki[2][2]][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r])) + 
       E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][A][r]*Derivative[1][B][r]*
        (4*ki[2][0][r] + 4*ki[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r] - 
         2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r]) + 
       B[r]*(Derivative[1][A][r]*(-8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][hi[2][0]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][hi[2][2]][r] + 12*E^\[CurlyPhi][r]*r^2*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][ki[2][0]][r] - 
           6*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][ki[2][2]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][pi[2][0]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][pi[2][2]][r] + 4*r^2*pi[2][0][r] - 
           24*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*pi[2][0][r] - 
           2*r^2*pi[2][2][r] + 12*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] + 
           12*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*pi[2][2][r] + 
           6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            \[CurlyPhi]i[2][0][r] - 3*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][B][r]*\[CurlyPhi]i[2][2][r]) + 
         E^\[CurlyPhi][r]*r*\[Alpha]*(3*r^2*(2 + r*Derivative[1][B][r])*
            Derivative[1][wi[1][1]][r]^2 + 2*Derivative[2][A][r]*
            (4*ki[2][0][r] + 4*ki[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r] - 
             2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r]))))))/
   (4*r^4*A[r]^2), (\[ScriptL]*(1 + \[ScriptL])*
   (((-6*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*h1[r]*
      Derivative[1][wi[1][1]][r])/(r^2*A[r]) - 
    (6*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][h0][r]*
      Derivative[1][wi[1][1]][r])/(r^2*A[r]) + 
    (4*E^\[CurlyPhi][r]*\[Alpha]*h0[r]*(3*B[r]*Derivative[1][wi[1][1]][r] + 
       Derivative[1][B][r]*wi[1][1][r]))/(r^3*A[r])))/2, 
 (H0[r]*(E^\[CurlyPhi][r]*r^5*\[Alpha]*B[r]^2*Derivative[1][A][r]*
      Derivative[1][wi[1][1]][r]^2 - E^\[CurlyPhi][r]*r^2*\[Alpha]*A[r]*B[r]*
      (-12*(-1 + B[r])*Derivative[1][A][r]*Derivative[1][hi[2][2]][r] + 
       r^2*Derivative[1][wi[1][1]][r]*
        ((18 + 6*B[r] + 3*r*Derivative[1][B][r])*Derivative[1][wi[1][1]][r] + 
         4*r*B[r]*Derivative[2][wi[1][1]][r])) + 
     3*A[r]^2*(4*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*Derivative[2][hi[2][2]][
         r] + 2*r*B[r]*(r^3*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][hi[2][2]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[2][hi[2][2]][r]) + E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][B][r]*(2*r*(-1 + 3*B[r])*Derivative[1][hi[2][2]][r] - 
         12*hi[2][2][r]))))/(4*r^4*A[r]^2) - 
  (3*B[r]*Derivative[2][\[CurlyPhi]1][r]*pi[2][2][r])/r + 
  (Derivative[1][\[CurlyPhi]1][r]*(-3*B[r]*Derivative[1][A][r]*pi[2][2][r] + 
     3*A[r]*(B[r]*(Derivative[1][hi[2][2]][r] + 
         2*Derivative[1][ki[2][2]][r] - Derivative[1][pi[2][2]][r]) - 
       Derivative[1][B][r]*pi[2][2][r])))/(2*r*A[r]) - 
  ((3*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][H1][r]*
    (2*hi[2][2][r] - 4*ki[2][2][r] + 2*pi[2][2][r] - \[CurlyPhi]i[2][2][r] + 
     B[r]*(2*r*Derivative[1][ki[2][2]][r] - 2*hi[2][2][r] - 4*pi[2][2][r] + 
       \[CurlyPhi]i[2][2][r])))/(r^2*A[r]) - 
  (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[2][H0][r]*
    (2*hi[2][2][r] - 4*ki[2][2][r] + 2*pi[2][2][r] - \[CurlyPhi]i[2][2][r] + 
     B[r]*(2*r*Derivative[1][ki[2][2]][r] - 2*hi[2][2][r] - 4*pi[2][2][r] + 
       \[CurlyPhi]i[2][2][r])))/(2*r^2) + 
  (3*B[r]*Derivative[1][H2][r]*
    (A[r]*(-2*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
        Derivative[1][hi[2][2]][r] - r^3*Derivative[1][\[CurlyPhi]i[2][2]][
         r] + 12*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] + 
       4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r]) + 
     E^\[CurlyPhi][r]*r*\[Alpha]*(Derivative[1][A][r]*(4*ki[2][2][r] - 
         4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]) + 
       B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 - 3*Derivative[1][A][r]*
          (2*r*Derivative[1][ki[2][2]][r] - 6*pi[2][2][r] + 
           \[CurlyPhi]i[2][2][r])))))/(4*r^3*A[r]) + 
  ((I/2)*\[Omega]*H1[r]*(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*
      Derivative[1][wi[1][1]][r]^2 - 
     3*A[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (4*Derivative[1][ki[2][2]][r] - 3*Derivative[1][pi[2][2]][r] + 
         2*r*Derivative[2][ki[2][2]][r]) + E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][B][r]*(2*hi[2][2][r] - 4*ki[2][2][r] + 2*pi[2][2][r] - 
         \[CurlyPhi]i[2][2][r]) + B[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][pi[2][2]][r] + r^3*Derivative[1][\[CurlyPhi]i[2][2]][
           r] - 2*r^3*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 
         12*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] - 
         2*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          (2*r*Derivative[1][ki[2][2]][r] - 2*hi[2][2][r] - 4*pi[2][2][r] + 
           \[CurlyPhi]i[2][2][r])))))/(r^3*A[r]^2) - 
  (Derivative[1][H0][r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
      (6*Derivative[1][A][r]*(2*hi[2][2][r] - 4*ki[2][2][r] + 2*pi[2][2][r] - 
         \[CurlyPhi]i[2][2][r]) + B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 + 
         6*Derivative[1][A][r]*(2*r*Derivative[1][ki[2][2]][r] - 
           2*hi[2][2][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))) + 
     3*A[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (-2*Derivative[1][hi[2][2]][r] + 4*Derivative[1][ki[2][2]][r] - 
         3*Derivative[1][pi[2][2]][r] + 2*r*Derivative[2][ki[2][2]][r]) + 
       E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
        (2*hi[2][2][r] - 4*ki[2][2][r] + 2*pi[2][2][r] - 
         \[CurlyPhi]i[2][2][r]) + B[r]*(4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][hi[2][2]][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][pi[2][2]][r] + r^3*Derivative[1][\[CurlyPhi]i[2][2]][
           r] - 2*r^3*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] - 
         12*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] - 
         2*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          (2*r*Derivative[1][ki[2][2]][r] - 2*hi[2][2][r] - 4*pi[2][2][r] + 
           \[CurlyPhi]i[2][2][r])))))/(4*r^3*A[r]) - 
  (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[2][K][r]*
    (-12*A[r]*(r*B[r]*Derivative[1][hi[2][2]][r] - 3*hi[2][2][r]) + 
     r*(4*r*wi[1][1][r]^2 + B[r]*(r*(r^2*Derivative[1][wi[1][1]][r]^2 - 
           4*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 4*wi[1][1][r]^2) - 
         6*Derivative[1][A][r]*(r*Derivative[1][ki[2][2]][r] - 
           2*ki[2][2][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/
   (4*r^2*A[r]) + (\[ScriptL]*(1 + \[ScriptL])*
    ((6*\[CurlyPhi]1[r]*ki[2][2][r])/r^3 + 
     (E^\[CurlyPhi][r]*\[Alpha]*H2[r]*
       (-6*A[r]*(r*B[r]*Derivative[1][hi[2][2]][r] - 2*hi[2][2][r]) + 
        r*B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 - 3*Derivative[1][A][r]*
           (r*Derivative[1][ki[2][2]][r] - 2*ki[2][2][r] - 4*pi[2][2][r] + 
            \[CurlyPhi]i[2][2][r]))))/(2*r^4*A[r]) - 
     (E^\[CurlyPhi][r]*\[Alpha]*H0[r]*(r^4*B[r]*Derivative[1][wi[1][1]][r]^
          2 - 3*A[r]*(2*r*B[r]*(2*Derivative[1][ki[2][2]][r] - 
            Derivative[1][pi[2][2]][r] + r*Derivative[2][ki[2][2]][r]) - 
          4*pi[2][2][r] + r*Derivative[1][B][r]*
           (r*Derivative[1][ki[2][2]][r] - 2*hi[2][2][r] - 2*ki[2][2][r] - 
            2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/(2*r^4*A[r]) - 
     (E^\[CurlyPhi][r]*\[Alpha]*K[r]*
       (6*A[r]^2*(Derivative[1][B][r]*Derivative[1][hi[2][2]][r] + 
          2*B[r]*Derivative[2][hi[2][2]][r]) + B[r]*Derivative[1][A][r]*
         (-4*r^2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
          3*Derivative[1][A][r]*(4*ki[2][2][r] + 2*pi[2][2][r] - 
            \[CurlyPhi]i[2][2][r])) + 
        A[r]*(Derivative[1][B][r]*(4*r*wi[1][1][r]*(r*Derivative[1][wi[1][1]][
                r] + wi[1][1][r]) - 3*Derivative[1][A][r]*(4*ki[2][2][r] + 
              2*pi[2][2][r] - \[CurlyPhi]i[2][2][r])) + 
          2*B[r]*(Derivative[1][A][r]*(6*Derivative[1][hi[2][2]][r] - 
              3*Derivative[1][pi[2][2]][r]) + 3*r^2*Derivative[1][wi[1][1]][
               r]^2 - 12*Derivative[2][A][r]*ki[2][2][r] - 
            6*Derivative[2][A][r]*pi[2][2][r] + 12*r*Derivative[1][wi[1][1]][
              r]*wi[1][1][r] + 4*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] + 
            3*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r]))))/(4*r^2*A[r]^2)))/
   2 + (K[r]*(8*E^\[CurlyPhi][r]*r^3*\[Alpha]*(-1 + B[r])*B[r]*
      Derivative[1][A][r]^2*wi[1][1][r]^2 - 
     r*A[r]*(-4*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][A][r]*
        Derivative[1][B][r]*wi[1][1][r]^2 + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        B[r]^2*(-6*Derivative[1][A][r]^2*Derivative[1][ki[2][2]][r] + 
         4*r*Derivative[2][A][r]*wi[1][1][r]^2 + Derivative[1][A][r]*
          (r^2*Derivative[1][wi[1][1]][r]^2 + 8*r*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + 8*wi[1][1][r]^2)) + 
       B[r]*(2*E^\[CurlyPhi][r]*r^4*\[Alpha]*\[Omega]^2*
          Derivative[1][wi[1][1]][r]^2 - 16*E^\[CurlyPhi][r]*r^2*\[Alpha]*
          Derivative[1][A][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
         4*r*Derivative[1][A][r]*(-4*E^\[CurlyPhi][r]*\[Alpha] + 
           3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]^2 - 
         8*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[2][A][r]*wi[1][1][r]^2 + 
         3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]^2*
          (16*ki[2][2][r] - 2*(2*pi[2][2][r] - \[CurlyPhi]i[2][2][r])))) - 
     6*A[r]^3*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        (-2*r*Derivative[1][hi[2][2]][r] - 12*hi[2][2][r]) + 
       2*r*(B[r]*(2*r^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][2]][
             r] - 2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][hi[2][2]][r]) - 
         6*\[CurlyPhi]i[2][2][r])) + 
     A[r]^2*(4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (7*r^2*Derivative[1][wi[1][1]][r]^2 - 6*r*Derivative[1][ki[2][2]][r]*
          Derivative[2][A][r] - 6*Derivative[1][A][r]*
          (2*Derivative[1][ki[2][2]][r] + r*Derivative[2][ki[2][2]][r]) + 
         4*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] + 4*wi[1][1][r]^2 + 
         2*Derivative[1][wi[1][1]][r]*(r^3*Derivative[2][wi[1][1]][r] + 
           8*r*wi[1][1][r])) + E^\[CurlyPhi][r]*r*\[Alpha]*
        (-72*\[Omega]^2*pi[2][2][r] + Derivative[1][B][r]*
          (12*r^2*\[Omega]^2*Derivative[1][ki[2][2]][r] - 
           24*r*\[Omega]^2*hi[2][2][r] - 24*r*\[Omega]^2*ki[2][2][r] + 
           48*Derivative[1][A][r]*ki[2][2][r] - 24*r*\[Omega]^2*pi[2][2][r] - 
           12*Derivative[1][A][r]*pi[2][2][r] - 8*r^2*Derivative[1][wi[1][1]][
             r]*wi[1][1][r] - 32*r*wi[1][1][r]^2 + 12*r*\[Omega]^2*
            \[CurlyPhi]i[2][2][r] + 6*Derivative[1][A][r]*\[CurlyPhi]i[2][2][
             r])) + 2*B[r]*(-3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
          (-4*r*Derivative[1][hi[2][2]][r] + 6*r^2*Derivative[1][B][r]*
            Derivative[1][ki[2][2]][r] + 2*r*Derivative[1][pi[2][2]][r] - 
           12*pi[2][2][r]) + r*(24*E^\[CurlyPhi][r]*r*\[Alpha]*\[Omega]^2*
            Derivative[1][ki[2][2]][r] - 12*E^\[CurlyPhi][r]*r*\[Alpha]*
            \[Omega]^2*Derivative[1][pi[2][2]][r] + 28*E^\[CurlyPhi][r]*r^2*
            \[Alpha]*Derivative[1][wi[1][1]][r]^2 + 3*E^\[CurlyPhi][r]*r^3*
            \[Alpha]*Derivative[1][B][r]*Derivative[1][wi[1][1]][r]^2 + 
           12*E^\[CurlyPhi][r]*r^2*\[Alpha]*\[Omega]^2*
            Derivative[2][ki[2][2]][r] + 48*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[2][A][r]*ki[2][2][r] - 12*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[2][A][r]*pi[2][2][r] - 32*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][wi[1][1]][r]*wi[1][1][r] + 12*E^\[CurlyPhi][r]*r^2*
            \[Alpha]*Derivative[1][B][r]*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + 4*r^4*Derivative[1][\[CurlyPhi]][r]*
            Derivative[1][wi[1][1]][r]*wi[1][1][r] - 8*E^\[CurlyPhi][r]*r^2*
            \[Alpha]*Derivative[2][wi[1][1]][r]*wi[1][1][r] - 
           8*E^\[CurlyPhi][r]*\[Alpha]*wi[1][1][r]^2 + 12*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][B][r]*wi[1][1][r]^2 + 
           4*r^3*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]^2 + 
           6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*\[CurlyPhi]i[2][2][
             r])))))/(8*r^3*A[r]^3) + 
  (Derivative[1][K][r]*(12*A[r]^2*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (2*Derivative[1][hi[2][2]][r] + r*Derivative[2][hi[2][2]][r]) - 
       3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*hi[2][2][r] + 
       B[r]*(3*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][hi[2][2]][r] + r^3*Derivative[1][\[CurlyPhi]i[2][2]][
           r] - 2*(6*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r]*(ki[2][2][r] + pi[2][2][r])))) + 
     E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*Derivative[1][A][r]*
      (8*r*wi[1][1][r]^2 + B[r]*(r*(r^2*Derivative[1][wi[1][1]][r]^2 - 
           4*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 8*wi[1][1][r]^2) - 
         6*Derivative[1][A][r]*(r*Derivative[1][ki[2][2]][r] - 
           2*ki[2][2][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))) + 
     r*A[r]*(-4*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
        wi[1][1][r]^2 - 4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (-3*Derivative[1][A][r]*(2*r*Derivative[1][hi[2][2]][r] + 
           2*r*Derivative[1][ki[2][2]][r] - 3*r*Derivative[1][pi[2][2]][r] + 
           r^2*Derivative[2][ki[2][2]][r] - 4*ki[2][2][r] - 8*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][2][r]) + r*(-3*r*Derivative[1][ki[2][2]][r]*
            Derivative[2][A][r] + 6*Derivative[2][A][r]*ki[2][2][r] + 
           12*Derivative[2][A][r]*pi[2][2][r] - 2*r^2*Derivative[2][wi[1][1]][
             r]*wi[1][1][r] - 8*wi[1][1][r]^2 + Derivative[1][wi[1][1]][r]*
            (r^3*Derivative[2][wi[1][1]][r] - 14*r*wi[1][1][r]) - 
           3*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r])) + 
       B[r]*(r*(4*wi[1][1][r]*(10*E^\[CurlyPhi][r]*r*\[Alpha]*
              Derivative[1][wi[1][1]][r] + (-8*E^\[CurlyPhi][r]*\[Alpha] + 
               r^3*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) - 
           3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            (r^2*Derivative[1][wi[1][1]][r]^2 - 4*r*Derivative[1][wi[1][1]][
               r]*wi[1][1][r] - 4*wi[1][1][r]^2)) + 18*E^\[CurlyPhi][r]*
          \[Alpha]*Derivative[1][A][r]*(-2*pi[2][2][r] + 
           r*Derivative[1][B][r]*(r*Derivative[1][ki[2][2]][r] - 
             2*ki[2][2][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))))/
   (8*r^3*A[r]^2) + 
  (H2[r]*(-3*A[r]^2*(8*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*
        Derivative[2][hi[2][2]][r] + r*Derivative[1][B][r]*
        (-2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][hi[2][2]][r] + 
         r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         4*(3*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r])) + 
       2*r*B[r]*((6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
         r*(2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 2*E^\[CurlyPhi][r]*
            \[Alpha]*Derivative[2][hi[2][2]][r] + 
           r^2*Derivative[2][\[CurlyPhi]i[2][2]][r] + 
           2*r*Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][ki[2][2]][r] - 
             r*Derivative[1][pi[2][2]][r] - 4*pi[2][2][r]) - 
           4*r^2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r]))) - 
     E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*Derivative[1][A][r]*
      (3*Derivative[1][A][r]*(4*ki[2][2][r] - 4*pi[2][2][r] + 
         \[CurlyPhi]i[2][2][r]) + 2*B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 - 
         3*Derivative[1][A][r]*(2*r*Derivative[1][ki[2][2]][r] - 
           6*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))) + 
     r*A[r]*(3*E^\[CurlyPhi][r]*r*\[Alpha]*(4*\[Omega]^2*hi[2][2][r] + 
         (-8*\[Omega]^2 + 4*Derivative[1][A][r]*Derivative[1][B][r])*
          ki[2][2][r] + 4*\[Omega]^2*pi[2][2][r] - 4*Derivative[1][A][r]*
          Derivative[1][B][r]*pi[2][2][r] - 2*\[Omega]^2*\[CurlyPhi]i[2][2][
           r] + Derivative[1][A][r]*Derivative[1][B][r]*\[CurlyPhi]i[2][2][
           r]) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (6*r^2*Derivative[1][wi[1][1]][r]^2 - 3*Derivative[1][A][r]*
          (4*Derivative[1][hi[2][2]][r] + 8*Derivative[1][ki[2][2]][r] - 
           9*Derivative[1][pi[2][2]][r] + 4*r*Derivative[2][ki[2][2]][r]) + 
         4*r^3*Derivative[1][wi[1][1]][r]*Derivative[2][wi[1][1]][r] - 
         6*Derivative[2][A][r]*(2*r*Derivative[1][ki[2][2]][r] - 
           6*pi[2][2][r] + \[CurlyPhi]i[2][2][r])) + 
       B[r]*(-3*Derivative[1][A][r]*(-4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][hi[2][2]][r] + 12*E^\[CurlyPhi][r]*r^2*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][ki[2][2]][r] + 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][pi[2][2]][r] + 
           r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 24*E^\[CurlyPhi][r]*
            \[Alpha]*pi[2][2][r] - 36*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][B][r]*pi[2][2][r] - 
           4*r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
           6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            \[CurlyPhi]i[2][2][r]) + E^\[CurlyPhi][r]*r*\[Alpha]*
          (12*r*\[Omega]^2*Derivative[1][ki[2][2]][r] + 
           r^2*(18 + 6*r*Derivative[1][B][r])*Derivative[1][wi[1][1]][r]^2 - 
           12*\[Omega]^2*hi[2][2][r] - 6*\[Omega]^2*(4*pi[2][2][r] - 
             \[CurlyPhi]i[2][2][r]) + 6*Derivative[2][A][r]*
            (4*ki[2][2][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))))/
   (4*r^4*A[r]^2) + 
  (\[CurlyPhi]1[r]*(-6*A[r]^2*(-2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        Derivative[2][hi[2][2]][r] + 
       r*B[r]*((r - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r])*
          Derivative[1][hi[2][2]][r] + 2*r*Derivative[1][ki[2][2]][r] - 
         r*Derivative[1][pi[2][2]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[2][hi[2][2]][r]) + Derivative[1][B][r]*
        (E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][hi[2][2]][r] + 
         6*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] - r^2*pi[2][2][r])) + 
     E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]*
      (3*Derivative[1][A][r]*(4*ki[2][2][r] - 2*pi[2][2][r] + 
         \[CurlyPhi]i[2][2][r]) + B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 - 
         3*Derivative[1][A][r]*(2*r*Derivative[1][ki[2][2]][r] - 
           4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))) - 
     A[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (3*r^2*Derivative[1][wi[1][1]][r]^2 - 3*Derivative[1][A][r]*
          (2*Derivative[1][hi[2][2]][r] + 4*Derivative[1][ki[2][2]][r] - 
           3*Derivative[1][pi[2][2]][r] + 2*r*Derivative[2][ki[2][2]][r]) + 
         2*r^3*Derivative[1][wi[1][1]][r]*Derivative[2][wi[1][1]][r] - 
         3*Derivative[2][A][r]*(2*r*Derivative[1][ki[2][2]][r] - 
           4*pi[2][2][r] + \[CurlyPhi]i[2][2][r])) + 
       3*r*(4*r^2*\[Omega]^2*hi[2][2][r] + E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][A][r]*Derivative[1][B][r]*(4*ki[2][2][r] - 
           2*pi[2][2][r] + \[CurlyPhi]i[2][2][r])) + 
       3*B[r]*(Derivative[1][A][r]*(4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][hi[2][2]][r] - 2*(E^\[CurlyPhi][r]*r*\[Alpha]*
              Derivative[1][pi[2][2]][r] + (r^2 - 6*E^\[CurlyPhi][r]*
                \[Alpha])*pi[2][2][r]) - 3*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][B][r]*(2*r*Derivative[1][ki[2][2]][r] - 
             4*pi[2][2][r] + \[CurlyPhi]i[2][2][r])) + E^\[CurlyPhi][r]*r*
          \[Alpha]*(r^2*(6 + r*Derivative[1][B][r])*
            Derivative[1][wi[1][1]][r]^2 + 2*Derivative[2][A][r]*
            (4*ki[2][2][r] - 2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))))/
   (4*r^4*A[r]^2), ((2*I)*\[Omega]*\[CurlyPhi]1[r]*wi[1][1][r])/(r*A[r]) + 
  (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*H2[r]*(-2*wi[1][1][r] + 
     B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(r^2*A[r]) + 
  (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][H1][r]*
    (-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/
   (r^2*A[r]) + ((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*K[r]*
    (-(r*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]) + 
     A[r]*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
         r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]))))/(r*A[r]^2) + 
  (H1[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]*
       Derivative[1][wi[1][1]][r]) + 
     A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(3*Derivative[1][wi[1][1]][r] + 
         r*Derivative[2][wi[1][1]][r]) - 2*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*wi[1][1][r] + 
       B[r]*(E^\[CurlyPhi][r]*\[Alpha]*(-2 + 3*r*Derivative[1][B][r])*
          Derivative[1][wi[1][1]][r] + 2*(3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r] + r^2*Derivative[1][\[CurlyPhi]][r])*
          wi[1][1][r]))))/(2*r^2*A[r]^2), 
 ((6*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][h0][r]*
    (r*Derivative[1][ki[2][2]][r] - pi[2][2][r]))/(r^3*A[r]) + 
  (3*E^\[CurlyPhi][r]*\[ScriptL]*(1 + \[ScriptL])*\[Alpha]*B[r]*h1[r]*
    (2*A[r]*(r*Derivative[1][hi[2][2]][r] - hi[2][2][r]) + 
     r*Derivative[1][A][r]*(hi[2][2][r] - pi[2][2][r]) + 
     r^3*Derivative[1][wi[1][1]][r]*wi[1][1][r]))/(r^5*A[r]) - 
  (B[r]*Derivative[1][h1][r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (r^4*Derivative[1][wi[1][1]][r]^2 + 12*A[r]*hi[2][2][r]) + 
     6*A[r]*(-4*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] + 
       r^2*\[CurlyPhi]i[2][2][r])))/(2*r^4*A[r]) - 
  ((I/2)*\[Omega]*h0[r]*(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]*
      Derivative[1][wi[1][1]][r]^2 + 
     6*A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*r*Derivative[1][ki[2][2]][r] - pi[2][2][r]) - 
       4*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] + r^2*\[CurlyPhi]i[2][2][r])))/
   (r^4*A[r]^2) + (h1[r]*(E^\[CurlyPhi][r]*r^5*\[Alpha]*B[r]^2*
      Derivative[1][A][r]*Derivative[1][wi[1][1]][r]^2 + 
     6*A[r]^2*(-8*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (r*Derivative[1][hi[2][2]][r] - hi[2][2][r]) + 
       2*B[r]*(-2*r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*hi[2][2][r] - 
         r^3*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 
         r^3*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r]) + 
       r*Derivative[1][B][r]*(4*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] - 
         r^2*\[CurlyPhi]i[2][2][r])) + r*A[r]*B[r]*
      (-24*E^\[CurlyPhi][r]*r*\[Alpha]*(r*\[Omega]^2 + 
         B[r]*Derivative[1][A][r])*Derivative[1][ki[2][2]][r] - 
       3*E^\[CurlyPhi][r]*r^4*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][wi[1][1]][r]^2 - 4*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (r^4*Derivative[1][wi[1][1]][r]*Derivative[2][wi[1][1]][r] + 
         3*Derivative[1][A][r]*(2*hi[2][2][r] - 3*pi[2][2][r])) + 
       6*(4*E^\[CurlyPhi][r]*r*\[Alpha]*\[Omega]^2*pi[2][2][r] + 
         Derivative[1][A][r]*(4*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] - 
           r^2*\[CurlyPhi]i[2][2][r])))))/(4*r^5*A[r]^2), 
 (E^\[CurlyPhi][r]*\[Alpha]*H0[r]*
    (-((B[r]*Derivative[1][wi[1][1]][r]^2)/A[r]) + (12*pi[2][2][r])/r^4))/4 + 
  (\[CurlyPhi]1[r]*wi[1][1][r]^2)/(r*A[r]) + 
  (E^\[CurlyPhi][r]*\[Alpha]*H2[r]*
    (r^2*B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])^2 - 
     4*(3*A[r]*hi[2][2][r] + r^2*wi[1][1][r]^2)))/(4*r^4*A[r]), 
 ((-I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][h1][r]*
    (-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/
   (r^2*A[r]) - (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[2][h0][r]*
    (-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/
   (r^2*A[r]) + (E^\[CurlyPhi][r]*\[ScriptL]*(1 + \[ScriptL])*\[Alpha]*h0[r]*
    (-(r*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]) + 
     A[r]*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
         r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]))))/(2*r^3*A[r]^2) - 
  (Derivative[1][h0][r]*(-(E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
       (-4*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 
          4*wi[1][1][r]))) + A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]) - 
       2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*wi[1][1][r] + 
       B[r]*(E^\[CurlyPhi][r]*\[Alpha]*(-2 + 3*r*Derivative[1][B][r])*
          Derivative[1][wi[1][1]][r] + 2*(3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r] + r^2*Derivative[1][\[CurlyPhi]][r])*
          wi[1][1][r]))))/(2*r^2*A[r]^2) - 
  ((I/2)*\[Omega]*h1[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
       Derivative[1][A][r]*Derivative[1][wi[1][1]][r]) + 
     A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(3*Derivative[1][wi[1][1]][r] + 
         r*Derivative[2][wi[1][1]][r]) - 2*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*wi[1][1][r] + 
       B[r]*(E^\[CurlyPhi][r]*\[Alpha]*(2 + 3*r*Derivative[1][B][r])*
          Derivative[1][wi[1][1]][r] + 2*(3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r] + r^2*Derivative[1][\[CurlyPhi]][r])*
          wi[1][1][r]))))/(r^2*A[r]^2) + 
  (h0[r]*(-2*r^3*A[r]^2*B[r]*Derivative[1][\[CurlyPhi]][r]*
      Derivative[1][wi[1][1]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + B[r])*
      B[r]*Derivative[1][A][r]^2*wi[1][1][r] + 
     r*A[r]*(-2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
        Derivative[1][B][r]*wi[1][1][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
         2*Derivative[2][A][r]*wi[1][1][r]) - 
       B[r]*(4*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]*wi[1][1][r] + 
         Derivative[1][A][r]*(2*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][wi[1][1]][r] - 2*(3*E^\[CurlyPhi][r]*\[Alpha]*
              Derivative[1][B][r] + r^2*Derivative[1][\[CurlyPhi]][r])*
            wi[1][1][r])))))/(2*r^3*A[r]^3), 
 ((-6*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*H1[r]*
    (r*Derivative[1][ki[2][2]][r] - pi[2][2][r]))/(r^3*A[r]) - 
  (6*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][H0][r]*
    (r*Derivative[1][ki[2][2]][r] - pi[2][2][r]))/r^3 - 
  (3*\[CurlyPhi]1[r]*(hi[2][2][r] + pi[2][2][r]))/r^3 + 
  (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][K][r]*
    (2*A[r]*(r*Derivative[1][hi[2][2]][r] - hi[2][2][r]) + 
     r*Derivative[1][A][r]*(hi[2][2][r] - pi[2][2][r]) + 
     r^3*Derivative[1][wi[1][1]][r]*wi[1][1][r]))/(r^3*A[r]) - 
  (E^\[CurlyPhi][r]*\[Alpha]*K[r]*(A[r]*Derivative[1][B][r]*
      (-3*Derivative[1][A][r]*ki[2][2][r] + r*wi[1][1][r]*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])) + 
     B[r]*(3*Derivative[1][A][r]^2*ki[2][2][r] - r^2*Derivative[1][A][r]*
        Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
       2*A[r]*(-3*Derivative[2][A][r]*ki[2][2][r] + 
         r*(3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r])*
          wi[1][1][r]))))/(r^2*A[r]^2) - 
  (H2[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
       (5*r^3*Derivative[1][wi[1][1]][r]^2 - 12*Derivative[1][A][r]*
         (r*Derivative[1][ki[2][2]][r] + hi[2][2][r] - 3*pi[2][2][r]))) + 
     6*A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*r*Derivative[1][hi[2][2]][r] - hi[2][2][r]) - 
       4*E^\[CurlyPhi][r]*\[Alpha]*hi[2][2][r] + r^2*\[CurlyPhi]i[2][2][r])))/
   (4*r^4*A[r]) - (H0[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
      (5*r^3*Derivative[1][wi[1][1]][r]^2 + 12*Derivative[1][A][r]*
        (r*Derivative[1][ki[2][2]][r] - pi[2][2][r])) - 
     6*A[r]*(-2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*hi[2][2][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*r*Derivative[1][ki[2][2]][r] - 
         pi[2][2][r]) - 4*E^\[CurlyPhi][r]*\[Alpha]*pi[2][2][r] + 
       r^2*\[CurlyPhi]i[2][2][r])))/(4*r^4*A[r]), 
 ((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*h0[r]*
    (r^4*B[r]*Derivative[1][wi[1][1]][r]^2 - 12*A[r]*pi[2][2][r]))/
   (r^4*A[r]^2) + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][h1][r]*
    (r^2*B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])^2 - 
     4*(3*A[r]*hi[2][2][r] + r^2*wi[1][1][r]^2)))/(2*r^4*A[r]) - 
  (h1[r]*(12*E^\[CurlyPhi][r]*\[Alpha]*A[r]^2*Derivative[1][B][r]*
      hi[2][2][r] + E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]^2*Derivative[1][A][r]*
      Derivative[1][wi[1][1]][r]*(r*Derivative[1][wi[1][1]][r] + 
       4*wi[1][1][r]) - A[r]*(-4*E^\[CurlyPhi][r]*r^2*\[Alpha]*
        Derivative[1][B][r]*wi[1][1][r]^2 + 4*E^\[CurlyPhi][r]*r^2*\[Alpha]*
        B[r]^2*(3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r])*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) + 
       B[r]*(-12*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*pi[2][2][r] + 
         4*r^4*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]^2 + 
         3*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
          (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])^2))))/(4*r^4*A[r]^2)}
