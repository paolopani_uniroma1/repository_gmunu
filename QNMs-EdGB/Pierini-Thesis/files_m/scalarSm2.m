{{0, 0}, 
 {-(\[Sigma]^2*((3*\[ScriptCapitalM]^3*
       (r*(-18 + 15*\[ScriptL] - 7*\[ScriptL]^2)*\[ScriptCapitalM] + 
        32*\[ScriptCapitalM]^2 - 2*r^4*\[Omega]^2)*Q[-1 + \[ScriptL], m]*
       Q[\[ScriptL], m])/(r^6*(r - 2*\[ScriptCapitalM])) - 
     (\[ScriptCapitalM]^3*\[Zeta]^2*(-350*r^3*(355522 - 84103*\[ScriptL] + 
          38175*\[ScriptL]^2)*\[ScriptCapitalM]^6 + 
        2800*r^2*(131006 - 32305*\[ScriptL] + 15673*\[ScriptL]^2)*
         \[ScriptCapitalM]^7 - 98000*r*(7546 - 831*\[ScriptL] + 
          423*\[ScriptL]^2)*\[ScriptCapitalM]^8 + 
        580160000*\[ScriptCapitalM]^9 + 62482*r^11*\[Omega]^2 + 
        187446*r^10*\[ScriptCapitalM]*\[Omega]^2 + 
        399225*r^9*\[ScriptCapitalM]^2*\[Omega]^2 + r^6*\[ScriptCapitalM]^3*
         (4484530 - 1839407*\[ScriptL] + 798519*\[ScriptL]^2 - 
          4869200*\[ScriptCapitalM]^2*\[Omega]^2) - 
        42*r^7*\[ScriptCapitalM]^2*(5192 - 3894*\[ScriptL] + 
          1298*\[ScriptL]^2 + 11875*\[ScriptCapitalM]^2*\[Omega]^2) - 
        6*r^8*\[ScriptCapitalM]*(27258 - 13629*\[ScriptL] + 
          4543*\[ScriptL]^2 + 125690*\[ScriptCapitalM]^2*\[Omega]^2) - 
        20*r^4*\[ScriptCapitalM]^5*(-2377886 + 413505*\[ScriptL] - 
          198105*\[ScriptL]^2 + 803600*\[ScriptCapitalM]^2*\[Omega]^2) + 
        5*r^5*\[ScriptCapitalM]^4*(-3480406 + 1342781*\[ScriptL] - 
          665317*\[ScriptL]^2 + 1595440*\[ScriptCapitalM]^2*\[Omega]^2))*
       Q[-1 + \[ScriptL], m]*Q[\[ScriptL], m])/
      (98000*r^12*(r - 2*\[ScriptCapitalM])^2) - 
     (\[ScriptCapitalM]^3*\[Zeta]^3*
       (-28224000*r^3*(73748554 - 3949799*\[ScriptL] + 2020123*\[ScriptL]^2)*
         \[ScriptCapitalM]^9 + 1267728000*r^2*(1165938 - 121095*\[ScriptL] + 
          54175*\[ScriptL]^2)*\[ScriptCapitalM]^10 - 135224320000*r*
         (16967 - 3120*\[ScriptL] + 1585*\[ScriptL]^2)*\[ScriptCapitalM]^11 + 
        3612517708800000*\[ScriptCapitalM]^12 + 171696544404*r^14*
         \[Omega]^2 + 515089633212*r^13*\[ScriptCapitalM]*\[Omega]^2 + 
        1114949414700*r^12*\[ScriptCapitalM]^2*\[Omega]^2 - 
        940800*r^4*\[ScriptCapitalM]^8*(-1352786945 + 279173814*\[ScriptL] - 
          138981838*\[ScriptL]^2 + 95942000*\[ScriptCapitalM]^2*\[Omega]^2) - 
        22400*r^5*\[ScriptCapitalM]^7*(17314033854 - 5068603407*\[ScriptL] + 
          2336830349*\[ScriptL]^2 + 882127400*\[ScriptCapitalM]^2*
           \[Omega]^2) - 15400*r^6*\[ScriptCapitalM]^6*
         (-2150681002 + 711833145*\[ScriptL] - 292743573*\[ScriptL]^2 + 
          3283356720*\[ScriptCapitalM]^2*\[Omega]^2) - 
        154*r^10*\[ScriptCapitalM]^2*(3642554052 - 2731915539*\[ScriptL] + 
          910638513*\[ScriptL]^2 + 5000382500*\[ScriptCapitalM]^2*
           \[Omega]^2) + 33*r^11*\[ScriptCapitalM]*(-12748939182 + 
          6374469591*\[ScriptL] - 2124823197*\[ScriptL]^2 + 
          17787721240*\[ScriptCapitalM]^2*\[Omega]^2) - 
        44*r^9*\[ScriptCapitalM]^3*(-112553938890 + 34062357591*\[ScriptL] - 
          15273940347*\[ScriptL]^2 + 140307823600*\[ScriptCapitalM]^2*
           \[Omega]^2) + 40*r^7*\[ScriptCapitalM]^5*(570732516046 + 
          12121984545*\[ScriptL] - 14983126895*\[ScriptL]^2 + 
          321897189600*\[ScriptCapitalM]^2*\[Omega]^2) - 
        60*r^8*\[ScriptCapitalM]^4*(163179214836 - 62090074761*\[ScriptL] + 
          34117743077*\[ScriptL]^2 + 404474859040*\[ScriptCapitalM]^2*
           \[Omega]^2))*Q[-1 + \[ScriptL], m]*Q[\[ScriptL], m])/
      (380318400000*r^15*(r - 2*\[ScriptCapitalM])^2) - 
     (\[ScriptCapitalM]^3*\[Zeta]^4*(783151649280000*r^3*
         (80455455406990 - 9986255833785*\[ScriptL] + 4841037319377*
           \[ScriptL]^2)*\[ScriptCapitalM]^13 - 383744308147200000*r^2*
         (322281206046 - 32248446373*\[ScriptL] + 16060513469*\[ScriptL]^2)*
         \[ScriptCapitalM]^14 + 6036297967155456000000*r*
         (27026018 - 1400235*\[ScriptL] + 711715*\[ScriptL]^2)*
         \[ScriptCapitalM]^15 - 93304747841752686919680000000*
         \[ScriptCapitalM]^16 + 81049063336212857729844*r^18*\[Omega]^2 + 
        81049063336212857729844*r^17*\[ScriptCapitalM]*\[Omega]^2 + 
        40583794899252851096136*r^16*\[ScriptCapitalM]^2*\[Omega]^2 + 
        2738292480000*r^4*\[ScriptCapitalM]^12*(-10604996759419894 + 
          1300604992487141*\[ScriptL] - 627622359662221*\[ScriptL]^2 + 
          664494999168000*\[ScriptCapitalM]^2*\[Omega]^2) - 
        60850944000*r^5*\[ScriptCapitalM]^11*(-217920331407247163 + 
          27285747552403675*\[ScriptL] - 13513398216540715*\[ScriptL]^2 + 
          27145608488598600*\[ScriptCapitalM]^2*\[Omega]^2) - 
        1881600*r^7*\[ScriptCapitalM]^9*(-632318353629658480220 + 
          167009576488734331581*\[ScriptL] - 77416217961293021077*
           \[ScriptL]^2 + 202441633876576294200*\[ScriptCapitalM]^2*
           \[Omega]^2) + 89600*r^8*\[ScriptCapitalM]^8*
         (-550113912626820062118 - 39304891891273185723*\[ScriptL] + 
          25991200386549857641*\[ScriptL]^2 + 1742841068058120561300*
           \[ScriptCapitalM]^2*\[Omega]^2) - 429*r^15*\[ScriptCapitalM]*
         (445339741064448144054 - 222669870532224072027*\[ScriptL] + 
          74223290177408024009*\[ScriptL]^2 + 1753003826701162679320*
           \[ScriptCapitalM]^2*\[Omega]^2) - 572*r^14*\[ScriptCapitalM]^2*
         (-222669870532224072027 + 2165323889889208792420*\[ScriptCapitalM]^2*
           \[Omega]^2) - 572*r^13*\[ScriptCapitalM]^3*
         (-4629033883408608067188 + 1860135740841274957233*\[ScriptL] - 
          760117123316396832711*\[ScriptL]^2 + 5681128998249094443200*
           \[ScriptCapitalM]^2*\[Omega]^2) - 11200*r^9*\[ScriptCapitalM]^7*
         (-3955889846575853447599 - 180043582309403242989*\[ScriptL] + 
          16501831755848499580*\[ScriptL]^2 + 8928123499478044627920*
           \[ScriptCapitalM]^2*\[Omega]^2) + 104*r^12*\[ScriptCapitalM]^4*
         (-67436025714469769887695 + 19902554571731397829362*\[ScriptL] - 
          10133618576082228298104*\[ScriptL]^2 + 9019028591926007429600*
           \[ScriptCapitalM]^2*\[Omega]^2) + 80*r^10*\[ScriptCapitalM]^6*
         (-604388847438428568712196 + 135313496703008332821255*\[ScriptL] - 
          61016069829975888128215*\[ScriptL]^2 + 395390739150081513963600*
           \[ScriptCapitalM]^2*\[Omega]^2) + 4704000*r^6*\[ScriptCapitalM]^10*
         (193900212804373378527*\[ScriptL] - 94130030719557170079*
           \[ScriptL]^2 + 22*(-50765439231456711533 + 9024205182270117840*
             \[ScriptCapitalM]^2*\[Omega]^2)) + 520*r^11*\[ScriptCapitalM]^5*
         (-6133644080749479888561*\[ScriptL] + 2897066397500884787167*
           \[ScriptL]^2 + 32*(956432602777377110759 + 738456793423584090870*
             \[ScriptCapitalM]^2*\[Omega]^2)))*Q[-1 + \[ScriptL], m]*
       Q[\[ScriptL], m])/(181088939014663680000000*r^18*
       (r - 2*\[ScriptCapitalM])^3))), 
  -(\[Sigma]^2*((48*\[ScriptCapitalM]^4*Q[-1 + \[ScriptL], m]*
       Q[\[ScriptL], m])/r^5 - (\[ScriptCapitalM]^4*
       (13629*r^6 + 68145*r^5*\[ScriptCapitalM] - 
        281910*r^4*\[ScriptCapitalM]^2 + 759850*r^3*\[ScriptCapitalM]^3 - 
        1691200*r^2*\[ScriptCapitalM]^4 + 5071500*r*\[ScriptCapitalM]^5 - 
        18130000*\[ScriptCapitalM]^6)*\[Zeta]^2*Q[-1 + \[ScriptL], m]*
       Q[\[ScriptL], m])/(12250*r^11) + 
     (\[ScriptCapitalM]^4*(-70119165501*r^9 - 350595827505*r^8*
         \[ScriptCapitalM] + 38868188040*r^7*\[ScriptCapitalM]^2 - 
        1074194502150*r^6*\[ScriptCapitalM]^3 + 898189014800*r^5*
         \[ScriptCapitalM]^4 + 15196791733200*r^4*\[ScriptCapitalM]^5 - 
        35340820368000*r^3*\[ScriptCapitalM]^6 + 115455792144000*r^2*
         \[ScriptCapitalM]^7 + 81540264960000*r*\[ScriptCapitalM]^8 + 
        225782356800000*\[ScriptCapitalM]^9)*\[Zeta]^3*Q[-1 + \[ScriptL], m]*
       Q[\[ScriptL], m])/(95079600000*r^14) - 
     (\[ScriptCapitalM]^4*(31841791486108042299861*r^12 + 
        159208957430540211499305*r^11*\[ScriptCapitalM] + 
        8412228766139457247560*r^10*\[ScriptCapitalM]^2 + 
        167000378077951616848650*r^9*\[ScriptCapitalM]^3 - 
        1715759510541126192942800*r^8*\[ScriptCapitalM]^4 - 
        674741103006751790209200*r^7*\[ScriptCapitalM]^5 + 
        5976755417397071890128000*r^6*\[ScriptCapitalM]^6 + 
        41985948901069384146936000*r^5*\[ScriptCapitalM]^7 - 
        111189600129161610524160000*r^4*\[ScriptCapitalM]^8 + 
        176982863536533084750720000*r^3*\[ScriptCapitalM]^9 - 
        575225786722959045273600000*r^2*\[ScriptCapitalM]^10 + 
        733124987930439808704000000*r*\[ScriptCapitalM]^11 - 
        2915773370054771466240000000*\[ScriptCapitalM]^12)*\[Zeta]^4*
       Q[-1 + \[ScriptL], m]*Q[\[ScriptL], m])/(45272234753665920000000*
       r^17)))}}
