{((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
    Derivative[1][B][r])/r^2 - 
  ((I/2)*\[Omega]*H2[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/r - 
  (B[r]*Derivative[1][H1][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/(2*r) + 
  (I/4)*\[Omega]*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
     Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
     (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])) - 
  (H1[r]*(r^2*A[r]*Derivative[1][B][r] + r*B[r]*(r*Derivative[1][A][r] - 
       3*E^\[CurlyPhi][r]*\[Alpha]*A[r]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r]) - E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*A[r]*(-Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
           r]))))/(4*r^2*A[r]), 
 ((I/4)*\[Omega]*H2[r]*(E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
     A[r]*(4*r*pi[2][0][r] - 2*r*pi[2][2][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
           8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r])))))/(r*A[r]) + 
  (B[r]*Derivative[1][H1][r]*(E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
     A[r]*(4*r*pi[2][0][r] - 2*r*pi[2][2][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
           8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r])))))/(4*r*A[r]) + 
  ((I/4)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
    (-(r^3*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r]) + 
     r^2*A[r]*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
       B[r]*(r*Derivative[1][wi[1][1]][r]^2 + 6*Derivative[1][wi[1][1]][r]*
          wi[1][1][r] + 2*r*Derivative[2][wi[1][1]][r]*wi[1][1][r])) + 
     A[r]^2*(2*B[r]*(4*Derivative[1][ki[2][0]][r] - 
         2*Derivative[1][ki[2][2]][r] - 2*Derivative[1][pi[2][0]][r] + 
         Derivative[1][pi[2][2]][r] + 2*r*Derivative[2][ki[2][0]][r] - 
         r*Derivative[2][ki[2][2]][r]) + Derivative[1][B][r]*
        (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
         4*pi[2][0][r] + 2*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
         \[CurlyPhi]i[2][2][r]))))/(r^2*A[r]^2) - 
  (H1[r]*(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*Derivative[1][A][r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
     r*A[r]*B[r]*(r*Derivative[1][A][r]*(4*pi[2][0][r] - 2*pi[2][2][r]) + 
       3*E^\[CurlyPhi][r]*r^3*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
        wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*r^2*(r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + r*Derivative[1][wi[1][1]][r]*
            Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] + 
           Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r]^2 + 
             2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
             r*Derivative[2][wi[1][1]][r]*wi[1][1][r])) + 
         Derivative[1][A][r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][
               r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
             \[CurlyPhi]i[2][2][r])))) + 
     A[r]^2*(2*r^2*Derivative[1][B][r]*(-2*pi[2][0][r] + pi[2][2][r]) + 
       r*B[r]*(4*r*Derivative[1][hi[2][0]][r] - 2*r*Derivative[1][hi[2][2]][
           r] - 6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][0]][r] + 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][2]][r] - 
         4*r*Derivative[1][pi[2][0]][r] + 2*r*Derivative[1][pi[2][2]][r] - 
         6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]i[2][0]][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         24*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] - 12*E^\[CurlyPhi][r]*
          \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          pi[2][2][r] - 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) - 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (-2*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
         Derivative[1][\[CurlyPhi]i[2][2]][r] + 2*r^2*Derivative[1][ki[2][0]][
           r]*Derivative[2][\[CurlyPhi]][r] - r^2*Derivative[1][ki[2][2]][r]*
          Derivative[2][\[CurlyPhi]][r] + 
         2*r*Derivative[2][\[CurlyPhi]i[2][0]][r] - 
         r*Derivative[2][\[CurlyPhi]i[2][2]][r] - 
         8*r*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] + 
         4*r*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
         2*r*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2*(2*r*Derivative[1][ki[2][0]][r] - 
           r*Derivative[1][ki[2][2]][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]) - 
         r*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][hi[2][0]][r] - 
           r*Derivative[1][hi[2][2]][r] - 6*r*Derivative[1][pi[2][0]][r] + 
           3*r*Derivative[1][pi[2][2]][r] + 
           4*r*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           2*r^2*Derivative[2][ki[2][0]][r] - r^2*Derivative[2][ki[2][2]][
             r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r])))))/(8*r^2*A[r]^2) - 
  ((I/8)*\[Omega]*K[r]*(2*r^2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r]^2 + 
     A[r]*(-8*ki[2][0][r] + 4*ki[2][2][r] - E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][r]*
          (4*ki[2][0][r] - 2*ki[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r] - 
           2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]*
          (2*Derivative[1][pi[2][0]][r] - Derivative[1][pi[2][2]][r] - 
           4*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
         2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
         Derivative[2][\[CurlyPhi]i[2][2]][r] + 4*Derivative[2][\[CurlyPhi]][
           r]*ki[2][0][r] - 2*Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] + 
         4*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
         2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
         2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         Derivative[1][\[CurlyPhi]][r]^2*(4*ki[2][0][r] - 2*ki[2][2][r] + 
           4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r])))))/A[r], 
 (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
    Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
     A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) - 
  (B[r]*Derivative[1][h1][r]*(2*wi[1][1][r] + 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
       (A[r]*Derivative[1][wi[1][1]][r] - Derivative[1][A][r]*wi[1][1][r]))/
      A[r]))/2 + (B[r]*h1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
      Derivative[1][A][r]^2*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
     A[r]^2*(Derivative[1][wi[1][1]][r]*
        (r*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          (3*Derivative[1][\[CurlyPhi]][r] + r*Derivative[1][\[CurlyPhi]][r]^
             2 + r*Derivative[2][\[CurlyPhi]][r])) + 
       2*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]][r]*
          Derivative[2][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
          wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])) - 
     r*A[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
       E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
          Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
          (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
           2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
           2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*r*A[r]^2) - 
  (h1[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]^2*
      Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
     A[r]^2*(B[r]*(4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]^2*
          Derivative[1][wi[1][1]][r] + Derivative[1][wi[1][1]][r]*
          Derivative[2][\[CurlyPhi]][r] + Derivative[1][\[CurlyPhi]][r]*
          Derivative[2][wi[1][1]][r]) + 2*Derivative[1][B][r]*wi[1][1][r]) - 
     A[r]*B[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
       E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
          Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
          (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
           2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
           2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*A[r]^2), 
 (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*H1[r]*
    (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
     r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))/
   (2*r^3) - ((I/4)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
    (-(r^4*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r]) + 
     r^3*A[r]*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
       B[r]*(r*Derivative[1][wi[1][1]][r]^2 + 6*Derivative[1][wi[1][1]][r]*
          wi[1][1][r] + 2*r*Derivative[2][wi[1][1]][r]*wi[1][1][r])) - 
     3*A[r]^2*(2*r*B[r]*(2*Derivative[1][ki[2][2]][r] - 
         Derivative[1][pi[2][2]][r] + r*Derivative[2][ki[2][2]][r]) - 
       4*pi[2][2][r] + r*Derivative[1][B][r]*(r*Derivative[1][ki[2][2]][r] - 
         2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/(r^3*A[r]^2) - 
  ((I/4)*\[Omega]*H2[r]*(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
     3*A[r]*(2*r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*
        \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][
            r])))))/(r^2*A[r]) - 
  (B[r]*Derivative[1][H1][r]*(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
     3*A[r]*(2*r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*
        \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][
            r])))))/(4*r^2*A[r]) - 
  ((I/8)*\[Omega]*K[r]*(-2*r^2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r]^2 + 
     3*A[r]*(-4*ki[2][2][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]][r]*(Derivative[1][pi[2][2]][r] - 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
         Derivative[2][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]^
           2*(2*ki[2][2][r] + 2*pi[2][2][r] - \[CurlyPhi]i[2][2][r]) + 
         Derivative[2][\[CurlyPhi]][r]*(2*ki[2][2][r] + 2*pi[2][2][r] - 
           \[CurlyPhi]i[2][2][r])) - E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         Derivative[1][\[CurlyPhi]][r]*(-2*ki[2][2][r] - 2*pi[2][2][r] + 
           \[CurlyPhi]i[2][2][r])))))/A[r] + 
  (H1[r]*(E^\[CurlyPhi][r]*r^5*\[Alpha]*B[r]^2*Derivative[1][A][r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
     r*A[r]*B[r]*(3*E^\[CurlyPhi][r]*r^4*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
        wi[1][1][r] + Derivative[1][A][r]*(-6*r^2*pi[2][2][r] + 
         6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
       E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (2*r^2*(r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + r*Derivative[1][wi[1][1]][r]*
            Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] + 
           Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r]^2 + 
             2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
             r*Derivative[2][wi[1][1]][r]*wi[1][1][r])) - 
         3*Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][ki[2][2]][r] - 
             4*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))) + 
     3*A[r]^2*(2*r*Derivative[1][B][r]*(r^2*pi[2][2][r] - 
         E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
       2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (-Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][hi[2][2]][r] - 
           3*r*Derivative[1][pi[2][2]][r] + 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           r^2*Derivative[2][ki[2][2]][r] + 4*pi[2][2][r] - 
           \[CurlyPhi]i[2][2][r]) + r*Derivative[1][\[CurlyPhi]][r]^2*
          (r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][
            r]) + r*(r*Derivative[1][ki[2][2]][r]*Derivative[2][\[CurlyPhi]][
             r] + Derivative[2][\[CurlyPhi]i[2][2]][r] + 
           Derivative[2][\[CurlyPhi]][r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][2][
              r]))) + B[r]*(-2*r^3*Derivative[1][hi[2][2]][r] + 
         2*r^3*Derivative[1][pi[2][2]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]i[2][2]][r] + 8*E^\[CurlyPhi][r]*\[Alpha]*
          \[CurlyPhi]i[2][2][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         3*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r]))))))/(8*r^3*A[r]^2), 
 ((I/2)*\[Omega]*B[r]*Derivative[1][h1][r]*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/r + 
  (B[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
    Derivative[2][h0][r])/(2*r) + (\[ScriptL]*(1 + \[ScriptL])*h0[r]*
    (-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
      Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
   (4*r^2) - (Derivative[1][h0][r]*(-(r^2*A[r]*Derivative[1][B][r]) + 
     r*B[r]*(r*Derivative[1][A][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*A[r]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) + 
     E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (-(r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]) + 
       2*A[r]*(-Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
           r]))))/(4*r^2*A[r]) + 
  ((I/4)*\[Omega]*h1[r]*(-(r*B[r]*Derivative[1][A][r]*
       (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])) + 
     A[r]*(r^2*Derivative[1][B][r] - r*B[r]*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) - 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
           r]))))/(r^2*A[r]) - 
  (h0[r]*(-(r*B[r]*Derivative[1][A][r]^2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])) + 
     A[r]^2*(-4 + 4*r*Derivative[1][B][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] - 
       4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]) + 
       B[r]*(4 - 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r] + (r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
          Derivative[1][\[CurlyPhi]][r]^2 + 4*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[2][\[CurlyPhi]][r])) + 
     A[r]*(2*r*B[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[2][A][r] + 
       Derivative[1][A][r]*(r*Derivative[1][B][r]*
          (r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
             r]) - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
          (-Derivative[1][\[CurlyPhi]][r] + r*Derivative[1][\[CurlyPhi]][r]^
             2 + r*Derivative[2][\[CurlyPhi]][r])))))/(4*r^2*A[r]^2), 
 ((-1/4*I)*\[Omega]*B[r]*Derivative[1][h1][r]*
    (E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
      Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
     A[r]*(4*r*pi[2][0][r] - 2*r*pi[2][2][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
           8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r])))))/(r*A[r]) - 
  (B[r]*Derivative[2][h0][r]*(E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
     A[r]*(4*r*pi[2][0][r] - 2*r*pi[2][2][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][r] - 
           8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r])))))/(4*r*A[r]) - 
  (\[ScriptL]*(1 + \[ScriptL])*h0[r]*
    ((2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
       wi[1][1][r]^2)/A[r] + (-8*ki[2][0][r] + 4*ki[2][2][r] - 
       E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][r]*
          (4*ki[2][0][r] - 2*ki[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r] - 
           2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]*
          (2*Derivative[1][pi[2][0]][r] - Derivative[1][pi[2][2]][r] - 
           4*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
         2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
         Derivative[2][\[CurlyPhi]i[2][2]][r] + 4*Derivative[2][\[CurlyPhi]][
           r]*ki[2][0][r] - 2*Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] + 
         4*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
         2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
         2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         Derivative[1][\[CurlyPhi]][r]^2*(4*ki[2][0][r] - 2*ki[2][2][r] + 
           4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r])))/r^2))/8 - 
  (Derivative[1][h0][r]*(-(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*
       Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
       Derivative[1][wi[1][1]][r]*wi[1][1][r]) + 
     A[r]^2*(2*r^2*Derivative[1][B][r]*(2*pi[2][0][r] - pi[2][2][r]) + 
       r*B[r]*(4*r*Derivative[1][hi[2][0]][r] - 2*r*Derivative[1][hi[2][2]][
           r] + 6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][0]][r] - 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][2]][r] + 
         4*r*Derivative[1][pi[2][0]][r] - 2*r*Derivative[1][pi[2][2]][r] + 
         6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]i[2][0]][r] - 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         24*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 12*E^\[CurlyPhi][r]*
          \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          pi[2][2][r] + 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] - 
         3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (-2*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
         Derivative[1][\[CurlyPhi]i[2][2]][r] + 2*r^2*Derivative[1][ki[2][0]][
           r]*Derivative[2][\[CurlyPhi]][r] - r^2*Derivative[1][ki[2][2]][r]*
          Derivative[2][\[CurlyPhi]][r] + 
         2*r*Derivative[2][\[CurlyPhi]i[2][0]][r] - 
         r*Derivative[2][\[CurlyPhi]i[2][2]][r] - 
         8*r*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] + 
         4*r*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
         2*r*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2*(2*r*Derivative[1][ki[2][0]][r] - 
           r*Derivative[1][ki[2][2]][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]) - 
         r*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         Derivative[1][\[CurlyPhi]][r]*(-2*r*Derivative[1][hi[2][0]][r] + 
           r*Derivative[1][hi[2][2]][r] - 6*r*Derivative[1][pi[2][0]][r] + 
           3*r*Derivative[1][pi[2][2]][r] + 
           4*r*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           2*r^2*Derivative[2][ki[2][0]][r] - r^2*Derivative[2][ki[2][2]][
             r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r]))) + r*A[r]*B[r]*
      (2*r*Derivative[1][A][r]*(-2*pi[2][0][r] + pi[2][2][r]) + 
       r^3*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
        wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*r^2*(r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][
             r] + r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][
             r] + Derivative[1][\[CurlyPhi]][r]*
            (2*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]))*
          wi[1][1][r] + Derivative[1][A][r]*
          (-2*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
           Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(-2*r*Derivative[1][ki[2][0]][r] + r*Derivative[1][ki[2][2]][
               r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
             \[CurlyPhi]i[2][2][r]))))))/(8*r^2*A[r]^2) - 
  ((I/8)*\[Omega]*h1[r]*(-(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*
       Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
       Derivative[1][wi[1][1]][r]*wi[1][1][r]) + 
     A[r]^2*(2*r^2*Derivative[1][B][r]*(2*pi[2][0][r] - pi[2][2][r]) + 
       r*B[r]*(4*r*Derivative[1][hi[2][0]][r] - 2*r*Derivative[1][hi[2][2]][
           r] - 8*r*Derivative[1][ki[2][0]][r] + 6*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][ki[2][0]][r] + 4*r*Derivative[1][ki[2][2]][r] - 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][2]][r] + 
         4*r*Derivative[1][pi[2][0]][r] - 2*r*Derivative[1][pi[2][2]][r] + 
         6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]i[2][0]][r] - 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         16*pi[2][0][r] - 24*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] - 8*pi[2][2][r] + 
         12*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 6*E^\[CurlyPhi][r]*
          \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][0][r] - 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] + 2*r^2*Derivative[1][ki[2][0]][
           r]*Derivative[2][\[CurlyPhi]][r] - r^2*Derivative[1][ki[2][2]][r]*
          Derivative[2][\[CurlyPhi]][r] + 
         2*r*Derivative[2][\[CurlyPhi]i[2][0]][r] - 
         r*Derivative[2][\[CurlyPhi]i[2][2]][r] - 
         8*r*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] + 
         4*r*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
         2*r*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2*(2*r*Derivative[1][ki[2][0]][r] - 
           r*Derivative[1][ki[2][2]][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]) + 
         Derivative[1][\[CurlyPhi]][r]*(-2*r*Derivative[1][hi[2][0]][r] + 
           r*Derivative[1][hi[2][2]][r] + 8*r*Derivative[1][ki[2][0]][r] - 
           4*r*Derivative[1][ki[2][2]][r] - 6*r*Derivative[1][pi[2][0]][r] + 
           3*r*Derivative[1][pi[2][2]][r] + 
           4*r*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           2*r^2*Derivative[2][ki[2][0]][r] - r^2*Derivative[2][ki[2][2]][
             r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r]) - r*Derivative[2][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r])) + r*A[r]*B[r]*
      (2*r*Derivative[1][A][r]*(-2*pi[2][0][r] + pi[2][2][r]) + 
       r^3*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
        wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (2*r^2*(r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][
             r] + r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][
             r] + Derivative[1][\[CurlyPhi]][r]*
            (4*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]))*
          wi[1][1][r] + Derivative[1][A][r]*
          (-2*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
           Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(-2*r*Derivative[1][ki[2][0]][r] + r*Derivative[1][ki[2][2]][
               r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
             \[CurlyPhi]i[2][2][r]))))))/(r^2*A[r]^2) - 
  (h0[r]*(r^2*B[r]*Derivative[1][A][r]*(2*r*Derivative[1][A][r]*
        (2*pi[2][0][r] - pi[2][2][r]) + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (r^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
          (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) + 
         Derivative[1][A][r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][
               r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
             \[CurlyPhi]i[2][2][r])))) + 
     2*A[r]^2*(4*r*(2*ki[2][0][r] + 2*ki[2][2][r] + 3*pi[2][2][r]) + 
       Derivative[1][B][r]*(2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][r] - 
         r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
             r])*Derivative[1][hi[2][2]][r] + 4*r^3*Derivative[1][ki[2][0]][
           r] - 12*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][0]][r] - 
         2*r^3*Derivative[1][ki[2][2]][r] + 6*E^\[CurlyPhi][r]*r^2*\[Alpha]*
          B[r]*Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][2]][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][0]][
           r] - E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][
           r] + 3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]i[2][2]][r] - 4*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[1][\[CurlyPhi]][r]*ki[2][0][r] - 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          ki[2][2][r] - 8*r^2*pi[2][0][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 24*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
         4*r^2*pi[2][2][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 12*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][0][r] - 6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] - 
         6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] - 
         E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r] + 3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) - 
       2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*(2*r*Derivative[1][hi[2][0]][r]*
          Derivative[2][\[CurlyPhi]][r] - r*Derivative[1][hi[2][2]][r]*
          Derivative[2][\[CurlyPhi]][r] + 4*r*Derivative[1][ki[2][0]][r]*
          Derivative[2][\[CurlyPhi]][r] - 2*r*Derivative[1][ki[2][2]][r]*
          Derivative[2][\[CurlyPhi]][r] + Derivative[1][\[CurlyPhi]][r]*
          (-2*Derivative[1][hi[2][0]][r] + Derivative[1][hi[2][2]][r] + 
           8*Derivative[1][ki[2][0]][r] - 4*Derivative[1][ki[2][2]][r] - 
           6*Derivative[1][pi[2][0]][r] + 3*Derivative[1][pi[2][2]][r] + 
           4*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           2*r*Derivative[2][hi[2][0]][r] - r*Derivative[2][hi[2][2]][r] + 
           4*r*Derivative[2][ki[2][0]][r] - 2*r*Derivative[2][ki[2][2]][r]) + 
         2*Derivative[2][\[CurlyPhi]i[2][0]][r] - 
         Derivative[2][\[CurlyPhi]i[2][2]][r] - 8*Derivative[2][\[CurlyPhi]][
           r]*pi[2][0][r] + 4*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
         2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         Derivative[1][\[CurlyPhi]][r]^2*(2*r*Derivative[1][hi[2][0]][r] - 
           r*Derivative[1][hi[2][2]][r] + 4*r*Derivative[1][ki[2][0]][r] - 
           2*r*Derivative[1][ki[2][2]][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
           2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]) - 
         Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) - 
       B[r]*(-24*r^2*Derivative[1][ki[2][0]][r] + 
         12*r^2*Derivative[1][ki[2][2]][r] + 8*r^2*Derivative[1][pi[2][0]][
           r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][pi[2][0]][r] - 4*r^2*Derivative[1][pi[2][2]][r] - 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][pi[2][2]][r] - 2*r^3*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][\[CurlyPhi]i[2][0]][r] - 8*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][\[CurlyPhi]i[2][0]][r] + 
         r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][2]][
           r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         4*r^3*Derivative[2][hi[2][0]][r] + 2*r^3*Derivative[2][hi[2][2]][
           r] - 8*r^3*Derivative[2][ki[2][0]][r] + 
         4*r^3*Derivative[2][ki[2][2]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[2][\[CurlyPhi]i[2][0]][r] + 2*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[2][\[CurlyPhi]i[2][2]][r] + 
         8*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*
          ki[2][0][r] + 8*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[2][\[CurlyPhi]][r]*ki[2][0][r] + 8*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*ki[2][2][r] + 
         8*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
          ki[2][2][r] + 8*r*pi[2][0][r] + 2*r^3*Derivative[1][\[CurlyPhi]][r]^
           2*pi[2][0][r] + 8*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]^2*pi[2][0][r] + 8*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
         4*r*pi[2][2][r] + 12*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 
         r^3*Derivative[1][\[CurlyPhi]][r]^2*pi[2][2][r] - 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*
          pi[2][2][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 4*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*\[CurlyPhi]i[2][0][r] - 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][0][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]^2*\[CurlyPhi]i[2][2][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r])) - 
     r*A[r]*(2*r^2*Derivative[1][A][r]*Derivative[1][B][r]*
        (2*pi[2][0][r] - pi[2][2][r]) + 
       r*B[r]*(r*(r^2*(2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]^2 + 
           4*Derivative[2][A][r]*(2*pi[2][0][r] - pi[2][2][r]) + 
           2*r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
            wi[1][1][r]) + Derivative[1][A][r]*
          (-8*r*Derivative[1][hi[2][0]][r] + 4*r*Derivative[1][hi[2][2]][r] + 
           6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][0]][r] - 
           3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][2]][r] + 
           4*r*Derivative[1][pi[2][0]][r] - 2*r*Derivative[1][pi[2][2]][r] + 
           6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]i[2][0]][r] - 3*E^\[CurlyPhi][r]*
            \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]i[2][2]][
             r] - 24*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 12*E^\[CurlyPhi][r]*
            \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
            pi[2][2][r] + 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] - 
           3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r])) + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][A][r]*
          (-2*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
           Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           2*r^2*Derivative[1][ki[2][0]][r]*Derivative[2][\[CurlyPhi]][r] - 
           r^2*Derivative[1][ki[2][2]][r]*Derivative[2][\[CurlyPhi]][r] + 
           2*r*Derivative[2][\[CurlyPhi]i[2][0]][r] - 
           r*Derivative[2][\[CurlyPhi]i[2][2]][r] - 
           8*r*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] + 
           4*r*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
           2*r*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
           r*Derivative[1][\[CurlyPhi]][r]^2*(2*r*Derivative[1][ki[2][0]][
               r] - r*Derivative[1][ki[2][2]][r] - 8*pi[2][0][r] + 
             4*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][
              r]) - r*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
           Derivative[1][\[CurlyPhi]][r]*(4*r*Derivative[1][hi[2][0]][r] - 
             2*r*Derivative[1][hi[2][2]][r] - 6*r*Derivative[1][pi[2][0]][
               r] + 3*r*Derivative[1][pi[2][2]][r] + 
             4*r*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
             2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             2*r^2*Derivative[2][ki[2][0]][r] - r^2*Derivative[2][ki[2][2]][
               r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
             \[CurlyPhi]i[2][2][r])) + 
         r*(2*Derivative[1][\[CurlyPhi]i[2][0]][r]*Derivative[2][A][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r]*Derivative[2][A][r] + 
           r^3*Derivative[1][wi[1][1]][r]^2*Derivative[2][\[CurlyPhi]][r] + 
           2*r^2*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r]*
            wi[1][1][r] + r^2*Derivative[1][\[CurlyPhi]][r]^2*
            Derivative[1][wi[1][1]][r]*(r*Derivative[1][wi[1][1]][r] + 
             2*wi[1][1][r]) + Derivative[1][\[CurlyPhi]][r]*
            (3*r^2*Derivative[1][wi[1][1]][r]^2 + 2*r*Derivative[1][ki[2][0]][
               r]*Derivative[2][A][r] - r*Derivative[1][ki[2][2]][r]*
              Derivative[2][A][r] - 8*Derivative[2][A][r]*pi[2][0][r] + 
             4*Derivative[2][A][r]*pi[2][2][r] + 2*r^2*Derivative[2][
                wi[1][1]][r]*wi[1][1][r] + 2*Derivative[1][wi[1][1]][r]*
              (r^3*Derivative[2][wi[1][1]][r] + 3*r*wi[1][1][r]) + 
             2*Derivative[2][A][r]*\[CurlyPhi]i[2][0][r] - 
             Derivative[2][A][r]*\[CurlyPhi]i[2][2][r]))))))/(8*r^3*A[r]^2), 
 (-3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]1][r]*
    Derivative[1][wi[1][1]][r])/(2*r) - 
  (K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
      Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
    wi[1][1][r])/2 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*
    Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
     A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r*A[r]) - 
  (H2[r]*(2*wi[1][1][r] - (E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*(2*A[r]*Derivative[1][wi[1][1]][r] + 
        Derivative[1][A][r]*wi[1][1][r]))/A[r]))/4 - 
  (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
    (A[r]^2*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r] + 
       2*B[r]*(3*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
          Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r])) + 
     r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] - 
     r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
       B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
         2*Derivative[2][A][r]*wi[1][1][r]))))/(4*r^2*A[r]^2) + 
  (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
    (r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] + 
     A[r]^2*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
         r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])) - 
     r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
       B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
         2*Derivative[2][A][r]*wi[1][1][r]))))/(4*r^2*A[r]^2), 
 (((3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*h1[r]*
    (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
     r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))/
   r^3 - (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][h0][r]*
    (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
     r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))/
   (2*r^3) + ((I/4)*\[Omega]*B[r]*Derivative[1][h1][r]*
    (E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
      Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
     3*A[r]*(2*r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*
        \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][
            r])))))/(r^2*A[r]) + 
  (B[r]*Derivative[2][h0][r]*(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
     3*A[r]*(2*r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*
        \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
          (r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][
            r])))))/(4*r^2*A[r]) - (\[ScriptL]*(1 + \[ScriptL])*h0[r]*
    (-1/2*((-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
        wi[1][1][r]^2)/A[r] + 
     (3*(-4*ki[2][2][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]][r]*(Derivative[1][pi[2][2]][r] - 
            2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
          Derivative[2][\[CurlyPhi]i[2][2]][r] + 
          Derivative[1][\[CurlyPhi]][r]^2*(2*ki[2][2][r] + 2*pi[2][2][r] - 
            \[CurlyPhi]i[2][2][r]) + Derivative[2][\[CurlyPhi]][r]*
           (2*ki[2][2][r] + 2*pi[2][2][r] - \[CurlyPhi]i[2][2][r])) - 
        E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
            r]*(-2*ki[2][2][r] - 2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/
      (4*r^2)))/2 - 
  (3*h0[r]*(r*A[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r])*hi[2][2][r] - 
     E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (-2*r*A[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r])*hi[2][2][r] + 
       Derivative[1][A][r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         \[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
          (-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/(2*r^3*A[r]) + 
  ((I/8)*\[Omega]*h1[r]*(-(E^\[CurlyPhi][r]*r^5*\[Alpha]*B[r]^2*
       Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
       Derivative[1][wi[1][1]][r]*wi[1][1][r]) - 
     3*A[r]^2*(2*r*Derivative[1][B][r]*(r^2*pi[2][2][r] - 
         E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
       B[r]*(2*r^3*Derivative[1][hi[2][2]][r] + 
         r^3*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][2]][r] + 
         2*r^3*Derivative[1][pi[2][2]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]i[2][2]][r] + 3*E^\[CurlyPhi][r]*r^2*
          \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         8*r^2*pi[2][2][r] - 8*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 12*E^\[CurlyPhi][r]*r^2*
          \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          pi[2][2][r] - 8*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] + 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r] + 3*E^\[CurlyPhi][r]*r^2*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r]) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][ki[2][2]][r] - 
           4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]) + 
         Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][hi[2][2]][r]) + 
           4*r*Derivative[1][ki[2][2]][r] - 3*r*Derivative[1][pi[2][2]][r] + 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           r^2*Derivative[2][ki[2][2]][r] - 4*pi[2][2][r] + 
           \[CurlyPhi]i[2][2][r]) + r*(r*Derivative[1][ki[2][2]][r]*
            Derivative[2][\[CurlyPhi]][r] + Derivative[2][\[CurlyPhi]i[2][2]][
            r] + Derivative[2][\[CurlyPhi]][r]*(-4*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r])))) + r*A[r]*B[r]*
      (r^4*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
        wi[1][1][r] + 6*Derivative[1][A][r]*(r^2*pi[2][2][r] - 
         E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
       E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (2*r^2*(r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][
             r] + r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][
             r] + Derivative[1][\[CurlyPhi]][r]*
            (4*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]))*
          wi[1][1][r] + 3*Derivative[1][A][r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r]))))))/(r^3*A[r]^2) - 
  (Derivative[1][h0][r]*(E^\[CurlyPhi][r]*r^5*\[Alpha]*B[r]^2*
      Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
      Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
     r*A[r]*B[r]*(r^4*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
        wi[1][1][r] + 6*Derivative[1][A][r]*(r^2*pi[2][2][r] - 
         E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
       E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        (2*r^2*(r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][
             r] + r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][
             r] + Derivative[1][\[CurlyPhi]][r]*
            (2*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]))*
          wi[1][1][r] + 3*Derivative[1][A][r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r])))) + 
     3*A[r]^2*(2*r*Derivative[1][B][r]*(r^2*pi[2][2][r] - 
         E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
       2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (-Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][hi[2][2]][r]) - 
           3*r*Derivative[1][pi[2][2]][r] + 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           r^2*Derivative[2][ki[2][2]][r] + 4*pi[2][2][r] - 
           \[CurlyPhi]i[2][2][r]) + r*Derivative[1][\[CurlyPhi]][r]^2*
          (r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + \[CurlyPhi]i[2][2][
            r]) + r*(r*Derivative[1][ki[2][2]][r]*Derivative[2][\[CurlyPhi]][
             r] + Derivative[2][\[CurlyPhi]i[2][2]][r] + 
           Derivative[2][\[CurlyPhi]][r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][2][
              r]))) + B[r]*(2*r^3*Derivative[1][hi[2][2]][r] + 
         2*r^3*Derivative[1][pi[2][2]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]i[2][2]][r] + 8*E^\[CurlyPhi][r]*\[Alpha]*
          \[CurlyPhi]i[2][2][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         3*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r]))))))/(8*r^3*A[r]^2) - 
  (h0[r]*(-(r*B[r]*Derivative[1][A][r]*(-6*Derivative[1][A][r]*
         (r^2*pi[2][2][r] - E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][
            r]) + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         (r^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
           (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) - 
          3*Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
            Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][ki[2][2]][r] - 
              4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))) + 
     3*A[r]^2*(2*Derivative[1][B][r]*
        (r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
             r])*Derivative[1][hi[2][2]][r] + 
         2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][2]][r] + 
         E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][2]][
           r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          ki[2][2][r] - 4*r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 12*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
         6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] + 
         E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r] - 3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) - 
       2*(4*r*(2*ki[2][2][r] + 3*pi[2][2][r]) + 
         B[r]*(-12*r^2*Derivative[1][ki[2][2]][r] + 
           2*r*(2*r + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][
               r])*Derivative[1][pi[2][2]][r] - 
           r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][
               2]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][2]][
             r] - 2*r^3*Derivative[2][hi[2][2]][r] - 
           4*r^3*Derivative[2][ki[2][2]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[2][\[CurlyPhi]i[2][2]][r] - 8*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*ki[2][2][r] - 
           8*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
            ki[2][2][r] + 4*r*pi[2][2][r] - 12*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r]^2*pi[2][2][r] + 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*
            pi[2][2][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 2*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*\[CurlyPhi]i[2][2][r] - 
           2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][2][r]) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
          (r*Derivative[1][hi[2][2]][r]*Derivative[2][\[CurlyPhi]][r] + 
           2*r*Derivative[1][ki[2][2]][r]*Derivative[2][\[CurlyPhi]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(-Derivative[1][hi[2][2]][r] + 
             4*Derivative[1][ki[2][2]][r] - 3*Derivative[1][pi[2][2]][r] + 
             2*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             r*Derivative[2][hi[2][2]][r] + 2*r*Derivative[2][ki[2][2]][r]) + 
           Derivative[2][\[CurlyPhi]i[2][2]][r] - 
           4*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
           Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
           Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][hi[2][2]][r] + 
             2*r*Derivative[1][ki[2][2]][r] - 4*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r])))) + 
     A[r]*(-6*r^3*Derivative[1][A][r]*Derivative[1][B][r]*pi[2][2][r] + 
       6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][A][r]*Derivative[1][B][r]*
        \[CurlyPhi]i[2][2][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        (r*(-3*Derivative[1][\[CurlyPhi]i[2][2]][r]*Derivative[2][A][r] + 
           r^2*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r]*
            (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) + 
           r^2*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r]*
            (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) + 
           Derivative[1][\[CurlyPhi]][r]*(3*r^2*Derivative[1][wi[1][1]][r]^
               2 - 3*r*Derivative[1][ki[2][2]][r]*Derivative[2][A][r] + 
             12*Derivative[2][A][r]*pi[2][2][r] + 2*r^2*Derivative[2][
                wi[1][1]][r]*wi[1][1][r] + 2*Derivative[1][wi[1][1]][r]*
              (r^3*Derivative[2][wi[1][1]][r] + 3*r*wi[1][1][r]) - 
             3*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r])) - 
         3*Derivative[1][A][r]*(-Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][hi[2][2]][r] - 
             3*r*Derivative[1][pi[2][2]][r] + 2*r*Derivative[1][
                \[CurlyPhi]i[2][2]][r] + r^2*Derivative[2][ki[2][2]][r] + 
             4*pi[2][2][r] - \[CurlyPhi]i[2][2][r]) + 
           r*Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][ki[2][2]][r] - 
             4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]) + 
           r*(r*Derivative[1][ki[2][2]][r]*Derivative[2][\[CurlyPhi]][r] + 
             Derivative[2][\[CurlyPhi]i[2][2]][r] + 
             Derivative[2][\[CurlyPhi]][r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][
                 2][r])))) + 
       B[r]*(r^5*(2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]^2 + 
         2*r^4*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
          wi[1][1][r] - 12*r*Derivative[2][A][r]*(r^2*pi[2][2][r] - 
           E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
         3*Derivative[1][A][r]*(4*r^3*Derivative[1][hi[2][2]][r] - 
           2*r^3*Derivative[1][pi[2][2]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]i[2][2]][r] - 8*E^\[CurlyPhi][r]*
            \[Alpha]*\[CurlyPhi]i[2][2][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] - 
           3*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
            (Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][ki[2][2]][r] - 4*
                pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))))/(8*r^3*A[r]^2), 
 (\[ScriptL]*(1 + \[ScriptL])*
    (((-1/2*I)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
          Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
       Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
        A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) + 
     (B[r]*h1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2*
         Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
        A[r]^2*(Derivative[1][wi[1][1]][r]*
           (r*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
               Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
             B[r]*(3*Derivative[1][\[CurlyPhi]][r] + 
              r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][
                 \[CurlyPhi]][r])) + 2*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
             Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
          2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]][r]*
             Derivative[2][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
             wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])) - 
        r*A[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
             Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
          E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
             Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
             (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
              2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
              2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*r*A[r]^2)))/
   2 + (\[ScriptL]*(1 + \[ScriptL])*
    (((I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
          Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
       Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][A][r]*wi[1][1][r] - 
        A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) - 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]*h1[r]*(r^2*B[r]*Derivative[1][A][r]^2*
         Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
        A[r]^2*(3*r*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
           (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) + 
          2*B[r]*(Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][wi[1][1]][
                r] + r^2*Derivative[2][wi[1][1]][r] - 2*wi[1][1][r]) + 
            r*Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][wi[1][1]][r] + 
              2*wi[1][1][r]) + r*Derivative[2][\[CurlyPhi]][r]*
             (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]))) - 
        r*A[r]*(3*r*Derivative[1][A][r]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
          B[r]*(2*r*Derivative[1][\[CurlyPhi]][r]*Derivative[2][A][r]*
             wi[1][1][r] + Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][
                r]*(r*Derivative[1][wi[1][1]][r] - 2*wi[1][1][r]) + 
              2*r*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
              2*r*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/
      (4*r^2*A[r]^2)))/2, 
 (-3*B[r]*Derivative[1][H1][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r])*hi[2][2][r])/r + 
  (H1[r]*(-3*r*B[r]*Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r])*hi[2][2][r] + 
     A[r]*(-3*r^2*Derivative[1][B][r]*hi[2][2][r] - 
       3*r*B[r]*(2*r*Derivative[1][hi[2][2]][r] - 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r]) + 
       6*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]*
          (r*Derivative[1][hi[2][2]][r] - hi[2][2][r]) + 
         r*Derivative[1][\[CurlyPhi]][r]^2*hi[2][2][r] + 
         r*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r]))))/(2*r^2*A[r]) + 
  ((3*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][\[CurlyPhi]1][
     r]*(r*Derivative[1][ki[2][2]][r] - pi[2][2][r]))/r^2 + 
  (3*E^\[CurlyPhi][r]*\[ScriptL]*(1 + \[ScriptL])*\[Alpha]*B[r]*H1[r]*
    (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
     r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))/
   (4*r^3) + (((3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*
    Derivative[1][K][r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
     \[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
      (-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))/r - 
  (((3*I)/2)*\[Omega]*H2[r]*(r^2*(hi[2][2][r] + pi[2][2][r]) + 
     E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
       \[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
        (r*Derivative[1][ki[2][2]][r] - hi[2][2][r] - 3*pi[2][2][r] + 
         \[CurlyPhi]i[2][2][r]))))/r^2 + 
  (((3*I)/4)*\[Omega]*\[CurlyPhi]1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
      (r^3*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
        (-2*r*Derivative[1][ki[2][2]][r] + 2*pi[2][2][r])) + 
     2*A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (-1 + r*Derivative[1][\[CurlyPhi]][r])*
        (r*Derivative[1][ki[2][2]][r] - pi[2][2][r]) + 
       r*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*(hi[2][2][r] - 
           \[CurlyPhi]i[2][2][r]) + r*\[CurlyPhi]i[2][2][r]))))/(r^3*A[r]) + 
  (((3*I)/4)*\[Omega]*K[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
       Derivative[1][A][r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
        \[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
         (-pi[2][2][r] + \[CurlyPhi]i[2][2][r]))) + 
     A[r]*(r*(r*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*hi[2][2][r] + 
         2*r*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*ki[2][2][r] - 2*r*pi[2][2][r] + 
         E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*\[CurlyPhi]i[2][2][
           r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         r^2*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r] + 
         2*r^2*Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] + 
         r^2*Derivative[1][\[CurlyPhi]][r]^2*(hi[2][2][r] + 2*ki[2][2][r]) - 
         \[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r]))))/(r^2*A[r]), 
 (-1/4*(H0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
     wi[1][1][r]) - (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*
     Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r*A[r]) - 
   (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
     (r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] + 
      A[r]^2*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
          r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
         (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])) - 
      r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
        B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
          2*Derivative[2][A][r]*wi[1][1][r]))))/(4*r^2*A[r]^2))/2, 
 ((((-3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*h1[r]*
     (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
      r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][
         r])))/r^3 + (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][h0][r]*
     (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
      r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][
         r])))/(2*r^3) + 
   (3*h0[r]*(r*A[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         Derivative[1][\[CurlyPhi]][r])*hi[2][2][r] - 
      E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (-2*r*A[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
          Derivative[2][\[CurlyPhi]][r])*hi[2][2][r] + Derivative[1][A][r]*
         (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
          r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][
             r])))))/(2*r^3*A[r]))/2, 
 (((-1/2*I)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
       Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
      2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
        Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
   (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
     Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) + 
   (B[r]*h1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2*
       Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
      A[r]^2*(Derivative[1][wi[1][1]][r]*
         (r*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
           B[r]*(3*Derivative[1][\[CurlyPhi]][r] + 
            r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
              r])) + 2*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]][r]*
           Derivative[2][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
           wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])) - 
      r*A[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
           Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
           Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
           (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
            2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
            2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*r*A[r]^2))/2, 
 (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*H1[r]*
   (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
    r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))/
  (4*r^3)}
