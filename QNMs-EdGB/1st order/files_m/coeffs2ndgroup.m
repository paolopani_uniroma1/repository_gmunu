{((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
    Derivative[1][B][r])/r^2 - 
  ((I/2)*\[Omega]*H2[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/r - 
  (B[r]*Derivative[1][H1][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/(2*r) + 
  (I/4)*\[Omega]*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
     Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
     (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])) - 
  (H1[r]*(r^2*A[r]*Derivative[1][B][r] + r*B[r]*(r*Derivative[1][A][r] - 
       3*E^\[CurlyPhi][r]*\[Alpha]*A[r]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r]) - E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*A[r]*(-Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
           r]))))/(4*r^2*A[r]), 
 a*(((-I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
    (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
      Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
       A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) + 
    (B[r]*h1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2*
        Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
       A[r]^2*(Derivative[1][wi[1][1]][r]*
          (r*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
            B[r]*(3*Derivative[1][\[CurlyPhi]][r] + 
             r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
               r])) + 2*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
         2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]][r]*
            Derivative[2][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
            wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])) - 
       r*A[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
         E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
            Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
            (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
             2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
             2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*r*A[r]^2)) - 
  a*(((-I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
    (B[r]*Derivative[1][h1][r]*(2*wi[1][1][r] + 
       (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
         (A[r]*Derivative[1][wi[1][1]][r] - Derivative[1][A][r]*wi[1][1][r]))/
        A[r]))/2 + (h1[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        Derivative[1][A][r]^2*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
       A[r]^2*(B[r]*(4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
         2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]^2*
            Derivative[1][wi[1][1]][r] + Derivative[1][wi[1][1]][r]*
            Derivative[2][\[CurlyPhi]][r] + Derivative[1][\[CurlyPhi]][r]*
            Derivative[2][wi[1][1]][r]) + 2*Derivative[1][B][r]*
          wi[1][1][r]) - A[r]*B[r]*(Derivative[1][A][r]*
          (-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
         E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
            Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
            (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
             2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
             2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*A[r]^2)), 
 ((I/2)*\[Omega]*B[r]*Derivative[1][h1][r]*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/r + 
  (B[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
    Derivative[2][h0][r])/(2*r) + 
  (\[CapitalLambda]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
      Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
   (2*r^2) - (Derivative[1][h0][r]*(-(r^2*A[r]*Derivative[1][B][r]) + 
     r*B[r]*(r*Derivative[1][A][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*A[r]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) + 
     E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (-(r*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]) + 
       2*A[r]*(-Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
           r]))))/(4*r^2*A[r]) + 
  ((I/4)*\[Omega]*h1[r]*(-(r*B[r]*Derivative[1][A][r]*
       (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])) + 
     A[r]*(r^2*Derivative[1][B][r] - r*B[r]*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) - 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
           r]))))/(r^2*A[r]) - 
  (h0[r]*(-(r*B[r]*Derivative[1][A][r]^2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])) + 
     A[r]^2*(-4 + 4*r*Derivative[1][B][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] - 
       4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]) + 
       B[r]*(4 - 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r] + (r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
          Derivative[1][\[CurlyPhi]][r]^2 + 4*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[2][\[CurlyPhi]][r])) + 
     A[r]*(2*r*B[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[2][A][r] + 
       Derivative[1][A][r]*(r*Derivative[1][B][r]*
          (r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
             r]) - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
          (-Derivative[1][\[CurlyPhi]][r] + r*Derivative[1][\[CurlyPhi]][r]^
             2 + r*Derivative[2][\[CurlyPhi]][r])))))/(4*r^2*A[r]^2), 
 -(a*((3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]1][r]*
      Derivative[1][wi[1][1]][r])/(2*r) + 
    (K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
      wi[1][1][r])/2 - (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*
      Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
       A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r*A[r]) + 
    (H2[r]*(2*wi[1][1][r] - (E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(2*A[r]*Derivative[1][wi[1][1]][r] + 
          Derivative[1][A][r]*wi[1][1][r]))/A[r]))/4 + 
    (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
      (A[r]^2*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r] + 
         2*B[r]*(3*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
            Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r])) + 
       r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] - 
       r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
         B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
           2*Derivative[2][A][r]*wi[1][1][r]))))/(4*r^2*A[r]^2) - 
    (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
      (r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] + 
       A[r]^2*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
           r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
          (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])) - 
       r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
         B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
           2*Derivative[2][A][r]*wi[1][1][r]))))/(4*r^2*A[r]^2))), 
 a*(\[CapitalLambda]*(((-I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
          Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
       Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
        A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) + 
     (B[r]*h1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2*
         Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
        A[r]^2*(Derivative[1][wi[1][1]][r]*
           (r*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
               Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
             B[r]*(3*Derivative[1][\[CurlyPhi]][r] + 
              r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][
                 \[CurlyPhi]][r])) + 2*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
             Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
          2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]][r]*
             Derivative[2][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
             wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])) - 
        r*A[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
             Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
          E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
             Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
             (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
              2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
              2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/
      (4*r*A[r]^2)) + \[CapitalLambda]*
    (((I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
          Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
       Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][A][r]*wi[1][1][r] - 
        A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) - 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]*h1[r]*(r^2*B[r]*Derivative[1][A][r]^2*
         Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
        A[r]^2*(3*r*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
           (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) + 
          2*B[r]*(Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][wi[1][1]][
                r] + r^2*Derivative[2][wi[1][1]][r] - 2*wi[1][1][r]) + 
            r*Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][wi[1][1]][r] + 
              2*wi[1][1][r]) + r*Derivative[2][\[CurlyPhi]][r]*
             (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]))) - 
        r*A[r]*(3*r*Derivative[1][A][r]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
          B[r]*(2*r*Derivative[1][\[CurlyPhi]][r]*Derivative[2][A][r]*
             wi[1][1][r] + Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][
                r]*(r*Derivative[1][wi[1][1]][r] - 2*wi[1][1][r]) + 
              2*r*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
              2*r*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/
      (4*r^2*A[r]^2))), 
 -(a*((H0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
       wi[1][1][r])/4 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*
       Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
        A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r*A[r]) + 
     (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
       (r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] + 
        A[r]^2*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
            r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
           (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])) - 
        r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
          B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
            2*Derivative[2][A][r]*wi[1][1][r]))))/(4*r^2*A[r]^2)))/2, 
 (a*(((-I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
    (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][h1][r]*
      Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
       A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(2*r*A[r]) + 
    (B[r]*h1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2*
        Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
       A[r]^2*(Derivative[1][wi[1][1]][r]*
          (r*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
            B[r]*(3*Derivative[1][\[CurlyPhi]][r] + 
             r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
               r])) + 2*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
         2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]][r]*
            Derivative[2][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
            wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])) - 
       r*A[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
         E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]][r]*
            Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
            (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
             2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
             2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*r*A[r]^2)))/
  2, a*(((-I)*r*\[Omega]*B[r]*Derivative[1][H1][r]*
     (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
     wi[1][1][r])/A[r] - 
   (r*B[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
     Derivative[2][H0][r]*wi[1][1][r])/2 - 
   (r^2*\[Omega]^2*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
     wi[1][1][r])/(4*A[r]) - ((I/2)*r*\[Omega]*H1[r]*
     (r*Derivative[1][B][r] + B[r]*(2 - 3*E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) - 
      2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]^2 + 
        Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] + 
   (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[2][\[CurlyPhi]1][r]*
     (A[r]*Derivative[1][wi[1][1]][r] - Derivative[1][A][r]*wi[1][1][r]))/
    (2*A[r]) + (r*B[r]*Derivative[1][H2][r]*
     (A[r]*((r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
            r])*Derivative[1][wi[1][1]][r] - 2*wi[1][1][r]) - 
      Derivative[1][A][r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]))/(4*A[r]) + 
   (r*B[r]*Derivative[2][K][r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]) + 
      A[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
         (r*Derivative[1][wi[1][1]][r] - 2*wi[1][1][r]) + 4*r*wi[1][1][r])))/
    (4*A[r]) - (r*Derivative[1][H0][r]*(2*B[r]*Derivative[1][A][r]*
       (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
       wi[1][1][r] + A[r]*(r*Derivative[1][B][r]*wi[1][1][r] + 
        B[r]*(r*Derivative[1][wi[1][1]][r] - 
          (-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) - 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]*
           Derivative[1][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
           wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r]))))/
    (4*A[r]) + (B[r]*Derivative[1][\[CurlyPhi]1][r]*
     (E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2*wi[1][1][r] + 
      A[r]^2*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         ((1 + 2*r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][
            r] + r*Derivative[2][wi[1][1]][r]) + 
        r*(3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][wi[1][1]][r] + 2*r*Derivative[1][\[CurlyPhi]][r]*
           wi[1][1][r])) - E^\[CurlyPhi][r]*\[Alpha]*A[r]*
       (3*r*Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
        B[r]*(2*r*Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
           (r*Derivative[1][wi[1][1]][r] + 
            4*(-1 + r*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])))))/
    (4*r*A[r]^2) - \[CapitalLambda]*
    ((H0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
       wi[1][1][r])/2 - (K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
          Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/2 + 
     H2[r]*(wi[1][1][r] + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(A[r]*Derivative[1][wi[1][1]][r] - 
          Derivative[1][A][r]*wi[1][1][r]))/(2*A[r])) + 
     (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
       (A[r]^2*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r] + 
          2*B[r]*(3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][
              r])) + r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] - 
        r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
          B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
            2*Derivative[2][A][r]*wi[1][1][r]))))/(2*r^2*A[r]^2)) + 
   \[CapitalLambda]*
    ((H0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
       wi[1][1][r])/4 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*
       Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
        A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r*A[r]) + 
     (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
       (r*B[r]*Derivative[1][A][r]^2*wi[1][1][r] + 
        A[r]^2*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
            r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
           (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])) - 
        r*A[r]*(Derivative[1][A][r]*Derivative[1][B][r]*wi[1][1][r] + 
          B[r]*(Derivative[1][A][r]*Derivative[1][wi[1][1]][r] + 
            2*Derivative[2][A][r]*wi[1][1][r]))))/(4*r^2*A[r]^2)) - 
   (H2[r]*(-(r*B[r]*Derivative[1][A][r]^2*(r - 2*E^\[CurlyPhi][r]*\[Alpha]*
          B[r]*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) + 
      A[r]^2*(4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
         (r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r] + 
          r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r] + 
          Derivative[1][\[CurlyPhi]][r]*(3*Derivative[1][wi[1][1]][r] + 
            r*Derivative[2][wi[1][1]][r])) + r*Derivative[1][B][r]*
         (-(r*Derivative[1][wi[1][1]][r]) + 2*wi[1][1][r]) + 
        r*B[r]*((-8 + 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] - 
          2*r*Derivative[2][wi[1][1]][r] + r*Derivative[1][\[CurlyPhi]][r]^2*
           wi[1][1][r])) + r*A[r]*
       (r*(-2*\[Omega]^2 + Derivative[1][A][r]*Derivative[1][B][r])*
         wi[1][1][r] + B[r]*(2*(E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*
             Derivative[1][\[CurlyPhi]][r] + r*Derivative[2][A][r])*
           wi[1][1][r] + Derivative[1][A][r]*(r*Derivative[1][wi[1][1]][r] - 
            2*(-1 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
               Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])) - 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(2*Derivative[1][\[CurlyPhi]][r]*
           Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
           (Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] + 
            2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
            2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/(4*A[r]^2) + 
   (\[CurlyPhi]1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]^2*
       (-1 + r*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
      A[r]^2*B[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][wi[1][1]][r]*(-1 + r*Derivative[1][\[CurlyPhi]][r] + 
            r^2*Derivative[1][\[CurlyPhi]][r]^2 + 
            r^2*Derivative[2][\[CurlyPhi]][r]) + 
          r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[2][wi[1][1]][
            r]) + r*(3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           (-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][
            r] - 2*r*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r])) - 
      E^\[CurlyPhi][r]*\[Alpha]*A[r]*(2*r^2*\[Omega]^2*Derivative[1][B][r]*
         wi[1][1][r] + 3*r*B[r]*Derivative[1][A][r]*Derivative[1][B][r]*
         (-1 + r*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
        B[r]^2*(2*r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
           Derivative[2][A][r]*wi[1][1][r] + Derivative[1][A][r]*
           (r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][
              r] + 2*(2 - 2*r*Derivative[1][\[CurlyPhi]][r] + 
              r^2*Derivative[1][\[CurlyPhi]][r]^2 + r^2*Derivative[2][
                 \[CurlyPhi]][r])*wi[1][1][r])))))/(4*r^2*A[r]^2) + 
   (Derivative[1][K][r]*(E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*
       Derivative[1][A][r]^2*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
      A[r]^2*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
         (Derivative[1][\[CurlyPhi]][r]*(4*r*Derivative[1][wi[1][1]][r] + 
            r^2*Derivative[2][wi[1][1]][r] - 6*wi[1][1][r]) + 
          r*Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][wi[1][1]][r] - 
            2*wi[1][1][r]) + r*Derivative[2][\[CurlyPhi]][r]*
           (r*Derivative[1][wi[1][1]][r] - 2*wi[1][1][r])) + 
        4*r^2*Derivative[1][B][r]*wi[1][1][r] + 
        3*r*B[r]*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] - 
            2*wi[1][1][r]) + 8*wi[1][1][r])) - E^\[CurlyPhi][r]*r*\[Alpha]*
       A[r]*B[r]*(3*r*Derivative[1][A][r]*Derivative[1][B][r]*
         Derivative[1][\[CurlyPhi]][r]*wi[1][1][r] + 
        B[r]*(2*r*Derivative[1][\[CurlyPhi]][r]*Derivative[2][A][r]*
           wi[1][1][r] + Derivative[1][A][r]*
           (2*r*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r] + 
            2*r*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] + 
            Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
              2*wi[1][1][r]))))))/(8*A[r]^2)), 
 ((-I/2)*\[Omega]*H1[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/(r*A[r]) - 
  (H0[r]*(-2*A[r] + r*Derivative[1][A][r])*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/
   (4*r^2*A[r]) - (Derivative[1][H0][r]*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/
   (2*r) - (Derivative[1][K][r]*
    (-2 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
       Derivative[1][\[CurlyPhi]][r])/A[r]))/4 - 
  (\[CurlyPhi]1[r]*(-(r^2*A[r]*Derivative[1][\[CurlyPhi]][r]) + 
     E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
      (-2 + r*Derivative[1][\[CurlyPhi]][r])))/(2*r^3*A[r]) - 
  (H2[r]*(2*A[r] + Derivative[1][A][r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r])))/(4*r*A[r]) - 
  (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
    Derivative[1][\[CurlyPhi]1][r])/(2*r^2*A[r]), 
 -(a*((h0[r]*Derivative[1][wi[1][1]][r])/A[r] - 
     (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*h1[r]*
       Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r])/A[r] - 
     (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][h0][r]*
       Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r])/(2*A[r]))) + 
  a*((h0[r]*(-(r*A[r]*Derivative[1][wi[1][1]][r]) + Derivative[1][A][r]*
        (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
        wi[1][1][r]))/(2*r*A[r]^2) - 
    ((I/4)*\[Omega]*h1[r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
         2*wi[1][1][r])))/(r*A[r]) + 
    (Derivative[1][h0][r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
         2*wi[1][1][r])))/(4*r*A[r])), 
 (I*\[Omega]*h0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/(r^2*A[r]) - 
  ((I/2)*\[Omega]*Derivative[1][h0][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/(r*A[r]) - 
  (\[CapitalLambda]*h1[r]*(2 - (E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r])/A[r]))/(2*r^2) - 
  (h1[r]*(-(r*B[r]*Derivative[1][A][r]^2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])) + 
     A[r]^2*(-4 + 2*r*Derivative[1][B][r] + 
       r^2*B[r]*Derivative[1][\[CurlyPhi]][r]^2) + 
     A[r]*(r^2*(-2*\[Omega]^2 + Derivative[1][A][r]*Derivative[1][B][r]) + 
       B[r]*(Derivative[1][A][r]*(2*r - E^\[CurlyPhi][r]*\[Alpha]*
            (-2 + 3*r*Derivative[1][B][r])*Derivative[1][\[CurlyPhi]][r]) + 
         2*r*(E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*Derivative[1][\[CurlyPhi]][
             r] + r*Derivative[2][A][r])) - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
        B[r]^2*(Derivative[1][\[CurlyPhi]][r]*Derivative[2][A][r] + 
         Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
           Derivative[2][\[CurlyPhi]][r])))))/(4*r^2*A[r]^2), 
 -(a*((((-3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
      Derivative[1][wi[1][1]][r])/(r*A[r]) - 
    (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H1[r]*Derivative[1][\[CurlyPhi]][r]*
      Derivative[1][wi[1][1]][r])/(2*A[r]) - 
    (H1[r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
         2*wi[1][1][r])))/(4*r*A[r]))), 
 a*(\[CapitalLambda]*((h0[r]*(-(r*A[r]*Derivative[1][wi[1][1]][r]) + 
        Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]))/(2*r*A[r]^2) - 
     ((I/4)*\[Omega]*h1[r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
          2*wi[1][1][r])))/(r*A[r]) + 
     (Derivative[1][h0][r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
          2*wi[1][1][r])))/(4*r*A[r])) + \[CapitalLambda]*
    ((Derivative[1][h0][r]*(2*r*wi[1][1][r] - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
          2*wi[1][1][r])))/(r*A[r]) - 
     ((I/2)*\[Omega]*h1[r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
          2*wi[1][1][r])))/(r*A[r]) + 
     (h0[r]*(-(r*Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) + 
        A[r]*(r*(r + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] - 
          2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
              r])*wi[1][1][r])))/(r^2*A[r]^2))), 
 -(a*H1[r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
       2*wi[1][1][r])))/(8*r*A[r]), 
 (a*((h0[r]*(-(r*A[r]*Derivative[1][wi[1][1]][r]) + Derivative[1][A][r]*
        (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
        wi[1][1][r]))/(2*r*A[r]^2) - 
    ((I/4)*\[Omega]*h1[r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
         2*wi[1][1][r])))/(r*A[r]) + 
    (Derivative[1][h0][r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
         2*wi[1][1][r])))/(4*r*A[r])))/2, 
 a*(-(E^\[CurlyPhi][r]*\[Alpha]*\[CapitalLambda]*B[r]*H1[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r])/(2*A[r]) - 
   ((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*\[CurlyPhi]1[r]*
     (-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r])/
    (r*A[r]) - ((I/4)*r*\[Omega]*H2[r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r])/A[r] + 
   ((I/4)*r*\[Omega]*H0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r])/A[r] + 
   ((I/2)*r*\[Omega]*K[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r])/A[r] - 
   ((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][\[CurlyPhi]1][
      r]*Derivative[1][wi[1][1]][r])/A[r] - 
   (H1[r]*(r*B[r]*Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
      A[r]*(-(r^2*Derivative[1][B][r]*Derivative[1][wi[1][1]][r]) - 
        r*B[r]*((8 - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
          2*r*Derivative[2][wi[1][1]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
         B[r]^2*(r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][
            r] + r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r] + 
          Derivative[1][\[CurlyPhi]][r]*(3*Derivative[1][wi[1][1]][r] + 
            r*Derivative[2][wi[1][1]][r])))))/(4*A[r]^2) - 
   ((I/4)*r*\[Omega]*Derivative[1][K][r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] - 
        2*wi[1][1][r]) + 2*r*wi[1][1][r]))/A[r] + 
   (\[CapitalLambda]*H1[r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
        2*wi[1][1][r])))/(4*r*A[r]))}
