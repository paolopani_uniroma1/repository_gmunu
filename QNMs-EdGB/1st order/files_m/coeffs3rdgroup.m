{-(a*(((I/4)*E^\[CurlyPhi][r]*r^2*\[Alpha]*\[Omega]*B[r]*H2[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r])/A[r] + 
    (E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*Derivative[1][H1][r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r])/(4*A[r]) + 
    ((I/4)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
      (-(r*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]) + 
       A[r]*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r] + 
         2*B[r]*(3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][
             r]))))/A[r]^2 + (E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*H1[r]*
      (-(r*B[r]*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
         Derivative[1][wi[1][1]][r]) + 
       A[r]*(3*r*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][wi[1][1]][r] + 2*B[r]*
          (r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r] + 
           r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][wi[1][1]][r] + 
             r*Derivative[2][wi[1][1]][r])))))/(8*A[r]^2) - 
    ((I/4)*r^2*\[Omega]*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r])), 
 a*(((-I/4)*E^\[CurlyPhi][r]*r^2*\[Alpha]*\[Omega]*B[r]^2*
     Derivative[1][h1][r]*Derivative[1][\[CurlyPhi]][r]*
     Derivative[1][wi[1][1]][r])/A[r] - 
   (E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*Derivative[1][\[CurlyPhi]][r]*
     Derivative[1][wi[1][1]][r]*Derivative[2][h0][r])/(4*A[r]) - 
   (r*B[r]*Derivative[1][h0][r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[1][wi[1][1]][r]) + 
      A[r]*(Derivative[1][wi[1][1]][r]*
         (r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
           B[r]*(2*Derivative[1][\[CurlyPhi]][r] + 
            r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
              r])) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*Derivative[2][wi[1][1]][r])))/
    (8*A[r]^2) - ((I/8)*r*\[Omega]*B[r]*h1[r]*
     (-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]*
        Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]) + 
      A[r]*(Derivative[1][wi[1][1]][r]*
         (r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
           B[r]*(4*Derivative[1][\[CurlyPhi]][r] + 
            r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
              r])) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*Derivative[2][wi[1][1]][r])))/A[r]^2 + 
   (h0[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]*
        Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]) + 
      A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
         (r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r] + 
          r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r] + 
          Derivative[1][\[CurlyPhi]][r]*(3*Derivative[1][wi[1][1]][r] + 
            r*Derivative[2][wi[1][1]][r])) - \[ScriptL]*(1 + \[ScriptL])*
         (-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
        B[r]*(r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] - 
          2*E^\[CurlyPhi][r]*\[ScriptL]*(1 + \[ScriptL])*\[Alpha]*
           (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])*
           wi[1][1][r]))))/(4*A[r]^2)), 
 (-(H2[r]*(-2 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
         Derivative[1][\[CurlyPhi]][r])/A[r]))/4 - 
   (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
     (A[r]*Derivative[1][A][r]*Derivative[1][B][r] - 
      B[r]*(Derivative[1][A][r]^2 - 2*A[r]*Derivative[2][A][r])))/
    (4*r*A[r]^2) - (H0[r]*(2 - E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       Derivative[1][\[CurlyPhi]][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/4)/
  2, 
 (-(Derivative[1][h1][r]*(B[r] - (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r])/(2*A[r]))) + 
   ((I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
    A[r] - (h1[r]*(2*A[r]^2*Derivative[1][B][r] + E^\[CurlyPhi][r]*\[Alpha]*
       B[r]^2*Derivative[1][A][r]^2*Derivative[1][\[CurlyPhi]][r] - 
      A[r]*B[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
          r]*Derivative[2][A][r] + Derivative[1][A][r]*
         (-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][
             r])))))/(4*A[r]^2))/2}
