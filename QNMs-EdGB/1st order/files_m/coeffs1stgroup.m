{(A[r]*B[r]*Derivative[1][H2][r]*(2*r - E^\[CurlyPhi][r]*\[Alpha]*
      (-1 + 3*B[r])*Derivative[1][\[CurlyPhi]][r]))/(2*r^2) - 
  (A[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r] - 
     4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (-1 + r*Derivative[1][\[CurlyPhi]][r]) + 
     B[r]*(-4*E^\[CurlyPhi][r]*\[Alpha] - 3*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][B][r] + r*(r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
        Derivative[1][\[CurlyPhi]][r]))*Derivative[1][\[CurlyPhi]1][r])/
   (2*r^4) + (A[r]*B[r]*(-r + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r])*Derivative[2][K][r])/r + 
  (A[r]*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
      Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
   (2*r^2) - (A[r]*Derivative[1][K][r]*(r^2*Derivative[1][B][r] - 
     3*r*B[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r]) - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (2*Derivative[1][\[CurlyPhi]][r] + r*Derivative[1][\[CurlyPhi]][r]^2 + 
       r*Derivative[2][\[CurlyPhi]][r])))/(2*r^2) + 
  (A[r]*H2[r]*(2*Derivative[1][B][r]*(2*r + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]) - 8*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]) + 
     B[r]*(4 - 12*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r] + (r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
        Derivative[1][\[CurlyPhi]][r]^2 + 4*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[2][\[CurlyPhi]][r])))/(4*r^2) + 
  (A[r]*H0[r]*(-4 + 2*Derivative[1][B][r]*(2*r + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]) - 4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]) + 
     B[r]*(4 - 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r] + (r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
        Derivative[1][\[CurlyPhi]][r]^2 + 4*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[2][\[CurlyPhi]][r])))/(4*r^2) + 
  \[CapitalLambda]*(-((E^\[CurlyPhi][r]*\[Alpha]*A[r]*\[CurlyPhi]1[r]*
       Derivative[1][B][r])/r^4) + 
    (A[r]*H2[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]))/r^3 - 
    (A[r]*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
     (2*r^2)) + (A[r]*\[CurlyPhi]1[r]*
    (-(E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
       (-1 + r*Derivative[1][\[CurlyPhi]][r])) + 2*E^\[CurlyPhi][r]*\[Alpha]*
      B[r]^2*(2 - 2*r*Derivative[1][\[CurlyPhi]][r] + 
       r^2*Derivative[1][\[CurlyPhi]][r]^2 + r^2*Derivative[2][\[CurlyPhi]][
         r]) + B[r]*(r*(r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
        Derivative[1][\[CurlyPhi]][r] - 2*E^\[CurlyPhi][r]*r^2*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]^2 + 3*E^\[CurlyPhi][r]*r*\[Alpha]*
        Derivative[1][B][r]*(-1 + r*Derivative[1][\[CurlyPhi]][r]) - 
       2*E^\[CurlyPhi][r]*\[Alpha]*(2 + r^2*Derivative[2][\[CurlyPhi]][r]))))/
   (2*r^5) + (E^\[CurlyPhi][r]*\[Alpha]*A[r]*(-1 + B[r])*B[r]*
    Derivative[2][\[CurlyPhi]1][r])/r^3, 
 a*((I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*Derivative[1][B][r]*
     wi[1][1][r])/r^2 - (I*\[Omega]*H2[r]*
     (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
     wi[1][1][r])/r + (B[r]*Derivative[1][H1][r]*
     (-r + E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
     wi[1][1][r])/r + (I/2)*\[Omega]*K[r]*
    (-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
      Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
    wi[1][1][r] + 
   (H1[r]*(-(r*B[r]*Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) + 
      A[r]*(-(r^2*Derivative[1][B][r]*wi[1][1][r]) + 
        r*B[r]*(r*Derivative[1][wi[1][1]][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*
           Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]) + 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(2*r*Derivative[1][\[CurlyPhi]][r]^2*
           wi[1][1][r] + 2*r*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] - 
          Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
            2*wi[1][1][r])))))/(2*r^2*A[r])), 
 ((-I)*\[Omega]*H1[r]*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][\[CurlyPhi]][r]))/(r^2*A[r]) + 
  (Derivative[1][H0][r]*(-2*r + E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][\[CurlyPhi]][r]))/(2*r^2) + 
  (H2[r]*(-2 + (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]*
       Derivative[1][\[CurlyPhi]][r])/A[r]))/(2*r^2*B[r]) + 
  (K[r]*(2*r^2*\[Omega]^2 + 2*A[r] - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      (2*r*\[Omega]^2 + Derivative[1][A][r])*Derivative[1][\[CurlyPhi]][r]))/
   (2*r^2*A[r]*B[r]) + (Derivative[1][K][r]*
    (2 + (Derivative[1][A][r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]))/A[r]))/(2*r) + 
  \[CapitalLambda]*((E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
      Derivative[1][A][r])/(r^4*A[r]) + 
    (H0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
         r]))/(r^3*B[r]) + 
    (K[r]*(-2/B[r] + (E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
         Derivative[1][\[CurlyPhi]][r])/A[r]))/(2*r^2)) + 
  (\[CurlyPhi]1[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*\[Omega]^2 - 
     3*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]*
      (-1 + r*Derivative[1][\[CurlyPhi]][r]) + 
     B[r]*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
        (-1 + r*Derivative[1][\[CurlyPhi]][r]) + 
       r*(-2*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2 + 
         r*A[r]*Derivative[1][\[CurlyPhi]][r]))))/(2*r^4*A[r]*B[r]) - 
  (((E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*Derivative[1][A][r])/A[r] + 
     r^2*Derivative[1][\[CurlyPhi]][r])*Derivative[1][\[CurlyPhi]1][r])/
   (2*r^3), 
 a*((H1[r]*(r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
      2*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
         Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]))/(2*r^2*A[r]) - 
   (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
     (-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/
    (r^3*A[r]*B[r]) - ((I/2)*\[Omega]*K[r]*(-2*r*wi[1][1][r] + 
      E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
       (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(r*A[r]*B[r])), 
 (\[CapitalLambda]*H1[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/r^3 - 
  ((I/2)*\[Omega]*K[r]*(-2*A[r] + r*Derivative[1][A][r])*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/
   (r^2*A[r]) + (I*\[Omega]*Derivative[1][K][r]*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/r - 
  ((I/2)*\[Omega]*H2[r]*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][\[CurlyPhi]][r]))/r^2 + 
  ((I/2)*\[Omega]*\[CurlyPhi]1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + B[r])*
      Derivative[1][A][r] + A[r]*(-2*E^\[CurlyPhi][r]*\[Alpha] + 
       r*(r^2 + 2*E^\[CurlyPhi][r]*\[Alpha])*Derivative[1][\[CurlyPhi]][r] - 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(-1 + r*Derivative[1][\[CurlyPhi]][
           r]))))/(r^4*A[r]) - (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*
    (-1 + B[r])*Derivative[1][\[CurlyPhi]1][r])/r^3 + 
  (H1[r]*(-4 + 2*Derivative[1][B][r]*(2*r + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]) - 4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]) + 
     B[r]*(4 - 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r] + (r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
        Derivative[1][\[CurlyPhi]][r]^2 + 4*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[2][\[CurlyPhi]][r])))/(4*r^2), 
 a*(((-I/2)*\[Omega]*H1[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])/(r*A[r]) - 
   (Derivative[1][H0][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])/(2*r) + 
   (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][K][r]*
     Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r*A[r]) + 
   (H0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
     (-(r*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r^2*A[r]) + 
   (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
     (r*Derivative[1][A][r]*(1 + B[r] - r*B[r]*Derivative[1][\[CurlyPhi]][r])*
       wi[1][1][r] + 
      A[r]*(r*(-1 + B[r]*(-1 + r*Derivative[1][\[CurlyPhi]][r]))*
         Derivative[1][wi[1][1]][r] + 2*(-1 + B[r])*
         (-1 + r*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])))/(2*r^4*A[r]) + 
   (H2[r]*(-(r*Derivative[1][A][r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) + 
      A[r]*(r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
        2*(r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
           Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])))/(4*r^2*A[r]) + 
   (E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]1][r]*
     (-(r*B[r]*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 
          2*wi[1][1][r]))))/(2*r^3*A[r])), 
 ((-I)*r*\[Omega]*B[r]*Derivative[1][H1][r]*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/
   A[r] - (r*B[r]*Derivative[1][H2][r]*
    (2*A[r] + Derivative[1][A][r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r])))/(4*A[r]) + 
  (B[r]*Derivative[1][\[CurlyPhi]1][r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
      Derivative[1][A][r]^2 + 2*r^2*A[r]^2*Derivative[1][\[CurlyPhi]][r] - 
     E^\[CurlyPhi][r]*\[Alpha]*A[r]*(3*r*Derivative[1][A][r]*
        Derivative[1][B][r] + 2*B[r]*(2*Derivative[1][A][r]*
          (-1 + r*Derivative[1][\[CurlyPhi]][r]) + r*Derivative[2][A][r]))))/
   (4*r*A[r]^2) - (r*B[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r])*Derivative[2][H0][r])/2 + 
  (r^2*B[r]*(2*A[r] - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
      Derivative[1][\[CurlyPhi]][r])*Derivative[2][K][r])/(4*A[r]) - 
  ((I/2)*r*\[Omega]*H1[r]*(r*Derivative[1][B][r] + 
     B[r]*(2 - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r]) - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
   A[r] + \[CapitalLambda]*
   ((H2[r]*(-2 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
         Derivative[1][\[CurlyPhi]][r])/A[r]))/4 + 
    (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
      (A[r]*Derivative[1][A][r]*Derivative[1][B][r] - 
       B[r]*(Derivative[1][A][r]^2 - 2*A[r]*Derivative[2][A][r])))/
     (4*r*A[r]^2) + (H0[r]*(2 - E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
     4) - (r*Derivative[1][H0][r]*(2*B[r]*Derivative[1][A][r]*
      (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]) + 
     A[r]*(r*Derivative[1][B][r] + B[r]*(2 - 3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) - 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))))/(4*A[r]) + 
  (r*H2[r]*(B[r]*Derivative[1][A][r]^2*(r - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]) - A[r]^2*(2*Derivative[1][B][r] + 
       r*B[r]*Derivative[1][\[CurlyPhi]][r]^2) + 
     A[r]*(r*(2*\[Omega]^2 - Derivative[1][A][r]*Derivative[1][B][r]) + 
       B[r]*(Derivative[1][A][r]*(-2 + 6*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) - 
         2*(E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*Derivative[1][\[CurlyPhi]][
             r] + r*Derivative[2][A][r])) + 4*E^\[CurlyPhi][r]*\[Alpha]*
        B[r]^2*(Derivative[1][\[CurlyPhi]][r]*Derivative[2][A][r] + 
         Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
           Derivative[2][\[CurlyPhi]][r])))))/(4*A[r]^2) + 
  (r*Derivative[1][K][r]*(2*A[r]^2*(4*B[r] + r*Derivative[1][B][r]) + 
     E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]^2*
      Derivative[1][\[CurlyPhi]][r] - A[r]*B[r]*
      (2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[2][A][r] + Derivative[1][A][r]*
        (r*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          (2*Derivative[1][\[CurlyPhi]][r] + r*Derivative[1][\[CurlyPhi]][r]^
             2 + r*Derivative[2][\[CurlyPhi]][r])))))/(8*A[r]^2) + 
  (\[CurlyPhi]1[r]*(-2*r^2*A[r]^2*B[r]*Derivative[1][\[CurlyPhi]][r] + 
     E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]^2*
      (-1 + r*Derivative[1][\[CurlyPhi]][r]) - E^\[CurlyPhi][r]*\[Alpha]*A[r]*
      (2*r^2*\[Omega]^2*Derivative[1][B][r] + 3*r*B[r]*Derivative[1][A][r]*
        Derivative[1][B][r]*(-1 + r*Derivative[1][\[CurlyPhi]][r]) + 
       2*B[r]^2*(r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
          Derivative[2][A][r] + Derivative[1][A][r]*
          (2 - 2*r*Derivative[1][\[CurlyPhi]][r] + 
           r^2*Derivative[1][\[CurlyPhi]][r]^2 + 
           r^2*Derivative[2][\[CurlyPhi]][r])))))/(4*r^2*A[r]^2) + 
  (r*K[r]*(B[r]*Derivative[1][A][r]^2*(-r + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]) + A[r]^2*(2*Derivative[1][B][r] + 
       r*B[r]*Derivative[1][\[CurlyPhi]][r]^2) - 
     A[r]*(r*(-(Derivative[1][A][r]*Derivative[1][B][r]) + 
         \[Omega]^2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])) + 2*E^\[CurlyPhi][r]*\[Alpha]*
        B[r]^2*(Derivative[1][\[CurlyPhi]][r]*Derivative[2][A][r] + 
         Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
           Derivative[2][\[CurlyPhi]][r])) + 
       B[r]*(Derivative[1][A][r]*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]) + 
         2*r*(E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*Derivative[1][\[CurlyPhi]][
              r]^2 - Derivative[2][A][r] + E^\[CurlyPhi][r]*\[Alpha]*
            \[Omega]^2*Derivative[2][\[CurlyPhi]][r])))))/(4*A[r]^2) - 
  (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]*
    Derivative[2][\[CurlyPhi]1][r])/(2*A[r]), 
 a*(((-I/4)*r^2*\[Omega]*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
       Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
      2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
        Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r] - 
   ((I/4)*r*\[Omega]*H2[r]*(-4*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
        4*wi[1][1][r])))/A[r] - (r*B[r]*Derivative[1][H1][r]*
     (-4*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
        4*wi[1][1][r])))/(4*A[r]) - ((I/4)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*
     \[CurlyPhi]1[r]*(-(r*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][
         r]) + A[r]*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
          r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
         (r*Derivative[1][wi[1][1]][r] + 4*wi[1][1][r]))))/A[r]^2 + 
   (r*H1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]*
       Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r] - 
      A[r]*(-4*r*Derivative[1][B][r]*wi[1][1][r] + 
        B[r]*(r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
          4*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]*
           (4*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]) + 
          Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][wi[1][1]][r] + 
            4*wi[1][1][r]) + Derivative[2][\[CurlyPhi]][r]*
           (r*Derivative[1][wi[1][1]][r] + 4*wi[1][1][r])))))/(8*A[r]^2)), 
 ((-2*I)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*(-1 + B[r])*B[r]*
    Derivative[1][H1][r])/(r^2*A[r]) - 
  (B[r]*Derivative[1][H2][r]*(E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][A][r] + r^2*A[r]*Derivative[1][\[CurlyPhi]][r]))/
   (2*r^2*A[r]) - (I*\[Omega]*H1[r]*(E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][B][r] + r^2*B[r]*Derivative[1][\[CurlyPhi]][r]))/
   (r^2*A[r]) + (Derivative[1][H0][r]*(-2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      Derivative[1][A][r] + E^\[CurlyPhi][r]*\[Alpha]*A[r]*
      Derivative[1][B][r] + B[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][A][r] - A[r]*(3*E^\[CurlyPhi][r]*\[Alpha]*
          Derivative[1][B][r] + r^2*Derivative[1][\[CurlyPhi]][r]))))/
   (2*r^2*A[r]) + ((B[r]*Derivative[1][A][r] + A[r]*Derivative[1][B][r])*
    Derivative[1][\[CurlyPhi]1][r])/(2*r*A[r]) + 
  (E^\[CurlyPhi][r]*\[Alpha]*K[r]*
    (A[r]*(4*r*\[Omega]^2 + 2*Derivative[1][A][r])*Derivative[1][B][r] - 
     2*B[r]*(Derivative[1][A][r]^2 - 2*A[r]*Derivative[2][A][r])))/
   (4*r^2*A[r]^2) + (B[r]*Derivative[1][K][r]*
    (-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]^2) + 
     2*r^2*A[r]^2*Derivative[1][\[CurlyPhi]][r] + E^\[CurlyPhi][r]*\[Alpha]*
      A[r]*(3*r*Derivative[1][A][r]*Derivative[1][B][r] + 
       2*B[r]*(2*Derivative[1][A][r] + r*Derivative[2][A][r]))))/
   (2*r^2*A[r]^2) - (\[CurlyPhi]1[r]*(E^\[CurlyPhi][r]*\[Alpha]*(-1 + B[r])*
      B[r]*Derivative[1][A][r]^2 + r*A[r]^2*Derivative[1][B][r] + 
     A[r]*(-2*r^2*\[Omega]^2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*
        Derivative[1][B][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        Derivative[2][A][r] + B[r]*(Derivative[1][A][r]*
          (r - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]) + 
         2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[2][A][r]))))/(2*r^3*A[r]^2) + 
  \[CapitalLambda]*((-2*\[CurlyPhi]1[r])/r^3 - 
    (E^\[CurlyPhi][r]*\[Alpha]*B[r]*H2[r]*Derivative[1][A][r])/(r^3*A[r]) + 
    (E^\[CurlyPhi][r]*\[Alpha]*H0[r]*Derivative[1][B][r])/r^3 + 
    (E^\[CurlyPhi][r]*\[Alpha]*K[r]*
      (-(A[r]*Derivative[1][A][r]*Derivative[1][B][r]) + 
       B[r]*(Derivative[1][A][r]^2 - 2*A[r]*Derivative[2][A][r])))/
     (2*r^2*A[r]^2)) - (E^\[CurlyPhi][r]*\[Alpha]*(-1 + B[r])*B[r]*
    Derivative[2][H0][r])/r^2 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
    Derivative[1][A][r]*Derivative[2][K][r])/(r*A[r]) + 
  (H2[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*(-1 + 2*B[r])*
      Derivative[1][A][r]^2 + 
     A[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*(2*\[Omega]^2 - Derivative[1][A][r]*
           Derivative[1][B][r])) - 4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
        Derivative[2][A][r] + 
       B[r]*(-(Derivative[1][A][r]*(6*E^\[CurlyPhi][r]*r*\[Alpha]*
             Derivative[1][B][r] + r^3*Derivative[1][\[CurlyPhi]][r])) + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*(\[Omega]^2 + Derivative[2][A][r]))) - 
     r^2*A[r]^2*(r*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*B[r]*(2*Derivative[1][\[CurlyPhi]][r] + 
         r*Derivative[2][\[CurlyPhi]][r]))))/(2*r^3*A[r]^2) + 
  (B[r]*Derivative[2][\[CurlyPhi]1][r])/r, 
 a*(((2*I)*\[Omega]*\[CurlyPhi]1[r]*wi[1][1][r])/(r*A[r]) + 
   (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*H2[r]*(-2*wi[1][1][r] + 
      B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(r^2*A[r]) + 
   (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][H1][r]*
     (-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/
    (r^2*A[r]) + ((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*K[r]*
     (-(r*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]) + 
      A[r]*(2*B[r]*(3*Derivative[1][wi[1][1]][r] + 
          r*Derivative[2][wi[1][1]][r]) + Derivative[1][B][r]*
         (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]))))/(r*A[r]^2) + 
   (H1[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]*
        Derivative[1][wi[1][1]][r]) + 
      A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
         (3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]) - 
        2*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*wi[1][1][r] + 
        B[r]*(E^\[CurlyPhi][r]*\[Alpha]*(-2 + 3*r*Derivative[1][B][r])*
           Derivative[1][wi[1][1]][r] + 2*(3*E^\[CurlyPhi][r]*\[Alpha]*
             Derivative[1][B][r] + r^2*Derivative[1][\[CurlyPhi]][r])*
           wi[1][1][r]))))/(2*r^2*A[r]^2))}
