{(\[ScriptL]*(1 + \[ScriptL])*H1[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
      Derivative[1][\[CurlyPhi]][r]))/(2*r^3) - 
  ((I/2)*\[Omega]*K[r]*(-2*A[r] + r*Derivative[1][A][r])*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/
   (r^2*A[r]) + (I*\[Omega]*Derivative[1][K][r]*
    (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]))/r - 
  ((I/2)*\[Omega]*H2[r]*(2*r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
      Derivative[1][\[CurlyPhi]][r]))/r^2 + 
  ((I/2)*\[Omega]*\[CurlyPhi]1[r]*(E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + B[r])*
      Derivative[1][A][r] + A[r]*(-2*E^\[CurlyPhi][r]*\[Alpha] + 
       r*(r^2 + 2*E^\[CurlyPhi][r]*\[Alpha])*Derivative[1][\[CurlyPhi]][r] - 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(-1 + r*Derivative[1][\[CurlyPhi]][
           r]))))/(r^4*A[r]) - (I*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*
    (-1 + B[r])*Derivative[1][\[CurlyPhi]1][r])/r^3 + 
  (H1[r]*(-4 + 2*Derivative[1][B][r]*(2*r + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][\[CurlyPhi]][r]) - 4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
      (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]) + 
     B[r]*(4 - 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        Derivative[1][\[CurlyPhi]][r] + (r^2 + 4*E^\[CurlyPhi][r]*\[Alpha])*
        Derivative[1][\[CurlyPhi]][r]^2 + 4*E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[2][\[CurlyPhi]][r])))/(4*r^2) + 
  a^2*(((I/4)*r*\[Omega]*H0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
      wi[1][1][r])/A[r] + (\[ScriptL]*(1 + \[ScriptL])*H1[r]*
      (2*r*(-2*ki[2][0][r] + ki[2][2][r]) - (E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] + A[r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
            Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
              r]*(2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][
                r] - 4*ki[2][0][r] + 2*ki[2][2][r] - 4*pi[2][0][r] + 
              2*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][
               r]))))/A[r]))/(4*r^3) - ((I/2)*E^\[CurlyPhi][r]*\[Alpha]*
      \[Omega]*Derivative[1][\[CurlyPhi]1][r]*
      (r^3*B[r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
       A[r]*(4*ki[2][0][r] + 4*ki[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
         B[r]*(4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][
             r] - 4*pi[2][0][r] + 2*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
           \[CurlyPhi]i[2][2][r]) + \[CurlyPhi]i[2][2][r])))/(r^3*A[r]) - 
    ((I/4)*\[Omega]*Derivative[1][K][r]*
      (r^3*wi[1][1][r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] - 
           2*wi[1][1][r]) + 2*r*wi[1][1][r]) + 
       A[r]*(8*r^2*ki[2][0][r] - 4*r^2*ki[2][2][r] + 2*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(2*r*Derivative[1][ki[2][0]][r] - r*Derivative[1][ki[2][2]][
               r] - 4*ki[2][0][r] + 2*ki[2][2][r] - 4*pi[2][0][r] + 
             2*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][
              r])) + 6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r])))/
     (r^2*A[r]) - ((I/4)*\[Omega]*H2[r]*
      (r^4*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
           r])*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
       A[r]*(4*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][0]][r] - 
         2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][2]][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][0]][
           r] - E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][
           r] + 3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]i[2][2]][r] - 4*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[1][\[CurlyPhi]][r]*ki[2][0][r] - 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          ki[2][2][r] - 8*r^2*pi[2][0][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 24*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
         4*r^2*pi[2][2][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 12*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][0][r] - 6*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] - 
         6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] - 
         E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r] + 3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r])))/(r^3*A[r]) - 
    ((I/4)*\[Omega]*\[CurlyPhi]1[r]*
      (A[r]*(-4*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + B[r])*
          Derivative[1][hi[2][0]][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
          (-1 + B[r])*Derivative[1][hi[2][2]][r] - 8*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][ki[2][0]][r] + 8*E^\[CurlyPhi][r]*r^2*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][0]][
           r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][ki[2][2]][
           r] - 4*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][ki[2][2]][r] - 
         2*r^3*Derivative[1][\[CurlyPhi]i[2][0]][r] - 4*E^\[CurlyPhi][r]*r*
          \[Alpha]*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][0]][
           r] + r^3*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][2]][
           r] - 8*E^\[CurlyPhi][r]*\[Alpha]*ki[2][0][r] + 
         8*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          ki[2][0][r] - 8*E^\[CurlyPhi][r]*\[Alpha]*ki[2][2][r] + 
         8*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          ki[2][2][r] + 8*E^\[CurlyPhi][r]*\[Alpha]*B[r]*pi[2][0][r] - 
         8*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          pi[2][0][r] - 4*E^\[CurlyPhi][r]*\[Alpha]*B[r]*pi[2][2][r] + 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          pi[2][2][r] + 4*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][0][r] - 
         4*E^\[CurlyPhi][r]*\[Alpha]*B[r]*\[CurlyPhi]i[2][0][r] - 
         4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][0][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] - 
         2*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] + 
         2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*\[CurlyPhi]i[2][2][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) - 
       E^\[CurlyPhi][r]*r*\[Alpha]*(B[r]*(r^2*Derivative[1][wi[1][1]][r]*
            (r*Derivative[1][wi[1][1]][r] - 2*(-1 + r*Derivative[1][
                  \[CurlyPhi]][r])*wi[1][1][r]) + Derivative[1][A][r]*
            (4*r*Derivative[1][ki[2][0]][r] - 2*r*Derivative[1][ki[2][2]][
               r] - 4*pi[2][0][r] + 2*pi[2][2][r] + 2*\[CurlyPhi]i[2][0][r] - 
             \[CurlyPhi]i[2][2][r])) + Derivative[1][A][r]*
          (4*ki[2][0][r] + 4*ki[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r]))))/(r^4*A[r]) + 
    ((I/8)*\[Omega]*K[r]*
      (-4*A[r]*(2*r^2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][r] - 
         r^2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
             r])*Derivative[1][hi[2][2]][r] + 2*r^3*Derivative[1][ki[2][0]][
           r] - r^3*Derivative[1][ki[2][2]][r] + 2*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]i[2][2]][
           r] + 4*r^2*ki[2][0][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*ki[2][0][r] - 2*r^2*ki[2][2][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          ki[2][2][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 2*E^\[CurlyPhi][r]*r*
          \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
         2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          \[CurlyPhi]i[2][0][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*
          \[CurlyPhi]i[2][2][r] - E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) + 
       r*(4*r^4*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
         E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*(r^2*Derivative[1][\[CurlyPhi]][r]*
            Derivative[1][wi[1][1]][r]*(r*Derivative[1][wi[1][1]][r] - 
             4*wi[1][1][r]) + Derivative[1][A][r]*
            (4*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
             2*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             Derivative[1][\[CurlyPhi]][r]*(4*r*Derivative[1][ki[2][0]][r] - 
               2*r*Derivative[1][ki[2][2]][r] - 8*ki[2][0][r] + 4*
                ki[2][2][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 4*
                \[CurlyPhi]i[2][0][r] - 2*\[CurlyPhi]i[2][2][r]))) + 
         Derivative[1][A][r]*(8*r^2*ki[2][0][r] - 4*r^2*ki[2][2][r] + 
           6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]))))/(r^3*A[r]) + 
    (H1[r]*(-((r*B[r]*Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
          wi[1][1][r])/A[r]^2) + (r^2*Derivative[1][B][r]*
          Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
         r*B[r]*(r*Derivative[1][wi[1][1]][r]^2 - 
           (-8 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + 2*r*Derivative[2][wi[1][1]][r]*wi[1][1][r]) - 
         E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(2*r*Derivative[1][\[CurlyPhi]][r]^
             2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
           2*r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r]*
            wi[1][1][r] + Derivative[1][\[CurlyPhi]][r]*
            (r*Derivative[1][wi[1][1]][r]^2 + 6*Derivative[1][wi[1][1]][r]*
              wi[1][1][r] + 2*r*Derivative[2][wi[1][1]][r]*wi[1][1][r])))/
        A[r] - (-4*r*(3*hi[2][2][r] + 2*ki[2][0][r] + 2*ki[2][2][r] + 
           3*pi[2][2][r]) + Derivative[1][B][r]*
          (-4*r^3*Derivative[1][ki[2][0]][r] + 2*r^3*Derivative[1][ki[2][2]][
             r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]i[2][0]][r] + E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
            ki[2][0][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] + 8*r^2*pi[2][0][r] + 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
            pi[2][0][r] - 4*r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 2*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
           6*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] + 
           E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][2][r]) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
          (4*r*Derivative[1][ki[2][0]][r]*Derivative[2][\[CurlyPhi]][r] - 
           2*r*Derivative[1][ki[2][2]][r]*Derivative[2][\[CurlyPhi]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(8*Derivative[1][ki[2][0]][r] - 
             4*Derivative[1][ki[2][2]][r] - 6*Derivative[1][pi[2][0]][r] + 
             3*Derivative[1][pi[2][2]][r] + 4*Derivative[1][\[CurlyPhi]i[2][
                 0]][r] - 2*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             4*r*Derivative[2][ki[2][0]][r] - 2*r*Derivative[2][ki[2][2]][
               r]) + 2*Derivative[2][\[CurlyPhi]i[2][0]][r] - 
           Derivative[2][\[CurlyPhi]i[2][2]][r] - 
           8*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] + 
           4*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
           2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
           Derivative[1][\[CurlyPhi]][r]^2*(4*r*Derivative[1][ki[2][0]][r] - 
             2*r*Derivative[1][ki[2][2]][r] - 8*pi[2][0][r] + 4*pi[2][2][r] + 
             2*\[CurlyPhi]i[2][0][r] - \[CurlyPhi]i[2][2][r]) - 
           Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) + 
         B[r]*(12*r^2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][0]][r] - 
           6*r^2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][2]][r] + 
           8*r^2*Derivative[1][pi[2][0]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][pi[2][0]][r] - 
           4*r^2*Derivative[1][pi[2][2]][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][pi[2][2]][r] + 
           6*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           2*r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[
                2][0]][r] - 8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][0]][
             r] - 3*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][
               2]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][2]][
             r] - 8*r^3*Derivative[2][ki[2][0]][r] + 
           4*r^3*Derivative[2][ki[2][2]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[2][\[CurlyPhi]i[2][0]][r] + 2*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[2][\[CurlyPhi]i[2][2]][r] + 
           12*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
            hi[2][2][r] + 8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]^2*ki[2][0][r] + 8*E^\[CurlyPhi][r]*
            r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*ki[2][0][r] + 
           8*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*
            ki[2][2][r] + 8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] + 8*r*pi[2][0][r] - 
           24*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]*pi[2][0][r] + 
           2*r^3*Derivative[1][\[CurlyPhi]][r]^2*pi[2][0][r] + 
           8*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*
            pi[2][0][r] + 8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 4*r*pi[2][2][r] + 
           12*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
            pi[2][2][r] + 12*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 
           r^3*Derivative[1][\[CurlyPhi]][r]^2*pi[2][2][r] - 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*
            pi[2][2][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 6*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][0][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]^2*\[CurlyPhi]i[2][0][r] - 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][0][r] - 3*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][2][r] + 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]^2*\[CurlyPhi]i[2][2][r] + 
           2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][2][r]))/r^3))/4), 
 (a*\[ScriptL]*(1 + \[ScriptL])*h1[r]*
   (r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
   wi[1][1][r])/r^3, 
 a^2*(((-1/4*I)*r*\[Omega]*H0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*wi[1][1][r])/
    A[r] + ((I/4)*\[Omega]*H2[r]*
     (r^4*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
          r])*Derivative[1][wi[1][1]][r]*wi[1][1][r] - 
      3*A[r]*(2*r^2*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][2]][r] - 
        E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + 3*B[r])*
         Derivative[1][\[CurlyPhi]i[2][2]][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
         Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] - 4*r^2*pi[2][2][r] - 
        2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
         pi[2][2][r] + 12*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 6*E^\[CurlyPhi][r]*
         \[Alpha]*\[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*
         Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] - 
        3*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
         \[CurlyPhi]i[2][2][r])))/(r^3*A[r]) + 
   ((I/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*Derivative[1][\[CurlyPhi]1][r]*
     (r^3*B[r]*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
      3*A[r]*(4*ki[2][2][r] + \[CurlyPhi]i[2][2][r] - 
        B[r]*(2*r*Derivative[1][ki[2][2]][r] - 2*pi[2][2][r] + 
          \[CurlyPhi]i[2][2][r]))))/(r^3*A[r]) + 
   (H1[r]*((r*B[r]*Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
        wi[1][1][r])/A[r]^2 - (r^2*Derivative[1][B][r]*
         Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
        r*B[r]*(r*Derivative[1][wi[1][1]][r]^2 - 
          (-8 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] + 2*r*Derivative[2][wi[1][1]][r]*wi[1][1][r]) - 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(2*r*Derivative[1][\[CurlyPhi]][r]^2*
           Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
          2*r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r]*
           wi[1][1][r] + Derivative[1][\[CurlyPhi]][r]*
           (r*Derivative[1][wi[1][1]][r]^2 + 6*Derivative[1][wi[1][1]][r]*
             wi[1][1][r] + 2*r*Derivative[2][wi[1][1]][r]*wi[1][1][r])))/
       A[r] - (3*(4*r*(3*hi[2][2][r] + 2*ki[2][2][r] + 3*pi[2][2][r]) - 
         Derivative[1][B][r]*(2*r^3*Derivative[1][ki[2][2]][r] + 
           E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
            ki[2][2][r] - 4*r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 6*E^\[CurlyPhi][r]*
            \[Alpha]*\[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) + 
         B[r]*(6*r^2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r])*Derivative[1][ki[2][2]][r] + 
           2*r*(2*r + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][
               r])*Derivative[1][pi[2][2]][r] + 3*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
           r^3*Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][
               2]][r] - 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*Derivative[1][\[CurlyPhi]i[2][2]][
             r] - 4*r^3*Derivative[2][ki[2][2]][r] - 2*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[2][\[CurlyPhi]i[2][2]][r] - 
           12*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r]*
            hi[2][2][r] - 8*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]^2*ki[2][2][r] - 8*E^\[CurlyPhi][r]*
            r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] + 
           4*r*pi[2][2][r] - 12*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 12*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
            pi[2][2][r] + r^3*Derivative[1][\[CurlyPhi]][r]^2*pi[2][2][r] + 
           4*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[1][\[CurlyPhi]][r]^2*
            pi[2][2][r] + 4*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 3*E^\[CurlyPhi][r]*r*
            \[Alpha]*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
            Derivative[1][\[CurlyPhi]][r]^2*\[CurlyPhi]i[2][2][r] - 
           2*E^\[CurlyPhi][r]*r*\[Alpha]*Derivative[2][\[CurlyPhi]][r]*
            \[CurlyPhi]i[2][2][r]) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
          (2*r*Derivative[1][ki[2][2]][r]*Derivative[2][\[CurlyPhi]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(4*Derivative[1][ki[2][2]][r] - 
             3*Derivative[1][pi[2][2]][r] + 2*Derivative[1][\[CurlyPhi]i[2][
                 2]][r] + 2*r*Derivative[2][ki[2][2]][r]) + 
           Derivative[2][\[CurlyPhi]i[2][2]][r] - 
           4*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
           Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
           Derivative[1][\[CurlyPhi]][r]^2*(2*r*Derivative[1][ki[2][2]][r] - 
             4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/r^3))/4 + 
   ((I/4)*\[Omega]*\[CurlyPhi]1[r]*
     (-3*A[r]*(-2*E^\[CurlyPhi][r]*r*\[Alpha]*(-1 + B[r])*
         Derivative[1][hi[2][2]][r] - r^3*Derivative[1][\[CurlyPhi]i[2][2]][
          r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
         Derivative[1][\[CurlyPhi]i[2][2]][r] + 8*E^\[CurlyPhi][r]*\[Alpha]*
         ki[2][2][r] - 8*E^\[CurlyPhi][r]*r*\[Alpha]*
         Derivative[1][\[CurlyPhi]][r]*ki[2][2][r] + 2*E^\[CurlyPhi][r]*
         \[Alpha]*B[r]*(2*r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*
           Derivative[1][ki[2][2]][r] + r*Derivative[1][\[CurlyPhi]i[2][2]][
            r] - (-1 + r*Derivative[1][\[CurlyPhi]][r])*(2*pi[2][2][r] - 
            \[CurlyPhi]i[2][2][r])) + 2*E^\[CurlyPhi][r]*\[Alpha]*
         \[CurlyPhi]i[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*
         Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) - 
      E^\[CurlyPhi][r]*r*\[Alpha]*(3*Derivative[1][A][r]*
         (4*ki[2][2][r] + \[CurlyPhi]i[2][2][r]) + 
        B[r]*(r^2*Derivative[1][wi[1][1]][r]*(r*Derivative[1][wi[1][1]][r] - 
            2*(-1 + r*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) - 
          3*Derivative[1][A][r]*(2*r*Derivative[1][ki[2][2]][r] - 
            2*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/(r^4*A[r]) + 
   ((I/4)*\[Omega]*Derivative[1][K][r]*
     (r^3*wi[1][1][r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] - 
          2*wi[1][1][r]) + 2*r*wi[1][1][r]) - 
      6*A[r]*(2*r^2*ki[2][2][r] - 3*E^\[CurlyPhi][r]*\[Alpha]*
         \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
            r]*(r*Derivative[1][ki[2][2]][r] - 2*ki[2][2][r] - 
            2*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/(r^2*A[r]) + 
   (\[ScriptL]*(1 + \[ScriptL])*H1[r]*
     ((E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[1][wi[1][1]][r]*wi[1][1][r])/A[r] - 
      (3*(2*r^2*ki[2][2][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*
          \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(r*Derivative[1][ki[2][2]][r] - 2*ki[2][2][r] - 
             2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/r^4))/4 - 
   ((I/8)*\[Omega]*K[r]*
     (12*A[r]*(r^2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
        r^3*Derivative[1][ki[2][2]][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]i[2][2]][r] + 2*r^2*ki[2][2][r] - 
        2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
         ki[2][2][r] - 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 3*E^\[CurlyPhi][r]*
         \[Alpha]*\[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r]) + 
      r*(4*r^4*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
        2*Derivative[1][A][r]*(-6*r^2*ki[2][2][r] + 9*E^\[CurlyPhi][r]*
           \[Alpha]*\[CurlyPhi]i[2][2][r]) + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         (r^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
           (r*Derivative[1][wi[1][1]][r] - 4*wi[1][1][r]) - 
          6*Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
            Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][ki[2][2]][r] - 
              2*ki[2][2][r] - 2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))))/
    (r^3*A[r])), 
 a*(((-1/2*I)*\[Omega]*H1[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])/(r*A[r]) - 
   (Derivative[1][H0][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])/(2*r) + 
   (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][K][r]*
     Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r*A[r]) + 
   (H0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r])*
     (-(r*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*r^2*A[r]) + 
   (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
     (r*Derivative[1][A][r]*(1 + B[r] - r*B[r]*Derivative[1][\[CurlyPhi]][r])*
       wi[1][1][r] + 
      A[r]*(r*(-1 + B[r]*(-1 + r*Derivative[1][\[CurlyPhi]][r]))*
         Derivative[1][wi[1][1]][r] + 2*(-1 + B[r])*
         (-1 + r*Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])))/(2*r^4*A[r]) + 
   (H2[r]*(-(r*Derivative[1][A][r]*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]) + 
      A[r]*(r*(r - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
        2*(r - E^\[CurlyPhi][r]*\[Alpha]*(-1 + 3*B[r])*
           Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])))/(4*r^2*A[r]) + 
   (E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]1][r]*
     (-(r*B[r]*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(-2*wi[1][1][r] + B[r]*(r*Derivative[1][wi[1][1]][r] + 
          2*wi[1][1][r]))))/(2*r^3*A[r])), 
 a^2*((3*E^\[CurlyPhi][r]*\[ScriptL]*(1 + \[ScriptL])*\[Alpha]*h0[r]*
     (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
      r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][
         r])))/(2*r^5) + (Derivative[1][h0][r]*
     ((E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[1][wi[1][1]][r]*wi[1][1][r])/A[r] - 
      (3*(-(r^2*hi[2][2][r]) + r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*
          \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(r*Derivative[1][ki[2][2]][r] + hi[2][2][r] - 3*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r]))))/r^4))/2 + 
   (I/2)*\[Omega]*h1[r]*((2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*wi[1][1][r])/
      A[r] - (3*(-(r^2*hi[2][2][r]) + r^2*pi[2][2][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r] + 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][
            r] - 2*\[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
           (r*Derivative[1][ki[2][2]][r] + hi[2][2][r] - 3*pi[2][2][r] + 
            \[CurlyPhi]i[2][2][r]))))/r^4) + 
   (h0[r]*(-6*r^2*A[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] - 
      r*(2*r^4*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
        3*Derivative[1][A][r]*(r^2*hi[2][2][r] - r^2*pi[2][2][r] + 
          2*E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]i[2][2][r]) + 
        E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*(r^3*Derivative[1][\[CurlyPhi]][r]*
           Derivative[1][wi[1][1]][r]^2 - 3*Derivative[1][A][r]*
           (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
              r]*(r*Derivative[1][ki[2][2]][r] + hi[2][2][r] - 
              3*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))))/(2*r^5*A[r])), 
 (a^2*H1[r]*(E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
     wi[1][1][r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) - 
    2*(r^4*wi[1][1][r]^2 + 3*E^\[CurlyPhi][r]*\[Alpha]*A[r]*
       \[CurlyPhi]i[2][2][r])))/(4*r^4*A[r]), 
 -1/4*(a*E^\[CurlyPhi][r]*\[ScriptL]*(1 + \[ScriptL])*\[Alpha]*B[r]*h1[r]*
     Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][A][r]*wi[1][1][r]) + 
      A[r]*(r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(r^3*A[r]) - 
  a*(((-I)*\[Omega]*h0[r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])/(r^2*A[r]) + 
    ((I/2)*\[Omega]*Derivative[1][h0][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r])*wi[1][1][r])/(r*A[r]) - 
    (h1[r]*(r*B[r]*Derivative[1][A][r]^2*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
       A[r]^2*(Derivative[1][B][r]*(r^2*Derivative[1][wi[1][1]][r] + 
           2*(r + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]][r])*
            wi[1][1][r]) + B[r]*((8*r - E^\[CurlyPhi][r]*\[Alpha]*
              (-2 + 3*r*Derivative[1][B][r])*Derivative[1][\[CurlyPhi]][r])*
            Derivative[1][wi[1][1]][r] + 2*r^2*Derivative[2][wi[1][1]][r] - 
           2*(3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
              Derivative[1][\[CurlyPhi]][r] - 2*(1 + E^\[CurlyPhi][r]*
                \[Alpha]*Derivative[1][\[CurlyPhi]][r]^2 + E^\[CurlyPhi][r]*
                \[Alpha]*Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r]) - 
         2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]*
            (3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][r]) + 
           Derivative[1][\[CurlyPhi]][r]^2*(r*Derivative[1][wi[1][1]][r] + 
             2*wi[1][1][r]) + Derivative[2][\[CurlyPhi]][r]*
            (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]))) - 
       A[r]*(r^2*(-2*\[Omega]^2 + Derivative[1][A][r]*Derivative[1][B][r])*
          wi[1][1][r] + B[r]*(2*r*(E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*
              Derivative[1][\[CurlyPhi]][r] + r*Derivative[2][A][r])*
            wi[1][1][r] + Derivative[1][A][r]*(r^2*Derivative[1][wi[1][1]][
               r] - (-2*r + E^\[CurlyPhi][r]*\[Alpha]*(-2 + 
                 3*r*Derivative[1][B][r])*Derivative[1][\[CurlyPhi]][r])*
              wi[1][1][r])) - E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*
          (2*Derivative[1][\[CurlyPhi]][r]*Derivative[2][A][r]*wi[1][1][r] + 
           Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][r]*
              Derivative[1][wi[1][1]][r] + 2*Derivative[1][\[CurlyPhi]][r]^2*
              wi[1][1][r] + 2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r])))))/
     (4*r^2*A[r]^2)), 
 -(a^2*((((-3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
      (A[r]*(-2*r*Derivative[1][ki[2][2]][r] + 2*pi[2][2][r]) + 
       r^3*Derivative[1][wi[1][1]][r]*wi[1][1][r]))/(r^4*A[r]) + 
    (((3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*K[r]*
      (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
       r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][
          r])))/r^3 + 
    H1[r]*(-1/2*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
         Derivative[1][wi[1][1]][r]*wi[1][1][r])/A[r] + 
      (3*(r^2*hi[2][2][r] + r^2*pi[2][2][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*
          \[CurlyPhi]i[2][2][r] + E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(r*Derivative[1][ki[2][2]][r] - hi[2][2][r] - 3*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r]))))/(2*r^4)))), 
 -(a^2*((Derivative[1][h0][r]*(2*r^4*wi[1][1][r]^2 - E^\[CurlyPhi][r]*r^3*
        \[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) + 
       6*E^\[CurlyPhi][r]*\[Alpha]*A[r]*\[CurlyPhi]i[2][2][r]))/
     (4*r^4*A[r]) + ((I/4)*\[Omega]*h1[r]*(E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]*
        Derivative[1][\[CurlyPhi]][r]*wi[1][1][r]*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r]) - 
       2*(r^4*wi[1][1][r]^2 + 3*E^\[CurlyPhi][r]*\[Alpha]*A[r]*
          \[CurlyPhi]i[2][2][r])))/(r^4*A[r]) + 
    (h0[r]*(-2*r^3*Derivative[1][A][r]*(r - E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]^2 + 
       E^\[CurlyPhi][r]*\[Alpha]*A[r]*(r^3*B[r]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][wi[1][1]][r]*(r*Derivative[1][wi[1][1]][r] + 
           2*wi[1][1][r]) - 6*Derivative[1][A][r]*\[CurlyPhi]i[2][2][r])))/
     (4*r^4*A[r]^2)))}
