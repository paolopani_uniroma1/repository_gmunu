{-(a*(((I/4)*E^\[CurlyPhi][r]*r^2*\[Alpha]*\[Omega]*B[r]*H2[r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r])/A[r] + 
    (E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*Derivative[1][H1][r]*
      Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r])/(4*A[r]) + 
    ((I/4)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*\[CurlyPhi]1[r]*
      (-(r*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]) + 
       A[r]*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r] + 
         2*B[r]*(3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][
             r]))))/A[r]^2 + (E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*H1[r]*
      (-(r*B[r]*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
         Derivative[1][wi[1][1]][r]) + 
       A[r]*(3*r*Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][wi[1][1]][r] + 2*B[r]*
          (r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r] + 
           r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][wi[1][1]][r] + 
             r*Derivative[2][wi[1][1]][r])))))/(8*A[r]^2) - 
    ((I/4)*r^2*\[Omega]*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r] + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]^2 + 
         Derivative[2][\[CurlyPhi]][r]))*wi[1][1][r])/A[r])), 
 -(a^2*((r*B[r]*h1[r]*Derivative[1][wi[1][1]][r]*(-2*r*wi[1][1][r] + 
       E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
        (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*A[r]) - 
    (((3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][h0][r]*
      (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
       r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][
          r])))/(r*A[r]) + (B[r]*Derivative[1][h1][r]*
      (2*A[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          (2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][r]) + 
         6*ki[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r]) + 
       E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][r]*
          (6*ki[2][2][r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 
           2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r]))))/(4*A[r]) + 
    ((I/4)*\[Omega]*h0[r]*(-8*hi[2][0][r] + 4*hi[2][2][r] - 
       E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
        (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
         Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][r]*
          (4*hi[2][0][r] - 2*hi[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r] - 
           2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) + 
       2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(Derivative[1][\[CurlyPhi]][r]*
          (2*Derivative[1][pi[2][0]][r] - Derivative[1][pi[2][2]][r] - 
           4*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
         2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
         Derivative[2][\[CurlyPhi]i[2][2]][r] + 4*Derivative[2][\[CurlyPhi]][
           r]*hi[2][0][r] - 2*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r] + 
         4*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
         2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
         2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         Derivative[1][\[CurlyPhi]][r]^2*(4*hi[2][0][r] - 2*hi[2][2][r] + 
           4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
           \[CurlyPhi]i[2][2][r]))))/A[r] - 
    (B[r]*Derivative[1][h1][r]*
      (A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          (2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][r]) + 
         8*pi[2][0][r] - 4*pi[2][2][r]) + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (-2*r^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
          wi[1][1][r] + Derivative[1][A][r]*
          (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
             r]*(8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
             \[CurlyPhi]i[2][2][r])))))/(4*A[r]) - 
    ((I/4)*\[Omega]*h0[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (-6*r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
         2*r^2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
         r^2*Derivative[2][\[CurlyPhi]i[2][2]][r] + 
         4*r^2*Derivative[2][\[CurlyPhi]][r]*hi[2][0][r] - 
         2*r^2*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r] + 
         6*r^2*Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] + 
         4*r^2*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
         2*r^2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
         2*r^2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
         r*Derivative[1][\[CurlyPhi]][r]*(2*r*Derivative[1][pi[2][0]][r] - 
           r*Derivative[1][pi[2][2]][r] - 
           4*r*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
           2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 6*pi[2][2][r] - 
           6*\[CurlyPhi]i[2][2][r]) + 6*\[CurlyPhi]i[2][2][r] + 
         r^2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
         r^2*Derivative[1][\[CurlyPhi]][r]^2*(4*hi[2][0][r] - 2*hi[2][2][r] + 
           6*ki[2][2][r] + 4*pi[2][0][r] - 2*pi[2][2][r] - 
           2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) - 
       r^2*(8*hi[2][0][r] - 4*hi[2][2][r] + 12*ki[2][2][r] + 
         E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
             r]*(4*hi[2][0][r] - 2*hi[2][2][r] + 6*ki[2][2][r] + 
             4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
             \[CurlyPhi]i[2][2][r])))))/(r^2*A[r]) - 
    (h1[r]*(2*A[r]^2*(B[r]*((-4 + 6*E^\[CurlyPhi][r]*\[Alpha]*
              Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*
            Derivative[1][hi[2][0]][r] + (2 - 3*E^\[CurlyPhi][r]*\[Alpha]*
              Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*
            Derivative[1][hi[2][2]][r] + 4*Derivative[1][pi[2][0]][r] - 
           2*Derivative[1][pi[2][2]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
          (Derivative[1][\[CurlyPhi]][r]^2*(2*Derivative[1][hi[2][0]][r] - 
             Derivative[1][hi[2][2]][r]) + (2*Derivative[1][hi[2][0]][r] - 
             Derivative[1][hi[2][2]][r])*Derivative[2][\[CurlyPhi]][r] + 
           Derivative[1][\[CurlyPhi]][r]*(2*Derivative[2][hi[2][0]][r] - 
             Derivative[2][hi[2][2]][r])) + 2*Derivative[1][B][r]*
          (2*pi[2][0][r] - pi[2][2][r])) - E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        Derivative[1][A][r]*(-2*r^2*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][wi[1][1]][r]*wi[1][1][r] + Derivative[1][A][r]*
          (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
           Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
             r]*(8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
             \[CurlyPhi]i[2][2][r]))) - A[r]*B[r]*
        (6*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
          wi[1][1][r] + Derivative[1][A][r]*(-8*pi[2][0][r] + 4*pi[2][2][r] - 
           3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
             Derivative[1][\[CurlyPhi]i[2][2]][r] - 
             Derivative[1][\[CurlyPhi]][r]*(8*pi[2][0][r] - 4*pi[2][2][r] - 2*
                \[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r]))) + 
         2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          (-2*Derivative[1][\[CurlyPhi]i[2][0]][r]*Derivative[2][A][r] + 
           Derivative[1][\[CurlyPhi]i[2][2]][r]*Derivative[2][A][r] + 
           2*r^2*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r]*
            wi[1][1][r] + 2*r^2*Derivative[1][wi[1][1]][r]*
            Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] + Derivative[1][A][r]*
            (Derivative[1][\[CurlyPhi]][r]*(-4*Derivative[1][hi[2][0]][r] + 2*
                Derivative[1][hi[2][2]][r] + 6*Derivative[1][pi[2][0]][r] - 3*
                Derivative[1][pi[2][2]][r] - 4*Derivative[1][\[CurlyPhi]i[2][
                   0]][r] + 2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
             2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
             Derivative[2][\[CurlyPhi]i[2][2]][r] + 
             8*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
             4*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
             2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
             Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
             Derivative[1][\[CurlyPhi]][r]^2*(8*pi[2][0][r] - 4*pi[2][2][r] - 
               2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) + 
           Derivative[1][\[CurlyPhi]][r]*(2*r^2*Derivative[1][wi[1][1]][r]^
               2 + 4*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
             2*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] + 
             Derivative[2][A][r]*(8*pi[2][0][r] - 4*pi[2][2][r] - 2*
                \[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r]))))))/
     (8*A[r]^2) + 
    (h1[r]*(2*B[r]*((-4 + 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][r] + 
         (2 - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
         12*Derivative[1][ki[2][2]][r] + 4*Derivative[1][pi[2][0]][r] - 
         2*Derivative[1][pi[2][2]][r]) + 4*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        (Derivative[1][\[CurlyPhi]][r]^2*(2*Derivative[1][hi[2][0]][r] - 
           Derivative[1][hi[2][2]][r]) + (2*Derivative[1][hi[2][0]][r] - 
           Derivative[1][hi[2][2]][r])*Derivative[2][\[CurlyPhi]][r] + 
         Derivative[1][\[CurlyPhi]][r]*(2*Derivative[2][hi[2][0]][r] - 
           Derivative[2][hi[2][2]][r])) + 4*Derivative[1][B][r]*
        (3*ki[2][2][r] + 2*pi[2][0][r] - pi[2][2][r]) - 
       (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]^2*
         (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
          Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
            r]*(6*ki[2][2][r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 
            2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])))/A[r]^2 - 
       (2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[2][A][r]*
           (-2*Derivative[1][\[CurlyPhi]i[2][0]][r] + 
            Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
              r]*(6*ki[2][2][r] + 8*pi[2][0][r] - 4*pi[2][2][r] - 
              2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) + 
          Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][r]*
             (-4*Derivative[1][hi[2][0]][r] + 2*Derivative[1][hi[2][2]][r] + 
              6*Derivative[1][ki[2][2]][r] + 6*Derivative[1][pi[2][0]][r] - 
              3*Derivative[1][pi[2][2]][r] - 4*Derivative[1][\[CurlyPhi]i[2][
                  0]][r] + 2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
            2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
            Derivative[2][\[CurlyPhi]i[2][2]][r] + 
            6*Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] + 
            8*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
            4*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
            2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
            Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
            Derivative[1][\[CurlyPhi]][r]^2*(6*ki[2][2][r] + 8*pi[2][0][r] - 
              4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][
               r]))))/A[r] + (B[r]*(12*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]^2*
           (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
            r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][
                2][r])) + r*Derivative[1][A][r]*(12*ki[2][2][r] + 
            8*pi[2][0][r] - 4*pi[2][2][r] + 3*E^\[CurlyPhi][r]*\[Alpha]*
             Derivative[1][B][r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
              Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][
                 \[CurlyPhi]][r]*(6*ki[2][2][r] + 8*pi[2][0][r] - 
                4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][
                 r])))))/(r*A[r])))/8)), 
 a*(((-1/4*I)*E^\[CurlyPhi][r]*r^2*\[Alpha]*\[Omega]*B[r]^2*
     Derivative[1][h1][r]*Derivative[1][\[CurlyPhi]][r]*
     Derivative[1][wi[1][1]][r])/A[r] - 
   (E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]^2*Derivative[1][\[CurlyPhi]][r]*
     Derivative[1][wi[1][1]][r]*Derivative[2][h0][r])/(4*A[r]) - 
   (r*B[r]*Derivative[1][h0][r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[1][wi[1][1]][r]) + 
      A[r]*(Derivative[1][wi[1][1]][r]*
         (r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
           B[r]*(2*Derivative[1][\[CurlyPhi]][r] + 
            r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
              r])) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*Derivative[2][wi[1][1]][r])))/
    (8*A[r]^2) - ((I/8)*r*\[Omega]*B[r]*h1[r]*
     (-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*Derivative[1][A][r]*
        Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]) + 
      A[r]*(Derivative[1][wi[1][1]][r]*
         (r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
           B[r]*(4*Derivative[1][\[CurlyPhi]][r] + 
            r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
              r])) + 2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*Derivative[2][wi[1][1]][r])))/A[r]^2 + 
   (h0[r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]^2*Derivative[1][A][r]*
        Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]) + 
      A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
         (r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r] + 
          r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r] + 
          Derivative[1][\[CurlyPhi]][r]*(3*Derivative[1][wi[1][1]][r] + 
            r*Derivative[2][wi[1][1]][r])) - \[ScriptL]*(1 + \[ScriptL])*
         (-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r])*wi[1][1][r] + 
        B[r]*(r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] - 
          2*E^\[CurlyPhi][r]*\[ScriptL]*(1 + \[ScriptL])*\[Alpha]*
           (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])*
           wi[1][1][r]))))/(4*A[r]^2)), 
 a^2*((-3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]1][r]*
     (2*A[r]*(r*Derivative[1][hi[2][2]][r] - hi[2][2][r]) + 
      r*Derivative[1][A][r]*(hi[2][2][r] - pi[2][2][r])))/(2*r^2*A[r]) + 
   (((3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*H1[r]*
     (r*Derivative[1][\[CurlyPhi]i[2][2]][r] - \[CurlyPhi]i[2][2][r] + 
      r*Derivative[1][\[CurlyPhi]][r]*(-pi[2][2][r] + \[CurlyPhi]i[2][2][
         r])))/(r*A[r]) + (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
     Derivative[1][H0][r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
      \[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
       (-pi[2][2][r] + \[CurlyPhi]i[2][2][r])))/(2*r) + 
   (3*H0[r]*(-4*hi[2][2][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]*(Derivative[1][pi[2][2]][r] - 
          2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
        Derivative[2][\[CurlyPhi]i[2][2]][r] + 
        Derivative[1][\[CurlyPhi]][r]^2*(2*hi[2][2][r] + 2*pi[2][2][r] - 
          \[CurlyPhi]i[2][2][r]) + Derivative[2][\[CurlyPhi]][r]*
         (2*hi[2][2][r] + 2*pi[2][2][r] - \[CurlyPhi]i[2][2][r])) - 
      E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]*
         (-2*hi[2][2][r] - 2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/8 + 
   (3*H2[r]*(2*A[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][hi[2][2]][r] - 
          2*hi[2][2][r]) + 2*r*ki[2][2][r]) + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][A][r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
        2*\[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
         (2*hi[2][2][r] - 2*ki[2][2][r] - 2*pi[2][2][r] + 
          \[CurlyPhi]i[2][2][r]))))/(8*r*A[r]) - 
   (\[CurlyPhi]1[r]*(3*E^\[CurlyPhi][r]*r*\[Alpha]*A[r]*
       (2*B[r]*(r^3*Derivative[1][wi[1][1]][r]^2 + Derivative[1][A][r]*
           (2*r*Derivative[1][hi[2][2]][r] - r*Derivative[1][pi[2][2]][r] + 
            2*(-2 + r*Derivative[1][\[CurlyPhi]][r])*(hi[2][2][r] - 
              pi[2][2][r])) + r*Derivative[2][A][r]*(2*ki[2][2][r] - 
            2*pi[2][2][r] - \[CurlyPhi]i[2][2][r])) + r*Derivative[1][A][r]*
         Derivative[1][B][r]*(2*ki[2][2][r] - 2*pi[2][2][r] - 
          \[CurlyPhi]i[2][2][r])) - 3*E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*
       Derivative[1][A][r]^2*(2*ki[2][2][r] - 2*pi[2][2][r] - 
        \[CurlyPhi]i[2][2][r]) + 6*A[r]^2*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (2*r*(-2 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][
            r] + r^2*Derivative[2][hi[2][2]][r] - 
          2*(-2 + r*Derivative[1][\[CurlyPhi]][r])*hi[2][2][r]) + 
        r^2*(E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][hi[2][2]][r] + 2*\[CurlyPhi]i[2][2][r]))))/
    (8*r^3*A[r]^2) - 
   (K[r]*(6*A[r]^2*((-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
         hi[2][2][r] - 2*pi[2][2][r]) - 3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       Derivative[1][A][r]^2*\[CurlyPhi]i[2][2][r] + 
      A[r]*(-2*r^2*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r])*wi[1][1][r]^2 + 
        3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][A][r]*Derivative[1][B][r]*
         \[CurlyPhi]i[2][2][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (3*Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] - 
          2*r^2*Derivative[1][\[CurlyPhi]][r]^2*wi[1][1][r]^2 - 
          2*r^2*Derivative[2][\[CurlyPhi]][r]*wi[1][1][r]^2 + 
          3*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r]))))/(8*A[r]^2) + 
   (H2[r]*(6*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
         Derivative[1][hi[2][2]][r] + 2*pi[2][2][r]) + 
      (E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*r^2*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][wi[1][1]][r]*wi[1][1][r] + 3*Derivative[1][A][r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/A[r]))/8 + 
   (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
     (6*A[r]^2*(Derivative[1][B][r]*Derivative[1][hi[2][2]][r] + 
        2*B[r]*Derivative[2][hi[2][2]][r]) + B[r]*Derivative[1][A][r]*
       (-2*r^2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
        Derivative[1][A][r]*(6*pi[2][2][r] - 3*\[CurlyPhi]i[2][2][r])) + 
      A[r]*(2*B[r]*(Derivative[1][A][r]*(6*Derivative[1][hi[2][2]][r] - 
            3*Derivative[1][pi[2][2]][r]) + 3*r^2*Derivative[1][wi[1][1]][r]^
            2 - 6*Derivative[2][A][r]*pi[2][2][r] + 
          6*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
          2*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] + 
          3*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r]) + 
        Derivative[1][B][r]*(2*r^2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
          Derivative[1][A][r]*(-6*pi[2][2][r] + 3*\[CurlyPhi]i[2][2][r])))))/
    (8*r*A[r]^2) + (3*H0[r]*(2*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
       Derivative[1][A][r]*(r*Derivative[1][\[CurlyPhi]i[2][2]][r] - 
        \[CurlyPhi]i[2][2][r] + r*Derivative[1][\[CurlyPhi]][r]*
         (-pi[2][2][r] + \[CurlyPhi]i[2][2][r])) + 
      A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (-2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 
          r^2*Derivative[2][\[CurlyPhi]i[2][2]][r] + 
          2*r^2*Derivative[2][\[CurlyPhi]][r]*ki[2][2][r] - 
          2*r^2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] + 
          r*Derivative[1][\[CurlyPhi]][r]*(-(r*Derivative[1][pi[2][2]][r]) + 
            2*r*Derivative[1][\[CurlyPhi]i[2][2]][r] + 2*pi[2][2][r] - 
            2*\[CurlyPhi]i[2][2][r]) + 2*\[CurlyPhi]i[2][2][r] + 
          r^2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
          r^2*Derivative[1][\[CurlyPhi]][r]^2*(2*ki[2][2][r] - 
            2*pi[2][2][r] + \[CurlyPhi]i[2][2][r])) + 
        r^2*(-4*ki[2][2][r] + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
              r]*(2*ki[2][2][r] - 2*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))))/
    (8*r^2*A[r])), 
 (-1/4*(H2[r]*(-2 + (E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
        Derivative[1][\[CurlyPhi]][r])/A[r])) - 
   (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
     (A[r]*Derivative[1][A][r]*Derivative[1][B][r] - 
      B[r]*(Derivative[1][A][r]^2 - 2*A[r]*Derivative[2][A][r])))/
    (4*r*A[r]^2) - (H0[r]*(2 - E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       Derivative[1][\[CurlyPhi]][r] - 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
    4 - a^2*((H0[r]*(-8*hi[2][0][r] + 4*hi[2][2][r] - E^\[CurlyPhi][r]*
         \[Alpha]*Derivative[1][B][r]*(2*Derivative[1][\[CurlyPhi]i[2][0]][
            r] - Derivative[1][\[CurlyPhi]i[2][2]][r] - 
          Derivative[1][\[CurlyPhi]][r]*(4*hi[2][0][r] - 2*hi[2][2][r] + 
            4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
            \[CurlyPhi]i[2][2][r])) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][pi[2][0]][r] - 
            Derivative[1][pi[2][2]][r] - 4*Derivative[1][\[CurlyPhi]i[2][0]][
              r] + 2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
          2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
          Derivative[2][\[CurlyPhi]i[2][2]][r] + 4*Derivative[2][\[CurlyPhi]][
            r]*hi[2][0][r] - 2*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r] + 
          4*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
          2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
          2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
          Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
          Derivative[1][\[CurlyPhi]][r]^2*(4*hi[2][0][r] - 2*hi[2][2][r] + 
            4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
            \[CurlyPhi]i[2][2][r]))))/8 + 
     (H2[r]*(A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][hi[2][0]][r] - 
            Derivative[1][hi[2][2]][r]) + 8*pi[2][0][r] - 4*pi[2][2][r]) + 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]*(-2*r^2*Derivative[1][\[CurlyPhi]][r]*
           Derivative[1][wi[1][1]][r]*wi[1][1][r] + Derivative[1][A][r]*
           (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
            Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
              r]*(8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
              \[CurlyPhi]i[2][2][r])))))/(8*A[r]) - 
     (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
       (A[r]^2*(Derivative[1][B][r]*(-4*Derivative[1][hi[2][0]][r] + 
            2*Derivative[1][hi[2][2]][r]) + 4*B[r]*
           (-2*Derivative[2][hi[2][0]][r] + Derivative[2][hi[2][2]][r])) - 
        B[r]*Derivative[1][A][r]*(2*r^2*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] + Derivative[1][A][r]*(4*pi[2][0][r] - 2*pi[2][2][r] - 
            2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])) + 
        A[r]*(2*B[r]*(-(Derivative[1][A][r]*(4*Derivative[1][hi[2][0]][r] - 2*
                Derivative[1][hi[2][2]][r] - 2*Derivative[1][pi[2][0]][r] + 
               Derivative[1][pi[2][2]][r])) + 3*r^2*Derivative[1][wi[1][1]][
               r]^2 + 4*Derivative[2][A][r]*pi[2][0][r] - 
            2*Derivative[2][A][r]*pi[2][2][r] + 6*r*Derivative[1][wi[1][1]][
              r]*wi[1][1][r] + 2*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] - 
            2*Derivative[2][A][r]*\[CurlyPhi]i[2][0][r] + Derivative[2][A][r]*
             \[CurlyPhi]i[2][2][r]) + Derivative[1][B][r]*
           (2*r^2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
            Derivative[1][A][r]*(4*pi[2][0][r] - 2*pi[2][2][r] - 
              2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][r])))))/
      (8*r*A[r]^2)))/2, 
 -1/2*(a^2*((3*H0[r]*(-4*hi[2][2][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]][r]*(Derivative[1][pi[2][2]][r] - 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
         Derivative[2][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]^
           2*(2*hi[2][2][r] + 2*pi[2][2][r] - \[CurlyPhi]i[2][2][r]) + 
         Derivative[2][\[CurlyPhi]][r]*(2*hi[2][2][r] + 2*pi[2][2][r] - 
           \[CurlyPhi]i[2][2][r])) - E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         Derivative[1][\[CurlyPhi]][r]*(-2*hi[2][2][r] - 2*pi[2][2][r] + 
           \[CurlyPhi]i[2][2][r]))))/8 + 
    (H2[r]*(6*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][hi[2][2]][r] + 2*pi[2][2][r]) + 
       (E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*r^2*Derivative[1][\[CurlyPhi]][r]*
           Derivative[1][wi[1][1]][r]*wi[1][1][r] + 3*Derivative[1][A][r]*
           (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
              r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))))/A[r]))/8 + 
    (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
      (6*A[r]^2*(Derivative[1][B][r]*Derivative[1][hi[2][2]][r] + 
         2*B[r]*Derivative[2][hi[2][2]][r]) + B[r]*Derivative[1][A][r]*
        (-2*r^2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
         Derivative[1][A][r]*(6*pi[2][2][r] - 3*\[CurlyPhi]i[2][2][r])) + 
       A[r]*(2*B[r]*(Derivative[1][A][r]*(6*Derivative[1][hi[2][2]][r] - 
             3*Derivative[1][pi[2][2]][r]) + 3*r^2*Derivative[1][wi[1][1]][r]^
             2 - 6*Derivative[2][A][r]*pi[2][2][r] + 
           6*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
           2*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] + 
           3*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r]) + 
         Derivative[1][B][r]*(2*r^2*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
           Derivative[1][A][r]*(-6*pi[2][2][r] + 3*\[CurlyPhi]i[2][2][r])))))/
     (8*r*A[r]^2))), 
 (-(Derivative[1][h1][r]*(B[r] - (E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*
        Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r])/(2*A[r]))) + 
   ((I/2)*\[Omega]*h0[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r])))/
    A[r] - (h1[r]*(2*A[r]^2*Derivative[1][B][r] + E^\[CurlyPhi][r]*\[Alpha]*
       B[r]^2*Derivative[1][A][r]^2*Derivative[1][\[CurlyPhi]][r] - 
      A[r]*B[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][
          r]*Derivative[2][A][r] + Derivative[1][A][r]*
         (-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][
             r])))))/(4*A[r]^2) - 
   a^2*((r*B[r]*h1[r]*Derivative[1][wi[1][1]][r]*(-2*r*wi[1][1][r] + 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
         (r*Derivative[1][wi[1][1]][r] + 2*wi[1][1][r])))/(4*A[r]) + 
     ((I/4)*\[Omega]*h0[r]*(-8*hi[2][0][r] + 4*hi[2][2][r] - 
        E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
          Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
            r]*(4*hi[2][0][r] - 2*hi[2][2][r] + 4*pi[2][0][r] - 
            2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][
             r])) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][pi[2][0]][r] - 
            Derivative[1][pi[2][2]][r] - 4*Derivative[1][\[CurlyPhi]i[2][0]][
              r] + 2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
          2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
          Derivative[2][\[CurlyPhi]i[2][2]][r] + 4*Derivative[2][\[CurlyPhi]][
            r]*hi[2][0][r] - 2*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r] + 
          4*Derivative[2][\[CurlyPhi]][r]*pi[2][0][r] - 
          2*Derivative[2][\[CurlyPhi]][r]*pi[2][2][r] - 
          2*Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][0][r] + 
          Derivative[2][\[CurlyPhi]][r]*\[CurlyPhi]i[2][2][r] + 
          Derivative[1][\[CurlyPhi]][r]^2*(4*hi[2][0][r] - 2*hi[2][2][r] + 
            4*pi[2][0][r] - 2*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
            \[CurlyPhi]i[2][2][r]))))/A[r] - 
     (B[r]*Derivative[1][h1][r]*(A[r]*(2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           Derivative[1][\[CurlyPhi]][r]*(2*Derivative[1][hi[2][0]][r] - 
            Derivative[1][hi[2][2]][r]) + 8*pi[2][0][r] - 4*pi[2][2][r]) + 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]*(-2*r^2*Derivative[1][\[CurlyPhi]][r]*
           Derivative[1][wi[1][1]][r]*wi[1][1][r] + Derivative[1][A][r]*
           (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
            Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
              r]*(8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
              \[CurlyPhi]i[2][2][r])))))/(4*A[r]) - 
     (h1[r]*(2*A[r]^2*(B[r]*((-4 + 6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][
                 B][r]*Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][0]][
              r] + (2 - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
               Derivative[1][\[CurlyPhi]][r])*Derivative[1][hi[2][2]][r] + 
            4*Derivative[1][pi[2][0]][r] - 2*Derivative[1][pi[2][2]][r]) + 
          2*E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*(Derivative[1][\[CurlyPhi]][r]^2*
             (2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][r]) + 
            (2*Derivative[1][hi[2][0]][r] - Derivative[1][hi[2][2]][r])*
             Derivative[2][\[CurlyPhi]][r] + Derivative[1][\[CurlyPhi]][r]*
             (2*Derivative[2][hi[2][0]][r] - Derivative[2][hi[2][2]][r])) + 
          2*Derivative[1][B][r]*(2*pi[2][0][r] - pi[2][2][r])) - 
        E^\[CurlyPhi][r]*\[Alpha]*B[r]^2*Derivative[1][A][r]*
         (-2*r^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] + Derivative[1][A][r]*
           (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - 
            Derivative[1][\[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][
              r]*(8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
              \[CurlyPhi]i[2][2][r]))) - A[r]*B[r]*
         (6*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] + Derivative[1][A][r]*(-8*pi[2][0][r] + 
            4*pi[2][2][r] - 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             (2*Derivative[1][\[CurlyPhi]i[2][0]][r] - Derivative[1][
                \[CurlyPhi]i[2][2]][r] - Derivative[1][\[CurlyPhi]][r]*(
                8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
                \[CurlyPhi]i[2][2][r]))) + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
           (-2*Derivative[1][\[CurlyPhi]i[2][0]][r]*Derivative[2][A][r] + 
            Derivative[1][\[CurlyPhi]i[2][2]][r]*Derivative[2][A][r] + 
            2*r^2*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r]*
             wi[1][1][r] + 2*r^2*Derivative[1][wi[1][1]][r]*
             Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] + Derivative[1][A][r]*
             (Derivative[1][\[CurlyPhi]][r]*(-4*Derivative[1][hi[2][0]][r] + 
                2*Derivative[1][hi[2][2]][r] + 6*Derivative[1][pi[2][0]][r] - 
                3*Derivative[1][pi[2][2]][r] - 4*Derivative[1][
                   \[CurlyPhi]i[2][0]][r] + 2*Derivative[1][\[CurlyPhi]i[2][
                    2]][r]) - 2*Derivative[2][\[CurlyPhi]i[2][0]][r] + 
              Derivative[2][\[CurlyPhi]i[2][2]][r] + 8*Derivative[2][
                 \[CurlyPhi]][r]*pi[2][0][r] - 4*Derivative[2][\[CurlyPhi]][
                r]*pi[2][2][r] - 2*Derivative[2][\[CurlyPhi]][r]*
               \[CurlyPhi]i[2][0][r] + Derivative[2][\[CurlyPhi]][r]*
               \[CurlyPhi]i[2][2][r] + Derivative[1][\[CurlyPhi]][r]^2*(
                8*pi[2][0][r] - 4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + 
                \[CurlyPhi]i[2][2][r])) + Derivative[1][\[CurlyPhi]][r]*
             (2*r^2*Derivative[1][wi[1][1]][r]^2 + 4*r*Derivative[1][
                 wi[1][1]][r]*wi[1][1][r] + 2*r^2*Derivative[2][wi[1][1]][
                r]*wi[1][1][r] + Derivative[2][A][r]*(8*pi[2][0][r] - 
                4*pi[2][2][r] - 2*\[CurlyPhi]i[2][0][r] + \[CurlyPhi]i[2][2][
                 r]))))))/(8*A[r]^2)))/2, 
 -1/2*(a^2*(-1/4*(r*B[r]*h1[r]*Derivative[1][wi[1][1]][r]*
       (-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
          2*wi[1][1][r])))/A[r] + (((3*I)/4)*\[Omega]*h0[r]*
      (-4*hi[2][2][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
        (Derivative[1][\[CurlyPhi]][r]*(Derivative[1][pi[2][2]][r] - 
           2*Derivative[1][\[CurlyPhi]i[2][2]][r]) - 
         Derivative[2][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][r]^
           2*(2*hi[2][2][r] + 2*pi[2][2][r] - \[CurlyPhi]i[2][2][r]) + 
         Derivative[2][\[CurlyPhi]][r]*(2*hi[2][2][r] + 2*pi[2][2][r] - 
           \[CurlyPhi]i[2][2][r])) - E^\[CurlyPhi][r]*\[Alpha]*
        Derivative[1][B][r]*(Derivative[1][\[CurlyPhi]i[2][2]][r] + 
         Derivative[1][\[CurlyPhi]][r]*(-2*hi[2][2][r] - 2*pi[2][2][r] + 
           \[CurlyPhi]i[2][2][r]))))/A[r] - 
    (B[r]*Derivative[1][h1][r]*
      (6*A[r]*(E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][hi[2][2]][r] + 2*pi[2][2][r]) + 
       E^\[CurlyPhi][r]*\[Alpha]*B[r]*(2*r^2*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][wi[1][1]][r]*wi[1][1][r] + 3*Derivative[1][A][r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][2][r])))))/(4*A[r]) - 
    (h1[r]*(6*A[r]^2*B[r]*((-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*
            Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r])*
          Derivative[1][hi[2][2]][r] + 2*Derivative[1][pi[2][2]][r]) + 
       12*E^\[CurlyPhi][r]*\[Alpha]*A[r]^2*B[r]^2*
        (Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][hi[2][2]][r] + 
         Derivative[1][hi[2][2]][r]*Derivative[2][\[CurlyPhi]][r] + 
         Derivative[1][\[CurlyPhi]][r]*Derivative[2][hi[2][2]][r]) + 
       12*A[r]^2*Derivative[1][B][r]*pi[2][2][r] - E^\[CurlyPhi][r]*\[Alpha]*
        B[r]^2*Derivative[1][A][r]*(2*r^2*Derivative[1][\[CurlyPhi]][r]*
          Derivative[1][wi[1][1]][r]*wi[1][1][r] + 3*Derivative[1][A][r]*
          (Derivative[1][\[CurlyPhi]i[2][2]][r] + Derivative[1][\[CurlyPhi]][
             r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))) + 
       2*E^\[CurlyPhi][r]*\[Alpha]*A[r]*B[r]^2*
        (3*Derivative[1][\[CurlyPhi]i[2][2]][r]*Derivative[2][A][r] + 
         2*r^2*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r]*
          wi[1][1][r] + 2*r^2*Derivative[1][wi[1][1]][r]*
          Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] + 
         Derivative[1][\[CurlyPhi]][r]*(2*r^2*Derivative[1][wi[1][1]][r]^2 + 
           4*r*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
           2*r^2*Derivative[2][wi[1][1]][r]*wi[1][1][r] + 
           3*Derivative[2][A][r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][2][r])) + 
         3*Derivative[1][A][r]*(Derivative[1][\[CurlyPhi]][r]*
            (2*Derivative[1][hi[2][2]][r] - 3*Derivative[1][pi[2][2]][r] + 
             2*Derivative[1][\[CurlyPhi]i[2][2]][r]) + 
           Derivative[2][\[CurlyPhi]i[2][2]][r] + 
           Derivative[1][\[CurlyPhi]][r]^2*(-4*pi[2][2][r] + 
             \[CurlyPhi]i[2][2][r]) + Derivative[2][\[CurlyPhi]][r]*
            (-4*pi[2][2][r] + \[CurlyPhi]i[2][2][r]))) + 
       3*A[r]*B[r]*(2*E^\[CurlyPhi][r]*r^2*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
          wi[1][1][r] + Derivative[1][A][r]*(4*pi[2][2][r] + 
           3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
            (Derivative[1][\[CurlyPhi]i[2][2]][r] + 
             Derivative[1][\[CurlyPhi]][r]*(-4*pi[2][2][r] + \[CurlyPhi]i[2][
                 2][r]))))))/(8*A[r]^2))), 
 -1/8*(a^2*r*\[ScriptL]*(1 + \[ScriptL])*B[r]*h1[r]*
    Derivative[1][wi[1][1]][r]*(-2*r*wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*
      B[r]*Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r] + 
       2*wi[1][1][r])))/A[r], 
 a^2*((E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[2][\[CurlyPhi]1][r]*
     (r^4*B[r]*Derivative[1][wi[1][1]][r]^2 - 12*A[r]*hi[2][2][r]))/
    (8*r*A[r]) + (H0[r]*(-(E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*
        Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[1][wi[1][1]][r]^2) + r^3*A[r]*B[r]*Derivative[1][wi[1][1]][
        r]*(Derivative[1][wi[1][1]][r]*
         (r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r]) + 2*E^\[CurlyPhi][r]*\[Alpha]*
           B[r]*(6*Derivative[1][\[CurlyPhi]][r] + 
            r*Derivative[1][\[CurlyPhi]][r]^2 + r*Derivative[2][\[CurlyPhi]][
              r])) + 4*E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
         Derivative[1][\[CurlyPhi]][r]*Derivative[2][wi[1][1]][r]) + 
      A[r]^2*(24*hi[2][2][r] - 12*E^\[CurlyPhi][r]*\[Alpha]*
         Derivative[1][B][r]*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 
        2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*(-12*Derivative[1][\[CurlyPhi]][r]^2*
           hi[2][2][r] - 12*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r]))))/
    (16*A[r]^2) + (E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][\[CurlyPhi]1][r]*
     (-(r^5*B[r]^2*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]^2) - 
      12*A[r]^2*(r*Derivative[1][B][r] + 
        4*B[r]*(-1 + r*Derivative[1][\[CurlyPhi]][r]))*hi[2][2][r] + 
      r*A[r]*B[r]*(4*r^3*B[r]*Derivative[1][wi[1][1]][r]*
         ((2 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r] + 
          r*Derivative[2][wi[1][1]][r]) + 
        3*(r^4*Derivative[1][B][r]*Derivative[1][wi[1][1]][r]^2 - 
          4*Derivative[1][A][r]*pi[2][2][r]))))/(16*r^2*A[r]^2) - 
   (E^\[CurlyPhi][r]*r^4*\[Alpha]*B[r]^2*Derivative[1][\[CurlyPhi]][r]*
     Derivative[1][wi[1][1]][r]*Derivative[2][K][r]*wi[1][1][r])/(4*A[r]) - 
   (r^2*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
       Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
       (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
     wi[1][1][r]^2)/(4*A[r]) - (\[ScriptL]*(1 + \[ScriptL])*
     (-1/4*(E^\[CurlyPhi][r]*r^2*\[Alpha]*B[r]*H2[r]*
         Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]*
         wi[1][1][r])/A[r] - (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
        (-(r*B[r]*Derivative[1][A][r]*Derivative[1][wi[1][1]][r]) + 
         A[r]*(r*Derivative[1][B][r]*Derivative[1][wi[1][1]][r] + 
           2*B[r]*(3*Derivative[1][wi[1][1]][r] + r*Derivative[2][wi[1][1]][
               r])))*wi[1][1][r])/(4*A[r]^2) + 
      (r^2*K[r]*(-2 + E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
          Derivative[1][\[CurlyPhi]][r] + 2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
          (Derivative[1][\[CurlyPhi]][r]^2 + Derivative[2][\[CurlyPhi]][r]))*
        wi[1][1][r]^2)/(4*A[r])))/2 - 
   (r^3*B[r]*Derivative[1][K][r]*(-(E^\[CurlyPhi][r]*r*\[Alpha]*B[r]*
        Derivative[1][A][r]*Derivative[1][\[CurlyPhi]][r]*
        Derivative[1][wi[1][1]][r]*wi[1][1][r]) + 
      A[r]*(r*(-4 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
           Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]*
         wi[1][1][r] + E^\[CurlyPhi][r]*\[Alpha]*B[r]*
         (2*r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r]*
           wi[1][1][r] + 2*r*Derivative[1][wi[1][1]][r]*
           Derivative[2][\[CurlyPhi]][r]*wi[1][1][r] + 
          Derivative[1][\[CurlyPhi]][r]*(r*Derivative[1][wi[1][1]][r]^2 + 
            12*Derivative[1][wi[1][1]][r]*wi[1][1][r] + 
            2*r*Derivative[2][wi[1][1]][r]*wi[1][1][r])))))/(8*A[r]^2) + 
   (((3*I)/2)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*B[r]*Derivative[1][H1][r]*
     \[CurlyPhi]i[2][2][r])/A[r] + (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*
     Derivative[2][H0][r]*\[CurlyPhi]i[2][2][r])/4 + 
   (3*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][H2][r]*
     (-(r^4*B[r]*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]^
         2) + 4*A[r]*Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + 
      2*Derivative[1][A][r]*\[CurlyPhi]i[2][2][r]))/(16*A[r]) + 
   ((I/8)*E^\[CurlyPhi][r]*\[Alpha]*\[Omega]*H1[r]*
     (-(r^4*B[r]^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]^
         2) + 12*A[r]*B[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
      6*A[r]*Derivative[1][B][r]*\[CurlyPhi]i[2][2][r]))/A[r]^2 + 
   (E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][H0][r]*
     (r^4*B[r]^2*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]^2 + 
      6*A[r]*Derivative[1][B][r]*\[CurlyPhi]i[2][2][r] + 
      12*B[r]*(A[r]*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
        Derivative[1][A][r]*\[CurlyPhi]i[2][2][r])))/(16*A[r]) + 
   (E^\[CurlyPhi][r]*\[Alpha]*\[CurlyPhi]1[r]*
     (2*A[r]^2*(-6*r*Derivative[1][B][r]*(-1 + r*Derivative[1][\[CurlyPhi]][
            r])*hi[2][2][r] - 12*B[r]*(2 - 2*r*Derivative[1][\[CurlyPhi]][
            r] + r^2*Derivative[1][\[CurlyPhi]][r]^2 + 
          r^2*Derivative[2][\[CurlyPhi]][r])*hi[2][2][r]) + 
      r^2*B[r]*(r^3*(2*r*\[Omega]^2 + B[r]*Derivative[1][A][r]*
           (1 - r*Derivative[1][\[CurlyPhi]][r]))*Derivative[1][wi[1][1]][r]^
          2 + 6*Derivative[1][A][r]^2*\[CurlyPhi]i[2][2][r]) + 
      r*A[r]*(2*r^3*B[r]^2*Derivative[1][wi[1][1]][r]*
         (Derivative[1][wi[1][1]][r]*(-4 + 4*r*Derivative[1][\[CurlyPhi]][
              r] + r^2*Derivative[1][\[CurlyPhi]][r]^2 + 
            r^2*Derivative[2][\[CurlyPhi]][r]) + 
          2*r*(-1 + r*Derivative[1][\[CurlyPhi]][r])*Derivative[2][wi[1][1]][
            r]) - r*(24*\[Omega]^2*pi[2][2][r] + 6*Derivative[1][A][r]*
           Derivative[1][B][r]*\[CurlyPhi]i[2][2][r]) + 
        B[r]*(3*r^4*Derivative[1][B][r]*(-1 + r*Derivative[1][\[CurlyPhi]][
              r])*Derivative[1][wi[1][1]][r]^2 + 2*Derivative[1][A][r]*
           (6*pi[2][2][r] - 6*r*Derivative[1][\[CurlyPhi]][r]*pi[2][2][r]) - 
          12*r*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r]))))/
    (16*r^3*A[r]^2) + 
   (H2[r]*(2*A[r]^2*(6*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
         Derivative[1][\[CurlyPhi]][r]*hi[2][2][r] + E^\[CurlyPhi][r]*
         \[Alpha]*B[r]*(12*Derivative[1][\[CurlyPhi]][r]^2*hi[2][2][r] + 
          12*Derivative[2][\[CurlyPhi]][r]*hi[2][2][r]) - 12*pi[2][2][r]) + 
      2*E^\[CurlyPhi][r]*\[Alpha]*B[r]*Derivative[1][A][r]*
       (r^4*B[r]*Derivative[1][\[CurlyPhi]][r]*Derivative[1][wi[1][1]][r]^2 - 
        3*Derivative[1][A][r]*\[CurlyPhi]i[2][2][r]) - 
      A[r]*(4*E^\[CurlyPhi][r]*r^3*\[Alpha]*B[r]^2*Derivative[1][wi[1][1]][r]*
         (r*Derivative[1][\[CurlyPhi]][r]^2*Derivative[1][wi[1][1]][r] + 
          r*Derivative[1][wi[1][1]][r]*Derivative[2][\[CurlyPhi]][r] + 
          2*Derivative[1][\[CurlyPhi]][r]*(3*Derivative[1][wi[1][1]][r] + 
            r*Derivative[2][wi[1][1]][r])) + 6*E^\[CurlyPhi][r]*\[Alpha]*
         (2*\[Omega]^2 - Derivative[1][A][r]*Derivative[1][B][r])*
         \[CurlyPhi]i[2][2][r] + 
        B[r]*(2*r^4*(-2 + 3*E^\[CurlyPhi][r]*\[Alpha]*Derivative[1][B][r]*
             Derivative[1][\[CurlyPhi]][r])*Derivative[1][wi[1][1]][r]^2 - 
          E^\[CurlyPhi][r]*\[Alpha]*(24*Derivative[1][A][r]*
             Derivative[1][\[CurlyPhi]][r]*pi[2][2][r] + 
            12*Derivative[2][A][r]*\[CurlyPhi]i[2][2][r])))))/(16*A[r]^2))}
