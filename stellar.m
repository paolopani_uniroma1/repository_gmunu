(* ::Package:: *)

BeginPackage["stellar`"];


(* ::Subsubsection:: *)
(*Usage*)


tov::usage =
 	"tov[eos,p0,poly,K,\[Gamma],profile] solves the TOV equations for central pressure p0.";

interpeos::usage = 
	"Interpolation of energy and density profiles.";

tovPW::usage =
 	"tovPW[p0,\[ScriptP]1,\[Gamma]1,\[Gamma]2,\[Gamma]3,profile] solves the TOV equations for PW polytropes with parameters (\[ScriptP]1,\[Gamma]1,\[Gamma]2,\[Gamma]3) and central pressure p0.";
 	
pweos::usage = 
	"pweos[pre,\[ScriptP]1,\[Gamma]1,\[Gamma]2,\[Gamma]3] build the PW eos for a specific set of parameters.";


Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*Logarithmic interpolation of energy and pressure profiles*)


interpeos[eos_]:=Block[{mB=938.27*10^6,\[Rho]fact=1.322*10^(-12),\[Epsilon]fact=7.42427*10^(-19),
	pfact = 8.2611*10^(-40),\[Epsilon],\[Rho],x,pmin,pmax,\[Kappa]1,\[CapitalGamma]1,\[Kappa]2,\[CapitalGamma]2,\[Rho]fin},


\[Rho] = Interpolation[Thread[{Log[eos[[All,4]]pfact],eos[[All,2]] mB \[Rho]fact}]];

\[Epsilon] = Interpolation[Thread[{Log[eos[[All,4]]pfact],eos[[All,3]]\[Epsilon]fact}]];

{pmin,pmax} = {Min[eos[[All,4]]],Max[eos[[All,4]]]}pfact;

\[CapitalGamma]1 = 4/3;
\[Kappa]1 = pmin/Min[eos[[All,2]] mB \[Rho]fact]^\[CapitalGamma]1;


\[CapitalGamma]2 = 2;
\[Kappa]2 = pmax/Max[eos[[All,2]] mB \[Rho]fact]^\[CapitalGamma]2;

Return[{\[Rho],\[Epsilon]}]

]


(* ::Subsection::Closed:: *)
(*Piecewise EoS*)


(* Input e output are in Geometrized units, but the  calculations are made in 
 cgs to take into account polytropic index. Note that eps has the same dimension 
 of rho, i.i. it's divided by c^2*)


pweos[pre_,\[ScriptP]1_,\[Gamma]1_,\[Gamma]2_,\[Gamma]3_]:=Block[{\[Rho],\[Epsilon],p,p2,\[Epsilon]fact=7.42427*10^(-19),pfact = 8.2611*10^(-40),cc2=299792 10^5,p1=10^\[ScriptP]1,\[Rho]1,\[Rho]2,\[Rho]0=2.7 10^14,K1,K2,K3,\[CapitalGamma]0,K0,\[CapitalGamma]1c,\[CapitalGamma]2c,\[CapitalGamma]3c,K1c,K2c,K3c,
\[Rho]c,p0,\[Rho]1c,\[Rho]2c,\[Rho]3c,p1c,p2c,p3c,a0,a1,a2,a3,e0,e1,e2,aC1,aC2,aC3,eC1,eC2,eC3},

(* crust piece - Sly4 *)

p = pre/pfact;

{\[Rho]1,\[Rho]2} = {1.85,3.7}\[Rho]0;

{K1,K2,K3}={p1/\[Rho]1^\[Gamma]1,p1/\[Rho]1^\[Gamma]2,K2 \[Rho]2^(\[Gamma]2-\[Gamma]3)};
p2 = K2*\[Rho]2^\[Gamma]2;

{\[CapitalGamma]0,K0} = {1.35692,3.99874 10^-8 cc2^2};

{\[CapitalGamma]1c,\[CapitalGamma]2c,\[CapitalGamma]3c}={0.62223,1.28733,1.58425};
{K1c,K2c,K3c}={5.32697*10,1.06186 10^-6,6.8011 10^-9}cc2^2;

\[Rho]c = (K0/K1)^(1/(\[Gamma]1-\[CapitalGamma]0));
p0 = K0 \[Rho]c^\[CapitalGamma]0;

{\[Rho]1c,\[Rho]2c,\[Rho]3c} = {2.6278 10^12,3.78358 10^11,2.44034 10^7};
{p1c,p2c,p3c} = {K1c \[Rho]1c^\[CapitalGamma]1c,K2c \[Rho]2c^\[CapitalGamma]2c,K3c \[Rho]3c^\[CapitalGamma]3c};

(* EoS *)

aC3 = 0;
eC3 = \[Rho]3c+K3c/cc2^2/(\[CapitalGamma]3c-1)*\[Rho]3c^\[CapitalGamma]3c;

aC2 = eC3/\[Rho]3c-1-K2c/cc2^2/(\[CapitalGamma]2c-1)*\[Rho]3c^(\[CapitalGamma]2c-1);
eC2 = (1+aC2)*\[Rho]2c+K2c/cc2^2/(\[CapitalGamma]2c-1)*\[Rho]2c^\[CapitalGamma]2c;

aC1 = eC2/\[Rho]2c-1-K1c/cc2^2/(\[CapitalGamma]1c-1)*\[Rho]2c^(\[CapitalGamma]1c-1);
eC1 = (1+aC1)*\[Rho]1c+K1c/cc2^2/(\[CapitalGamma]1c-1)*\[Rho]1c^\[CapitalGamma]1c;

a0 = eC1/\[Rho]1c-1-K0/cc2^2/(\[CapitalGamma]0-1)*\[Rho]1c^(\[CapitalGamma]0-1);
e0 = (1+a0)*\[Rho]c+K0/cc2^2/(\[CapitalGamma]0-1)*\[Rho]c^\[CapitalGamma]0;

a1 = e0/\[Rho]c-1-K1/cc2^2/(\[Gamma]1-1)*\[Rho]c^(\[Gamma]1-1);
e1 = (1+a1)*\[Rho]1+K1/cc2^2/(\[Gamma]1-1)*\[Rho]1^\[Gamma]1;

a2 = e1/\[Rho]1-1-K2/cc2^2/(\[Gamma]2-1)*\[Rho]1^(\[Gamma]2-1);
e2 = (1+a2)*\[Rho]2+K2/cc2^2/(\[Gamma]2-1)*\[Rho]2^\[Gamma]2;

a3 = e2/\[Rho]2-1-K3/cc2^2/(\[Gamma]3-1)*\[Rho]2^(\[Gamma]3-1);
		
\[Rho] = Which[p >= p2,(p/K3)^(1/\[Gamma]3), p < p2 && p >= p1, (p/K2)^(1/\[Gamma]2),
		p < p1 && p >= p0, (p/K1)^(1/\[Gamma]1),p < p0 && p >= p1c, (p/K0)^(1/\[CapitalGamma]0),
		p < p1c && p >= p2c, (p/K1c)^(1/\[CapitalGamma]1c),p < p2c && p >= p3c, (p/K2c)^(1/\[CapitalGamma]2c),
		p < p3c,(p/K3c)^(1/\[CapitalGamma]3c)];

\[Epsilon] = Which[p >= p2,(1+a3)\[Rho]+K3/cc2^2/(\[Gamma]3-1) \[Rho]^\[Gamma]3, p < p2 && p >= p1, (1+a2)\[Rho]+K2/cc2^2/(\[Gamma]2-1) \[Rho]^\[Gamma]2,
		p < p1 && p >= p0, (1+a1)\[Rho]+K1/cc2^2/(\[Gamma]1-1) \[Rho]^\[Gamma]1,p < p0 && p >= p1c, (1+a0)\[Rho]+K0/cc2^2/(\[CapitalGamma]0-1) \[Rho]^\[CapitalGamma]0,
		p < p1c && p >= p2c, (1+aC1)\[Rho]+K1c/cc2^2/(\[CapitalGamma]1c-1) \[Rho]^\[CapitalGamma]1c,p < p2c && p >= p3c, (1+aC2)\[Rho]+K2c/cc2^2/(\[CapitalGamma]2c-1) \[Rho]^\[CapitalGamma]2c,
		p < p3c,(1+aC3)\[Rho]+K3c/cc2^2/(\[CapitalGamma]3c-1) \[Rho]^\[CapitalGamma]3c];
		

Return[{\[Rho] \[Epsilon]fact,\[Epsilon] \[Epsilon]fact}]

]


(* ::Subsection::Closed:: *)
(*TOV solver*)


tov[eos_,p0_,poly_,K_,\[Gamma]_,profile_]:=Block[{msun=1.4768,\[Rho],\[Epsilon],r,m,\[Nu],p,rI,mI,\[Nu]I,sol,M,R,x1,x2,
		rules,H,\[Beta],a0,HI,\[Beta]I,A,B,grr,f,\[ScriptCapitalC],w,k2,u,\[Chi],uI,\[Chi]I,I0,\[Lambda],n,prof,P,dr,i},

rules = Join[{Method->{"DoubleStep",Method->{"ExplicitRungeKutta",
	"DifferenceOrder"->8}},PrecisionGoal->5,AccuracyGoal->5}];

(* --- Interpolate energy-density profiles for the EOS --- *)

Which[poly == "F",
 
	{\[Rho],\[Epsilon]} = interpeos[eos],
	
poly == "T",

	\[Rho][p_] = (Exp[p]/K)^(1/\[Gamma]);
	\[Epsilon][p_] = \[Rho][p] + 0 Exp[p]/(\[Gamma]-1)
	
];

(* --- Initial Conditions --- *)

{x1,x2} = {Log[p0],Log[p0 10^-12]};

rI = Sqrt[(1.5 10^(-9) p0/\[Pi])/((\[Epsilon][x1]+p0)(\[Epsilon][x1]+3p0))];
mI = 4/3 \[Pi] \[Epsilon][x1] rI^3;
\[Chi]I = 1;

\[Nu]I = 0;
a0 = 0.1;
HI = a0*rI^2;
\[Beta]I = 2*a0 rI;
uI = 0;


(* --- Solve TOV to find \[Nu]0 --- *)

sol = NDSolve[
	{r'[p] == -r[p]*(r[p]-2m[p])/(\[Epsilon][p]+Exp[p])/(m[p]+4\[Pi] r[p]^3*Exp[p])Exp[p],
	m'[p] == 4\[Pi] r[p]^2 \[Epsilon][p] r'[p],
	\[Nu]'[p] == -Exp[p]/(\[Epsilon][p]+Exp[p]),
	r[x1] == rI, m[x1] == mI, \[Nu][x1] == \[Nu]I},
	{r,m,\[Nu]},{p,x1,x2},rules];

{M,R} = {m[x2],r[x2]}/.sol[[1]];

\[Nu]I = 0.5 (Log[1-2M/R]-(\[Nu][x2]/.sol[[1]]));

(* --- Functions for the Love number integration --- *)

grr[p_] = 1/(1-2m[p]/r[p]);

f[p_]= \[Epsilon]'[p]/Exp[p];

A[p_] = 2grr[p](-2\[Pi](5\[Epsilon][p]+9Exp[p]+(\[Epsilon][p]+Exp[p])*f[p])+3/r[p]^2+2grr[p](m[p]/r[p]^2
			+4\[Pi] r[p]Exp[p])^2);
B[p_] = 2/r[p]grr[p]*(-1+m[p]/r[p]+2\[Pi] r[p]^2 (\[Epsilon][p]-Exp[p]));

(* --- Solve TOV for the complete configuration --- *)

sol = NDSolve[
	{r'[p] == -r[p]*(r[p]-2 m[p])/(\[Epsilon][p]+Exp[p])/(m[p]+4\[Pi] r[p]^3*Exp[p])Exp[p],
	m'[p] == 4\[Pi] r[p]^2 \[Epsilon][p] r'[p],
	H'[p] == \[Beta][p]*r'[p],
	\[Beta]'[p] == (H[p]*A[p]+\[Beta][p]*B[p])r'[p],
	u'[p] == (16\[Pi] r[p]^5(\[Epsilon][p]+Exp[p])\[Chi][p]/(r[p]-2m[p]))r'[p],
	\[Chi]'[p] == (u[p]/r[p]^4-4\[Pi] r[p]^2(\[Epsilon][p]+Exp[p])\[Chi][p]/
			(r[p]-2m[p]))r'[p],
	r[x1] == rI, m[x1] == mI,H[x1] == HI, \[Beta][x1] == \[Beta]I, u[x1] == uI,\[Chi][x1] == \[Chi]I},
	{r,m,H,\[Beta],u,\[Chi]},{p,x1,x2},rules];

\[ScriptCapitalC] = M/R ;

w = R (\[Beta][x2]/.sol[[1]])/(H[x2]/.sol[[1]]);

k2 = 8/5\[ScriptCapitalC]^5*(1-2*\[ScriptCapitalC])^2*(2+2\[ScriptCapitalC]*(w-1)-w)/(2\[ScriptCapitalC]*(6-3w + 
		3\[ScriptCapitalC]*(5w-8))+4\[ScriptCapitalC]^3*(13-11w+\[ScriptCapitalC]*(3w-2)+2\[ScriptCapitalC]^2 * 
		(1+w))+3(1-2\[ScriptCapitalC])^2*(2-w+2\[ScriptCapitalC]*(w-1))*Log[1-2\[ScriptCapitalC]]);   

\[Lambda]   = 2/3k2 R^5;

I0 = u[x2]/6/(\[Chi][x2]+u[x2]/(3*R^3))//.sol[[1]];
		
(* --- Print results --- *)

Print[TableForm[{{"",M/msun,R,\[ScriptCapitalC],I0,k2,\[Lambda] ""}},
		TableAlignments->Center,TableHeadings->{None,
		{"","M[\!\(\*SubscriptBox[\(M\), \(\[CircleDot]\)]\)]","R[km]","\[ScriptCapitalC]","I[\!\(\*SuperscriptBox[\(km\), \(3\)]\)]","\!\(\*SubscriptBox[\(k\), \(2\)]\)","\[Lambda][\!\(\*SuperscriptBox[\(km\), \(5\)]\)]"}}]];

Which[profile == "F",
 
	Return[{M/msun,R,I0,k2}],
	
profile == "T",

	n  = 2000;
	dr = (R-rI)/n;
	x1 = rI;
	P  = (1-10^(-9))p0;

	prof = Array[0,{n,4}];

	For[i=1,i<=n,i++, 

		x2 = x1 + dr;

		sol = NDSolve[
		{p'[r] == -(\[Epsilon][Log[P]]+P)(m[r]+4\[Pi] r^3*P)/
			r/(r-2m[r])/P,
		m'[r] == 4\[Pi] r^2 \[Epsilon][Log[P]],
		p[x1] == Log[P],m[x1] == mI},
		{p,m},{r,x1,x2},rules];

		P = Exp[(p[x2]/.sol)[[1]]];

		mI = (m[x2]/.sol)[[1]];

		prof[[i,1]] = x2;
		prof[[i,2]] = P;
		prof[[i,3]] = \[Rho][Log[P]];
		prof[[i,4]] = \[Epsilon][Log[P]];

		x1 = x2;

		Clear[sol]];

	Return[{M/msun,R,I0,k2,prof}]

]

]


(* ::Subsection::Closed:: *)
(*TOV solver for piecewise polytropes*)


tovPW[p0_,\[ScriptP]1_,\[Gamma]1_,\[Gamma]2_,\[Gamma]3_,profile_]:=Block[{msun=1.4768,\[Rho],\[Epsilon],r,m,\[Nu],p,rI,mI,\[Nu]I,sol,M,R,x1,x2,
		rules,H,\[Beta],a0,HI,\[Beta]I,A,B,grr,f,\[ScriptCapitalC],w,k2,u,\[Chi],uI,\[Chi]I,I0,\[Lambda],nn=2000,n,dr,P,prof,i},

rules = Join[{Method->{"DoubleStep",Method->{"ExplicitRungeKutta",
	"DifferenceOrder"->8}},PrecisionGoal->5,AccuracyGoal->5}];

(* --- Initial Conditions --- *)

{x1,x2} = {Log[p0],Log[p0 10^-12]};

(* --- build the pievewise EoS --- *)

\[Rho] = Interpolation[Table[{p,pweos[Exp[p],\[ScriptP]1,\[Gamma]1,\[Gamma]2,\[Gamma]3][[1]]},{p,x1,x2,(x2-x1)/nn}],InterpolationOrder->1];
\[Epsilon] = Interpolation[Table[{p,pweos[Exp[p],\[ScriptP]1,\[Gamma]1,\[Gamma]2,\[Gamma]3][[2]]},{p,x1,x2,(x2-x1)/nn}],InterpolationOrder->1];

rI = Sqrt[(1.5 10^(-9) p0/\[Pi])/((\[Epsilon][x1]+p0)(\[Epsilon][x1]+3p0))];
mI = 4/3 \[Pi] \[Epsilon][x1] rI^3;
\[Chi]I = 1;

\[Nu]I = 0;
a0 = 0.1;
HI = a0*rI^2;
\[Beta]I = 2*a0 rI;
uI = 0;
(* --- Solve TOV to find \[Nu]0 --- *)

sol = NDSolve[
	{r'[p] == -r[p]*(r[p]-2m[p])/(\[Epsilon][p]+Exp[p])/(m[p]+4\[Pi] r[p]^3*Exp[p])Exp[p],
	m'[p] == 4\[Pi] r[p]^2 \[Epsilon][p] r'[p],
	\[Nu]'[p] == -Exp[p]/(\[Epsilon][p]+Exp[p]),
	r[x1] == rI, m[x1] == mI, \[Nu][x1] == \[Nu]I},
	{r,m,\[Nu]},{p,x1,x2},rules];

{M,R} = {m[x2],r[x2]}/.sol[[1]];
Print[{M,R}];
\[Nu]I = 0.5 (Log[1-2M/R]-(\[Nu][x2]/.sol[[1]]));

(* --- Functions for the Love number integration --- *)

grr[p_] = 1/(1-2m[p]/r[p]);

f[p_]= \[Epsilon]'[p]/Exp[p];

A[p_] = 2grr[p](-2\[Pi](5\[Epsilon][p]+9Exp[p]+(\[Epsilon][p]+Exp[p])*f[p])+3/r[p]^2+2grr[p](m[p]/r[p]^2
			+4\[Pi] r[p]Exp[p])^2);
B[p_] = 2/r[p]grr[p]*(-1+m[p]/r[p]+2\[Pi] r[p]^2 (\[Epsilon][p]-Exp[p]));

(* --- Solve TOV for the complete configuration --- *)

sol = NDSolve[
	{r'[p] == -r[p]*(r[p]-2 m[p])/(\[Epsilon][p]+Exp[p])/(m[p]+4\[Pi] r[p]^3*Exp[p])Exp[p],
	m'[p] == 4\[Pi] r[p]^2 \[Epsilon][p] r'[p],
	H'[p] == \[Beta][p]*r'[p],
	\[Beta]'[p] == (H[p]*A[p]+\[Beta][p]*B[p])r'[p],
	u'[p] == (16\[Pi] r[p]^5(\[Epsilon][p]+Exp[p])\[Chi][p]/(r[p]-2m[p]))r'[p],
	\[Chi]'[p] == (u[p]/r[p]^4-4\[Pi] r[p]^2(\[Epsilon][p]+Exp[p])\[Chi][p]/
			(r[p]-2m[p]))r'[p],
	r[x1] == rI, m[x1] == mI,H[x1] == HI, \[Beta][x1] == \[Beta]I, u[x1] == uI,\[Chi][x1] == \[Chi]I},
	{r,m,H,\[Beta],u,\[Chi]},{p,x1,x2},rules];

\[ScriptCapitalC] = M/R ;

w = R (\[Beta][x2]/.sol[[1]])/(H[x2]/.sol[[1]]);

k2 = 8/5\[ScriptCapitalC]^5*(1-2*\[ScriptCapitalC])^2*(2+2\[ScriptCapitalC]*(w-1)-w)/(2\[ScriptCapitalC]*(6-3w + 
		3\[ScriptCapitalC]*(5w-8))+4\[ScriptCapitalC]^3*(13-11w+\[ScriptCapitalC]*(3w-2)+2\[ScriptCapitalC]^2 * 
		(1+w))+3(1-2\[ScriptCapitalC])^2*(2-w+2\[ScriptCapitalC]*(w-1))*Log[1-2\[ScriptCapitalC]]);   

\[Lambda]   = 2/3k2 R^5;

I0 = u[x2]/6/(\[Chi][x2]+u[x2]/(3*R^3))//.sol[[1]];
		
(* --- Print results --- *)

Print[TableForm[{{"",M/msun,R,\[ScriptCapitalC],I0,k2,\[Lambda] ""}},
		TableAlignments->Center,TableHeadings->{None,
		{"","M[\!\(\*SubscriptBox[\(M\), \(\[CircleDot]\)]\)]","R[km]","\[ScriptCapitalC]","I[\!\(\*SuperscriptBox[\(km\), \(3\)]\)]","\!\(\*SubscriptBox[\(k\), \(2\)]\)","\[Lambda][\!\(\*SuperscriptBox[\(km\), \(5\)]\)]"}}]];

Which[profile == "F",
 
	Return[{M/msun,R,I0,k2}],
	
profile == "T",

	n  = 2000;
	dr = (R-rI)/n;
	x1 = rI;
	P  = (1-10^(-9))p0;

	prof = Array[0,{n,4}];

	For[i=1,i<=n,i++, 

		x2 = x1 + dr;

		sol = NDSolve[
		{p'[r] == -(\[Epsilon][Log[P]]+P)(m[r]+4\[Pi] r^3*P)/
			r/(r-2m[r])/P,
		m'[r] == 4\[Pi] r^2 \[Epsilon][Log[P]],
		p[x1] == Log[P],m[x1] == mI},
		{p,m},{r,x1,x2},rules];

		P = Exp[(p[x2]/.sol)[[1]]];

		mI = (m[x2]/.sol)[[1]];

		prof[[i,1]] = x2;
		prof[[i,2]] = P;
		prof[[i,3]] = \[Rho][Log[P]];
		prof[[i,4]] = \[Epsilon][Log[P]];

		x1 = x2;

		Clear[sol]];

	Return[{M/msun,R,I0,k2,prof}]

];

]


(* ::Subsection::Closed:: *)
(*End*)


End[];

EndPackage[];

