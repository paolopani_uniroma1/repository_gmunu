(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 7.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[     17032,        490]
NotebookOptionsPosition[     16449,        465]
NotebookOutlinePosition[     16782,        480]
CellTagsIndexPosition[     16739,        477]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 StyleBox[Cell[TextData[{
   "Massive scalar bound states of a Kerr black hole\n",
   StyleBox[
   "References: Sam Dolan \[OpenCurlyDoubleQuote]Instability of the massive \
Klein-Gordon field on the Kerr spacetime\[CloseCurlyDoubleQuote] (2007);  \
Richard Brito, Vitor Cardoso and Paolo Pani, \[OpenCurlyDoubleQuote]Black \
hole superradiance: from fundamental physics to astrophysics\
\[CloseCurlyDoubleQuote] (2015)",
    FontSize->12]
  }], "Subtitle",
   CellChangeTimes->{
    3.5701092341113586`*^9, {3.570117652780466*^9, 3.5701176573116493`*^9}, {
     3.570118625235464*^9, 3.5701186769267683`*^9}, {3.570227375137948*^9, 
     3.570227401005803*^9}, {3.571072184950651*^9, 3.571072199211156*^9}, {
     3.571072309951724*^9, 3.571072360711537*^9}, 3.5710725997199793`*^9, {
     3.571072634393869*^9, 3.571072695533065*^9}}],
  FontSize->12]], "Subtitle",
 CellChangeTimes->{{3.604390909950038*^9, 3.604390998156616*^9}, {
  3.629784083942204*^9, 3.629784094105228*^9}, {3.629785487116335*^9, 
  3.6297855599142036`*^9}, {3.629785682461231*^9, 3.62978569023075*^9}}],

Cell["\<\
We first set the parameters of the Kerr spacetime, the field and the mode we\
\[CloseCurlyQuote]re interested in:\
\>", "Text",
 CellChangeTimes->{3.629785713139008*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"LEAVER", "[", 
    RowBox[{
    "w_", ",", "ar_", ",", "mu_", ",", "l_", ",", "m_", ",", "NITMAX_", ",", 
     "NITMAX2_"}], "]"}], ":=", 
   RowBox[{"(", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"br", "=", 
      RowBox[{"Sqrt", "[", 
       RowBox[{"1", "-", 
        RowBox[{"ar", "^", "2"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"rminus", "=", 
      RowBox[{"(", 
       RowBox[{"1", "-", "br"}], ")"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"rplus", "=", 
      RowBox[{"1", "+", "br"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"s", "=", "0"}], ";", "\[IndentingNewLine]", 
     RowBox[{"k1", "=", 
      RowBox[{
       RowBox[{"1", "/", "2"}], "*", 
       RowBox[{"Abs", "[", 
        RowBox[{"m", "-", "s"}], "]"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"k2", "=", 
      RowBox[{
       RowBox[{"1", "/", "2"}], "*", 
       RowBox[{"Abs", "[", 
        RowBox[{"m", "+", "s"}], "]"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Alm", "=", 
      RowBox[{
       RowBox[{"l", "*", 
        RowBox[{"(", 
         RowBox[{"l", "+", "1"}], ")"}]}], "-", 
       RowBox[{"s", "*", 
        RowBox[{"(", 
         RowBox[{"s", "+", "1"}], ")"}]}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"OmegaH", "=", 
      RowBox[{"ar", "/", 
       RowBox[{"(", 
        RowBox[{"2", "*", "M", "*", "rplus"}], ")"}]}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"(*", 
      RowBox[{
      "**", "**", "**", "**", "**", "**", "**", "**", "**", "**", "**", "**", 
       "**", "**", "**", "**"}], "*****)"}], "\[IndentingNewLine]", 
     RowBox[{"(*", "*****)"}], "\[IndentingNewLine]", 
     RowBox[{"k1", "=", 
      RowBox[{
       RowBox[{"1", "/", "2"}], "*", 
       RowBox[{"Abs", "[", 
        RowBox[{"m", "-", "s"}], "]"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"k2", "=", 
      RowBox[{
       RowBox[{"1", "/", "2"}], "*", 
       RowBox[{"Abs", "[", 
        RowBox[{"m", "+", "s"}], "]"}]}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"(*", 
      RowBox[{
       RowBox[{"Alm", "=", 
        RowBox[{
         RowBox[{"l", "*", 
          RowBox[{"(", 
           RowBox[{"l", "+", "1"}], ")"}]}], "-", 
         RowBox[{"s", "*", 
          RowBox[{"(", 
           RowBox[{"s", "+", "1"}], ")"}]}]}]}], ";"}], "*)"}], 
     "\[IndentingNewLine]", 
     RowBox[{"wang", "=", 
      SqrtBox[
       RowBox[{
        SuperscriptBox["w", "2"], "-", 
        SuperscriptBox["mu", "2"]}]]}], ";", "\[IndentingNewLine]", 
     "\[IndentingNewLine]", 
     RowBox[{"\[Gamma]ang", "=", 
      RowBox[{"Function", "[", 
       RowBox[{"n", ",", 
        RowBox[{"2", "*", "ar", "*", "wang", "*", 
         RowBox[{"(", 
          RowBox[{"n", "+", "k1", "+", "k2", "+", "s"}], ")"}]}]}], "]"}]}], 
     ";", 
     RowBox[{"\[Beta]ang", "=", 
      RowBox[{"Function", "[", 
       RowBox[{"n", ",", 
        RowBox[{
         RowBox[{"n", "*", 
          RowBox[{"(", 
           RowBox[{"n", "-", "1"}], ")"}]}], "+", 
         RowBox[{"2", "*", "n", "*", 
          RowBox[{"(", 
           RowBox[{"k1", "+", "k2", "+", "1", "-", 
            RowBox[{"2", "*", "ar", "*", "wang"}]}], ")"}]}], "-", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"2", "*", "ar", "*", "wang", "*", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{"2", "*", "k1"}], "+", "s", "+", "1"}], ")"}]}], "-", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{"k1", "+", "k2"}], ")"}], "*", 
            RowBox[{"(", 
             RowBox[{"k1", "+", "k2", "+", "1"}], ")"}]}]}], ")"}], "-", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{
            RowBox[{"ar", "^", "2"}], "*", 
            RowBox[{"wang", "^", "2"}]}], "+", 
           RowBox[{"s", "*", 
            RowBox[{"(", 
             RowBox[{"s", "+", "1"}], ")"}]}], "+", "Sep"}], ")"}]}]}], 
       "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"\[Alpha]ang", "=", 
      RowBox[{"Function", "[", 
       RowBox[{"n", ",", 
        RowBox[{
         RowBox[{"-", "2"}], "*", 
         RowBox[{"(", 
          RowBox[{"n", "+", "1"}], ")"}], "*", 
         RowBox[{"(", 
          RowBox[{"n", "+", 
           RowBox[{"2", "*", "k1"}], "+", "1"}], ")"}]}]}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Leaver31Ang", "[", "Sep_", "]"}], ":=", 
      RowBox[{"Module", "[", 
       RowBox[{
        RowBox[{"{", "Rn", "}"}], ",", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"For", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"n", "=", "NITMAX2"}], ";", 
             RowBox[{"Rn", "=", 
              RowBox[{"-", "1.0"}]}], ";"}], "}"}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"n", ">", "0"}], ",", "\[IndentingNewLine]", 
           RowBox[{"{", "\[IndentingNewLine]", 
            RowBox[{
             RowBox[{"Rn", "=", 
              FractionBox[
               RowBox[{"\[Gamma]ang", "[", "n", "]"}], 
               RowBox[{
                RowBox[{"\[Beta]ang", "[", "n", "]"}], "-", 
                RowBox[{
                 RowBox[{"\[Alpha]ang", "[", "n", "]"}], "*", "Rn"}]}]]}], 
             ";", 
             RowBox[{"n", "--"}], ";"}], "}"}]}], "\[IndentingNewLine]", 
          "]"}], ";", "Rn"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"Leaver33Ang", "[", "Sep_", "]"}], ":=", 
      RowBox[{
       FractionBox[
        RowBox[{"\[Beta]ang", "[", "0", "]"}], 
        RowBox[{"\[Alpha]ang", "[", "0", "]"}]], "-", 
       RowBox[{"Leaver31Ang", "[", "Sep", "]"}]}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Alm", "=", 
      RowBox[{
       RowBox[{
        RowBox[{"FindRoot", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"Leaver33Ang", "[", "Sep", "]"}], "\[Equal]", "0"}], ",", 
          RowBox[{"{", 
           RowBox[{"Sep", ",", "Alm"}], "}"}]}], "]"}], "[", 
        RowBox[{"[", "1", "]"}], "]"}], "[", 
       RowBox[{"[", "2", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"(*", 
      RowBox[{
      "**", "**", "**", "**", "**", "**", "**", "**", "**", "**", "**", "**", 
       "**", "**", "**", "**"}], "*****)"}], "\[IndentingNewLine]", 
     RowBox[{"q", "=", 
      RowBox[{"-", 
       RowBox[{"Sqrt", "[", 
        RowBox[{
         RowBox[{"mu", "^", "2"}], "-", 
         RowBox[{"w", "^", "2"}]}], "]"}]}]}], ";", 
     RowBox[{"c0", "=", 
      RowBox[{"1", "-", 
       RowBox[{"2", "*", "I", "*", "w"}], "-", 
       RowBox[{"2", "*", 
        RowBox[{"I", "/", "br"}], "*", 
        RowBox[{"(", 
         RowBox[{"w", "-", 
          RowBox[{"ar", "*", 
           RowBox[{"m", "/", "2"}]}]}], ")"}]}]}]}], ";", 
     RowBox[{"c1", "=", 
      RowBox[{
       RowBox[{"-", "4"}], "+", 
       RowBox[{"4", "*", "I", "*", 
        RowBox[{"(", 
         RowBox[{"w", "-", 
          RowBox[{"I", "*", "q", "*", 
           RowBox[{"(", 
            RowBox[{"1", "+", "br"}], ")"}]}]}], ")"}]}], "+", 
       RowBox[{"4", "*", 
        RowBox[{"I", "/", "br"}], "*", 
        RowBox[{"(", 
         RowBox[{"w", "-", 
          RowBox[{"ar", "*", 
           RowBox[{"m", "/", "2"}]}]}], ")"}]}], "-", 
       RowBox[{"2", "*", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"w", "^", "2"}], "+", 
           RowBox[{"q", "^", "2"}]}], ")"}], "/", "q"}]}]}]}], ";", 
     RowBox[{"c2", "=", 
      RowBox[{"3", "-", 
       RowBox[{"2", "*", "I", "*", "w"}], "-", 
       RowBox[{"2", "*", 
        RowBox[{"I", "/", "br"}], "*", 
        RowBox[{"(", 
         RowBox[{"w", "-", 
          RowBox[{"ar", "*", 
           RowBox[{"m", "/", "2"}]}]}], ")"}]}], "-", 
       RowBox[{"2", "*", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"q", "^", "2"}], "-", 
           RowBox[{"w", "^", "2"}]}], ")"}], "/", "q"}]}]}]}], ";", 
     RowBox[{"c3", "=", 
      RowBox[{
       RowBox[{"2", "*", "I", "*", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{"w", "-", 
            RowBox[{"I", "*", "q"}]}], ")"}], "^", "3"}], "/", "q"}]}], "+", 
       RowBox[{"2", "*", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"w", "-", 
           RowBox[{"I", "*", "q"}]}], ")"}], "^", "2"}], "*", "br"}], "+", 
       RowBox[{
        RowBox[{"q", "^", "2"}], "*", 
        RowBox[{"ar", "^", "2"}]}], "+", 
       RowBox[{"2", "*", "I", "*", "q", "*", "ar", "*", "m"}], "-", "Alm", 
       "-", "1", "-", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"w", "-", 
           RowBox[{"I", "*", "q"}]}], ")"}], "^", "2"}], "/", "q"}], "+", 
       RowBox[{"2", "*", "q", "*", "br"}], "+", 
       RowBox[{"2", "*", 
        RowBox[{"I", "/", "br"}], "*", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{"(", 
             RowBox[{"w", "-", 
              RowBox[{"I", "*", "q"}]}], ")"}], "^", "2"}], "/", "q"}], "+", 
          "1"}], ")"}], "*", 
        RowBox[{"(", 
         RowBox[{"w", "-", 
          RowBox[{"ar", "*", 
           RowBox[{"m", "/", "2"}]}]}], ")"}]}]}]}], ";", 
     RowBox[{"c4", "=", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"w", "-", 
           RowBox[{"I", "*", "q"}]}], ")"}], "^", "4"}], "/", 
        RowBox[{"q", "^", "2"}]}], "+", 
       RowBox[{"2", "*", "I", "*", "w", "*", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{"w", "-", 
            RowBox[{"I", "*", "q"}]}], ")"}], "^", "2"}], "/", "q"}]}], "-", 
       RowBox[{"2", "*", 
        RowBox[{"I", "/", "br"}], "*", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{"w", "-", 
            RowBox[{"I", "*", "q"}]}], ")"}], "^", "2"}], "/", "q"}], "*", 
        RowBox[{"(", 
         RowBox[{"w", "-", 
          RowBox[{"ar", "*", 
           RowBox[{"m", "/", "2"}]}]}], ")"}]}]}]}], ";", 
     RowBox[{"\[Gamma]", "=", 
      RowBox[{"Function", "[", 
       RowBox[{"n", ",", 
        RowBox[{
         SuperscriptBox["n", "2"], "+", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"c2", "-", "3"}], ")"}], "*", "n"}], "+", "c4", "+", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "c2"}], "+", "2"}], ")"}], "*", "0"}]}]}], "]"}]}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"\[Beta]", "=", 
      RowBox[{"Function", "[", 
       RowBox[{"n", ",", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "2"}], "*", 
          RowBox[{"n", "^", "2"}]}], "+", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"c1", "+", "2"}], ")"}], "*", "n"}], "+", "c3"}]}], 
       "]"}]}], ";", 
     RowBox[{"\[Alpha]", "=", 
      RowBox[{"Function", "[", 
       RowBox[{"n", ",", 
        RowBox[{
         SuperscriptBox["n", "2"], "+", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"c0", "+", "1"}], ")"}], "*", "n"}], "+", "c0"}]}], 
       "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Leaver31", "=", 
      RowBox[{"Module", "[", 
       RowBox[{
        RowBox[{"{", "Rn", "}"}], ",", 
        RowBox[{
         RowBox[{"For", "[", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{
             RowBox[{"n", "=", "NITMAX"}], ";", 
             RowBox[{"Rn", "=", 
              RowBox[{"-", "1.0"}]}], ";"}], "}"}], ",", 
           RowBox[{"n", ">", "0"}], ",", 
           RowBox[{"{", 
            RowBox[{
             RowBox[{"Rn", "=", 
              FractionBox[
               RowBox[{"\[Gamma]", "[", "n", "]"}], 
               RowBox[{
                RowBox[{"\[Beta]", "[", "n", "]"}], "-", 
                RowBox[{
                 RowBox[{"\[Alpha]", "[", "n", "]"}], "*", "Rn"}]}]]}], ";", 
             RowBox[{"n", "--"}], ";"}], "}"}]}], "\[IndentingNewLine]", 
          "]"}], ";", "Rn"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"Leaver33", "=", 
      RowBox[{
       FractionBox[
        RowBox[{"\[Beta]", "[", "0", "]"}], 
        RowBox[{"\[Alpha]", "[", "0", "]"}]], "-", "Leaver31"}]}]}], ")"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.623492745501553*^9, 3.623492894972369*^9}, {
   3.623492936012189*^9, 3.623492970821443*^9}, {3.623494618863717*^9, 
   3.623494629189802*^9}, 3.623494672737934*^9, {3.6234955918782663`*^9, 
   3.62349559868524*^9}, {3.6235061790173693`*^9, 3.623506209885455*^9}, {
   3.623506245576393*^9, 3.623506259700528*^9}, {3.6235063098163967`*^9, 
   3.6235063821158323`*^9}, {3.623506448203854*^9, 3.623506482985901*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"LEAVER", "[", 
  RowBox[{
   FractionBox["18", "100"], ",", 
   FractionBox["9", "10"], ",", 
   FractionBox["2", "10"], ",", "1", ",", "1", ",", "100", ",", "100"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.6234929839331093`*^9, 3.623493009692733*^9}, {
  3.6235064606029367`*^9, 3.623506460981698*^9}, {3.623650706298067*^9, 
  3.623650712302579*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "1.8577272036815997`"}], "+", 
  RowBox[{"1.1695100360399597`", " ", "\[ImaginaryI]"}]}]], "Output",
 CellChangeTimes->{{3.623493004584443*^9, 3.623493009995138*^9}, 
   3.6234946326765757`*^9, 3.623506391987241*^9, 3.6235064614391737`*^9, 
   3.6236507140823402`*^9, 3.623651065922968*^9, 3.629785721365534*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"fun", "[", 
    RowBox[{"x_", "?", "NumberQ"}], "]"}], ":=", 
   RowBox[{"LEAVER", "[", 
    RowBox[{
    "x", ",", "0.9", ",", "0.2", ",", "1", ",", "1", ",", "200", ",", "100"}],
     "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"FindRoot", "[", 
  RowBox[{
   RowBox[{"fun", "[", "x", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{"0.199", "+", 
      RowBox[{
       SuperscriptBox["10", 
        RowBox[{"-", "8"}]], "I"}]}]}], "}"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.6234930177608433`*^9, 3.623493072304367*^9}, {
  3.623506408847069*^9, 3.6235064176925097`*^9}, {3.623506464188047*^9, 
  3.6235064645829573`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"x", "\[Rule]", 
   RowBox[{"0.19895873371535125`", "\[VeryThinSpace]", "+", 
    RowBox[{"1.5549689332842948`*^-9", " ", "\[ImaginaryI]"}]}]}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.623493063328356*^9, 3.623493072753619*^9}, 
   3.623494635002653*^9, {3.623506410060321*^9, 3.6235064278099337`*^9}, 
   3.6235064679127607`*^9, 3.623651069005925*^9, 3.629785726690501*^9}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1366, 725},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[567, 22, 1092, 20, 73, "Subtitle"],
Cell[1662, 44, 179, 4, 31, "Text"],
Cell[1844, 50, 12663, 355, 1069, "Input"],
Cell[CellGroupData[{
Cell[14532, 409, 378, 9, 62, "Input"],
Cell[14913, 420, 351, 6, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15301, 431, 701, 20, 58, "Input"],
Cell[16005, 453, 416, 8, 38, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
