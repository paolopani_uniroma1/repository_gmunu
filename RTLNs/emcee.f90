program emcee

  use mpi

  implicit none

  integer(4) :: ierr, ncore, myrank, myid, color, ensemble, i, j
!  integer, parameter::n = 10000, color
!  integer, dimension (n)::a, b
!  integer :: src,dest
!  integer :: status(MPI_STATUS_SIZE)
!  ! dont forget the status param in recv integer status (MPI_STATUS_SIZE)
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, ncore, ierr)
  call mpi_comm_rank(MPI_COMM_WORLD, myrank, ierr)
  
  if (myrank < ncore/2) then
     color = 1
  else
     color = 2
  end if

  call mpi_comm_split(MPI_COMM_WORLD, color, myrank, ensemble, ierr)
  call mpi_comm_rank(ensemble, myid, ierr)

!  if (myrank < size-1) then
!     dest=myrank+1
!  else
!    dest=0
!  endif

!  if (myrank >0) then
!    src=myrank-1 
!  else
!    src=size-1 
!  endif

!  a = myrank

!  call mpi_send (a, n, MPI_INTEGER, dest, 10, MPI_COMM_WORLD, ierr)
!  call mpi_recv (b, n, MPI_INTEGER, src, 10, MPI_COMM_WORLD, status, ierr)
  print*, ncore
  do
  j =1
  end do
  write (*, *) "I am task ", myrank, "but", myid
  call mpi_comm_free(ensemble, ierr)
  call mpi_finalize (ierr)

end program
