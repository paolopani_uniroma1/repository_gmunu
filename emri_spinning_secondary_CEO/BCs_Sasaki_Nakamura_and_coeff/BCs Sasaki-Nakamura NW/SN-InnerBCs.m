(* Created with the Wolfram Language : www.wolfram.com *)
c[k] -> ((-12*a*m*\[Kappa]^2*\[Omega] + 12*a^2*\[Kappa]^2*\[Omega]^2 + 
     2*\[Lambda]*\[Omega]^2 + \[Lambda]^2*\[Omega]^2 + 12*a*m*\[Omega]^3 - 
     (12*I)*M*\[Omega]^3 - 12*a^2*\[Omega]^4 - 
     \[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]))*c[-14 + k])/
   (-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + (144*I)*a^11*k*m*rh - 
    (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 576*a^10*k*M*rh - 
    216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 648*a^10*k*rh^2 + 
    360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 144*a^10*k*m^2*rh^2 - 
    144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  (((-8*I)*a*m*\[Kappa]^2*\[Lambda] - (24*I)*a*(-13 + k)*m*\[Kappa]*
      \[Omega] - (24*I)*a^2*\[Kappa]^2*\[Omega] - 168*a*m*rh*\[Kappa]^2*
      \[Omega] + (8*I)*a^2*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (24*I)*a^2*(-13 + k)*\[Kappa]*\[Omega]^2 + 168*a^2*rh*\[Kappa]^2*
      \[Omega]^2 + (8*I)*a*m*\[Lambda]*\[Omega]^2 + 
     28*rh*\[Lambda]*\[Omega]^2 + 14*rh*\[Lambda]^2*\[Omega]^2 + 
     (24*I)*a^2*\[Omega]^3 + 168*a*m*rh*\[Omega]^3 - 
     (168*I)*M*rh*\[Omega]^3 - (8*I)*a^2*\[Lambda]*\[Omega]^3 - 
     168*a^2*rh*\[Omega]^4 - 14*rh*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 2*(-13 + k)*\[Kappa]*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]))*c[-13 + k])/
   (-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + (144*I)*a^11*k*m*rh - 
    (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 576*a^10*k*M*rh - 
    216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 648*a^10*k*rh^2 + 
    360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 144*a^10*k*m^2*rh^2 - 
    144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  ((-12*a^2*\[Kappa]^2 + 24*a^2*m^2*\[Kappa]^2 - (24*I)*a*m*M*\[Kappa]^2 - 
     4*\[Lambda] + 8*a*m*\[Kappa]*\[Lambda] + 16*a*(-12 + k)*m*\[Kappa]*
      \[Lambda] - 2*a^2*\[Kappa]^2*\[Lambda] - (104*I)*a*m*rh*\[Kappa]^2*
      \[Lambda] - 4*\[Lambda]^2 - a^2*\[Kappa]^2*\[Lambda]^2 - \[Lambda]^3 - 
     24*a*m*\[Omega] - 12*a*(13 - k)*(-12 + k)*m*\[Omega] + 
     (24*I)*M*\[Omega] + 24*a^2*\[Kappa]*\[Omega] + 
     48*a^2*(-12 + k)*\[Kappa]*\[Omega] + (48*I)*a*(-12 + k)*m*M*\[Kappa]*
      \[Omega] - (336*I)*a*(-12 + k)*m*rh*\[Kappa]*\[Omega] - 
     96*a^3*m*\[Kappa]^2*\[Omega] + (36*I)*a^2*M*\[Kappa]^2*\[Omega] - 
     (312*I)*a^2*rh*\[Kappa]^2*\[Omega] - 1092*a*m*rh^2*\[Kappa]^2*\[Omega] - 
     24*a*m*\[Lambda]*\[Omega] + (12*I)*M*\[Lambda]*\[Omega] - 
     8*a^2*\[Kappa]*\[Lambda]*\[Omega] - 16*a^2*(-12 + k)*\[Kappa]*\[Lambda]*
      \[Omega] + (104*I)*a^2*rh*\[Kappa]^2*\[Lambda]*\[Omega] - 
     2*a*m*\[Lambda]^2*\[Omega] + 12*a^2*\[Omega]^2 + 
     12*a^2*(13 - k)*(-12 + k)*\[Omega]^2 - 48*a^2*m^2*\[Omega]^2 + 
     (48*I)*a*m*M*\[Omega]^2 - (48*I)*a^2*(-12 + k)*M*\[Kappa]*\[Omega]^2 + 
     (336*I)*a^2*(-12 + k)*rh*\[Kappa]*\[Omega]^2 + 
     72*a^4*\[Kappa]^2*\[Omega]^2 + 1092*a^2*rh^2*\[Kappa]^2*\[Omega]^2 + 
     28*a^2*\[Lambda]*\[Omega]^2 + (104*I)*a*m*rh*\[Lambda]*\[Omega]^2 + 
     182*rh^2*\[Lambda]*\[Omega]^2 + 4*a^2*\[Lambda]^2*\[Omega]^2 + 
     91*rh^2*\[Lambda]^2*\[Omega]^2 + 120*a^3*m*\[Omega]^3 - 
     (72*I)*a^2*M*\[Omega]^3 + (312*I)*a^2*rh*\[Omega]^3 + 
     1092*a*m*rh^2*\[Omega]^3 - (1092*I)*M*rh^2*\[Omega]^3 - 
     (104*I)*a^2*rh*\[Lambda]*\[Omega]^3 - 72*a^4*\[Omega]^4 - 
     1092*a^2*rh^2*\[Omega]^4 - (13 - k)*(-12 + k)*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3*a^2*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     91*rh^2*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     4*(-12 + k)*M*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 28*(-12 + k)*rh*\[Kappa]*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]))*c[-12 + k])/
   (-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + (144*I)*a^11*k*m*rh - 
    (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 576*a^10*k*M*rh - 
    216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 648*a^10*k*rh^2 + 
    360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 144*a^10*k*m^2*rh^2 - 
    144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  (((-24*I)*a^2*\[Kappa] - (24*I)*a^2*(-11 + k)*\[Kappa] + 
     (48*I)*a^2*m^2*\[Kappa] + (48*I)*a^2*(-11 + k)*m^2*\[Kappa] + 
     48*a*m*M*\[Kappa] + 48*a*(-11 + k)*m*M*\[Kappa] + 
     (24*I)*a^3*m*\[Kappa]^2 + 24*a^2*M*\[Kappa]^2 - 144*a^2*rh*\[Kappa]^2 + 
     288*a^2*m^2*rh*\[Kappa]^2 - (288*I)*a*m*M*rh*\[Kappa]^2 - 
     (16*I)*a*m*\[Lambda] + (8*I)*a*(-11 + k)*m*\[Lambda] - 
     (8*I)*a*(12 - k)*(-11 + k)*m*\[Lambda] + 20*M*\[Lambda] - 
     48*rh*\[Lambda] - (8*I)*a^2*(-11 + k)*\[Kappa]*\[Lambda] - 
     16*a*m*M*\[Kappa]*\[Lambda] - 32*a*(-11 + k)*m*M*\[Kappa]*\[Lambda] + 
     96*a*m*rh*\[Kappa]*\[Lambda] + 208*a*(-11 + k)*m*rh*\[Kappa]*\[Lambda] - 
     (32*I)*a^3*m*\[Kappa]^2*\[Lambda] - 24*a^2*rh*\[Kappa]^2*\[Lambda] - 
     (624*I)*a*m*rh^2*\[Kappa]^2*\[Lambda] - (8*I)*a*m*\[Lambda]^2 + 
     14*M*\[Lambda]^2 - 48*rh*\[Lambda]^2 - (4*I)*a^2*(-11 + k)*\[Kappa]*
      \[Lambda]^2 - 12*a^2*rh*\[Kappa]^2*\[Lambda]^2 + 2*M*\[Lambda]^3 - 
     12*rh*\[Lambda]^3 - (24*I)*a^2*\[Omega] + (24*I)*a^2*(-11 + k)*
      \[Omega] - (24*I)*a^2*(12 - k)*(-11 + k)*\[Omega] - 
     (48*I)*a^2*m^2*\[Omega] + 72*a*m*M*\[Omega] + 
     24*a*(-11 + k)*m*M*\[Omega] + 48*a*(12 - k)*(-11 + k)*m*M*\[Omega] - 
     (120*I)*M^2*\[Omega] - 288*a*m*rh*\[Omega] - 168*a*(12 - k)*(-11 + k)*m*
      rh*\[Omega] + (288*I)*M*rh*\[Omega] - (96*I)*a^3*m*\[Kappa]*\[Omega] - 
     (192*I)*a^3*(-11 + k)*m*\[Kappa]*\[Omega] - 96*a^2*M*\[Kappa]*\[Omega] - 
     192*a^2*(-11 + k)*M*\[Kappa]*\[Omega] + 264*a^2*rh*\[Kappa]*\[Omega] + 
     624*a^2*(-11 + k)*rh*\[Kappa]*\[Omega] + (624*I)*a*(-11 + k)*m*M*rh*
      \[Kappa]*\[Omega] - (2184*I)*a*(-11 + k)*m*rh^2*\[Kappa]*\[Omega] - 
     (120*I)*a^4*\[Kappa]^2*\[Omega] - 1152*a^3*m*rh*\[Kappa]^2*\[Omega] + 
     (432*I)*a^2*M*rh*\[Kappa]^2*\[Omega] - (1872*I)*a^2*rh^2*\[Kappa]^2*
      \[Omega] - 4368*a*m*rh^3*\[Kappa]^2*\[Omega] - 
     8*a^2*rh*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     (8*I)*a^2*\[Lambda]*\[Omega] - (8*I)*a^2*(-11 + k)*\[Lambda]*\[Omega] + 
     (8*I)*a^2*(12 - k)*(-11 + k)*\[Lambda]*\[Omega] - 
     (16*I)*a^2*m^2*\[Lambda]*\[Omega] + 40*a*m*M*\[Lambda]*\[Omega] - 
     (24*I)*M^2*\[Lambda]*\[Omega] - 288*a*m*rh*\[Lambda]*\[Omega] + 
     (144*I)*M*rh*\[Lambda]*\[Omega] + 16*a^2*M*\[Kappa]*\[Lambda]*\[Omega] + 
     32*a^2*(-11 + k)*M*\[Kappa]*\[Lambda]*\[Omega] - 
     88*a^2*rh*\[Kappa]*\[Lambda]*\[Omega] - 208*a^2*(-11 + k)*rh*\[Kappa]*
      \[Lambda]*\[Omega] + (32*I)*a^4*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (624*I)*a^2*rh^2*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (8*I)*a^2*\[Lambda]^2*\[Omega] - 24*a*m*rh*\[Lambda]^2*\[Omega] + 
     (24*I)*a^3*m*\[Omega]^2 - 48*a^2*M*\[Omega]^2 - 
     24*a^2*(-11 + k)*M*\[Omega]^2 - 48*a^2*(12 - k)*(-11 + k)*M*\[Omega]^2 + 
     144*a^2*rh*\[Omega]^2 + 168*a^2*(12 - k)*(-11 + k)*rh*\[Omega]^2 - 
     576*a^2*m^2*rh*\[Omega]^2 + (576*I)*a*m*M*rh*\[Omega]^2 + 
     (48*I)*a^4*\[Kappa]*\[Omega]^2 + (144*I)*a^4*(-11 + k)*\[Kappa]*
      \[Omega]^2 - (624*I)*a^2*(-11 + k)*M*rh*\[Kappa]*\[Omega]^2 + 
     (2184*I)*a^2*(-11 + k)*rh^2*\[Kappa]*\[Omega]^2 + 
     864*a^4*rh*\[Kappa]^2*\[Omega]^2 + 4368*a^2*rh^3*\[Kappa]^2*\[Omega]^2 + 
     (48*I)*a^3*m*\[Lambda]*\[Omega]^2 - 40*a^2*M*\[Lambda]*\[Omega]^2 + 
     336*a^2*rh*\[Lambda]*\[Omega]^2 + (624*I)*a*m*rh^2*\[Lambda]*
      \[Omega]^2 + 728*rh^3*\[Lambda]*\[Omega]^2 + 48*a^2*rh*\[Lambda]^2*
      \[Omega]^2 + 364*rh^3*\[Lambda]^2*\[Omega]^2 + (72*I)*a^4*\[Omega]^3 + 
     1440*a^3*m*rh*\[Omega]^3 - (864*I)*a^2*M*rh*\[Omega]^3 + 
     (1872*I)*a^2*rh^2*\[Omega]^3 + 4368*a*m*rh^3*\[Omega]^3 - 
     (4368*I)*M*rh^3*\[Omega]^3 - (32*I)*a^4*\[Lambda]*\[Omega]^3 - 
     (624*I)*a^2*rh^2*\[Lambda]*\[Omega]^3 - 864*a^4*rh*\[Omega]^4 - 
     4368*a^2*rh^3*\[Omega]^4 + 4*(12 - k)*(-11 + k)*M*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     14*(12 - k)*(-11 + k)*rh*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 36*a^2*rh*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 364*rh^3*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - (2*I)*(-11 + k)*M*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - 4*a^2*(-11 + k)*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
     52*(-11 + k)*M*rh*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 182*(-11 + k)*rh^2*\[Kappa]*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]))*c[-11 + k])/
   (-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + (144*I)*a^11*k*m*rh - 
    (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 576*a^10*k*M*rh - 
    216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 648*a^10*k*rh^2 + 
    360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 144*a^10*k*m^2*rh^2 - 
    144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  ((24*a^2*(-10 + k) - 12*a^2*(11 - k)*(-10 + k) - 48*a^2*(-10 + k)*m^2 + 
     24*a^2*(11 - k)*(-10 + k)*m^2 + (48*I)*a*(-10 + k)*m*M - 
     (24*I)*a*(11 - k)*(-10 + k)*m*M - 72*a^3*m*\[Kappa] - 
     48*a^3*(-10 + k)*m*\[Kappa] + (120*I)*a^2*M*\[Kappa] + 
     (96*I)*a^2*(-10 + k)*M*\[Kappa] - (96*I)*a^2*m^2*M*\[Kappa] - 
     (96*I)*a^2*(-10 + k)*m^2*M*\[Kappa] - 96*a*m*M^2*\[Kappa] - 
     96*a*(-10 + k)*m*M^2*\[Kappa] - (264*I)*a^2*rh*\[Kappa] - 
     (288*I)*a^2*(-10 + k)*rh*\[Kappa] + (528*I)*a^2*m^2*rh*\[Kappa] + 
     (576*I)*a^2*(-10 + k)*m^2*rh*\[Kappa] + 528*a*m*M*rh*\[Kappa] + 
     528*a*(-10 + k)*m*M*rh*\[Kappa] - 60*a^4*\[Kappa]^2 + 
     96*a^4*m^2*\[Kappa]^2 - (96*I)*a^3*m*M*\[Kappa]^2 + 
     (264*I)*a^3*m*rh*\[Kappa]^2 + 264*a^2*M*rh*\[Kappa]^2 - 
     792*a^2*rh^2*\[Kappa]^2 + 1584*a^2*m^2*rh^2*\[Kappa]^2 - 
     (1584*I)*a*m*M*rh^2*\[Kappa]^2 - 30*a^2*\[Lambda] + 
     34*a^2*m^2*\[Lambda] + (56*I)*a*m*M*\[Lambda] - 
     (16*I)*a*(-10 + k)*m*M*\[Lambda] + (32*I)*a*(11 - k)*(-10 + k)*m*M*
      \[Lambda] - 24*M^2*\[Lambda] - (176*I)*a*m*rh*\[Lambda] + 
     (96*I)*a*(-10 + k)*m*rh*\[Lambda] - (104*I)*a*(11 - k)*(-10 + k)*m*rh*
      \[Lambda] + 220*M*rh*\[Lambda] - 264*rh^2*\[Lambda] + 
     32*a^3*m*\[Kappa]*\[Lambda] + 64*a^3*(-10 + k)*m*\[Kappa]*\[Lambda] + 
     (16*I)*a^2*(-10 + k)*M*\[Kappa]*\[Lambda] - (96*I)*a^2*(-10 + k)*rh*
      \[Kappa]*\[Lambda] - 176*a*m*M*rh*\[Kappa]*\[Lambda] - 
     384*a*(-10 + k)*m*M*rh*\[Kappa]*\[Lambda] + 528*a*m*rh^2*\[Kappa]*
      \[Lambda] + 1248*a*(-10 + k)*m*rh^2*\[Kappa]*\[Lambda] - 
     6*a^4*\[Kappa]^2*\[Lambda] - (352*I)*a^3*m*rh*\[Kappa]^2*\[Lambda] - 
     132*a^2*rh^2*\[Kappa]^2*\[Lambda] - (2288*I)*a*m*rh^3*\[Kappa]^2*
      \[Lambda] - 21*a^2*\[Lambda]^2 + a^2*m^2*\[Lambda]^2 + 
     (16*I)*a*m*M*\[Lambda]^2 - 12*M^2*\[Lambda]^2 - 
     (88*I)*a*m*rh*\[Lambda]^2 + 154*M*rh*\[Lambda]^2 - 
     264*rh^2*\[Lambda]^2 + (8*I)*a^2*(-10 + k)*M*\[Kappa]*\[Lambda]^2 - 
     (48*I)*a^2*(-10 + k)*rh*\[Kappa]*\[Lambda]^2 - 
     3*a^4*\[Kappa]^2*\[Lambda]^2 - 66*a^2*rh^2*\[Kappa]^2*\[Lambda]^2 - 
     3*a^2*\[Lambda]^3 + 22*M*rh*\[Lambda]^3 - 66*rh^2*\[Lambda]^3 - 
     108*a^3*m*\[Omega] + 96*a^3*(-10 + k)*m*\[Omega] - 
     96*a^3*(11 - k)*(-10 + k)*m*\[Omega] + 60*a^3*m^3*\[Omega] + 
     (300*I)*a^2*M*\[Omega] - (96*I)*a^2*(-10 + k)*M*\[Omega] + 
     (120*I)*a^2*(11 - k)*(-10 + k)*M*\[Omega] + (36*I)*a^2*m^2*M*\[Omega] - 
     48*a*m*M^2*\[Omega] - 48*a*(-10 + k)*m*M^2*\[Omega] - 
     48*a*(11 - k)*(-10 + k)*m*M^2*\[Omega] + (144*I)*M^3*\[Omega] - 
     (264*I)*a^2*rh*\[Omega] + (264*I)*a^2*(-10 + k)*rh*\[Omega] - 
     (312*I)*a^2*(11 - k)*(-10 + k)*rh*\[Omega] - (528*I)*a^2*m^2*rh*
      \[Omega] + 792*a*m*M*rh*\[Omega] + 264*a*(-10 + k)*m*M*rh*\[Omega] + 
     624*a*(11 - k)*(-10 + k)*m*M*rh*\[Omega] - (1320*I)*M^2*rh*\[Omega] - 
     1584*a*m*rh^2*\[Omega] - 1092*a*(11 - k)*(-10 + k)*m*rh^2*\[Omega] + 
     (1584*I)*M*rh^2*\[Omega] + 168*a^4*\[Kappa]*\[Omega] + 
     240*a^4*(-10 + k)*\[Kappa]*\[Omega] + (192*I)*a^3*m*M*\[Kappa]*
      \[Omega] + (336*I)*a^3*(-10 + k)*m*M*\[Kappa]*\[Omega] + 
     96*a^2*M^2*\[Kappa]*\[Omega] + 192*a^2*(-10 + k)*M^2*\[Kappa]*\[Omega] - 
     (1056*I)*a^3*m*rh*\[Kappa]*\[Omega] - (2304*I)*a^3*(-10 + k)*m*rh*
      \[Kappa]*\[Omega] - 960*a^2*M*rh*\[Kappa]*\[Omega] - 
     2304*a^2*(-10 + k)*M*rh*\[Kappa]*\[Omega] + 1320*a^2*rh^2*\[Kappa]*
      \[Omega] + 3744*a^2*(-10 + k)*rh^2*\[Kappa]*\[Omega] + 
     (3744*I)*a*(-10 + k)*m*M*rh^2*\[Kappa]*\[Omega] - 
     (8736*I)*a*(-10 + k)*m*rh^3*\[Kappa]*\[Omega] - 
     264*a^5*m*\[Kappa]^2*\[Omega] + (132*I)*a^4*M*\[Kappa]^2*\[Omega] - 
     (1224*I)*a^4*rh*\[Kappa]^2*\[Omega] - 6336*a^3*m*rh^2*\[Kappa]^2*
      \[Omega] + (2376*I)*a^2*M*rh^2*\[Kappa]^2*\[Omega] - 
     (6864*I)*a^2*rh^3*\[Kappa]^2*\[Omega] - 12012*a*m*rh^4*\[Kappa]^2*
      \[Omega] + 16*a^2*M*rh*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (16*I)*a^4*rh*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] - 
     (8*I)*a^2*(-10 + k)*rh*(-3 + \[Lambda])*\[Omega] - 
     88*a^2*rh^2*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     136*a^3*m*\[Lambda]*\[Omega] + (28*I)*a^2*M*\[Lambda]*\[Omega] + 
     (16*I)*a^2*(-10 + k)*M*\[Lambda]*\[Omega] - (32*I)*a^2*(11 - k)*
      (-10 + k)*M*\[Lambda]*\[Omega] - (88*I)*a^2*rh*\[Lambda]*\[Omega] - 
     (88*I)*a^2*(-10 + k)*rh*\[Lambda]*\[Omega] + (104*I)*a^2*(11 - k)*
      (-10 + k)*rh*\[Lambda]*\[Omega] - (176*I)*a^2*m^2*rh*\[Lambda]*
      \[Omega] + 440*a*m*M*rh*\[Lambda]*\[Omega] - (264*I)*M^2*rh*\[Lambda]*
      \[Omega] - 1584*a*m*rh^2*\[Lambda]*\[Omega] + 
     (792*I)*M*rh^2*\[Lambda]*\[Omega] - 32*a^4*\[Kappa]*\[Lambda]*\[Omega] - 
     64*a^4*(-10 + k)*\[Kappa]*\[Lambda]*\[Omega] + 
     160*a^2*M*rh*\[Kappa]*\[Lambda]*\[Omega] + 384*a^2*(-10 + k)*M*rh*
      \[Kappa]*\[Lambda]*\[Omega] - 440*a^2*rh^2*\[Kappa]*\[Lambda]*
      \[Omega] - 1248*a^2*(-10 + k)*rh^2*\[Kappa]*\[Lambda]*\[Omega] + 
     (336*I)*a^4*rh*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (2288*I)*a^2*rh^3*\[Kappa]^2*\[Lambda]*\[Omega] - 
     6*a^3*m*\[Lambda]^2*\[Omega] - (16*I)*a^2*M*\[Lambda]^2*\[Omega] + 
     (88*I)*a^2*rh*\[Lambda]^2*\[Omega] - 132*a*m*rh^2*\[Lambda]^2*\[Omega] + 
     72*a^4*\[Omega]^2 - 48*a^4*(-10 + k)*\[Omega]^2 + 
     72*a^4*(11 - k)*(-10 + k)*\[Omega]^2 - 276*a^4*m^2*\[Omega]^2 + 
     (24*I)*a^3*m*M*\[Omega]^2 + 48*a^2*M^2*\[Omega]^2 + 
     48*a^2*(-10 + k)*M^2*\[Omega]^2 + 48*a^2*(11 - k)*(-10 + k)*M^2*
      \[Omega]^2 + (264*I)*a^3*m*rh*\[Omega]^2 - 528*a^2*M*rh*\[Omega]^2 - 
     288*a^2*(-10 + k)*M*rh*\[Omega]^2 - 624*a^2*(11 - k)*(-10 + k)*M*rh*
      \[Omega]^2 + 792*a^2*rh^2*\[Omega]^2 + 1092*a^2*(11 - k)*(-10 + k)*rh^2*
      \[Omega]^2 - 3168*a^2*m^2*rh^2*\[Omega]^2 + (3168*I)*a*m*M*rh^2*
      \[Omega]^2 - (96*I)*a^4*M*\[Kappa]*\[Omega]^2 - 
     (240*I)*a^4*(-10 + k)*M*\[Kappa]*\[Omega]^2 + (528*I)*a^4*rh*\[Kappa]*
      \[Omega]^2 + (1728*I)*a^4*(-10 + k)*rh*\[Kappa]*\[Omega]^2 - 
     (3744*I)*a^2*(-10 + k)*M*rh^2*\[Kappa]*\[Omega]^2 + 
     (8736*I)*a^2*(-10 + k)*rh^3*\[Kappa]*\[Omega]^2 + 
     168*a^6*\[Kappa]^2*\[Omega]^2 + 4752*a^4*rh^2*\[Kappa]^2*\[Omega]^2 + 
     12012*a^2*rh^4*\[Kappa]^2*\[Omega]^2 + 104*a^4*\[Lambda]*\[Omega]^2 + 
     (528*I)*a^3*m*rh*\[Lambda]*\[Omega]^2 - 440*a^2*M*rh*\[Lambda]*
      \[Omega]^2 + 1848*a^2*rh^2*\[Lambda]*\[Omega]^2 + 
     (2288*I)*a*m*rh^3*\[Lambda]*\[Omega]^2 + 2002*rh^4*\[Lambda]*
      \[Omega]^2 + 6*a^4*\[Lambda]^2*\[Omega]^2 + 264*a^2*rh^2*\[Lambda]^2*
      \[Omega]^2 + 1001*rh^4*\[Lambda]^2*\[Omega]^2 + 384*a^5*m*\[Omega]^3 - 
     (72*I)*a^4*M*\[Omega]^3 + (792*I)*a^4*rh*\[Omega]^3 + 
     7920*a^3*m*rh^2*\[Omega]^3 - (4752*I)*a^2*M*rh^2*\[Omega]^3 + 
     (6864*I)*a^2*rh^3*\[Omega]^3 + 12012*a*m*rh^4*\[Omega]^3 - 
     (12012*I)*M*rh^4*\[Omega]^3 - (352*I)*a^4*rh*\[Lambda]*\[Omega]^3 - 
     (2288*I)*a^2*rh^3*\[Lambda]*\[Omega]^3 - 168*a^6*\[Omega]^4 - 
     4752*a^4*rh^2*\[Omega]^4 - 12012*a^2*rh^4*\[Omega]^4 + 
     24*a*(-10 + k)*m*M*rh*(2*\[Kappa] + \[Omega]) - 
     4*a^2*(11 - k)*(-10 + k)*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 4*(11 - k)*(-10 + k)*M^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     52*(11 - k)*(-10 + k)*M*rh*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 91*(11 - k)*(-10 + k)*rh^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3*a^4*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     198*a^2*rh^2*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1001*rh^4*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + (4*I)*(-10 + k)*M^2*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - (24*I)*(-10 + k)*M*rh*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
     4*a^2*(-10 + k)*M*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 48*a^2*(-10 + k)*rh*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
     312*(-10 + k)*M*rh^2*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 728*(-10 + k)*rh^3*\[Kappa]*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]))*c[-10 + k])/
   (-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + (144*I)*a^11*k*m*rh - 
    (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 576*a^10*k*M*rh - 
    216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 648*a^10*k*rh^2 + 
    360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 144*a^10*k*m^2*rh^2 - 
    144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  (((-48*I)*a^3*m - (72*I)*a^3*(-9 + k)*m + (24*I)*a^3*(10 - k)*(-9 + k)*m + 
     (48*I)*a^3*m^3 - 144*a^2*(-9 + k)*M + 72*a^2*(10 - k)*(-9 + k)*M + 
     144*a^2*(-9 + k)*m^2*M - 96*a^2*(10 - k)*(-9 + k)*m^2*M + 
     (48*I)*a*m*M^2 - (144*I)*a*(-9 + k)*m*M^2 + (96*I)*a*(10 - k)*(-9 + k)*m*
      M^2 + 264*a^2*(-9 + k)*rh - 144*a^2*(10 - k)*(-9 + k)*rh - 
     528*a^2*(-9 + k)*m^2*rh + 288*a^2*(10 - k)*(-9 + k)*m^2*rh + 
     (528*I)*a*(-9 + k)*m*M*rh - (288*I)*a*(10 - k)*(-9 + k)*m*M*rh - 
     (144*I)*a^4*\[Kappa] - (120*I)*a^4*(-9 + k)*\[Kappa] + 
     (192*I)*a^4*m^2*\[Kappa] + (192*I)*a^4*(-9 + k)*m^2*\[Kappa] + 
     336*a^3*m*M*\[Kappa] + 288*a^3*(-9 + k)*m*M*\[Kappa] - 
     (144*I)*a^2*M^2*\[Kappa] - (96*I)*a^2*(-9 + k)*M^2*\[Kappa] - 
     720*a^3*m*rh*\[Kappa] - 528*a^3*(-9 + k)*m*rh*\[Kappa] + 
     (1200*I)*a^2*M*rh*\[Kappa] + (1056*I)*a^2*(-9 + k)*M*rh*\[Kappa] - 
     (960*I)*a^2*m^2*M*rh*\[Kappa] - (1056*I)*a^2*(-9 + k)*m^2*M*rh*
      \[Kappa] - 960*a*m*M^2*rh*\[Kappa] - 960*a*(-9 + k)*m*M^2*rh*\[Kappa] - 
     (1320*I)*a^2*rh^2*\[Kappa] - (1584*I)*a^2*(-9 + k)*rh^2*\[Kappa] + 
     (2640*I)*a^2*m^2*rh^2*\[Kappa] + (3168*I)*a^2*(-9 + k)*m^2*rh^2*
      \[Kappa] + 2640*a*m*M*rh^2*\[Kappa] + 2640*a*(-9 + k)*m*M*rh^2*
      \[Kappa] + (96*I)*a^5*m*\[Kappa]^2 + 96*a^4*M*\[Kappa]^2 - 
     600*a^4*rh*\[Kappa]^2 + 960*a^4*m^2*rh*\[Kappa]^2 - 
     (936*I)*a^3*m*M*rh*\[Kappa]^2 + (1320*I)*a^3*m*rh^2*\[Kappa]^2 + 
     1320*a^2*M*rh^2*\[Kappa]^2 - 2640*a^2*rh^3*\[Kappa]^2 + 
     5280*a^2*m^2*rh^3*\[Kappa]^2 - (5280*I)*a*m*M*rh^3*\[Kappa]^2 - 
     (88*I)*a^3*m*\[Lambda] + (32*I)*a^3*(-9 + k)*m*\[Lambda] - 
     (32*I)*a^3*(10 - k)*(-9 + k)*m*\[Lambda] + (8*I)*a^3*m^3*\[Lambda] + 
     104*a^2*M*\[Lambda] - 4*a^2*(-9 + k)*M*\[Lambda] - 
     64*a^2*m^2*M*\[Lambda] - (48*I)*a*m*M^2*\[Lambda] - 
     (32*I)*a*(10 - k)*(-9 + k)*m*M^2*\[Lambda] - 300*a^2*rh*\[Lambda] + 
     340*a^2*m^2*rh*\[Lambda] + (560*I)*a*m*M*rh*\[Lambda] - 
     (176*I)*a*(-9 + k)*m*M*rh*\[Lambda] + (384*I)*a*(10 - k)*(-9 + k)*m*M*rh*
      \[Lambda] - 240*M^2*rh*\[Lambda] - (880*I)*a*m*rh^2*\[Lambda] + 
     (528*I)*a*(-9 + k)*m*rh^2*\[Lambda] - (624*I)*a*(10 - k)*(-9 + k)*m*rh^2*
      \[Lambda] + 1100*M*rh^2*\[Lambda] - 880*rh^3*\[Lambda] - 
     (20*I)*a^4*(-9 + k)*\[Kappa]*\[Lambda] - 48*a^3*m*M*\[Kappa]*\[Lambda] - 
     96*a^3*(-9 + k)*m*M*\[Kappa]*\[Lambda] + 320*a^3*m*rh*\[Kappa]*
      \[Lambda] + 704*a^3*(-9 + k)*m*rh*\[Kappa]*\[Lambda] + 
     (176*I)*a^2*(-9 + k)*M*rh*\[Kappa]*\[Lambda] - 
     (528*I)*a^2*(-9 + k)*rh^2*\[Kappa]*\[Lambda] - 
     880*a*m*M*rh^2*\[Kappa]*\[Lambda] - 2112*a*(-9 + k)*m*M*rh^2*\[Kappa]*
      \[Lambda] + 1760*a*m*rh^3*\[Kappa]*\[Lambda] + 
     4576*a*(-9 + k)*m*rh^3*\[Kappa]*\[Lambda] - (48*I)*a^5*m*\[Kappa]^2*
      \[Lambda] - 60*a^4*rh*\[Kappa]^2*\[Lambda] - 
     (1760*I)*a^3*m*rh^2*\[Kappa]^2*\[Lambda] - 440*a^2*rh^3*\[Kappa]^2*
      \[Lambda] - (5720*I)*a*m*rh^4*\[Kappa]^2*\[Lambda] - 
     (24*I)*a^3*m*\[Lambda]^2 + 60*a^2*M*\[Lambda]^2 - 
     2*a^2*(-9 + k)*M*\[Lambda]^2 - 210*a^2*rh*\[Lambda]^2 + 
     10*a^2*m^2*rh*\[Lambda]^2 + (160*I)*a*m*M*rh*\[Lambda]^2 - 
     120*M^2*rh*\[Lambda]^2 - (440*I)*a*m*rh^2*\[Lambda]^2 + 
     770*M*rh^2*\[Lambda]^2 - 880*rh^3*\[Lambda]^2 - 
     (10*I)*a^4*(-9 + k)*\[Kappa]*\[Lambda]^2 + (88*I)*a^2*(-9 + k)*M*rh*
      \[Kappa]*\[Lambda]^2 - (264*I)*a^2*(-9 + k)*rh^2*\[Kappa]*\[Lambda]^2 - 
     30*a^4*rh*\[Kappa]^2*\[Lambda]^2 - 220*a^2*rh^3*\[Kappa]^2*\[Lambda]^2 + 
     4*a^2*M*\[Lambda]^3 - 30*a^2*rh*\[Lambda]^3 + 110*M*rh^2*\[Lambda]^3 - 
     220*rh^3*\[Lambda]^3 - (24*I)*a^3*m*M*rh*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (168*I)*a^4*\[Omega] + 
     (168*I)*a^4*(-9 + k)*\[Omega] - (120*I)*a^4*(10 - k)*(-9 + k)*\[Omega] - 
     (216*I)*a^4*m^2*\[Omega] + 336*a^3*m*M*\[Omega] - 
     264*a^3*(-9 + k)*m*M*\[Omega] + 336*a^3*(10 - k)*(-9 + k)*m*M*\[Omega] - 
     (816*I)*a^2*M^2*\[Omega] + (168*I)*a^2*(-9 + k)*M^2*\[Omega] - 
     (192*I)*a^2*(10 - k)*(-9 + k)*M^2*\[Omega] - 1080*a^3*m*rh*\[Omega] + 
     1056*a^3*(-9 + k)*m*rh*\[Omega] - 1152*a^3*(10 - k)*(-9 + k)*m*rh*
      \[Omega] + 600*a^3*m^3*rh*\[Omega] + (3000*I)*a^2*M*rh*\[Omega] - 
     (1008*I)*a^2*(-9 + k)*M*rh*\[Omega] + (1440*I)*a^2*(10 - k)*(-9 + k)*M*
      rh*\[Omega] + (360*I)*a^2*m^2*M*rh*\[Omega] - 480*a*m*M^2*rh*\[Omega] - 
     480*a*(-9 + k)*m*M^2*rh*\[Omega] - 576*a*(10 - k)*(-9 + k)*m*M^2*rh*
      \[Omega] + (1440*I)*M^3*rh*\[Omega] - (1320*I)*a^2*rh^2*\[Omega] + 
     (1320*I)*a^2*(-9 + k)*rh^2*\[Omega] - (1872*I)*a^2*(10 - k)*(-9 + k)*
      rh^2*\[Omega] - (2640*I)*a^2*m^2*rh^2*\[Omega] + 
     3960*a*m*M*rh^2*\[Omega] + 1320*a*(-9 + k)*m*M*rh^2*\[Omega] + 
     3744*a*(10 - k)*(-9 + k)*m*M*rh^2*\[Omega] - 
     (6600*I)*M^2*rh^2*\[Omega] - 5280*a*m*rh^3*\[Omega] - 
     4368*a*(10 - k)*(-9 + k)*m*rh^3*\[Omega] + (5280*I)*M*rh^3*\[Omega] - 
     (384*I)*a^5*m*\[Kappa]*\[Omega] - (528*I)*a^5*(-9 + k)*m*\[Kappa]*
      \[Omega] - 480*a^4*M*\[Kappa]*\[Omega] - 696*a^4*(-9 + k)*M*\[Kappa]*
      \[Omega] + 1608*a^4*rh*\[Kappa]*\[Omega] + 2640*a^4*(-9 + k)*rh*
      \[Kappa]*\[Omega] + (1728*I)*a^3*m*M*rh*\[Kappa]*\[Omega] + 
     (3696*I)*a^3*(-9 + k)*m*M*rh*\[Kappa]*\[Omega] + 
     960*a^2*M^2*rh*\[Kappa]*\[Omega] + 2112*a^2*(-9 + k)*M^2*rh*\[Kappa]*
      \[Omega] - (5280*I)*a^3*m*rh^2*\[Kappa]*\[Omega] - 
     (12672*I)*a^3*(-9 + k)*m*rh^2*\[Kappa]*\[Omega] - 
     4320*a^2*M*rh^2*\[Kappa]*\[Omega] - 12576*a^2*(-9 + k)*M*rh^2*\[Kappa]*
      \[Omega] + 3960*a^2*rh^3*\[Kappa]*\[Omega] + 13728*a^2*(-9 + k)*rh^3*
      \[Kappa]*\[Omega] + (13728*I)*a*(-9 + k)*m*M*rh^3*\[Kappa]*\[Omega] - 
     (24024*I)*a*(-9 + k)*m*rh^4*\[Kappa]*\[Omega] - 
     (240*I)*a^6*\[Kappa]^2*\[Omega] - 2640*a^5*m*rh*\[Kappa]^2*\[Omega] + 
     (1320*I)*a^4*M*rh*\[Kappa]^2*\[Omega] - (5736*I)*a^4*rh^2*\[Kappa]^2*
      \[Omega] - 21120*a^3*m*rh^3*\[Kappa]^2*\[Omega] + 
     (7920*I)*a^2*M*rh^3*\[Kappa]^2*\[Omega] - (17160*I)*a^2*rh^4*\[Kappa]^2*
      \[Omega] - 24024*a*m*rh^5*\[Kappa]^2*\[Omega] + 
     160*a^2*M*rh^2*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (144*I)*a^4*rh^2*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (16*I)*a^2*(-9 + k)*M*rh*(-3 + \[Lambda])*\[Omega] - 
     (88*I)*a^2*(-9 + k)*rh^2*(-3 + \[Lambda])*\[Omega] - 
     24*a^4*rh*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     440*a^2*rh^3*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
     (16*I)*a^4*\[Lambda]*\[Omega] - (32*I)*a^4*(-9 + k)*\[Lambda]*\[Omega] + 
     (32*I)*a^4*(10 - k)*(-9 + k)*\[Lambda]*\[Omega] - 
     (56*I)*a^4*m^2*\[Lambda]*\[Omega] + 208*a^3*m*M*\[Lambda]*\[Omega] + 
     (32*I)*a^2*(10 - k)*(-9 + k)*M^2*\[Lambda]*\[Omega] - 
     1360*a^3*m*rh*\[Lambda]*\[Omega] + (280*I)*a^2*M*rh*\[Lambda]*\[Omega] + 
     (160*I)*a^2*(-9 + k)*M*rh*\[Lambda]*\[Omega] - 
     (384*I)*a^2*(10 - k)*(-9 + k)*M*rh*\[Lambda]*\[Omega] - 
     (440*I)*a^2*rh^2*\[Lambda]*\[Omega] - (440*I)*a^2*(-9 + k)*rh^2*
      \[Lambda]*\[Omega] + (624*I)*a^2*(10 - k)*(-9 + k)*rh^2*\[Lambda]*
      \[Omega] - (880*I)*a^2*m^2*rh^2*\[Lambda]*\[Omega] + 
     2200*a*m*M*rh^2*\[Lambda]*\[Omega] - (1320*I)*M^2*rh^2*\[Lambda]*
      \[Omega] - 5280*a*m*rh^3*\[Lambda]*\[Omega] + 
     (2640*I)*M*rh^3*\[Lambda]*\[Omega] + 48*a^4*M*\[Kappa]*\[Lambda]*
      \[Omega] + 96*a^4*(-9 + k)*M*\[Kappa]*\[Lambda]*\[Omega] - 
     296*a^4*rh*\[Kappa]*\[Lambda]*\[Omega] - 704*a^4*(-9 + k)*rh*\[Kappa]*
      \[Lambda]*\[Omega] + 720*a^2*M*rh^2*\[Kappa]*\[Lambda]*\[Omega] + 
     2112*a^2*(-9 + k)*M*rh^2*\[Kappa]*\[Lambda]*\[Omega] - 
     1320*a^2*rh^3*\[Kappa]*\[Lambda]*\[Omega] - 4576*a^2*(-9 + k)*rh^3*
      \[Kappa]*\[Lambda]*\[Omega] + (48*I)*a^6*\[Kappa]^2*\[Lambda]*
      \[Omega] + (1616*I)*a^4*rh^2*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (5720*I)*a^2*rh^4*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (24*I)*a^4*\[Lambda]^2*\[Omega] - 60*a^3*m*rh*\[Lambda]^2*\[Omega] - 
     (160*I)*a^2*M*rh*\[Lambda]^2*\[Omega] + (440*I)*a^2*rh^2*\[Lambda]^2*
      \[Omega] - 440*a*m*rh^3*\[Lambda]^2*\[Omega] + 
     (144*I)*a^5*m*\[Omega]^2 - 288*a^4*M*\[Omega]^2 + 
     120*a^4*(-9 + k)*M*\[Omega]^2 - 240*a^4*(10 - k)*(-9 + k)*M*\[Omega]^2 + 
     720*a^4*rh*\[Omega]^2 - 528*a^4*(-9 + k)*rh*\[Omega]^2 + 
     864*a^4*(10 - k)*(-9 + k)*rh*\[Omega]^2 - 2760*a^4*m^2*rh*\[Omega]^2 + 
     (240*I)*a^3*m*M*rh*\[Omega]^2 + 480*a^2*M^2*rh*\[Omega]^2 + 
     528*a^2*(-9 + k)*M^2*rh*\[Omega]^2 + 576*a^2*(10 - k)*(-9 + k)*M^2*rh*
      \[Omega]^2 + (1320*I)*a^3*m*rh^2*\[Omega]^2 - 
     2640*a^2*M*rh^2*\[Omega]^2 - 1560*a^2*(-9 + k)*M*rh^2*\[Omega]^2 - 
     3744*a^2*(10 - k)*(-9 + k)*M*rh^2*\[Omega]^2 + 
     2640*a^2*rh^3*\[Omega]^2 + 4368*a^2*(10 - k)*(-9 + k)*rh^3*\[Omega]^2 - 
     10560*a^2*m^2*rh^3*\[Omega]^2 + (10560*I)*a*m*M*rh^3*\[Omega]^2 + 
     (192*I)*a^6*\[Kappa]*\[Omega]^2 + (336*I)*a^6*(-9 + k)*\[Kappa]*
      \[Omega]^2 - (960*I)*a^4*M*rh*\[Kappa]*\[Omega]^2 - 
     (2640*I)*a^4*(-9 + k)*M*rh*\[Kappa]*\[Omega]^2 + 
     (2640*I)*a^4*rh^2*\[Kappa]*\[Omega]^2 + (9504*I)*a^4*(-9 + k)*rh^2*
      \[Kappa]*\[Omega]^2 - (13728*I)*a^2*(-9 + k)*M*rh^3*\[Kappa]*
      \[Omega]^2 + (24024*I)*a^2*(-9 + k)*rh^4*\[Kappa]*\[Omega]^2 + 
     1680*a^6*rh*\[Kappa]^2*\[Omega]^2 + 15840*a^4*rh^3*\[Kappa]^2*
      \[Omega]^2 + 24024*a^2*rh^5*\[Kappa]^2*\[Omega]^2 + 
     (96*I)*a^5*m*\[Lambda]*\[Omega]^2 - 144*a^4*M*\[Lambda]*\[Omega]^2 + 
     1040*a^4*rh*\[Lambda]*\[Omega]^2 + (2640*I)*a^3*m*rh^2*\[Lambda]*
      \[Omega]^2 - 2200*a^2*M*rh^2*\[Lambda]*\[Omega]^2 + 
     6160*a^2*rh^3*\[Lambda]*\[Omega]^2 + (5720*I)*a*m*rh^4*\[Lambda]*
      \[Omega]^2 + 4004*rh^5*\[Lambda]*\[Omega]^2 + 
     60*a^4*rh*\[Lambda]^2*\[Omega]^2 + 880*a^2*rh^3*\[Lambda]^2*\[Omega]^2 + 
     2002*rh^5*\[Lambda]^2*\[Omega]^2 + (48*I)*a^6*\[Omega]^3 + 
     3840*a^5*m*rh*\[Omega]^3 - (720*I)*a^4*M*rh*\[Omega]^3 + 
     (3960*I)*a^4*rh^2*\[Omega]^3 + 26400*a^3*m*rh^3*\[Omega]^3 - 
     (15840*I)*a^2*M*rh^3*\[Omega]^3 + (17160*I)*a^2*rh^4*\[Omega]^3 + 
     24024*a*m*rh^5*\[Omega]^3 - (24024*I)*M*rh^5*\[Omega]^3 - 
     (48*I)*a^6*\[Lambda]*\[Omega]^3 - (1760*I)*a^4*rh^2*\[Lambda]*
      \[Omega]^3 - (5720*I)*a^2*rh^4*\[Lambda]*\[Omega]^3 - 
     1680*a^6*rh*\[Omega]^4 - 15840*a^4*rh^3*\[Omega]^4 - 
     24024*a^2*rh^5*\[Omega]^4 - 48*a*(-9 + k)*m*M^2*rh*
      (2*\[Kappa] + \[Omega]) + 264*a*(-9 + k)*m*M*rh^2*
      (2*\[Kappa] + \[Omega]) - 24*a^2*(-9 + k)*M*rh^2*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 12*a^2*(10 - k)*(-9 + k)*M*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     48*a^2*(10 - k)*(-9 + k)*rh*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 48*(10 - k)*(-9 + k)*M^2*rh*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     312*(10 - k)*(-9 + k)*M*rh^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 364*(10 - k)*(-9 + k)*rh^3*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     30*a^4*rh*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     660*a^2*rh^3*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 2002*rh^5*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - (4*I)*a^2*(-9 + k)*M*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) + (44*I)*(-9 + k)*M^2*rh*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (132*I)*(-9 + k)*M*rh^2*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 2*a^4*(-9 + k)*\[Kappa]*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) + 44*a^2*(-9 + k)*M*rh*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     264*a^2*(-9 + k)*rh^2*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 1144*(-9 + k)*M*rh^3*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     2002*(-9 + k)*rh^4*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-9 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  ((-12*a^4 + 144*a^4*(-8 + k) - 60*a^4*(9 - k)*(-8 + k) + 60*a^4*m^2 - 
     192*a^4*(-8 + k)*m^2 + 96*a^4*(9 - k)*(-8 + k)*m^2 - 24*a^4*m^4 + 
     (48*I)*a^3*m*M + (432*I)*a^3*(-8 + k)*m*M - (192*I)*a^3*(9 - k)*(-8 + k)*
      m*M - (72*I)*a^3*m^3*M + 288*a^2*(-8 + k)*M^2 - 
     144*a^2*(9 - k)*(-8 + k)*M^2 - 96*a^2*(-8 + k)*m^2*M^2 + 
     96*a^2*(9 - k)*(-8 + k)*m^2*M^2 - (96*I)*a*m*M^3 + 
     (96*I)*a*(-8 + k)*m*M^3 - (96*I)*a*(9 - k)*(-8 + k)*m*M^3 - 
     (432*I)*a^3*m*rh - (720*I)*a^3*(-8 + k)*m*rh + 
     (264*I)*a^3*(9 - k)*(-8 + k)*m*rh + (432*I)*a^3*m^3*rh - 
     1440*a^2*(-8 + k)*M*rh + 792*a^2*(9 - k)*(-8 + k)*M*rh + 
     1440*a^2*(-8 + k)*m^2*M*rh - 1056*a^2*(9 - k)*(-8 + k)*m^2*M*rh + 
     (432*I)*a*m*M^2*rh - (1440*I)*a*(-8 + k)*m*M^2*rh + 
     (1056*I)*a*(9 - k)*(-8 + k)*m*M^2*rh + 1320*a^2*(-8 + k)*rh^2 - 
     792*a^2*(9 - k)*(-8 + k)*rh^2 - 2640*a^2*(-8 + k)*m^2*rh^2 + 
     1584*a^2*(9 - k)*(-8 + k)*m^2*rh^2 + (2640*I)*a*(-8 + k)*m*M*rh^2 - 
     (1584*I)*a*(9 - k)*(-8 + k)*m*M*rh^2 - 288*a^5*m*\[Kappa] - 
     192*a^5*(-8 + k)*m*\[Kappa] + (528*I)*a^4*M*\[Kappa] + 
     (384*I)*a^4*(-8 + k)*M*\[Kappa] - (288*I)*a^4*m^2*M*\[Kappa] - 
     (288*I)*a^4*(-8 + k)*m^2*M*\[Kappa] - 288*a^3*m*M^2*\[Kappa] - 
     288*a^3*(-8 + k)*m*M^2*\[Kappa] - (1296*I)*a^4*rh*\[Kappa] - 
     (1200*I)*a^4*(-8 + k)*rh*\[Kappa] + (1728*I)*a^4*m^2*rh*\[Kappa] + 
     (1920*I)*a^4*(-8 + k)*m^2*rh*\[Kappa] + 3024*a^3*m*M*rh*\[Kappa] + 
     2688*a^3*(-8 + k)*m*M*rh*\[Kappa] - (1296*I)*a^2*M^2*rh*\[Kappa] - 
     (960*I)*a^2*(-8 + k)*M^2*rh*\[Kappa] - 3240*a^3*m*rh^2*\[Kappa] - 
     2640*a^3*(-8 + k)*m*rh^2*\[Kappa] + (5400*I)*a^2*M*rh^2*\[Kappa] + 
     (5280*I)*a^2*(-8 + k)*M*rh^2*\[Kappa] - (4320*I)*a^2*m^2*M*rh^2*
      \[Kappa] - (5280*I)*a^2*(-8 + k)*m^2*M*rh^2*\[Kappa] - 
     4320*a*m*M^2*rh^2*\[Kappa] - 4320*a*(-8 + k)*m*M^2*rh^2*\[Kappa] - 
     (3960*I)*a^2*rh^3*\[Kappa] - (5280*I)*a^2*(-8 + k)*rh^3*\[Kappa] + 
     (7920*I)*a^2*m^2*rh^3*\[Kappa] + (10560*I)*a^2*(-8 + k)*m^2*rh^3*
      \[Kappa] + 7920*a*m*M*rh^3*\[Kappa] + 7920*a*(-8 + k)*m*M*rh^3*
      \[Kappa] - 120*a^6*\[Kappa]^2 + 144*a^6*m^2*\[Kappa]^2 - 
     (144*I)*a^5*m*M*\[Kappa]^2 + (864*I)*a^5*m*rh*\[Kappa]^2 + 
     864*a^4*M*rh*\[Kappa]^2 - 2700*a^4*rh^2*\[Kappa]^2 + 
     4320*a^4*m^2*rh^2*\[Kappa]^2 - (4104*I)*a^3*m*M*rh^2*\[Kappa]^2 + 
     (3960*I)*a^3*m*rh^3*\[Kappa]^2 + 3960*a^2*M*rh^3*\[Kappa]^2 - 
     5940*a^2*rh^4*\[Kappa]^2 + 11880*a^2*m^2*rh^4*\[Kappa]^2 - 
     (11880*I)*a*m*M*rh^4*\[Kappa]^2 - 64*a^4*\[Lambda] + 
     100*a^4*m^2*\[Lambda] + (264*I)*a^3*m*M*\[Lambda] - 
     (80*I)*a^3*(-8 + k)*m*M*\[Lambda] + (96*I)*a^3*(9 - k)*(-8 + k)*m*M*
      \[Lambda] - 80*a^2*M^2*\[Lambda] + 8*a^2*(-8 + k)*M^2*\[Lambda] - 
     (792*I)*a^3*m*rh*\[Lambda] + (320*I)*a^3*(-8 + k)*m*rh*\[Lambda] - 
     (352*I)*a^3*(9 - k)*(-8 + k)*m*rh*\[Lambda] + 
     (72*I)*a^3*m^3*rh*\[Lambda] + 936*a^2*M*rh*\[Lambda] - 
     40*a^2*(-8 + k)*M*rh*\[Lambda] - 576*a^2*m^2*M*rh*\[Lambda] - 
     (432*I)*a*m*M^2*rh*\[Lambda] - (352*I)*a*(9 - k)*(-8 + k)*m*M^2*rh*
      \[Lambda] - 1350*a^2*rh^2*\[Lambda] + 1530*a^2*m^2*rh^2*\[Lambda] + 
     (2520*I)*a*m*M*rh^2*\[Lambda] - (880*I)*a*(-8 + k)*m*M*rh^2*\[Lambda] + 
     (2112*I)*a*(9 - k)*(-8 + k)*m*M*rh^2*\[Lambda] - 
     1080*M^2*rh^2*\[Lambda] - (2640*I)*a*m*rh^3*\[Lambda] + 
     (1760*I)*a*(-8 + k)*m*rh^3*\[Lambda] - (2288*I)*a*(9 - k)*(-8 + k)*m*
      rh^3*\[Lambda] + 3300*M*rh^3*\[Lambda] - 1980*rh^4*\[Lambda] + 
     48*a^5*m*\[Kappa]*\[Lambda] + 96*a^5*(-8 + k)*m*\[Kappa]*\[Lambda] + 
     (24*I)*a^4*(-8 + k)*M*\[Kappa]*\[Lambda] - (200*I)*a^4*(-8 + k)*rh*
      \[Kappa]*\[Lambda] - 432*a^3*m*M*rh*\[Kappa]*\[Lambda] - 
     960*a^3*(-8 + k)*m*M*rh*\[Kappa]*\[Lambda] + 1440*a^3*m*rh^2*\[Kappa]*
      \[Lambda] + 3520*a^3*(-8 + k)*m*rh^2*\[Kappa]*\[Lambda] + 
     (880*I)*a^2*(-8 + k)*M*rh^2*\[Kappa]*\[Lambda] - 
     (1760*I)*a^2*(-8 + k)*rh^3*\[Kappa]*\[Lambda] - 
     2640*a*m*M*rh^3*\[Kappa]*\[Lambda] - 7040*a*(-8 + k)*m*M*rh^3*\[Kappa]*
      \[Lambda] + 3960*a*m*rh^4*\[Kappa]*\[Lambda] + 
     11440*a*(-8 + k)*m*rh^4*\[Kappa]*\[Lambda] - 
     6*a^6*\[Kappa]^2*\[Lambda] - (432*I)*a^5*m*rh*\[Kappa]^2*\[Lambda] - 
     270*a^4*rh^2*\[Kappa]^2*\[Lambda] - (5280*I)*a^3*m*rh^3*\[Kappa]^2*
      \[Lambda] - 990*a^2*rh^4*\[Kappa]^2*\[Lambda] - 
     (10296*I)*a*m*rh^5*\[Kappa]^2*\[Lambda] - 38*a^4*\[Lambda]^2 + 
     2*a^4*m^2*\[Lambda]^2 + (32*I)*a^3*m*M*\[Lambda]^2 - 
     40*a^2*M^2*\[Lambda]^2 + 4*a^2*(-8 + k)*M^2*\[Lambda]^2 - 
     (216*I)*a^3*m*rh*\[Lambda]^2 + 540*a^2*M*rh*\[Lambda]^2 - 
     20*a^2*(-8 + k)*M*rh*\[Lambda]^2 - 945*a^2*rh^2*\[Lambda]^2 + 
     45*a^2*m^2*rh^2*\[Lambda]^2 + (720*I)*a*m*M*rh^2*\[Lambda]^2 - 
     540*M^2*rh^2*\[Lambda]^2 - (1320*I)*a*m*rh^3*\[Lambda]^2 + 
     2310*M*rh^3*\[Lambda]^2 - 1980*rh^4*\[Lambda]^2 + 
     (12*I)*a^4*(-8 + k)*M*\[Kappa]*\[Lambda]^2 - (100*I)*a^4*(-8 + k)*rh*
      \[Kappa]*\[Lambda]^2 + (440*I)*a^2*(-8 + k)*M*rh^2*\[Kappa]*
      \[Lambda]^2 - (880*I)*a^2*(-8 + k)*rh^3*\[Kappa]*\[Lambda]^2 - 
     3*a^6*\[Kappa]^2*\[Lambda]^2 - 135*a^4*rh^2*\[Kappa]^2*\[Lambda]^2 - 
     495*a^2*rh^4*\[Kappa]^2*\[Lambda]^2 - 3*a^4*\[Lambda]^3 + 
     36*a^2*M*rh*\[Lambda]^3 - 135*a^2*rh^2*\[Lambda]^3 + 
     330*M*rh^3*\[Lambda]^3 - 495*rh^4*\[Lambda]^3 - 
     (216*I)*a^3*m*M*rh^2*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
     24*a^3*(-8 + k)*m*M*rh*(4*\[Kappa] - 5*\[Omega]) - 288*a^5*m*\[Omega] + 
     384*a^5*(-8 + k)*m*\[Omega] - 264*a^5*(9 - k)*(-8 + k)*m*\[Omega] + 
     216*a^5*m^3*\[Omega] + (1080*I)*a^4*M*\[Omega] - 
     (672*I)*a^4*(-8 + k)*M*\[Omega] + (480*I)*a^4*(9 - k)*(-8 + k)*M*
      \[Omega] + (288*I)*a^4*m^2*M*\[Omega] - 288*a^3*m*M^2*\[Omega] + 
     192*a^3*(-8 + k)*m*M^2*\[Omega] - 288*a^3*(9 - k)*(-8 + k)*m*M^2*
      \[Omega] + (576*I)*a^2*M^3*\[Omega] - (144*I)*a^2*(-8 + k)*M^3*
      \[Omega] + (96*I)*a^2*(9 - k)*(-8 + k)*M^3*\[Omega] - 
     (1512*I)*a^4*rh*\[Omega] + (1632*I)*a^4*(-8 + k)*rh*\[Omega] - 
     (1320*I)*a^4*(9 - k)*(-8 + k)*rh*\[Omega] - (1944*I)*a^4*m^2*rh*
      \[Omega] + 3024*a^3*m*M*rh*\[Omega] - 2568*a^3*(-8 + k)*m*M*rh*
      \[Omega] + 3696*a^3*(9 - k)*(-8 + k)*m*M*rh*\[Omega] - 
     (7344*I)*a^2*M^2*rh*\[Omega] + (1680*I)*a^2*(-8 + k)*M^2*rh*\[Omega] - 
     (2112*I)*a^2*(9 - k)*(-8 + k)*M^2*rh*\[Omega] - 
     4860*a^3*m*rh^2*\[Omega] + 5280*a^3*(-8 + k)*m*rh^2*\[Omega] - 
     6336*a^3*(9 - k)*(-8 + k)*m*rh^2*\[Omega] + 2700*a^3*m^3*rh^2*\[Omega] + 
     (13500*I)*a^2*M*rh^2*\[Omega] - (4800*I)*a^2*(-8 + k)*M*rh^2*\[Omega] + 
     (7920*I)*a^2*(9 - k)*(-8 + k)*M*rh^2*\[Omega] + 
     (1620*I)*a^2*m^2*M*rh^2*\[Omega] - 2160*a*m*M^2*rh^2*\[Omega] - 
     2160*a*(-8 + k)*m*M^2*rh^2*\[Omega] - 3168*a*(9 - k)*(-8 + k)*m*M^2*rh^2*
      \[Omega] + (6480*I)*M^3*rh^2*\[Omega] - (3960*I)*a^2*rh^3*\[Omega] + 
     (3960*I)*a^2*(-8 + k)*rh^3*\[Omega] - (6864*I)*a^2*(9 - k)*(-8 + k)*rh^3*
      \[Omega] - (7920*I)*a^2*m^2*rh^3*\[Omega] + 11880*a*m*M*rh^3*\[Omega] + 
     3960*a*(-8 + k)*m*M*rh^3*\[Omega] + 13728*a*(9 - k)*(-8 + k)*m*M*rh^3*
      \[Omega] - (19800*I)*M^2*rh^3*\[Omega] - 11880*a*m*rh^4*\[Omega] - 
     12012*a*(9 - k)*(-8 + k)*m*rh^4*\[Omega] + (11880*I)*M*rh^4*\[Omega] + 
     432*a^6*\[Kappa]*\[Omega] + 480*a^6*(-8 + k)*\[Kappa]*\[Omega] + 
     (576*I)*a^5*m*M*\[Kappa]*\[Omega] + (720*I)*a^5*(-8 + k)*m*M*\[Kappa]*
      \[Omega] + 288*a^4*M^2*\[Kappa]*\[Omega] + 432*a^4*(-8 + k)*M^2*
      \[Kappa]*\[Omega] - (3456*I)*a^5*m*rh*\[Kappa]*\[Omega] - 
     (5280*I)*a^5*(-8 + k)*m*rh*\[Kappa]*\[Omega] - 
     4032*a^4*M*rh*\[Kappa]*\[Omega] - 6960*a^4*(-8 + k)*M*rh*\[Kappa]*
      \[Omega] + 6912*a^4*rh^2*\[Kappa]*\[Omega] + 13200*a^4*(-8 + k)*rh^2*
      \[Kappa]*\[Omega] + (6912*I)*a^3*m*M*rh^2*\[Kappa]*\[Omega] + 
     (18480*I)*a^3*(-8 + k)*m*M*rh^2*\[Kappa]*\[Omega] + 
     4320*a^2*M^2*rh^2*\[Kappa]*\[Omega] + 10368*a^2*(-8 + k)*M^2*rh^2*
      \[Kappa]*\[Omega] - (15840*I)*a^3*m*rh^3*\[Kappa]*\[Omega] - 
     (42240*I)*a^3*(-8 + k)*m*rh^3*\[Kappa]*\[Omega] - 
     11520*a^2*M*rh^3*\[Kappa]*\[Omega] - 41280*a^2*(-8 + k)*M*rh^3*\[Kappa]*
      \[Omega] + 7920*a^2*rh^4*\[Kappa]*\[Omega] + 34320*a^2*(-8 + k)*rh^4*
      \[Kappa]*\[Omega] + (34320*I)*a*(-8 + k)*m*M*rh^4*\[Kappa]*\[Omega] - 
     (48048*I)*a*(-8 + k)*m*rh^5*\[Kappa]*\[Omega] - 
     336*a^7*m*\[Kappa]^2*\[Omega] + (180*I)*a^6*M*\[Kappa]^2*\[Omega] - 
     (1872*I)*a^6*rh*\[Kappa]^2*\[Omega] - 11880*a^5*m*rh^2*\[Kappa]^2*
      \[Omega] + (5916*I)*a^4*M*rh^2*\[Kappa]^2*\[Omega] - 
     (16344*I)*a^4*rh^3*\[Kappa]^2*\[Omega] - 47520*a^3*m*rh^4*\[Kappa]^2*
      \[Omega] + (17820*I)*a^2*M*rh^4*\[Kappa]^2*\[Omega] - 
     (30888*I)*a^2*rh^5*\[Kappa]^2*\[Omega] - 36036*a*m*rh^6*\[Kappa]^2*
      \[Omega] + 48*a^4*M*rh*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     720*a^2*M*rh^3*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (48*I)*a^6*rh*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (576*I)*a^4*rh^3*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] - 
     (16*I)*a^4*(-8 + k)*rh*(-3 + \[Lambda])*\[Omega] + 
     (160*I)*a^2*(-8 + k)*M*rh^2*(-3 + \[Lambda])*\[Omega] - 
     (440*I)*a^2*(-8 + k)*rh^3*(-3 + \[Lambda])*\[Omega] - 
     216*a^4*rh^2*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     1320*a^2*rh^4*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     264*a^5*m*\[Lambda]*\[Omega] - (132*I)*a^4*M*\[Lambda]*\[Omega] + 
     (80*I)*a^4*(-8 + k)*M*\[Lambda]*\[Omega] - (96*I)*a^4*(9 - k)*(-8 + k)*M*
      \[Lambda]*\[Omega] + (144*I)*a^4*rh*\[Lambda]*\[Omega] - 
     (304*I)*a^4*(-8 + k)*rh*\[Lambda]*\[Omega] + (352*I)*a^4*(9 - k)*
      (-8 + k)*rh*\[Lambda]*\[Omega] - (504*I)*a^4*m^2*rh*\[Lambda]*
      \[Omega] + 1872*a^3*m*M*rh*\[Lambda]*\[Omega] + 
     (352*I)*a^2*(9 - k)*(-8 + k)*M^2*rh*\[Lambda]*\[Omega] - 
     6120*a^3*m*rh^2*\[Lambda]*\[Omega] + (1260*I)*a^2*M*rh^2*\[Lambda]*
      \[Omega] + (720*I)*a^2*(-8 + k)*M*rh^2*\[Lambda]*\[Omega] - 
     (2112*I)*a^2*(9 - k)*(-8 + k)*M*rh^2*\[Lambda]*\[Omega] - 
     (1320*I)*a^2*rh^3*\[Lambda]*\[Omega] - (1320*I)*a^2*(-8 + k)*rh^3*
      \[Lambda]*\[Omega] + (2288*I)*a^2*(9 - k)*(-8 + k)*rh^3*\[Lambda]*
      \[Omega] - (2640*I)*a^2*m^2*rh^3*\[Lambda]*\[Omega] + 
     6600*a*m*M*rh^3*\[Lambda]*\[Omega] - (3960*I)*M^2*rh^3*\[Lambda]*
      \[Omega] - 11880*a*m*rh^4*\[Lambda]*\[Omega] + 
     (5940*I)*M*rh^4*\[Lambda]*\[Omega] - 48*a^6*\[Kappa]*\[Lambda]*
      \[Omega] - 96*a^6*(-8 + k)*\[Kappa]*\[Lambda]*\[Omega] + 
     384*a^4*M*rh*\[Kappa]*\[Lambda]*\[Omega] + 960*a^4*(-8 + k)*M*rh*
      \[Kappa]*\[Lambda]*\[Omega] - 1224*a^4*rh^2*\[Kappa]*\[Lambda]*
      \[Omega] - 3520*a^4*(-8 + k)*rh^2*\[Kappa]*\[Lambda]*\[Omega] + 
     1920*a^2*M*rh^3*\[Kappa]*\[Lambda]*\[Omega] + 7040*a^2*(-8 + k)*M*rh^3*
      \[Kappa]*\[Lambda]*\[Omega] - 2640*a^2*rh^4*\[Kappa]*\[Lambda]*
      \[Omega] - 11440*a^2*(-8 + k)*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 
     (384*I)*a^6*rh*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (4704*I)*a^4*rh^3*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (10296*I)*a^2*rh^5*\[Kappa]^2*\[Lambda]*\[Omega] - 
     6*a^5*m*\[Lambda]^2*\[Omega] - (32*I)*a^4*M*\[Lambda]^2*\[Omega] + 
     (216*I)*a^4*rh*\[Lambda]^2*\[Omega] - 270*a^3*m*rh^2*\[Lambda]^2*
      \[Omega] - (720*I)*a^2*M*rh^2*\[Lambda]^2*\[Omega] + 
     (1320*I)*a^2*rh^3*\[Lambda]^2*\[Omega] - 990*a*m*rh^4*\[Lambda]^2*
      \[Omega] + (24*I)*a^4*M*rh^2*\[Kappa]*(\[Kappa] - 4*\[Omega])*
      \[Omega] + 192*a^6*\[Omega]^2 - 192*a^6*(-8 + k)*\[Omega]^2 + 
     168*a^6*(9 - k)*(-8 + k)*\[Omega]^2 - 552*a^6*m^2*\[Omega]^2 - 
     (312*I)*a^5*m*M*\[Omega]^2 + 288*a^4*M^2*\[Omega]^2 - 
     96*a^4*(-8 + k)*M^2*\[Omega]^2 + 192*a^4*(9 - k)*(-8 + k)*M^2*
      \[Omega]^2 + (1296*I)*a^5*m*rh*\[Omega]^2 - 2592*a^4*M*rh*\[Omega]^2 + 
     1200*a^4*(-8 + k)*M*rh*\[Omega]^2 - 2640*a^4*(9 - k)*(-8 + k)*M*rh*
      \[Omega]^2 + 3240*a^4*rh^2*\[Omega]^2 - 2640*a^4*(-8 + k)*rh^2*
      \[Omega]^2 + 4752*a^4*(9 - k)*(-8 + k)*rh^2*\[Omega]^2 - 
     12420*a^4*m^2*rh^2*\[Omega]^2 + (1080*I)*a^3*m*M*rh^2*\[Omega]^2 + 
     2160*a^2*M^2*rh^2*\[Omega]^2 + 2592*a^2*(-8 + k)*M^2*rh^2*\[Omega]^2 + 
     3168*a^2*(9 - k)*(-8 + k)*M^2*rh^2*\[Omega]^2 + 
     (3960*I)*a^3*m*rh^3*\[Omega]^2 - 7920*a^2*M*rh^3*\[Omega]^2 - 
     5040*a^2*(-8 + k)*M*rh^3*\[Omega]^2 - 13728*a^2*(9 - k)*(-8 + k)*M*rh^3*
      \[Omega]^2 + 5940*a^2*rh^4*\[Omega]^2 + 12012*a^2*(9 - k)*(-8 + k)*rh^4*
      \[Omega]^2 - 23760*a^2*m^2*rh^4*\[Omega]^2 + (23760*I)*a*m*M*rh^4*
      \[Omega]^2 - (288*I)*a^6*M*\[Kappa]*\[Omega]^2 - 
     (432*I)*a^6*(-8 + k)*M*\[Kappa]*\[Omega]^2 + (1728*I)*a^6*rh*\[Kappa]*
      \[Omega]^2 + (3360*I)*a^6*(-8 + k)*rh*\[Kappa]*\[Omega]^2 - 
     (4224*I)*a^4*M*rh^2*\[Kappa]*\[Omega]^2 - (13200*I)*a^4*(-8 + k)*M*rh^2*
      \[Kappa]*\[Omega]^2 + (7920*I)*a^4*rh^3*\[Kappa]*\[Omega]^2 + 
     (31680*I)*a^4*(-8 + k)*rh^3*\[Kappa]*\[Omega]^2 - 
     (34320*I)*a^2*(-8 + k)*M*rh^4*\[Kappa]*\[Omega]^2 + 
     (48048*I)*a^2*(-8 + k)*rh^5*\[Kappa]*\[Omega]^2 + 
     192*a^8*\[Kappa]^2*\[Omega]^2 + 7560*a^6*rh^2*\[Kappa]^2*\[Omega]^2 + 
     35640*a^4*rh^4*\[Kappa]^2*\[Omega]^2 + 36036*a^2*rh^6*\[Kappa]^2*
      \[Omega]^2 + 164*a^6*\[Lambda]*\[Omega]^2 + (864*I)*a^5*m*rh*\[Lambda]*
      \[Omega]^2 - 1296*a^4*M*rh*\[Lambda]*\[Omega]^2 + 
     4680*a^4*rh^2*\[Lambda]*\[Omega]^2 + (7920*I)*a^3*m*rh^3*\[Lambda]*
      \[Omega]^2 - 6600*a^2*M*rh^3*\[Lambda]*\[Omega]^2 + 
     13860*a^2*rh^4*\[Lambda]*\[Omega]^2 + (10296*I)*a*m*rh^5*\[Lambda]*
      \[Omega]^2 + 6006*rh^6*\[Lambda]*\[Omega]^2 + 
     4*a^6*\[Lambda]^2*\[Omega]^2 + 270*a^4*rh^2*\[Lambda]^2*\[Omega]^2 + 
     1980*a^2*rh^4*\[Lambda]^2*\[Omega]^2 + 3003*rh^6*\[Lambda]^2*
      \[Omega]^2 + 552*a^7*m*\[Omega]^3 + (96*I)*a^6*M*\[Omega]^3 + 
     (432*I)*a^6*rh*\[Omega]^3 + 17280*a^5*m*rh^2*\[Omega]^3 - 
     (3240*I)*a^4*M*rh^2*\[Omega]^3 + (11880*I)*a^4*rh^3*\[Omega]^3 + 
     59400*a^3*m*rh^4*\[Omega]^3 - (35640*I)*a^2*M*rh^4*\[Omega]^3 + 
     (30888*I)*a^2*rh^5*\[Omega]^3 + 36036*a*m*rh^6*\[Omega]^3 - 
     (36036*I)*M*rh^6*\[Omega]^3 - (432*I)*a^6*rh*\[Lambda]*\[Omega]^3 - 
     (5280*I)*a^4*rh^3*\[Lambda]*\[Omega]^3 - (10296*I)*a^2*rh^5*\[Lambda]*
      \[Omega]^3 - 192*a^8*\[Omega]^4 - 7560*a^6*rh^2*\[Omega]^4 - 
     35640*a^4*rh^4*\[Omega]^4 - 36036*a^2*rh^6*\[Omega]^4 + 
     48*a^3*(-8 + k)*m*M*rh*(2*\[Kappa] + \[Omega]) - 
     480*a*(-8 + k)*m*M^2*rh^2*(2*\[Kappa] + \[Omega]) + 
     1320*a*(-8 + k)*m*M*rh^3*(2*\[Kappa] + \[Omega]) + 
     48*a^2*(-8 + k)*M^2*rh^2*\[Omega]*(4*\[Kappa] + \[Omega]) - 
     240*a^2*(-8 + k)*M*rh^3*\[Omega]*(4*\[Kappa] + \[Omega]) - 
     6*a^4*(9 - k)*(-8 + k)*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     8*a^2*(9 - k)*(-8 + k)*M^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 132*a^2*(9 - k)*(-8 + k)*M*rh*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     264*a^2*(9 - k)*(-8 + k)*rh^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 264*(9 - k)*(-8 + k)*M^2*rh^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     1144*(9 - k)*(-8 + k)*M*rh^3*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1001*(9 - k)*(-8 + k)*rh^4*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     a^6*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     135*a^4*rh^2*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1485*a^2*rh^4*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3003*rh^6*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     (4*I)*a^2*(-8 + k)*M^2*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - (40*I)*a^2*(-8 + k)*M*rh*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) + (220*I)*(-8 + k)*M^2*rh^2*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (440*I)*(-8 + k)*M*rh^3*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 20*a^4*(-8 + k)*rh*\[Kappa]*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) + 220*a^2*(-8 + k)*M*rh^2*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     880*a^2*(-8 + k)*rh^3*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 2860*(-8 + k)*M*rh^4*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     4004*(-8 + k)*rh^5*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-8 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  (((-120*I)*a^5*m - (288*I)*a^5*(-7 + k)*m + (96*I)*a^5*(8 - k)*(-7 + k)*m + 
     (120*I)*a^5*m^3 + 72*a^4*M - 720*a^4*(-7 + k)*M + 
     288*a^4*(8 - k)*(-7 + k)*M - 216*a^4*m^2*M + 528*a^4*(-7 + k)*m^2*M - 
     288*a^4*(8 - k)*(-7 + k)*m^2*M + (336*I)*a^3*m*M^2 - 
     (720*I)*a^3*(-7 + k)*m*M^2 + (384*I)*a^3*(8 - k)*(-7 + k)*m*M^2 - 
     192*a^2*(-7 + k)*M^3 + 96*a^2*(8 - k)*(-7 + k)*M^3 - 96*a^4*rh + 
     1296*a^4*(-7 + k)*rh - 600*a^4*(8 - k)*(-7 + k)*rh + 480*a^4*m^2*rh - 
     1728*a^4*(-7 + k)*m^2*rh + 960*a^4*(8 - k)*(-7 + k)*m^2*rh - 
     192*a^4*m^4*rh + (384*I)*a^3*m*M*rh + (3888*I)*a^3*(-7 + k)*m*M*rh - 
     (1920*I)*a^3*(8 - k)*(-7 + k)*m*M*rh - (576*I)*a^3*m^3*M*rh + 
     2592*a^2*(-7 + k)*M^2*rh - 1440*a^2*(8 - k)*(-7 + k)*M^2*rh - 
     864*a^2*(-7 + k)*m^2*M^2*rh + 960*a^2*(8 - k)*(-7 + k)*m^2*M^2*rh - 
     (768*I)*a*m*M^3*rh + (864*I)*a*(-7 + k)*m*M^3*rh - 
     (960*I)*a*(8 - k)*(-7 + k)*m*M^3*rh - (1728*I)*a^3*m*rh^2 - 
     (3240*I)*a^3*(-7 + k)*m*rh^2 + (1320*I)*a^3*(8 - k)*(-7 + k)*m*rh^2 + 
     (1728*I)*a^3*m^3*rh^2 - 6480*a^2*(-7 + k)*M*rh^2 + 
     3960*a^2*(8 - k)*(-7 + k)*M*rh^2 + 6480*a^2*(-7 + k)*m^2*M*rh^2 - 
     5280*a^2*(8 - k)*(-7 + k)*m^2*M*rh^2 + (1728*I)*a*m*M^2*rh^2 - 
     (6480*I)*a*(-7 + k)*m*M^2*rh^2 + (5280*I)*a*(8 - k)*(-7 + k)*m*M^2*
      rh^2 + 3960*a^2*(-7 + k)*rh^3 - 2640*a^2*(8 - k)*(-7 + k)*rh^3 - 
     7920*a^2*(-7 + k)*m^2*rh^3 + 5280*a^2*(8 - k)*(-7 + k)*m^2*rh^3 + 
     (7920*I)*a*(-7 + k)*m*M*rh^3 - (5280*I)*a*(8 - k)*(-7 + k)*m*M*rh^3 - 
     (336*I)*a^6*\[Kappa] - (240*I)*a^6*(-7 + k)*\[Kappa] + 
     (288*I)*a^6*m^2*\[Kappa] + (288*I)*a^6*(-7 + k)*m^2*\[Kappa] + 
     720*a^5*m*M*\[Kappa] + 576*a^5*(-7 + k)*m*M*\[Kappa] - 
     (432*I)*a^4*M^2*\[Kappa] - (288*I)*a^4*(-7 + k)*M^2*\[Kappa] - 
     2304*a^5*m*rh*\[Kappa] - 1728*a^5*(-7 + k)*m*rh*\[Kappa] + 
     (4224*I)*a^4*M*rh*\[Kappa] + (3456*I)*a^4*(-7 + k)*M*rh*\[Kappa] - 
     (2304*I)*a^4*m^2*M*rh*\[Kappa] - (2592*I)*a^4*(-7 + k)*m^2*M*rh*
      \[Kappa] - 2304*a^3*m*M^2*rh*\[Kappa] - 2304*a^3*(-7 + k)*m*M^2*rh*
      \[Kappa] - (5184*I)*a^4*rh^2*\[Kappa] - (5400*I)*a^4*(-7 + k)*rh^2*
      \[Kappa] + (6912*I)*a^4*m^2*rh^2*\[Kappa] + (8640*I)*a^4*(-7 + k)*m^2*
      rh^2*\[Kappa] + 12096*a^3*m*M*rh^2*\[Kappa] + 
     11232*a^3*(-7 + k)*m*M*rh^2*\[Kappa] - (5184*I)*a^2*M^2*rh^2*\[Kappa] - 
     (4320*I)*a^2*(-7 + k)*M^2*rh^2*\[Kappa] - 8640*a^3*m*rh^3*\[Kappa] - 
     7920*a^3*(-7 + k)*m*rh^3*\[Kappa] + (14400*I)*a^2*M*rh^3*\[Kappa] + 
     (15840*I)*a^2*(-7 + k)*M*rh^3*\[Kappa] - (11520*I)*a^2*m^2*M*rh^3*
      \[Kappa] - (15840*I)*a^2*(-7 + k)*m^2*M*rh^3*\[Kappa] - 
     11520*a*m*M^2*rh^3*\[Kappa] - 11520*a*(-7 + k)*m*M^2*rh^3*\[Kappa] - 
     (7920*I)*a^2*rh^4*\[Kappa] - (11880*I)*a^2*(-7 + k)*rh^4*\[Kappa] + 
     (15840*I)*a^2*m^2*rh^4*\[Kappa] + (23760*I)*a^2*(-7 + k)*m^2*rh^4*
      \[Kappa] + 15840*a*m*M*rh^4*\[Kappa] + 15840*a*(-7 + k)*m*M*rh^4*
      \[Kappa] + (144*I)*a^7*m*\[Kappa]^2 + 144*a^6*M*\[Kappa]^2 - 
     960*a^6*rh*\[Kappa]^2 + 1152*a^6*m^2*rh*\[Kappa]^2 - 
     (1080*I)*a^5*m*M*rh*\[Kappa]^2 + (3456*I)*a^5*m*rh^2*\[Kappa]^2 + 
     3456*a^4*M*rh^2*\[Kappa]^2 - 7200*a^4*rh^3*\[Kappa]^2 + 
     11520*a^4*m^2*rh^3*\[Kappa]^2 - (10656*I)*a^3*m*M*rh^3*\[Kappa]^2 + 
     (7920*I)*a^3*m*rh^4*\[Kappa]^2 + 7920*a^2*M*rh^4*\[Kappa]^2 - 
     9504*a^2*rh^5*\[Kappa]^2 + 19008*a^2*m^2*rh^5*\[Kappa]^2 - 
     (19008*I)*a*m*M*rh^5*\[Kappa]^2 - (160*I)*a^5*m*\[Lambda] + 
     (48*I)*a^5*(-7 + k)*m*\[Lambda] - (48*I)*a^5*(8 - k)*(-7 + k)*m*
      \[Lambda] + (16*I)*a^5*m^3*\[Lambda] + 132*a^4*M*\[Lambda] - 
     8*a^4*(-7 + k)*M*\[Lambda] - 128*a^4*m^2*M*\[Lambda] - 
     (192*I)*a^3*m*M^2*\[Lambda] + (64*I)*a^3*(-7 + k)*m*M^2*\[Lambda] - 
     (64*I)*a^3*(8 - k)*(-7 + k)*m*M^2*\[Lambda] - 512*a^4*rh*\[Lambda] + 
     800*a^4*m^2*rh*\[Lambda] + (2112*I)*a^3*m*M*rh*\[Lambda] - 
     (720*I)*a^3*(-7 + k)*m*M*rh*\[Lambda] + (960*I)*a^3*(8 - k)*(-7 + k)*m*M*
      rh*\[Lambda] - 640*a^2*M^2*rh*\[Lambda] + 72*a^2*(-7 + k)*M^2*rh*
      \[Lambda] - (3168*I)*a^3*m*rh^2*\[Lambda] + (1440*I)*a^3*(-7 + k)*m*
      rh^2*\[Lambda] - (1760*I)*a^3*(8 - k)*(-7 + k)*m*rh^2*\[Lambda] + 
     (288*I)*a^3*m^3*rh^2*\[Lambda] + 3744*a^2*M*rh^2*\[Lambda] - 
     180*a^2*(-7 + k)*M*rh^2*\[Lambda] - 2304*a^2*m^2*M*rh^2*\[Lambda] - 
     (1728*I)*a*m*M^2*rh^2*\[Lambda] - (1760*I)*a*(8 - k)*(-7 + k)*m*M^2*rh^2*
      \[Lambda] - 3600*a^2*rh^3*\[Lambda] + 4080*a^2*m^2*rh^3*\[Lambda] + 
     (6720*I)*a*m*M*rh^3*\[Lambda] - (2640*I)*a*(-7 + k)*m*M*rh^3*\[Lambda] + 
     (7040*I)*a*(8 - k)*(-7 + k)*m*M*rh^3*\[Lambda] - 
     2880*M^2*rh^3*\[Lambda] - (5280*I)*a*m*rh^4*\[Lambda] + 
     (3960*I)*a*(-7 + k)*m*rh^4*\[Lambda] - (5720*I)*a*(8 - k)*(-7 + k)*m*
      rh^4*\[Lambda] + 6600*M*rh^4*\[Lambda] - 3168*rh^5*\[Lambda] - 
     (16*I)*a^6*(-7 + k)*\[Kappa]*\[Lambda] - 48*a^5*m*M*\[Kappa]*\[Lambda] - 
     96*a^5*(-7 + k)*m*M*\[Kappa]*\[Lambda] + 384*a^5*m*rh*\[Kappa]*
      \[Lambda] + 864*a^5*(-7 + k)*m*rh*\[Kappa]*\[Lambda] + 
     (216*I)*a^4*(-7 + k)*M*rh*\[Kappa]*\[Lambda] - 
     (900*I)*a^4*(-7 + k)*rh^2*\[Kappa]*\[Lambda] - 
     1728*a^3*m*M*rh^2*\[Kappa]*\[Lambda] - 4320*a^3*(-7 + k)*m*M*rh^2*
      \[Kappa]*\[Lambda] + 3840*a^3*m*rh^3*\[Kappa]*\[Lambda] + 
     10560*a^3*(-7 + k)*m*rh^3*\[Kappa]*\[Lambda] + 
     (2640*I)*a^2*(-7 + k)*M*rh^3*\[Kappa]*\[Lambda] - 
     (3960*I)*a^2*(-7 + k)*rh^4*\[Kappa]*\[Lambda] - 
     5280*a*m*M*rh^4*\[Kappa]*\[Lambda] - 15840*a*(-7 + k)*m*M*rh^4*\[Kappa]*
      \[Lambda] + 6336*a*m*rh^5*\[Kappa]*\[Lambda] + 
     20592*a*(-7 + k)*m*rh^5*\[Kappa]*\[Lambda] - (32*I)*a^7*m*\[Kappa]^2*
      \[Lambda] - 48*a^6*rh*\[Kappa]^2*\[Lambda] - 
     (1728*I)*a^5*m*rh^2*\[Kappa]^2*\[Lambda] - 720*a^4*rh^3*\[Kappa]^2*
      \[Lambda] - (10560*I)*a^3*m*rh^4*\[Kappa]^2*\[Lambda] - 
     1584*a^2*rh^5*\[Kappa]^2*\[Lambda] - (13728*I)*a*m*rh^6*\[Kappa]^2*
      \[Lambda] - (24*I)*a^5*m*\[Lambda]^2 + 70*a^4*M*\[Lambda]^2 - 
     4*a^4*(-7 + k)*M*\[Lambda]^2 - 304*a^4*rh*\[Lambda]^2 + 
     16*a^4*m^2*rh*\[Lambda]^2 + (256*I)*a^3*m*M*rh*\[Lambda]^2 - 
     320*a^2*M^2*rh*\[Lambda]^2 + 36*a^2*(-7 + k)*M^2*rh*\[Lambda]^2 - 
     (864*I)*a^3*m*rh^2*\[Lambda]^2 + 2160*a^2*M*rh^2*\[Lambda]^2 - 
     90*a^2*(-7 + k)*M*rh^2*\[Lambda]^2 - 2520*a^2*rh^3*\[Lambda]^2 + 
     120*a^2*m^2*rh^3*\[Lambda]^2 + (1920*I)*a*m*M*rh^3*\[Lambda]^2 - 
     1440*M^2*rh^3*\[Lambda]^2 - (2640*I)*a*m*rh^4*\[Lambda]^2 + 
     4620*M*rh^4*\[Lambda]^2 - 3168*rh^5*\[Lambda]^2 - 
     (8*I)*a^6*(-7 + k)*\[Kappa]*\[Lambda]^2 + (108*I)*a^4*(-7 + k)*M*rh*
      \[Kappa]*\[Lambda]^2 - (450*I)*a^4*(-7 + k)*rh^2*\[Kappa]*\[Lambda]^2 + 
     (1320*I)*a^2*(-7 + k)*M*rh^3*\[Kappa]*\[Lambda]^2 - 
     (1980*I)*a^2*(-7 + k)*rh^4*\[Kappa]*\[Lambda]^2 - 
     24*a^6*rh*\[Kappa]^2*\[Lambda]^2 - 360*a^4*rh^3*\[Kappa]^2*\[Lambda]^2 - 
     792*a^2*rh^5*\[Kappa]^2*\[Lambda]^2 + 2*a^4*M*\[Lambda]^3 - 
     24*a^4*rh*\[Lambda]^3 + 144*a^2*M*rh^2*\[Lambda]^3 - 
     360*a^2*rh^3*\[Lambda]^3 + 660*M*rh^4*\[Lambda]^3 - 
     792*rh^5*\[Lambda]^3 - (72*I)*a^5*m*M*rh*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (864*I)*a^3*m*M*rh^3*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - 48*a^3*(-7 + k)*m*M^2*rh*
      (4*\[Kappa] - 5*\[Omega]) + 216*a^3*(-7 + k)*m*M*rh^2*
      (4*\[Kappa] - 5*\[Omega]) - (360*I)*a^6*\[Omega] + 
     (432*I)*a^6*(-7 + k)*\[Omega] - (240*I)*a^6*(8 - k)*(-7 + k)*\[Omega] - 
     (360*I)*a^6*m^2*\[Omega] + 792*a^5*m*M*\[Omega] - 
     1080*a^5*(-7 + k)*m*M*\[Omega] + 720*a^5*(8 - k)*(-7 + k)*m*M*\[Omega] - 
     (1704*I)*a^4*M^2*\[Omega] + (960*I)*a^4*(-7 + k)*M^2*\[Omega] - 
     (576*I)*a^4*(8 - k)*(-7 + k)*M^2*\[Omega] - 2304*a^5*m*rh*\[Omega] + 
     3456*a^5*(-7 + k)*m*rh*\[Omega] - 2640*a^5*(8 - k)*(-7 + k)*m*rh*
      \[Omega] + 1728*a^5*m^3*rh*\[Omega] + (8640*I)*a^4*M*rh*\[Omega] - 
     (6000*I)*a^4*(-7 + k)*M*rh*\[Omega] + (4800*I)*a^4*(8 - k)*(-7 + k)*M*rh*
      \[Omega] + (2304*I)*a^4*m^2*M*rh*\[Omega] - 
     2304*a^3*m*M^2*rh*\[Omega] + 1536*a^3*(-7 + k)*m*M^2*rh*\[Omega] - 
     2880*a^3*(8 - k)*(-7 + k)*m*M^2*rh*\[Omega] + 
     (4608*I)*a^2*M^3*rh*\[Omega] - (1296*I)*a^2*(-7 + k)*M^3*rh*\[Omega] + 
     (960*I)*a^2*(8 - k)*(-7 + k)*M^3*rh*\[Omega] - 
     (6048*I)*a^4*rh^2*\[Omega] + (7128*I)*a^4*(-7 + k)*rh^2*\[Omega] - 
     (6600*I)*a^4*(8 - k)*(-7 + k)*rh^2*\[Omega] - (7776*I)*a^4*m^2*rh^2*
      \[Omega] + 12096*a^3*m*M*rh^2*\[Omega] - 11232*a^3*(-7 + k)*m*M*rh^2*
      \[Omega] + 18480*a^3*(8 - k)*(-7 + k)*m*M*rh^2*\[Omega] - 
     (29376*I)*a^2*M^2*rh^2*\[Omega] + (7560*I)*a^2*(-7 + k)*M^2*rh^2*
      \[Omega] - (10560*I)*a^2*(8 - k)*(-7 + k)*M^2*rh^2*\[Omega] - 
     12960*a^3*m*rh^3*\[Omega] + 15840*a^3*(-7 + k)*m*rh^3*\[Omega] - 
     21120*a^3*(8 - k)*(-7 + k)*m*rh^3*\[Omega] + 
     7200*a^3*m^3*rh^3*\[Omega] + (36000*I)*a^2*M*rh^3*\[Omega] - 
     (13680*I)*a^2*(-7 + k)*M*rh^3*\[Omega] + (26400*I)*a^2*(8 - k)*(-7 + k)*
      M*rh^3*\[Omega] + (4320*I)*a^2*m^2*M*rh^3*\[Omega] - 
     5760*a*m*M^2*rh^3*\[Omega] - 5760*a*(-7 + k)*m*M^2*rh^3*\[Omega] - 
     10560*a*(8 - k)*(-7 + k)*m*M^2*rh^3*\[Omega] + 
     (17280*I)*M^3*rh^3*\[Omega] - (7920*I)*a^2*rh^4*\[Omega] + 
     (7920*I)*a^2*(-7 + k)*rh^4*\[Omega] - (17160*I)*a^2*(8 - k)*(-7 + k)*
      rh^4*\[Omega] - (15840*I)*a^2*m^2*rh^4*\[Omega] + 
     23760*a*m*M*rh^4*\[Omega] + 7920*a*(-7 + k)*m*M*rh^4*\[Omega] + 
     34320*a*(8 - k)*(-7 + k)*m*M*rh^4*\[Omega] - (39600*I)*M^2*rh^4*
      \[Omega] - 19008*a*m*rh^5*\[Omega] - 24024*a*(8 - k)*(-7 + k)*m*rh^5*
      \[Omega] + (19008*I)*M*rh^5*\[Omega] - (576*I)*a^7*m*\[Kappa]*
      \[Omega] - (672*I)*a^7*(-7 + k)*m*\[Kappa]*\[Omega] - 
     864*a^6*M*\[Kappa]*\[Omega] - 960*a^6*(-7 + k)*M*\[Kappa]*\[Omega] + 
     3384*a^6*rh*\[Kappa]*\[Omega] + 4032*a^6*(-7 + k)*rh*\[Kappa]*\[Omega] + 
     (4032*I)*a^5*m*M*rh*\[Kappa]*\[Omega] + (6480*I)*a^5*(-7 + k)*m*M*rh*
      \[Kappa]*\[Omega] + 2304*a^4*M^2*rh*\[Kappa]*\[Omega] + 
     3888*a^4*(-7 + k)*M^2*rh*\[Kappa]*\[Omega] - (13824*I)*a^5*m*rh^2*
      \[Kappa]*\[Omega] - (23760*I)*a^5*(-7 + k)*m*rh^2*\[Kappa]*\[Omega] - 
     14976*a^4*M*rh^2*\[Kappa]*\[Omega] - 31008*a^4*(-7 + k)*M*rh^2*\[Kappa]*
      \[Omega] + 17568*a^4*rh^3*\[Kappa]*\[Omega] + 
     39600*a^4*(-7 + k)*rh^3*\[Kappa]*\[Omega] + (16128*I)*a^3*m*M*rh^3*
      \[Kappa]*\[Omega] + (55440*I)*a^3*(-7 + k)*m*M*rh^3*\[Kappa]*\[Omega] + 
     11520*a^2*M^2*rh^3*\[Kappa]*\[Omega] + 29952*a^2*(-7 + k)*M^2*rh^3*
      \[Kappa]*\[Omega] - (31680*I)*a^3*m*rh^4*\[Kappa]*\[Omega] - 
     (95040*I)*a^3*(-7 + k)*m*rh^4*\[Kappa]*\[Omega] - 
     20160*a^2*M*rh^4*\[Kappa]*\[Omega] - 90720*a^2*(-7 + k)*M*rh^4*\[Kappa]*
      \[Omega] + 11088*a^2*rh^5*\[Kappa]*\[Omega] + 
     61776*a^2*(-7 + k)*rh^5*\[Kappa]*\[Omega] + (61776*I)*a*(-7 + k)*m*M*
      rh^5*\[Kappa]*\[Omega] - (72072*I)*a*(-7 + k)*m*rh^6*\[Kappa]*
      \[Omega] - (240*I)*a^8*\[Kappa]^2*\[Omega] - 2688*a^7*m*rh*\[Kappa]^2*
      \[Omega] + (1440*I)*a^6*M*rh*\[Kappa]^2*\[Omega] - 
     (6624*I)*a^6*rh^2*\[Kappa]^2*\[Omega] - 31680*a^5*m*rh^3*\[Kappa]^2*
      \[Omega] + (15648*I)*a^4*M*rh^3*\[Kappa]^2*\[Omega] - 
     (31536*I)*a^4*rh^4*\[Kappa]^2*\[Omega] - 76032*a^3*m*rh^5*\[Kappa]^2*
      \[Omega] + (28512*I)*a^2*M*rh^5*\[Kappa]^2*\[Omega] - 
     (41184*I)*a^2*rh^6*\[Kappa]^2*\[Omega] - 41184*a*m*rh^7*\[Kappa]^2*
      \[Omega] - 32*a^6*(-7 + k)*rh*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     384*a^4*M*rh^2*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     1920*a^2*M*rh^4*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (336*I)*a^6*rh^2*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (1344*I)*a^4*rh^4*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (16*I)*a^4*(-7 + k)*M*rh*(-3 + \[Lambda])*\[Omega] - 
     (144*I)*a^4*(-7 + k)*rh^2*(-3 + \[Lambda])*\[Omega] + 
     (720*I)*a^2*(-7 + k)*M*rh^3*(-3 + \[Lambda])*\[Omega] - 
     (1320*I)*a^2*(-7 + k)*rh^4*(-3 + \[Lambda])*\[Omega] - 
     24*a^6*rh*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     864*a^4*rh^3*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     2640*a^2*rh^5*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
     (88*I)*a^6*\[Lambda]*\[Omega] - (48*I)*a^6*(-7 + k)*\[Lambda]*\[Omega] + 
     (48*I)*a^6*(8 - k)*(-7 + k)*\[Lambda]*\[Omega] - 
     (64*I)*a^6*m^2*\[Lambda]*\[Omega] + 296*a^5*m*M*\[Lambda]*\[Omega] + 
     (168*I)*a^4*M^2*\[Lambda]*\[Omega] - (64*I)*a^4*(-7 + k)*M^2*\[Lambda]*
      \[Omega] + (64*I)*a^4*(8 - k)*(-7 + k)*M^2*\[Lambda]*\[Omega] - 
     2112*a^5*m*rh*\[Lambda]*\[Omega] - (1056*I)*a^4*M*rh*\[Lambda]*
      \[Omega] + (704*I)*a^4*(-7 + k)*M*rh*\[Lambda]*\[Omega] - 
     (960*I)*a^4*(8 - k)*(-7 + k)*M*rh*\[Lambda]*\[Omega] + 
     (576*I)*a^4*rh^2*\[Lambda]*\[Omega] - (1296*I)*a^4*(-7 + k)*rh^2*
      \[Lambda]*\[Omega] + (1760*I)*a^4*(8 - k)*(-7 + k)*rh^2*\[Lambda]*
      \[Omega] - (2016*I)*a^4*m^2*rh^2*\[Lambda]*\[Omega] + 
     7488*a^3*m*M*rh^2*\[Lambda]*\[Omega] + (1760*I)*a^2*(8 - k)*(-7 + k)*M^2*
      rh^2*\[Lambda]*\[Omega] - 16320*a^3*m*rh^3*\[Lambda]*\[Omega] + 
     (3360*I)*a^2*M*rh^3*\[Lambda]*\[Omega] + (1920*I)*a^2*(-7 + k)*M*rh^3*
      \[Lambda]*\[Omega] - (7040*I)*a^2*(8 - k)*(-7 + k)*M*rh^3*\[Lambda]*
      \[Omega] - (2640*I)*a^2*rh^4*\[Lambda]*\[Omega] - 
     (2640*I)*a^2*(-7 + k)*rh^4*\[Lambda]*\[Omega] + 
     (5720*I)*a^2*(8 - k)*(-7 + k)*rh^4*\[Lambda]*\[Omega] - 
     (5280*I)*a^2*m^2*rh^4*\[Lambda]*\[Omega] + 13200*a*m*M*rh^4*\[Lambda]*
      \[Omega] - (7920*I)*M^2*rh^4*\[Lambda]*\[Omega] - 
     19008*a*m*rh^5*\[Lambda]*\[Omega] + (9504*I)*M*rh^5*\[Lambda]*\[Omega] + 
     48*a^6*M*\[Kappa]*\[Lambda]*\[Omega] + 96*a^6*(-7 + k)*M*\[Kappa]*
      \[Lambda]*\[Omega] - 360*a^6*rh*\[Kappa]*\[Lambda]*\[Omega] - 
     832*a^6*(-7 + k)*rh*\[Kappa]*\[Lambda]*\[Omega] + 
     1344*a^4*M*rh^2*\[Kappa]*\[Lambda]*\[Omega] + 4320*a^4*(-7 + k)*M*rh^2*
      \[Kappa]*\[Lambda]*\[Omega] - 2976*a^4*rh^3*\[Kappa]*\[Lambda]*
      \[Omega] - 10560*a^4*(-7 + k)*rh^3*\[Kappa]*\[Lambda]*\[Omega] + 
     3360*a^2*M*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 15840*a^2*(-7 + k)*M*rh^4*
      \[Kappa]*\[Lambda]*\[Omega] - 3696*a^2*rh^5*\[Kappa]*\[Lambda]*
      \[Omega] - 20592*a^2*(-7 + k)*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
     (32*I)*a^8*\[Kappa]^2*\[Lambda]*\[Omega] + (1392*I)*a^6*rh^2*\[Kappa]^2*
      \[Lambda]*\[Omega] + (9216*I)*a^4*rh^4*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (13728*I)*a^2*rh^6*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (24*I)*a^6*\[Lambda]^2*\[Omega] - 48*a^5*m*rh*\[Lambda]^2*\[Omega] - 
     (256*I)*a^4*M*rh*\[Lambda]^2*\[Omega] + (864*I)*a^4*rh^2*\[Lambda]^2*
      \[Omega] - 720*a^3*m*rh^3*\[Lambda]^2*\[Omega] - 
     (1920*I)*a^2*M*rh^3*\[Lambda]^2*\[Omega] + (2640*I)*a^2*rh^4*\[Lambda]^2*
      \[Omega] - 1584*a*m*rh^5*\[Lambda]^2*\[Omega] + 
     (192*I)*a^4*M*rh^3*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
     24*a^4*(-7 + k)*M*rh^2*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     (288*I)*a^7*m*\[Omega]^2 - 552*a^6*M*\[Omega]^2 + 
     552*a^6*(-7 + k)*M*\[Omega]^2 - 432*a^6*(8 - k)*(-7 + k)*M*\[Omega]^2 + 
     1536*a^6*rh*\[Omega]^2 - 1728*a^6*(-7 + k)*rh*\[Omega]^2 + 
     1680*a^6*(8 - k)*(-7 + k)*rh*\[Omega]^2 - 4416*a^6*m^2*rh*\[Omega]^2 - 
     (2496*I)*a^5*m*M*rh*\[Omega]^2 + 2304*a^4*M^2*rh*\[Omega]^2 - 
     864*a^4*(-7 + k)*M^2*rh*\[Omega]^2 + 1920*a^4*(8 - k)*(-7 + k)*M^2*rh*
      \[Omega]^2 + (5184*I)*a^5*m*rh^2*\[Omega]^2 - 
     10368*a^4*M*rh^2*\[Omega]^2 + 5376*a^4*(-7 + k)*M*rh^2*\[Omega]^2 - 
     13200*a^4*(8 - k)*(-7 + k)*M*rh^2*\[Omega]^2 + 
     8640*a^4*rh^3*\[Omega]^2 - 7920*a^4*(-7 + k)*rh^3*\[Omega]^2 + 
     15840*a^4*(8 - k)*(-7 + k)*rh^3*\[Omega]^2 - 33120*a^4*m^2*rh^3*
      \[Omega]^2 + (2880*I)*a^3*m*M*rh^3*\[Omega]^2 + 
     5760*a^2*M^2*rh^3*\[Omega]^2 + 7488*a^2*(-7 + k)*M^2*rh^3*\[Omega]^2 + 
     10560*a^2*(8 - k)*(-7 + k)*M^2*rh^3*\[Omega]^2 + 
     (7920*I)*a^3*m*rh^4*\[Omega]^2 - 15840*a^2*M*rh^4*\[Omega]^2 - 
     10800*a^2*(-7 + k)*M*rh^4*\[Omega]^2 - 34320*a^2*(8 - k)*(-7 + k)*M*rh^4*
      \[Omega]^2 + 9504*a^2*rh^5*\[Omega]^2 + 24024*a^2*(8 - k)*(-7 + k)*rh^5*
      \[Omega]^2 - 38016*a^2*m^2*rh^5*\[Omega]^2 + (38016*I)*a*m*M*rh^5*
      \[Omega]^2 + (288*I)*a^8*\[Kappa]*\[Omega]^2 + 
     (384*I)*a^8*(-7 + k)*\[Kappa]*\[Omega]^2 - (2304*I)*a^6*M*rh*\[Kappa]*
      \[Omega]^2 - (3888*I)*a^6*(-7 + k)*M*rh*\[Kappa]*\[Omega]^2 + 
     (6912*I)*a^6*rh^2*\[Kappa]*\[Omega]^2 + (15120*I)*a^6*(-7 + k)*rh^2*
      \[Kappa]*\[Omega]^2 - (10752*I)*a^4*M*rh^3*\[Kappa]*\[Omega]^2 - 
     (39600*I)*a^4*(-7 + k)*M*rh^3*\[Kappa]*\[Omega]^2 + 
     (15840*I)*a^4*rh^4*\[Kappa]*\[Omega]^2 + (71280*I)*a^4*(-7 + k)*rh^4*
      \[Kappa]*\[Omega]^2 - (61776*I)*a^2*(-7 + k)*M*rh^5*\[Kappa]*
      \[Omega]^2 + (72072*I)*a^2*(-7 + k)*rh^6*\[Kappa]*\[Omega]^2 + 
     1536*a^8*rh*\[Kappa]^2*\[Omega]^2 + 20160*a^6*rh^3*\[Kappa]^2*
      \[Omega]^2 + 57024*a^4*rh^5*\[Kappa]^2*\[Omega]^2 + 
     41184*a^2*rh^7*\[Kappa]^2*\[Omega]^2 + (80*I)*a^7*m*\[Lambda]*
      \[Omega]^2 - 168*a^6*M*\[Lambda]*\[Omega]^2 + 
     1312*a^6*rh*\[Lambda]*\[Omega]^2 + (3456*I)*a^5*m*rh^2*\[Lambda]*
      \[Omega]^2 - 5184*a^4*M*rh^2*\[Lambda]*\[Omega]^2 + 
     12480*a^4*rh^3*\[Lambda]*\[Omega]^2 + (15840*I)*a^3*m*rh^4*\[Lambda]*
      \[Omega]^2 - 13200*a^2*M*rh^4*\[Lambda]*\[Omega]^2 + 
     22176*a^2*rh^5*\[Lambda]*\[Omega]^2 + (13728*I)*a*m*rh^6*\[Lambda]*
      \[Omega]^2 + 6864*rh^7*\[Lambda]*\[Omega]^2 + 
     32*a^6*rh*\[Lambda]^2*\[Omega]^2 + 720*a^4*rh^3*\[Lambda]^2*\[Omega]^2 + 
     3168*a^2*rh^5*\[Lambda]^2*\[Omega]^2 + 3432*rh^7*\[Lambda]^2*
      \[Omega]^2 - (48*I)*a^8*\[Omega]^3 + 4416*a^7*m*rh*\[Omega]^3 + 
     (768*I)*a^6*M*rh*\[Omega]^3 + (1728*I)*a^6*rh^2*\[Omega]^3 + 
     46080*a^5*m*rh^3*\[Omega]^3 - (8640*I)*a^4*M*rh^3*\[Omega]^3 + 
     (23760*I)*a^4*rh^4*\[Omega]^3 + 95040*a^3*m*rh^5*\[Omega]^3 - 
     (57024*I)*a^2*M*rh^5*\[Omega]^3 + (41184*I)*a^2*rh^6*\[Omega]^3 + 
     41184*a*m*rh^7*\[Omega]^3 - (41184*I)*M*rh^7*\[Omega]^3 - 
     (32*I)*a^8*\[Lambda]*\[Omega]^3 - (1728*I)*a^6*rh^2*\[Lambda]*
      \[Omega]^3 - (10560*I)*a^4*rh^4*\[Lambda]*\[Omega]^3 - 
     (13728*I)*a^2*rh^6*\[Lambda]*\[Omega]^3 - 1536*a^8*rh*\[Omega]^4 - 
     20160*a^6*rh^3*\[Omega]^4 - 57024*a^4*rh^5*\[Omega]^4 - 
     41184*a^2*rh^7*\[Omega]^4 - 48*a^3*(-7 + k)*m*M^2*rh*
      (2*\[Kappa] + \[Omega]) + 432*a^3*(-7 + k)*m*M*rh^2*
      (2*\[Kappa] + \[Omega]) - 2160*a*(-7 + k)*m*M^2*rh^3*
      (2*\[Kappa] + \[Omega]) + 3960*a*(-7 + k)*m*M*rh^4*
      (2*\[Kappa] + \[Omega]) - 48*a^4*(-7 + k)*M*rh^2*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 432*a^2*(-7 + k)*M^2*rh^3*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 1080*a^2*(-7 + k)*M*rh^4*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 12*a^4*(8 - k)*(-7 + k)*M*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     60*a^4*(8 - k)*(-7 + k)*rh*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 80*a^2*(8 - k)*(-7 + k)*M^2*rh*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     660*a^2*(8 - k)*(-7 + k)*M*rh^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 880*a^2*(8 - k)*(-7 + k)*rh^3*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     880*(8 - k)*(-7 + k)*M^2*rh^3*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 2860*(8 - k)*(-7 + k)*M*rh^4*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     2002*(8 - k)*(-7 + k)*rh^5*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 8*a^6*rh*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 360*a^4*rh^3*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     2376*a^2*rh^5*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 3432*rh^7*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - (2*I)*a^4*(-7 + k)*M*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) + (36*I)*a^2*(-7 + k)*M^2*rh*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (180*I)*a^2*(-7 + k)*M*rh^2*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (660*I)*(-7 + k)*M^2*rh^3*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - (990*I)*(-7 + k)*M*rh^4*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     90*a^4*(-7 + k)*rh^2*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 660*a^2*(-7 + k)*M*rh^3*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     1980*a^2*(-7 + k)*rh^4*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 5148*(-7 + k)*M*rh^5*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     6006*(-7 + k)*rh^6*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-7 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  ((-12*a^6 + 336*a^6*(-6 + k) - 120*a^6*(7 - k)*(-6 + k) + 156*a^6*m^2 - 
     288*a^6*(-6 + k)*m^2 + 144*a^6*(7 - k)*(-6 + k)*m^2 - 48*a^6*m^4 - 
     (24*I)*a^5*m*M + (1104*I)*a^5*(-6 + k)*m*M - (432*I)*a^5*(7 - k)*
      (-6 + k)*m*M - (144*I)*a^5*m^3*M - 144*a^4*M^2 + 
     1152*a^4*(-6 + k)*M^2 - 432*a^4*(7 - k)*(-6 + k)*M^2 + 192*a^4*m^2*M^2 - 
     384*a^4*(-6 + k)*m^2*M^2 + 192*a^4*(7 - k)*(-6 + k)*m^2*M^2 - 
     (384*I)*a^3*m*M^3 + (384*I)*a^3*(-6 + k)*m*M^3 - 
     (192*I)*a^3*(7 - k)*(-6 + k)*m*M^3 - (840*I)*a^5*m*rh - 
     (2304*I)*a^5*(-6 + k)*m*rh + (864*I)*a^5*(7 - k)*(-6 + k)*m*rh + 
     (840*I)*a^5*m^3*rh + 504*a^4*M*rh - 5760*a^4*(-6 + k)*M*rh + 
     2592*a^4*(7 - k)*(-6 + k)*M*rh - 1512*a^4*m^2*M*rh + 
     4224*a^4*(-6 + k)*m^2*M*rh - 2592*a^4*(7 - k)*(-6 + k)*m^2*M*rh + 
     (2352*I)*a^3*m*M^2*rh - (5760*I)*a^3*(-6 + k)*m*M^2*rh + 
     (3456*I)*a^3*(7 - k)*(-6 + k)*m*M^2*rh - 1536*a^2*(-6 + k)*M^3*rh + 
     864*a^2*(7 - k)*(-6 + k)*M^3*rh - 336*a^4*rh^2 + 
     5184*a^4*(-6 + k)*rh^2 - 2700*a^4*(7 - k)*(-6 + k)*rh^2 + 
     1680*a^4*m^2*rh^2 - 6912*a^4*(-6 + k)*m^2*rh^2 + 
     4320*a^4*(7 - k)*(-6 + k)*m^2*rh^2 - 672*a^4*m^4*rh^2 + 
     (1344*I)*a^3*m*M*rh^2 + (15552*I)*a^3*(-6 + k)*m*M*rh^2 - 
     (8640*I)*a^3*(7 - k)*(-6 + k)*m*M*rh^2 - (2016*I)*a^3*m^3*M*rh^2 + 
     10368*a^2*(-6 + k)*M^2*rh^2 - 6480*a^2*(7 - k)*(-6 + k)*M^2*rh^2 - 
     3456*a^2*(-6 + k)*m^2*M^2*rh^2 + 4320*a^2*(7 - k)*(-6 + k)*m^2*M^2*
      rh^2 - (2688*I)*a*m*M^3*rh^2 + (3456*I)*a*(-6 + k)*m*M^3*rh^2 - 
     (4320*I)*a*(7 - k)*(-6 + k)*m*M^3*rh^2 - (4032*I)*a^3*m*rh^3 - 
     (8640*I)*a^3*(-6 + k)*m*rh^3 + (3960*I)*a^3*(7 - k)*(-6 + k)*m*rh^3 + 
     (4032*I)*a^3*m^3*rh^3 - 17280*a^2*(-6 + k)*M*rh^3 + 
     11880*a^2*(7 - k)*(-6 + k)*M*rh^3 + 17280*a^2*(-6 + k)*m^2*M*rh^3 - 
     15840*a^2*(7 - k)*(-6 + k)*m^2*M*rh^3 + (4032*I)*a*m*M^2*rh^3 - 
     (17280*I)*a*(-6 + k)*m*M^2*rh^3 + (15840*I)*a*(7 - k)*(-6 + k)*m*M^2*
      rh^3 + 7920*a^2*(-6 + k)*rh^4 - 5940*a^2*(7 - k)*(-6 + k)*rh^4 - 
     15840*a^2*(-6 + k)*m^2*rh^4 + 11880*a^2*(7 - k)*(-6 + k)*m^2*rh^4 + 
     (15840*I)*a*(-6 + k)*m*M*rh^4 - (11880*I)*a*(7 - k)*(-6 + k)*m*M*rh^4 - 
     432*a^7*m*\[Kappa] - 288*a^7*(-6 + k)*m*\[Kappa] + 
     (864*I)*a^6*M*\[Kappa] + (576*I)*a^6*(-6 + k)*M*\[Kappa] - 
     (288*I)*a^6*m^2*M*\[Kappa] - (288*I)*a^6*(-6 + k)*m^2*M*\[Kappa] - 
     288*a^5*m*M^2*\[Kappa] - 288*a^5*(-6 + k)*m*M^2*\[Kappa] - 
     (2352*I)*a^6*rh*\[Kappa] - (1920*I)*a^6*(-6 + k)*rh*\[Kappa] + 
     (2016*I)*a^6*m^2*rh*\[Kappa] + (2304*I)*a^6*(-6 + k)*m^2*rh*\[Kappa] + 
     5040*a^5*m*M*rh*\[Kappa] + 4320*a^5*(-6 + k)*m*M*rh*\[Kappa] - 
     (3024*I)*a^4*M^2*rh*\[Kappa] - (2304*I)*a^4*(-6 + k)*M^2*rh*\[Kappa] - 
     8064*a^5*m*rh^2*\[Kappa] - 6912*a^5*(-6 + k)*m*rh^2*\[Kappa] + 
     (14784*I)*a^4*M*rh^2*\[Kappa] + (13824*I)*a^4*(-6 + k)*M*rh^2*\[Kappa] - 
     (8064*I)*a^4*m^2*M*rh^2*\[Kappa] - (10368*I)*a^4*(-6 + k)*m^2*M*rh^2*
      \[Kappa] - 8064*a^3*m*M^2*rh^2*\[Kappa] - 8064*a^3*(-6 + k)*m*M^2*rh^2*
      \[Kappa] - (12096*I)*a^4*rh^3*\[Kappa] - (14400*I)*a^4*(-6 + k)*rh^3*
      \[Kappa] + (16128*I)*a^4*m^2*rh^3*\[Kappa] + (23040*I)*a^4*(-6 + k)*m^2*
      rh^3*\[Kappa] + 28224*a^3*m*M*rh^3*\[Kappa] + 
     27648*a^3*(-6 + k)*m*M*rh^3*\[Kappa] - (12096*I)*a^2*M^2*rh^3*\[Kappa] - 
     (11520*I)*a^2*(-6 + k)*M^2*rh^3*\[Kappa] - 15120*a^3*m*rh^4*\[Kappa] - 
     15840*a^3*(-6 + k)*m*rh^4*\[Kappa] + (25200*I)*a^2*M*rh^4*\[Kappa] + 
     (31680*I)*a^2*(-6 + k)*M*rh^4*\[Kappa] - (20160*I)*a^2*m^2*M*rh^4*
      \[Kappa] - (31680*I)*a^2*(-6 + k)*m^2*M*rh^4*\[Kappa] - 
     20160*a*m*M^2*rh^4*\[Kappa] - 20160*a*(-6 + k)*m*M^2*rh^4*\[Kappa] - 
     (11088*I)*a^2*rh^5*\[Kappa] - (19008*I)*a^2*(-6 + k)*rh^5*\[Kappa] + 
     (22176*I)*a^2*m^2*rh^5*\[Kappa] + (38016*I)*a^2*(-6 + k)*m^2*rh^5*
      \[Kappa] + 22176*a*m*M*rh^5*\[Kappa] + 22176*a*(-6 + k)*m*M*rh^5*
      \[Kappa] - 120*a^8*\[Kappa]^2 + 96*a^8*m^2*\[Kappa]^2 - 
     (96*I)*a^7*m*M*\[Kappa]^2 + (1008*I)*a^7*m*rh*\[Kappa]^2 + 
     1008*a^6*M*rh*\[Kappa]^2 - 3360*a^6*rh^2*\[Kappa]^2 + 
     4032*a^6*m^2*rh^2*\[Kappa]^2 - (3528*I)*a^5*m*M*rh^2*\[Kappa]^2 + 
     (8064*I)*a^5*m*rh^3*\[Kappa]^2 + 8064*a^4*M*rh^3*\[Kappa]^2 - 
     12600*a^4*rh^4*\[Kappa]^2 + 20160*a^4*m^2*rh^4*\[Kappa]^2 - 
     (18144*I)*a^3*m*M*rh^4*\[Kappa]^2 + (11088*I)*a^3*m*rh^5*\[Kappa]^2 + 
     11088*a^2*M*rh^5*\[Kappa]^2 - 11088*a^2*rh^6*\[Kappa]^2 + 
     22176*a^2*m^2*rh^6*\[Kappa]^2 - (22176*I)*a*m*M*rh^6*\[Kappa]^2 - 
     54*a^6*\[Lambda] + 98*a^6*m^2*\[Lambda] + (296*I)*a^5*m*M*\[Lambda] - 
     (112*I)*a^5*(-6 + k)*m*M*\[Lambda] + (96*I)*a^5*(7 - k)*(-6 + k)*m*M*
      \[Lambda] - 32*a^4*M^2*\[Lambda] + 8*a^4*(-6 + k)*M^2*\[Lambda] - 
     (1120*I)*a^5*m*rh*\[Lambda] + (384*I)*a^5*(-6 + k)*m*rh*\[Lambda] - 
     (432*I)*a^5*(7 - k)*(-6 + k)*m*rh*\[Lambda] + 
     (112*I)*a^5*m^3*rh*\[Lambda] + 924*a^4*M*rh*\[Lambda] - 
     64*a^4*(-6 + k)*M*rh*\[Lambda] - 896*a^4*m^2*M*rh*\[Lambda] - 
     (1344*I)*a^3*m*M^2*rh*\[Lambda] + (512*I)*a^3*(-6 + k)*m*M^2*rh*
      \[Lambda] - (576*I)*a^3*(7 - k)*(-6 + k)*m*M^2*rh*\[Lambda] - 
     1792*a^4*rh^2*\[Lambda] + 2800*a^4*m^2*rh^2*\[Lambda] + 
     (7392*I)*a^3*m*M*rh^2*\[Lambda] - (2880*I)*a^3*(-6 + k)*m*M*rh^2*
      \[Lambda] + (4320*I)*a^3*(7 - k)*(-6 + k)*m*M*rh^2*\[Lambda] - 
     2240*a^2*M^2*rh^2*\[Lambda] + 288*a^2*(-6 + k)*M^2*rh^2*\[Lambda] - 
     (7392*I)*a^3*m*rh^3*\[Lambda] + (3840*I)*a^3*(-6 + k)*m*rh^3*\[Lambda] - 
     (5280*I)*a^3*(7 - k)*(-6 + k)*m*rh^3*\[Lambda] + 
     (672*I)*a^3*m^3*rh^3*\[Lambda] + 8736*a^2*M*rh^3*\[Lambda] - 
     480*a^2*(-6 + k)*M*rh^3*\[Lambda] - 5376*a^2*m^2*M*rh^3*\[Lambda] - 
     (4032*I)*a*m*M^2*rh^3*\[Lambda] - (5280*I)*a*(7 - k)*(-6 + k)*m*M^2*rh^3*
      \[Lambda] - 6300*a^2*rh^4*\[Lambda] + 7140*a^2*m^2*rh^4*\[Lambda] + 
     (11760*I)*a*m*M*rh^4*\[Lambda] - (5280*I)*a*(-6 + k)*m*M*rh^4*
      \[Lambda] + (15840*I)*a*(7 - k)*(-6 + k)*m*M*rh^4*\[Lambda] - 
     5040*M^2*rh^4*\[Lambda] - (7392*I)*a*m*rh^5*\[Lambda] + 
     (6336*I)*a*(-6 + k)*m*rh^5*\[Lambda] - (10296*I)*a*(7 - k)*(-6 + k)*m*
      rh^5*\[Lambda] + 9240*M*rh^5*\[Lambda] - 3696*rh^6*\[Lambda] + 
     32*a^7*m*\[Kappa]*\[Lambda] + 64*a^7*(-6 + k)*m*\[Kappa]*\[Lambda] + 
     (8*I)*a^6*(-6 + k)*M*\[Kappa]*\[Lambda] - (128*I)*a^6*(-6 + k)*rh*
      \[Kappa]*\[Lambda] - 336*a^5*m*M*rh*\[Kappa]*\[Lambda] - 
     768*a^5*(-6 + k)*m*M*rh*\[Kappa]*\[Lambda] + 1344*a^5*m*rh^2*\[Kappa]*
      \[Lambda] + 3456*a^5*(-6 + k)*m*rh^2*\[Kappa]*\[Lambda] + 
     (864*I)*a^4*(-6 + k)*M*rh^2*\[Kappa]*\[Lambda] - 
     (2400*I)*a^4*(-6 + k)*rh^3*\[Kappa]*\[Lambda] - 
     4032*a^3*m*M*rh^3*\[Kappa]*\[Lambda] - 11520*a^3*(-6 + k)*m*M*rh^3*
      \[Kappa]*\[Lambda] + 6720*a^3*m*rh^4*\[Kappa]*\[Lambda] + 
     21120*a^3*(-6 + k)*m*rh^4*\[Kappa]*\[Lambda] + 
     (5280*I)*a^2*(-6 + k)*M*rh^4*\[Kappa]*\[Lambda] - 
     (6336*I)*a^2*(-6 + k)*rh^5*\[Kappa]*\[Lambda] - 
     7392*a*m*M*rh^5*\[Kappa]*\[Lambda] - 25344*a*(-6 + k)*m*M*rh^5*\[Kappa]*
      \[Lambda] + 7392*a*m*rh^6*\[Kappa]*\[Lambda] + 
     27456*a*(-6 + k)*m*rh^6*\[Kappa]*\[Lambda] - 
     2*a^8*\[Kappa]^2*\[Lambda] - (224*I)*a^7*m*rh*\[Kappa]^2*\[Lambda] - 
     168*a^6*rh^2*\[Kappa]^2*\[Lambda] - (4032*I)*a^5*m*rh^3*\[Kappa]^2*
      \[Lambda] - 1260*a^4*rh^4*\[Kappa]^2*\[Lambda] - 
     (14784*I)*a^3*m*rh^5*\[Kappa]^2*\[Lambda] - 1848*a^2*rh^6*\[Kappa]^2*
      \[Lambda] - (13728*I)*a*m*rh^7*\[Kappa]^2*\[Lambda] - 
     29*a^6*\[Lambda]^2 + a^6*m^2*\[Lambda]^2 + (16*I)*a^5*m*M*\[Lambda]^2 - 
     16*a^4*M^2*\[Lambda]^2 + 4*a^4*(-6 + k)*M^2*\[Lambda]^2 - 
     (168*I)*a^5*m*rh*\[Lambda]^2 + 490*a^4*M*rh*\[Lambda]^2 - 
     32*a^4*(-6 + k)*M*rh*\[Lambda]^2 - 1064*a^4*rh^2*\[Lambda]^2 + 
     56*a^4*m^2*rh^2*\[Lambda]^2 + (896*I)*a^3*m*M*rh^2*\[Lambda]^2 - 
     1120*a^2*M^2*rh^2*\[Lambda]^2 + 144*a^2*(-6 + k)*M^2*rh^2*\[Lambda]^2 - 
     (2016*I)*a^3*m*rh^3*\[Lambda]^2 + 5040*a^2*M*rh^3*\[Lambda]^2 - 
     240*a^2*(-6 + k)*M*rh^3*\[Lambda]^2 - 4410*a^2*rh^4*\[Lambda]^2 + 
     210*a^2*m^2*rh^4*\[Lambda]^2 + (3360*I)*a*m*M*rh^4*\[Lambda]^2 - 
     2520*M^2*rh^4*\[Lambda]^2 - (3696*I)*a*m*rh^5*\[Lambda]^2 + 
     6468*M*rh^5*\[Lambda]^2 - 3696*rh^6*\[Lambda]^2 + 
     (4*I)*a^6*(-6 + k)*M*\[Kappa]*\[Lambda]^2 - (64*I)*a^6*(-6 + k)*rh*
      \[Kappa]*\[Lambda]^2 + (432*I)*a^4*(-6 + k)*M*rh^2*\[Kappa]*
      \[Lambda]^2 - (1200*I)*a^4*(-6 + k)*rh^3*\[Kappa]*\[Lambda]^2 + 
     (2640*I)*a^2*(-6 + k)*M*rh^4*\[Kappa]*\[Lambda]^2 - 
     (3168*I)*a^2*(-6 + k)*rh^5*\[Kappa]*\[Lambda]^2 - 
     a^8*\[Kappa]^2*\[Lambda]^2 - 84*a^6*rh^2*\[Kappa]^2*\[Lambda]^2 - 
     630*a^4*rh^4*\[Kappa]^2*\[Lambda]^2 - 924*a^2*rh^6*\[Kappa]^2*
      \[Lambda]^2 - a^6*\[Lambda]^3 + 14*a^4*M*rh*\[Lambda]^3 - 
     84*a^4*rh^2*\[Lambda]^3 + 336*a^2*M*rh^3*\[Lambda]^3 - 
     630*a^2*rh^4*\[Lambda]^3 + 924*M*rh^5*\[Lambda]^3 - 
     924*rh^6*\[Lambda]^3 - (504*I)*a^5*m*M*rh^2*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (2016*I)*a^3*m*M*rh^4*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) + 48*a^5*(-6 + k)*m*M*rh*
      (\[Kappa] - 6*\[Omega]) + 48*a^5*(-6 + k)*m*M*rh*
      (4*\[Kappa] - 5*\[Omega]) - 384*a^3*(-6 + k)*m*M^2*rh^2*
      (4*\[Kappa] - 5*\[Omega]) + 864*a^3*(-6 + k)*m*M*rh^3*
      (4*\[Kappa] - 5*\[Omega]) - 420*a^7*m*\[Omega] + 
     576*a^7*(-6 + k)*m*\[Omega] - 336*a^7*(7 - k)*(-6 + k)*m*\[Omega] + 
     252*a^7*m^3*\[Omega] + (1284*I)*a^6*M*\[Omega] - 
     (1440*I)*a^6*(-6 + k)*M*\[Omega] + (720*I)*a^6*(7 - k)*(-6 + k)*M*
      \[Omega] + (468*I)*a^6*m^2*M*\[Omega] - 480*a^5*m*M^2*\[Omega] + 
     816*a^5*(-6 + k)*m*M^2*\[Omega] - 432*a^5*(7 - k)*(-6 + k)*m*M^2*
      \[Omega] + (576*I)*a^4*M^3*\[Omega] - (432*I)*a^4*(-6 + k)*M^3*
      \[Omega] + (192*I)*a^4*(7 - k)*(-6 + k)*M^3*\[Omega] - 
     (2520*I)*a^6*rh*\[Omega] + (3432*I)*a^6*(-6 + k)*rh*\[Omega] - 
     (2160*I)*a^6*(7 - k)*(-6 + k)*rh*\[Omega] - (2520*I)*a^6*m^2*rh*
      \[Omega] + 5544*a^5*m*M*rh*\[Omega] - 8136*a^5*(-6 + k)*m*M*rh*
      \[Omega] + 6480*a^5*(7 - k)*(-6 + k)*m*M*rh*\[Omega] - 
     (11928*I)*a^4*M^2*rh*\[Omega] + (7680*I)*a^4*(-6 + k)*M^2*rh*\[Omega] - 
     (5184*I)*a^4*(7 - k)*(-6 + k)*M^2*rh*\[Omega] - 
     8064*a^5*m*rh^2*\[Omega] + 13824*a^5*(-6 + k)*m*rh^2*\[Omega] - 
     11880*a^5*(7 - k)*(-6 + k)*m*rh^2*\[Omega] + 
     6048*a^5*m^3*rh^2*\[Omega] + (30240*I)*a^4*M*rh^2*\[Omega] - 
     (23808*I)*a^4*(-6 + k)*M*rh^2*\[Omega] + (21600*I)*a^4*(7 - k)*(-6 + k)*
      M*rh^2*\[Omega] + (8064*I)*a^4*m^2*M*rh^2*\[Omega] - 
     8064*a^3*m*M^2*rh^2*\[Omega] + 5376*a^3*(-6 + k)*m*M^2*rh^2*\[Omega] - 
     12960*a^3*(7 - k)*(-6 + k)*m*M^2*rh^2*\[Omega] + 
     (16128*I)*a^2*M^3*rh^2*\[Omega] - (5184*I)*a^2*(-6 + k)*M^3*rh^2*
      \[Omega] + (4320*I)*a^2*(7 - k)*(-6 + k)*M^3*rh^2*\[Omega] - 
     (14112*I)*a^4*rh^3*\[Omega] + (18432*I)*a^4*(-6 + k)*rh^3*\[Omega] - 
     (19800*I)*a^4*(7 - k)*(-6 + k)*rh^3*\[Omega] - 
     (18144*I)*a^4*m^2*rh^3*\[Omega] + 28224*a^3*m*M*rh^3*\[Omega] - 
     29088*a^3*(-6 + k)*m*M*rh^3*\[Omega] + 55440*a^3*(7 - k)*(-6 + k)*m*M*
      rh^3*\[Omega] - (68544*I)*a^2*M^2*rh^3*\[Omega] + 
     (20160*I)*a^2*(-6 + k)*M^2*rh^3*\[Omega] - (31680*I)*a^2*(7 - k)*
      (-6 + k)*M^2*rh^3*\[Omega] - 22680*a^3*m*rh^4*\[Omega] + 
     31680*a^3*(-6 + k)*m*rh^4*\[Omega] - 47520*a^3*(7 - k)*(-6 + k)*m*rh^4*
      \[Omega] + 12600*a^3*m^3*rh^4*\[Omega] + (63000*I)*a^2*M*rh^4*
      \[Omega] - (25920*I)*a^2*(-6 + k)*M*rh^4*\[Omega] + 
     (59400*I)*a^2*(7 - k)*(-6 + k)*M*rh^4*\[Omega] + 
     (7560*I)*a^2*m^2*M*rh^4*\[Omega] - 10080*a*m*M^2*rh^4*\[Omega] - 
     10080*a*(-6 + k)*m*M^2*rh^4*\[Omega] - 23760*a*(7 - k)*(-6 + k)*m*M^2*
      rh^4*\[Omega] + (30240*I)*M^3*rh^4*\[Omega] - 
     (11088*I)*a^2*rh^5*\[Omega] + (11088*I)*a^2*(-6 + k)*rh^5*\[Omega] - 
     (30888*I)*a^2*(7 - k)*(-6 + k)*rh^5*\[Omega] - 
     (22176*I)*a^2*m^2*rh^5*\[Omega] + 33264*a*m*M*rh^5*\[Omega] + 
     11088*a*(-6 + k)*m*M*rh^5*\[Omega] + 61776*a*(7 - k)*(-6 + k)*m*M*rh^5*
      \[Omega] - (55440*I)*M^2*rh^5*\[Omega] - 22176*a*m*rh^6*\[Omega] - 
     36036*a*(7 - k)*(-6 + k)*m*rh^6*\[Omega] + (22176*I)*M*rh^6*\[Omega] + 
     528*a^8*\[Kappa]*\[Omega] + 480*a^8*(-6 + k)*\[Kappa]*\[Omega] + 
     (576*I)*a^7*m*M*\[Kappa]*\[Omega] + (624*I)*a^7*(-6 + k)*m*M*\[Kappa]*
      \[Omega] + 288*a^6*M^2*\[Kappa]*\[Omega] + 336*a^6*(-6 + k)*M^2*
      \[Kappa]*\[Omega] - (4032*I)*a^7*m*rh*\[Kappa]*\[Omega] - 
     (5376*I)*a^7*(-6 + k)*m*rh*\[Kappa]*\[Omega] - 
     5760*a^6*M*rh*\[Kappa]*\[Omega] - 7104*a^6*(-6 + k)*M*rh*\[Kappa]*
      \[Omega] + 11592*a^6*rh^2*\[Kappa]*\[Omega] + 
     15264*a^6*(-6 + k)*rh^2*\[Kappa]*\[Omega] + (12096*I)*a^5*m*M*rh^2*
      \[Kappa]*\[Omega] + (25920*I)*a^5*(-6 + k)*m*M*rh^2*\[Kappa]*\[Omega] + 
     8064*a^4*M^2*rh^2*\[Kappa]*\[Omega] + 15120*a^4*(-6 + k)*M^2*rh^2*
      \[Kappa]*\[Omega] - (32256*I)*a^5*m*rh^3*\[Kappa]*\[Omega] - 
     (63360*I)*a^5*(-6 + k)*m*rh^3*\[Kappa]*\[Omega] - 
     32256*a^4*M*rh^3*\[Kappa]*\[Omega] - 81024*a^4*(-6 + k)*M*rh^3*\[Kappa]*
      \[Omega] + 29232*a^4*rh^4*\[Kappa]*\[Omega] + 
     79200*a^4*(-6 + k)*rh^4*\[Kappa]*\[Omega] + (24192*I)*a^3*m*M*rh^4*
      \[Kappa]*\[Omega] + (110880*I)*a^3*(-6 + k)*m*M*rh^4*\[Kappa]*
      \[Omega] + 20160*a^2*M^2*rh^4*\[Kappa]*\[Omega] + 
     56448*a^2*(-6 + k)*M^2*rh^4*\[Kappa]*\[Omega] - 
     (44352*I)*a^3*m*rh^5*\[Kappa]*\[Omega] - (152064*I)*a^3*(-6 + k)*m*rh^5*
      \[Kappa]*\[Omega] - 24192*a^2*M*rh^5*\[Kappa]*\[Omega] - 
     140544*a^2*(-6 + k)*M*rh^5*\[Kappa]*\[Omega] + 
     11088*a^2*rh^6*\[Kappa]*\[Omega] + 82368*a^2*(-6 + k)*rh^6*\[Kappa]*
      \[Omega] + (82368*I)*a*(-6 + k)*m*M*rh^6*\[Kappa]*\[Omega] - 
     (82368*I)*a*(-6 + k)*m*rh^7*\[Kappa]*\[Omega] - 
     204*a^9*m*\[Kappa]^2*\[Omega] + (108*I)*a^8*M*\[Kappa]^2*\[Omega] - 
     (1392*I)*a^8*rh*\[Kappa]^2*\[Omega] - 9408*a^7*m*rh^2*\[Kappa]^2*
      \[Omega] + (4968*I)*a^6*M*rh^2*\[Kappa]^2*\[Omega] - 
     (14112*I)*a^6*rh^3*\[Kappa]^2*\[Omega] - 55440*a^5*m*rh^4*\[Kappa]^2*
      \[Omega] + (27048*I)*a^4*M*rh^4*\[Kappa]^2*\[Omega] - 
     (43344*I)*a^4*rh^5*\[Kappa]^2*\[Omega] - 88704*a^3*m*rh^6*\[Kappa]^2*
      \[Omega] + (33264*I)*a^2*M*rh^6*\[Kappa]^2*\[Omega] - 
     (41184*I)*a^2*rh^7*\[Kappa]^2*\[Omega] - 36036*a*m*rh^8*\[Kappa]^2*
      \[Omega] + 64*a^6*(-6 + k)*M*rh*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     224*a^6*(-6 + k)*rh^2*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     48*a^6*M*rh*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     1344*a^4*M*rh^3*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     3360*a^2*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (48*I)*a^8*rh*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (1008*I)*a^6*rh^3*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (2016*I)*a^4*rh^5*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] - 
     (8*I)*a^6*(-6 + k)*rh*(-3 + \[Lambda])*\[Omega] + 
     (128*I)*a^4*(-6 + k)*M*rh^2*(-3 + \[Lambda])*\[Omega] - 
     (576*I)*a^4*(-6 + k)*rh^3*(-3 + \[Lambda])*\[Omega] + 
     (1920*I)*a^2*(-6 + k)*M*rh^4*(-3 + \[Lambda])*\[Omega] - 
     (2640*I)*a^2*(-6 + k)*rh^5*(-3 + \[Lambda])*\[Omega] - 
     168*a^6*rh^2*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     2016*a^4*rh^4*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     3696*a^2*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     216*a^7*m*\[Lambda]*\[Omega] - (236*I)*a^6*M*\[Lambda]*\[Omega] + 
     (112*I)*a^6*(-6 + k)*M*\[Lambda]*\[Omega] - (96*I)*a^6*(7 - k)*(-6 + k)*
      M*\[Lambda]*\[Omega] + (616*I)*a^6*rh*\[Lambda]*\[Omega] - 
     (376*I)*a^6*(-6 + k)*rh*\[Lambda]*\[Omega] + (432*I)*a^6*(7 - k)*
      (-6 + k)*rh*\[Lambda]*\[Omega] - (448*I)*a^6*m^2*rh*\[Lambda]*
      \[Omega] + 2072*a^5*m*M*rh*\[Lambda]*\[Omega] + 
     (1176*I)*a^4*M^2*rh*\[Lambda]*\[Omega] - (512*I)*a^4*(-6 + k)*M^2*rh*
      \[Lambda]*\[Omega] + (576*I)*a^4*(7 - k)*(-6 + k)*M^2*rh*\[Lambda]*
      \[Omega] - 7392*a^5*m*rh^2*\[Lambda]*\[Omega] - 
     (3696*I)*a^4*M*rh^2*\[Lambda]*\[Omega] + (2752*I)*a^4*(-6 + k)*M*rh^2*
      \[Lambda]*\[Omega] - (4320*I)*a^4*(7 - k)*(-6 + k)*M*rh^2*\[Lambda]*
      \[Omega] + (1344*I)*a^4*rh^3*\[Lambda]*\[Omega] - 
     (3264*I)*a^4*(-6 + k)*rh^3*\[Lambda]*\[Omega] + 
     (5280*I)*a^4*(7 - k)*(-6 + k)*rh^3*\[Lambda]*\[Omega] - 
     (4704*I)*a^4*m^2*rh^3*\[Lambda]*\[Omega] + 17472*a^3*m*M*rh^3*\[Lambda]*
      \[Omega] + (5280*I)*a^2*(7 - k)*(-6 + k)*M^2*rh^3*\[Lambda]*\[Omega] - 
     28560*a^3*m*rh^4*\[Lambda]*\[Omega] + (5880*I)*a^2*M*rh^4*\[Lambda]*
      \[Omega] + (3360*I)*a^2*(-6 + k)*M*rh^4*\[Lambda]*\[Omega] - 
     (15840*I)*a^2*(7 - k)*(-6 + k)*M*rh^4*\[Lambda]*\[Omega] - 
     (3696*I)*a^2*rh^5*\[Lambda]*\[Omega] - (3696*I)*a^2*(-6 + k)*rh^5*
      \[Lambda]*\[Omega] + (10296*I)*a^2*(7 - k)*(-6 + k)*rh^5*\[Lambda]*
      \[Omega] - (7392*I)*a^2*m^2*rh^5*\[Lambda]*\[Omega] + 
     18480*a*m*M*rh^5*\[Lambda]*\[Omega] - (11088*I)*M^2*rh^5*\[Lambda]*
      \[Omega] - 22176*a*m*rh^6*\[Lambda]*\[Omega] + 
     (11088*I)*M*rh^6*\[Lambda]*\[Omega] - 32*a^8*\[Kappa]*\[Lambda]*
      \[Omega] - 64*a^8*(-6 + k)*\[Kappa]*\[Lambda]*\[Omega] + 
     288*a^6*M*rh*\[Kappa]*\[Lambda]*\[Omega] + 704*a^6*(-6 + k)*M*rh*
      \[Kappa]*\[Lambda]*\[Omega] - 1176*a^6*rh^2*\[Kappa]*\[Lambda]*
      \[Omega] - 3232*a^6*(-6 + k)*rh^2*\[Kappa]*\[Lambda]*\[Omega] + 
     2688*a^4*M*rh^3*\[Kappa]*\[Lambda]*\[Omega] + 11520*a^4*(-6 + k)*M*rh^3*
      \[Kappa]*\[Lambda]*\[Omega] - 4704*a^4*rh^4*\[Kappa]*\[Lambda]*
      \[Omega] - 21120*a^4*(-6 + k)*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 
     4032*a^2*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 25344*a^2*(-6 + k)*M*rh^5*
      \[Kappa]*\[Lambda]*\[Omega] - 3696*a^2*rh^6*\[Kappa]*\[Lambda]*
      \[Omega] - 27456*a^2*(-6 + k)*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 
     (176*I)*a^8*rh*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (3024*I)*a^6*rh^3*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (12768*I)*a^4*rh^5*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (13728*I)*a^2*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
     2*a^7*m*\[Lambda]^2*\[Omega] - (16*I)*a^6*M*\[Lambda]^2*\[Omega] + 
     (168*I)*a^6*rh*\[Lambda]^2*\[Omega] - 168*a^5*m*rh^2*\[Lambda]^2*
      \[Omega] - (896*I)*a^4*M*rh^2*\[Lambda]^2*\[Omega] + 
     (2016*I)*a^4*rh^3*\[Lambda]^2*\[Omega] - 1260*a^3*m*rh^4*\[Lambda]^2*
      \[Omega] - (3360*I)*a^2*M*rh^4*\[Lambda]^2*\[Omega] + 
     (3696*I)*a^2*rh^5*\[Lambda]^2*\[Omega] - 1848*a*m*rh^6*\[Lambda]^2*
      \[Omega] + (72*I)*a^6*M*rh^2*\[Kappa]*(\[Kappa] - 4*\[Omega])*
      \[Omega] + (672*I)*a^4*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*
      \[Omega] + 48*a^4*(-6 + k)*M^2*rh^2*(5*\[Kappa] - 3*\[Omega])*
      \[Omega] - 192*a^4*(-6 + k)*M*rh^3*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     252*a^8*\[Omega]^2 - 288*a^8*(-6 + k)*\[Omega]^2 + 
     192*a^8*(7 - k)*(-6 + k)*\[Omega]^2 - 468*a^8*m^2*\[Omega]^2 - 
     (504*I)*a^7*m*M*\[Omega]^2 + 288*a^6*M^2*\[Omega]^2 - 
     432*a^6*(-6 + k)*M^2*\[Omega]^2 + 240*a^6*(7 - k)*(-6 + k)*M^2*
      \[Omega]^2 + (2016*I)*a^7*m*rh*\[Omega]^2 - 3864*a^6*M*rh*\[Omega]^2 + 
     4416*a^6*(-6 + k)*M*rh*\[Omega]^2 - 3888*a^6*(7 - k)*(-6 + k)*M*rh*
      \[Omega]^2 + 5376*a^6*rh^2*\[Omega]^2 - 6912*a^6*(-6 + k)*rh^2*
      \[Omega]^2 + 7560*a^6*(7 - k)*(-6 + k)*rh^2*\[Omega]^2 - 
     15456*a^6*m^2*rh^2*\[Omega]^2 - (8736*I)*a^5*m*M*rh^2*\[Omega]^2 + 
     8064*a^4*M^2*rh^2*\[Omega]^2 - 3360*a^4*(-6 + k)*M^2*rh^2*\[Omega]^2 + 
     8640*a^4*(7 - k)*(-6 + k)*M^2*rh^2*\[Omega]^2 + 
     (12096*I)*a^5*m*rh^3*\[Omega]^2 - 24192*a^4*M*rh^3*\[Omega]^2 + 
     14208*a^4*(-6 + k)*M*rh^3*\[Omega]^2 - 39600*a^4*(7 - k)*(-6 + k)*M*rh^3*
      \[Omega]^2 + 15120*a^4*rh^4*\[Omega]^2 - 15840*a^4*(-6 + k)*rh^4*
      \[Omega]^2 + 35640*a^4*(7 - k)*(-6 + k)*rh^4*\[Omega]^2 - 
     57960*a^4*m^2*rh^4*\[Omega]^2 + (5040*I)*a^3*m*M*rh^4*\[Omega]^2 + 
     10080*a^2*M^2*rh^4*\[Omega]^2 + 14112*a^2*(-6 + k)*M^2*rh^4*\[Omega]^2 + 
     23760*a^2*(7 - k)*(-6 + k)*M^2*rh^4*\[Omega]^2 + 
     (11088*I)*a^3*m*rh^5*\[Omega]^2 - 22176*a^2*M*rh^5*\[Omega]^2 - 
     16128*a^2*(-6 + k)*M*rh^5*\[Omega]^2 - 61776*a^2*(7 - k)*(-6 + k)*M*rh^5*
      \[Omega]^2 + 11088*a^2*rh^6*\[Omega]^2 + 36036*a^2*(7 - k)*(-6 + k)*
      rh^6*\[Omega]^2 - 44352*a^2*m^2*rh^6*\[Omega]^2 + 
     (44352*I)*a*m*M*rh^6*\[Omega]^2 - (288*I)*a^8*M*\[Kappa]*\[Omega]^2 - 
     (336*I)*a^8*(-6 + k)*M*\[Kappa]*\[Omega]^2 + (2016*I)*a^8*rh*\[Kappa]*
      \[Omega]^2 + (3072*I)*a^8*(-6 + k)*rh*\[Kappa]*\[Omega]^2 - 
     (7776*I)*a^6*M*rh^2*\[Kappa]*\[Omega]^2 - (15552*I)*a^6*(-6 + k)*M*rh^2*
      \[Kappa]*\[Omega]^2 + (16128*I)*a^6*rh^3*\[Kappa]*\[Omega]^2 + 
     (40320*I)*a^6*(-6 + k)*rh^3*\[Kappa]*\[Omega]^2 - 
     (17472*I)*a^4*M*rh^4*\[Kappa]*\[Omega]^2 - (79200*I)*a^4*(-6 + k)*M*rh^4*
      \[Kappa]*\[Omega]^2 + (22176*I)*a^4*rh^5*\[Kappa]*\[Omega]^2 + 
     (114048*I)*a^4*(-6 + k)*rh^5*\[Kappa]*\[Omega]^2 - 
     (82368*I)*a^2*(-6 + k)*M*rh^6*\[Kappa]*\[Omega]^2 + 
     (82368*I)*a^2*(-6 + k)*rh^7*\[Kappa]*\[Omega]^2 + 
     108*a^10*\[Kappa]^2*\[Omega]^2 + 5376*a^8*rh^2*\[Kappa]^2*\[Omega]^2 + 
     35280*a^6*rh^4*\[Kappa]^2*\[Omega]^2 + 66528*a^4*rh^6*\[Kappa]^2*
      \[Omega]^2 + 36036*a^2*rh^8*\[Kappa]^2*\[Omega]^2 + 
     118*a^8*\[Lambda]*\[Omega]^2 + (560*I)*a^7*m*rh*\[Lambda]*\[Omega]^2 - 
     1176*a^6*M*rh*\[Lambda]*\[Omega]^2 + 4592*a^6*rh^2*\[Lambda]*
      \[Omega]^2 + (8064*I)*a^5*m*rh^3*\[Lambda]*\[Omega]^2 - 
     12096*a^4*M*rh^3*\[Lambda]*\[Omega]^2 + 21840*a^4*rh^4*\[Lambda]*
      \[Omega]^2 + (22176*I)*a^3*m*rh^5*\[Lambda]*\[Omega]^2 - 
     18480*a^2*M*rh^5*\[Lambda]*\[Omega]^2 + 25872*a^2*rh^6*\[Lambda]*
      \[Omega]^2 + (13728*I)*a*m*rh^7*\[Lambda]*\[Omega]^2 + 
     6006*rh^8*\[Lambda]*\[Omega]^2 + a^8*\[Lambda]^2*\[Omega]^2 + 
     112*a^6*rh^2*\[Lambda]^2*\[Omega]^2 + 1260*a^4*rh^4*\[Lambda]^2*
      \[Omega]^2 + 3696*a^2*rh^6*\[Lambda]^2*\[Omega]^2 + 
     3003*rh^8*\[Lambda]^2*\[Omega]^2 + 372*a^9*m*\[Omega]^3 + 
     (180*I)*a^8*M*\[Omega]^3 - (336*I)*a^8*rh*\[Omega]^3 + 
     15456*a^7*m*rh^2*\[Omega]^3 + (2688*I)*a^6*M*rh^2*\[Omega]^3 + 
     (4032*I)*a^6*rh^3*\[Omega]^3 + 80640*a^5*m*rh^4*\[Omega]^3 - 
     (15120*I)*a^4*M*rh^4*\[Omega]^3 + (33264*I)*a^4*rh^5*\[Omega]^3 + 
     110880*a^3*m*rh^6*\[Omega]^3 - (66528*I)*a^2*M*rh^6*\[Omega]^3 + 
     (41184*I)*a^2*rh^7*\[Omega]^3 + 36036*a*m*rh^8*\[Omega]^3 - 
     (36036*I)*M*rh^8*\[Omega]^3 - (224*I)*a^8*rh*\[Lambda]*\[Omega]^3 - 
     (4032*I)*a^6*rh^3*\[Lambda]*\[Omega]^3 - (14784*I)*a^4*rh^5*\[Lambda]*
      \[Omega]^3 - (13728*I)*a^2*rh^7*\[Lambda]*\[Omega]^3 - 
     108*a^10*\[Omega]^4 - 5376*a^8*rh^2*\[Omega]^4 - 
     35280*a^6*rh^4*\[Omega]^4 - 66528*a^4*rh^6*\[Omega]^4 - 
     36036*a^2*rh^8*\[Omega]^4 + 24*a^5*(-6 + k)*m*M*rh*
      (2*\[Kappa] + \[Omega]) - 384*a^3*(-6 + k)*m*M^2*rh^2*
      (2*\[Kappa] + \[Omega]) + 1728*a^3*(-6 + k)*m*M*rh^3*
      (2*\[Kappa] + \[Omega]) - 5760*a*(-6 + k)*m*M^2*rh^4*
      (2*\[Kappa] + \[Omega]) + 7920*a*(-6 + k)*m*M*rh^5*
      (2*\[Kappa] + \[Omega]) + 48*a^4*(-6 + k)*M^2*rh^2*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 384*a^4*(-6 + k)*M*rh^3*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 1728*a^2*(-6 + k)*M^2*rh^4*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 2880*a^2*(-6 + k)*M*rh^5*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 4*a^6*(7 - k)*(-6 + k)*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     4*a^4*(7 - k)*(-6 + k)*M^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 108*a^4*(7 - k)*(-6 + k)*M*rh*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     270*a^4*(7 - k)*(-6 + k)*rh^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 360*a^2*(7 - k)*(-6 + k)*M^2*rh^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     1980*a^2*(7 - k)*(-6 + k)*M*rh^3*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1980*a^2*(7 - k)*(-6 + k)*rh^4*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     1980*(7 - k)*(-6 + k)*M^2*rh^4*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 5148*(7 - k)*(-6 + k)*M*rh^5*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3003*(7 - k)*(-6 + k)*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 28*a^6*rh^2*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     630*a^4*rh^4*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 2772*a^2*rh^6*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3003*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     (16*I)*a^4*(-6 + k)*M*rh*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (144*I)*a^2*(-6 + k)*M^2*rh^2*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (480*I)*a^2*(-6 + k)*M*rh^3*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (1320*I)*(-6 + k)*M^2*rh^4*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - (1584*I)*(-6 + k)*M*rh^5*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     240*a^4*(-6 + k)*rh^3*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 1320*a^2*(-6 + k)*M*rh^4*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     3168*a^2*(-6 + k)*rh^5*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 6864*(-6 + k)*M*rh^6*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     6864*(-6 + k)*rh^7*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-6 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  (((-96*I)*a^7*m - (432*I)*a^7*(-5 + k)*m + (144*I)*a^7*(6 - k)*(-5 + k)*m + 
     (96*I)*a^7*m^3 - 1296*a^6*(-5 + k)*M + 432*a^6*(6 - k)*(-5 + k)*M - 
     240*a^6*m^2*M + 624*a^6*(-5 + k)*m^2*M - 288*a^6*(6 - k)*(-5 + k)*m^2*
      M + (432*I)*a^5*m*M^2 - (1200*I)*a^5*(-5 + k)*m*M^2 + 
     (480*I)*a^5*(6 - k)*(-5 + k)*m*M^2 + 96*a^4*M^3 - 576*a^4*(-5 + k)*M^3 + 
     192*a^4*(6 - k)*(-5 + k)*M^3 - 72*a^6*rh + 2352*a^6*(-5 + k)*rh - 
     960*a^6*(6 - k)*(-5 + k)*rh + 936*a^6*m^2*rh - 
     2016*a^6*(-5 + k)*m^2*rh + 1152*a^6*(6 - k)*(-5 + k)*m^2*rh - 
     288*a^6*m^4*rh - (144*I)*a^5*m*M*rh + (7728*I)*a^5*(-5 + k)*m*M*rh - 
     (3456*I)*a^5*(6 - k)*(-5 + k)*m*M*rh - (864*I)*a^5*m^3*M*rh - 
     864*a^4*M^2*rh + 8064*a^4*(-5 + k)*M^2*rh - 3456*a^4*(6 - k)*(-5 + k)*
      M^2*rh + 1152*a^4*m^2*M^2*rh - 2688*a^4*(-5 + k)*m^2*M^2*rh + 
     1536*a^4*(6 - k)*(-5 + k)*m^2*M^2*rh - (2304*I)*a^3*m*M^3*rh + 
     (2688*I)*a^3*(-5 + k)*m*M^3*rh - (1536*I)*a^3*(6 - k)*(-5 + k)*m*M^3*
      rh - (2520*I)*a^5*m*rh^2 - (8064*I)*a^5*(-5 + k)*m*rh^2 + 
     (3456*I)*a^5*(6 - k)*(-5 + k)*m*rh^2 + (2520*I)*a^5*m^3*rh^2 + 
     1512*a^4*M*rh^2 - 20160*a^4*(-5 + k)*M*rh^2 + 10368*a^4*(6 - k)*(-5 + k)*
      M*rh^2 - 4536*a^4*m^2*M*rh^2 + 14784*a^4*(-5 + k)*m^2*M*rh^2 - 
     10368*a^4*(6 - k)*(-5 + k)*m^2*M*rh^2 + (7056*I)*a^3*m*M^2*rh^2 - 
     (20160*I)*a^3*(-5 + k)*m*M^2*rh^2 + (13824*I)*a^3*(6 - k)*(-5 + k)*m*M^2*
      rh^2 - 5376*a^2*(-5 + k)*M^3*rh^2 + 3456*a^2*(6 - k)*(-5 + k)*M^3*
      rh^2 - 672*a^4*rh^3 + 12096*a^4*(-5 + k)*rh^3 - 
     7200*a^4*(6 - k)*(-5 + k)*rh^3 + 3360*a^4*m^2*rh^3 - 
     16128*a^4*(-5 + k)*m^2*rh^3 + 11520*a^4*(6 - k)*(-5 + k)*m^2*rh^3 - 
     1344*a^4*m^4*rh^3 + (2688*I)*a^3*m*M*rh^3 + (36288*I)*a^3*(-5 + k)*m*M*
      rh^3 - (23040*I)*a^3*(6 - k)*(-5 + k)*m*M*rh^3 - 
     (4032*I)*a^3*m^3*M*rh^3 + 24192*a^2*(-5 + k)*M^2*rh^3 - 
     17280*a^2*(6 - k)*(-5 + k)*M^2*rh^3 - 8064*a^2*(-5 + k)*m^2*M^2*rh^3 + 
     11520*a^2*(6 - k)*(-5 + k)*m^2*M^2*rh^3 - (5376*I)*a*m*M^3*rh^3 + 
     (8064*I)*a*(-5 + k)*m*M^3*rh^3 - (11520*I)*a*(6 - k)*(-5 + k)*m*M^3*
      rh^3 - (6048*I)*a^3*m*rh^4 - (15120*I)*a^3*(-5 + k)*m*rh^4 + 
     (7920*I)*a^3*(6 - k)*(-5 + k)*m*rh^4 + (6048*I)*a^3*m^3*rh^4 - 
     30240*a^2*(-5 + k)*M*rh^4 + 23760*a^2*(6 - k)*(-5 + k)*M*rh^4 + 
     30240*a^2*(-5 + k)*m^2*M*rh^4 - 31680*a^2*(6 - k)*(-5 + k)*m^2*M*rh^4 + 
     (6048*I)*a*m*M^2*rh^4 - (30240*I)*a*(-5 + k)*m*M^2*rh^4 + 
     (31680*I)*a*(6 - k)*(-5 + k)*m*M^2*rh^4 + 11088*a^2*(-5 + k)*rh^5 - 
     9504*a^2*(6 - k)*(-5 + k)*rh^5 - 22176*a^2*(-5 + k)*m^2*rh^5 + 
     19008*a^2*(6 - k)*(-5 + k)*m^2*rh^5 + (22176*I)*a*(-5 + k)*m*M*rh^5 - 
     (19008*I)*a*(6 - k)*(-5 + k)*m*M*rh^5 - (384*I)*a^8*\[Kappa] - 
     (240*I)*a^8*(-5 + k)*\[Kappa] + (192*I)*a^8*m^2*\[Kappa] + 
     (192*I)*a^8*(-5 + k)*m^2*\[Kappa] + 624*a^7*m*M*\[Kappa] + 
     480*a^7*(-5 + k)*m*M*\[Kappa] - (432*I)*a^6*M^2*\[Kappa] - 
     (288*I)*a^6*(-5 + k)*M^2*\[Kappa] - 2592*a^7*m*rh*\[Kappa] - 
     2016*a^7*(-5 + k)*m*rh*\[Kappa] + (5184*I)*a^6*M*rh*\[Kappa] + 
     (4032*I)*a^6*(-5 + k)*M*rh*\[Kappa] - (1728*I)*a^6*m^2*M*rh*\[Kappa] - 
     (2016*I)*a^6*(-5 + k)*m^2*M*rh*\[Kappa] - 1728*a^5*m*M^2*rh*\[Kappa] - 
     1728*a^5*(-5 + k)*m*M^2*rh*\[Kappa] - (7056*I)*a^6*rh^2*\[Kappa] - 
     (6720*I)*a^6*(-5 + k)*rh^2*\[Kappa] + (6048*I)*a^6*m^2*rh^2*\[Kappa] + 
     (8064*I)*a^6*(-5 + k)*m^2*rh^2*\[Kappa] + 15120*a^5*m*M*rh^2*\[Kappa] + 
     14112*a^5*(-5 + k)*m*M*rh^2*\[Kappa] - (9072*I)*a^4*M^2*rh^2*\[Kappa] - 
     (8064*I)*a^4*(-5 + k)*M^2*rh^2*\[Kappa] - 16128*a^5*m*rh^3*\[Kappa] - 
     16128*a^5*(-5 + k)*m*rh^3*\[Kappa] + (29568*I)*a^4*M*rh^3*\[Kappa] + 
     (32256*I)*a^4*(-5 + k)*M*rh^3*\[Kappa] - (16128*I)*a^4*m^2*M*rh^3*
      \[Kappa] - (24192*I)*a^4*(-5 + k)*m^2*M*rh^3*\[Kappa] - 
     16128*a^3*m*M^2*rh^3*\[Kappa] - 16128*a^3*(-5 + k)*m*M^2*rh^3*\[Kappa] - 
     (18144*I)*a^4*rh^4*\[Kappa] - (25200*I)*a^4*(-5 + k)*rh^4*\[Kappa] + 
     (24192*I)*a^4*m^2*rh^4*\[Kappa] + (40320*I)*a^4*(-5 + k)*m^2*rh^4*
      \[Kappa] + 42336*a^3*m*M*rh^4*\[Kappa] + 44352*a^3*(-5 + k)*m*M*rh^4*
      \[Kappa] - (18144*I)*a^2*M^2*rh^4*\[Kappa] - (20160*I)*a^2*(-5 + k)*M^2*
      rh^4*\[Kappa] - 18144*a^3*m*rh^5*\[Kappa] - 22176*a^3*(-5 + k)*m*rh^5*
      \[Kappa] + (30240*I)*a^2*M*rh^5*\[Kappa] + (44352*I)*a^2*(-5 + k)*M*
      rh^5*\[Kappa] - (24192*I)*a^2*m^2*M*rh^5*\[Kappa] - 
     (44352*I)*a^2*(-5 + k)*m^2*M*rh^5*\[Kappa] - 24192*a*m*M^2*rh^5*
      \[Kappa] - 24192*a*(-5 + k)*m*M^2*rh^5*\[Kappa] - 
     (11088*I)*a^2*rh^6*\[Kappa] - (22176*I)*a^2*(-5 + k)*rh^6*\[Kappa] + 
     (22176*I)*a^2*m^2*rh^6*\[Kappa] + (44352*I)*a^2*(-5 + k)*m^2*rh^6*
      \[Kappa] + 22176*a*m*M*rh^6*\[Kappa] + 22176*a*(-5 + k)*m*M*rh^6*
      \[Kappa] + (96*I)*a^9*m*\[Kappa]^2 + 96*a^8*M*\[Kappa]^2 - 
     720*a^8*rh*\[Kappa]^2 + 576*a^8*m^2*rh*\[Kappa]^2 - 
     (504*I)*a^7*m*M*rh*\[Kappa]^2 + (3024*I)*a^7*m*rh^2*\[Kappa]^2 + 
     3024*a^6*M*rh^2*\[Kappa]^2 - 6720*a^6*rh^3*\[Kappa]^2 + 
     8064*a^6*m^2*rh^3*\[Kappa]^2 - (6552*I)*a^5*m*M*rh^3*\[Kappa]^2 + 
     (12096*I)*a^5*m*rh^4*\[Kappa]^2 + 12096*a^4*M*rh^4*\[Kappa]^2 - 
     15120*a^4*rh^5*\[Kappa]^2 + 24192*a^4*m^2*rh^5*\[Kappa]^2 - 
     (21168*I)*a^3*m*M*rh^5*\[Kappa]^2 + (11088*I)*a^3*m*rh^6*\[Kappa]^2 + 
     11088*a^2*M*rh^6*\[Kappa]^2 - 9504*a^2*rh^7*\[Kappa]^2 + 
     19008*a^2*m^2*rh^7*\[Kappa]^2 - (19008*I)*a*m*M*rh^7*\[Kappa]^2 - 
     (120*I)*a^7*m*\[Lambda] + (32*I)*a^7*(-5 + k)*m*\[Lambda] - 
     (32*I)*a^7*(6 - k)*(-5 + k)*m*\[Lambda] + (8*I)*a^7*m^3*\[Lambda] + 
     48*a^6*M*\[Lambda] - 4*a^6*(-5 + k)*M*\[Lambda] - 
     64*a^6*m^2*M*\[Lambda] - (48*I)*a^5*m*M^2*\[Lambda] + 
     (64*I)*a^5*(-5 + k)*m*M^2*\[Lambda] - (32*I)*a^5*(6 - k)*(-5 + k)*m*M^2*
      \[Lambda] - 324*a^6*rh*\[Lambda] + 588*a^6*m^2*rh*\[Lambda] + 
     (1776*I)*a^5*m*M*rh*\[Lambda] - (784*I)*a^5*(-5 + k)*m*M*rh*\[Lambda] + 
     (768*I)*a^5*(6 - k)*(-5 + k)*m*M*rh*\[Lambda] - 
     192*a^4*M^2*rh*\[Lambda] + 56*a^4*(-5 + k)*M^2*rh*\[Lambda] - 
     (3360*I)*a^5*m*rh^2*\[Lambda] + (1344*I)*a^5*(-5 + k)*m*rh^2*\[Lambda] - 
     (1728*I)*a^5*(6 - k)*(-5 + k)*m*rh^2*\[Lambda] + 
     (336*I)*a^5*m^3*rh^2*\[Lambda] + 2772*a^4*M*rh^2*\[Lambda] - 
     224*a^4*(-5 + k)*M*rh^2*\[Lambda] - 2688*a^4*m^2*M*rh^2*\[Lambda] - 
     (4032*I)*a^3*m*M^2*rh^2*\[Lambda] + (1792*I)*a^3*(-5 + k)*m*M^2*rh^2*
      \[Lambda] - (2304*I)*a^3*(6 - k)*(-5 + k)*m*M^2*rh^2*\[Lambda] - 
     3584*a^4*rh^3*\[Lambda] + 5600*a^4*m^2*rh^3*\[Lambda] + 
     (14784*I)*a^3*m*M*rh^3*\[Lambda] - (6720*I)*a^3*(-5 + k)*m*M*rh^3*
      \[Lambda] + (11520*I)*a^3*(6 - k)*(-5 + k)*m*M*rh^3*\[Lambda] - 
     4480*a^2*M^2*rh^3*\[Lambda] + 672*a^2*(-5 + k)*M^2*rh^3*\[Lambda] - 
     (11088*I)*a^3*m*rh^4*\[Lambda] + (6720*I)*a^3*(-5 + k)*m*rh^4*
      \[Lambda] - (10560*I)*a^3*(6 - k)*(-5 + k)*m*rh^4*\[Lambda] + 
     (1008*I)*a^3*m^3*rh^4*\[Lambda] + 13104*a^2*M*rh^4*\[Lambda] - 
     840*a^2*(-5 + k)*M*rh^4*\[Lambda] - 8064*a^2*m^2*M*rh^4*\[Lambda] - 
     (6048*I)*a*m*M^2*rh^4*\[Lambda] - (10560*I)*a*(6 - k)*(-5 + k)*m*M^2*
      rh^4*\[Lambda] - 7560*a^2*rh^5*\[Lambda] + 8568*a^2*m^2*rh^5*
      \[Lambda] + (14112*I)*a*m*M*rh^5*\[Lambda] - (7392*I)*a*(-5 + k)*m*M*
      rh^5*\[Lambda] + (25344*I)*a*(6 - k)*(-5 + k)*m*M*rh^5*\[Lambda] - 
     6048*M^2*rh^5*\[Lambda] - (7392*I)*a*m*rh^6*\[Lambda] + 
     (7392*I)*a*(-5 + k)*m*rh^6*\[Lambda] - (13728*I)*a*(6 - k)*(-5 + k)*m*
      rh^6*\[Lambda] + 9240*M*rh^6*\[Lambda] - 3168*rh^7*\[Lambda] - 
     (4*I)*a^8*(-5 + k)*\[Kappa]*\[Lambda] - 16*a^7*m*M*\[Kappa]*\[Lambda] - 
     32*a^7*(-5 + k)*m*M*\[Kappa]*\[Lambda] + 192*a^7*m*rh*\[Kappa]*
      \[Lambda] + 448*a^7*(-5 + k)*m*rh*\[Kappa]*\[Lambda] + 
     (56*I)*a^6*(-5 + k)*M*rh*\[Kappa]*\[Lambda] - (448*I)*a^6*(-5 + k)*rh^2*
      \[Kappa]*\[Lambda] - 1008*a^5*m*M*rh^2*\[Kappa]*\[Lambda] - 
     2688*a^5*(-5 + k)*m*M*rh^2*\[Kappa]*\[Lambda] + 
     2688*a^5*m*rh^3*\[Kappa]*\[Lambda] + 8064*a^5*(-5 + k)*m*rh^3*\[Kappa]*
      \[Lambda] + (2016*I)*a^4*(-5 + k)*M*rh^3*\[Kappa]*\[Lambda] - 
     (4200*I)*a^4*(-5 + k)*rh^4*\[Kappa]*\[Lambda] - 
     6048*a^3*m*M*rh^4*\[Kappa]*\[Lambda] - 20160*a^3*(-5 + k)*m*M*rh^4*
      \[Kappa]*\[Lambda] + 8064*a^3*m*rh^5*\[Kappa]*\[Lambda] + 
     29568*a^3*(-5 + k)*m*rh^5*\[Kappa]*\[Lambda] + 
     (7392*I)*a^2*(-5 + k)*M*rh^5*\[Kappa]*\[Lambda] - 
     (7392*I)*a^2*(-5 + k)*rh^6*\[Kappa]*\[Lambda] - 
     7392*a*m*M*rh^6*\[Kappa]*\[Lambda] - 29568*a*(-5 + k)*m*M*rh^6*\[Kappa]*
      \[Lambda] + 6336*a*m*rh^7*\[Kappa]*\[Lambda] + 
     27456*a*(-5 + k)*m*rh^7*\[Kappa]*\[Lambda] - (8*I)*a^9*m*\[Kappa]^2*
      \[Lambda] - 12*a^8*rh*\[Kappa]^2*\[Lambda] - 
     (672*I)*a^7*m*rh^2*\[Kappa]^2*\[Lambda] - 336*a^6*rh^3*\[Kappa]^2*
      \[Lambda] - (6048*I)*a^5*m*rh^4*\[Kappa]^2*\[Lambda] - 
     1512*a^4*rh^5*\[Kappa]^2*\[Lambda] - (14784*I)*a^3*m*rh^6*\[Kappa]^2*
      \[Lambda] - 1584*a^2*rh^7*\[Kappa]^2*\[Lambda] - 
     (10296*I)*a*m*rh^8*\[Kappa]^2*\[Lambda] - (8*I)*a^7*m*\[Lambda]^2 + 
     24*a^6*M*\[Lambda]^2 - 2*a^6*(-5 + k)*M*\[Lambda]^2 - 
     174*a^6*rh*\[Lambda]^2 + 6*a^6*m^2*rh*\[Lambda]^2 + 
     (96*I)*a^5*m*M*rh*\[Lambda]^2 - 96*a^4*M^2*rh*\[Lambda]^2 + 
     28*a^4*(-5 + k)*M^2*rh*\[Lambda]^2 - (504*I)*a^5*m*rh^2*\[Lambda]^2 + 
     1470*a^4*M*rh^2*\[Lambda]^2 - 112*a^4*(-5 + k)*M*rh^2*\[Lambda]^2 - 
     2128*a^4*rh^3*\[Lambda]^2 + 112*a^4*m^2*rh^3*\[Lambda]^2 + 
     (1792*I)*a^3*m*M*rh^3*\[Lambda]^2 - 2240*a^2*M^2*rh^3*\[Lambda]^2 + 
     336*a^2*(-5 + k)*M^2*rh^3*\[Lambda]^2 - (3024*I)*a^3*m*rh^4*
      \[Lambda]^2 + 7560*a^2*M*rh^4*\[Lambda]^2 - 420*a^2*(-5 + k)*M*rh^4*
      \[Lambda]^2 - 5292*a^2*rh^5*\[Lambda]^2 + 252*a^2*m^2*rh^5*
      \[Lambda]^2 + (4032*I)*a*m*M*rh^5*\[Lambda]^2 - 
     3024*M^2*rh^5*\[Lambda]^2 - (3696*I)*a*m*rh^6*\[Lambda]^2 + 
     6468*M*rh^6*\[Lambda]^2 - 3168*rh^7*\[Lambda]^2 - 
     (2*I)*a^8*(-5 + k)*\[Kappa]*\[Lambda]^2 + (28*I)*a^6*(-5 + k)*M*rh*
      \[Kappa]*\[Lambda]^2 - (224*I)*a^6*(-5 + k)*rh^2*\[Kappa]*\[Lambda]^2 + 
     (1008*I)*a^4*(-5 + k)*M*rh^3*\[Kappa]*\[Lambda]^2 - 
     (2100*I)*a^4*(-5 + k)*rh^4*\[Kappa]*\[Lambda]^2 + 
     (3696*I)*a^2*(-5 + k)*M*rh^5*\[Kappa]*\[Lambda]^2 - 
     (3696*I)*a^2*(-5 + k)*rh^6*\[Kappa]*\[Lambda]^2 - 
     6*a^8*rh*\[Kappa]^2*\[Lambda]^2 - 168*a^6*rh^3*\[Kappa]^2*\[Lambda]^2 - 
     756*a^4*rh^5*\[Kappa]^2*\[Lambda]^2 - 792*a^2*rh^7*\[Kappa]^2*
      \[Lambda]^2 - 6*a^6*rh*\[Lambda]^3 + 42*a^4*M*rh^2*\[Lambda]^3 - 
     168*a^4*rh^3*\[Lambda]^3 + 504*a^2*M*rh^4*\[Lambda]^3 - 
     756*a^2*rh^5*\[Lambda]^3 + 924*M*rh^6*\[Lambda]^3 - 
     792*rh^7*\[Lambda]^3 - (72*I)*a^7*m*M*rh*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (1512*I)*a^5*m*M*rh^3*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (3024*I)*a^3*m*M*rh^5*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - 96*a^5*(-5 + k)*m*M^2*rh*
      (\[Kappa] - 6*\[Omega]) + 336*a^5*(-5 + k)*m*M*rh^2*
      (\[Kappa] - 6*\[Omega]) - 48*a^5*(-5 + k)*m*M^2*rh*
      (4*\[Kappa] - 5*\[Omega]) + 336*a^5*(-5 + k)*m*M*rh^2*
      (4*\[Kappa] - 5*\[Omega]) - 1344*a^3*(-5 + k)*m*M^2*rh^3*
      (4*\[Kappa] - 5*\[Omega]) + 2016*a^3*(-5 + k)*m*M*rh^4*
      (4*\[Kappa] - 5*\[Omega]) - (312*I)*a^8*\[Omega] + 
     (528*I)*a^8*(-5 + k)*\[Omega] - (240*I)*a^8*(6 - k)*(-5 + k)*\[Omega] - 
     (264*I)*a^8*m^2*\[Omega] + 576*a^7*m*M*\[Omega] - 
     1272*a^7*(-5 + k)*m*M*\[Omega] + 624*a^7*(6 - k)*(-5 + k)*m*M*\[Omega] + 
     (1416*I)*a^6*(-5 + k)*M^2*\[Omega] - (576*I)*a^6*(6 - k)*(-5 + k)*M^2*
      \[Omega] - 2520*a^7*m*rh*\[Omega] + 4032*a^7*(-5 + k)*m*rh*\[Omega] - 
     2688*a^7*(6 - k)*(-5 + k)*m*rh*\[Omega] + 1512*a^7*m^3*rh*\[Omega] + 
     (7704*I)*a^6*M*rh*\[Omega] - (10080*I)*a^6*(-5 + k)*M*rh*\[Omega] + 
     (5760*I)*a^6*(6 - k)*(-5 + k)*M*rh*\[Omega] + (2808*I)*a^6*m^2*M*rh*
      \[Omega] - 2880*a^5*m*M^2*rh*\[Omega] + 4896*a^5*(-5 + k)*m*M^2*rh*
      \[Omega] - 3456*a^5*(6 - k)*(-5 + k)*m*M^2*rh*\[Omega] + 
     (3456*I)*a^4*M^3*rh*\[Omega] - (3024*I)*a^4*(-5 + k)*M^3*rh*\[Omega] + 
     (1536*I)*a^4*(6 - k)*(-5 + k)*M^3*rh*\[Omega] - 
     (7560*I)*a^6*rh^2*\[Omega] + (11928*I)*a^6*(-5 + k)*rh^2*\[Omega] - 
     (8640*I)*a^6*(6 - k)*(-5 + k)*rh^2*\[Omega] - (7560*I)*a^6*m^2*rh^2*
      \[Omega] + 16632*a^5*m*M*rh^2*\[Omega] - 26712*a^5*(-5 + k)*m*M*rh^2*
      \[Omega] + 25920*a^5*(6 - k)*(-5 + k)*m*M*rh^2*\[Omega] - 
     (35784*I)*a^4*M^2*rh^2*\[Omega] + (26880*I)*a^4*(-5 + k)*M^2*rh^2*
      \[Omega] - (20736*I)*a^4*(6 - k)*(-5 + k)*M^2*rh^2*\[Omega] - 
     16128*a^5*m*rh^3*\[Omega] + 32256*a^5*(-5 + k)*m*rh^3*\[Omega] - 
     31680*a^5*(6 - k)*(-5 + k)*m*rh^3*\[Omega] + 12096*a^5*m^3*rh^3*
      \[Omega] + (60480*I)*a^4*M*rh^3*\[Omega] - (55104*I)*a^4*(-5 + k)*M*
      rh^3*\[Omega] + (57600*I)*a^4*(6 - k)*(-5 + k)*M*rh^3*\[Omega] + 
     (16128*I)*a^4*m^2*M*rh^3*\[Omega] - 16128*a^3*m*M^2*rh^3*\[Omega] + 
     10752*a^3*(-5 + k)*m*M^2*rh^3*\[Omega] - 34560*a^3*(6 - k)*(-5 + k)*m*
      M^2*rh^3*\[Omega] + (32256*I)*a^2*M^3*rh^3*\[Omega] - 
     (12096*I)*a^2*(-5 + k)*M^3*rh^3*\[Omega] + (11520*I)*a^2*(6 - k)*
      (-5 + k)*M^3*rh^3*\[Omega] - (21168*I)*a^4*rh^4*\[Omega] + 
     (31248*I)*a^4*(-5 + k)*rh^4*\[Omega] - (39600*I)*a^4*(6 - k)*(-5 + k)*
      rh^4*\[Omega] - (27216*I)*a^4*m^2*rh^4*\[Omega] + 
     42336*a^3*m*M*rh^4*\[Omega] - 49392*a^3*(-5 + k)*m*M*rh^4*\[Omega] + 
     110880*a^3*(6 - k)*(-5 + k)*m*M*rh^4*\[Omega] - 
     (102816*I)*a^2*M^2*rh^4*\[Omega] + (35280*I)*a^2*(-5 + k)*M^2*rh^4*
      \[Omega] - (63360*I)*a^2*(6 - k)*(-5 + k)*M^2*rh^4*\[Omega] - 
     27216*a^3*m*rh^5*\[Omega] + 44352*a^3*(-5 + k)*m*rh^5*\[Omega] - 
     76032*a^3*(6 - k)*(-5 + k)*m*rh^5*\[Omega] + 15120*a^3*m^3*rh^5*
      \[Omega] + (75600*I)*a^2*M*rh^5*\[Omega] - (34272*I)*a^2*(-5 + k)*M*
      rh^5*\[Omega] + (95040*I)*a^2*(6 - k)*(-5 + k)*M*rh^5*\[Omega] + 
     (9072*I)*a^2*m^2*M*rh^5*\[Omega] - 12096*a*m*M^2*rh^5*\[Omega] - 
     12096*a*(-5 + k)*m*M^2*rh^5*\[Omega] - 38016*a*(6 - k)*(-5 + k)*m*M^2*
      rh^5*\[Omega] + (36288*I)*M^3*rh^5*\[Omega] - 
     (11088*I)*a^2*rh^6*\[Omega] + (11088*I)*a^2*(-5 + k)*rh^6*\[Omega] - 
     (41184*I)*a^2*(6 - k)*(-5 + k)*rh^6*\[Omega] - 
     (22176*I)*a^2*m^2*rh^6*\[Omega] + 33264*a*m*M*rh^6*\[Omega] + 
     11088*a*(-5 + k)*m*M*rh^6*\[Omega] + 82368*a*(6 - k)*(-5 + k)*m*M*rh^6*
      \[Omega] - (55440*I)*M^2*rh^6*\[Omega] - 19008*a*m*rh^7*\[Omega] - 
     41184*a*(6 - k)*(-5 + k)*m*rh^7*\[Omega] + (19008*I)*M*rh^7*\[Omega] - 
     (384*I)*a^9*m*\[Kappa]*\[Omega] - (408*I)*a^9*(-5 + k)*m*\[Kappa]*
      \[Omega] - 672*a^8*M*\[Kappa]*\[Omega] - 600*a^8*(-5 + k)*M*\[Kappa]*
      \[Omega] + 3144*a^8*rh*\[Kappa]*\[Omega] + 2784*a^8*(-5 + k)*rh*
      \[Kappa]*\[Omega] + (2880*I)*a^7*m*M*rh*\[Kappa]*\[Omega] + 
     (4368*I)*a^7*(-5 + k)*m*M*rh*\[Kappa]*\[Omega] + 
     1728*a^6*M^2*rh*\[Kappa]*\[Omega] + 2352*a^6*(-5 + k)*M^2*rh*\[Kappa]*
      \[Omega] - (12096*I)*a^7*m*rh^2*\[Kappa]*\[Omega] - 
     (18816*I)*a^7*(-5 + k)*m*rh^2*\[Kappa]*\[Omega] - 
     16416*a^6*M*rh^2*\[Kappa]*\[Omega] - 23040*a^6*(-5 + k)*M*rh^2*\[Kappa]*
      \[Omega] + 22680*a^6*rh^3*\[Kappa]*\[Omega] + 
     34272*a^6*(-5 + k)*rh^3*\[Kappa]*\[Omega] + (20160*I)*a^5*m*M*rh^3*
      \[Kappa]*\[Omega] + (60480*I)*a^5*(-5 + k)*m*M*rh^3*\[Kappa]*\[Omega] + 
     16128*a^4*M^2*rh^3*\[Kappa]*\[Omega] + 33264*a^4*(-5 + k)*M^2*rh^3*
      \[Kappa]*\[Omega] - (48384*I)*a^5*m*rh^4*\[Kappa]*\[Omega] - 
     (110880*I)*a^5*(-5 + k)*m*rh^4*\[Kappa]*\[Omega] - 
     44352*a^4*M*rh^4*\[Kappa]*\[Omega] - 137424*a^4*(-5 + k)*M*rh^4*\[Kappa]*
      \[Omega] + 33264*a^4*rh^5*\[Kappa]*\[Omega] + 
     110880*a^4*(-5 + k)*rh^5*\[Kappa]*\[Omega] + (24192*I)*a^3*m*M*rh^5*
      \[Kappa]*\[Omega] + (155232*I)*a^3*(-5 + k)*m*M*rh^5*\[Kappa]*
      \[Omega] + 24192*a^2*M^2*rh^5*\[Kappa]*\[Omega] + 
     72576*a^2*(-5 + k)*M^2*rh^5*\[Kappa]*\[Omega] - 
     (44352*I)*a^3*m*rh^6*\[Kappa]*\[Omega] - (177408*I)*a^3*(-5 + k)*m*rh^6*
      \[Kappa]*\[Omega] - 20160*a^2*M*rh^6*\[Kappa]*\[Omega] - 
     157248*a^2*(-5 + k)*M*rh^6*\[Kappa]*\[Omega] + 
     7920*a^2*rh^7*\[Kappa]*\[Omega] + 82368*a^2*(-5 + k)*rh^7*\[Kappa]*
      \[Omega] + (82368*I)*a*(-5 + k)*m*M*rh^7*\[Kappa]*\[Omega] - 
     (72072*I)*a*(-5 + k)*m*rh^8*\[Kappa]*\[Omega] - 
     (120*I)*a^10*\[Kappa]^2*\[Omega] - 1224*a^9*m*rh*\[Kappa]^2*\[Omega] + 
     (648*I)*a^8*M*rh*\[Kappa]^2*\[Omega] - (3600*I)*a^8*rh^2*\[Kappa]^2*
      \[Omega] - 18816*a^7*m*rh^3*\[Kappa]^2*\[Omega] + 
     (9648*I)*a^6*M*rh^3*\[Kappa]^2*\[Omega] - (20160*I)*a^6*rh^4*\[Kappa]^2*
      \[Omega] - 66528*a^5*m*rh^5*\[Kappa]^2*\[Omega] + 
     (31920*I)*a^4*M*rh^5*\[Kappa]^2*\[Omega] - (43344*I)*a^4*rh^6*\[Kappa]^2*
      \[Omega] - 76032*a^3*m*rh^7*\[Kappa]^2*\[Omega] + 
     (28512*I)*a^2*M*rh^7*\[Kappa]^2*\[Omega] - (30888*I)*a^2*rh^8*\[Kappa]^2*
      \[Omega] - 24024*a*m*rh^9*\[Kappa]^2*\[Omega] + 
     (48*I)*a^6*M^2*(-18 + \[Lambda])*\[Omega] - 64*a^8*(-5 + k)*rh*\[Kappa]*
      (-9 + \[Lambda])*\[Omega] + 384*a^6*(-5 + k)*M*rh^2*\[Kappa]*
      (-9 + \[Lambda])*\[Omega] - 672*a^6*(-5 + k)*rh^3*\[Kappa]*
      (-9 + \[Lambda])*\[Omega] + 288*a^6*M*rh^2*\[Kappa]*(-6 + \[Lambda])*
      \[Omega] + 2688*a^4*M*rh^4*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     4032*a^2*M*rh^6*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (240*I)*a^8*rh^2*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (1680*I)*a^6*rh^4*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (2016*I)*a^4*rh^6*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] - 
     (56*I)*a^6*(-5 + k)*rh^2*(-3 + \[Lambda])*\[Omega] + 
     (448*I)*a^4*(-5 + k)*M*rh^3*(-3 + \[Lambda])*\[Omega] - 
     (1344*I)*a^4*(-5 + k)*rh^4*(-3 + \[Lambda])*\[Omega] + 
     (3360*I)*a^2*(-5 + k)*M*rh^5*(-3 + \[Lambda])*\[Omega] - 
     (3696*I)*a^2*(-5 + k)*rh^6*(-3 + \[Lambda])*\[Omega] - 
     8*a^8*rh*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 504*a^6*rh^3*\[Kappa]*
      (-3 + \[Lambda])*\[Omega] - 3024*a^4*rh^5*\[Kappa]*(-3 + \[Lambda])*
      \[Omega] - 3696*a^2*rh^7*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
     (96*I)*a^8*\[Lambda]*\[Omega] - (32*I)*a^8*(-5 + k)*\[Lambda]*\[Omega] + 
     (32*I)*a^8*(6 - k)*(-5 + k)*\[Lambda]*\[Omega] - 
     (24*I)*a^8*m^2*\[Lambda]*\[Omega] + 128*a^7*m*M*\[Lambda]*\[Omega] - 
     (64*I)*a^6*(-5 + k)*M^2*\[Lambda]*\[Omega] + (32*I)*a^6*(6 - k)*(-5 + k)*
      M^2*\[Lambda]*\[Omega] - 1296*a^7*m*rh*\[Lambda]*\[Omega] - 
     (1416*I)*a^6*M*rh*\[Lambda]*\[Omega] + (784*I)*a^6*(-5 + k)*M*rh*
      \[Lambda]*\[Omega] - (768*I)*a^6*(6 - k)*(-5 + k)*M*rh*\[Lambda]*
      \[Omega] + (1848*I)*a^6*rh^2*\[Lambda]*\[Omega] - 
     (1288*I)*a^6*(-5 + k)*rh^2*\[Lambda]*\[Omega] + 
     (1728*I)*a^6*(6 - k)*(-5 + k)*rh^2*\[Lambda]*\[Omega] - 
     (1344*I)*a^6*m^2*rh^2*\[Lambda]*\[Omega] + 6216*a^5*m*M*rh^2*\[Lambda]*
      \[Omega] + (3528*I)*a^4*M^2*rh^2*\[Lambda]*\[Omega] - 
     (1792*I)*a^4*(-5 + k)*M^2*rh^2*\[Lambda]*\[Omega] + 
     (2304*I)*a^4*(6 - k)*(-5 + k)*M^2*rh^2*\[Lambda]*\[Omega] - 
     14784*a^5*m*rh^3*\[Lambda]*\[Omega] - (7392*I)*a^4*M*rh^3*\[Lambda]*
      \[Omega] + (6272*I)*a^4*(-5 + k)*M*rh^3*\[Lambda]*\[Omega] - 
     (11520*I)*a^4*(6 - k)*(-5 + k)*M*rh^3*\[Lambda]*\[Omega] + 
     (2016*I)*a^4*rh^4*\[Lambda]*\[Omega] - (5376*I)*a^4*(-5 + k)*rh^4*
      \[Lambda]*\[Omega] + (10560*I)*a^4*(6 - k)*(-5 + k)*rh^4*\[Lambda]*
      \[Omega] - (7056*I)*a^4*m^2*rh^4*\[Lambda]*\[Omega] + 
     26208*a^3*m*M*rh^4*\[Lambda]*\[Omega] + (10560*I)*a^2*(6 - k)*(-5 + k)*
      M^2*rh^4*\[Lambda]*\[Omega] - 34272*a^3*m*rh^5*\[Lambda]*\[Omega] + 
     (7056*I)*a^2*M*rh^5*\[Lambda]*\[Omega] + (4032*I)*a^2*(-5 + k)*M*rh^5*
      \[Lambda]*\[Omega] - (25344*I)*a^2*(6 - k)*(-5 + k)*M*rh^5*\[Lambda]*
      \[Omega] - (3696*I)*a^2*rh^6*\[Lambda]*\[Omega] - 
     (3696*I)*a^2*(-5 + k)*rh^6*\[Lambda]*\[Omega] + 
     (13728*I)*a^2*(6 - k)*(-5 + k)*rh^6*\[Lambda]*\[Omega] - 
     (7392*I)*a^2*m^2*rh^6*\[Lambda]*\[Omega] + 18480*a*m*M*rh^6*\[Lambda]*
      \[Omega] - (11088*I)*M^2*rh^6*\[Lambda]*\[Omega] - 
     19008*a*m*rh^7*\[Lambda]*\[Omega] + (9504*I)*M*rh^7*\[Lambda]*\[Omega] + 
     16*a^8*M*\[Kappa]*\[Lambda]*\[Omega] + 32*a^8*(-5 + k)*M*\[Kappa]*
      \[Lambda]*\[Omega] - 184*a^8*rh*\[Kappa]*\[Lambda]*\[Omega] - 
     384*a^8*(-5 + k)*rh*\[Kappa]*\[Lambda]*\[Omega] + 
     720*a^6*M*rh^2*\[Kappa]*\[Lambda]*\[Omega] + 2304*a^6*(-5 + k)*M*rh^2*
      \[Kappa]*\[Lambda]*\[Omega] - 2184*a^6*rh^3*\[Kappa]*\[Lambda]*
      \[Omega] - 7392*a^6*(-5 + k)*rh^3*\[Kappa]*\[Lambda]*\[Omega] + 
     3360*a^4*M*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 20160*a^4*(-5 + k)*M*rh^4*
      \[Kappa]*\[Lambda]*\[Omega] - 5040*a^4*rh^5*\[Kappa]*\[Lambda]*
      \[Omega] - 29568*a^4*(-5 + k)*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
     3360*a^2*M*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 29568*a^2*(-5 + k)*M*rh^6*
      \[Kappa]*\[Lambda]*\[Omega] - 2640*a^2*rh^7*\[Kappa]*\[Lambda]*
      \[Omega] - 27456*a^2*(-5 + k)*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
     (8*I)*a^10*\[Kappa]^2*\[Lambda]*\[Omega] + (432*I)*a^8*rh^2*\[Kappa]^2*
      \[Lambda]*\[Omega] + (4368*I)*a^6*rh^4*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (12768*I)*a^4*rh^6*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (10296*I)*a^2*rh^8*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (8*I)*a^8*\[Lambda]^2*\[Omega] - 12*a^7*m*rh*\[Lambda]^2*\[Omega] - 
     (96*I)*a^6*M*rh*\[Lambda]^2*\[Omega] + (504*I)*a^6*rh^2*\[Lambda]^2*
      \[Omega] - 336*a^5*m*rh^3*\[Lambda]^2*\[Omega] - 
     (1792*I)*a^4*M*rh^3*\[Lambda]^2*\[Omega] + (3024*I)*a^4*rh^4*\[Lambda]^2*
      \[Omega] - 1512*a^3*m*rh^5*\[Lambda]^2*\[Omega] - 
     (4032*I)*a^2*M*rh^5*\[Lambda]^2*\[Omega] + (3696*I)*a^2*rh^6*\[Lambda]^2*
      \[Omega] - 1584*a*m*rh^7*\[Lambda]^2*\[Omega] + 
     (432*I)*a^6*M*rh^3*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (1344*I)*a^4*M*rh^5*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
     48*a^6*(-5 + k)*M*rh^2*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     48*a^6*(-5 + k)*M*rh^2*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     336*a^4*(-5 + k)*M^2*rh^3*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     672*a^4*(-5 + k)*M*rh^4*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     (240*I)*a^9*m*\[Omega]^2 + 648*a^8*(-5 + k)*M*\[Omega]^2 - 
     336*a^8*(6 - k)*(-5 + k)*M*\[Omega]^2 + 1512*a^8*rh*\[Omega]^2 - 
     2016*a^8*(-5 + k)*rh*\[Omega]^2 + 1536*a^8*(6 - k)*(-5 + k)*rh*
      \[Omega]^2 - 2808*a^8*m^2*rh*\[Omega]^2 - (3024*I)*a^7*m*M*rh*
      \[Omega]^2 + 1728*a^6*M^2*rh*\[Omega]^2 - 3024*a^6*(-5 + k)*M^2*rh*
      \[Omega]^2 + 1920*a^6*(6 - k)*(-5 + k)*M^2*rh*\[Omega]^2 + 
     (6048*I)*a^7*m*rh^2*\[Omega]^2 - 11592*a^6*M*rh^2*\[Omega]^2 + 
     15192*a^6*(-5 + k)*M*rh^2*\[Omega]^2 - 15552*a^6*(6 - k)*(-5 + k)*M*rh^2*
      \[Omega]^2 + 10752*a^6*rh^3*\[Omega]^2 - 16128*a^6*(-5 + k)*rh^3*
      \[Omega]^2 + 20160*a^6*(6 - k)*(-5 + k)*rh^3*\[Omega]^2 - 
     30912*a^6*m^2*rh^3*\[Omega]^2 - (17472*I)*a^5*m*M*rh^3*\[Omega]^2 + 
     16128*a^4*M^2*rh^3*\[Omega]^2 - 7392*a^4*(-5 + k)*M^2*rh^3*\[Omega]^2 + 
     23040*a^4*(6 - k)*(-5 + k)*M^2*rh^3*\[Omega]^2 + 
     (18144*I)*a^5*m*rh^4*\[Omega]^2 - 36288*a^4*M*rh^4*\[Omega]^2 + 
     24528*a^4*(-5 + k)*M*rh^4*\[Omega]^2 - 79200*a^4*(6 - k)*(-5 + k)*M*rh^4*
      \[Omega]^2 + 18144*a^4*rh^5*\[Omega]^2 - 22176*a^4*(-5 + k)*rh^5*
      \[Omega]^2 + 57024*a^4*(6 - k)*(-5 + k)*rh^5*\[Omega]^2 - 
     69552*a^4*m^2*rh^5*\[Omega]^2 + (6048*I)*a^3*m*M*rh^5*\[Omega]^2 + 
     12096*a^2*M^2*rh^5*\[Omega]^2 + 18144*a^2*(-5 + k)*M^2*rh^5*\[Omega]^2 + 
     38016*a^2*(6 - k)*(-5 + k)*M^2*rh^5*\[Omega]^2 + 
     (11088*I)*a^3*m*rh^6*\[Omega]^2 - 22176*a^2*M*rh^6*\[Omega]^2 - 
     17136*a^2*(-5 + k)*M*rh^6*\[Omega]^2 - 82368*a^2*(6 - k)*(-5 + k)*M*rh^6*
      \[Omega]^2 + 9504*a^2*rh^7*\[Omega]^2 + 41184*a^2*(6 - k)*(-5 + k)*rh^7*
      \[Omega]^2 - 38016*a^2*m^2*rh^7*\[Omega]^2 + (38016*I)*a*m*M*rh^7*
      \[Omega]^2 + (192*I)*a^10*\[Kappa]*\[Omega]^2 + 
     (216*I)*a^10*(-5 + k)*\[Kappa]*\[Omega]^2 - (1728*I)*a^8*M*rh*\[Kappa]*
      \[Omega]^2 - (2352*I)*a^8*(-5 + k)*M*rh*\[Kappa]*\[Omega]^2 + 
     (6048*I)*a^8*rh^2*\[Kappa]*\[Omega]^2 + (10752*I)*a^8*(-5 + k)*rh^2*
      \[Kappa]*\[Omega]^2 - (14400*I)*a^6*M*rh^3*\[Kappa]*\[Omega]^2 - 
     (36288*I)*a^6*(-5 + k)*M*rh^3*\[Kappa]*\[Omega]^2 + 
     (24192*I)*a^6*rh^4*\[Kappa]*\[Omega]^2 + (70560*I)*a^6*(-5 + k)*rh^4*
      \[Kappa]*\[Omega]^2 - (18816*I)*a^4*M*rh^5*\[Kappa]*\[Omega]^2 - 
     (110880*I)*a^4*(-5 + k)*M*rh^5*\[Kappa]*\[Omega]^2 + 
     (22176*I)*a^4*rh^6*\[Kappa]*\[Omega]^2 + (133056*I)*a^4*(-5 + k)*rh^6*
      \[Kappa]*\[Omega]^2 - (82368*I)*a^2*(-5 + k)*M*rh^7*\[Kappa]*
      \[Omega]^2 + (72072*I)*a^2*(-5 + k)*rh^8*\[Kappa]*\[Omega]^2 + 
     648*a^10*rh*\[Kappa]^2*\[Omega]^2 + 10752*a^8*rh^3*\[Kappa]^2*
      \[Omega]^2 + 42336*a^6*rh^5*\[Kappa]^2*\[Omega]^2 + 
     57024*a^4*rh^7*\[Kappa]^2*\[Omega]^2 + 24024*a^2*rh^9*\[Kappa]^2*
      \[Omega]^2 + (24*I)*a^9*m*\[Lambda]*\[Omega]^2 + 
     708*a^8*rh*\[Lambda]*\[Omega]^2 + (1680*I)*a^7*m*rh^2*\[Lambda]*
      \[Omega]^2 - 3528*a^6*M*rh^2*\[Lambda]*\[Omega]^2 + 
     9184*a^6*rh^3*\[Lambda]*\[Omega]^2 + (12096*I)*a^5*m*rh^4*\[Lambda]*
      \[Omega]^2 - 18144*a^4*M*rh^4*\[Lambda]*\[Omega]^2 + 
     26208*a^4*rh^5*\[Lambda]*\[Omega]^2 + (22176*I)*a^3*m*rh^6*\[Lambda]*
      \[Omega]^2 - 18480*a^2*M*rh^6*\[Lambda]*\[Omega]^2 + 
     22176*a^2*rh^7*\[Lambda]*\[Omega]^2 + (10296*I)*a*m*rh^8*\[Lambda]*
      \[Omega]^2 + 4004*rh^9*\[Lambda]*\[Omega]^2 + 
     6*a^8*rh*\[Lambda]^2*\[Omega]^2 + 224*a^6*rh^3*\[Lambda]^2*\[Omega]^2 + 
     1512*a^4*rh^5*\[Lambda]^2*\[Omega]^2 + 3168*a^2*rh^7*\[Lambda]^2*
      \[Omega]^2 + 2002*rh^9*\[Lambda]^2*\[Omega]^2 - 
     16*a^8*M*(21 + 4*\[Lambda])*\[Omega]^2 + 2232*a^9*m*rh*\[Omega]^3 + 
     (1080*I)*a^8*M*rh*\[Omega]^3 - (1008*I)*a^8*rh^2*\[Omega]^3 + 
     30912*a^7*m*rh^3*\[Omega]^3 + (5376*I)*a^6*M*rh^3*\[Omega]^3 + 
     (6048*I)*a^6*rh^4*\[Omega]^3 + 96768*a^5*m*rh^5*\[Omega]^3 - 
     (18144*I)*a^4*M*rh^5*\[Omega]^3 + (33264*I)*a^4*rh^6*\[Omega]^3 + 
     95040*a^3*m*rh^7*\[Omega]^3 - (57024*I)*a^2*M*rh^7*\[Omega]^3 + 
     (30888*I)*a^2*rh^8*\[Omega]^3 + 24024*a*m*rh^9*\[Omega]^3 - 
     (24024*I)*M*rh^9*\[Omega]^3 - (672*I)*a^8*rh^2*\[Lambda]*\[Omega]^3 - 
     (6048*I)*a^6*rh^4*\[Lambda]*\[Omega]^3 - (14784*I)*a^4*rh^6*\[Lambda]*
      \[Omega]^3 - (10296*I)*a^2*rh^8*\[Lambda]*\[Omega]^3 - 
     (8*I)*a^10*(9 + \[Lambda])*\[Omega]^3 - 648*a^10*rh*\[Omega]^4 - 
     10752*a^8*rh^3*\[Omega]^4 - 42336*a^6*rh^5*\[Omega]^4 - 
     57024*a^4*rh^7*\[Omega]^4 - 24024*a^2*rh^9*\[Omega]^4 + 
     168*a^5*(-5 + k)*m*M*rh^2*(2*\[Kappa] + \[Omega]) - 
     1344*a^3*(-5 + k)*m*M^2*rh^3*(2*\[Kappa] + \[Omega]) + 
     4032*a^3*(-5 + k)*m*M*rh^4*(2*\[Kappa] + \[Omega]) - 
     10080*a*(-5 + k)*m*M^2*rh^5*(2*\[Kappa] + \[Omega]) + 
     11088*a*(-5 + k)*m*M*rh^6*(2*\[Kappa] + \[Omega]) - 
     24*a^6*(-5 + k)*M*rh^2*\[Omega]*(4*\[Kappa] + \[Omega]) + 
     336*a^4*(-5 + k)*M^2*rh^3*\[Omega]*(4*\[Kappa] + \[Omega]) - 
     1344*a^4*(-5 + k)*M*rh^4*\[Omega]*(4*\[Kappa] + \[Omega]) + 
     4032*a^2*(-5 + k)*M^2*rh^5*\[Omega]*(4*\[Kappa] + \[Omega]) - 
     5040*a^2*(-5 + k)*M*rh^6*\[Omega]*(4*\[Kappa] + \[Omega]) + 
     4*a^6*(6 - k)*(-5 + k)*M*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 32*a^6*(6 - k)*(-5 + k)*rh*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     32*a^4*(6 - k)*(-5 + k)*M^2*rh*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 432*a^4*(6 - k)*(-5 + k)*M*rh^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     720*a^4*(6 - k)*(-5 + k)*rh^3*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 960*a^2*(6 - k)*(-5 + k)*M^2*rh^3*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     3960*a^2*(6 - k)*(-5 + k)*M*rh^4*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 3168*a^2*(6 - k)*(-5 + k)*rh^5*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3168*(6 - k)*(-5 + k)*M^2*rh^5*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 6864*(6 - k)*(-5 + k)*M*rh^6*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3432*(6 - k)*(-5 + k)*rh^7*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 56*a^6*rh^3*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     756*a^4*rh^5*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 2376*a^2*rh^7*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     2002*rh^9*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     (56*I)*a^4*(-5 + k)*M*rh^2*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (336*I)*a^2*(-5 + k)*M^2*rh^3*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (840*I)*a^2*(-5 + k)*M*rh^4*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (1848*I)*(-5 + k)*M^2*rh^5*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - (1848*I)*(-5 + k)*M*rh^6*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     420*a^4*(-5 + k)*rh^4*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 1848*a^2*(-5 + k)*M*rh^5*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     3696*a^2*(-5 + k)*rh^6*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 6864*(-5 + k)*M*rh^7*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     6006*(-5 + k)*rh^8*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-5 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  ((36*a^8 + 384*a^8*(-4 + k) - 120*a^8*(5 - k)*(-4 + k) + 132*a^8*m^2 - 
     192*a^8*(-4 + k)*m^2 + 96*a^8*(5 - k)*(-4 + k)*m^2 - 24*a^8*m^4 + 
     (1104*I)*a^7*(-4 + k)*m*M - (384*I)*a^7*(5 - k)*(-4 + k)*m*M - 
     (72*I)*a^7*m^3*M + 144*a^6*M^2 + 1440*a^6*(-4 + k)*M^2 - 
     432*a^6*(5 - k)*(-4 + k)*M^2 - 96*a^6*m^2*M^2 - 
     288*a^6*(-4 + k)*m^2*M^2 + 96*a^6*(5 - k)*(-4 + k)*m^2*M^2 + 
     (288*I)*a^5*(-4 + k)*m*M^3 - (96*I)*a^5*(5 - k)*(-4 + k)*m*M^3 - 
     (480*I)*a^7*m*rh - (2592*I)*a^7*(-4 + k)*m*rh + 
     (1008*I)*a^7*(5 - k)*(-4 + k)*m*rh + (480*I)*a^7*m^3*rh - 
     7776*a^6*(-4 + k)*M*rh + 3024*a^6*(5 - k)*(-4 + k)*M*rh - 
     1200*a^6*m^2*M*rh + 3744*a^6*(-4 + k)*m^2*M*rh - 
     2016*a^6*(5 - k)*(-4 + k)*m^2*M*rh + (2160*I)*a^5*m*M^2*rh - 
     (7200*I)*a^5*(-4 + k)*m*M^2*rh + (3360*I)*a^5*(5 - k)*(-4 + k)*m*M^2*
      rh + 480*a^4*M^3*rh - 3456*a^4*(-4 + k)*M^3*rh + 
     1344*a^4*(5 - k)*(-4 + k)*M^3*rh - 180*a^6*rh^2 + 
     7056*a^6*(-4 + k)*rh^2 - 3360*a^6*(5 - k)*(-4 + k)*rh^2 + 
     2340*a^6*m^2*rh^2 - 6048*a^6*(-4 + k)*m^2*rh^2 + 
     4032*a^6*(5 - k)*(-4 + k)*m^2*rh^2 - 720*a^6*m^4*rh^2 - 
     (360*I)*a^5*m*M*rh^2 + (23184*I)*a^5*(-4 + k)*m*M*rh^2 - 
     (12096*I)*a^5*(5 - k)*(-4 + k)*m*M*rh^2 - (2160*I)*a^5*m^3*M*rh^2 - 
     2160*a^4*M^2*rh^2 + 24192*a^4*(-4 + k)*M^2*rh^2 - 
     12096*a^4*(5 - k)*(-4 + k)*M^2*rh^2 + 2880*a^4*m^2*M^2*rh^2 - 
     8064*a^4*(-4 + k)*m^2*M^2*rh^2 + 5376*a^4*(5 - k)*(-4 + k)*m^2*M^2*
      rh^2 - (5760*I)*a^3*m*M^3*rh^2 + (8064*I)*a^3*(-4 + k)*m*M^3*rh^2 - 
     (5376*I)*a^3*(5 - k)*(-4 + k)*m*M^3*rh^2 - (4200*I)*a^5*m*rh^3 - 
     (16128*I)*a^5*(-4 + k)*m*rh^3 + (8064*I)*a^5*(5 - k)*(-4 + k)*m*rh^3 + 
     (4200*I)*a^5*m^3*rh^3 + 2520*a^4*M*rh^3 - 40320*a^4*(-4 + k)*M*rh^3 + 
     24192*a^4*(5 - k)*(-4 + k)*M*rh^3 - 7560*a^4*m^2*M*rh^3 + 
     29568*a^4*(-4 + k)*m^2*M*rh^3 - 24192*a^4*(5 - k)*(-4 + k)*m^2*M*rh^3 + 
     (11760*I)*a^3*m*M^2*rh^3 - (40320*I)*a^3*(-4 + k)*m*M^2*rh^3 + 
     (32256*I)*a^3*(5 - k)*(-4 + k)*m*M^2*rh^3 - 10752*a^2*(-4 + k)*M^3*
      rh^3 + 8064*a^2*(5 - k)*(-4 + k)*M^3*rh^3 - 840*a^4*rh^4 + 
     18144*a^4*(-4 + k)*rh^4 - 12600*a^4*(5 - k)*(-4 + k)*rh^4 + 
     4200*a^4*m^2*rh^4 - 24192*a^4*(-4 + k)*m^2*rh^4 + 
     20160*a^4*(5 - k)*(-4 + k)*m^2*rh^4 - 1680*a^4*m^4*rh^4 + 
     (3360*I)*a^3*m*M*rh^4 + (54432*I)*a^3*(-4 + k)*m*M*rh^4 - 
     (40320*I)*a^3*(5 - k)*(-4 + k)*m*M*rh^4 - (5040*I)*a^3*m^3*M*rh^4 + 
     36288*a^2*(-4 + k)*M^2*rh^4 - 30240*a^2*(5 - k)*(-4 + k)*M^2*rh^4 - 
     12096*a^2*(-4 + k)*m^2*M^2*rh^4 + 20160*a^2*(5 - k)*(-4 + k)*m^2*M^2*
      rh^4 - (6720*I)*a*m*M^3*rh^4 + (12096*I)*a*(-4 + k)*m*M^3*rh^4 - 
     (20160*I)*a*(5 - k)*(-4 + k)*m*M^3*rh^4 - (6048*I)*a^3*m*rh^5 - 
     (18144*I)*a^3*(-4 + k)*m*rh^5 + (11088*I)*a^3*(5 - k)*(-4 + k)*m*rh^5 + 
     (6048*I)*a^3*m^3*rh^5 - 36288*a^2*(-4 + k)*M*rh^5 + 
     33264*a^2*(5 - k)*(-4 + k)*M*rh^5 + 36288*a^2*(-4 + k)*m^2*M*rh^5 - 
     44352*a^2*(5 - k)*(-4 + k)*m^2*M*rh^5 + (6048*I)*a*m*M^2*rh^5 - 
     (36288*I)*a*(-4 + k)*m*M^2*rh^5 + (44352*I)*a*(5 - k)*(-4 + k)*m*M^2*
      rh^5 + 11088*a^2*(-4 + k)*rh^6 - 11088*a^2*(5 - k)*(-4 + k)*rh^6 - 
     22176*a^2*(-4 + k)*m^2*rh^6 + 22176*a^2*(5 - k)*(-4 + k)*m^2*rh^6 + 
     (22176*I)*a*(-4 + k)*m*M*rh^6 - (22176*I)*a*(5 - k)*(-4 + k)*m*M*rh^6 - 
     288*a^9*m*\[Kappa] - 192*a^9*(-4 + k)*m*\[Kappa] + 
     (624*I)*a^8*M*\[Kappa] + (384*I)*a^8*(-4 + k)*M*\[Kappa] - 
     (96*I)*a^8*m^2*M*\[Kappa] - (96*I)*a^8*(-4 + k)*m^2*M*\[Kappa] - 
     96*a^7*m*M^2*\[Kappa] - 96*a^7*(-4 + k)*m*M^2*\[Kappa] - 
     (1920*I)*a^8*rh*\[Kappa] - (1440*I)*a^8*(-4 + k)*rh*\[Kappa] + 
     (960*I)*a^8*m^2*rh*\[Kappa] + (1152*I)*a^8*(-4 + k)*m^2*rh*\[Kappa] + 
     3120*a^7*m*M*rh*\[Kappa] + 2688*a^7*(-4 + k)*m*M*rh*\[Kappa] - 
     (2160*I)*a^6*M^2*rh*\[Kappa] - (1728*I)*a^6*(-4 + k)*M^2*rh*\[Kappa] - 
     6480*a^7*m*rh^2*\[Kappa] - 6048*a^7*(-4 + k)*m*rh^2*\[Kappa] + 
     (12960*I)*a^6*M*rh^2*\[Kappa] + (12096*I)*a^6*(-4 + k)*M*rh^2*\[Kappa] - 
     (4320*I)*a^6*m^2*M*rh^2*\[Kappa] - (6048*I)*a^6*(-4 + k)*m^2*M*rh^2*
      \[Kappa] - 4320*a^5*m*M^2*rh^2*\[Kappa] - 4320*a^5*(-4 + k)*m*M^2*rh^2*
      \[Kappa] - (11760*I)*a^6*rh^3*\[Kappa] - (13440*I)*a^6*(-4 + k)*rh^3*
      \[Kappa] + (10080*I)*a^6*m^2*rh^3*\[Kappa] + (16128*I)*a^6*(-4 + k)*m^2*
      rh^3*\[Kappa] + 25200*a^5*m*M*rh^3*\[Kappa] + 
     26208*a^5*(-4 + k)*m*M*rh^3*\[Kappa] - (15120*I)*a^4*M^2*rh^3*\[Kappa] - 
     (16128*I)*a^4*(-4 + k)*M^2*rh^3*\[Kappa] - 20160*a^5*m*rh^4*\[Kappa] - 
     24192*a^5*(-4 + k)*m*rh^4*\[Kappa] + (36960*I)*a^4*M*rh^4*\[Kappa] + 
     (48384*I)*a^4*(-4 + k)*M*rh^4*\[Kappa] - (20160*I)*a^4*m^2*M*rh^4*
      \[Kappa] - (36288*I)*a^4*(-4 + k)*m^2*M*rh^4*\[Kappa] - 
     20160*a^3*m*M^2*rh^4*\[Kappa] - 20160*a^3*(-4 + k)*m*M^2*rh^4*\[Kappa] - 
     (18144*I)*a^4*rh^5*\[Kappa] - (30240*I)*a^4*(-4 + k)*rh^5*\[Kappa] + 
     (24192*I)*a^4*m^2*rh^5*\[Kappa] + (48384*I)*a^4*(-4 + k)*m^2*rh^5*
      \[Kappa] + 42336*a^3*m*M*rh^5*\[Kappa] + 48384*a^3*(-4 + k)*m*M*rh^5*
      \[Kappa] - (18144*I)*a^2*M^2*rh^5*\[Kappa] - (24192*I)*a^2*(-4 + k)*M^2*
      rh^5*\[Kappa] - 15120*a^3*m*rh^6*\[Kappa] - 22176*a^3*(-4 + k)*m*rh^6*
      \[Kappa] + (25200*I)*a^2*M*rh^6*\[Kappa] + (44352*I)*a^2*(-4 + k)*M*
      rh^6*\[Kappa] - (20160*I)*a^2*m^2*M*rh^6*\[Kappa] - 
     (44352*I)*a^2*(-4 + k)*m^2*M*rh^6*\[Kappa] - 20160*a*m*M^2*rh^6*
      \[Kappa] - 20160*a*(-4 + k)*m*M^2*rh^6*\[Kappa] - 
     (7920*I)*a^2*rh^7*\[Kappa] - (19008*I)*a^2*(-4 + k)*rh^7*\[Kappa] + 
     (15840*I)*a^2*m^2*rh^7*\[Kappa] + (38016*I)*a^2*(-4 + k)*m^2*rh^7*
      \[Kappa] + 15840*a*m*M*rh^7*\[Kappa] + 15840*a*(-4 + k)*m*M*rh^7*
      \[Kappa] - 60*a^10*\[Kappa]^2 + 24*a^10*m^2*\[Kappa]^2 - 
     (24*I)*a^9*m*M*\[Kappa]^2 + (480*I)*a^9*m*rh*\[Kappa]^2 + 
     480*a^8*M*rh*\[Kappa]^2 - 1800*a^8*rh^2*\[Kappa]^2 + 
     1440*a^8*m^2*rh^2*\[Kappa]^2 - (1080*I)*a^7*m*M*rh^2*\[Kappa]^2 + 
     (5040*I)*a^7*m*rh^3*\[Kappa]^2 + 5040*a^6*M*rh^3*\[Kappa]^2 - 
     8400*a^6*rh^4*\[Kappa]^2 + 10080*a^6*m^2*rh^4*\[Kappa]^2 - 
     (7560*I)*a^5*m*M*rh^4*\[Kappa]^2 + (12096*I)*a^5*m*rh^5*\[Kappa]^2 + 
     12096*a^4*M*rh^5*\[Kappa]^2 - 12600*a^4*rh^6*\[Kappa]^2 + 
     20160*a^4*m^2*rh^6*\[Kappa]^2 - (17136*I)*a^3*m*M*rh^6*\[Kappa]^2 + 
     (7920*I)*a^3*m*rh^7*\[Kappa]^2 + 7920*a^2*M*rh^7*\[Kappa]^2 - 
     5940*a^2*rh^8*\[Kappa]^2 + 11880*a^2*m^2*rh^8*\[Kappa]^2 - 
     (11880*I)*a*m*M*rh^8*\[Kappa]^2 - 16*a^8*\[Lambda] + 
     32*a^8*m^2*\[Lambda] + (88*I)*a^7*m*M*\[Lambda] - 
     (48*I)*a^7*(-4 + k)*m*M*\[Lambda] + (32*I)*a^7*(5 - k)*(-4 + k)*m*M*
      \[Lambda] - (600*I)*a^7*m*rh*\[Lambda] + (192*I)*a^7*(-4 + k)*m*rh*
      \[Lambda] - (224*I)*a^7*(5 - k)*(-4 + k)*m*rh*\[Lambda] + 
     (40*I)*a^7*m^3*rh*\[Lambda] + 240*a^6*M*rh*\[Lambda] - 
     24*a^6*(-4 + k)*M*rh*\[Lambda] - 320*a^6*m^2*M*rh*\[Lambda] - 
     (240*I)*a^5*m*M^2*rh*\[Lambda] + (384*I)*a^5*(-4 + k)*m*M^2*rh*
      \[Lambda] - (224*I)*a^5*(5 - k)*(-4 + k)*m*M^2*rh*\[Lambda] - 
     810*a^6*rh^2*\[Lambda] + 1470*a^6*m^2*rh^2*\[Lambda] + 
     (4440*I)*a^5*m*M*rh^2*\[Lambda] - (2352*I)*a^5*(-4 + k)*m*M*rh^2*
      \[Lambda] + (2688*I)*a^5*(5 - k)*(-4 + k)*m*M*rh^2*\[Lambda] - 
     480*a^4*M^2*rh^2*\[Lambda] + 168*a^4*(-4 + k)*M^2*rh^2*\[Lambda] - 
     (5600*I)*a^5*m*rh^3*\[Lambda] + (2688*I)*a^5*(-4 + k)*m*rh^3*\[Lambda] - 
     (4032*I)*a^5*(5 - k)*(-4 + k)*m*rh^3*\[Lambda] + 
     (560*I)*a^5*m^3*rh^3*\[Lambda] + 4620*a^4*M*rh^3*\[Lambda] - 
     448*a^4*(-4 + k)*M*rh^3*\[Lambda] - 4480*a^4*m^2*M*rh^3*\[Lambda] - 
     (6720*I)*a^3*m*M^2*rh^3*\[Lambda] + (3584*I)*a^3*(-4 + k)*m*M^2*rh^3*
      \[Lambda] - (5376*I)*a^3*(5 - k)*(-4 + k)*m*M^2*rh^3*\[Lambda] - 
     4480*a^4*rh^4*\[Lambda] + 7000*a^4*m^2*rh^4*\[Lambda] + 
     (18480*I)*a^3*m*M*rh^4*\[Lambda] - (10080*I)*a^3*(-4 + k)*m*M*rh^4*
      \[Lambda] + (20160*I)*a^3*(5 - k)*(-4 + k)*m*M*rh^4*\[Lambda] - 
     5600*a^2*M^2*rh^4*\[Lambda] + 1008*a^2*(-4 + k)*M^2*rh^4*\[Lambda] - 
     (11088*I)*a^3*m*rh^5*\[Lambda] + (8064*I)*a^3*(-4 + k)*m*rh^5*
      \[Lambda] - (14784*I)*a^3*(5 - k)*(-4 + k)*m*rh^5*\[Lambda] + 
     (1008*I)*a^3*m^3*rh^5*\[Lambda] + 13104*a^2*M*rh^5*\[Lambda] - 
     1008*a^2*(-4 + k)*M*rh^5*\[Lambda] - 8064*a^2*m^2*M*rh^5*\[Lambda] - 
     (6048*I)*a*m*M^2*rh^5*\[Lambda] - (14784*I)*a*(5 - k)*(-4 + k)*m*M^2*
      rh^5*\[Lambda] - 6300*a^2*rh^6*\[Lambda] + 7140*a^2*m^2*rh^6*
      \[Lambda] + (11760*I)*a*m*M*rh^6*\[Lambda] - (7392*I)*a*(-4 + k)*m*M*
      rh^6*\[Lambda] + (29568*I)*a*(5 - k)*(-4 + k)*m*M*rh^6*\[Lambda] - 
     5040*M^2*rh^6*\[Lambda] - (5280*I)*a*m*rh^7*\[Lambda] + 
     (6336*I)*a*(-4 + k)*m*rh^7*\[Lambda] - (13728*I)*a*(5 - k)*(-4 + k)*m*
      rh^7*\[Lambda] + 6600*M*rh^7*\[Lambda] - 1980*rh^8*\[Lambda] + 
     8*a^9*m*\[Kappa]*\[Lambda] + 16*a^9*(-4 + k)*m*\[Kappa]*\[Lambda] - 
     (24*I)*a^8*(-4 + k)*rh*\[Kappa]*\[Lambda] - 80*a^7*m*M*rh*\[Kappa]*
      \[Lambda] - 192*a^7*(-4 + k)*m*M*rh*\[Kappa]*\[Lambda] + 
     480*a^7*m*rh^2*\[Kappa]*\[Lambda] + 1344*a^7*(-4 + k)*m*rh^2*\[Kappa]*
      \[Lambda] + (168*I)*a^6*(-4 + k)*M*rh^2*\[Kappa]*\[Lambda] - 
     (896*I)*a^6*(-4 + k)*rh^3*\[Kappa]*\[Lambda] - 
     1680*a^5*m*M*rh^3*\[Kappa]*\[Lambda] - 5376*a^5*(-4 + k)*m*M*rh^3*
      \[Kappa]*\[Lambda] + 3360*a^5*m*rh^4*\[Kappa]*\[Lambda] + 
     12096*a^5*(-4 + k)*m*rh^4*\[Kappa]*\[Lambda] + 
     (3024*I)*a^4*(-4 + k)*M*rh^4*\[Kappa]*\[Lambda] - 
     (5040*I)*a^4*(-4 + k)*rh^5*\[Kappa]*\[Lambda] - 
     6048*a^3*m*M*rh^5*\[Kappa]*\[Lambda] - 24192*a^3*(-4 + k)*m*M*rh^5*
      \[Kappa]*\[Lambda] + 6720*a^3*m*rh^6*\[Kappa]*\[Lambda] + 
     29568*a^3*(-4 + k)*m*rh^6*\[Kappa]*\[Lambda] + 
     (7392*I)*a^2*(-4 + k)*M*rh^6*\[Kappa]*\[Lambda] - 
     (6336*I)*a^2*(-4 + k)*rh^7*\[Kappa]*\[Lambda] - 
     5280*a*m*M*rh^7*\[Kappa]*\[Lambda] - 25344*a*(-4 + k)*m*M*rh^7*\[Kappa]*
      \[Lambda] + 3960*a*m*rh^8*\[Kappa]*\[Lambda] + 
     20592*a*(-4 + k)*m*rh^8*\[Kappa]*\[Lambda] - (40*I)*a^9*m*rh*\[Kappa]^2*
      \[Lambda] - 30*a^8*rh^2*\[Kappa]^2*\[Lambda] - 
     (1120*I)*a^7*m*rh^3*\[Kappa]^2*\[Lambda] - 420*a^6*rh^4*\[Kappa]^2*
      \[Lambda] - (6048*I)*a^5*m*rh^5*\[Kappa]^2*\[Lambda] - 
     1260*a^4*rh^6*\[Kappa]^2*\[Lambda] - (10560*I)*a^3*m*rh^7*\[Kappa]^2*
      \[Lambda] - 990*a^2*rh^8*\[Kappa]^2*\[Lambda] - 
     (5720*I)*a*m*rh^9*\[Kappa]^2*\[Lambda] - 8*a^8*\[Lambda]^2 - 
     (40*I)*a^7*m*rh*\[Lambda]^2 + 120*a^6*M*rh*\[Lambda]^2 - 
     12*a^6*(-4 + k)*M*rh*\[Lambda]^2 - 435*a^6*rh^2*\[Lambda]^2 + 
     15*a^6*m^2*rh^2*\[Lambda]^2 + (240*I)*a^5*m*M*rh^2*\[Lambda]^2 - 
     240*a^4*M^2*rh^2*\[Lambda]^2 + 84*a^4*(-4 + k)*M^2*rh^2*\[Lambda]^2 - 
     (840*I)*a^5*m*rh^3*\[Lambda]^2 + 2450*a^4*M*rh^3*\[Lambda]^2 - 
     224*a^4*(-4 + k)*M*rh^3*\[Lambda]^2 - 2660*a^4*rh^4*\[Lambda]^2 + 
     140*a^4*m^2*rh^4*\[Lambda]^2 + (2240*I)*a^3*m*M*rh^4*\[Lambda]^2 - 
     2800*a^2*M^2*rh^4*\[Lambda]^2 + 504*a^2*(-4 + k)*M^2*rh^4*\[Lambda]^2 - 
     (3024*I)*a^3*m*rh^5*\[Lambda]^2 + 7560*a^2*M*rh^5*\[Lambda]^2 - 
     504*a^2*(-4 + k)*M*rh^5*\[Lambda]^2 - 4410*a^2*rh^6*\[Lambda]^2 + 
     210*a^2*m^2*rh^6*\[Lambda]^2 + (3360*I)*a*m*M*rh^6*\[Lambda]^2 - 
     2520*M^2*rh^6*\[Lambda]^2 - (2640*I)*a*m*rh^7*\[Lambda]^2 + 
     4620*M*rh^7*\[Lambda]^2 - 1980*rh^8*\[Lambda]^2 - 
     (12*I)*a^8*(-4 + k)*rh*\[Kappa]*\[Lambda]^2 + (84*I)*a^6*(-4 + k)*M*rh^2*
      \[Kappa]*\[Lambda]^2 - (448*I)*a^6*(-4 + k)*rh^3*\[Kappa]*\[Lambda]^2 + 
     (1512*I)*a^4*(-4 + k)*M*rh^4*\[Kappa]*\[Lambda]^2 - 
     (2520*I)*a^4*(-4 + k)*rh^5*\[Kappa]*\[Lambda]^2 + 
     (3696*I)*a^2*(-4 + k)*M*rh^6*\[Kappa]*\[Lambda]^2 - 
     (3168*I)*a^2*(-4 + k)*rh^7*\[Kappa]*\[Lambda]^2 - 
     15*a^8*rh^2*\[Kappa]^2*\[Lambda]^2 - 210*a^6*rh^4*\[Kappa]^2*
      \[Lambda]^2 - 630*a^4*rh^6*\[Kappa]^2*\[Lambda]^2 - 
     495*a^2*rh^8*\[Kappa]^2*\[Lambda]^2 - 15*a^6*rh^2*\[Lambda]^3 + 
     70*a^4*M*rh^3*\[Lambda]^3 - 210*a^4*rh^4*\[Lambda]^3 + 
     504*a^2*M*rh^5*\[Lambda]^3 - 630*a^2*rh^6*\[Lambda]^3 + 
     660*M*rh^7*\[Lambda]^3 - 495*rh^8*\[Lambda]^3 - 
     (360*I)*a^7*m*M*rh^2*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (2520*I)*a^5*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (3024*I)*a^3*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
     96*a^7*(-4 + k)*m*M*rh*(\[Kappa] - 6*\[Omega]) - 
     576*a^5*(-4 + k)*m*M^2*rh^2*(\[Kappa] - 6*\[Omega]) + 
     1008*a^5*(-4 + k)*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 
     24*a^7*(-4 + k)*m*M*rh*(4*\[Kappa] - 5*\[Omega]) - 
     288*a^5*(-4 + k)*m*M^2*rh^2*(4*\[Kappa] - 5*\[Omega]) + 
     1008*a^5*(-4 + k)*m*M*rh^3*(4*\[Kappa] - 5*\[Omega]) - 
     2688*a^3*(-4 + k)*m*M^2*rh^4*(4*\[Kappa] - 5*\[Omega]) + 
     3024*a^3*(-4 + k)*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) - 
     288*a^9*m*\[Omega] + 384*a^9*(-4 + k)*m*\[Omega] - 
     204*a^9*(5 - k)*(-4 + k)*m*\[Omega] + 96*a^9*m^3*\[Omega] - 
     (1248*I)*a^8*(-4 + k)*M*\[Omega] + (480*I)*a^8*(5 - k)*(-4 + k)*M*
      \[Omega] + (216*I)*a^8*m^2*M*\[Omega] + 192*a^7*m*M^2*\[Omega] + 
     576*a^7*(-4 + k)*m*M^2*\[Omega] - 192*a^7*(5 - k)*(-4 + k)*m*M^2*
      \[Omega] - (288*I)*a^6*(-4 + k)*M^3*\[Omega] + 
     (96*I)*a^6*(5 - k)*(-4 + k)*M^3*\[Omega] - (1560*I)*a^8*rh*\[Omega] + 
     (3168*I)*a^8*(-4 + k)*rh*\[Omega] - (1680*I)*a^8*(5 - k)*(-4 + k)*rh*
      \[Omega] - (1320*I)*a^8*m^2*rh*\[Omega] + 2880*a^7*m*M*rh*\[Omega] - 
     6936*a^7*(-4 + k)*m*M*rh*\[Omega] + 4368*a^7*(5 - k)*(-4 + k)*m*M*rh*
      \[Omega] + (8496*I)*a^6*(-4 + k)*M^2*rh*\[Omega] - 
     (4032*I)*a^6*(5 - k)*(-4 + k)*M^2*rh*\[Omega] - 
     6300*a^7*m*rh^2*\[Omega] + 12096*a^7*(-4 + k)*m*rh^2*\[Omega] - 
     9408*a^7*(5 - k)*(-4 + k)*m*rh^2*\[Omega] + 3780*a^7*m^3*rh^2*\[Omega] + 
     (19260*I)*a^6*M*rh^2*\[Omega] - (30240*I)*a^6*(-4 + k)*M*rh^2*\[Omega] + 
     (20160*I)*a^6*(5 - k)*(-4 + k)*M*rh^2*\[Omega] + 
     (7020*I)*a^6*m^2*M*rh^2*\[Omega] - 7200*a^5*m*M^2*rh^2*\[Omega] + 
     12240*a^5*(-4 + k)*m*M^2*rh^2*\[Omega] - 12096*a^5*(5 - k)*(-4 + k)*m*
      M^2*rh^2*\[Omega] + (8640*I)*a^4*M^3*rh^2*\[Omega] - 
     (9072*I)*a^4*(-4 + k)*M^3*rh^2*\[Omega] + (5376*I)*a^4*(5 - k)*(-4 + k)*
      M^3*rh^2*\[Omega] - (12600*I)*a^6*rh^3*\[Omega] + 
     (23688*I)*a^6*(-4 + k)*rh^3*\[Omega] - (20160*I)*a^6*(5 - k)*(-4 + k)*
      rh^3*\[Omega] - (12600*I)*a^6*m^2*rh^3*\[Omega] + 
     27720*a^5*m*M*rh^3*\[Omega] - 49896*a^5*(-4 + k)*m*M*rh^3*\[Omega] + 
     60480*a^5*(5 - k)*(-4 + k)*m*M*rh^3*\[Omega] - 
     (59640*I)*a^4*M^2*rh^3*\[Omega] + (53760*I)*a^4*(-4 + k)*M^2*rh^3*
      \[Omega] - (48384*I)*a^4*(5 - k)*(-4 + k)*M^2*rh^3*\[Omega] - 
     20160*a^5*m*rh^4*\[Omega] + 48384*a^5*(-4 + k)*m*rh^4*\[Omega] - 
     55440*a^5*(5 - k)*(-4 + k)*m*rh^4*\[Omega] + 15120*a^5*m^3*rh^4*
      \[Omega] + (75600*I)*a^4*M*rh^4*\[Omega] - (81984*I)*a^4*(-4 + k)*M*
      rh^4*\[Omega] + (100800*I)*a^4*(5 - k)*(-4 + k)*M*rh^4*\[Omega] + 
     (20160*I)*a^4*m^2*M*rh^4*\[Omega] - 20160*a^3*m*M^2*rh^4*\[Omega] + 
     13440*a^3*(-4 + k)*m*M^2*rh^4*\[Omega] - 60480*a^3*(5 - k)*(-4 + k)*m*
      M^2*rh^4*\[Omega] + (40320*I)*a^2*M^3*rh^4*\[Omega] - 
     (18144*I)*a^2*(-4 + k)*M^3*rh^4*\[Omega] + (20160*I)*a^2*(5 - k)*
      (-4 + k)*M^3*rh^4*\[Omega] - (21168*I)*a^4*rh^5*\[Omega] + 
     (36288*I)*a^4*(-4 + k)*rh^5*\[Omega] - (55440*I)*a^4*(5 - k)*(-4 + k)*
      rh^5*\[Omega] - (27216*I)*a^4*m^2*rh^5*\[Omega] + 
     42336*a^3*m*M*rh^5*\[Omega] - 57456*a^3*(-4 + k)*m*M*rh^5*\[Omega] + 
     155232*a^3*(5 - k)*(-4 + k)*m*M*rh^5*\[Omega] - 
     (102816*I)*a^2*M^2*rh^5*\[Omega] + (42336*I)*a^2*(-4 + k)*M^2*rh^5*
      \[Omega] - (88704*I)*a^2*(5 - k)*(-4 + k)*M^2*rh^5*\[Omega] - 
     22680*a^3*m*rh^6*\[Omega] + 44352*a^3*(-4 + k)*m*rh^6*\[Omega] - 
     88704*a^3*(5 - k)*(-4 + k)*m*rh^6*\[Omega] + 12600*a^3*m^3*rh^6*
      \[Omega] + (63000*I)*a^2*M*rh^6*\[Omega] - (32256*I)*a^2*(-4 + k)*M*
      rh^6*\[Omega] + (110880*I)*a^2*(5 - k)*(-4 + k)*M*rh^6*\[Omega] + 
     (7560*I)*a^2*m^2*M*rh^6*\[Omega] - 10080*a*m*M^2*rh^6*\[Omega] - 
     10080*a*(-4 + k)*m*M^2*rh^6*\[Omega] - 44352*a*(5 - k)*(-4 + k)*m*M^2*
      rh^6*\[Omega] + (30240*I)*M^3*rh^6*\[Omega] - 
     (7920*I)*a^2*rh^7*\[Omega] + (7920*I)*a^2*(-4 + k)*rh^7*\[Omega] - 
     (41184*I)*a^2*(5 - k)*(-4 + k)*rh^7*\[Omega] - 
     (15840*I)*a^2*m^2*rh^7*\[Omega] + 23760*a*m*M*rh^7*\[Omega] + 
     7920*a*(-4 + k)*m*M*rh^7*\[Omega] + 82368*a*(5 - k)*(-4 + k)*m*M*rh^7*
      \[Omega] - (39600*I)*M^2*rh^7*\[Omega] - 11880*a*m*rh^8*\[Omega] - 
     36036*a*(5 - k)*(-4 + k)*m*rh^8*\[Omega] + (11880*I)*M*rh^8*\[Omega] + 
     312*a^10*\[Kappa]*\[Omega] + 240*a^10*(-4 + k)*\[Kappa]*\[Omega] + 
     (192*I)*a^9*m*M*\[Kappa]*\[Omega] + (192*I)*a^9*(-4 + k)*m*M*\[Kappa]*
      \[Omega] + 96*a^8*M^2*\[Kappa]*\[Omega] + 96*a^8*(-4 + k)*M^2*\[Kappa]*
      \[Omega] - (1920*I)*a^9*m*rh*\[Kappa]*\[Omega] - 
     (2448*I)*a^9*(-4 + k)*m*rh*\[Kappa]*\[Omega] - 
     3264*a^8*M*rh*\[Kappa]*\[Omega] - 3024*a^8*(-4 + k)*M*rh*\[Kappa]*
      \[Omega] + 7800*a^8*rh^2*\[Kappa]*\[Omega] + 7200*a^8*(-4 + k)*rh^2*
      \[Kappa]*\[Omega] + (5760*I)*a^7*m*M*rh^2*\[Kappa]*\[Omega] + 
     (13104*I)*a^7*(-4 + k)*m*M*rh^2*\[Kappa]*\[Omega] + 
     4320*a^6*M^2*rh^2*\[Kappa]*\[Omega] + 6720*a^6*(-4 + k)*M^2*rh^2*
      \[Kappa]*\[Omega] - (20160*I)*a^7*m*rh^3*\[Kappa]*\[Omega] - 
     (37632*I)*a^7*(-4 + k)*m*rh^3*\[Kappa]*\[Omega] - 
     25920*a^6*M*rh^3*\[Kappa]*\[Omega] - 42816*a^6*(-4 + k)*M*rh^3*\[Kappa]*
      \[Omega] + 27720*a^6*rh^4*\[Kappa]*\[Omega] + 
     50400*a^6*(-4 + k)*rh^4*\[Kappa]*\[Omega] + (20160*I)*a^5*m*M*rh^4*
      \[Kappa]*\[Omega] + (90720*I)*a^5*(-4 + k)*m*M*rh^4*\[Kappa]*\[Omega] + 
     20160*a^4*M^2*rh^4*\[Kappa]*\[Omega] + 45360*a^4*(-4 + k)*M^2*rh^4*
      \[Kappa]*\[Omega] - (48384*I)*a^5*m*rh^5*\[Kappa]*\[Omega] - 
     (133056*I)*a^5*(-4 + k)*m*rh^5*\[Kappa]*\[Omega] - 
     40320*a^4*M*rh^5*\[Kappa]*\[Omega] - 157920*a^4*(-4 + k)*M*rh^5*\[Kappa]*
      \[Omega] + 26208*a^4*rh^6*\[Kappa]*\[Omega] + 
     110880*a^4*(-4 + k)*rh^6*\[Kappa]*\[Omega] + (16128*I)*a^3*m*M*rh^6*
      \[Kappa]*\[Omega] + (155232*I)*a^3*(-4 + k)*m*M*rh^6*\[Kappa]*
      \[Omega] + 20160*a^2*M^2*rh^6*\[Kappa]*\[Omega] + 
     64512*a^2*(-4 + k)*M^2*rh^6*\[Kappa]*\[Omega] - 
     (31680*I)*a^3*m*rh^7*\[Kappa]*\[Omega] - (152064*I)*a^3*(-4 + k)*m*rh^7*
      \[Kappa]*\[Omega] - 11520*a^2*M*rh^7*\[Kappa]*\[Omega] - 
     127872*a^2*(-4 + k)*M*rh^7*\[Kappa]*\[Omega] + 
     3960*a^2*rh^8*\[Kappa]*\[Omega] + 61776*a^2*(-4 + k)*rh^8*\[Kappa]*
      \[Omega] + (61776*I)*a*(-4 + k)*m*M*rh^8*\[Kappa]*\[Omega] - 
     (48048*I)*a*(-4 + k)*m*rh^9*\[Kappa]*\[Omega] - 
     48*a^11*m*\[Kappa]^2*\[Omega] + (24*I)*a^10*M*\[Kappa]^2*\[Omega] - 
     (504*I)*a^10*rh*\[Kappa]^2*\[Omega] - 3060*a^9*m*rh^2*\[Kappa]^2*
      \[Omega] + (1548*I)*a^8*M*rh^2*\[Kappa]^2*\[Omega] - 
     (5520*I)*a^8*rh^3*\[Kappa]^2*\[Omega] - 23520*a^7*m*rh^4*\[Kappa]^2*
      \[Omega] + (11520*I)*a^6*M*rh^4*\[Kappa]^2*\[Omega] - 
     (20160*I)*a^6*rh^5*\[Kappa]^2*\[Omega] - 55440*a^5*m*rh^6*\[Kappa]^2*
      \[Omega] + (26040*I)*a^4*M*rh^6*\[Kappa]^2*\[Omega] - 
     (31536*I)*a^4*rh^7*\[Kappa]^2*\[Omega] - 47520*a^3*m*rh^8*\[Kappa]^2*
      \[Omega] + (17820*I)*a^2*M*rh^8*\[Kappa]^2*\[Omega] - 
     (17160*I)*a^2*rh^9*\[Kappa]^2*\[Omega] - 12012*a*m*rh^10*\[Kappa]^2*
      \[Omega] + (240*I)*a^6*M^2*rh*(-18 + \[Lambda])*\[Omega] + 
     64*a^8*(-4 + k)*M*rh*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     320*a^8*(-4 + k)*rh^2*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     960*a^6*(-4 + k)*M*rh^3*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     1120*a^6*(-4 + k)*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     16*a^8*M*rh*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     720*a^6*M*rh^3*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     3360*a^4*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     3360*a^2*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (16*I)*a^10*rh*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (480*I)*a^8*rh^3*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (1680*I)*a^6*rh^5*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (1344*I)*a^4*rh^7*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] - 
     (168*I)*a^6*(-4 + k)*rh^3*(-3 + \[Lambda])*\[Omega] + 
     (896*I)*a^4*(-4 + k)*M*rh^4*(-3 + \[Lambda])*\[Omega] - 
     (2016*I)*a^4*(-4 + k)*rh^5*(-3 + \[Lambda])*\[Omega] + 
     (4032*I)*a^2*(-4 + k)*M*rh^6*(-3 + \[Lambda])*\[Omega] - 
     (3696*I)*a^2*(-4 + k)*rh^7*(-3 + \[Lambda])*\[Omega] - 
     40*a^8*rh^2*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     840*a^6*rh^4*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     3024*a^4*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     2640*a^2*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     64*a^9*m*\[Lambda]*\[Omega] + (48*I)*a^8*(-4 + k)*M*\[Lambda]*\[Omega] - 
     (32*I)*a^8*(5 - k)*(-4 + k)*M*\[Lambda]*\[Omega] + 
     (480*I)*a^8*rh*\[Lambda]*\[Omega] - (192*I)*a^8*(-4 + k)*rh*\[Lambda]*
      \[Omega] + (224*I)*a^8*(5 - k)*(-4 + k)*rh*\[Lambda]*\[Omega] - 
     (120*I)*a^8*m^2*rh*\[Lambda]*\[Omega] + 640*a^7*m*M*rh*\[Lambda]*
      \[Omega] - (384*I)*a^6*(-4 + k)*M^2*rh*\[Lambda]*\[Omega] + 
     (224*I)*a^6*(5 - k)*(-4 + k)*M^2*rh*\[Lambda]*\[Omega] - 
     3240*a^7*m*rh^2*\[Lambda]*\[Omega] - (3540*I)*a^6*M*rh^2*\[Lambda]*
      \[Omega] + (2352*I)*a^6*(-4 + k)*M*rh^2*\[Lambda]*\[Omega] - 
     (2688*I)*a^6*(5 - k)*(-4 + k)*M*rh^2*\[Lambda]*\[Omega] + 
     (3080*I)*a^6*rh^3*\[Lambda]*\[Omega] - (2520*I)*a^6*(-4 + k)*rh^3*
      \[Lambda]*\[Omega] + (4032*I)*a^6*(5 - k)*(-4 + k)*rh^3*\[Lambda]*
      \[Omega] - (2240*I)*a^6*m^2*rh^3*\[Lambda]*\[Omega] + 
     10360*a^5*m*M*rh^3*\[Lambda]*\[Omega] + (5880*I)*a^4*M^2*rh^3*\[Lambda]*
      \[Omega] - (3584*I)*a^4*(-4 + k)*M^2*rh^3*\[Lambda]*\[Omega] + 
     (5376*I)*a^4*(5 - k)*(-4 + k)*M^2*rh^3*\[Lambda]*\[Omega] - 
     18480*a^5*m*rh^4*\[Lambda]*\[Omega] - (9240*I)*a^4*M*rh^4*\[Lambda]*
      \[Omega] + (9184*I)*a^4*(-4 + k)*M*rh^4*\[Lambda]*\[Omega] - 
     (20160*I)*a^4*(5 - k)*(-4 + k)*M*rh^4*\[Lambda]*\[Omega] + 
     (2016*I)*a^4*rh^5*\[Lambda]*\[Omega] - (6048*I)*a^4*(-4 + k)*rh^5*
      \[Lambda]*\[Omega] + (14784*I)*a^4*(5 - k)*(-4 + k)*rh^5*\[Lambda]*
      \[Omega] - (7056*I)*a^4*m^2*rh^5*\[Lambda]*\[Omega] + 
     26208*a^3*m*M*rh^5*\[Lambda]*\[Omega] + (14784*I)*a^2*(5 - k)*(-4 + k)*
      M^2*rh^5*\[Lambda]*\[Omega] - 28560*a^3*m*rh^6*\[Lambda]*\[Omega] + 
     (5880*I)*a^2*M*rh^6*\[Lambda]*\[Omega] + (3360*I)*a^2*(-4 + k)*M*rh^6*
      \[Lambda]*\[Omega] - (29568*I)*a^2*(5 - k)*(-4 + k)*M*rh^6*\[Lambda]*
      \[Omega] - (2640*I)*a^2*rh^7*\[Lambda]*\[Omega] - 
     (2640*I)*a^2*(-4 + k)*rh^7*\[Lambda]*\[Omega] + 
     (13728*I)*a^2*(5 - k)*(-4 + k)*rh^7*\[Lambda]*\[Omega] - 
     (5280*I)*a^2*m^2*rh^7*\[Lambda]*\[Omega] + 13200*a*m*M*rh^7*\[Lambda]*
      \[Omega] - (7920*I)*M^2*rh^7*\[Lambda]*\[Omega] - 
     11880*a*m*rh^8*\[Lambda]*\[Omega] + (5940*I)*M*rh^8*\[Lambda]*\[Omega] - 
     8*a^10*\[Kappa]*\[Lambda]*\[Omega] - 16*a^10*(-4 + k)*\[Kappa]*\[Lambda]*
      \[Omega] + 64*a^8*M*rh*\[Kappa]*\[Lambda]*\[Omega] + 
     128*a^8*(-4 + k)*M*rh*\[Kappa]*\[Lambda]*\[Omega] - 
     440*a^8*rh^2*\[Kappa]*\[Lambda]*\[Omega] - 1024*a^8*(-4 + k)*rh^2*
      \[Kappa]*\[Lambda]*\[Omega] + 960*a^6*M*rh^3*\[Kappa]*\[Lambda]*
      \[Omega] + 4416*a^6*(-4 + k)*M*rh^3*\[Kappa]*\[Lambda]*\[Omega] - 
     2520*a^6*rh^4*\[Kappa]*\[Lambda]*\[Omega] - 10976*a^6*(-4 + k)*rh^4*
      \[Kappa]*\[Lambda]*\[Omega] + 2688*a^4*M*rh^5*\[Kappa]*\[Lambda]*
      \[Omega] + 24192*a^4*(-4 + k)*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] - 
     3696*a^4*rh^6*\[Kappa]*\[Lambda]*\[Omega] - 29568*a^4*(-4 + k)*rh^6*
      \[Kappa]*\[Lambda]*\[Omega] + 1920*a^2*M*rh^7*\[Kappa]*\[Lambda]*
      \[Omega] + 25344*a^2*(-4 + k)*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] - 
     1320*a^2*rh^8*\[Kappa]*\[Lambda]*\[Omega] - 20592*a^2*(-4 + k)*rh^8*
      \[Kappa]*\[Lambda]*\[Omega] + (24*I)*a^10*rh*\[Kappa]^2*\[Lambda]*
      \[Omega] + (640*I)*a^8*rh^3*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (4368*I)*a^6*rh^5*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (9216*I)*a^4*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (5720*I)*a^2*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (40*I)*a^8*rh*\[Lambda]^2*\[Omega] - 30*a^7*m*rh^2*\[Lambda]^2*
      \[Omega] - (240*I)*a^6*M*rh^2*\[Lambda]^2*\[Omega] + 
     (840*I)*a^6*rh^3*\[Lambda]^2*\[Omega] - 420*a^5*m*rh^4*\[Lambda]^2*
      \[Omega] - (2240*I)*a^4*M*rh^4*\[Lambda]^2*\[Omega] + 
     (3024*I)*a^4*rh^5*\[Lambda]^2*\[Omega] - 1260*a^3*m*rh^6*\[Lambda]^2*
      \[Omega] - (3360*I)*a^2*M*rh^6*\[Lambda]^2*\[Omega] + 
     (2640*I)*a^2*rh^7*\[Lambda]^2*\[Omega] - 990*a*m*rh^8*\[Lambda]^2*
      \[Omega] - (8*I)*a^8*M*(-51 + 11*\[Lambda])*\[Omega] + 
     (72*I)*a^8*M*rh^2*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (1080*I)*a^6*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (1680*I)*a^4*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     96*a^6*(-4 + k)*M^2*rh^2*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     288*a^6*(-4 + k)*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] + 
     48*a^6*(-4 + k)*M^2*rh^2*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     288*a^6*(-4 + k)*M*rh^3*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     1008*a^4*(-4 + k)*M^2*rh^4*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     1344*a^4*(-4 + k)*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     192*a^10*(-4 + k)*\[Omega]^2 + 108*a^10*(5 - k)*(-4 + k)*\[Omega]^2 - 
     144*a^10*m^2*\[Omega]^2 - (216*I)*a^9*m*M*\[Omega]^2 - 
     96*a^8*M^2*\[Omega]^2 - 288*a^8*(-4 + k)*M^2*\[Omega]^2 + 
     96*a^8*(5 - k)*(-4 + k)*M^2*\[Omega]^2 + (1200*I)*a^9*m*rh*\[Omega]^2 + 
     3888*a^8*(-4 + k)*M*rh*\[Omega]^2 - 2352*a^8*(5 - k)*(-4 + k)*M*rh*
      \[Omega]^2 + 3780*a^8*rh^2*\[Omega]^2 - 6048*a^8*(-4 + k)*rh^2*
      \[Omega]^2 + 5376*a^8*(5 - k)*(-4 + k)*rh^2*\[Omega]^2 - 
     7020*a^8*m^2*rh^2*\[Omega]^2 - (7560*I)*a^7*m*M*rh^2*\[Omega]^2 + 
     4320*a^6*M^2*rh^2*\[Omega]^2 - 8640*a^6*(-4 + k)*M^2*rh^2*\[Omega]^2 + 
     6720*a^6*(5 - k)*(-4 + k)*M^2*rh^2*\[Omega]^2 + 
     (10080*I)*a^7*m*rh^3*\[Omega]^2 - 19320*a^6*M*rh^3*\[Omega]^2 + 
     29328*a^6*(-4 + k)*M*rh^3*\[Omega]^2 - 36288*a^6*(5 - k)*(-4 + k)*M*rh^3*
      \[Omega]^2 + 13440*a^6*rh^4*\[Omega]^2 - 24192*a^6*(-4 + k)*rh^4*
      \[Omega]^2 + 35280*a^6*(5 - k)*(-4 + k)*rh^4*\[Omega]^2 - 
     38640*a^6*m^2*rh^4*\[Omega]^2 - (21840*I)*a^5*m*M*rh^4*\[Omega]^2 + 
     20160*a^4*M^2*rh^4*\[Omega]^2 - 10080*a^4*(-4 + k)*M^2*rh^4*\[Omega]^2 + 
     40320*a^4*(5 - k)*(-4 + k)*M^2*rh^4*\[Omega]^2 + 
     (18144*I)*a^5*m*rh^5*\[Omega]^2 - 36288*a^4*M*rh^5*\[Omega]^2 + 
     28896*a^4*(-4 + k)*M*rh^5*\[Omega]^2 - 110880*a^4*(5 - k)*(-4 + k)*M*
      rh^5*\[Omega]^2 + 15120*a^4*rh^6*\[Omega]^2 - 
     22176*a^4*(-4 + k)*rh^6*\[Omega]^2 + 66528*a^4*(5 - k)*(-4 + k)*rh^6*
      \[Omega]^2 - 57960*a^4*m^2*rh^6*\[Omega]^2 + (5040*I)*a^3*m*M*rh^6*
      \[Omega]^2 + 10080*a^2*M^2*rh^6*\[Omega]^2 + 16128*a^2*(-4 + k)*M^2*
      rh^6*\[Omega]^2 + 44352*a^2*(5 - k)*(-4 + k)*M^2*rh^6*\[Omega]^2 + 
     (7920*I)*a^3*m*rh^7*\[Omega]^2 - 15840*a^2*M*rh^7*\[Omega]^2 - 
     12960*a^2*(-4 + k)*M*rh^7*\[Omega]^2 - 82368*a^2*(5 - k)*(-4 + k)*M*rh^7*
      \[Omega]^2 + 5940*a^2*rh^8*\[Omega]^2 + 36036*a^2*(5 - k)*(-4 + k)*rh^8*
      \[Omega]^2 - 23760*a^2*m^2*rh^8*\[Omega]^2 + (23760*I)*a*m*M*rh^8*
      \[Omega]^2 - (96*I)*a^10*M*\[Kappa]*\[Omega]^2 - 
     (96*I)*a^10*(-4 + k)*M*\[Kappa]*\[Omega]^2 + (960*I)*a^10*rh*\[Kappa]*
      \[Omega]^2 + (1296*I)*a^10*(-4 + k)*rh*\[Kappa]*\[Omega]^2 - 
     (4032*I)*a^8*M*rh^2*\[Kappa]*\[Omega]^2 - (7056*I)*a^8*(-4 + k)*M*rh^2*
      \[Kappa]*\[Omega]^2 + (10080*I)*a^8*rh^3*\[Kappa]*\[Omega]^2 + 
     (21504*I)*a^8*(-4 + k)*rh^3*\[Kappa]*\[Omega]^2 - 
     (15840*I)*a^6*M*rh^4*\[Kappa]*\[Omega]^2 - (54432*I)*a^6*(-4 + k)*M*rh^4*
      \[Kappa]*\[Omega]^2 + (24192*I)*a^6*rh^5*\[Kappa]*\[Omega]^2 + 
     (84672*I)*a^6*(-4 + k)*rh^5*\[Kappa]*\[Omega]^2 - 
     (13440*I)*a^4*M*rh^6*\[Kappa]*\[Omega]^2 - (110880*I)*a^4*(-4 + k)*M*
      rh^6*\[Kappa]*\[Omega]^2 + (15840*I)*a^4*rh^7*\[Kappa]*\[Omega]^2 + 
     (114048*I)*a^4*(-4 + k)*rh^7*\[Kappa]*\[Omega]^2 - 
     (61776*I)*a^2*(-4 + k)*M*rh^8*\[Kappa]*\[Omega]^2 + 
     (48048*I)*a^2*(-4 + k)*rh^9*\[Kappa]*\[Omega]^2 + 
     24*a^12*\[Kappa]^2*\[Omega]^2 + 1620*a^10*rh^2*\[Kappa]^2*\[Omega]^2 + 
     13440*a^8*rh^4*\[Kappa]^2*\[Omega]^2 + 35280*a^6*rh^6*\[Kappa]^2*
      \[Omega]^2 + 35640*a^4*rh^8*\[Kappa]^2*\[Omega]^2 + 
     12012*a^2*rh^10*\[Kappa]^2*\[Omega]^2 - 4*a^10*(-39 - 8*\[Lambda])*
      \[Omega]^2 + (120*I)*a^9*m*rh*\[Lambda]*\[Omega]^2 + 
     1770*a^8*rh^2*\[Lambda]*\[Omega]^2 + (2800*I)*a^7*m*rh^3*\[Lambda]*
      \[Omega]^2 - 5880*a^6*M*rh^3*\[Lambda]*\[Omega]^2 + 
     11480*a^6*rh^4*\[Lambda]*\[Omega]^2 + (12096*I)*a^5*m*rh^5*\[Lambda]*
      \[Omega]^2 - 18144*a^4*M*rh^5*\[Lambda]*\[Omega]^2 + 
     21840*a^4*rh^6*\[Lambda]*\[Omega]^2 + (15840*I)*a^3*m*rh^7*\[Lambda]*
      \[Omega]^2 - 13200*a^2*M*rh^7*\[Lambda]*\[Omega]^2 + 
     13860*a^2*rh^8*\[Lambda]*\[Omega]^2 + (5720*I)*a*m*rh^9*\[Lambda]*
      \[Omega]^2 + 2002*rh^10*\[Lambda]*\[Omega]^2 + 
     15*a^8*rh^2*\[Lambda]^2*\[Omega]^2 + 280*a^6*rh^4*\[Lambda]^2*
      \[Omega]^2 + 1260*a^4*rh^6*\[Lambda]^2*\[Omega]^2 + 
     1980*a^2*rh^8*\[Lambda]^2*\[Omega]^2 + 1001*rh^10*\[Lambda]^2*
      \[Omega]^2 - 80*a^8*M*rh*(21 + 4*\[Lambda])*\[Omega]^2 + 
     96*a^11*m*\[Omega]^3 + (72*I)*a^10*M*\[Omega]^3 + 
     5580*a^9*m*rh^2*\[Omega]^3 + (2700*I)*a^8*M*rh^2*\[Omega]^3 - 
     (1680*I)*a^8*rh^3*\[Omega]^3 + 38640*a^7*m*rh^4*\[Omega]^3 + 
     (6720*I)*a^6*M*rh^4*\[Omega]^3 + (6048*I)*a^6*rh^5*\[Omega]^3 + 
     80640*a^5*m*rh^6*\[Omega]^3 - (15120*I)*a^4*M*rh^6*\[Omega]^3 + 
     (23760*I)*a^4*rh^7*\[Omega]^3 + 59400*a^3*m*rh^8*\[Omega]^3 - 
     (35640*I)*a^2*M*rh^8*\[Omega]^3 + (17160*I)*a^2*rh^9*\[Omega]^3 + 
     12012*a*m*rh^10*\[Omega]^3 - (12012*I)*M*rh^10*\[Omega]^3 - 
     (1120*I)*a^8*rh^3*\[Lambda]*\[Omega]^3 - (6048*I)*a^6*rh^5*\[Lambda]*
      \[Omega]^3 - (10560*I)*a^4*rh^7*\[Lambda]*\[Omega]^3 - 
     (5720*I)*a^2*rh^9*\[Lambda]*\[Omega]^3 - (40*I)*a^10*rh*(9 + \[Lambda])*
      \[Omega]^3 - 24*a^12*\[Omega]^4 - 1620*a^10*rh^2*\[Omega]^4 - 
     13440*a^8*rh^4*\[Omega]^4 - 35280*a^6*rh^6*\[Omega]^4 - 
     35640*a^4*rh^8*\[Omega]^4 - 12012*a^2*rh^10*\[Omega]^4 + 
     504*a^5*(-4 + k)*m*M*rh^3*(2*\[Kappa] + \[Omega]) - 
     2688*a^3*(-4 + k)*m*M^2*rh^4*(2*\[Kappa] + \[Omega]) + 
     6048*a^3*(-4 + k)*m*M*rh^5*(2*\[Kappa] + \[Omega]) - 
     12096*a*(-4 + k)*m*M^2*rh^6*(2*\[Kappa] + \[Omega]) + 
     11088*a*(-4 + k)*m*M*rh^7*(2*\[Kappa] + \[Omega]) - 
     144*a^6*(-4 + k)*M*rh^3*\[Omega]*(4*\[Kappa] + \[Omega]) + 
     1008*a^4*(-4 + k)*M^2*rh^4*\[Omega]*(4*\[Kappa] + \[Omega]) - 
     2688*a^4*(-4 + k)*M*rh^5*\[Omega]*(4*\[Kappa] + \[Omega]) + 
     6048*a^2*(-4 + k)*M^2*rh^6*\[Omega]*(4*\[Kappa] + \[Omega]) - 
     6048*a^2*(-4 + k)*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
     a^8*(5 - k)*(-4 + k)*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     28*a^6*(5 - k)*(-4 + k)*M*rh*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 112*a^6*(5 - k)*(-4 + k)*rh^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     112*a^4*(5 - k)*(-4 + k)*M^2*rh^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 1008*a^4*(5 - k)*(-4 + k)*M*rh^3*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     1260*a^4*(5 - k)*(-4 + k)*rh^4*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1680*a^2*(5 - k)*(-4 + k)*M^2*rh^4*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     5544*a^2*(5 - k)*(-4 + k)*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 3696*a^2*(5 - k)*(-4 + k)*rh^6*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3696*(5 - k)*(-4 + k)*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 6864*(5 - k)*(-4 + k)*M*rh^7*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3003*(5 - k)*(-4 + k)*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 70*a^6*rh^4*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     630*a^4*rh^6*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1485*a^2*rh^8*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     1001*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     (112*I)*a^4*(-4 + k)*M*rh^3*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (504*I)*a^2*(-4 + k)*M^2*rh^4*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (1008*I)*a^2*(-4 + k)*M*rh^5*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (1848*I)*(-4 + k)*M^2*rh^6*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - (1584*I)*(-4 + k)*M*rh^7*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     504*a^4*(-4 + k)*rh^5*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 1848*a^2*(-4 + k)*M*rh^6*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     3168*a^2*(-4 + k)*rh^7*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 5148*(-4 + k)*M*rh^8*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     4004*(-4 + k)*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-4 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  (((-24*I)*a^9*m - (288*I)*a^9*(-3 + k)*m + (96*I)*a^9*(4 - k)*(-3 + k)*m + 
     (24*I)*a^9*m^3 - 216*a^8*M - 1008*a^8*(-3 + k)*M + 
     288*a^8*(4 - k)*(-3 + k)*M - 24*a^8*m^2*M + 240*a^8*(-3 + k)*m^2*M - 
     96*a^8*(4 - k)*(-3 + k)*m^2*M - (144*I)*a^7*m*M^2 - 
     (624*I)*a^7*(-3 + k)*m*M^2 + (192*I)*a^7*(4 - k)*(-3 + k)*m*M^2 - 
     192*a^6*M^3 - 384*a^6*(-3 + k)*M^3 + 96*a^6*(4 - k)*(-3 + k)*M^3 + 
     144*a^8*rh + 1920*a^8*(-3 + k)*rh - 720*a^8*(4 - k)*(-3 + k)*rh + 
     528*a^8*m^2*rh - 960*a^8*(-3 + k)*m^2*rh + 576*a^8*(4 - k)*(-3 + k)*m^2*
      rh - 96*a^8*m^4*rh + (5520*I)*a^7*(-3 + k)*m*M*rh - 
     (2304*I)*a^7*(4 - k)*(-3 + k)*m*M*rh - (288*I)*a^7*m^3*M*rh + 
     576*a^6*M^2*rh + 7200*a^6*(-3 + k)*M^2*rh - 2592*a^6*(4 - k)*(-3 + k)*
      M^2*rh - 384*a^6*m^2*M^2*rh - 1440*a^6*(-3 + k)*m^2*M^2*rh + 
     576*a^6*(4 - k)*(-3 + k)*m^2*M^2*rh + (1440*I)*a^5*(-3 + k)*m*M^3*rh - 
     (576*I)*a^5*(4 - k)*(-3 + k)*m*M^3*rh - (960*I)*a^7*m*rh^2 - 
     (6480*I)*a^7*(-3 + k)*m*rh^2 + (3024*I)*a^7*(4 - k)*(-3 + k)*m*rh^2 + 
     (960*I)*a^7*m^3*rh^2 - 19440*a^6*(-3 + k)*M*rh^2 + 
     9072*a^6*(4 - k)*(-3 + k)*M*rh^2 - 2400*a^6*m^2*M*rh^2 + 
     9360*a^6*(-3 + k)*m^2*M*rh^2 - 6048*a^6*(4 - k)*(-3 + k)*m^2*M*rh^2 + 
     (4320*I)*a^5*m*M^2*rh^2 - (18000*I)*a^5*(-3 + k)*m*M^2*rh^2 + 
     (10080*I)*a^5*(4 - k)*(-3 + k)*m*M^2*rh^2 + 960*a^4*M^3*rh^2 - 
     8640*a^4*(-3 + k)*M^3*rh^2 + 4032*a^4*(4 - k)*(-3 + k)*M^3*rh^2 - 
     240*a^6*rh^3 + 11760*a^6*(-3 + k)*rh^3 - 6720*a^6*(4 - k)*(-3 + k)*
      rh^3 + 3120*a^6*m^2*rh^3 - 10080*a^6*(-3 + k)*m^2*rh^3 + 
     8064*a^6*(4 - k)*(-3 + k)*m^2*rh^3 - 960*a^6*m^4*rh^3 - 
     (480*I)*a^5*m*M*rh^3 + (38640*I)*a^5*(-3 + k)*m*M*rh^3 - 
     (24192*I)*a^5*(4 - k)*(-3 + k)*m*M*rh^3 - (2880*I)*a^5*m^3*M*rh^3 - 
     2880*a^4*M^2*rh^3 + 40320*a^4*(-3 + k)*M^2*rh^3 - 
     24192*a^4*(4 - k)*(-3 + k)*M^2*rh^3 + 3840*a^4*m^2*M^2*rh^3 - 
     13440*a^4*(-3 + k)*m^2*M^2*rh^3 + 10752*a^4*(4 - k)*(-3 + k)*m^2*M^2*
      rh^3 - (7680*I)*a^3*m*M^3*rh^3 + (13440*I)*a^3*(-3 + k)*m*M^3*rh^3 - 
     (10752*I)*a^3*(4 - k)*(-3 + k)*m*M^3*rh^3 - (4200*I)*a^5*m*rh^4 - 
     (20160*I)*a^5*(-3 + k)*m*rh^4 + (12096*I)*a^5*(4 - k)*(-3 + k)*m*rh^4 + 
     (4200*I)*a^5*m^3*rh^4 + 2520*a^4*M*rh^4 - 50400*a^4*(-3 + k)*M*rh^4 + 
     36288*a^4*(4 - k)*(-3 + k)*M*rh^4 - 7560*a^4*m^2*M*rh^4 + 
     36960*a^4*(-3 + k)*m^2*M*rh^4 - 36288*a^4*(4 - k)*(-3 + k)*m^2*M*rh^4 + 
     (11760*I)*a^3*m*M^2*rh^4 - (50400*I)*a^3*(-3 + k)*m*M^2*rh^4 + 
     (48384*I)*a^3*(4 - k)*(-3 + k)*m*M^2*rh^4 - 13440*a^2*(-3 + k)*M^3*
      rh^4 + 12096*a^2*(4 - k)*(-3 + k)*M^3*rh^4 - 672*a^4*rh^5 + 
     18144*a^4*(-3 + k)*rh^5 - 15120*a^4*(4 - k)*(-3 + k)*rh^5 + 
     3360*a^4*m^2*rh^5 - 24192*a^4*(-3 + k)*m^2*rh^5 + 
     24192*a^4*(4 - k)*(-3 + k)*m^2*rh^5 - 1344*a^4*m^4*rh^5 + 
     (2688*I)*a^3*m*M*rh^5 + (54432*I)*a^3*(-3 + k)*m*M*rh^5 - 
     (48384*I)*a^3*(4 - k)*(-3 + k)*m*M*rh^5 - (4032*I)*a^3*m^3*M*rh^5 + 
     36288*a^2*(-3 + k)*M^2*rh^5 - 36288*a^2*(4 - k)*(-3 + k)*M^2*rh^5 - 
     12096*a^2*(-3 + k)*m^2*M^2*rh^5 + 24192*a^2*(4 - k)*(-3 + k)*m^2*M^2*
      rh^5 - (5376*I)*a*m*M^3*rh^5 + (12096*I)*a*(-3 + k)*m*M^3*rh^5 - 
     (24192*I)*a*(4 - k)*(-3 + k)*m*M^3*rh^5 - (4032*I)*a^3*m*rh^6 - 
     (15120*I)*a^3*(-3 + k)*m*rh^6 + (11088*I)*a^3*(4 - k)*(-3 + k)*m*rh^6 + 
     (4032*I)*a^3*m^3*rh^6 - 30240*a^2*(-3 + k)*M*rh^6 + 
     33264*a^2*(4 - k)*(-3 + k)*M*rh^6 + 30240*a^2*(-3 + k)*m^2*M*rh^6 - 
     44352*a^2*(4 - k)*(-3 + k)*m^2*M*rh^6 + (4032*I)*a*m*M^2*rh^6 - 
     (30240*I)*a*(-3 + k)*m*M^2*rh^6 + (44352*I)*a*(4 - k)*(-3 + k)*m*M^2*
      rh^6 + 7920*a^2*(-3 + k)*rh^7 - 9504*a^2*(4 - k)*(-3 + k)*rh^7 - 
     15840*a^2*(-3 + k)*m^2*rh^7 + 19008*a^2*(4 - k)*(-3 + k)*m^2*rh^7 + 
     (15840*I)*a*(-3 + k)*m*M*rh^7 - (19008*I)*a*(4 - k)*(-3 + k)*m*M*rh^7 - 
     (216*I)*a^10*\[Kappa] - (120*I)*a^10*(-3 + k)*\[Kappa] + 
     (48*I)*a^10*m^2*\[Kappa] + (48*I)*a^10*(-3 + k)*m^2*\[Kappa] + 
     192*a^9*m*M*\[Kappa] + 144*a^9*(-3 + k)*m*M*\[Kappa] - 
     (144*I)*a^8*M^2*\[Kappa] - (96*I)*a^8*(-3 + k)*M^2*\[Kappa] - 
     1152*a^9*m*rh*\[Kappa] - 960*a^9*(-3 + k)*m*rh*\[Kappa] + 
     (2496*I)*a^8*M*rh*\[Kappa] + (1920*I)*a^8*(-3 + k)*M*rh*\[Kappa] - 
     (384*I)*a^8*m^2*M*rh*\[Kappa] - (480*I)*a^8*(-3 + k)*m^2*M*rh*\[Kappa] - 
     384*a^7*m*M^2*rh*\[Kappa] - 384*a^7*(-3 + k)*m*M^2*rh*\[Kappa] - 
     (3840*I)*a^8*rh^2*\[Kappa] - (3600*I)*a^8*(-3 + k)*rh^2*\[Kappa] + 
     (1920*I)*a^8*m^2*rh^2*\[Kappa] + (2880*I)*a^8*(-3 + k)*m^2*rh^2*
      \[Kappa] + 6240*a^7*m*M*rh^2*\[Kappa] + 6240*a^7*(-3 + k)*m*M*rh^2*
      \[Kappa] - (4320*I)*a^6*M^2*rh^2*\[Kappa] - (4320*I)*a^6*(-3 + k)*M^2*
      rh^2*\[Kappa] - 8640*a^7*m*rh^3*\[Kappa] - 10080*a^7*(-3 + k)*m*rh^3*
      \[Kappa] + (17280*I)*a^6*M*rh^3*\[Kappa] + (20160*I)*a^6*(-3 + k)*M*
      rh^3*\[Kappa] - (5760*I)*a^6*m^2*M*rh^3*\[Kappa] - 
     (10080*I)*a^6*(-3 + k)*m^2*M*rh^3*\[Kappa] - 5760*a^5*m*M^2*rh^3*
      \[Kappa] - 5760*a^5*(-3 + k)*m*M^2*rh^3*\[Kappa] - 
     (11760*I)*a^6*rh^4*\[Kappa] - (16800*I)*a^6*(-3 + k)*rh^4*\[Kappa] + 
     (10080*I)*a^6*m^2*rh^4*\[Kappa] + (20160*I)*a^6*(-3 + k)*m^2*rh^4*
      \[Kappa] + 25200*a^5*m*M*rh^4*\[Kappa] + 30240*a^5*(-3 + k)*m*M*rh^4*
      \[Kappa] - (15120*I)*a^4*M^2*rh^4*\[Kappa] - (20160*I)*a^4*(-3 + k)*M^2*
      rh^4*\[Kappa] - 16128*a^5*m*rh^5*\[Kappa] - 24192*a^5*(-3 + k)*m*rh^5*
      \[Kappa] + (29568*I)*a^4*M*rh^5*\[Kappa] + (48384*I)*a^4*(-3 + k)*M*
      rh^5*\[Kappa] - (16128*I)*a^4*m^2*M*rh^5*\[Kappa] - 
     (36288*I)*a^4*(-3 + k)*m^2*M*rh^5*\[Kappa] - 16128*a^3*m*M^2*rh^5*
      \[Kappa] - 16128*a^3*(-3 + k)*m*M^2*rh^5*\[Kappa] - 
     (12096*I)*a^4*rh^6*\[Kappa] - (25200*I)*a^4*(-3 + k)*rh^6*\[Kappa] + 
     (16128*I)*a^4*m^2*rh^6*\[Kappa] + (40320*I)*a^4*(-3 + k)*m^2*rh^6*
      \[Kappa] + 28224*a^3*m*M*rh^6*\[Kappa] + 36288*a^3*(-3 + k)*m*M*rh^6*
      \[Kappa] - (12096*I)*a^2*M^2*rh^6*\[Kappa] - (20160*I)*a^2*(-3 + k)*M^2*
      rh^6*\[Kappa] - 8640*a^3*m*rh^7*\[Kappa] - 15840*a^3*(-3 + k)*m*rh^7*
      \[Kappa] + (14400*I)*a^2*M*rh^7*\[Kappa] + (31680*I)*a^2*(-3 + k)*M*
      rh^7*\[Kappa] - (11520*I)*a^2*m^2*M*rh^7*\[Kappa] - 
     (31680*I)*a^2*(-3 + k)*m^2*M*rh^7*\[Kappa] - 11520*a*m*M^2*rh^7*
      \[Kappa] - 11520*a*(-3 + k)*m*M^2*rh^7*\[Kappa] - 
     (3960*I)*a^2*rh^8*\[Kappa] - (11880*I)*a^2*(-3 + k)*rh^8*\[Kappa] + 
     (7920*I)*a^2*m^2*rh^8*\[Kappa] + (23760*I)*a^2*(-3 + k)*m^2*rh^8*
      \[Kappa] + 7920*a*m*M*rh^8*\[Kappa] + 7920*a*(-3 + k)*m*M*rh^8*
      \[Kappa] + (24*I)*a^11*m*\[Kappa]^2 + 24*a^10*M*\[Kappa]^2 - 
     240*a^10*rh*\[Kappa]^2 + 96*a^10*m^2*rh*\[Kappa]^2 - 
     (72*I)*a^9*m*M*rh*\[Kappa]^2 + (960*I)*a^9*m*rh^2*\[Kappa]^2 + 
     960*a^8*M*rh^2*\[Kappa]^2 - 2400*a^8*rh^3*\[Kappa]^2 + 
     1920*a^8*m^2*rh^3*\[Kappa]^2 - (1200*I)*a^7*m*M*rh^3*\[Kappa]^2 + 
     (5040*I)*a^7*m*rh^4*\[Kappa]^2 + 5040*a^6*M*rh^4*\[Kappa]^2 - 
     6720*a^6*rh^5*\[Kappa]^2 + 8064*a^6*m^2*rh^5*\[Kappa]^2 - 
     (5544*I)*a^5*m*M*rh^5*\[Kappa]^2 + (8064*I)*a^5*m*rh^6*\[Kappa]^2 + 
     8064*a^4*M*rh^6*\[Kappa]^2 - 7200*a^4*rh^7*\[Kappa]^2 + 
     11520*a^4*m^2*rh^7*\[Kappa]^2 - (9504*I)*a^3*m*M*rh^7*\[Kappa]^2 + 
     (3960*I)*a^3*m*rh^8*\[Kappa]^2 + 3960*a^2*M*rh^8*\[Kappa]^2 - 
     2640*a^2*rh^9*\[Kappa]^2 + 5280*a^2*m^2*rh^9*\[Kappa]^2 - 
     (5280*I)*a*m*M*rh^9*\[Kappa]^2 - (32*I)*a^9*m*\[Lambda] + 
     (8*I)*a^9*(-3 + k)*m*\[Lambda] - (8*I)*a^9*(4 - k)*(-3 + k)*m*
      \[Lambda] - 64*a^8*rh*\[Lambda] + 128*a^8*m^2*rh*\[Lambda] + 
     (352*I)*a^7*m*M*rh*\[Lambda] - (240*I)*a^7*(-3 + k)*m*M*rh*\[Lambda] + 
     (192*I)*a^7*(4 - k)*(-3 + k)*m*M*rh*\[Lambda] - 
     (1200*I)*a^7*m*rh^2*\[Lambda] + (480*I)*a^7*(-3 + k)*m*rh^2*\[Lambda] - 
     (672*I)*a^7*(4 - k)*(-3 + k)*m*rh^2*\[Lambda] + 
     (80*I)*a^7*m^3*rh^2*\[Lambda] + 480*a^6*M*rh^2*\[Lambda] - 
     60*a^6*(-3 + k)*M*rh^2*\[Lambda] - 640*a^6*m^2*M*rh^2*\[Lambda] - 
     (480*I)*a^5*m*M^2*rh^2*\[Lambda] + (960*I)*a^5*(-3 + k)*m*M^2*rh^2*
      \[Lambda] - (672*I)*a^5*(4 - k)*(-3 + k)*m*M^2*rh^2*\[Lambda] - 
     1080*a^6*rh^3*\[Lambda] + 1960*a^6*m^2*rh^3*\[Lambda] + 
     (5920*I)*a^5*m*M*rh^3*\[Lambda] - (3920*I)*a^5*(-3 + k)*m*M*rh^3*
      \[Lambda] + (5376*I)*a^5*(4 - k)*(-3 + k)*m*M*rh^3*\[Lambda] - 
     640*a^4*M^2*rh^3*\[Lambda] + 280*a^4*(-3 + k)*M^2*rh^3*\[Lambda] - 
     (5600*I)*a^5*m*rh^4*\[Lambda] + (3360*I)*a^5*(-3 + k)*m*rh^4*\[Lambda] - 
     (6048*I)*a^5*(4 - k)*(-3 + k)*m*rh^4*\[Lambda] + 
     (560*I)*a^5*m^3*rh^4*\[Lambda] + 4620*a^4*M*rh^4*\[Lambda] - 
     560*a^4*(-3 + k)*M*rh^4*\[Lambda] - 4480*a^4*m^2*M*rh^4*\[Lambda] - 
     (6720*I)*a^3*m*M^2*rh^4*\[Lambda] + (4480*I)*a^3*(-3 + k)*m*M^2*rh^4*
      \[Lambda] - (8064*I)*a^3*(4 - k)*(-3 + k)*m*M^2*rh^4*\[Lambda] - 
     3584*a^4*rh^5*\[Lambda] + 5600*a^4*m^2*rh^5*\[Lambda] + 
     (14784*I)*a^3*m*M*rh^5*\[Lambda] - (10080*I)*a^3*(-3 + k)*m*M*rh^5*
      \[Lambda] + (24192*I)*a^3*(4 - k)*(-3 + k)*m*M*rh^5*\[Lambda] - 
     4480*a^2*M^2*rh^5*\[Lambda] + 1008*a^2*(-3 + k)*M^2*rh^5*\[Lambda] - 
     (7392*I)*a^3*m*rh^6*\[Lambda] + (6720*I)*a^3*(-3 + k)*m*rh^6*\[Lambda] - 
     (14784*I)*a^3*(4 - k)*(-3 + k)*m*rh^6*\[Lambda] + 
     (672*I)*a^3*m^3*rh^6*\[Lambda] + 8736*a^2*M*rh^6*\[Lambda] - 
     840*a^2*(-3 + k)*M*rh^6*\[Lambda] - 5376*a^2*m^2*M*rh^6*\[Lambda] - 
     (4032*I)*a*m*M^2*rh^6*\[Lambda] - (14784*I)*a*(4 - k)*(-3 + k)*m*M^2*
      rh^6*\[Lambda] - 3600*a^2*rh^7*\[Lambda] + 4080*a^2*m^2*rh^7*
      \[Lambda] + (6720*I)*a*m*M*rh^7*\[Lambda] - (5280*I)*a*(-3 + k)*m*M*
      rh^7*\[Lambda] + (25344*I)*a*(4 - k)*(-3 + k)*m*M*rh^7*\[Lambda] - 
     2880*M^2*rh^7*\[Lambda] - (2640*I)*a*m*rh^8*\[Lambda] + 
     (3960*I)*a*(-3 + k)*m*rh^8*\[Lambda] - (10296*I)*a*(4 - k)*(-3 + k)*m*
      rh^8*\[Lambda] + 3300*M*rh^8*\[Lambda] - 880*rh^9*\[Lambda] + 
     32*a^9*m*rh*\[Kappa]*\[Lambda] + 80*a^9*(-3 + k)*m*rh*\[Kappa]*
      \[Lambda] - (60*I)*a^8*(-3 + k)*rh^2*\[Kappa]*\[Lambda] - 
     160*a^7*m*M*rh^2*\[Kappa]*\[Lambda] - 480*a^7*(-3 + k)*m*M*rh^2*\[Kappa]*
      \[Lambda] + 640*a^7*m*rh^3*\[Kappa]*\[Lambda] + 
     2240*a^7*(-3 + k)*m*rh^3*\[Kappa]*\[Lambda] + (280*I)*a^6*(-3 + k)*M*
      rh^3*\[Kappa]*\[Lambda] - (1120*I)*a^6*(-3 + k)*rh^4*\[Kappa]*
      \[Lambda] - 1680*a^5*m*M*rh^4*\[Kappa]*\[Lambda] - 
     6720*a^5*(-3 + k)*m*M*rh^4*\[Kappa]*\[Lambda] + 
     2688*a^5*m*rh^5*\[Kappa]*\[Lambda] + 12096*a^5*(-3 + k)*m*rh^5*\[Kappa]*
      \[Lambda] + (3024*I)*a^4*(-3 + k)*M*rh^5*\[Kappa]*\[Lambda] - 
     (4200*I)*a^4*(-3 + k)*rh^6*\[Kappa]*\[Lambda] - 
     4032*a^3*m*M*rh^6*\[Kappa]*\[Lambda] - 20160*a^3*(-3 + k)*m*M*rh^6*
      \[Kappa]*\[Lambda] + 3840*a^3*m*rh^7*\[Kappa]*\[Lambda] + 
     21120*a^3*(-3 + k)*m*rh^7*\[Kappa]*\[Lambda] + 
     (5280*I)*a^2*(-3 + k)*M*rh^7*\[Kappa]*\[Lambda] - 
     (3960*I)*a^2*(-3 + k)*rh^8*\[Kappa]*\[Lambda] - 
     2640*a*m*M*rh^8*\[Kappa]*\[Lambda] - 15840*a*(-3 + k)*m*M*rh^8*\[Kappa]*
      \[Lambda] + 1760*a*m*rh^9*\[Kappa]*\[Lambda] + 
     11440*a*(-3 + k)*m*rh^9*\[Kappa]*\[Lambda] - 
     (80*I)*a^9*m*rh^2*\[Kappa]^2*\[Lambda] - 40*a^8*rh^3*\[Kappa]^2*
      \[Lambda] - (1120*I)*a^7*m*rh^4*\[Kappa]^2*\[Lambda] - 
     336*a^6*rh^5*\[Kappa]^2*\[Lambda] - (4032*I)*a^5*m*rh^6*\[Kappa]^2*
      \[Lambda] - 720*a^4*rh^7*\[Kappa]^2*\[Lambda] - 
     (5280*I)*a^3*m*rh^8*\[Kappa]^2*\[Lambda] - 440*a^2*rh^9*\[Kappa]^2*
      \[Lambda] - (2288*I)*a*m*rh^10*\[Kappa]^2*\[Lambda] - 
     32*a^8*rh*\[Lambda]^2 - (80*I)*a^7*m*rh^2*\[Lambda]^2 + 
     240*a^6*M*rh^2*\[Lambda]^2 - 30*a^6*(-3 + k)*M*rh^2*\[Lambda]^2 - 
     580*a^6*rh^3*\[Lambda]^2 + 20*a^6*m^2*rh^3*\[Lambda]^2 + 
     (320*I)*a^5*m*M*rh^3*\[Lambda]^2 - 320*a^4*M^2*rh^3*\[Lambda]^2 + 
     140*a^4*(-3 + k)*M^2*rh^3*\[Lambda]^2 - (840*I)*a^5*m*rh^4*\[Lambda]^2 + 
     2450*a^4*M*rh^4*\[Lambda]^2 - 280*a^4*(-3 + k)*M*rh^4*\[Lambda]^2 - 
     2128*a^4*rh^5*\[Lambda]^2 + 112*a^4*m^2*rh^5*\[Lambda]^2 + 
     (1792*I)*a^3*m*M*rh^5*\[Lambda]^2 - 2240*a^2*M^2*rh^5*\[Lambda]^2 + 
     504*a^2*(-3 + k)*M^2*rh^5*\[Lambda]^2 - (2016*I)*a^3*m*rh^6*
      \[Lambda]^2 + 5040*a^2*M*rh^6*\[Lambda]^2 - 420*a^2*(-3 + k)*M*rh^6*
      \[Lambda]^2 - 2520*a^2*rh^7*\[Lambda]^2 + 120*a^2*m^2*rh^7*
      \[Lambda]^2 + (1920*I)*a*m*M*rh^7*\[Lambda]^2 - 
     1440*M^2*rh^7*\[Lambda]^2 - (1320*I)*a*m*rh^8*\[Lambda]^2 + 
     2310*M*rh^8*\[Lambda]^2 - 880*rh^9*\[Lambda]^2 - 
     (30*I)*a^8*(-3 + k)*rh^2*\[Kappa]*\[Lambda]^2 + 
     (140*I)*a^6*(-3 + k)*M*rh^3*\[Kappa]*\[Lambda]^2 - 
     (560*I)*a^6*(-3 + k)*rh^4*\[Kappa]*\[Lambda]^2 + 
     (1512*I)*a^4*(-3 + k)*M*rh^5*\[Kappa]*\[Lambda]^2 - 
     (2100*I)*a^4*(-3 + k)*rh^6*\[Kappa]*\[Lambda]^2 + 
     (2640*I)*a^2*(-3 + k)*M*rh^7*\[Kappa]*\[Lambda]^2 - 
     (1980*I)*a^2*(-3 + k)*rh^8*\[Kappa]*\[Lambda]^2 - 
     20*a^8*rh^3*\[Kappa]^2*\[Lambda]^2 - 168*a^6*rh^5*\[Kappa]^2*
      \[Lambda]^2 - 360*a^4*rh^7*\[Kappa]^2*\[Lambda]^2 - 
     220*a^2*rh^9*\[Kappa]^2*\[Lambda]^2 - 20*a^6*rh^3*\[Lambda]^3 + 
     70*a^4*M*rh^4*\[Lambda]^3 - 168*a^4*rh^5*\[Lambda]^3 + 
     336*a^2*M*rh^6*\[Lambda]^3 - 360*a^2*rh^7*\[Lambda]^3 + 
     330*M*rh^8*\[Lambda]^3 - 220*rh^9*\[Lambda]^3 - 
     (24*I)*a^9*m*M*rh*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (720*I)*a^7*m*M*rh^3*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (2520*I)*a^5*m*M*rh^5*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (2016*I)*a^3*m*M*rh^7*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     96*a^7*(-3 + k)*m*M^2*rh*(\[Kappa] - 6*\[Omega]) + 
     480*a^7*(-3 + k)*m*M*rh^2*(\[Kappa] - 6*\[Omega]) - 
     1440*a^5*(-3 + k)*m*M^2*rh^3*(\[Kappa] - 6*\[Omega]) + 
     1680*a^5*(-3 + k)*m*M*rh^4*(\[Kappa] - 6*\[Omega]) + 
     120*a^7*(-3 + k)*m*M*rh^2*(4*\[Kappa] - 5*\[Omega]) - 
     720*a^5*(-3 + k)*m*M^2*rh^3*(4*\[Kappa] - 5*\[Omega]) + 
     1680*a^5*(-3 + k)*m*M*rh^4*(4*\[Kappa] - 5*\[Omega]) - 
     3360*a^3*(-3 + k)*m*M^2*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
     3024*a^3*(-3 + k)*m*M*rh^6*(4*\[Kappa] - 5*\[Omega]) + 
     (312*I)*a^10*(-3 + k)*\[Omega] - (120*I)*a^10*(4 - k)*(-3 + k)*
      \[Omega] - (72*I)*a^10*m^2*\[Omega] + 48*a^9*m*M*\[Omega] - 
     480*a^9*(-3 + k)*m*M*\[Omega] + 192*a^9*(4 - k)*(-3 + k)*m*M*\[Omega] + 
     (144*I)*a^8*M^2*\[Omega] + (624*I)*a^8*(-3 + k)*M^2*\[Omega] - 
     (192*I)*a^8*(4 - k)*(-3 + k)*M^2*\[Omega] - 1152*a^9*m*rh*\[Omega] + 
     1920*a^9*(-3 + k)*m*rh*\[Omega] - 1224*a^9*(4 - k)*(-3 + k)*m*rh*
      \[Omega] + 384*a^9*m^3*rh*\[Omega] - (6240*I)*a^8*(-3 + k)*M*rh*
      \[Omega] + (2880*I)*a^8*(4 - k)*(-3 + k)*M*rh*\[Omega] + 
     (864*I)*a^8*m^2*M*rh*\[Omega] + 768*a^7*m*M^2*rh*\[Omega] + 
     2304*a^7*(-3 + k)*m*M^2*rh*\[Omega] - 1152*a^7*(4 - k)*(-3 + k)*m*M^2*rh*
      \[Omega] - (1440*I)*a^6*(-3 + k)*M^3*rh*\[Omega] + 
     (576*I)*a^6*(4 - k)*(-3 + k)*M^3*rh*\[Omega] - 
     (3120*I)*a^8*rh^2*\[Omega] + (7920*I)*a^8*(-3 + k)*rh^2*\[Omega] - 
     (5040*I)*a^8*(4 - k)*(-3 + k)*rh^2*\[Omega] - (2640*I)*a^8*m^2*rh^2*
      \[Omega] + 5760*a^7*m*M*rh^2*\[Omega] - 15600*a^7*(-3 + k)*m*M*rh^2*
      \[Omega] + 13104*a^7*(4 - k)*(-3 + k)*m*M*rh^2*\[Omega] + 
     (21240*I)*a^6*(-3 + k)*M^2*rh^2*\[Omega] - (12096*I)*a^6*(4 - k)*
      (-3 + k)*M^2*rh^2*\[Omega] - 8400*a^7*m*rh^3*\[Omega] + 
     20160*a^7*(-3 + k)*m*rh^3*\[Omega] - 18816*a^7*(4 - k)*(-3 + k)*m*rh^3*
      \[Omega] + 5040*a^7*m^3*rh^3*\[Omega] + (25680*I)*a^6*M*rh^3*\[Omega] - 
     (50400*I)*a^6*(-3 + k)*M*rh^3*\[Omega] + (40320*I)*a^6*(4 - k)*(-3 + k)*
      M*rh^3*\[Omega] + (9360*I)*a^6*m^2*M*rh^3*\[Omega] - 
     9600*a^5*m*M^2*rh^3*\[Omega] + 16320*a^5*(-3 + k)*m*M^2*rh^3*\[Omega] - 
     24192*a^5*(4 - k)*(-3 + k)*m*M^2*rh^3*\[Omega] + 
     (11520*I)*a^4*M^3*rh^3*\[Omega] - (15120*I)*a^4*(-3 + k)*M^3*rh^3*
      \[Omega] + (10752*I)*a^4*(4 - k)*(-3 + k)*M^3*rh^3*\[Omega] - 
     (12600*I)*a^6*rh^4*\[Omega] + (29400*I)*a^6*(-3 + k)*rh^4*\[Omega] - 
     (30240*I)*a^6*(4 - k)*(-3 + k)*rh^4*\[Omega] - 
     (12600*I)*a^6*m^2*rh^4*\[Omega] + 27720*a^5*m*M*rh^4*\[Omega] - 
     57960*a^5*(-3 + k)*m*M*rh^4*\[Omega] + 90720*a^5*(4 - k)*(-3 + k)*m*M*
      rh^4*\[Omega] - (59640*I)*a^4*M^2*rh^4*\[Omega] + 
     (67200*I)*a^4*(-3 + k)*M^2*rh^4*\[Omega] - (72576*I)*a^4*(4 - k)*
      (-3 + k)*M^2*rh^4*\[Omega] - 16128*a^5*m*rh^5*\[Omega] + 
     48384*a^5*(-3 + k)*m*rh^5*\[Omega] - 66528*a^5*(4 - k)*(-3 + k)*m*rh^5*
      \[Omega] + 12096*a^5*m^3*rh^5*\[Omega] + (60480*I)*a^4*M*rh^5*
      \[Omega] - (81312*I)*a^4*(-3 + k)*M*rh^5*\[Omega] + 
     (120960*I)*a^4*(4 - k)*(-3 + k)*M*rh^5*\[Omega] + 
     (16128*I)*a^4*m^2*M*rh^5*\[Omega] - 16128*a^3*m*M^2*rh^5*\[Omega] + 
     10752*a^3*(-3 + k)*m*M^2*rh^5*\[Omega] - 72576*a^3*(4 - k)*(-3 + k)*m*
      M^2*rh^5*\[Omega] + (32256*I)*a^2*M^3*rh^5*\[Omega] - 
     (18144*I)*a^2*(-3 + k)*M^3*rh^5*\[Omega] + (24192*I)*a^2*(4 - k)*
      (-3 + k)*M^3*rh^5*\[Omega] - (14112*I)*a^4*rh^6*\[Omega] + 
     (29232*I)*a^4*(-3 + k)*rh^6*\[Omega] - (55440*I)*a^4*(4 - k)*(-3 + k)*
      rh^6*\[Omega] - (18144*I)*a^4*m^2*rh^6*\[Omega] + 
     28224*a^3*m*M*rh^6*\[Omega] - 46368*a^3*(-3 + k)*m*M*rh^6*\[Omega] + 
     155232*a^3*(4 - k)*(-3 + k)*m*M*rh^6*\[Omega] - 
     (68544*I)*a^2*M^2*rh^6*\[Omega] + (35280*I)*a^2*(-3 + k)*M^2*rh^6*
      \[Omega] - (88704*I)*a^2*(4 - k)*(-3 + k)*M^2*rh^6*\[Omega] - 
     12960*a^3*m*rh^7*\[Omega] + 31680*a^3*(-3 + k)*m*rh^7*\[Omega] - 
     76032*a^3*(4 - k)*(-3 + k)*m*rh^7*\[Omega] + 
     7200*a^3*m^3*rh^7*\[Omega] + (36000*I)*a^2*M*rh^7*\[Omega] - 
     (21600*I)*a^2*(-3 + k)*M*rh^7*\[Omega] + (95040*I)*a^2*(4 - k)*(-3 + k)*
      M*rh^7*\[Omega] + (4320*I)*a^2*m^2*M*rh^7*\[Omega] - 
     5760*a*m*M^2*rh^7*\[Omega] - 5760*a*(-3 + k)*m*M^2*rh^7*\[Omega] - 
     38016*a*(4 - k)*(-3 + k)*m*M^2*rh^7*\[Omega] + 
     (17280*I)*M^3*rh^7*\[Omega] - (3960*I)*a^2*rh^8*\[Omega] + 
     (3960*I)*a^2*(-3 + k)*rh^8*\[Omega] - (30888*I)*a^2*(4 - k)*(-3 + k)*
      rh^8*\[Omega] - (7920*I)*a^2*m^2*rh^8*\[Omega] + 
     11880*a*m*M*rh^8*\[Omega] + 3960*a*(-3 + k)*m*M*rh^8*\[Omega] + 
     61776*a*(4 - k)*(-3 + k)*m*M*rh^8*\[Omega] - (19800*I)*M^2*rh^8*
      \[Omega] - 5280*a*m*rh^9*\[Omega] - 24024*a*(4 - k)*(-3 + k)*m*rh^9*
      \[Omega] + (5280*I)*M*rh^9*\[Omega] - (96*I)*a^11*m*\[Kappa]*\[Omega] - 
     (96*I)*a^11*(-3 + k)*m*\[Kappa]*\[Omega] - 192*a^10*M*\[Kappa]*
      \[Omega] - 144*a^10*(-3 + k)*M*\[Kappa]*\[Omega] + 
     1248*a^10*rh*\[Kappa]*\[Omega] + 912*a^10*(-3 + k)*rh*\[Kappa]*
      \[Omega] + (576*I)*a^9*m*M*rh*\[Kappa]*\[Omega] + 
     (960*I)*a^9*(-3 + k)*m*M*rh*\[Kappa]*\[Omega] + 
     384*a^8*M^2*rh*\[Kappa]*\[Omega] + 480*a^8*(-3 + k)*M^2*rh*\[Kappa]*
      \[Omega] - (3840*I)*a^9*m*rh^2*\[Kappa]*\[Omega] - 
     (6120*I)*a^9*(-3 + k)*m*rh^2*\[Kappa]*\[Omega] - 
     6336*a^8*M*rh^2*\[Kappa]*\[Omega] - 6480*a^8*(-3 + k)*M*rh^2*\[Kappa]*
      \[Omega] + 10320*a^8*rh^3*\[Kappa]*\[Omega] + 
     11040*a^8*(-3 + k)*rh^3*\[Kappa]*\[Omega] + (5760*I)*a^7*m*M*rh^3*
      \[Kappa]*\[Omega] + (21840*I)*a^7*(-3 + k)*m*M*rh^3*\[Kappa]*\[Omega] + 
     5760*a^6*M^2*rh^3*\[Kappa]*\[Omega] + 10080*a^6*(-3 + k)*M^2*rh^3*
      \[Kappa]*\[Omega] - (20160*I)*a^7*m*rh^4*\[Kappa]*\[Omega] - 
     (47040*I)*a^7*(-3 + k)*m*rh^4*\[Kappa]*\[Omega] - 
     24480*a^6*M*rh^4*\[Kappa]*\[Omega] - 49920*a^6*(-3 + k)*M*rh^4*\[Kappa]*
      \[Omega] + 21672*a^6*rh^5*\[Kappa]*\[Omega] + 
     50400*a^6*(-3 + k)*rh^5*\[Kappa]*\[Omega] + (12096*I)*a^5*m*M*rh^5*
      \[Kappa]*\[Omega] + (90720*I)*a^5*(-3 + k)*m*M*rh^5*\[Kappa]*\[Omega] + 
     16128*a^4*M^2*rh^5*\[Kappa]*\[Omega] + 39312*a^4*(-3 + k)*M^2*rh^5*
      \[Kappa]*\[Omega] - (32256*I)*a^5*m*rh^6*\[Kappa]*\[Omega] - 
     (110880*I)*a^5*(-3 + k)*m*rh^6*\[Kappa]*\[Omega] - 
     24192*a^4*M*rh^6*\[Kappa]*\[Omega] - 124320*a^4*(-3 + k)*M*rh^6*\[Kappa]*
      \[Omega] + 14112*a^4*rh^7*\[Kappa]*\[Omega] + 
     79200*a^4*(-3 + k)*rh^7*\[Kappa]*\[Omega] + (6912*I)*a^3*m*M*rh^7*
      \[Kappa]*\[Omega] + (110880*I)*a^3*(-3 + k)*m*M*rh^7*\[Kappa]*
      \[Omega] + 11520*a^2*M^2*rh^7*\[Kappa]*\[Omega] + 
     39168*a^2*(-3 + k)*M^2*rh^7*\[Kappa]*\[Omega] - 
     (15840*I)*a^3*m*rh^8*\[Kappa]*\[Omega] - (95040*I)*a^3*(-3 + k)*m*rh^8*
      \[Kappa]*\[Omega] - 4320*a^2*M*rh^8*\[Kappa]*\[Omega] - 
     74880*a^2*(-3 + k)*M*rh^8*\[Kappa]*\[Omega] + 1320*a^2*rh^9*\[Kappa]*
      \[Omega] + 34320*a^2*(-3 + k)*rh^9*\[Kappa]*\[Omega] + 
     (34320*I)*a*(-3 + k)*m*M*rh^9*\[Kappa]*\[Omega] - 
     (24024*I)*a*(-3 + k)*m*rh^10*\[Kappa]*\[Omega] - 
     (24*I)*a^12*\[Kappa]^2*\[Omega] - 192*a^11*m*rh*\[Kappa]^2*\[Omega] + 
     (96*I)*a^10*M*rh*\[Kappa]^2*\[Omega] - (912*I)*a^10*rh^2*\[Kappa]^2*
      \[Omega] - 4080*a^9*m*rh^3*\[Kappa]^2*\[Omega] + 
     (1872*I)*a^8*M*rh^3*\[Kappa]^2*\[Omega] - (5520*I)*a^8*rh^4*\[Kappa]^2*
      \[Omega] - 18816*a^7*m*rh^5*\[Kappa]^2*\[Omega] + 
     (8640*I)*a^6*M*rh^5*\[Kappa]^2*\[Omega] - (14112*I)*a^6*rh^6*\[Kappa]^2*
      \[Omega] - 31680*a^5*m*rh^7*\[Kappa]^2*\[Omega] + 
     (14496*I)*a^4*M*rh^7*\[Kappa]^2*\[Omega] - (16344*I)*a^4*rh^8*\[Kappa]^2*
      \[Omega] - 21120*a^3*m*rh^9*\[Kappa]^2*\[Omega] + 
     (7920*I)*a^2*M*rh^9*\[Kappa]^2*\[Omega] - (6864*I)*a^2*rh^10*\[Kappa]^2*
      \[Omega] - 4368*a*m*rh^11*\[Kappa]^2*\[Omega] + 
     (480*I)*a^6*M^2*rh^2*(-18 + \[Lambda])*\[Omega] - 
     32*a^10*(-3 + k)*rh*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     256*a^8*(-3 + k)*M*rh^2*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     640*a^8*(-3 + k)*rh^3*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     1280*a^6*(-3 + k)*M*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     1120*a^6*(-3 + k)*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     64*a^8*M*rh^2*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     960*a^6*M*rh^4*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     2688*a^4*M*rh^6*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     1920*a^2*M*rh^8*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (48*I)*a^10*rh^2*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (480*I)*a^8*rh^4*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (1008*I)*a^6*rh^6*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (576*I)*a^4*rh^8*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (32*I)*a^10*(-3 + \[Lambda])*\[Omega] - (280*I)*a^6*(-3 + k)*rh^4*
      (-3 + \[Lambda])*\[Omega] + (1120*I)*a^4*(-3 + k)*M*rh^5*
      (-3 + \[Lambda])*\[Omega] - (2016*I)*a^4*(-3 + k)*rh^6*(-3 + \[Lambda])*
      \[Omega] + (3360*I)*a^2*(-3 + k)*M*rh^7*(-3 + \[Lambda])*\[Omega] - 
     (2640*I)*a^2*(-3 + k)*rh^8*(-3 + \[Lambda])*\[Omega] - 
     80*a^8*rh^3*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     840*a^6*rh^5*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     2016*a^4*rh^7*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     1320*a^2*rh^9*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     (8*I)*a^10*(-3 + k)*\[Lambda]*\[Omega] + (8*I)*a^10*(4 - k)*(-3 + k)*
      \[Lambda]*\[Omega] - 256*a^9*m*rh*\[Lambda]*\[Omega] + 
     (240*I)*a^8*(-3 + k)*M*rh*\[Lambda]*\[Omega] - 
     (192*I)*a^8*(4 - k)*(-3 + k)*M*rh*\[Lambda]*\[Omega] + 
     (960*I)*a^8*rh^2*\[Lambda]*\[Omega] - (480*I)*a^8*(-3 + k)*rh^2*
      \[Lambda]*\[Omega] + (672*I)*a^8*(4 - k)*(-3 + k)*rh^2*\[Lambda]*
      \[Omega] - (240*I)*a^8*m^2*rh^2*\[Lambda]*\[Omega] + 
     1280*a^7*m*M*rh^2*\[Lambda]*\[Omega] - (960*I)*a^6*(-3 + k)*M^2*rh^2*
      \[Lambda]*\[Omega] + (672*I)*a^6*(4 - k)*(-3 + k)*M^2*rh^2*\[Lambda]*
      \[Omega] - 4320*a^7*m*rh^3*\[Lambda]*\[Omega] - 
     (4720*I)*a^6*M*rh^3*\[Lambda]*\[Omega] + (3920*I)*a^6*(-3 + k)*M*rh^3*
      \[Lambda]*\[Omega] - (5376*I)*a^6*(4 - k)*(-3 + k)*M*rh^3*\[Lambda]*
      \[Omega] + (3080*I)*a^6*rh^4*\[Lambda]*\[Omega] - 
     (3080*I)*a^6*(-3 + k)*rh^4*\[Lambda]*\[Omega] + 
     (6048*I)*a^6*(4 - k)*(-3 + k)*rh^4*\[Lambda]*\[Omega] - 
     (2240*I)*a^6*m^2*rh^4*\[Lambda]*\[Omega] + 10360*a^5*m*M*rh^4*\[Lambda]*
      \[Omega] + (5880*I)*a^4*M^2*rh^4*\[Lambda]*\[Omega] - 
     (4480*I)*a^4*(-3 + k)*M^2*rh^4*\[Lambda]*\[Omega] + 
     (8064*I)*a^4*(4 - k)*(-3 + k)*M^2*rh^4*\[Lambda]*\[Omega] - 
     14784*a^5*m*rh^5*\[Lambda]*\[Omega] - (7392*I)*a^4*M*rh^5*\[Lambda]*
      \[Omega] + (8960*I)*a^4*(-3 + k)*M*rh^5*\[Lambda]*\[Omega] - 
     (24192*I)*a^4*(4 - k)*(-3 + k)*M*rh^5*\[Lambda]*\[Omega] + 
     (1344*I)*a^4*rh^6*\[Lambda]*\[Omega] - (4704*I)*a^4*(-3 + k)*rh^6*
      \[Lambda]*\[Omega] + (14784*I)*a^4*(4 - k)*(-3 + k)*rh^6*\[Lambda]*
      \[Omega] - (4704*I)*a^4*m^2*rh^6*\[Lambda]*\[Omega] + 
     17472*a^3*m*M*rh^6*\[Lambda]*\[Omega] + (14784*I)*a^2*(4 - k)*(-3 + k)*
      M^2*rh^6*\[Lambda]*\[Omega] - 16320*a^3*m*rh^7*\[Lambda]*\[Omega] + 
     (3360*I)*a^2*M*rh^7*\[Lambda]*\[Omega] + (1920*I)*a^2*(-3 + k)*M*rh^7*
      \[Lambda]*\[Omega] - (25344*I)*a^2*(4 - k)*(-3 + k)*M*rh^7*\[Lambda]*
      \[Omega] - (1320*I)*a^2*rh^8*\[Lambda]*\[Omega] - 
     (1320*I)*a^2*(-3 + k)*rh^8*\[Lambda]*\[Omega] + 
     (10296*I)*a^2*(4 - k)*(-3 + k)*rh^8*\[Lambda]*\[Omega] - 
     (2640*I)*a^2*m^2*rh^8*\[Lambda]*\[Omega] + 6600*a*m*M*rh^8*\[Lambda]*
      \[Omega] - (3960*I)*M^2*rh^8*\[Lambda]*\[Omega] - 
     5280*a*m*rh^9*\[Lambda]*\[Omega] + (2640*I)*M*rh^9*\[Lambda]*\[Omega] - 
     32*a^10*rh*\[Kappa]*\[Lambda]*\[Omega] - 48*a^10*(-3 + k)*rh*\[Kappa]*
      \[Lambda]*\[Omega] + 96*a^8*M*rh^2*\[Kappa]*\[Lambda]*\[Omega] + 
     224*a^8*(-3 + k)*M*rh^2*\[Kappa]*\[Lambda]*\[Omega] - 
     560*a^8*rh^3*\[Kappa]*\[Lambda]*\[Omega] - 1600*a^8*(-3 + k)*rh^3*
      \[Kappa]*\[Lambda]*\[Omega] + 720*a^6*M*rh^4*\[Kappa]*\[Lambda]*
      \[Omega] + 5440*a^6*(-3 + k)*M*rh^4*\[Kappa]*\[Lambda]*\[Omega] - 
     1848*a^6*rh^5*\[Kappa]*\[Lambda]*\[Omega] - 10976*a^6*(-3 + k)*rh^5*
      \[Kappa]*\[Lambda]*\[Omega] + 1344*a^4*M*rh^6*\[Kappa]*\[Lambda]*
      \[Omega] + 20160*a^4*(-3 + k)*M*rh^6*\[Kappa]*\[Lambda]*\[Omega] - 
     1824*a^4*rh^7*\[Kappa]*\[Lambda]*\[Omega] - 21120*a^4*(-3 + k)*rh^7*
      \[Kappa]*\[Lambda]*\[Omega] + 720*a^2*M*rh^8*\[Kappa]*\[Lambda]*
      \[Omega] + 15840*a^2*(-3 + k)*M*rh^8*\[Kappa]*\[Lambda]*\[Omega] - 
     440*a^2*rh^9*\[Kappa]*\[Lambda]*\[Omega] - 11440*a^2*(-3 + k)*rh^9*
      \[Kappa]*\[Lambda]*\[Omega] + (32*I)*a^10*rh^2*\[Kappa]^2*\[Lambda]*
      \[Omega] + (640*I)*a^8*rh^4*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (3024*I)*a^6*rh^6*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (4704*I)*a^4*rh^8*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (2288*I)*a^2*rh^10*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (80*I)*a^8*rh^2*\[Lambda]^2*\[Omega] - 40*a^7*m*rh^3*\[Lambda]^2*
      \[Omega] - (320*I)*a^6*M*rh^3*\[Lambda]^2*\[Omega] + 
     (840*I)*a^6*rh^4*\[Lambda]^2*\[Omega] - 336*a^5*m*rh^5*\[Lambda]^2*
      \[Omega] - (1792*I)*a^4*M*rh^5*\[Lambda]^2*\[Omega] + 
     (2016*I)*a^4*rh^6*\[Lambda]^2*\[Omega] - 720*a^3*m*rh^7*\[Lambda]^2*
      \[Omega] - (1920*I)*a^2*M*rh^7*\[Lambda]^2*\[Omega] + 
     (1320*I)*a^2*rh^8*\[Lambda]^2*\[Omega] - 440*a*m*rh^9*\[Lambda]^2*
      \[Omega] - (32*I)*a^8*M*rh*(-51 + 11*\[Lambda])*\[Omega] + 
     (288*I)*a^8*M*rh^3*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (1440*I)*a^6*M*rh^5*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (1344*I)*a^4*M*rh^7*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
     96*a^8*(-3 + k)*M*rh^2*(\[Kappa] - 3*\[Omega])*\[Omega] + 
     480*a^6*(-3 + k)*M^2*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     720*a^6*(-3 + k)*M*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     24*a^8*(-3 + k)*M*rh^2*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     240*a^6*(-3 + k)*M^2*rh^3*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     720*a^6*(-3 + k)*M*rh^4*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     1680*a^4*(-3 + k)*M^2*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     1680*a^4*(-3 + k)*M*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     (72*I)*a^11*m*\[Omega]^2 - 24*a^10*M*\[Omega]^2 + 
     240*a^10*(-3 + k)*M*\[Omega]^2 - 96*a^10*(4 - k)*(-3 + k)*M*\[Omega]^2 - 
     960*a^10*(-3 + k)*rh*\[Omega]^2 + 648*a^10*(4 - k)*(-3 + k)*rh*
      \[Omega]^2 - 576*a^10*m^2*rh*\[Omega]^2 - (864*I)*a^9*m*M*rh*
      \[Omega]^2 - 384*a^8*M^2*rh*\[Omega]^2 - 1440*a^8*(-3 + k)*M^2*rh*
      \[Omega]^2 + 576*a^8*(4 - k)*(-3 + k)*M^2*rh*\[Omega]^2 + 
     (2400*I)*a^9*m*rh^2*\[Omega]^2 + 9360*a^8*(-3 + k)*M*rh^2*\[Omega]^2 - 
     7056*a^8*(4 - k)*(-3 + k)*M*rh^2*\[Omega]^2 + 5040*a^8*rh^3*\[Omega]^2 - 
     10080*a^8*(-3 + k)*rh^3*\[Omega]^2 + 10752*a^8*(4 - k)*(-3 + k)*rh^3*
      \[Omega]^2 - 9360*a^8*m^2*rh^3*\[Omega]^2 - (10080*I)*a^7*m*M*rh^3*
      \[Omega]^2 + 5760*a^6*M^2*rh^3*\[Omega]^2 - 12960*a^6*(-3 + k)*M^2*rh^3*
      \[Omega]^2 + 13440*a^6*(4 - k)*(-3 + k)*M^2*rh^3*\[Omega]^2 + 
     (10080*I)*a^7*m*rh^4*\[Omega]^2 - 19320*a^6*M*rh^4*\[Omega]^2 + 
     34680*a^6*(-3 + k)*M*rh^4*\[Omega]^2 - 54432*a^6*(4 - k)*(-3 + k)*M*rh^4*
      \[Omega]^2 + 10752*a^6*rh^5*\[Omega]^2 - 24192*a^6*(-3 + k)*rh^5*
      \[Omega]^2 + 42336*a^6*(4 - k)*(-3 + k)*rh^5*\[Omega]^2 - 
     30912*a^6*m^2*rh^5*\[Omega]^2 - (17472*I)*a^5*m*M*rh^5*\[Omega]^2 + 
     16128*a^4*M^2*rh^5*\[Omega]^2 - 8736*a^4*(-3 + k)*M^2*rh^5*\[Omega]^2 + 
     48384*a^4*(4 - k)*(-3 + k)*M^2*rh^5*\[Omega]^2 + 
     (12096*I)*a^5*m*rh^6*\[Omega]^2 - 24192*a^4*M*rh^6*\[Omega]^2 + 
     23520*a^4*(-3 + k)*M*rh^6*\[Omega]^2 - 110880*a^4*(4 - k)*(-3 + k)*M*
      rh^6*\[Omega]^2 + 8640*a^4*rh^7*\[Omega]^2 - 15840*a^4*(-3 + k)*rh^7*
      \[Omega]^2 + 57024*a^4*(4 - k)*(-3 + k)*rh^7*\[Omega]^2 - 
     33120*a^4*m^2*rh^7*\[Omega]^2 + (2880*I)*a^3*m*M*rh^7*\[Omega]^2 + 
     5760*a^2*M^2*rh^7*\[Omega]^2 + 9792*a^2*(-3 + k)*M^2*rh^7*\[Omega]^2 + 
     38016*a^2*(4 - k)*(-3 + k)*M^2*rh^7*\[Omega]^2 + 
     (3960*I)*a^3*m*rh^8*\[Omega]^2 - 7920*a^2*M*rh^8*\[Omega]^2 - 
     6840*a^2*(-3 + k)*M*rh^8*\[Omega]^2 - 61776*a^2*(4 - k)*(-3 + k)*M*rh^8*
      \[Omega]^2 + 2640*a^2*rh^9*\[Omega]^2 + 24024*a^2*(4 - k)*(-3 + k)*rh^9*
      \[Omega]^2 - 10560*a^2*m^2*rh^9*\[Omega]^2 + (10560*I)*a*m*M*rh^9*
      \[Omega]^2 + (48*I)*a^12*\[Kappa]*\[Omega]^2 + 
     (48*I)*a^12*(-3 + k)*\[Kappa]*\[Omega]^2 - (384*I)*a^10*M*rh*\[Kappa]*
      \[Omega]^2 - (480*I)*a^10*(-3 + k)*M*rh*\[Kappa]*\[Omega]^2 + 
     (1920*I)*a^10*rh^2*\[Kappa]*\[Omega]^2 + (3240*I)*a^10*(-3 + k)*rh^2*
      \[Kappa]*\[Omega]^2 - (4608*I)*a^8*M*rh^3*\[Kappa]*\[Omega]^2 - 
     (11760*I)*a^8*(-3 + k)*M*rh^3*\[Kappa]*\[Omega]^2 + 
     (10080*I)*a^8*rh^4*\[Kappa]*\[Omega]^2 + (26880*I)*a^8*(-3 + k)*rh^4*
      \[Kappa]*\[Omega]^2 - (10368*I)*a^6*M*rh^5*\[Kappa]*\[Omega]^2 - 
     (54432*I)*a^6*(-3 + k)*M*rh^5*\[Kappa]*\[Omega]^2 + 
     (16128*I)*a^6*rh^6*\[Kappa]*\[Omega]^2 + (70560*I)*a^6*(-3 + k)*rh^6*
      \[Kappa]*\[Omega]^2 - (6144*I)*a^4*M*rh^7*\[Kappa]*\[Omega]^2 - 
     (79200*I)*a^4*(-3 + k)*M*rh^7*\[Kappa]*\[Omega]^2 + 
     (7920*I)*a^4*rh^8*\[Kappa]*\[Omega]^2 + (71280*I)*a^4*(-3 + k)*rh^8*
      \[Kappa]*\[Omega]^2 - (34320*I)*a^2*(-3 + k)*M*rh^9*\[Kappa]*
      \[Omega]^2 + (24024*I)*a^2*(-3 + k)*rh^10*\[Kappa]*\[Omega]^2 + 
     96*a^12*rh*\[Kappa]^2*\[Omega]^2 + 2160*a^10*rh^3*\[Kappa]^2*
      \[Omega]^2 + 10752*a^8*rh^5*\[Kappa]^2*\[Omega]^2 + 
     20160*a^6*rh^7*\[Kappa]^2*\[Omega]^2 + 15840*a^4*rh^9*\[Kappa]^2*
      \[Omega]^2 + 4368*a^2*rh^11*\[Kappa]^2*\[Omega]^2 - 
     16*a^10*rh*(-39 - 8*\[Lambda])*\[Omega]^2 + (240*I)*a^9*m*rh^2*\[Lambda]*
      \[Omega]^2 + 2360*a^8*rh^3*\[Lambda]*\[Omega]^2 + 
     (2800*I)*a^7*m*rh^4*\[Lambda]*\[Omega]^2 - 5880*a^6*M*rh^4*\[Lambda]*
      \[Omega]^2 + 9184*a^6*rh^5*\[Lambda]*\[Omega]^2 + 
     (8064*I)*a^5*m*rh^6*\[Lambda]*\[Omega]^2 - 12096*a^4*M*rh^6*\[Lambda]*
      \[Omega]^2 + 12480*a^4*rh^7*\[Lambda]*\[Omega]^2 + 
     (7920*I)*a^3*m*rh^8*\[Lambda]*\[Omega]^2 - 6600*a^2*M*rh^8*\[Lambda]*
      \[Omega]^2 + 6160*a^2*rh^9*\[Lambda]*\[Omega]^2 + 
     (2288*I)*a*m*rh^10*\[Lambda]*\[Omega]^2 + 728*rh^11*\[Lambda]*
      \[Omega]^2 + 20*a^8*rh^3*\[Lambda]^2*\[Omega]^2 + 
     224*a^6*rh^5*\[Lambda]^2*\[Omega]^2 + 720*a^4*rh^7*\[Lambda]^2*
      \[Omega]^2 + 880*a^2*rh^9*\[Lambda]^2*\[Omega]^2 + 
     364*rh^11*\[Lambda]^2*\[Omega]^2 - 160*a^8*M*rh^2*(21 + 4*\[Lambda])*
      \[Omega]^2 - (24*I)*a^12*\[Omega]^3 + 384*a^11*m*rh*\[Omega]^3 + 
     (288*I)*a^10*M*rh*\[Omega]^3 + 7440*a^9*m*rh^3*\[Omega]^3 + 
     (3600*I)*a^8*M*rh^3*\[Omega]^3 - (1680*I)*a^8*rh^4*\[Omega]^3 + 
     30912*a^7*m*rh^5*\[Omega]^3 + (5376*I)*a^6*M*rh^5*\[Omega]^3 + 
     (4032*I)*a^6*rh^6*\[Omega]^3 + 46080*a^5*m*rh^7*\[Omega]^3 - 
     (8640*I)*a^4*M*rh^7*\[Omega]^3 + (11880*I)*a^4*rh^8*\[Omega]^3 + 
     26400*a^3*m*rh^9*\[Omega]^3 - (15840*I)*a^2*M*rh^9*\[Omega]^3 + 
     (6864*I)*a^2*rh^10*\[Omega]^3 + 4368*a*m*rh^11*\[Omega]^3 - 
     (4368*I)*M*rh^11*\[Omega]^3 - (1120*I)*a^8*rh^4*\[Lambda]*\[Omega]^3 - 
     (4032*I)*a^6*rh^6*\[Lambda]*\[Omega]^3 - (5280*I)*a^4*rh^8*\[Lambda]*
      \[Omega]^3 - (2288*I)*a^2*rh^10*\[Lambda]*\[Omega]^3 - 
     (80*I)*a^10*rh^2*(9 + \[Lambda])*\[Omega]^3 - 96*a^12*rh*\[Omega]^4 - 
     2160*a^10*rh^3*\[Omega]^4 - 10752*a^8*rh^5*\[Omega]^4 - 
     20160*a^6*rh^7*\[Omega]^4 - 15840*a^4*rh^9*\[Omega]^4 - 
     4368*a^2*rh^11*\[Omega]^4 + 840*a^5*(-3 + k)*m*M*rh^4*
      (2*\[Kappa] + \[Omega]) - 3360*a^3*(-3 + k)*m*M^2*rh^5*
      (2*\[Kappa] + \[Omega]) + 6048*a^3*(-3 + k)*m*M*rh^6*
      (2*\[Kappa] + \[Omega]) - 10080*a*(-3 + k)*m*M^2*rh^7*
      (2*\[Kappa] + \[Omega]) + 7920*a*(-3 + k)*m*M*rh^8*
      (2*\[Kappa] + \[Omega]) - 360*a^6*(-3 + k)*M*rh^4*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 1680*a^4*(-3 + k)*M^2*rh^5*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 3360*a^4*(-3 + k)*M*rh^6*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 6048*a^2*(-3 + k)*M^2*rh^7*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 5040*a^2*(-3 + k)*M*rh^8*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 6*a^8*(4 - k)*(-3 + k)*rh*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     84*a^6*(4 - k)*(-3 + k)*M*rh^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 224*a^6*(4 - k)*(-3 + k)*rh^3*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     224*a^4*(4 - k)*(-3 + k)*M^2*rh^3*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 1512*a^4*(4 - k)*(-3 + k)*M*rh^4*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     1512*a^4*(4 - k)*(-3 + k)*rh^5*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 2016*a^2*(4 - k)*(-3 + k)*M^2*rh^5*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     5544*a^2*(4 - k)*(-3 + k)*M*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 3168*a^2*(4 - k)*(-3 + k)*rh^7*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     3168*(4 - k)*(-3 + k)*M^2*rh^7*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 5148*(4 - k)*(-3 + k)*M*rh^8*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     2002*(4 - k)*(-3 + k)*rh^9*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 56*a^6*rh^5*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     360*a^4*rh^7*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 660*a^2*rh^9*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     364*rh^11*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     (140*I)*a^4*(-3 + k)*M*rh^4*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (504*I)*a^2*(-3 + k)*M^2*rh^5*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (840*I)*a^2*(-3 + k)*M*rh^6*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (1320*I)*(-3 + k)*M^2*rh^7*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - (990*I)*(-3 + k)*M*rh^8*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     420*a^4*(-3 + k)*rh^6*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 1320*a^2*(-3 + k)*M*rh^7*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     1980*a^2*(-3 + k)*rh^8*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 2860*(-3 + k)*M*rh^9*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     2002*(-3 + k)*rh^10*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-3 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  ((60*a^10 + 216*a^10*(-2 + k) - 60*a^10*(3 - k)*(-2 + k) + 36*a^10*m^2 - 
     48*a^10*(-2 + k)*m^2 + 24*a^10*(3 - k)*(-2 + k)*m^2 + (72*I)*a^9*m*M + 
     (384*I)*a^9*(-2 + k)*m*M - (120*I)*a^9*(3 - k)*(-2 + k)*m*M + 
     288*a^8*M^2 + 576*a^8*(-2 + k)*M^2 - 144*a^8*(3 - k)*(-2 + k)*M^2 - 
     (72*I)*a^9*m*rh - (1152*I)*a^9*(-2 + k)*m*rh + 
     (480*I)*a^9*(3 - k)*(-2 + k)*m*rh + (72*I)*a^9*m^3*rh - 648*a^8*M*rh - 
     4032*a^8*(-2 + k)*M*rh + 1440*a^8*(3 - k)*(-2 + k)*M*rh - 
     72*a^8*m^2*M*rh + 960*a^8*(-2 + k)*m^2*M*rh - 480*a^8*(3 - k)*(-2 + k)*
      m^2*M*rh - (432*I)*a^7*m*M^2*rh - (2496*I)*a^7*(-2 + k)*m*M^2*rh + 
     (960*I)*a^7*(3 - k)*(-2 + k)*m*M^2*rh - 576*a^6*M^3*rh - 
     1536*a^6*(-2 + k)*M^3*rh + 480*a^6*(3 - k)*(-2 + k)*M^3*rh + 
     216*a^8*rh^2 + 3840*a^8*(-2 + k)*rh^2 - 1800*a^8*(3 - k)*(-2 + k)*rh^2 + 
     792*a^8*m^2*rh^2 - 1920*a^8*(-2 + k)*m^2*rh^2 + 
     1440*a^8*(3 - k)*(-2 + k)*m^2*rh^2 - 144*a^8*m^4*rh^2 + 
     (11040*I)*a^7*(-2 + k)*m*M*rh^2 - (5760*I)*a^7*(3 - k)*(-2 + k)*m*M*
      rh^2 - (432*I)*a^7*m^3*M*rh^2 + 864*a^6*M^2*rh^2 + 
     14400*a^6*(-2 + k)*M^2*rh^2 - 6480*a^6*(3 - k)*(-2 + k)*M^2*rh^2 - 
     576*a^6*m^2*M^2*rh^2 - 2880*a^6*(-2 + k)*m^2*M^2*rh^2 + 
     1440*a^6*(3 - k)*(-2 + k)*m^2*M^2*rh^2 + (2880*I)*a^5*(-2 + k)*m*M^3*
      rh^2 - (1440*I)*a^5*(3 - k)*(-2 + k)*m*M^3*rh^2 - (960*I)*a^7*m*rh^3 - 
     (8640*I)*a^7*(-2 + k)*m*rh^3 + (5040*I)*a^7*(3 - k)*(-2 + k)*m*rh^3 + 
     (960*I)*a^7*m^3*rh^3 - 25920*a^6*(-2 + k)*M*rh^3 + 
     15120*a^6*(3 - k)*(-2 + k)*M*rh^3 - 2400*a^6*m^2*M*rh^3 + 
     12480*a^6*(-2 + k)*m^2*M*rh^3 - 10080*a^6*(3 - k)*(-2 + k)*m^2*M*rh^3 + 
     (4320*I)*a^5*m*M^2*rh^3 - (24000*I)*a^5*(-2 + k)*m*M^2*rh^3 + 
     (16800*I)*a^5*(3 - k)*(-2 + k)*m*M^2*rh^3 + 960*a^4*M^3*rh^3 - 
     11520*a^4*(-2 + k)*M^3*rh^3 + 6720*a^4*(3 - k)*(-2 + k)*M^3*rh^3 - 
     180*a^6*rh^4 + 11760*a^6*(-2 + k)*rh^4 - 8400*a^6*(3 - k)*(-2 + k)*
      rh^4 + 2340*a^6*m^2*rh^4 - 10080*a^6*(-2 + k)*m^2*rh^4 + 
     10080*a^6*(3 - k)*(-2 + k)*m^2*rh^4 - 720*a^6*m^4*rh^4 - 
     (360*I)*a^5*m*M*rh^4 + (38640*I)*a^5*(-2 + k)*m*M*rh^4 - 
     (30240*I)*a^5*(3 - k)*(-2 + k)*m*M*rh^4 - (2160*I)*a^5*m^3*M*rh^4 - 
     2160*a^4*M^2*rh^4 + 40320*a^4*(-2 + k)*M^2*rh^4 - 
     30240*a^4*(3 - k)*(-2 + k)*M^2*rh^4 + 2880*a^4*m^2*M^2*rh^4 - 
     13440*a^4*(-2 + k)*m^2*M^2*rh^4 + 13440*a^4*(3 - k)*(-2 + k)*m^2*M^2*
      rh^4 - (5760*I)*a^3*m*M^3*rh^4 + (13440*I)*a^3*(-2 + k)*m*M^3*rh^4 - 
     (13440*I)*a^3*(3 - k)*(-2 + k)*m*M^3*rh^4 - (2520*I)*a^5*m*rh^5 - 
     (16128*I)*a^5*(-2 + k)*m*rh^5 + (12096*I)*a^5*(3 - k)*(-2 + k)*m*rh^5 + 
     (2520*I)*a^5*m^3*rh^5 + 1512*a^4*M*rh^5 - 40320*a^4*(-2 + k)*M*rh^5 + 
     36288*a^4*(3 - k)*(-2 + k)*M*rh^5 - 4536*a^4*m^2*M*rh^5 + 
     29568*a^4*(-2 + k)*m^2*M*rh^5 - 36288*a^4*(3 - k)*(-2 + k)*m^2*M*rh^5 + 
     (7056*I)*a^3*m*M^2*rh^5 - (40320*I)*a^3*(-2 + k)*m*M^2*rh^5 + 
     (48384*I)*a^3*(3 - k)*(-2 + k)*m*M^2*rh^5 - 10752*a^2*(-2 + k)*M^3*
      rh^5 + 12096*a^2*(3 - k)*(-2 + k)*M^3*rh^5 - 336*a^4*rh^6 + 
     12096*a^4*(-2 + k)*rh^6 - 12600*a^4*(3 - k)*(-2 + k)*rh^6 + 
     1680*a^4*m^2*rh^6 - 16128*a^4*(-2 + k)*m^2*rh^6 + 
     20160*a^4*(3 - k)*(-2 + k)*m^2*rh^6 - 672*a^4*m^4*rh^6 + 
     (1344*I)*a^3*m*M*rh^6 + (36288*I)*a^3*(-2 + k)*m*M*rh^6 - 
     (40320*I)*a^3*(3 - k)*(-2 + k)*m*M*rh^6 - (2016*I)*a^3*m^3*M*rh^6 + 
     24192*a^2*(-2 + k)*M^2*rh^6 - 30240*a^2*(3 - k)*(-2 + k)*M^2*rh^6 - 
     8064*a^2*(-2 + k)*m^2*M^2*rh^6 + 20160*a^2*(3 - k)*(-2 + k)*m^2*M^2*
      rh^6 - (2688*I)*a*m*M^3*rh^6 + (8064*I)*a*(-2 + k)*m*M^3*rh^6 - 
     (20160*I)*a*(3 - k)*(-2 + k)*m*M^3*rh^6 - (1728*I)*a^3*m*rh^7 - 
     (8640*I)*a^3*(-2 + k)*m*rh^7 + (7920*I)*a^3*(3 - k)*(-2 + k)*m*rh^7 + 
     (1728*I)*a^3*m^3*rh^7 - 17280*a^2*(-2 + k)*M*rh^7 + 
     23760*a^2*(3 - k)*(-2 + k)*M*rh^7 + 17280*a^2*(-2 + k)*m^2*M*rh^7 - 
     31680*a^2*(3 - k)*(-2 + k)*m^2*M*rh^7 + (1728*I)*a*m*M^2*rh^7 - 
     (17280*I)*a*(-2 + k)*m*M^2*rh^7 + (31680*I)*a*(3 - k)*(-2 + k)*m*M^2*
      rh^7 + 3960*a^2*(-2 + k)*rh^8 - 5940*a^2*(3 - k)*(-2 + k)*rh^8 - 
     7920*a^2*(-2 + k)*m^2*rh^8 + 11880*a^2*(3 - k)*(-2 + k)*m^2*rh^8 + 
     (7920*I)*a*(-2 + k)*m*M*rh^8 - (11880*I)*a*(3 - k)*(-2 + k)*m*M*rh^8 - 
     72*a^11*m*\[Kappa] - 48*a^11*(-2 + k)*m*\[Kappa] + 
     (168*I)*a^10*M*\[Kappa] + (96*I)*a^10*(-2 + k)*M*\[Kappa] - 
     (648*I)*a^10*rh*\[Kappa] - (480*I)*a^10*(-2 + k)*rh*\[Kappa] + 
     (144*I)*a^10*m^2*rh*\[Kappa] + (192*I)*a^10*(-2 + k)*m^2*rh*\[Kappa] + 
     576*a^9*m*M*rh*\[Kappa] + 528*a^9*(-2 + k)*m*M*rh*\[Kappa] - 
     (432*I)*a^8*M^2*rh*\[Kappa] - (384*I)*a^8*(-2 + k)*M^2*rh*\[Kappa] - 
     1728*a^9*m*rh^2*\[Kappa] - 1920*a^9*(-2 + k)*m*rh^2*\[Kappa] + 
     (3744*I)*a^8*M*rh^2*\[Kappa] + (3840*I)*a^8*(-2 + k)*M*rh^2*\[Kappa] - 
     (576*I)*a^8*m^2*M*rh^2*\[Kappa] - (960*I)*a^8*(-2 + k)*m^2*M*rh^2*
      \[Kappa] - 576*a^7*m*M^2*rh^2*\[Kappa] - 576*a^7*(-2 + k)*m*M^2*rh^2*
      \[Kappa] - (3840*I)*a^8*rh^3*\[Kappa] - (4800*I)*a^8*(-2 + k)*rh^3*
      \[Kappa] + (1920*I)*a^8*m^2*rh^3*\[Kappa] + (3840*I)*a^8*(-2 + k)*m^2*
      rh^3*\[Kappa] + 6240*a^7*m*M*rh^3*\[Kappa] + 7680*a^7*(-2 + k)*m*M*rh^3*
      \[Kappa] - (4320*I)*a^6*M^2*rh^3*\[Kappa] - (5760*I)*a^6*(-2 + k)*M^2*
      rh^3*\[Kappa] - 6480*a^7*m*rh^4*\[Kappa] - 10080*a^7*(-2 + k)*m*rh^4*
      \[Kappa] + (12960*I)*a^6*M*rh^4*\[Kappa] + (20160*I)*a^6*(-2 + k)*M*
      rh^4*\[Kappa] - (4320*I)*a^6*m^2*M*rh^4*\[Kappa] - 
     (10080*I)*a^6*(-2 + k)*m^2*M*rh^4*\[Kappa] - 4320*a^5*m*M^2*rh^4*
      \[Kappa] - 4320*a^5*(-2 + k)*m*M^2*rh^4*\[Kappa] - 
     (7056*I)*a^6*rh^5*\[Kappa] - (13440*I)*a^6*(-2 + k)*rh^5*\[Kappa] + 
     (6048*I)*a^6*m^2*rh^5*\[Kappa] + (16128*I)*a^6*(-2 + k)*m^2*rh^5*
      \[Kappa] + 15120*a^5*m*M*rh^5*\[Kappa] + 22176*a^5*(-2 + k)*m*M*rh^5*
      \[Kappa] - (9072*I)*a^4*M^2*rh^5*\[Kappa] - (16128*I)*a^4*(-2 + k)*M^2*
      rh^5*\[Kappa] - 8064*a^5*m*rh^6*\[Kappa] - 16128*a^5*(-2 + k)*m*rh^6*
      \[Kappa] + (14784*I)*a^4*M*rh^6*\[Kappa] + (32256*I)*a^4*(-2 + k)*M*
      rh^6*\[Kappa] - (8064*I)*a^4*m^2*M*rh^6*\[Kappa] - 
     (24192*I)*a^4*(-2 + k)*m^2*M*rh^6*\[Kappa] - 8064*a^3*m*M^2*rh^6*
      \[Kappa] - 8064*a^3*(-2 + k)*m*M^2*rh^6*\[Kappa] - 
     (5184*I)*a^4*rh^7*\[Kappa] - (14400*I)*a^4*(-2 + k)*rh^7*\[Kappa] + 
     (6912*I)*a^4*m^2*rh^7*\[Kappa] + (23040*I)*a^4*(-2 + k)*m^2*rh^7*
      \[Kappa] + 12096*a^3*m*M*rh^7*\[Kappa] + 18432*a^3*(-2 + k)*m*M*rh^7*
      \[Kappa] - (5184*I)*a^2*M^2*rh^7*\[Kappa] - (11520*I)*a^2*(-2 + k)*M^2*
      rh^7*\[Kappa] - 3240*a^3*m*rh^8*\[Kappa] - 7920*a^3*(-2 + k)*m*rh^8*
      \[Kappa] + (5400*I)*a^2*M*rh^8*\[Kappa] + (15840*I)*a^2*(-2 + k)*M*rh^8*
      \[Kappa] - (4320*I)*a^2*m^2*M*rh^8*\[Kappa] - 
     (15840*I)*a^2*(-2 + k)*m^2*M*rh^8*\[Kappa] - 
     4320*a*m*M^2*rh^8*\[Kappa] - 4320*a*(-2 + k)*m*M^2*rh^8*\[Kappa] - 
     (1320*I)*a^2*rh^9*\[Kappa] - (5280*I)*a^2*(-2 + k)*rh^9*\[Kappa] + 
     (2640*I)*a^2*m^2*rh^9*\[Kappa] + (10560*I)*a^2*(-2 + k)*m^2*rh^9*
      \[Kappa] + 2640*a*m*M*rh^9*\[Kappa] + 2640*a*(-2 + k)*m*M*rh^9*
      \[Kappa] - 12*a^12*\[Kappa]^2 + (72*I)*a^11*m*rh*\[Kappa]^2 + 
     72*a^10*M*rh*\[Kappa]^2 - 360*a^10*rh^2*\[Kappa]^2 + 
     144*a^10*m^2*rh^2*\[Kappa]^2 - (72*I)*a^9*m*M*rh^2*\[Kappa]^2 + 
     (960*I)*a^9*m*rh^3*\[Kappa]^2 + 960*a^8*M*rh^3*\[Kappa]^2 - 
     1800*a^8*rh^4*\[Kappa]^2 + 1440*a^8*m^2*rh^4*\[Kappa]^2 - 
     (720*I)*a^7*m*M*rh^4*\[Kappa]^2 + (3024*I)*a^7*m*rh^5*\[Kappa]^2 + 
     3024*a^6*M*rh^5*\[Kappa]^2 - 3360*a^6*rh^6*\[Kappa]^2 + 
     4032*a^6*m^2*rh^6*\[Kappa]^2 - (2520*I)*a^5*m*M*rh^6*\[Kappa]^2 + 
     (3456*I)*a^5*m*rh^7*\[Kappa]^2 + 3456*a^4*M*rh^7*\[Kappa]^2 - 
     2700*a^4*rh^8*\[Kappa]^2 + 4320*a^4*m^2*rh^8*\[Kappa]^2 - 
     (3456*I)*a^3*m*M*rh^8*\[Kappa]^2 + (1320*I)*a^3*m*rh^9*\[Kappa]^2 + 
     1320*a^2*M*rh^9*\[Kappa]^2 - 792*a^2*rh^10*\[Kappa]^2 + 
     1584*a^2*m^2*rh^10*\[Kappa]^2 - (1584*I)*a*m*M*rh^10*\[Kappa]^2 - 
     (96*I)*a^9*m*rh*\[Lambda] + (32*I)*a^9*(-2 + k)*m*rh*\[Lambda] - 
     (40*I)*a^9*(3 - k)*(-2 + k)*m*rh*\[Lambda] - 96*a^8*rh^2*\[Lambda] + 
     192*a^8*m^2*rh^2*\[Lambda] + (528*I)*a^7*m*M*rh^2*\[Lambda] - 
     (480*I)*a^7*(-2 + k)*m*M*rh^2*\[Lambda] + (480*I)*a^7*(3 - k)*(-2 + k)*m*
      M*rh^2*\[Lambda] - (1200*I)*a^7*m*rh^3*\[Lambda] + 
     (640*I)*a^7*(-2 + k)*m*rh^3*\[Lambda] - (1120*I)*a^7*(3 - k)*(-2 + k)*m*
      rh^3*\[Lambda] + (80*I)*a^7*m^3*rh^3*\[Lambda] + 
     480*a^6*M*rh^3*\[Lambda] - 80*a^6*(-2 + k)*M*rh^3*\[Lambda] - 
     640*a^6*m^2*M*rh^3*\[Lambda] - (480*I)*a^5*m*M^2*rh^3*\[Lambda] + 
     (1280*I)*a^5*(-2 + k)*m*M^2*rh^3*\[Lambda] - (1120*I)*a^5*(3 - k)*
      (-2 + k)*m*M^2*rh^3*\[Lambda] - 810*a^6*rh^4*\[Lambda] + 
     1470*a^6*m^2*rh^4*\[Lambda] + (4440*I)*a^5*m*M*rh^4*\[Lambda] - 
     (3920*I)*a^5*(-2 + k)*m*M*rh^4*\[Lambda] + (6720*I)*a^5*(3 - k)*(-2 + k)*
      m*M*rh^4*\[Lambda] - 480*a^4*M^2*rh^4*\[Lambda] + 
     280*a^4*(-2 + k)*M^2*rh^4*\[Lambda] - (3360*I)*a^5*m*rh^5*\[Lambda] + 
     (2688*I)*a^5*(-2 + k)*m*rh^5*\[Lambda] - (6048*I)*a^5*(3 - k)*(-2 + k)*m*
      rh^5*\[Lambda] + (336*I)*a^5*m^3*rh^5*\[Lambda] + 
     2772*a^4*M*rh^5*\[Lambda] - 448*a^4*(-2 + k)*M*rh^5*\[Lambda] - 
     2688*a^4*m^2*M*rh^5*\[Lambda] - (4032*I)*a^3*m*M^2*rh^5*\[Lambda] + 
     (3584*I)*a^3*(-2 + k)*m*M^2*rh^5*\[Lambda] - (8064*I)*a^3*(3 - k)*
      (-2 + k)*m*M^2*rh^5*\[Lambda] - 1792*a^4*rh^6*\[Lambda] + 
     2800*a^4*m^2*rh^6*\[Lambda] + (7392*I)*a^3*m*M*rh^6*\[Lambda] - 
     (6720*I)*a^3*(-2 + k)*m*M*rh^6*\[Lambda] + (20160*I)*a^3*(3 - k)*
      (-2 + k)*m*M*rh^6*\[Lambda] - 2240*a^2*M^2*rh^6*\[Lambda] + 
     672*a^2*(-2 + k)*M^2*rh^6*\[Lambda] - (3168*I)*a^3*m*rh^7*\[Lambda] + 
     (3840*I)*a^3*(-2 + k)*m*rh^7*\[Lambda] - (10560*I)*a^3*(3 - k)*(-2 + k)*
      m*rh^7*\[Lambda] + (288*I)*a^3*m^3*rh^7*\[Lambda] + 
     3744*a^2*M*rh^7*\[Lambda] - 480*a^2*(-2 + k)*M*rh^7*\[Lambda] - 
     2304*a^2*m^2*M*rh^7*\[Lambda] - (1728*I)*a*m*M^2*rh^7*\[Lambda] - 
     (10560*I)*a*(3 - k)*(-2 + k)*m*M^2*rh^7*\[Lambda] - 
     1350*a^2*rh^8*\[Lambda] + 1530*a^2*m^2*rh^8*\[Lambda] + 
     (2520*I)*a*m*M*rh^8*\[Lambda] - (2640*I)*a*(-2 + k)*m*M*rh^8*\[Lambda] + 
     (15840*I)*a*(3 - k)*(-2 + k)*m*M*rh^8*\[Lambda] - 
     1080*M^2*rh^8*\[Lambda] - (880*I)*a*m*rh^9*\[Lambda] + 
     (1760*I)*a*(-2 + k)*m*rh^9*\[Lambda] - (5720*I)*a*(3 - k)*(-2 + k)*m*
      rh^9*\[Lambda] + 1100*M*rh^9*\[Lambda] - 264*rh^10*\[Lambda] + 
     48*a^9*m*rh^2*\[Kappa]*\[Lambda] + 160*a^9*(-2 + k)*m*rh^2*\[Kappa]*
      \[Lambda] - (80*I)*a^8*(-2 + k)*rh^3*\[Kappa]*\[Lambda] - 
     160*a^7*m*M*rh^3*\[Kappa]*\[Lambda] - 640*a^7*(-2 + k)*m*M*rh^3*\[Kappa]*
      \[Lambda] + 480*a^7*m*rh^4*\[Kappa]*\[Lambda] + 
     2240*a^7*(-2 + k)*m*rh^4*\[Kappa]*\[Lambda] + (280*I)*a^6*(-2 + k)*M*
      rh^4*\[Kappa]*\[Lambda] - (896*I)*a^6*(-2 + k)*rh^5*\[Kappa]*
      \[Lambda] - 1008*a^5*m*M*rh^5*\[Kappa]*\[Lambda] - 
     5376*a^5*(-2 + k)*m*M*rh^5*\[Kappa]*\[Lambda] + 
     1344*a^5*m*rh^6*\[Kappa]*\[Lambda] + 8064*a^5*(-2 + k)*m*rh^6*\[Kappa]*
      \[Lambda] + (2016*I)*a^4*(-2 + k)*M*rh^6*\[Kappa]*\[Lambda] - 
     (2400*I)*a^4*(-2 + k)*rh^7*\[Kappa]*\[Lambda] - 
     1728*a^3*m*M*rh^7*\[Kappa]*\[Lambda] - 11520*a^3*(-2 + k)*m*M*rh^7*
      \[Kappa]*\[Lambda] + 1440*a^3*m*rh^8*\[Kappa]*\[Lambda] + 
     10560*a^3*(-2 + k)*m*rh^8*\[Kappa]*\[Lambda] + 
     (2640*I)*a^2*(-2 + k)*M*rh^8*\[Kappa]*\[Lambda] - 
     (1760*I)*a^2*(-2 + k)*rh^9*\[Kappa]*\[Lambda] - 
     880*a*m*M*rh^9*\[Kappa]*\[Lambda] - 7040*a*(-2 + k)*m*M*rh^9*\[Kappa]*
      \[Lambda] + 528*a*m*rh^10*\[Kappa]*\[Lambda] + 
     4576*a*(-2 + k)*m*rh^10*\[Kappa]*\[Lambda] - 
     (80*I)*a^9*m*rh^3*\[Kappa]^2*\[Lambda] - 30*a^8*rh^4*\[Kappa]^2*
      \[Lambda] - (672*I)*a^7*m*rh^5*\[Kappa]^2*\[Lambda] - 
     168*a^6*rh^6*\[Kappa]^2*\[Lambda] - (1728*I)*a^5*m*rh^7*\[Kappa]^2*
      \[Lambda] - 270*a^4*rh^8*\[Kappa]^2*\[Lambda] - 
     (1760*I)*a^3*m*rh^9*\[Kappa]^2*\[Lambda] - 132*a^2*rh^10*\[Kappa]^2*
      \[Lambda] - (624*I)*a*m*rh^11*\[Kappa]^2*\[Lambda] - 
     48*a^8*rh^2*\[Lambda]^2 - (80*I)*a^7*m*rh^3*\[Lambda]^2 + 
     240*a^6*M*rh^3*\[Lambda]^2 - 40*a^6*(-2 + k)*M*rh^3*\[Lambda]^2 - 
     435*a^6*rh^4*\[Lambda]^2 + 15*a^6*m^2*rh^4*\[Lambda]^2 + 
     (240*I)*a^5*m*M*rh^4*\[Lambda]^2 - 240*a^4*M^2*rh^4*\[Lambda]^2 + 
     140*a^4*(-2 + k)*M^2*rh^4*\[Lambda]^2 - (504*I)*a^5*m*rh^5*\[Lambda]^2 + 
     1470*a^4*M*rh^5*\[Lambda]^2 - 224*a^4*(-2 + k)*M*rh^5*\[Lambda]^2 - 
     1064*a^4*rh^6*\[Lambda]^2 + 56*a^4*m^2*rh^6*\[Lambda]^2 + 
     (896*I)*a^3*m*M*rh^6*\[Lambda]^2 - 1120*a^2*M^2*rh^6*\[Lambda]^2 + 
     336*a^2*(-2 + k)*M^2*rh^6*\[Lambda]^2 - (864*I)*a^3*m*rh^7*\[Lambda]^2 + 
     2160*a^2*M*rh^7*\[Lambda]^2 - 240*a^2*(-2 + k)*M*rh^7*\[Lambda]^2 - 
     945*a^2*rh^8*\[Lambda]^2 + 45*a^2*m^2*rh^8*\[Lambda]^2 + 
     (720*I)*a*m*M*rh^8*\[Lambda]^2 - 540*M^2*rh^8*\[Lambda]^2 - 
     (440*I)*a*m*rh^9*\[Lambda]^2 + 770*M*rh^9*\[Lambda]^2 - 
     264*rh^10*\[Lambda]^2 - (40*I)*a^8*(-2 + k)*rh^3*\[Kappa]*\[Lambda]^2 + 
     (140*I)*a^6*(-2 + k)*M*rh^4*\[Kappa]*\[Lambda]^2 - 
     (448*I)*a^6*(-2 + k)*rh^5*\[Kappa]*\[Lambda]^2 + 
     (1008*I)*a^4*(-2 + k)*M*rh^6*\[Kappa]*\[Lambda]^2 - 
     (1200*I)*a^4*(-2 + k)*rh^7*\[Kappa]*\[Lambda]^2 + 
     (1320*I)*a^2*(-2 + k)*M*rh^8*\[Kappa]*\[Lambda]^2 - 
     (880*I)*a^2*(-2 + k)*rh^9*\[Kappa]*\[Lambda]^2 - 
     15*a^8*rh^4*\[Kappa]^2*\[Lambda]^2 - 84*a^6*rh^6*\[Kappa]^2*
      \[Lambda]^2 - 135*a^4*rh^8*\[Kappa]^2*\[Lambda]^2 - 
     66*a^2*rh^10*\[Kappa]^2*\[Lambda]^2 - 15*a^6*rh^4*\[Lambda]^3 + 
     42*a^4*M*rh^5*\[Lambda]^3 - 84*a^4*rh^6*\[Lambda]^3 + 
     144*a^2*M*rh^7*\[Lambda]^3 - 135*a^2*rh^8*\[Lambda]^3 + 
     110*M*rh^9*\[Lambda]^3 - 66*rh^10*\[Lambda]^3 - 
     (72*I)*a^9*m*M*rh^2*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (720*I)*a^7*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (1512*I)*a^5*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
     (864*I)*a^3*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
     48*a^9*(-2 + k)*m*M*rh*(\[Kappa] - 6*\[Omega]) - 
     384*a^7*(-2 + k)*m*M^2*rh^2*(\[Kappa] - 6*\[Omega]) + 
     960*a^7*(-2 + k)*m*M*rh^3*(\[Kappa] - 6*\[Omega]) - 
     1920*a^5*(-2 + k)*m*M^2*rh^4*(\[Kappa] - 6*\[Omega]) + 
     1680*a^5*(-2 + k)*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
     240*a^7*(-2 + k)*m*M*rh^3*(4*\[Kappa] - 5*\[Omega]) - 
     960*a^5*(-2 + k)*m*M^2*rh^4*(4*\[Kappa] - 5*\[Omega]) + 
     1680*a^5*(-2 + k)*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) - 
     2688*a^3*(-2 + k)*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) + 
     2016*a^3*(-2 + k)*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) - 
     72*a^11*m*\[Omega] + 96*a^11*(-2 + k)*m*\[Omega] - 
     48*a^11*(3 - k)*(-2 + k)*m*\[Omega] - (72*I)*a^10*M*\[Omega] - 
     (384*I)*a^10*(-2 + k)*M*\[Omega] + (120*I)*a^10*(3 - k)*(-2 + k)*M*
      \[Omega] + (1248*I)*a^10*(-2 + k)*rh*\[Omega] - 
     (600*I)*a^10*(3 - k)*(-2 + k)*rh*\[Omega] - (216*I)*a^10*m^2*rh*
      \[Omega] + 144*a^9*m*M*rh*\[Omega] - 1632*a^9*(-2 + k)*m*M*rh*
      \[Omega] + 960*a^9*(3 - k)*(-2 + k)*m*M*rh*\[Omega] + 
     (432*I)*a^8*M^2*rh*\[Omega] + (2496*I)*a^8*(-2 + k)*M^2*rh*\[Omega] - 
     (960*I)*a^8*(3 - k)*(-2 + k)*M^2*rh*\[Omega] - 
     1728*a^9*m*rh^2*\[Omega] + 3840*a^9*(-2 + k)*m*rh^2*\[Omega] - 
     3060*a^9*(3 - k)*(-2 + k)*m*rh^2*\[Omega] + 576*a^9*m^3*rh^2*\[Omega] - 
     (12480*I)*a^8*(-2 + k)*M*rh^2*\[Omega] + (7200*I)*a^8*(3 - k)*(-2 + k)*M*
      rh^2*\[Omega] + (1296*I)*a^8*m^2*M*rh^2*\[Omega] + 
     1152*a^7*m*M^2*rh^2*\[Omega] + 3456*a^7*(-2 + k)*m*M^2*rh^2*\[Omega] - 
     2880*a^7*(3 - k)*(-2 + k)*m*M^2*rh^2*\[Omega] - 
     (2880*I)*a^6*(-2 + k)*M^3*rh^2*\[Omega] + (1440*I)*a^6*(3 - k)*(-2 + k)*
      M^3*rh^2*\[Omega] - (3120*I)*a^8*rh^3*\[Omega] + 
     (10560*I)*a^8*(-2 + k)*rh^3*\[Omega] - (8400*I)*a^8*(3 - k)*(-2 + k)*
      rh^3*\[Omega] - (2640*I)*a^8*m^2*rh^3*\[Omega] + 
     5760*a^7*m*M*rh^3*\[Omega] - 18480*a^7*(-2 + k)*m*M*rh^3*\[Omega] + 
     21840*a^7*(3 - k)*(-2 + k)*m*M*rh^3*\[Omega] + 
     (28320*I)*a^6*(-2 + k)*M^2*rh^3*\[Omega] - (20160*I)*a^6*(3 - k)*
      (-2 + k)*M^2*rh^3*\[Omega] - 6300*a^7*m*rh^4*\[Omega] + 
     20160*a^7*(-2 + k)*m*rh^4*\[Omega] - 23520*a^7*(3 - k)*(-2 + k)*m*rh^4*
      \[Omega] + 3780*a^7*m^3*rh^4*\[Omega] + (19260*I)*a^6*M*rh^4*\[Omega] - 
     (50400*I)*a^6*(-2 + k)*M*rh^4*\[Omega] + (50400*I)*a^6*(3 - k)*(-2 + k)*
      M*rh^4*\[Omega] + (7020*I)*a^6*m^2*M*rh^4*\[Omega] - 
     7200*a^5*m*M^2*rh^4*\[Omega] + 12240*a^5*(-2 + k)*m*M^2*rh^4*\[Omega] - 
     30240*a^5*(3 - k)*(-2 + k)*m*M^2*rh^4*\[Omega] + 
     (8640*I)*a^4*M^3*rh^4*\[Omega] - (15120*I)*a^4*(-2 + k)*M^3*rh^4*
      \[Omega] + (13440*I)*a^4*(3 - k)*(-2 + k)*M^3*rh^4*\[Omega] - 
     (7560*I)*a^6*rh^5*\[Omega] + (23352*I)*a^6*(-2 + k)*rh^5*\[Omega] - 
     (30240*I)*a^6*(3 - k)*(-2 + k)*rh^5*\[Omega] - 
     (7560*I)*a^6*m^2*rh^5*\[Omega] + 16632*a^5*m*M*rh^5*\[Omega] - 
     42840*a^5*(-2 + k)*m*M*rh^5*\[Omega] + 90720*a^5*(3 - k)*(-2 + k)*m*M*
      rh^5*\[Omega] - (35784*I)*a^4*M^2*rh^5*\[Omega] + 
     (53760*I)*a^4*(-2 + k)*M^2*rh^5*\[Omega] - (72576*I)*a^4*(3 - k)*
      (-2 + k)*M^2*rh^5*\[Omega] - 8064*a^5*m*rh^6*\[Omega] + 
     32256*a^5*(-2 + k)*m*rh^6*\[Omega] - 55440*a^5*(3 - k)*(-2 + k)*m*rh^6*
      \[Omega] + 6048*a^5*m^3*rh^6*\[Omega] + (30240*I)*a^4*M*rh^6*\[Omega] - 
     (53760*I)*a^4*(-2 + k)*M*rh^6*\[Omega] + (100800*I)*a^4*(3 - k)*(-2 + k)*
      M*rh^6*\[Omega] + (8064*I)*a^4*m^2*M*rh^6*\[Omega] - 
     8064*a^3*m*M^2*rh^6*\[Omega] + 5376*a^3*(-2 + k)*m*M^2*rh^6*\[Omega] - 
     60480*a^3*(3 - k)*(-2 + k)*m*M^2*rh^6*\[Omega] + 
     (16128*I)*a^2*M^3*rh^6*\[Omega] - (12096*I)*a^2*(-2 + k)*M^3*rh^6*
      \[Omega] + (20160*I)*a^2*(3 - k)*(-2 + k)*M^3*rh^6*\[Omega] - 
     (6048*I)*a^4*rh^7*\[Omega] + (16128*I)*a^4*(-2 + k)*rh^7*\[Omega] - 
     (39600*I)*a^4*(3 - k)*(-2 + k)*rh^7*\[Omega] - 
     (7776*I)*a^4*m^2*rh^7*\[Omega] + 12096*a^3*m*M*rh^7*\[Omega] - 
     25632*a^3*(-2 + k)*m*M*rh^7*\[Omega] + 110880*a^3*(3 - k)*(-2 + k)*m*M*
      rh^7*\[Omega] - (29376*I)*a^2*M^2*rh^7*\[Omega] + 
     (20160*I)*a^2*(-2 + k)*M^2*rh^7*\[Omega] - (63360*I)*a^2*(3 - k)*
      (-2 + k)*M^2*rh^7*\[Omega] - 4860*a^3*m*rh^8*\[Omega] + 
     15840*a^3*(-2 + k)*m*rh^8*\[Omega] - 47520*a^3*(3 - k)*(-2 + k)*m*rh^8*
      \[Omega] + 2700*a^3*m^3*rh^8*\[Omega] + (13500*I)*a^2*M*rh^8*\[Omega] - 
     (10080*I)*a^2*(-2 + k)*M*rh^8*\[Omega] + (59400*I)*a^2*(3 - k)*(-2 + k)*
      M*rh^8*\[Omega] + (1620*I)*a^2*m^2*M*rh^8*\[Omega] - 
     2160*a*m*M^2*rh^8*\[Omega] - 2160*a*(-2 + k)*m*M^2*rh^8*\[Omega] - 
     23760*a*(3 - k)*(-2 + k)*m*M^2*rh^8*\[Omega] + 
     (6480*I)*M^3*rh^8*\[Omega] - (1320*I)*a^2*rh^9*\[Omega] + 
     (1320*I)*a^2*(-2 + k)*rh^9*\[Omega] - (17160*I)*a^2*(3 - k)*(-2 + k)*
      rh^9*\[Omega] - (2640*I)*a^2*m^2*rh^9*\[Omega] + 
     3960*a*m*M*rh^9*\[Omega] + 1320*a*(-2 + k)*m*M*rh^9*\[Omega] + 
     34320*a*(3 - k)*(-2 + k)*m*M*rh^9*\[Omega] - 
     (6600*I)*M^2*rh^9*\[Omega] - 1584*a*m*rh^10*\[Omega] - 
     12012*a*(3 - k)*(-2 + k)*m*rh^10*\[Omega] + (1584*I)*M*rh^10*\[Omega] + 
     72*a^12*\[Kappa]*\[Omega] + 48*a^12*(-2 + k)*\[Kappa]*\[Omega] - 
     (288*I)*a^11*m*rh*\[Kappa]*\[Omega] - (384*I)*a^11*(-2 + k)*m*rh*
      \[Kappa]*\[Omega] - 576*a^10*M*rh*\[Kappa]*\[Omega] - 
     576*a^10*(-2 + k)*M*rh*\[Kappa]*\[Omega] + 1872*a^10*rh^2*\[Kappa]*
      \[Omega] + 1536*a^10*(-2 + k)*rh^2*\[Kappa]*\[Omega] + 
     (576*I)*a^9*m*M*rh^2*\[Kappa]*\[Omega] + (1920*I)*a^9*(-2 + k)*m*M*rh^2*
      \[Kappa]*\[Omega] + 576*a^8*M^2*rh^2*\[Kappa]*\[Omega] + 
     864*a^8*(-2 + k)*M^2*rh^2*\[Kappa]*\[Omega] - 
     (3840*I)*a^9*m*rh^3*\[Kappa]*\[Omega] - (8160*I)*a^9*(-2 + k)*m*rh^3*
      \[Kappa]*\[Omega] - 6144*a^8*M*rh^3*\[Kappa]*\[Omega] - 
     7680*a^8*(-2 + k)*M*rh^3*\[Kappa]*\[Omega] + 7680*a^8*rh^4*\[Kappa]*
      \[Omega] + 11040*a^8*(-2 + k)*rh^4*\[Kappa]*\[Omega] + 
     (2880*I)*a^7*m*M*rh^4*\[Kappa]*\[Omega] + (21840*I)*a^7*(-2 + k)*m*M*
      rh^4*\[Kappa]*\[Omega] + 4320*a^6*M^2*rh^4*\[Kappa]*\[Omega] + 
     8400*a^6*(-2 + k)*M^2*rh^4*\[Kappa]*\[Omega] - 
     (12096*I)*a^7*m*rh^5*\[Kappa]*\[Omega] - (37632*I)*a^7*(-2 + k)*m*rh^5*
      \[Kappa]*\[Omega] - 13824*a^6*M*rh^5*\[Kappa]*\[Omega] - 
     37440*a^6*(-2 + k)*M*rh^5*\[Kappa]*\[Omega] + 10584*a^6*rh^6*\[Kappa]*
      \[Omega] + 34272*a^6*(-2 + k)*rh^6*\[Kappa]*\[Omega] + 
     (4032*I)*a^5*m*M*rh^6*\[Kappa]*\[Omega] + (60480*I)*a^5*(-2 + k)*m*M*
      rh^6*\[Kappa]*\[Omega] + 8064*a^4*M^2*rh^6*\[Kappa]*\[Omega] + 
     21168*a^4*(-2 + k)*M^2*rh^6*\[Kappa]*\[Omega] - 
     (13824*I)*a^5*m*rh^7*\[Kappa]*\[Omega] - (63360*I)*a^5*(-2 + k)*m*rh^7*
      \[Kappa]*\[Omega] - 9216*a^4*M*rh^7*\[Kappa]*\[Omega] - 
     66048*a^4*(-2 + k)*M*rh^7*\[Kappa]*\[Omega] + 4968*a^4*rh^8*\[Kappa]*
      \[Omega] + 39600*a^4*(-2 + k)*rh^8*\[Kappa]*\[Omega] + 
     (1728*I)*a^3*m*M*rh^8*\[Kappa]*\[Omega] + (55440*I)*a^3*(-2 + k)*m*M*
      rh^8*\[Kappa]*\[Omega] + 4320*a^2*M^2*rh^8*\[Kappa]*\[Omega] + 
     15552*a^2*(-2 + k)*M^2*rh^8*\[Kappa]*\[Omega] - 
     (5280*I)*a^3*m*rh^9*\[Kappa]*\[Omega] - (42240*I)*a^3*(-2 + k)*m*rh^9*
      \[Kappa]*\[Omega] - 960*a^2*M*rh^9*\[Kappa]*\[Omega] - 
     30720*a^2*(-2 + k)*M*rh^9*\[Kappa]*\[Omega] + 264*a^2*rh^10*\[Kappa]*
      \[Omega] + 13728*a^2*(-2 + k)*rh^10*\[Kappa]*\[Omega] + 
     (13728*I)*a*(-2 + k)*m*M*rh^10*\[Kappa]*\[Omega] - 
     (8736*I)*a*(-2 + k)*m*rh^11*\[Kappa]*\[Omega] - 
     (72*I)*a^12*rh*\[Kappa]^2*\[Omega] - 288*a^11*m*rh^2*\[Kappa]^2*
      \[Omega] + (120*I)*a^10*M*rh^2*\[Kappa]^2*\[Omega] - 
     (912*I)*a^10*rh^3*\[Kappa]^2*\[Omega] - 3060*a^9*m*rh^4*\[Kappa]^2*
      \[Omega] + (1188*I)*a^8*M*rh^4*\[Kappa]^2*\[Omega] - 
     (3600*I)*a^8*rh^5*\[Kappa]^2*\[Omega] - 9408*a^7*m*rh^6*\[Kappa]^2*
      \[Omega] + (3960*I)*a^6*M*rh^6*\[Kappa]^2*\[Omega] - 
     (6624*I)*a^6*rh^7*\[Kappa]^2*\[Omega] - 11880*a^5*m*rh^8*\[Kappa]^2*
      \[Omega] + (5268*I)*a^4*M*rh^8*\[Kappa]^2*\[Omega] - 
     (5736*I)*a^4*rh^9*\[Kappa]^2*\[Omega] - 6336*a^3*m*rh^10*\[Kappa]^2*
      \[Omega] + (2376*I)*a^2*M*rh^10*\[Kappa]^2*\[Omega] - 
     (1872*I)*a^2*rh^11*\[Kappa]^2*\[Omega] - 1092*a*m*rh^12*\[Kappa]^2*
      \[Omega] + (480*I)*a^6*M^2*rh^3*(-18 + \[Lambda])*\[Omega] - 
     96*a^10*(-2 + k)*rh^2*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     384*a^8*(-2 + k)*M*rh^3*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     640*a^8*(-2 + k)*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     960*a^6*(-2 + k)*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     672*a^6*(-2 + k)*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     96*a^8*M*rh^3*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     720*a^6*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     1344*a^4*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     720*a^2*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (48*I)*a^10*rh^3*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (240*I)*a^8*rh^5*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (336*I)*a^6*rh^7*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (144*I)*a^4*rh^9*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (96*I)*a^10*rh*(-3 + \[Lambda])*\[Omega] - (280*I)*a^6*(-2 + k)*rh^5*
      (-3 + \[Lambda])*\[Omega] + (896*I)*a^4*(-2 + k)*M*rh^6*
      (-3 + \[Lambda])*\[Omega] - (1344*I)*a^4*(-2 + k)*rh^7*(-3 + \[Lambda])*
      \[Omega] + (1920*I)*a^2*(-2 + k)*M*rh^8*(-3 + \[Lambda])*\[Omega] - 
     (1320*I)*a^2*(-2 + k)*rh^9*(-3 + \[Lambda])*\[Omega] - 
     80*a^8*rh^4*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     504*a^6*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     864*a^4*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     440*a^2*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     (32*I)*a^10*(-2 + k)*rh*\[Lambda]*\[Omega] + (40*I)*a^10*(3 - k)*
      (-2 + k)*rh*\[Lambda]*\[Omega] - 384*a^9*m*rh^2*\[Lambda]*\[Omega] + 
     (480*I)*a^8*(-2 + k)*M*rh^2*\[Lambda]*\[Omega] - 
     (480*I)*a^8*(3 - k)*(-2 + k)*M*rh^2*\[Lambda]*\[Omega] + 
     (960*I)*a^8*rh^3*\[Lambda]*\[Omega] - (640*I)*a^8*(-2 + k)*rh^3*
      \[Lambda]*\[Omega] + (1120*I)*a^8*(3 - k)*(-2 + k)*rh^3*\[Lambda]*
      \[Omega] - (240*I)*a^8*m^2*rh^3*\[Lambda]*\[Omega] + 
     1280*a^7*m*M*rh^3*\[Lambda]*\[Omega] - (1280*I)*a^6*(-2 + k)*M^2*rh^3*
      \[Lambda]*\[Omega] + (1120*I)*a^6*(3 - k)*(-2 + k)*M^2*rh^3*\[Lambda]*
      \[Omega] - 3240*a^7*m*rh^4*\[Lambda]*\[Omega] - 
     (3540*I)*a^6*M*rh^4*\[Lambda]*\[Omega] + (3920*I)*a^6*(-2 + k)*M*rh^4*
      \[Lambda]*\[Omega] - (6720*I)*a^6*(3 - k)*(-2 + k)*M*rh^4*\[Lambda]*
      \[Omega] + (1848*I)*a^6*rh^5*\[Lambda]*\[Omega] - 
     (2408*I)*a^6*(-2 + k)*rh^5*\[Lambda]*\[Omega] + 
     (6048*I)*a^6*(3 - k)*(-2 + k)*rh^5*\[Lambda]*\[Omega] - 
     (1344*I)*a^6*m^2*rh^5*\[Lambda]*\[Omega] + 6216*a^5*m*M*rh^5*\[Lambda]*
      \[Omega] + (3528*I)*a^4*M^2*rh^5*\[Lambda]*\[Omega] - 
     (3584*I)*a^4*(-2 + k)*M^2*rh^5*\[Lambda]*\[Omega] + 
     (8064*I)*a^4*(3 - k)*(-2 + k)*M^2*rh^5*\[Lambda]*\[Omega] - 
     7392*a^5*m*rh^6*\[Lambda]*\[Omega] - (3696*I)*a^4*M*rh^6*\[Lambda]*
      \[Omega] + (5824*I)*a^4*(-2 + k)*M*rh^6*\[Lambda]*\[Omega] - 
     (20160*I)*a^4*(3 - k)*(-2 + k)*M*rh^6*\[Lambda]*\[Omega] + 
     (576*I)*a^4*rh^7*\[Lambda]*\[Omega] - (2496*I)*a^4*(-2 + k)*rh^7*
      \[Lambda]*\[Omega] + (10560*I)*a^4*(3 - k)*(-2 + k)*rh^7*\[Lambda]*
      \[Omega] - (2016*I)*a^4*m^2*rh^7*\[Lambda]*\[Omega] + 
     7488*a^3*m*M*rh^7*\[Lambda]*\[Omega] + (10560*I)*a^2*(3 - k)*(-2 + k)*
      M^2*rh^7*\[Lambda]*\[Omega] - 6120*a^3*m*rh^8*\[Lambda]*\[Omega] + 
     (1260*I)*a^2*M*rh^8*\[Lambda]*\[Omega] + (720*I)*a^2*(-2 + k)*M*rh^8*
      \[Lambda]*\[Omega] - (15840*I)*a^2*(3 - k)*(-2 + k)*M*rh^8*\[Lambda]*
      \[Omega] - (440*I)*a^2*rh^9*\[Lambda]*\[Omega] - 
     (440*I)*a^2*(-2 + k)*rh^9*\[Lambda]*\[Omega] + 
     (5720*I)*a^2*(3 - k)*(-2 + k)*rh^9*\[Lambda]*\[Omega] - 
     (880*I)*a^2*m^2*rh^9*\[Lambda]*\[Omega] + 2200*a*m*M*rh^9*\[Lambda]*
      \[Omega] - (1320*I)*M^2*rh^9*\[Lambda]*\[Omega] - 
     1584*a*m*rh^10*\[Lambda]*\[Omega] + (792*I)*M*rh^10*\[Lambda]*\[Omega] - 
     48*a^10*rh^2*\[Kappa]*\[Lambda]*\[Omega] - 64*a^10*(-2 + k)*rh^2*
      \[Kappa]*\[Lambda]*\[Omega] + 64*a^8*M*rh^3*\[Kappa]*\[Lambda]*
      \[Omega] + 256*a^8*(-2 + k)*M*rh^3*\[Kappa]*\[Lambda]*\[Omega] - 
     400*a^8*rh^4*\[Kappa]*\[Lambda]*\[Omega] - 1600*a^8*(-2 + k)*rh^4*
      \[Kappa]*\[Lambda]*\[Omega] + 288*a^6*M*rh^5*\[Kappa]*\[Lambda]*
      \[Omega] + 4416*a^6*(-2 + k)*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] - 
     840*a^6*rh^6*\[Kappa]*\[Lambda]*\[Omega] - 7392*a^6*(-2 + k)*rh^6*
      \[Kappa]*\[Lambda]*\[Omega] + 384*a^4*M*rh^7*\[Kappa]*\[Lambda]*
      \[Omega] + 11520*a^4*(-2 + k)*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] - 
     576*a^4*rh^8*\[Kappa]*\[Lambda]*\[Omega] - 10560*a^4*(-2 + k)*rh^8*
      \[Kappa]*\[Lambda]*\[Omega] + 160*a^2*M*rh^9*\[Kappa]*\[Lambda]*
      \[Omega] + 7040*a^2*(-2 + k)*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] - 
     88*a^2*rh^10*\[Kappa]*\[Lambda]*\[Omega] - 4576*a^2*(-2 + k)*rh^10*
      \[Kappa]*\[Lambda]*\[Omega] + (32*I)*a^10*rh^3*\[Kappa]^2*\[Lambda]*
      \[Omega] + (432*I)*a^8*rh^5*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (1392*I)*a^6*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (1616*I)*a^4*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (624*I)*a^2*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (80*I)*a^8*rh^3*\[Lambda]^2*\[Omega] - 30*a^7*m*rh^4*\[Lambda]^2*
      \[Omega] - (240*I)*a^6*M*rh^4*\[Lambda]^2*\[Omega] + 
     (504*I)*a^6*rh^5*\[Lambda]^2*\[Omega] - 168*a^5*m*rh^6*\[Lambda]^2*
      \[Omega] - (896*I)*a^4*M*rh^6*\[Lambda]^2*\[Omega] + 
     (864*I)*a^4*rh^7*\[Lambda]^2*\[Omega] - 270*a^3*m*rh^8*\[Lambda]^2*
      \[Omega] - (720*I)*a^2*M*rh^8*\[Lambda]^2*\[Omega] + 
     (440*I)*a^2*rh^9*\[Lambda]^2*\[Omega] - 132*a*m*rh^10*\[Lambda]^2*
      \[Omega] - (48*I)*a^8*M*rh^2*(-51 + 11*\[Lambda])*\[Omega] + 
     (24*I)*a^10*M*rh^2*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (432*I)*a^8*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (1080*I)*a^6*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (672*I)*a^4*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     96*a^8*(-2 + k)*M^2*rh^2*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     384*a^8*(-2 + k)*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] + 
     960*a^6*(-2 + k)*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     960*a^6*(-2 + k)*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     96*a^8*(-2 + k)*M*rh^3*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     480*a^6*(-2 + k)*M^2*rh^4*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     960*a^6*(-2 + k)*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     1680*a^4*(-2 + k)*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     1344*a^4*(-2 + k)*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     36*a^12*\[Omega]^2 - 48*a^12*(-2 + k)*\[Omega]^2 + 
     24*a^12*(3 - k)*(-2 + k)*\[Omega]^2 + (216*I)*a^11*m*rh*\[Omega]^2 - 
     72*a^10*M*rh*\[Omega]^2 + 960*a^10*(-2 + k)*M*rh*\[Omega]^2 - 
     480*a^10*(3 - k)*(-2 + k)*M*rh*\[Omega]^2 - 1920*a^10*(-2 + k)*rh^2*
      \[Omega]^2 + 1620*a^10*(3 - k)*(-2 + k)*rh^2*\[Omega]^2 - 
     864*a^10*m^2*rh^2*\[Omega]^2 - (1296*I)*a^9*m*M*rh^2*\[Omega]^2 - 
     576*a^8*M^2*rh^2*\[Omega]^2 - 2592*a^8*(-2 + k)*M^2*rh^2*\[Omega]^2 + 
     1440*a^8*(3 - k)*(-2 + k)*M^2*rh^2*\[Omega]^2 + 
     (2400*I)*a^9*m*rh^3*\[Omega]^2 + 11520*a^8*(-2 + k)*M*rh^3*\[Omega]^2 - 
     11760*a^8*(3 - k)*(-2 + k)*M*rh^3*\[Omega]^2 + 
     3780*a^8*rh^4*\[Omega]^2 - 10080*a^8*(-2 + k)*rh^4*\[Omega]^2 + 
     13440*a^8*(3 - k)*(-2 + k)*rh^4*\[Omega]^2 - 
     7020*a^8*m^2*rh^4*\[Omega]^2 - (7560*I)*a^7*m*M*rh^4*\[Omega]^2 + 
     4320*a^6*M^2*rh^4*\[Omega]^2 - 10800*a^6*(-2 + k)*M^2*rh^4*\[Omega]^2 + 
     16800*a^6*(3 - k)*(-2 + k)*M^2*rh^4*\[Omega]^2 + 
     (6048*I)*a^7*m*rh^5*\[Omega]^2 - 11592*a^6*M*rh^5*\[Omega]^2 + 
     25632*a^6*(-2 + k)*M*rh^5*\[Omega]^2 - 54432*a^6*(3 - k)*(-2 + k)*M*rh^5*
      \[Omega]^2 + 5376*a^6*rh^6*\[Omega]^2 - 16128*a^6*(-2 + k)*rh^6*
      \[Omega]^2 + 35280*a^6*(3 - k)*(-2 + k)*rh^6*\[Omega]^2 - 
     15456*a^6*m^2*rh^6*\[Omega]^2 - (8736*I)*a^5*m*M*rh^6*\[Omega]^2 + 
     8064*a^4*M^2*rh^6*\[Omega]^2 - 4704*a^4*(-2 + k)*M^2*rh^6*\[Omega]^2 + 
     40320*a^4*(3 - k)*(-2 + k)*M^2*rh^6*\[Omega]^2 + 
     (5184*I)*a^5*m*rh^7*\[Omega]^2 - 10368*a^4*M*rh^7*\[Omega]^2 + 
     13056*a^4*(-2 + k)*M*rh^7*\[Omega]^2 - 79200*a^4*(3 - k)*(-2 + k)*M*rh^7*
      \[Omega]^2 + 3240*a^4*rh^8*\[Omega]^2 - 7920*a^4*(-2 + k)*rh^8*
      \[Omega]^2 + 35640*a^4*(3 - k)*(-2 + k)*rh^8*\[Omega]^2 - 
     12420*a^4*m^2*rh^8*\[Omega]^2 + (1080*I)*a^3*m*M*rh^8*\[Omega]^2 + 
     2160*a^2*M^2*rh^8*\[Omega]^2 + 3888*a^2*(-2 + k)*M^2*rh^8*\[Omega]^2 + 
     23760*a^2*(3 - k)*(-2 + k)*M^2*rh^8*\[Omega]^2 + 
     (1320*I)*a^3*m*rh^9*\[Omega]^2 - 2640*a^2*M*rh^9*\[Omega]^2 - 
     2400*a^2*(-2 + k)*M*rh^9*\[Omega]^2 - 34320*a^2*(3 - k)*(-2 + k)*M*rh^9*
      \[Omega]^2 + 792*a^2*rh^10*\[Omega]^2 + 12012*a^2*(3 - k)*(-2 + k)*
      rh^10*\[Omega]^2 - 3168*a^2*m^2*rh^10*\[Omega]^2 + 
     (3168*I)*a*m*M*rh^10*\[Omega]^2 + (144*I)*a^12*rh*\[Kappa]*\[Omega]^2 + 
     (192*I)*a^12*(-2 + k)*rh*\[Kappa]*\[Omega]^2 - 
     (480*I)*a^10*M*rh^2*\[Kappa]*\[Omega]^2 - (960*I)*a^10*(-2 + k)*M*rh^2*
      \[Kappa]*\[Omega]^2 + (1920*I)*a^10*rh^3*\[Kappa]*\[Omega]^2 + 
     (4320*I)*a^10*(-2 + k)*rh^3*\[Kappa]*\[Omega]^2 - 
     (2592*I)*a^8*M*rh^4*\[Kappa]*\[Omega]^2 - (11760*I)*a^8*(-2 + k)*M*rh^4*
      \[Kappa]*\[Omega]^2 + (6048*I)*a^8*rh^5*\[Kappa]*\[Omega]^2 + 
     (21504*I)*a^8*(-2 + k)*rh^5*\[Kappa]*\[Omega]^2 - 
     (3744*I)*a^6*M*rh^6*\[Kappa]*\[Omega]^2 - (36288*I)*a^6*(-2 + k)*M*rh^6*
      \[Kappa]*\[Omega]^2 + (6912*I)*a^6*rh^7*\[Kappa]*\[Omega]^2 + 
     (40320*I)*a^6*(-2 + k)*rh^7*\[Kappa]*\[Omega]^2 - 
     (1632*I)*a^4*M*rh^8*\[Kappa]*\[Omega]^2 - (39600*I)*a^4*(-2 + k)*M*rh^8*
      \[Kappa]*\[Omega]^2 + (2640*I)*a^4*rh^9*\[Kappa]*\[Omega]^2 + 
     (31680*I)*a^4*(-2 + k)*rh^9*\[Kappa]*\[Omega]^2 - 
     (13728*I)*a^2*(-2 + k)*M*rh^10*\[Kappa]*\[Omega]^2 + 
     (8736*I)*a^2*(-2 + k)*rh^11*\[Kappa]*\[Omega]^2 + 
     144*a^12*rh^2*\[Kappa]^2*\[Omega]^2 + 1620*a^10*rh^4*\[Kappa]^2*
      \[Omega]^2 + 5376*a^8*rh^6*\[Kappa]^2*\[Omega]^2 + 
     7560*a^6*rh^8*\[Kappa]^2*\[Omega]^2 + 4752*a^4*rh^10*\[Kappa]^2*
      \[Omega]^2 + 1092*a^2*rh^12*\[Kappa]^2*\[Omega]^2 - 
     24*a^10*rh^2*(-39 - 8*\[Lambda])*\[Omega]^2 + 
     (240*I)*a^9*m*rh^3*\[Lambda]*\[Omega]^2 + 1770*a^8*rh^4*\[Lambda]*
      \[Omega]^2 + (1680*I)*a^7*m*rh^5*\[Lambda]*\[Omega]^2 - 
     3528*a^6*M*rh^5*\[Lambda]*\[Omega]^2 + 4592*a^6*rh^6*\[Lambda]*
      \[Omega]^2 + (3456*I)*a^5*m*rh^7*\[Lambda]*\[Omega]^2 - 
     5184*a^4*M*rh^7*\[Lambda]*\[Omega]^2 + 4680*a^4*rh^8*\[Lambda]*
      \[Omega]^2 + (2640*I)*a^3*m*rh^9*\[Lambda]*\[Omega]^2 - 
     2200*a^2*M*rh^9*\[Lambda]*\[Omega]^2 + 1848*a^2*rh^10*\[Lambda]*
      \[Omega]^2 + (624*I)*a*m*rh^11*\[Lambda]*\[Omega]^2 + 
     182*rh^12*\[Lambda]*\[Omega]^2 + 15*a^8*rh^4*\[Lambda]^2*\[Omega]^2 + 
     112*a^6*rh^6*\[Lambda]^2*\[Omega]^2 + 270*a^4*rh^8*\[Lambda]^2*
      \[Omega]^2 + 264*a^2*rh^10*\[Lambda]^2*\[Omega]^2 + 
     91*rh^12*\[Lambda]^2*\[Omega]^2 - 160*a^8*M*rh^3*(21 + 4*\[Lambda])*
      \[Omega]^2 - (72*I)*a^12*rh*\[Omega]^3 + 576*a^11*m*rh^2*\[Omega]^3 + 
     (432*I)*a^10*M*rh^2*\[Omega]^3 + 5580*a^9*m*rh^4*\[Omega]^3 + 
     (2700*I)*a^8*M*rh^4*\[Omega]^3 - (1008*I)*a^8*rh^5*\[Omega]^3 + 
     15456*a^7*m*rh^6*\[Omega]^3 + (2688*I)*a^6*M*rh^6*\[Omega]^3 + 
     (1728*I)*a^6*rh^7*\[Omega]^3 + 17280*a^5*m*rh^8*\[Omega]^3 - 
     (3240*I)*a^4*M*rh^8*\[Omega]^3 + (3960*I)*a^4*rh^9*\[Omega]^3 + 
     7920*a^3*m*rh^10*\[Omega]^3 - (4752*I)*a^2*M*rh^10*\[Omega]^3 + 
     (1872*I)*a^2*rh^11*\[Omega]^3 + 1092*a*m*rh^12*\[Omega]^3 - 
     (1092*I)*M*rh^12*\[Omega]^3 - (672*I)*a^8*rh^5*\[Lambda]*\[Omega]^3 - 
     (1728*I)*a^6*rh^7*\[Lambda]*\[Omega]^3 - (1760*I)*a^4*rh^9*\[Lambda]*
      \[Omega]^3 - (624*I)*a^2*rh^11*\[Lambda]*\[Omega]^3 - 
     (80*I)*a^10*rh^3*(9 + \[Lambda])*\[Omega]^3 - 144*a^12*rh^2*\[Omega]^4 - 
     1620*a^10*rh^4*\[Omega]^4 - 5376*a^8*rh^6*\[Omega]^4 - 
     7560*a^6*rh^8*\[Omega]^4 - 4752*a^4*rh^10*\[Omega]^4 - 
     1092*a^2*rh^12*\[Omega]^4 + 840*a^5*(-2 + k)*m*M*rh^5*
      (2*\[Kappa] + \[Omega]) - 2688*a^3*(-2 + k)*m*M^2*rh^6*
      (2*\[Kappa] + \[Omega]) + 4032*a^3*(-2 + k)*m*M*rh^7*
      (2*\[Kappa] + \[Omega]) - 5760*a*(-2 + k)*m*M^2*rh^8*
      (2*\[Kappa] + \[Omega]) + 3960*a*(-2 + k)*m*M*rh^9*
      (2*\[Kappa] + \[Omega]) - 480*a^6*(-2 + k)*M*rh^5*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 1680*a^4*(-2 + k)*M^2*rh^6*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 2688*a^4*(-2 + k)*M*rh^7*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 4032*a^2*(-2 + k)*M^2*rh^8*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 2880*a^2*(-2 + k)*M*rh^9*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 15*a^8*(3 - k)*(-2 + k)*rh^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     140*a^6*(3 - k)*(-2 + k)*M*rh^3*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 280*a^6*(3 - k)*(-2 + k)*rh^4*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     280*a^4*(3 - k)*(-2 + k)*M^2*rh^4*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 1512*a^4*(3 - k)*(-2 + k)*M*rh^5*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     1260*a^4*(3 - k)*(-2 + k)*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1680*a^2*(3 - k)*(-2 + k)*M^2*rh^6*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     3960*a^2*(3 - k)*(-2 + k)*M*rh^7*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 1980*a^2*(3 - k)*(-2 + k)*rh^8*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     1980*(3 - k)*(-2 + k)*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 2860*(3 - k)*(-2 + k)*M*rh^9*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     1001*(3 - k)*(-2 + k)*rh^10*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 28*a^6*rh^6*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     135*a^4*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 198*a^2*rh^10*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     91*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     (112*I)*a^4*(-2 + k)*M*rh^5*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (336*I)*a^2*(-2 + k)*M^2*rh^6*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (480*I)*a^2*(-2 + k)*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + (660*I)*(-2 + k)*M^2*rh^8*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) - (440*I)*(-2 + k)*M*rh^9*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     240*a^4*(-2 + k)*rh^7*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 660*a^2*(-2 + k)*M*rh^8*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     880*a^2*(-2 + k)*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) + 1144*(-2 + k)*M*rh^10*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     728*(-2 + k)*rh^11*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]))*c[-2 + k])/(-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + 
    (144*I)*a^11*k*m*rh - (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 
    576*a^10*k*M*rh - 216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 
    648*a^10*k*rh^2 + 360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 
    144*a^10*k*m^2*rh^2 - 144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega])) + 
  (((-72*I)*a^11*(-1 + k)*m + (24*I)*a^11*(2 - k)*(-1 + k)*m - 144*a^10*M - 
     288*a^10*(-1 + k)*M + 72*a^10*(2 - k)*(-1 + k)*M + 120*a^10*rh + 
     648*a^10*(-1 + k)*rh - 240*a^10*(2 - k)*(-1 + k)*rh + 72*a^10*m^2*rh - 
     144*a^10*(-1 + k)*m^2*rh + 96*a^10*(2 - k)*(-1 + k)*m^2*rh + 
     (144*I)*a^9*m*M*rh + (1152*I)*a^9*(-1 + k)*m*M*rh - 
     (480*I)*a^9*(2 - k)*(-1 + k)*m*M*rh + 576*a^8*M^2*rh + 
     1728*a^8*(-1 + k)*M^2*rh - 576*a^8*(2 - k)*(-1 + k)*M^2*rh - 
     (72*I)*a^9*m*rh^2 - (1728*I)*a^9*(-1 + k)*m*rh^2 + 
     (960*I)*a^9*(2 - k)*(-1 + k)*m*rh^2 + (72*I)*a^9*m^3*rh^2 - 
     648*a^8*M*rh^2 - 6048*a^8*(-1 + k)*M*rh^2 + 2880*a^8*(2 - k)*(-1 + k)*M*
      rh^2 - 72*a^8*m^2*M*rh^2 + 1440*a^8*(-1 + k)*m^2*M*rh^2 - 
     960*a^8*(2 - k)*(-1 + k)*m^2*M*rh^2 - (432*I)*a^7*m*M^2*rh^2 - 
     (3744*I)*a^7*(-1 + k)*m*M^2*rh^2 + (1920*I)*a^7*(2 - k)*(-1 + k)*m*M^2*
      rh^2 - 576*a^6*M^3*rh^2 - 2304*a^6*(-1 + k)*M^3*rh^2 + 
     960*a^6*(2 - k)*(-1 + k)*M^3*rh^2 + 144*a^8*rh^3 + 
     3840*a^8*(-1 + k)*rh^3 - 2400*a^8*(2 - k)*(-1 + k)*rh^3 + 
     528*a^8*m^2*rh^3 - 1920*a^8*(-1 + k)*m^2*rh^3 + 
     1920*a^8*(2 - k)*(-1 + k)*m^2*rh^3 - 96*a^8*m^4*rh^3 + 
     (11040*I)*a^7*(-1 + k)*m*M*rh^3 - (7680*I)*a^7*(2 - k)*(-1 + k)*m*M*
      rh^3 - (288*I)*a^7*m^3*M*rh^3 + 576*a^6*M^2*rh^3 + 
     14400*a^6*(-1 + k)*M^2*rh^3 - 8640*a^6*(2 - k)*(-1 + k)*M^2*rh^3 - 
     384*a^6*m^2*M^2*rh^3 - 2880*a^6*(-1 + k)*m^2*M^2*rh^3 + 
     1920*a^6*(2 - k)*(-1 + k)*m^2*M^2*rh^3 + (2880*I)*a^5*(-1 + k)*m*M^3*
      rh^3 - (1920*I)*a^5*(2 - k)*(-1 + k)*m*M^3*rh^3 - (480*I)*a^7*m*rh^4 - 
     (6480*I)*a^7*(-1 + k)*m*rh^4 + (5040*I)*a^7*(2 - k)*(-1 + k)*m*rh^4 + 
     (480*I)*a^7*m^3*rh^4 - 19440*a^6*(-1 + k)*M*rh^4 + 
     15120*a^6*(2 - k)*(-1 + k)*M*rh^4 - 1200*a^6*m^2*M*rh^4 + 
     9360*a^6*(-1 + k)*m^2*M*rh^4 - 10080*a^6*(2 - k)*(-1 + k)*m^2*M*rh^4 + 
     (2160*I)*a^5*m*M^2*rh^4 - (18000*I)*a^5*(-1 + k)*m*M^2*rh^4 + 
     (16800*I)*a^5*(2 - k)*(-1 + k)*m*M^2*rh^4 + 480*a^4*M^3*rh^4 - 
     8640*a^4*(-1 + k)*M^3*rh^4 + 6720*a^4*(2 - k)*(-1 + k)*M^3*rh^4 - 
     72*a^6*rh^5 + 7056*a^6*(-1 + k)*rh^5 - 6720*a^6*(2 - k)*(-1 + k)*rh^5 + 
     936*a^6*m^2*rh^5 - 6048*a^6*(-1 + k)*m^2*rh^5 + 
     8064*a^6*(2 - k)*(-1 + k)*m^2*rh^5 - 288*a^6*m^4*rh^5 - 
     (144*I)*a^5*m*M*rh^5 + (23184*I)*a^5*(-1 + k)*m*M*rh^5 - 
     (24192*I)*a^5*(2 - k)*(-1 + k)*m*M*rh^5 - (864*I)*a^5*m^3*M*rh^5 - 
     864*a^4*M^2*rh^5 + 24192*a^4*(-1 + k)*M^2*rh^5 - 
     24192*a^4*(2 - k)*(-1 + k)*M^2*rh^5 + 1152*a^4*m^2*M^2*rh^5 - 
     8064*a^4*(-1 + k)*m^2*M^2*rh^5 + 10752*a^4*(2 - k)*(-1 + k)*m^2*M^2*
      rh^5 - (2304*I)*a^3*m*M^3*rh^5 + (8064*I)*a^3*(-1 + k)*m*M^3*rh^5 - 
     (10752*I)*a^3*(2 - k)*(-1 + k)*m*M^3*rh^5 - (840*I)*a^5*m*rh^6 - 
     (8064*I)*a^5*(-1 + k)*m*rh^6 + (8064*I)*a^5*(2 - k)*(-1 + k)*m*rh^6 + 
     (840*I)*a^5*m^3*rh^6 + 504*a^4*M*rh^6 - 20160*a^4*(-1 + k)*M*rh^6 + 
     24192*a^4*(2 - k)*(-1 + k)*M*rh^6 - 1512*a^4*m^2*M*rh^6 + 
     14784*a^4*(-1 + k)*m^2*M*rh^6 - 24192*a^4*(2 - k)*(-1 + k)*m^2*M*rh^6 + 
     (2352*I)*a^3*m*M^2*rh^6 - (20160*I)*a^3*(-1 + k)*m*M^2*rh^6 + 
     (32256*I)*a^3*(2 - k)*(-1 + k)*m*M^2*rh^6 - 5376*a^2*(-1 + k)*M^3*rh^6 + 
     8064*a^2*(2 - k)*(-1 + k)*M^3*rh^6 - 96*a^4*rh^7 + 
     5184*a^4*(-1 + k)*rh^7 - 7200*a^4*(2 - k)*(-1 + k)*rh^7 + 
     480*a^4*m^2*rh^7 - 6912*a^4*(-1 + k)*m^2*rh^7 + 
     11520*a^4*(2 - k)*(-1 + k)*m^2*rh^7 - 192*a^4*m^4*rh^7 + 
     (384*I)*a^3*m*M*rh^7 + (15552*I)*a^3*(-1 + k)*m*M*rh^7 - 
     (23040*I)*a^3*(2 - k)*(-1 + k)*m*M*rh^7 - (576*I)*a^3*m^3*M*rh^7 + 
     10368*a^2*(-1 + k)*M^2*rh^7 - 17280*a^2*(2 - k)*(-1 + k)*M^2*rh^7 - 
     3456*a^2*(-1 + k)*m^2*M^2*rh^7 + 11520*a^2*(2 - k)*(-1 + k)*m^2*M^2*
      rh^7 - (768*I)*a*m*M^3*rh^7 + (3456*I)*a*(-1 + k)*m*M^3*rh^7 - 
     (11520*I)*a*(2 - k)*(-1 + k)*m*M^3*rh^7 - (432*I)*a^3*m*rh^8 - 
     (3240*I)*a^3*(-1 + k)*m*rh^8 + (3960*I)*a^3*(2 - k)*(-1 + k)*m*rh^8 + 
     (432*I)*a^3*m^3*rh^8 - 6480*a^2*(-1 + k)*M*rh^8 + 
     11880*a^2*(2 - k)*(-1 + k)*M*rh^8 + 6480*a^2*(-1 + k)*m^2*M*rh^8 - 
     15840*a^2*(2 - k)*(-1 + k)*m^2*M*rh^8 + (432*I)*a*m*M^2*rh^8 - 
     (6480*I)*a*(-1 + k)*m*M^2*rh^8 + (15840*I)*a*(2 - k)*(-1 + k)*m*M^2*
      rh^8 + 1320*a^2*(-1 + k)*rh^9 - 2640*a^2*(2 - k)*(-1 + k)*rh^9 - 
     2640*a^2*(-1 + k)*m^2*rh^9 + 5280*a^2*(2 - k)*(-1 + k)*m^2*rh^9 + 
     (2640*I)*a*(-1 + k)*m*M*rh^9 - (5280*I)*a*(2 - k)*(-1 + k)*m*M*rh^9 - 
     (48*I)*a^12*\[Kappa] - (24*I)*a^12*(-1 + k)*\[Kappa] - 
     144*a^11*m*rh*\[Kappa] - 144*a^11*(-1 + k)*m*rh*\[Kappa] + 
     (336*I)*a^10*M*rh*\[Kappa] + (288*I)*a^10*(-1 + k)*M*rh*\[Kappa] - 
     (648*I)*a^10*rh^2*\[Kappa] - (720*I)*a^10*(-1 + k)*rh^2*\[Kappa] + 
     (144*I)*a^10*m^2*rh^2*\[Kappa] + (288*I)*a^10*(-1 + k)*m^2*rh^2*
      \[Kappa] + 576*a^9*m*M*rh^2*\[Kappa] + 720*a^9*(-1 + k)*m*M*rh^2*
      \[Kappa] - (432*I)*a^8*M^2*rh^2*\[Kappa] - (576*I)*a^8*(-1 + k)*M^2*
      rh^2*\[Kappa] - 1152*a^9*m*rh^3*\[Kappa] - 1920*a^9*(-1 + k)*m*rh^3*
      \[Kappa] + (2496*I)*a^8*M*rh^3*\[Kappa] + (3840*I)*a^8*(-1 + k)*M*rh^3*
      \[Kappa] - (384*I)*a^8*m^2*M*rh^3*\[Kappa] - (960*I)*a^8*(-1 + k)*m^2*M*
      rh^3*\[Kappa] - 384*a^7*m*M^2*rh^3*\[Kappa] - 
     384*a^7*(-1 + k)*m*M^2*rh^3*\[Kappa] - (1920*I)*a^8*rh^4*\[Kappa] - 
     (3600*I)*a^8*(-1 + k)*rh^4*\[Kappa] + (960*I)*a^8*m^2*rh^4*\[Kappa] + 
     (2880*I)*a^8*(-1 + k)*m^2*rh^4*\[Kappa] + 3120*a^7*m*M*rh^4*\[Kappa] + 
     5280*a^7*(-1 + k)*m*M*rh^4*\[Kappa] - (2160*I)*a^6*M^2*rh^4*\[Kappa] - 
     (4320*I)*a^6*(-1 + k)*M^2*rh^4*\[Kappa] - 2592*a^7*m*rh^5*\[Kappa] - 
     6048*a^7*(-1 + k)*m*rh^5*\[Kappa] + (5184*I)*a^6*M*rh^5*\[Kappa] + 
     (12096*I)*a^6*(-1 + k)*M*rh^5*\[Kappa] - (1728*I)*a^6*m^2*M*rh^5*
      \[Kappa] - (6048*I)*a^6*(-1 + k)*m^2*M*rh^5*\[Kappa] - 
     1728*a^5*m*M^2*rh^5*\[Kappa] - 1728*a^5*(-1 + k)*m*M^2*rh^5*\[Kappa] - 
     (2352*I)*a^6*rh^6*\[Kappa] - (6720*I)*a^6*(-1 + k)*rh^6*\[Kappa] + 
     (2016*I)*a^6*m^2*rh^6*\[Kappa] + (8064*I)*a^6*(-1 + k)*m^2*rh^6*
      \[Kappa] + 5040*a^5*m*M*rh^6*\[Kappa] + 10080*a^5*(-1 + k)*m*M*rh^6*
      \[Kappa] - (3024*I)*a^4*M^2*rh^6*\[Kappa] - (8064*I)*a^4*(-1 + k)*M^2*
      rh^6*\[Kappa] - 2304*a^5*m*rh^7*\[Kappa] - 6912*a^5*(-1 + k)*m*rh^7*
      \[Kappa] + (4224*I)*a^4*M*rh^7*\[Kappa] + (13824*I)*a^4*(-1 + k)*M*rh^7*
      \[Kappa] - (2304*I)*a^4*m^2*M*rh^7*\[Kappa] - 
     (10368*I)*a^4*(-1 + k)*m^2*M*rh^7*\[Kappa] - 2304*a^3*m*M^2*rh^7*
      \[Kappa] - 2304*a^3*(-1 + k)*m*M^2*rh^7*\[Kappa] - 
     (1296*I)*a^4*rh^8*\[Kappa] - (5400*I)*a^4*(-1 + k)*rh^8*\[Kappa] + 
     (1728*I)*a^4*m^2*rh^8*\[Kappa] + (8640*I)*a^4*(-1 + k)*m^2*rh^8*
      \[Kappa] + 3024*a^3*m*M*rh^8*\[Kappa] + 6048*a^3*(-1 + k)*m*M*rh^8*
      \[Kappa] - (1296*I)*a^2*M^2*rh^8*\[Kappa] - (4320*I)*a^2*(-1 + k)*M^2*
      rh^8*\[Kappa] - 720*a^3*m*rh^9*\[Kappa] - 2640*a^3*(-1 + k)*m*rh^9*
      \[Kappa] + (1200*I)*a^2*M*rh^9*\[Kappa] + (5280*I)*a^2*(-1 + k)*M*rh^9*
      \[Kappa] - (960*I)*a^2*m^2*M*rh^9*\[Kappa] - (5280*I)*a^2*(-1 + k)*m^2*
      M*rh^9*\[Kappa] - 960*a*m*M^2*rh^9*\[Kappa] - 
     960*a*(-1 + k)*m*M^2*rh^9*\[Kappa] - (264*I)*a^2*rh^10*\[Kappa] - 
     (1584*I)*a^2*(-1 + k)*rh^10*\[Kappa] + (528*I)*a^2*m^2*rh^10*\[Kappa] + 
     (3168*I)*a^2*(-1 + k)*m^2*rh^10*\[Kappa] + 528*a*m*M*rh^10*\[Kappa] + 
     528*a*(-1 + k)*m*M*rh^10*\[Kappa] - 24*a^12*rh*\[Kappa]^2 + 
     (72*I)*a^11*m*rh^2*\[Kappa]^2 + 72*a^10*M*rh^2*\[Kappa]^2 - 
     240*a^10*rh^3*\[Kappa]^2 + 96*a^10*m^2*rh^3*\[Kappa]^2 - 
     (24*I)*a^9*m*M*rh^3*\[Kappa]^2 + (480*I)*a^9*m*rh^4*\[Kappa]^2 + 
     480*a^8*M*rh^4*\[Kappa]^2 - 720*a^8*rh^5*\[Kappa]^2 + 
     576*a^8*m^2*rh^5*\[Kappa]^2 - (216*I)*a^7*m*M*rh^5*\[Kappa]^2 + 
     (1008*I)*a^7*m*rh^6*\[Kappa]^2 + 1008*a^6*M*rh^6*\[Kappa]^2 - 
     960*a^6*rh^7*\[Kappa]^2 + 1152*a^6*m^2*rh^7*\[Kappa]^2 - 
     (648*I)*a^5*m*M*rh^7*\[Kappa]^2 + (864*I)*a^5*m*rh^8*\[Kappa]^2 + 
     864*a^4*M*rh^8*\[Kappa]^2 - 600*a^4*rh^9*\[Kappa]^2 + 
     960*a^4*m^2*rh^9*\[Kappa]^2 - (744*I)*a^3*m*M*rh^9*\[Kappa]^2 + 
     (264*I)*a^3*m*rh^10*\[Kappa]^2 + 264*a^2*M*rh^10*\[Kappa]^2 - 
     144*a^2*rh^11*\[Kappa]^2 + 288*a^2*m^2*rh^11*\[Kappa]^2 - 
     (288*I)*a*m*M*rh^11*\[Kappa]^2 - (96*I)*a^9*m*rh^2*\[Lambda] + 
     (48*I)*a^9*(-1 + k)*m*rh^2*\[Lambda] - (80*I)*a^9*(2 - k)*(-1 + k)*m*
      rh^2*\[Lambda] - 64*a^8*rh^3*\[Lambda] + 128*a^8*m^2*rh^3*\[Lambda] + 
     (352*I)*a^7*m*M*rh^3*\[Lambda] - (480*I)*a^7*(-1 + k)*m*M*rh^3*
      \[Lambda] + (640*I)*a^7*(2 - k)*(-1 + k)*m*M*rh^3*\[Lambda] - 
     (600*I)*a^7*m*rh^4*\[Lambda] + (480*I)*a^7*(-1 + k)*m*rh^4*\[Lambda] - 
     (1120*I)*a^7*(2 - k)*(-1 + k)*m*rh^4*\[Lambda] + 
     (40*I)*a^7*m^3*rh^4*\[Lambda] + 240*a^6*M*rh^4*\[Lambda] - 
     60*a^6*(-1 + k)*M*rh^4*\[Lambda] - 320*a^6*m^2*M*rh^4*\[Lambda] - 
     (240*I)*a^5*m*M^2*rh^4*\[Lambda] + (960*I)*a^5*(-1 + k)*m*M^2*rh^4*
      \[Lambda] - (1120*I)*a^5*(2 - k)*(-1 + k)*m*M^2*rh^4*\[Lambda] - 
     324*a^6*rh^5*\[Lambda] + 588*a^6*m^2*rh^5*\[Lambda] + 
     (1776*I)*a^5*m*M*rh^5*\[Lambda] - (2352*I)*a^5*(-1 + k)*m*M*rh^5*
      \[Lambda] + (5376*I)*a^5*(2 - k)*(-1 + k)*m*M*rh^5*\[Lambda] - 
     192*a^4*M^2*rh^5*\[Lambda] + 168*a^4*(-1 + k)*M^2*rh^5*\[Lambda] - 
     (1120*I)*a^5*m*rh^6*\[Lambda] + (1344*I)*a^5*(-1 + k)*m*rh^6*\[Lambda] - 
     (4032*I)*a^5*(2 - k)*(-1 + k)*m*rh^6*\[Lambda] + 
     (112*I)*a^5*m^3*rh^6*\[Lambda] + 924*a^4*M*rh^6*\[Lambda] - 
     224*a^4*(-1 + k)*M*rh^6*\[Lambda] - 896*a^4*m^2*M*rh^6*\[Lambda] - 
     (1344*I)*a^3*m*M^2*rh^6*\[Lambda] + (1792*I)*a^3*(-1 + k)*m*M^2*rh^6*
      \[Lambda] - (5376*I)*a^3*(2 - k)*(-1 + k)*m*M^2*rh^6*\[Lambda] - 
     512*a^4*rh^7*\[Lambda] + 800*a^4*m^2*rh^7*\[Lambda] + 
     (2112*I)*a^3*m*M*rh^7*\[Lambda] - (2880*I)*a^3*(-1 + k)*m*M*rh^7*
      \[Lambda] + (11520*I)*a^3*(2 - k)*(-1 + k)*m*M*rh^7*\[Lambda] - 
     640*a^2*M^2*rh^7*\[Lambda] + 288*a^2*(-1 + k)*M^2*rh^7*\[Lambda] - 
     (792*I)*a^3*m*rh^8*\[Lambda] + (1440*I)*a^3*(-1 + k)*m*rh^8*\[Lambda] - 
     (5280*I)*a^3*(2 - k)*(-1 + k)*m*rh^8*\[Lambda] + 
     (72*I)*a^3*m^3*rh^8*\[Lambda] + 936*a^2*M*rh^8*\[Lambda] - 
     180*a^2*(-1 + k)*M*rh^8*\[Lambda] - 576*a^2*m^2*M*rh^8*\[Lambda] - 
     (432*I)*a*m*M^2*rh^8*\[Lambda] - (5280*I)*a*(2 - k)*(-1 + k)*m*M^2*rh^8*
      \[Lambda] - 300*a^2*rh^9*\[Lambda] + 340*a^2*m^2*rh^9*\[Lambda] + 
     (560*I)*a*m*M*rh^9*\[Lambda] - (880*I)*a*(-1 + k)*m*M*rh^9*\[Lambda] + 
     (7040*I)*a*(2 - k)*(-1 + k)*m*M*rh^9*\[Lambda] - 
     240*M^2*rh^9*\[Lambda] - (176*I)*a*m*rh^10*\[Lambda] + 
     (528*I)*a*(-1 + k)*m*rh^10*\[Lambda] - (2288*I)*a*(2 - k)*(-1 + k)*m*
      rh^10*\[Lambda] + 220*M*rh^10*\[Lambda] - 48*rh^11*\[Lambda] + 
     32*a^9*m*rh^3*\[Kappa]*\[Lambda] + 160*a^9*(-1 + k)*m*rh^3*\[Kappa]*
      \[Lambda] - (60*I)*a^8*(-1 + k)*rh^4*\[Kappa]*\[Lambda] - 
     80*a^7*m*M*rh^4*\[Kappa]*\[Lambda] - 480*a^7*(-1 + k)*m*M*rh^4*\[Kappa]*
      \[Lambda] + 192*a^7*m*rh^5*\[Kappa]*\[Lambda] + 
     1344*a^7*(-1 + k)*m*rh^5*\[Kappa]*\[Lambda] + (168*I)*a^6*(-1 + k)*M*
      rh^5*\[Kappa]*\[Lambda] - (448*I)*a^6*(-1 + k)*rh^6*\[Kappa]*
      \[Lambda] - 336*a^5*m*M*rh^6*\[Kappa]*\[Lambda] - 
     2688*a^5*(-1 + k)*m*M*rh^6*\[Kappa]*\[Lambda] + 
     384*a^5*m*rh^7*\[Kappa]*\[Lambda] + 3456*a^5*(-1 + k)*m*rh^7*\[Kappa]*
      \[Lambda] + (864*I)*a^4*(-1 + k)*M*rh^7*\[Kappa]*\[Lambda] - 
     (900*I)*a^4*(-1 + k)*rh^8*\[Kappa]*\[Lambda] - 
     432*a^3*m*M*rh^8*\[Kappa]*\[Lambda] - 4320*a^3*(-1 + k)*m*M*rh^8*
      \[Kappa]*\[Lambda] + 320*a^3*m*rh^9*\[Kappa]*\[Lambda] + 
     3520*a^3*(-1 + k)*m*rh^9*\[Kappa]*\[Lambda] + (880*I)*a^2*(-1 + k)*M*
      rh^9*\[Kappa]*\[Lambda] - (528*I)*a^2*(-1 + k)*rh^10*\[Kappa]*
      \[Lambda] - 176*a*m*M*rh^10*\[Kappa]*\[Lambda] - 
     2112*a*(-1 + k)*m*M*rh^10*\[Kappa]*\[Lambda] + 
     96*a*m*rh^11*\[Kappa]*\[Lambda] + 1248*a*(-1 + k)*m*rh^11*\[Kappa]*
      \[Lambda] - (40*I)*a^9*m*rh^4*\[Kappa]^2*\[Lambda] - 
     12*a^8*rh^5*\[Kappa]^2*\[Lambda] - (224*I)*a^7*m*rh^6*\[Kappa]^2*
      \[Lambda] - 48*a^6*rh^7*\[Kappa]^2*\[Lambda] - 
     (432*I)*a^5*m*rh^8*\[Kappa]^2*\[Lambda] - 60*a^4*rh^9*\[Kappa]^2*
      \[Lambda] - (352*I)*a^3*m*rh^10*\[Kappa]^2*\[Lambda] - 
     24*a^2*rh^11*\[Kappa]^2*\[Lambda] - (104*I)*a*m*rh^12*\[Kappa]^2*
      \[Lambda] - 32*a^8*rh^3*\[Lambda]^2 - (40*I)*a^7*m*rh^4*\[Lambda]^2 + 
     120*a^6*M*rh^4*\[Lambda]^2 - 30*a^6*(-1 + k)*M*rh^4*\[Lambda]^2 - 
     174*a^6*rh^5*\[Lambda]^2 + 6*a^6*m^2*rh^5*\[Lambda]^2 + 
     (96*I)*a^5*m*M*rh^5*\[Lambda]^2 - 96*a^4*M^2*rh^5*\[Lambda]^2 + 
     84*a^4*(-1 + k)*M^2*rh^5*\[Lambda]^2 - (168*I)*a^5*m*rh^6*\[Lambda]^2 + 
     490*a^4*M*rh^6*\[Lambda]^2 - 112*a^4*(-1 + k)*M*rh^6*\[Lambda]^2 - 
     304*a^4*rh^7*\[Lambda]^2 + 16*a^4*m^2*rh^7*\[Lambda]^2 + 
     (256*I)*a^3*m*M*rh^7*\[Lambda]^2 - 320*a^2*M^2*rh^7*\[Lambda]^2 + 
     144*a^2*(-1 + k)*M^2*rh^7*\[Lambda]^2 - (216*I)*a^3*m*rh^8*\[Lambda]^2 + 
     540*a^2*M*rh^8*\[Lambda]^2 - 90*a^2*(-1 + k)*M*rh^8*\[Lambda]^2 - 
     210*a^2*rh^9*\[Lambda]^2 + 10*a^2*m^2*rh^9*\[Lambda]^2 + 
     (160*I)*a*m*M*rh^9*\[Lambda]^2 - 120*M^2*rh^9*\[Lambda]^2 - 
     (88*I)*a*m*rh^10*\[Lambda]^2 + 154*M*rh^10*\[Lambda]^2 - 
     48*rh^11*\[Lambda]^2 - (30*I)*a^8*(-1 + k)*rh^4*\[Kappa]*\[Lambda]^2 + 
     (84*I)*a^6*(-1 + k)*M*rh^5*\[Kappa]*\[Lambda]^2 - 
     (224*I)*a^6*(-1 + k)*rh^6*\[Kappa]*\[Lambda]^2 + 
     (432*I)*a^4*(-1 + k)*M*rh^7*\[Kappa]*\[Lambda]^2 - 
     (450*I)*a^4*(-1 + k)*rh^8*\[Kappa]*\[Lambda]^2 + 
     (440*I)*a^2*(-1 + k)*M*rh^9*\[Kappa]*\[Lambda]^2 - 
     (264*I)*a^2*(-1 + k)*rh^10*\[Kappa]*\[Lambda]^2 - 
     6*a^8*rh^5*\[Kappa]^2*\[Lambda]^2 - 24*a^6*rh^7*\[Kappa]^2*\[Lambda]^2 - 
     30*a^4*rh^9*\[Kappa]^2*\[Lambda]^2 - 12*a^2*rh^11*\[Kappa]^2*
      \[Lambda]^2 - 6*a^6*rh^5*\[Lambda]^3 + 14*a^4*M*rh^6*\[Lambda]^3 - 
     24*a^4*rh^7*\[Lambda]^3 + 36*a^2*M*rh^8*\[Lambda]^3 - 
     30*a^2*rh^9*\[Lambda]^3 + 22*M*rh^10*\[Lambda]^3 - 
     12*rh^11*\[Lambda]^3 - (72*I)*a^9*m*M*rh^3*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (360*I)*a^7*m*M*rh^5*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (504*I)*a^5*m*M*rh^7*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) - (216*I)*a^3*m*M*rh^9*\[Kappa]*
      (\[Kappa] - 8*\[Omega]) + 144*a^9*(-1 + k)*m*M*rh^2*
      (\[Kappa] - 6*\[Omega]) - 576*a^7*(-1 + k)*m*M^2*rh^3*
      (\[Kappa] - 6*\[Omega]) + 960*a^7*(-1 + k)*m*M*rh^4*
      (\[Kappa] - 6*\[Omega]) - 1440*a^5*(-1 + k)*m*M^2*rh^5*
      (\[Kappa] - 6*\[Omega]) + 1008*a^5*(-1 + k)*m*M*rh^6*
      (\[Kappa] - 6*\[Omega]) + 240*a^7*(-1 + k)*m*M*rh^4*
      (4*\[Kappa] - 5*\[Omega]) - 720*a^5*(-1 + k)*m*M^2*rh^5*
      (4*\[Kappa] - 5*\[Omega]) + 1008*a^5*(-1 + k)*m*M*rh^6*
      (4*\[Kappa] - 5*\[Omega]) - 1344*a^3*(-1 + k)*m*M^2*rh^7*
      (4*\[Kappa] - 5*\[Omega]) + 864*a^3*(-1 + k)*m*M*rh^8*
      (4*\[Kappa] - 5*\[Omega]) + (72*I)*a^12*(-1 + k)*\[Omega] - 
     (24*I)*a^12*(2 - k)*(-1 + k)*\[Omega] - 144*a^11*m*rh*\[Omega] + 
     288*a^11*(-1 + k)*m*rh*\[Omega] - 192*a^11*(2 - k)*(-1 + k)*m*rh*
      \[Omega] - (144*I)*a^10*M*rh*\[Omega] - (1152*I)*a^10*(-1 + k)*M*rh*
      \[Omega] + (480*I)*a^10*(2 - k)*(-1 + k)*M*rh*\[Omega] + 
     (1872*I)*a^10*(-1 + k)*rh^2*\[Omega] - (1200*I)*a^10*(2 - k)*(-1 + k)*
      rh^2*\[Omega] - (216*I)*a^10*m^2*rh^2*\[Omega] + 
     144*a^9*m*M*rh^2*\[Omega] - 2016*a^9*(-1 + k)*m*M*rh^2*\[Omega] + 
     1920*a^9*(2 - k)*(-1 + k)*m*M*rh^2*\[Omega] + (432*I)*a^8*M^2*rh^2*
      \[Omega] + (3744*I)*a^8*(-1 + k)*M^2*rh^2*\[Omega] - 
     (1920*I)*a^8*(2 - k)*(-1 + k)*M^2*rh^2*\[Omega] - 
     1152*a^9*m*rh^3*\[Omega] + 3840*a^9*(-1 + k)*m*rh^3*\[Omega] - 
     4080*a^9*(2 - k)*(-1 + k)*m*rh^3*\[Omega] + 384*a^9*m^3*rh^3*\[Omega] - 
     (12480*I)*a^8*(-1 + k)*M*rh^3*\[Omega] + (9600*I)*a^8*(2 - k)*(-1 + k)*M*
      rh^3*\[Omega] + (864*I)*a^8*m^2*M*rh^3*\[Omega] + 
     768*a^7*m*M^2*rh^3*\[Omega] + 2304*a^7*(-1 + k)*m*M^2*rh^3*\[Omega] - 
     3840*a^7*(2 - k)*(-1 + k)*m*M^2*rh^3*\[Omega] - 
     (2880*I)*a^6*(-1 + k)*M^3*rh^3*\[Omega] + (1920*I)*a^6*(2 - k)*(-1 + k)*
      M^3*rh^3*\[Omega] - (1560*I)*a^8*rh^4*\[Omega] + 
     (7920*I)*a^8*(-1 + k)*rh^4*\[Omega] - (8400*I)*a^8*(2 - k)*(-1 + k)*rh^4*
      \[Omega] - (1320*I)*a^8*m^2*rh^4*\[Omega] + 
     2880*a^7*m*M*rh^4*\[Omega] - 12120*a^7*(-1 + k)*m*M*rh^4*\[Omega] + 
     21840*a^7*(2 - k)*(-1 + k)*m*M*rh^4*\[Omega] + 
     (21240*I)*a^6*(-1 + k)*M^2*rh^4*\[Omega] - (20160*I)*a^6*(2 - k)*
      (-1 + k)*M^2*rh^4*\[Omega] - 2520*a^7*m*rh^5*\[Omega] + 
     12096*a^7*(-1 + k)*m*rh^5*\[Omega] - 18816*a^7*(2 - k)*(-1 + k)*m*rh^5*
      \[Omega] + 1512*a^7*m^3*rh^5*\[Omega] + (7704*I)*a^6*M*rh^5*\[Omega] - 
     (30240*I)*a^6*(-1 + k)*M*rh^5*\[Omega] + (40320*I)*a^6*(2 - k)*(-1 + k)*
      M*rh^5*\[Omega] + (2808*I)*a^6*m^2*M*rh^5*\[Omega] - 
     2880*a^5*m*M^2*rh^5*\[Omega] + 4896*a^5*(-1 + k)*m*M^2*rh^5*\[Omega] - 
     24192*a^5*(2 - k)*(-1 + k)*m*M^2*rh^5*\[Omega] + 
     (3456*I)*a^4*M^3*rh^5*\[Omega] - (9072*I)*a^4*(-1 + k)*M^3*rh^5*
      \[Omega] + (10752*I)*a^4*(2 - k)*(-1 + k)*M^3*rh^5*\[Omega] - 
     (2520*I)*a^6*rh^6*\[Omega] + (11592*I)*a^6*(-1 + k)*rh^6*\[Omega] - 
     (20160*I)*a^6*(2 - k)*(-1 + k)*rh^6*\[Omega] - 
     (2520*I)*a^6*m^2*rh^6*\[Omega] + 5544*a^5*m*M*rh^6*\[Omega] - 
     19656*a^5*(-1 + k)*m*M*rh^6*\[Omega] + 60480*a^5*(2 - k)*(-1 + k)*m*M*
      rh^6*\[Omega] - (11928*I)*a^4*M^2*rh^6*\[Omega] + 
     (26880*I)*a^4*(-1 + k)*M^2*rh^6*\[Omega] - (48384*I)*a^4*(2 - k)*
      (-1 + k)*M^2*rh^6*\[Omega] - 2304*a^5*m*rh^7*\[Omega] + 
     13824*a^5*(-1 + k)*m*rh^7*\[Omega] - 31680*a^5*(2 - k)*(-1 + k)*m*rh^7*
      \[Omega] + 1728*a^5*m^3*rh^7*\[Omega] + (8640*I)*a^4*M*rh^7*\[Omega] - 
     (22848*I)*a^4*(-1 + k)*M*rh^7*\[Omega] + (57600*I)*a^4*(2 - k)*(-1 + k)*
      M*rh^7*\[Omega] + (2304*I)*a^4*m^2*M*rh^7*\[Omega] - 
     2304*a^3*m*M^2*rh^7*\[Omega] + 1536*a^3*(-1 + k)*m*M^2*rh^7*\[Omega] - 
     34560*a^3*(2 - k)*(-1 + k)*m*M^2*rh^7*\[Omega] + 
     (4608*I)*a^2*M^3*rh^7*\[Omega] - (5184*I)*a^2*(-1 + k)*M^3*rh^7*
      \[Omega] + (11520*I)*a^2*(2 - k)*(-1 + k)*M^3*rh^7*\[Omega] - 
     (1512*I)*a^4*rh^8*\[Omega] + (5832*I)*a^4*(-1 + k)*rh^8*\[Omega] - 
     (19800*I)*a^4*(2 - k)*(-1 + k)*rh^8*\[Omega] - 
     (1944*I)*a^4*m^2*rh^8*\[Omega] + 3024*a^3*m*M*rh^8*\[Omega] - 
     9288*a^3*(-1 + k)*m*M*rh^8*\[Omega] + 55440*a^3*(2 - k)*(-1 + k)*m*M*
      rh^8*\[Omega] - (7344*I)*a^2*M^2*rh^8*\[Omega] + 
     (7560*I)*a^2*(-1 + k)*M^2*rh^8*\[Omega] - (31680*I)*a^2*(2 - k)*(-1 + k)*
      M^2*rh^8*\[Omega] - 1080*a^3*m*rh^9*\[Omega] + 
     5280*a^3*(-1 + k)*m*rh^9*\[Omega] - 21120*a^3*(2 - k)*(-1 + k)*m*rh^9*
      \[Omega] + 600*a^3*m^3*rh^9*\[Omega] + (3000*I)*a^2*M*rh^9*\[Omega] - 
     (3120*I)*a^2*(-1 + k)*M*rh^9*\[Omega] + (26400*I)*a^2*(2 - k)*(-1 + k)*M*
      rh^9*\[Omega] + (360*I)*a^2*m^2*M*rh^9*\[Omega] - 
     480*a*m*M^2*rh^9*\[Omega] - 480*a*(-1 + k)*m*M^2*rh^9*\[Omega] - 
     10560*a*(2 - k)*(-1 + k)*m*M^2*rh^9*\[Omega] + 
     (1440*I)*M^3*rh^9*\[Omega] - (264*I)*a^2*rh^10*\[Omega] + 
     (264*I)*a^2*(-1 + k)*rh^10*\[Omega] - (6864*I)*a^2*(2 - k)*(-1 + k)*
      rh^10*\[Omega] - (528*I)*a^2*m^2*rh^10*\[Omega] + 
     792*a*m*M*rh^10*\[Omega] + 264*a*(-1 + k)*m*M*rh^10*\[Omega] + 
     13728*a*(2 - k)*(-1 + k)*m*M*rh^10*\[Omega] - 
     (1320*I)*M^2*rh^10*\[Omega] - 288*a*m*rh^11*\[Omega] - 
     4368*a*(2 - k)*(-1 + k)*m*rh^11*\[Omega] + (288*I)*M*rh^11*\[Omega] + 
     144*a^12*rh*\[Kappa]*\[Omega] + 144*a^12*(-1 + k)*rh*\[Kappa]*\[Omega] - 
     (288*I)*a^11*m*rh^2*\[Kappa]*\[Omega] - (576*I)*a^11*(-1 + k)*m*rh^2*
      \[Kappa]*\[Omega] - 576*a^10*M*rh^2*\[Kappa]*\[Omega] - 
     816*a^10*(-1 + k)*M*rh^2*\[Kappa]*\[Omega] + 1248*a^10*rh^3*\[Kappa]*
      \[Omega] + 1536*a^10*(-1 + k)*rh^3*\[Kappa]*\[Omega] + 
     (192*I)*a^9*m*M*rh^3*\[Kappa]*\[Omega] + (1920*I)*a^9*(-1 + k)*m*M*rh^3*
      \[Kappa]*\[Omega] + 384*a^8*M^2*rh^3*\[Kappa]*\[Omega] + 
     672*a^8*(-1 + k)*M^2*rh^3*\[Kappa]*\[Omega] - 
     (1920*I)*a^9*m*rh^4*\[Kappa]*\[Omega] - (6120*I)*a^9*(-1 + k)*m*rh^4*
      \[Kappa]*\[Omega] - 2976*a^8*M*rh^4*\[Kappa]*\[Omega] - 
     5400*a^8*(-1 + k)*M*rh^4*\[Kappa]*\[Omega] + 3048*a^8*rh^5*\[Kappa]*
      \[Omega] + 7200*a^8*(-1 + k)*rh^5*\[Kappa]*\[Omega] + 
     (576*I)*a^7*m*M*rh^5*\[Kappa]*\[Omega] + (13104*I)*a^7*(-1 + k)*m*M*rh^5*
      \[Kappa]*\[Omega] + 1728*a^6*M^2*rh^5*\[Kappa]*\[Omega] + 
     3696*a^6*(-1 + k)*M^2*rh^5*\[Kappa]*\[Omega] - 
     (4032*I)*a^7*m*rh^6*\[Kappa]*\[Omega] - (18816*I)*a^7*(-1 + k)*m*rh^6*
      \[Kappa]*\[Omega] - 4320*a^6*M*rh^6*\[Kappa]*\[Omega] - 
     17664*a^6*(-1 + k)*M*rh^6*\[Kappa]*\[Omega] + 2952*a^6*rh^7*\[Kappa]*
      \[Omega] + 15264*a^6*(-1 + k)*rh^7*\[Kappa]*\[Omega] + 
     (576*I)*a^5*m*M*rh^7*\[Kappa]*\[Omega] + (25920*I)*a^5*(-1 + k)*m*M*rh^7*
      \[Kappa]*\[Omega] + 2304*a^4*M^2*rh^7*\[Kappa]*\[Omega] + 
     6480*a^4*(-1 + k)*M^2*rh^7*\[Kappa]*\[Omega] - 
     (3456*I)*a^5*m*rh^8*\[Kappa]*\[Omega] - (23760*I)*a^5*(-1 + k)*m*rh^8*
      \[Kappa]*\[Omega] - 2016*a^4*M*rh^8*\[Kappa]*\[Omega] - 
     22584*a^4*(-1 + k)*M*rh^8*\[Kappa]*\[Omega] + 1032*a^4*rh^9*\[Kappa]*
      \[Omega] + 13200*a^4*(-1 + k)*rh^9*\[Kappa]*\[Omega] + 
     (192*I)*a^3*m*M*rh^9*\[Kappa]*\[Omega] + (18480*I)*a^3*(-1 + k)*m*M*rh^9*
      \[Kappa]*\[Omega] + 960*a^2*M^2*rh^9*\[Kappa]*\[Omega] + 
     3648*a^2*(-1 + k)*M^2*rh^9*\[Kappa]*\[Omega] - 
     (1056*I)*a^3*m*rh^10*\[Kappa]*\[Omega] - (12672*I)*a^3*(-1 + k)*m*rh^10*
      \[Kappa]*\[Omega] - 96*a^2*M*rh^10*\[Kappa]*\[Omega] - 
     8352*a^2*(-1 + k)*M*rh^10*\[Kappa]*\[Omega] + 24*a^2*rh^11*\[Kappa]*
      \[Omega] + 3744*a^2*(-1 + k)*rh^11*\[Kappa]*\[Omega] + 
     (3744*I)*a*(-1 + k)*m*M*rh^11*\[Kappa]*\[Omega] - 
     (2184*I)*a*(-1 + k)*m*rh^12*\[Kappa]*\[Omega] - 
     (72*I)*a^12*rh^2*\[Kappa]^2*\[Omega] - 192*a^11*m*rh^3*\[Kappa]^2*
      \[Omega] + (48*I)*a^10*M*rh^3*\[Kappa]^2*\[Omega] - 
     (504*I)*a^10*rh^4*\[Kappa]^2*\[Omega] - 1224*a^9*m*rh^5*\[Kappa]^2*
      \[Omega] + (360*I)*a^8*M*rh^5*\[Kappa]^2*\[Omega] - 
     (1392*I)*a^8*rh^6*\[Kappa]^2*\[Omega] - 2688*a^7*m*rh^7*\[Kappa]^2*
      \[Omega] + (1008*I)*a^6*M*rh^7*\[Kappa]^2*\[Omega] - 
     (1872*I)*a^6*rh^8*\[Kappa]^2*\[Omega] - 2640*a^5*m*rh^9*\[Kappa]^2*
      \[Omega] + (1128*I)*a^4*M*rh^9*\[Kappa]^2*\[Omega] - 
     (1224*I)*a^4*rh^10*\[Kappa]^2*\[Omega] - 1152*a^3*m*rh^11*\[Kappa]^2*
      \[Omega] + (432*I)*a^2*M*rh^11*\[Kappa]^2*\[Omega] - 
     (312*I)*a^2*rh^12*\[Kappa]^2*\[Omega] - 168*a*m*rh^13*\[Kappa]^2*
      \[Omega] + (240*I)*a^6*M^2*rh^4*(-18 + \[Lambda])*\[Omega] - 
     96*a^10*(-1 + k)*rh^3*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     256*a^8*(-1 + k)*M*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     320*a^8*(-1 + k)*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     384*a^6*(-1 + k)*M*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
     224*a^6*(-1 + k)*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
     64*a^8*M*rh^4*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     288*a^6*M*rh^6*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     384*a^4*M*rh^8*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     160*a^2*M*rh^10*\[Kappa]*(-6 + \[Lambda])*\[Omega] + 
     (16*I)*a^10*rh^4*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (48*I)*a^8*rh^6*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (48*I)*a^6*rh^8*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (16*I)*a^4*rh^10*\[Kappa]^2*(-6 + \[Lambda])*\[Omega] + 
     (96*I)*a^10*rh^2*(-3 + \[Lambda])*\[Omega] - (168*I)*a^6*(-1 + k)*rh^6*
      (-3 + \[Lambda])*\[Omega] + (448*I)*a^4*(-1 + k)*M*rh^7*
      (-3 + \[Lambda])*\[Omega] - (576*I)*a^4*(-1 + k)*rh^8*(-3 + \[Lambda])*
      \[Omega] + (720*I)*a^2*(-1 + k)*M*rh^9*(-3 + \[Lambda])*\[Omega] - 
     (440*I)*a^2*(-1 + k)*rh^10*(-3 + \[Lambda])*\[Omega] - 
     40*a^8*rh^5*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     168*a^6*rh^7*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     216*a^4*rh^9*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     88*a^2*rh^11*\[Kappa]*(-3 + \[Lambda])*\[Omega] - 
     (48*I)*a^10*(-1 + k)*rh^2*\[Lambda]*\[Omega] + 
     (80*I)*a^10*(2 - k)*(-1 + k)*rh^2*\[Lambda]*\[Omega] - 
     256*a^9*m*rh^3*\[Lambda]*\[Omega] + (480*I)*a^8*(-1 + k)*M*rh^3*
      \[Lambda]*\[Omega] - (640*I)*a^8*(2 - k)*(-1 + k)*M*rh^3*\[Lambda]*
      \[Omega] + (480*I)*a^8*rh^4*\[Lambda]*\[Omega] - 
     (480*I)*a^8*(-1 + k)*rh^4*\[Lambda]*\[Omega] + 
     (1120*I)*a^8*(2 - k)*(-1 + k)*rh^4*\[Lambda]*\[Omega] - 
     (120*I)*a^8*m^2*rh^4*\[Lambda]*\[Omega] + 640*a^7*m*M*rh^4*\[Lambda]*
      \[Omega] - (960*I)*a^6*(-1 + k)*M^2*rh^4*\[Lambda]*\[Omega] + 
     (1120*I)*a^6*(2 - k)*(-1 + k)*M^2*rh^4*\[Lambda]*\[Omega] - 
     1296*a^7*m*rh^5*\[Lambda]*\[Omega] - (1416*I)*a^6*M*rh^5*\[Lambda]*
      \[Omega] + (2352*I)*a^6*(-1 + k)*M*rh^5*\[Lambda]*\[Omega] - 
     (5376*I)*a^6*(2 - k)*(-1 + k)*M*rh^5*\[Lambda]*\[Omega] + 
     (616*I)*a^6*rh^6*\[Lambda]*\[Omega] - (1176*I)*a^6*(-1 + k)*rh^6*
      \[Lambda]*\[Omega] + (4032*I)*a^6*(2 - k)*(-1 + k)*rh^6*\[Lambda]*
      \[Omega] - (448*I)*a^6*m^2*rh^6*\[Lambda]*\[Omega] + 
     2072*a^5*m*M*rh^6*\[Lambda]*\[Omega] + (1176*I)*a^4*M^2*rh^6*\[Lambda]*
      \[Omega] - (1792*I)*a^4*(-1 + k)*M^2*rh^6*\[Lambda]*\[Omega] + 
     (5376*I)*a^4*(2 - k)*(-1 + k)*M^2*rh^6*\[Lambda]*\[Omega] - 
     2112*a^5*m*rh^7*\[Lambda]*\[Omega] - (1056*I)*a^4*M*rh^7*\[Lambda]*
      \[Omega] + (2432*I)*a^4*(-1 + k)*M*rh^7*\[Lambda]*\[Omega] - 
     (11520*I)*a^4*(2 - k)*(-1 + k)*M*rh^7*\[Lambda]*\[Omega] + 
     (144*I)*a^4*rh^8*\[Lambda]*\[Omega] - (864*I)*a^4*(-1 + k)*rh^8*
      \[Lambda]*\[Omega] + (5280*I)*a^4*(2 - k)*(-1 + k)*rh^8*\[Lambda]*
      \[Omega] - (504*I)*a^4*m^2*rh^8*\[Lambda]*\[Omega] + 
     1872*a^3*m*M*rh^8*\[Lambda]*\[Omega] + (5280*I)*a^2*(2 - k)*(-1 + k)*M^2*
      rh^8*\[Lambda]*\[Omega] - 1360*a^3*m*rh^9*\[Lambda]*\[Omega] + 
     (280*I)*a^2*M*rh^9*\[Lambda]*\[Omega] + (160*I)*a^2*(-1 + k)*M*rh^9*
      \[Lambda]*\[Omega] - (7040*I)*a^2*(2 - k)*(-1 + k)*M*rh^9*\[Lambda]*
      \[Omega] - (88*I)*a^2*rh^10*\[Lambda]*\[Omega] - 
     (88*I)*a^2*(-1 + k)*rh^10*\[Lambda]*\[Omega] + 
     (2288*I)*a^2*(2 - k)*(-1 + k)*rh^10*\[Lambda]*\[Omega] - 
     (176*I)*a^2*m^2*rh^10*\[Lambda]*\[Omega] + 440*a*m*M*rh^10*\[Lambda]*
      \[Omega] - (264*I)*M^2*rh^10*\[Lambda]*\[Omega] - 
     288*a*m*rh^11*\[Lambda]*\[Omega] + (144*I)*M*rh^11*\[Lambda]*\[Omega] - 
     32*a^10*rh^3*\[Kappa]*\[Lambda]*\[Omega] - 64*a^10*(-1 + k)*rh^3*
      \[Kappa]*\[Lambda]*\[Omega] + 16*a^8*M*rh^4*\[Kappa]*\[Lambda]*
      \[Omega] + 224*a^8*(-1 + k)*M*rh^4*\[Kappa]*\[Lambda]*\[Omega] - 
     152*a^8*rh^5*\[Kappa]*\[Lambda]*\[Omega] - 1024*a^8*(-1 + k)*rh^5*
      \[Kappa]*\[Lambda]*\[Omega] + 48*a^6*M*rh^6*\[Kappa]*\[Lambda]*
      \[Omega] + 2304*a^6*(-1 + k)*M*rh^6*\[Kappa]*\[Lambda]*\[Omega] - 
     216*a^6*rh^7*\[Kappa]*\[Lambda]*\[Omega] - 3232*a^6*(-1 + k)*rh^7*
      \[Kappa]*\[Lambda]*\[Omega] + 48*a^4*M*rh^8*\[Kappa]*\[Lambda]*
      \[Omega] + 4320*a^4*(-1 + k)*M*rh^8*\[Kappa]*\[Lambda]*\[Omega] - 
     104*a^4*rh^9*\[Kappa]*\[Lambda]*\[Omega] - 3520*a^4*(-1 + k)*rh^9*
      \[Kappa]*\[Lambda]*\[Omega] + 16*a^2*M*rh^10*\[Kappa]*\[Lambda]*
      \[Omega] + 2112*a^2*(-1 + k)*M*rh^10*\[Kappa]*\[Lambda]*\[Omega] - 
     8*a^2*rh^11*\[Kappa]*\[Lambda]*\[Omega] - 1248*a^2*(-1 + k)*rh^11*
      \[Kappa]*\[Lambda]*\[Omega] + (24*I)*a^10*rh^4*\[Kappa]^2*\[Lambda]*
      \[Omega] + (176*I)*a^8*rh^6*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (384*I)*a^6*rh^8*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (336*I)*a^4*rh^10*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (104*I)*a^2*rh^12*\[Kappa]^2*\[Lambda]*\[Omega] + 
     (40*I)*a^8*rh^4*\[Lambda]^2*\[Omega] - 12*a^7*m*rh^5*\[Lambda]^2*
      \[Omega] - (96*I)*a^6*M*rh^5*\[Lambda]^2*\[Omega] + 
     (168*I)*a^6*rh^6*\[Lambda]^2*\[Omega] - 48*a^5*m*rh^7*\[Lambda]^2*
      \[Omega] - (256*I)*a^4*M*rh^7*\[Lambda]^2*\[Omega] + 
     (216*I)*a^4*rh^8*\[Lambda]^2*\[Omega] - 60*a^3*m*rh^9*\[Lambda]^2*
      \[Omega] - (160*I)*a^2*M*rh^9*\[Lambda]^2*\[Omega] + 
     (88*I)*a^2*rh^10*\[Lambda]^2*\[Omega] - 24*a*m*rh^11*\[Lambda]^2*
      \[Omega] - (32*I)*a^8*M*rh^3*(-51 + 11*\[Lambda])*\[Omega] + 
     (48*I)*a^10*M*rh^3*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (288*I)*a^8*M*rh^5*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (432*I)*a^6*M*rh^7*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
     (192*I)*a^4*M*rh^9*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
     48*a^10*(-1 + k)*M*rh^2*(\[Kappa] - 3*\[Omega])*\[Omega] + 
     288*a^8*(-1 + k)*M^2*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     576*a^8*(-1 + k)*M*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
     960*a^6*(-1 + k)*M^2*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     720*a^6*(-1 + k)*M*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] - 
     144*a^8*(-1 + k)*M*rh^4*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     480*a^6*(-1 + k)*M^2*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     720*a^6*(-1 + k)*M*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     1008*a^4*(-1 + k)*M^2*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
     672*a^4*(-1 + k)*M*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
     72*a^12*rh*\[Omega]^2 - 144*a^12*(-1 + k)*rh*\[Omega]^2 + 
     96*a^12*(2 - k)*(-1 + k)*rh*\[Omega]^2 + (216*I)*a^11*m*rh^2*
      \[Omega]^2 - 72*a^10*M*rh^2*\[Omega]^2 + 1296*a^10*(-1 + k)*M*rh^2*
      \[Omega]^2 - 960*a^10*(2 - k)*(-1 + k)*M*rh^2*\[Omega]^2 - 
     1920*a^10*(-1 + k)*rh^3*\[Omega]^2 + 2160*a^10*(2 - k)*(-1 + k)*rh^3*
      \[Omega]^2 - 576*a^10*m^2*rh^3*\[Omega]^2 - (864*I)*a^9*m*M*rh^3*
      \[Omega]^2 - 384*a^8*M^2*rh^3*\[Omega]^2 - 2016*a^8*(-1 + k)*M^2*rh^3*
      \[Omega]^2 + 1920*a^8*(2 - k)*(-1 + k)*M^2*rh^3*\[Omega]^2 + 
     (1200*I)*a^9*m*rh^4*\[Omega]^2 + 7560*a^8*(-1 + k)*M*rh^4*\[Omega]^2 - 
     11760*a^8*(2 - k)*(-1 + k)*M*rh^4*\[Omega]^2 + 
     1512*a^8*rh^5*\[Omega]^2 - 6048*a^8*(-1 + k)*rh^5*\[Omega]^2 + 
     10752*a^8*(2 - k)*(-1 + k)*rh^5*\[Omega]^2 - 
     2808*a^8*m^2*rh^5*\[Omega]^2 - (3024*I)*a^7*m*M*rh^5*\[Omega]^2 + 
     1728*a^6*M^2*rh^5*\[Omega]^2 - 4752*a^6*(-1 + k)*M^2*rh^5*\[Omega]^2 + 
     13440*a^6*(2 - k)*(-1 + k)*M^2*rh^5*\[Omega]^2 + 
     (2016*I)*a^7*m*rh^6*\[Omega]^2 - 3864*a^6*M*rh^6*\[Omega]^2 + 
     11496*a^6*(-1 + k)*M*rh^6*\[Omega]^2 - 36288*a^6*(2 - k)*(-1 + k)*M*rh^6*
      \[Omega]^2 + 1536*a^6*rh^7*\[Omega]^2 - 6912*a^6*(-1 + k)*rh^7*
      \[Omega]^2 + 20160*a^6*(2 - k)*(-1 + k)*rh^7*\[Omega]^2 - 
     4416*a^6*m^2*rh^7*\[Omega]^2 - (2496*I)*a^5*m*M*rh^7*\[Omega]^2 + 
     2304*a^4*M^2*rh^7*\[Omega]^2 - 1440*a^4*(-1 + k)*M^2*rh^7*\[Omega]^2 + 
     23040*a^4*(2 - k)*(-1 + k)*M^2*rh^7*\[Omega]^2 + 
     (1296*I)*a^5*m*rh^8*\[Omega]^2 - 2592*a^4*M*rh^8*\[Omega]^2 + 
     4728*a^4*(-1 + k)*M*rh^8*\[Omega]^2 - 39600*a^4*(2 - k)*(-1 + k)*M*rh^8*
      \[Omega]^2 + 720*a^4*rh^9*\[Omega]^2 - 2640*a^4*(-1 + k)*rh^9*
      \[Omega]^2 + 15840*a^4*(2 - k)*(-1 + k)*rh^9*\[Omega]^2 - 
     2760*a^4*m^2*rh^9*\[Omega]^2 + (240*I)*a^3*m*M*rh^9*\[Omega]^2 + 
     480*a^2*M^2*rh^9*\[Omega]^2 + 912*a^2*(-1 + k)*M^2*rh^9*\[Omega]^2 + 
     10560*a^2*(2 - k)*(-1 + k)*M^2*rh^9*\[Omega]^2 + 
     (264*I)*a^3*m*rh^10*\[Omega]^2 - 528*a^2*M*rh^10*\[Omega]^2 - 
     504*a^2*(-1 + k)*M*rh^10*\[Omega]^2 - 13728*a^2*(2 - k)*(-1 + k)*M*rh^10*
      \[Omega]^2 + 144*a^2*rh^11*\[Omega]^2 + 4368*a^2*(2 - k)*(-1 + k)*rh^11*
      \[Omega]^2 - 576*a^2*m^2*rh^11*\[Omega]^2 + (576*I)*a*m*M*rh^11*
      \[Omega]^2 + (144*I)*a^12*rh^2*\[Kappa]*\[Omega]^2 + 
     (288*I)*a^12*(-1 + k)*rh^2*\[Kappa]*\[Omega]^2 - 
     (192*I)*a^10*M*rh^3*\[Kappa]*\[Omega]^2 - (960*I)*a^10*(-1 + k)*M*rh^3*
      \[Kappa]*\[Omega]^2 + (960*I)*a^10*rh^4*\[Kappa]*\[Omega]^2 + 
     (3240*I)*a^10*(-1 + k)*rh^4*\[Kappa]*\[Omega]^2 - 
     (576*I)*a^8*M*rh^5*\[Kappa]*\[Omega]^2 - (7056*I)*a^8*(-1 + k)*M*rh^5*
      \[Kappa]*\[Omega]^2 + (2016*I)*a^8*rh^6*\[Kappa]*\[Omega]^2 + 
     (10752*I)*a^8*(-1 + k)*rh^6*\[Kappa]*\[Omega]^2 - 
     (576*I)*a^6*M*rh^7*\[Kappa]*\[Omega]^2 - (15552*I)*a^6*(-1 + k)*M*rh^7*
      \[Kappa]*\[Omega]^2 + (1728*I)*a^6*rh^8*\[Kappa]*\[Omega]^2 + 
     (15120*I)*a^6*(-1 + k)*rh^8*\[Kappa]*\[Omega]^2 - 
     (192*I)*a^4*M*rh^9*\[Kappa]*\[Omega]^2 - (13200*I)*a^4*(-1 + k)*M*rh^9*
      \[Kappa]*\[Omega]^2 + (528*I)*a^4*rh^10*\[Kappa]*\[Omega]^2 + 
     (9504*I)*a^4*(-1 + k)*rh^10*\[Kappa]*\[Omega]^2 - 
     (3744*I)*a^2*(-1 + k)*M*rh^11*\[Kappa]*\[Omega]^2 + 
     (2184*I)*a^2*(-1 + k)*rh^12*\[Kappa]*\[Omega]^2 + 
     96*a^12*rh^3*\[Kappa]^2*\[Omega]^2 + 648*a^10*rh^5*\[Kappa]^2*
      \[Omega]^2 + 1536*a^8*rh^7*\[Kappa]^2*\[Omega]^2 + 
     1680*a^6*rh^9*\[Kappa]^2*\[Omega]^2 + 864*a^4*rh^11*\[Kappa]^2*
      \[Omega]^2 + 168*a^2*rh^13*\[Kappa]^2*\[Omega]^2 - 
     16*a^10*rh^3*(-39 - 8*\[Lambda])*\[Omega]^2 + 
     (120*I)*a^9*m*rh^4*\[Lambda]*\[Omega]^2 + 708*a^8*rh^5*\[Lambda]*
      \[Omega]^2 + (560*I)*a^7*m*rh^6*\[Lambda]*\[Omega]^2 - 
     1176*a^6*M*rh^6*\[Lambda]*\[Omega]^2 + 1312*a^6*rh^7*\[Lambda]*
      \[Omega]^2 + (864*I)*a^5*m*rh^8*\[Lambda]*\[Omega]^2 - 
     1296*a^4*M*rh^8*\[Lambda]*\[Omega]^2 + 1040*a^4*rh^9*\[Lambda]*
      \[Omega]^2 + (528*I)*a^3*m*rh^10*\[Lambda]*\[Omega]^2 - 
     440*a^2*M*rh^10*\[Lambda]*\[Omega]^2 + 336*a^2*rh^11*\[Lambda]*
      \[Omega]^2 + (104*I)*a*m*rh^12*\[Lambda]*\[Omega]^2 + 
     28*rh^13*\[Lambda]*\[Omega]^2 + 6*a^8*rh^5*\[Lambda]^2*\[Omega]^2 + 
     32*a^6*rh^7*\[Lambda]^2*\[Omega]^2 + 60*a^4*rh^9*\[Lambda]^2*
      \[Omega]^2 + 48*a^2*rh^11*\[Lambda]^2*\[Omega]^2 + 
     14*rh^13*\[Lambda]^2*\[Omega]^2 - 80*a^8*M*rh^4*(21 + 4*\[Lambda])*
      \[Omega]^2 - (72*I)*a^12*rh^2*\[Omega]^3 + 384*a^11*m*rh^3*\[Omega]^3 + 
     (288*I)*a^10*M*rh^3*\[Omega]^3 + 2232*a^9*m*rh^5*\[Omega]^3 + 
     (1080*I)*a^8*M*rh^5*\[Omega]^3 - (336*I)*a^8*rh^6*\[Omega]^3 + 
     4416*a^7*m*rh^7*\[Omega]^3 + (768*I)*a^6*M*rh^7*\[Omega]^3 + 
     (432*I)*a^6*rh^8*\[Omega]^3 + 3840*a^5*m*rh^9*\[Omega]^3 - 
     (720*I)*a^4*M*rh^9*\[Omega]^3 + (792*I)*a^4*rh^10*\[Omega]^3 + 
     1440*a^3*m*rh^11*\[Omega]^3 - (864*I)*a^2*M*rh^11*\[Omega]^3 + 
     (312*I)*a^2*rh^12*\[Omega]^3 + 168*a*m*rh^13*\[Omega]^3 - 
     (168*I)*M*rh^13*\[Omega]^3 - (224*I)*a^8*rh^6*\[Lambda]*\[Omega]^3 - 
     (432*I)*a^6*rh^8*\[Lambda]*\[Omega]^3 - (352*I)*a^4*rh^10*\[Lambda]*
      \[Omega]^3 - (104*I)*a^2*rh^12*\[Lambda]*\[Omega]^3 - 
     (40*I)*a^10*rh^4*(9 + \[Lambda])*\[Omega]^3 - 96*a^12*rh^3*\[Omega]^4 - 
     648*a^10*rh^5*\[Omega]^4 - 1536*a^8*rh^7*\[Omega]^4 - 
     1680*a^6*rh^9*\[Omega]^4 - 864*a^4*rh^11*\[Omega]^4 - 
     168*a^2*rh^13*\[Omega]^4 + 504*a^5*(-1 + k)*m*M*rh^6*
      (2*\[Kappa] + \[Omega]) - 1344*a^3*(-1 + k)*m*M^2*rh^7*
      (2*\[Kappa] + \[Omega]) + 1728*a^3*(-1 + k)*m*M*rh^8*
      (2*\[Kappa] + \[Omega]) - 2160*a*(-1 + k)*m*M^2*rh^9*
      (2*\[Kappa] + \[Omega]) + 1320*a*(-1 + k)*m*M*rh^10*
      (2*\[Kappa] + \[Omega]) - 360*a^6*(-1 + k)*M*rh^6*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 1008*a^4*(-1 + k)*M^2*rh^7*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 1344*a^4*(-1 + k)*M*rh^8*\[Omega]*
      (4*\[Kappa] + \[Omega]) + 1728*a^2*(-1 + k)*M^2*rh^9*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 1080*a^2*(-1 + k)*M*rh^10*\[Omega]*
      (4*\[Kappa] + \[Omega]) - 20*a^8*(2 - k)*(-1 + k)*rh^3*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     140*a^6*(2 - k)*(-1 + k)*M*rh^4*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 224*a^6*(2 - k)*(-1 + k)*rh^5*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     224*a^4*(2 - k)*(-1 + k)*M^2*rh^5*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 1008*a^4*(2 - k)*(-1 + k)*M*rh^6*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     720*a^4*(2 - k)*(-1 + k)*rh^7*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 960*a^2*(2 - k)*(-1 + k)*M^2*rh^7*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
     1980*a^2*(2 - k)*(-1 + k)*M*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 880*a^2*(2 - k)*(-1 + k)*rh^9*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     880*(2 - k)*(-1 + k)*M^2*rh^9*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) + 1144*(2 - k)*(-1 + k)*M*rh^10*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     364*(2 - k)*(-1 + k)*rh^11*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 8*a^6*rh^7*\[Kappa]^2*
      (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     30*a^4*rh^9*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
     36*a^2*rh^11*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - 14*rh^13*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - 
       (12*I)*M*\[Omega]) - (56*I)*a^4*(-1 + k)*M*rh^6*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
     (144*I)*a^2*(-1 + k)*M^2*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - (180*I)*a^2*(-1 + k)*M*rh^8*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]) + (220*I)*(-1 + k)*M^2*rh^9*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
     (132*I)*(-1 + k)*M*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 90*a^4*(-1 + k)*rh^8*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
     220*a^2*(-1 + k)*M*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 264*a^2*(-1 + k)*rh^10*\[Kappa]*
      ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
     312*(-1 + k)*M*rh^11*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
       12*M*\[Omega]) - 182*(-1 + k)*rh^12*\[Kappa]*((2*I)*\[Lambda] + 
       I*\[Lambda]^2 + 12*M*\[Omega]))*c[-1 + k])/
   (-24*a^12 - 48*a^12*k + 12*a^12*(1 - k)*k + (144*I)*a^11*k*m*rh - 
    (72*I)*a^11*(1 - k)*k*m*rh + 144*a^10*M*rh + 576*a^10*k*M*rh - 
    216*a^10*(1 - k)*k*M*rh - 60*a^10*rh^2 - 648*a^10*k*rh^2 + 
    360*a^10*(1 - k)*k*rh^2 - 36*a^10*m^2*rh^2 + 144*a^10*k*m^2*rh^2 - 
    144*a^10*(1 - k)*k*m^2*rh^2 - (72*I)*a^9*m*M*rh^2 - 
    (1152*I)*a^9*k*m*M*rh^2 + (720*I)*a^9*(1 - k)*k*m*M*rh^2 - 
    288*a^8*M^2*rh^2 - 1728*a^8*k*M^2*rh^2 + 864*a^8*(1 - k)*k*M^2*rh^2 + 
    (24*I)*a^9*m*rh^3 + (1152*I)*a^9*k*m*rh^3 - (960*I)*a^9*(1 - k)*k*m*
     rh^3 - (24*I)*a^9*m^3*rh^3 + 216*a^8*M*rh^3 + 4032*a^8*k*M*rh^3 - 
    2880*a^8*(1 - k)*k*M*rh^3 + 24*a^8*m^2*M*rh^3 - 960*a^8*k*m^2*M*rh^3 + 
    960*a^8*(1 - k)*k*m^2*M*rh^3 + (144*I)*a^7*m*M^2*rh^3 + 
    (2496*I)*a^7*k*m*M^2*rh^3 - (1920*I)*a^7*(1 - k)*k*m*M^2*rh^3 + 
    192*a^6*M^3*rh^3 + 1536*a^6*k*M^3*rh^3 - 960*a^6*(1 - k)*k*M^3*rh^3 - 
    36*a^8*rh^4 - 1920*a^8*k*rh^4 + 1800*a^8*(1 - k)*k*rh^4 - 
    132*a^8*m^2*rh^4 + 960*a^8*k*m^2*rh^4 - 1440*a^8*(1 - k)*k*m^2*rh^4 + 
    24*a^8*m^4*rh^4 - (5520*I)*a^7*k*m*M*rh^4 + (5760*I)*a^7*(1 - k)*k*m*M*
     rh^4 + (72*I)*a^7*m^3*M*rh^4 - 144*a^6*M^2*rh^4 - 7200*a^6*k*M^2*rh^4 + 
    6480*a^6*(1 - k)*k*M^2*rh^4 + 96*a^6*m^2*M^2*rh^4 + 
    1440*a^6*k*m^2*M^2*rh^4 - 1440*a^6*(1 - k)*k*m^2*M^2*rh^4 - 
    (1440*I)*a^5*k*m*M^3*rh^4 + (1440*I)*a^5*(1 - k)*k*m*M^3*rh^4 + 
    (96*I)*a^7*m*rh^5 + (2592*I)*a^7*k*m*rh^5 - (3024*I)*a^7*(1 - k)*k*m*
     rh^5 - (96*I)*a^7*m^3*rh^5 + 7776*a^6*k*M*rh^5 - 
    9072*a^6*(1 - k)*k*M*rh^5 + 240*a^6*m^2*M*rh^5 - 3744*a^6*k*m^2*M*rh^5 + 
    6048*a^6*(1 - k)*k*m^2*M*rh^5 - (432*I)*a^5*m*M^2*rh^5 + 
    (7200*I)*a^5*k*m*M^2*rh^5 - (10080*I)*a^5*(1 - k)*k*m*M^2*rh^5 - 
    96*a^4*M^3*rh^5 + 3456*a^4*k*M^3*rh^5 - 4032*a^4*(1 - k)*k*M^3*rh^5 + 
    12*a^6*rh^6 - 2352*a^6*k*rh^6 + 3360*a^6*(1 - k)*k*rh^6 - 
    156*a^6*m^2*rh^6 + 2016*a^6*k*m^2*rh^6 - 4032*a^6*(1 - k)*k*m^2*rh^6 + 
    48*a^6*m^4*rh^6 + (24*I)*a^5*m*M*rh^6 - (7728*I)*a^5*k*m*M*rh^6 + 
    (12096*I)*a^5*(1 - k)*k*m*M*rh^6 + (144*I)*a^5*m^3*M*rh^6 + 
    144*a^4*M^2*rh^6 - 8064*a^4*k*M^2*rh^6 + 12096*a^4*(1 - k)*k*M^2*rh^6 - 
    192*a^4*m^2*M^2*rh^6 + 2688*a^4*k*m^2*M^2*rh^6 - 
    5376*a^4*(1 - k)*k*m^2*M^2*rh^6 + (384*I)*a^3*m*M^3*rh^6 - 
    (2688*I)*a^3*k*m*M^3*rh^6 + (5376*I)*a^3*(1 - k)*k*m*M^3*rh^6 + 
    (120*I)*a^5*m*rh^7 + (2304*I)*a^5*k*m*rh^7 - (3456*I)*a^5*(1 - k)*k*m*
     rh^7 - (120*I)*a^5*m^3*rh^7 - 72*a^4*M*rh^7 + 5760*a^4*k*M*rh^7 - 
    10368*a^4*(1 - k)*k*M*rh^7 + 216*a^4*m^2*M*rh^7 - 4224*a^4*k*m^2*M*rh^7 + 
    10368*a^4*(1 - k)*k*m^2*M*rh^7 - (336*I)*a^3*m*M^2*rh^7 + 
    (5760*I)*a^3*k*m*M^2*rh^7 - (13824*I)*a^3*(1 - k)*k*m*M^2*rh^7 + 
    1536*a^2*k*M^3*rh^7 - 3456*a^2*(1 - k)*k*M^3*rh^7 + 12*a^4*rh^8 - 
    1296*a^4*k*rh^8 + 2700*a^4*(1 - k)*k*rh^8 - 60*a^4*m^2*rh^8 + 
    1728*a^4*k*m^2*rh^8 - 4320*a^4*(1 - k)*k*m^2*rh^8 + 24*a^4*m^4*rh^8 - 
    (48*I)*a^3*m*M*rh^8 - (3888*I)*a^3*k*m*M*rh^8 + 
    (8640*I)*a^3*(1 - k)*k*m*M*rh^8 + (72*I)*a^3*m^3*M*rh^8 - 
    2592*a^2*k*M^2*rh^8 + 6480*a^2*(1 - k)*k*M^2*rh^8 + 
    864*a^2*k*m^2*M^2*rh^8 - 4320*a^2*(1 - k)*k*m^2*M^2*rh^8 + 
    (96*I)*a*m*M^3*rh^8 - (864*I)*a*k*m*M^3*rh^8 + 
    (4320*I)*a*(1 - k)*k*m*M^3*rh^8 + (48*I)*a^3*m*rh^9 + 
    (720*I)*a^3*k*m*rh^9 - (1320*I)*a^3*(1 - k)*k*m*rh^9 - 
    (48*I)*a^3*m^3*rh^9 + 1440*a^2*k*M*rh^9 - 3960*a^2*(1 - k)*k*M*rh^9 - 
    1440*a^2*k*m^2*M*rh^9 + 5280*a^2*(1 - k)*k*m^2*M*rh^9 - 
    (48*I)*a*m*M^2*rh^9 + (1440*I)*a*k*m*M^2*rh^9 - 
    (5280*I)*a*(1 - k)*k*m*M^2*rh^9 - 264*a^2*k*rh^10 + 
    792*a^2*(1 - k)*k*rh^10 + 528*a^2*k*m^2*rh^10 - 
    1584*a^2*(1 - k)*k*m^2*rh^10 - (528*I)*a*k*m*M*rh^10 + 
    (1584*I)*a*(1 - k)*k*m*M*rh^10 + (48*I)*a^12*rh*\[Kappa] + 
    (48*I)*a^12*k*rh*\[Kappa] + 72*a^11*m*rh^2*\[Kappa] + 
    144*a^11*k*m*rh^2*\[Kappa] - (168*I)*a^10*M*rh^2*\[Kappa] - 
    (288*I)*a^10*k*M*rh^2*\[Kappa] + (216*I)*a^10*rh^3*\[Kappa] + 
    (480*I)*a^10*k*rh^3*\[Kappa] - (48*I)*a^10*m^2*rh^3*\[Kappa] - 
    (192*I)*a^10*k*m^2*rh^3*\[Kappa] - 192*a^9*m*M*rh^3*\[Kappa] - 
    432*a^9*k*m*M*rh^3*\[Kappa] + (144*I)*a^8*M^2*rh^3*\[Kappa] + 
    (384*I)*a^8*k*M^2*rh^3*\[Kappa] + 288*a^9*m*rh^4*\[Kappa] + 
    960*a^9*k*m*rh^4*\[Kappa] - (624*I)*a^8*M*rh^4*\[Kappa] - 
    (1920*I)*a^8*k*M*rh^4*\[Kappa] + (96*I)*a^8*m^2*M*rh^4*\[Kappa] + 
    (480*I)*a^8*k*m^2*M*rh^4*\[Kappa] + 96*a^7*m*M^2*rh^4*\[Kappa] + 
    96*a^7*k*m*M^2*rh^4*\[Kappa] + (384*I)*a^8*rh^5*\[Kappa] + 
    (1440*I)*a^8*k*rh^5*\[Kappa] - (192*I)*a^8*m^2*rh^5*\[Kappa] - 
    (1152*I)*a^8*k*m^2*rh^5*\[Kappa] - 624*a^7*m*M*rh^5*\[Kappa] - 
    1920*a^7*k*m*M*rh^5*\[Kappa] + (432*I)*a^6*M^2*rh^5*\[Kappa] + 
    (1728*I)*a^6*k*M^2*rh^5*\[Kappa] + 432*a^7*m*rh^6*\[Kappa] + 
    2016*a^7*k*m*rh^6*\[Kappa] - (864*I)*a^6*M*rh^6*\[Kappa] - 
    (4032*I)*a^6*k*M*rh^6*\[Kappa] + (288*I)*a^6*m^2*M*rh^6*\[Kappa] + 
    (2016*I)*a^6*k*m^2*M*rh^6*\[Kappa] + 288*a^5*m*M^2*rh^6*\[Kappa] + 
    288*a^5*k*m*M^2*rh^6*\[Kappa] + (336*I)*a^6*rh^7*\[Kappa] + 
    (1920*I)*a^6*k*rh^7*\[Kappa] - (288*I)*a^6*m^2*rh^7*\[Kappa] - 
    (2304*I)*a^6*k*m^2*rh^7*\[Kappa] - 720*a^5*m*M*rh^7*\[Kappa] - 
    2592*a^5*k*m*M*rh^7*\[Kappa] + (432*I)*a^4*M^2*rh^7*\[Kappa] + 
    (2304*I)*a^4*k*M^2*rh^7*\[Kappa] + 288*a^5*m*rh^8*\[Kappa] + 
    1728*a^5*k*m*rh^8*\[Kappa] - (528*I)*a^4*M*rh^8*\[Kappa] - 
    (3456*I)*a^4*k*M*rh^8*\[Kappa] + (288*I)*a^4*m^2*M*rh^8*\[Kappa] + 
    (2592*I)*a^4*k*m^2*M*rh^8*\[Kappa] + 288*a^3*m*M^2*rh^8*\[Kappa] + 
    288*a^3*k*m*M^2*rh^8*\[Kappa] + (144*I)*a^4*rh^9*\[Kappa] + 
    (1200*I)*a^4*k*rh^9*\[Kappa] - (192*I)*a^4*m^2*rh^9*\[Kappa] - 
    (1920*I)*a^4*k*m^2*rh^9*\[Kappa] - 336*a^3*m*M*rh^9*\[Kappa] - 
    1152*a^3*k*m*M*rh^9*\[Kappa] + (144*I)*a^2*M^2*rh^9*\[Kappa] + 
    (960*I)*a^2*k*M^2*rh^9*\[Kappa] + 72*a^3*m*rh^10*\[Kappa] + 
    528*a^3*k*m*rh^10*\[Kappa] - (120*I)*a^2*M*rh^10*\[Kappa] - 
    (1056*I)*a^2*k*M*rh^10*\[Kappa] + (96*I)*a^2*m^2*M*rh^10*\[Kappa] + 
    (1056*I)*a^2*k*m^2*M*rh^10*\[Kappa] + 96*a*m*M^2*rh^10*\[Kappa] + 
    96*a*k*m*M^2*rh^10*\[Kappa] + (24*I)*a^2*rh^11*\[Kappa] + 
    (288*I)*a^2*k*rh^11*\[Kappa] - (48*I)*a^2*m^2*rh^11*\[Kappa] - 
    (576*I)*a^2*k*m^2*rh^11*\[Kappa] - 48*a*m*M*rh^11*\[Kappa] - 
    48*a*k*m*M*rh^11*\[Kappa] + 12*a^12*rh^2*\[Kappa]^2 - 
    (24*I)*a^11*m*rh^3*\[Kappa]^2 - 24*a^10*M*rh^3*\[Kappa]^2 + 
    60*a^10*rh^4*\[Kappa]^2 - 24*a^10*m^2*rh^4*\[Kappa]^2 - 
    (96*I)*a^9*m*rh^5*\[Kappa]^2 - 96*a^8*M*rh^5*\[Kappa]^2 + 
    120*a^8*rh^6*\[Kappa]^2 - 96*a^8*m^2*rh^6*\[Kappa]^2 + 
    (24*I)*a^7*m*M*rh^6*\[Kappa]^2 - (144*I)*a^7*m*rh^7*\[Kappa]^2 - 
    144*a^6*M*rh^7*\[Kappa]^2 + 120*a^6*rh^8*\[Kappa]^2 - 
    144*a^6*m^2*rh^8*\[Kappa]^2 + (72*I)*a^5*m*M*rh^8*\[Kappa]^2 - 
    (96*I)*a^5*m*rh^9*\[Kappa]^2 - 96*a^4*M*rh^9*\[Kappa]^2 + 
    60*a^4*rh^10*\[Kappa]^2 - 96*a^4*m^2*rh^10*\[Kappa]^2 + 
    (72*I)*a^3*m*M*rh^10*\[Kappa]^2 - (24*I)*a^3*m*rh^11*\[Kappa]^2 - 
    24*a^2*M*rh^11*\[Kappa]^2 + 12*a^2*rh^12*\[Kappa]^2 - 
    24*a^2*m^2*rh^12*\[Kappa]^2 + (24*I)*a*m*M*rh^12*\[Kappa]^2 + 
    (32*I)*a^9*m*rh^3*\[Lambda] - (32*I)*a^9*k*m*rh^3*\[Lambda] + 
    (80*I)*a^9*(1 - k)*k*m*rh^3*\[Lambda] + 16*a^8*rh^4*\[Lambda] - 
    32*a^8*m^2*rh^4*\[Lambda] - (88*I)*a^7*m*M*rh^4*\[Lambda] + 
    (240*I)*a^7*k*m*M*rh^4*\[Lambda] - (480*I)*a^7*(1 - k)*k*m*M*rh^4*
     \[Lambda] + (120*I)*a^7*m*rh^5*\[Lambda] - (192*I)*a^7*k*m*rh^5*
     \[Lambda] + (672*I)*a^7*(1 - k)*k*m*rh^5*\[Lambda] - 
    (8*I)*a^7*m^3*rh^5*\[Lambda] - 48*a^6*M*rh^5*\[Lambda] + 
    24*a^6*k*M*rh^5*\[Lambda] + 64*a^6*m^2*M*rh^5*\[Lambda] + 
    (48*I)*a^5*m*M^2*rh^5*\[Lambda] - (384*I)*a^5*k*m*M^2*rh^5*\[Lambda] + 
    (672*I)*a^5*(1 - k)*k*m*M^2*rh^5*\[Lambda] + 54*a^6*rh^6*\[Lambda] - 
    98*a^6*m^2*rh^6*\[Lambda] - (296*I)*a^5*m*M*rh^6*\[Lambda] + 
    (784*I)*a^5*k*m*M*rh^6*\[Lambda] - (2688*I)*a^5*(1 - k)*k*m*M*rh^6*
     \[Lambda] + 32*a^4*M^2*rh^6*\[Lambda] - 56*a^4*k*M^2*rh^6*\[Lambda] + 
    (160*I)*a^5*m*rh^7*\[Lambda] - (384*I)*a^5*k*m*rh^7*\[Lambda] + 
    (1728*I)*a^5*(1 - k)*k*m*rh^7*\[Lambda] - (16*I)*a^5*m^3*rh^7*\[Lambda] - 
    132*a^4*M*rh^7*\[Lambda] + 64*a^4*k*M*rh^7*\[Lambda] + 
    128*a^4*m^2*M*rh^7*\[Lambda] + (192*I)*a^3*m*M^2*rh^7*\[Lambda] - 
    (512*I)*a^3*k*m*M^2*rh^7*\[Lambda] + (2304*I)*a^3*(1 - k)*k*m*M^2*rh^7*
     \[Lambda] + 64*a^4*rh^8*\[Lambda] - 100*a^4*m^2*rh^8*\[Lambda] - 
    (264*I)*a^3*m*M*rh^8*\[Lambda] + (720*I)*a^3*k*m*M*rh^8*\[Lambda] - 
    (4320*I)*a^3*(1 - k)*k*m*M*rh^8*\[Lambda] + 80*a^2*M^2*rh^8*\[Lambda] - 
    72*a^2*k*M^2*rh^8*\[Lambda] + (88*I)*a^3*m*rh^9*\[Lambda] - 
    (320*I)*a^3*k*m*rh^9*\[Lambda] + (1760*I)*a^3*(1 - k)*k*m*rh^9*
     \[Lambda] - (8*I)*a^3*m^3*rh^9*\[Lambda] - 104*a^2*M*rh^9*\[Lambda] + 
    40*a^2*k*M*rh^9*\[Lambda] + 64*a^2*m^2*M*rh^9*\[Lambda] + 
    (48*I)*a*m*M^2*rh^9*\[Lambda] + (1760*I)*a*(1 - k)*k*m*M^2*rh^9*
     \[Lambda] + 30*a^2*rh^10*\[Lambda] - 34*a^2*m^2*rh^10*\[Lambda] - 
    (56*I)*a*m*M*rh^10*\[Lambda] + (176*I)*a*k*m*M*rh^10*\[Lambda] - 
    (2112*I)*a*(1 - k)*k*m*M*rh^10*\[Lambda] + 24*M^2*rh^10*\[Lambda] + 
    (16*I)*a*m*rh^11*\[Lambda] - (96*I)*a*k*m*rh^11*\[Lambda] + 
    (624*I)*a*(1 - k)*k*m*rh^11*\[Lambda] - 20*M*rh^11*\[Lambda] + 
    4*rh^12*\[Lambda] - 8*a^9*m*rh^4*\[Kappa]*\[Lambda] - 
    80*a^9*k*m*rh^4*\[Kappa]*\[Lambda] + (24*I)*a^8*k*rh^5*\[Kappa]*
     \[Lambda] + 16*a^7*m*M*rh^5*\[Kappa]*\[Lambda] + 
    192*a^7*k*m*M*rh^5*\[Kappa]*\[Lambda] - 32*a^7*m*rh^6*\[Kappa]*
     \[Lambda] - 448*a^7*k*m*rh^6*\[Kappa]*\[Lambda] - 
    (56*I)*a^6*k*M*rh^6*\[Kappa]*\[Lambda] + (128*I)*a^6*k*rh^7*\[Kappa]*
     \[Lambda] + 48*a^5*m*M*rh^7*\[Kappa]*\[Lambda] + 
    768*a^5*k*m*M*rh^7*\[Kappa]*\[Lambda] - 48*a^5*m*rh^8*\[Kappa]*
     \[Lambda] - 864*a^5*k*m*rh^8*\[Kappa]*\[Lambda] - 
    (216*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda] + (200*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda] + 48*a^3*m*M*rh^9*\[Kappa]*\[Lambda] + 
    960*a^3*k*m*M*rh^9*\[Kappa]*\[Lambda] - 32*a^3*m*rh^10*\[Kappa]*
     \[Lambda] - 704*a^3*k*m*rh^10*\[Kappa]*\[Lambda] - 
    (176*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda] + (96*I)*a^2*k*rh^11*\[Kappa]*
     \[Lambda] + 16*a*m*M*rh^11*\[Kappa]*\[Lambda] + 
    384*a*k*m*M*rh^11*\[Kappa]*\[Lambda] - 8*a*m*rh^12*\[Kappa]*\[Lambda] - 
    208*a*k*m*rh^12*\[Kappa]*\[Lambda] + (8*I)*a^9*m*rh^5*\[Kappa]^2*
     \[Lambda] + 2*a^8*rh^6*\[Kappa]^2*\[Lambda] + 
    (32*I)*a^7*m*rh^7*\[Kappa]^2*\[Lambda] + 6*a^6*rh^8*\[Kappa]^2*
     \[Lambda] + (48*I)*a^5*m*rh^9*\[Kappa]^2*\[Lambda] + 
    6*a^4*rh^10*\[Kappa]^2*\[Lambda] + (32*I)*a^3*m*rh^11*\[Kappa]^2*
     \[Lambda] + 2*a^2*rh^12*\[Kappa]^2*\[Lambda] + 
    (8*I)*a*m*rh^13*\[Kappa]^2*\[Lambda] + 8*a^8*rh^4*\[Lambda]^2 + 
    (8*I)*a^7*m*rh^5*\[Lambda]^2 - 24*a^6*M*rh^5*\[Lambda]^2 + 
    12*a^6*k*M*rh^5*\[Lambda]^2 + 29*a^6*rh^6*\[Lambda]^2 - 
    a^6*m^2*rh^6*\[Lambda]^2 - (16*I)*a^5*m*M*rh^6*\[Lambda]^2 + 
    16*a^4*M^2*rh^6*\[Lambda]^2 - 28*a^4*k*M^2*rh^6*\[Lambda]^2 + 
    (24*I)*a^5*m*rh^7*\[Lambda]^2 - 70*a^4*M*rh^7*\[Lambda]^2 + 
    32*a^4*k*M*rh^7*\[Lambda]^2 + 38*a^4*rh^8*\[Lambda]^2 - 
    2*a^4*m^2*rh^8*\[Lambda]^2 - (32*I)*a^3*m*M*rh^8*\[Lambda]^2 + 
    40*a^2*M^2*rh^8*\[Lambda]^2 - 36*a^2*k*M^2*rh^8*\[Lambda]^2 + 
    (24*I)*a^3*m*rh^9*\[Lambda]^2 - 60*a^2*M*rh^9*\[Lambda]^2 + 
    20*a^2*k*M*rh^9*\[Lambda]^2 + 21*a^2*rh^10*\[Lambda]^2 - 
    a^2*m^2*rh^10*\[Lambda]^2 - (16*I)*a*m*M*rh^10*\[Lambda]^2 + 
    12*M^2*rh^10*\[Lambda]^2 + (8*I)*a*m*rh^11*\[Lambda]^2 - 
    14*M*rh^11*\[Lambda]^2 + 4*rh^12*\[Lambda]^2 + 
    (12*I)*a^8*k*rh^5*\[Kappa]*\[Lambda]^2 - (28*I)*a^6*k*M*rh^6*\[Kappa]*
     \[Lambda]^2 + (64*I)*a^6*k*rh^7*\[Kappa]*\[Lambda]^2 - 
    (108*I)*a^4*k*M*rh^8*\[Kappa]*\[Lambda]^2 + (100*I)*a^4*k*rh^9*\[Kappa]*
     \[Lambda]^2 - (88*I)*a^2*k*M*rh^10*\[Kappa]*\[Lambda]^2 + 
    (48*I)*a^2*k*rh^11*\[Kappa]*\[Lambda]^2 + a^8*rh^6*\[Kappa]^2*
     \[Lambda]^2 + 3*a^6*rh^8*\[Kappa]^2*\[Lambda]^2 + 
    3*a^4*rh^10*\[Kappa]^2*\[Lambda]^2 + a^2*rh^12*\[Kappa]^2*\[Lambda]^2 + 
    a^6*rh^6*\[Lambda]^3 - 2*a^4*M*rh^7*\[Lambda]^3 + 
    3*a^4*rh^8*\[Lambda]^3 - 4*a^2*M*rh^9*\[Lambda]^3 + 
    3*a^2*rh^10*\[Lambda]^3 - 2*M*rh^11*\[Lambda]^3 + rh^12*\[Lambda]^3 + 
    (24*I)*a^9*m*M*rh^4*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^7*m*M*rh^6*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (72*I)*a^5*m*M*rh^8*\[Kappa]*(\[Kappa] - 8*\[Omega]) + 
    (24*I)*a^3*m*M*rh^10*\[Kappa]*(\[Kappa] - 8*\[Omega]) - 
    144*a^9*k*m*M*rh^3*(\[Kappa] - 6*\[Omega]) + 384*a^7*k*m*M^2*rh^4*
     (\[Kappa] - 6*\[Omega]) - 480*a^7*k*m*M*rh^5*(\[Kappa] - 6*\[Omega]) + 
    576*a^5*k*m*M^2*rh^6*(\[Kappa] - 6*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(\[Kappa] - 6*\[Omega]) - 
    120*a^7*k*m*M*rh^5*(4*\[Kappa] - 5*\[Omega]) + 
    288*a^5*k*m*M^2*rh^6*(4*\[Kappa] - 5*\[Omega]) - 
    336*a^5*k*m*M*rh^7*(4*\[Kappa] - 5*\[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(4*\[Kappa] - 5*\[Omega]) - 
    216*a^3*k*m*M*rh^9*(4*\[Kappa] - 5*\[Omega]) - 
    (144*I)*a^12*k*rh*\[Omega] + (72*I)*a^12*(1 - k)*k*rh*\[Omega] + 
    72*a^11*m*rh^2*\[Omega] - 288*a^11*k*m*rh^2*\[Omega] + 
    288*a^11*(1 - k)*k*m*rh^2*\[Omega] + (72*I)*a^10*M*rh^2*\[Omega] + 
    (1152*I)*a^10*k*M*rh^2*\[Omega] - (720*I)*a^10*(1 - k)*k*M*rh^2*
     \[Omega] - (1248*I)*a^10*k*rh^3*\[Omega] + (1200*I)*a^10*(1 - k)*k*rh^3*
     \[Omega] + (72*I)*a^10*m^2*rh^3*\[Omega] - 48*a^9*m*M*rh^3*\[Omega] + 
    1056*a^9*k*m*M*rh^3*\[Omega] - 1920*a^9*(1 - k)*k*m*M*rh^3*\[Omega] - 
    (144*I)*a^8*M^2*rh^3*\[Omega] - (2496*I)*a^8*k*M^2*rh^3*\[Omega] + 
    (1920*I)*a^8*(1 - k)*k*M^2*rh^3*\[Omega] + 288*a^9*m*rh^4*\[Omega] - 
    1920*a^9*k*m*rh^4*\[Omega] + 3060*a^9*(1 - k)*k*m*rh^4*\[Omega] - 
    96*a^9*m^3*rh^4*\[Omega] + (6240*I)*a^8*k*M*rh^4*\[Omega] - 
    (7200*I)*a^8*(1 - k)*k*M*rh^4*\[Omega] - (216*I)*a^8*m^2*M*rh^4*
     \[Omega] - 192*a^7*m*M^2*rh^4*\[Omega] - 576*a^7*k*m*M^2*rh^4*\[Omega] + 
    2880*a^7*(1 - k)*k*m*M^2*rh^4*\[Omega] + (1440*I)*a^6*k*M^3*rh^4*
     \[Omega] - (1440*I)*a^6*(1 - k)*k*M^3*rh^4*\[Omega] + 
    (312*I)*a^8*rh^5*\[Omega] - (3168*I)*a^8*k*rh^5*\[Omega] + 
    (5040*I)*a^8*(1 - k)*k*rh^5*\[Omega] + (264*I)*a^8*m^2*rh^5*\[Omega] - 
    576*a^7*m*M*rh^5*\[Omega] + 4152*a^7*k*m*M*rh^5*\[Omega] - 
    13104*a^7*(1 - k)*k*m*M*rh^5*\[Omega] - (8496*I)*a^6*k*M^2*rh^5*
     \[Omega] + (12096*I)*a^6*(1 - k)*k*M^2*rh^5*\[Omega] + 
    420*a^7*m*rh^6*\[Omega] - 4032*a^7*k*m*rh^6*\[Omega] + 
    9408*a^7*(1 - k)*k*m*rh^6*\[Omega] - 252*a^7*m^3*rh^6*\[Omega] - 
    (1284*I)*a^6*M*rh^6*\[Omega] + (10080*I)*a^6*k*M*rh^6*\[Omega] - 
    (20160*I)*a^6*(1 - k)*k*M*rh^6*\[Omega] - (468*I)*a^6*m^2*M*rh^6*
     \[Omega] + 480*a^5*m*M^2*rh^6*\[Omega] - 816*a^5*k*m*M^2*rh^6*\[Omega] + 
    12096*a^5*(1 - k)*k*m*M^2*rh^6*\[Omega] - (576*I)*a^4*M^3*rh^6*\[Omega] + 
    (3024*I)*a^4*k*M^3*rh^6*\[Omega] - (5376*I)*a^4*(1 - k)*k*M^3*rh^6*
     \[Omega] + (360*I)*a^6*rh^7*\[Omega] - (3288*I)*a^6*k*rh^7*\[Omega] + 
    (8640*I)*a^6*(1 - k)*k*rh^7*\[Omega] + (360*I)*a^6*m^2*rh^7*\[Omega] - 
    792*a^5*m*M*rh^7*\[Omega] + 5112*a^5*k*m*M*rh^7*\[Omega] - 
    25920*a^5*(1 - k)*k*m*M*rh^7*\[Omega] + (1704*I)*a^4*M^2*rh^7*\[Omega] - 
    (7680*I)*a^4*k*M^2*rh^7*\[Omega] + (20736*I)*a^4*(1 - k)*k*M^2*rh^7*
     \[Omega] + 288*a^5*m*rh^8*\[Omega] - 3456*a^5*k*m*rh^8*\[Omega] + 
    11880*a^5*(1 - k)*k*m*rh^8*\[Omega] - 216*a^5*m^3*rh^8*\[Omega] - 
    (1080*I)*a^4*M*rh^8*\[Omega] + (5664*I)*a^4*k*M*rh^8*\[Omega] - 
    (21600*I)*a^4*(1 - k)*k*M*rh^8*\[Omega] - (288*I)*a^4*m^2*M*rh^8*
     \[Omega] + 288*a^3*m*M^2*rh^8*\[Omega] - 192*a^3*k*m*M^2*rh^8*\[Omega] + 
    12960*a^3*(1 - k)*k*m*M^2*rh^8*\[Omega] - (576*I)*a^2*M^3*rh^8*\[Omega] + 
    (1296*I)*a^2*k*M^3*rh^8*\[Omega] - (4320*I)*a^2*(1 - k)*k*M^3*rh^8*
     \[Omega] + (168*I)*a^4*rh^9*\[Omega] - (1248*I)*a^4*k*rh^9*\[Omega] + 
    (6600*I)*a^4*(1 - k)*k*rh^9*\[Omega] + (216*I)*a^4*m^2*rh^9*\[Omega] - 
    336*a^3*m*M*rh^9*\[Omega] + 1992*a^3*k*m*M*rh^9*\[Omega] - 
    18480*a^3*(1 - k)*k*m*M*rh^9*\[Omega] + (816*I)*a^2*M^2*rh^9*\[Omega] - 
    (1680*I)*a^2*k*M^2*rh^9*\[Omega] + (10560*I)*a^2*(1 - k)*k*M^2*rh^9*
     \[Omega] + 108*a^3*m*rh^10*\[Omega] - 1056*a^3*k*m*rh^10*\[Omega] + 
    6336*a^3*(1 - k)*k*m*rh^10*\[Omega] - 60*a^3*m^3*rh^10*\[Omega] - 
    (300*I)*a^2*M*rh^10*\[Omega] + (576*I)*a^2*k*M*rh^10*\[Omega] - 
    (7920*I)*a^2*(1 - k)*k*M*rh^10*\[Omega] - (36*I)*a^2*m^2*M*rh^10*
     \[Omega] + 48*a*m*M^2*rh^10*\[Omega] + 48*a*k*m*M^2*rh^10*\[Omega] + 
    3168*a*(1 - k)*k*m*M^2*rh^10*\[Omega] - (144*I)*M^3*rh^10*\[Omega] + 
    (24*I)*a^2*rh^11*\[Omega] - (24*I)*a^2*k*rh^11*\[Omega] + 
    (1872*I)*a^2*(1 - k)*k*rh^11*\[Omega] + (48*I)*a^2*m^2*rh^11*\[Omega] - 
    72*a*m*M*rh^11*\[Omega] - 24*a*k*m*M*rh^11*\[Omega] - 
    3744*a*(1 - k)*k*m*M*rh^11*\[Omega] + (120*I)*M^2*rh^11*\[Omega] + 
    24*a*m*rh^12*\[Omega] + 1092*a*(1 - k)*k*m*rh^12*\[Omega] - 
    (24*I)*M*rh^12*\[Omega] - 72*a^12*rh^2*\[Kappa]*\[Omega] - 
    144*a^12*k*rh^2*\[Kappa]*\[Omega] + (96*I)*a^11*m*rh^3*\[Kappa]*
     \[Omega] + (384*I)*a^11*k*m*rh^3*\[Kappa]*\[Omega] + 
    192*a^10*M*rh^3*\[Kappa]*\[Omega] + 480*a^10*k*M*rh^3*\[Kappa]*\[Omega] - 
    312*a^10*rh^4*\[Kappa]*\[Omega] - 912*a^10*k*rh^4*\[Kappa]*\[Omega] - 
    (960*I)*a^9*k*m*M*rh^4*\[Kappa]*\[Omega] - 96*a^8*M^2*rh^4*\[Kappa]*
     \[Omega] - 192*a^8*k*M^2*rh^4*\[Kappa]*\[Omega] + 
    (384*I)*a^9*m*rh^5*\[Kappa]*\[Omega] + (2448*I)*a^9*k*m*rh^5*\[Kappa]*
     \[Omega] + 576*a^8*M*rh^5*\[Kappa]*\[Omega] + 
    2160*a^8*k*M*rh^5*\[Kappa]*\[Omega] - 504*a^8*rh^6*\[Kappa]*\[Omega] - 
    2784*a^8*k*rh^6*\[Kappa]*\[Omega] - (4368*I)*a^7*k*m*M*rh^6*\[Kappa]*
     \[Omega] - 288*a^6*M^2*rh^6*\[Kappa]*\[Omega] - 
    672*a^6*k*M^2*rh^6*\[Kappa]*\[Omega] + (576*I)*a^7*m*rh^7*\[Kappa]*
     \[Omega] + (5376*I)*a^7*k*m*rh^7*\[Kappa]*\[Omega] + 
    576*a^6*M*rh^7*\[Kappa]*\[Omega] + 4800*a^6*k*M*rh^7*\[Kappa]*\[Omega] - 
    360*a^6*rh^8*\[Kappa]*\[Omega] - 4032*a^6*k*rh^8*\[Kappa]*\[Omega] - 
    (6480*I)*a^5*k*m*M*rh^8*\[Kappa]*\[Omega] - 288*a^4*M^2*rh^8*\[Kappa]*
     \[Omega] - 864*a^4*k*M^2*rh^8*\[Kappa]*\[Omega] + 
    (384*I)*a^5*m*rh^9*\[Kappa]*\[Omega] + (5280*I)*a^5*k*m*rh^9*\[Kappa]*
     \[Omega] + 192*a^4*M*rh^9*\[Kappa]*\[Omega] + 
    4464*a^4*k*M*rh^9*\[Kappa]*\[Omega] - 96*a^4*rh^10*\[Kappa]*\[Omega] - 
    2640*a^4*k*rh^10*\[Kappa]*\[Omega] - (3696*I)*a^3*k*m*M*rh^10*\[Kappa]*
     \[Omega] - 96*a^2*M^2*rh^10*\[Kappa]*\[Omega] - 
    384*a^2*k*M^2*rh^10*\[Kappa]*\[Omega] + (96*I)*a^3*m*rh^11*\[Kappa]*
     \[Omega] + (2304*I)*a^3*k*m*rh^11*\[Kappa]*\[Omega] + 
    1344*a^2*k*M*rh^11*\[Kappa]*\[Omega] - 624*a^2*k*rh^12*\[Kappa]*
     \[Omega] - (624*I)*a*k*m*M*rh^12*\[Kappa]*\[Omega] + 
    (336*I)*a*k*m*rh^13*\[Kappa]*\[Omega] + (24*I)*a^12*rh^3*\[Kappa]^2*
     \[Omega] + 48*a^11*m*rh^4*\[Kappa]^2*\[Omega] + 
    (120*I)*a^10*rh^5*\[Kappa]^2*\[Omega] + 204*a^9*m*rh^6*\[Kappa]^2*
     \[Omega] - (36*I)*a^8*M*rh^6*\[Kappa]^2*\[Omega] + 
    (240*I)*a^8*rh^7*\[Kappa]^2*\[Omega] + 336*a^7*m*rh^8*\[Kappa]^2*
     \[Omega] - (108*I)*a^6*M*rh^8*\[Kappa]^2*\[Omega] + 
    (240*I)*a^6*rh^9*\[Kappa]^2*\[Omega] + 264*a^5*m*rh^10*\[Kappa]^2*
     \[Omega] - (108*I)*a^4*M*rh^10*\[Kappa]^2*\[Omega] + 
    (120*I)*a^4*rh^11*\[Kappa]^2*\[Omega] + 96*a^3*m*rh^12*\[Kappa]^2*
     \[Omega] - (36*I)*a^2*M*rh^12*\[Kappa]^2*\[Omega] + 
    (24*I)*a^2*rh^13*\[Kappa]^2*\[Omega] + 12*a*m*rh^14*\[Kappa]^2*\[Omega] - 
    (48*I)*a^6*M^2*rh^5*(-18 + \[Lambda])*\[Omega] + 
    32*a^10*k*rh^4*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^8*k*M*rh^5*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    64*a^8*k*rh^6*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    64*a^6*k*M*rh^7*\[Kappa]*(-9 + \[Lambda])*\[Omega] + 
    32*a^6*k*rh^8*\[Kappa]*(-9 + \[Lambda])*\[Omega] - 
    16*a^8*M*rh^5*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^6*M*rh^7*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    48*a^4*M*rh^9*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    16*a^2*M*rh^11*\[Kappa]*(-6 + \[Lambda])*\[Omega] - 
    (32*I)*a^10*rh^3*(-3 + \[Lambda])*\[Omega] + 
    (56*I)*a^6*k*rh^7*(-3 + \[Lambda])*\[Omega] - 
    (128*I)*a^4*k*M*rh^8*(-3 + \[Lambda])*\[Omega] + 
    (144*I)*a^4*k*rh^9*(-3 + \[Lambda])*\[Omega] - 
    (160*I)*a^2*k*M*rh^10*(-3 + \[Lambda])*\[Omega] + 
    (88*I)*a^2*k*rh^11*(-3 + \[Lambda])*\[Omega] + 
    8*a^8*rh^6*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    24*a^4*rh^10*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    8*a^2*rh^12*\[Kappa]*(-3 + \[Lambda])*\[Omega] + 
    (32*I)*a^10*k*rh^3*\[Lambda]*\[Omega] - (80*I)*a^10*(1 - k)*k*rh^3*
     \[Lambda]*\[Omega] + 64*a^9*m*rh^4*\[Lambda]*\[Omega] - 
    (240*I)*a^8*k*M*rh^4*\[Lambda]*\[Omega] + (480*I)*a^8*(1 - k)*k*M*rh^4*
     \[Lambda]*\[Omega] - (96*I)*a^8*rh^5*\[Lambda]*\[Omega] + 
    (192*I)*a^8*k*rh^5*\[Lambda]*\[Omega] - (672*I)*a^8*(1 - k)*k*rh^5*
     \[Lambda]*\[Omega] + (24*I)*a^8*m^2*rh^5*\[Lambda]*\[Omega] - 
    128*a^7*m*M*rh^5*\[Lambda]*\[Omega] + (384*I)*a^6*k*M^2*rh^5*\[Lambda]*
     \[Omega] - (672*I)*a^6*(1 - k)*k*M^2*rh^5*\[Lambda]*\[Omega] + 
    216*a^7*m*rh^6*\[Lambda]*\[Omega] + (236*I)*a^6*M*rh^6*\[Lambda]*
     \[Omega] - (784*I)*a^6*k*M*rh^6*\[Lambda]*\[Omega] + 
    (2688*I)*a^6*(1 - k)*k*M*rh^6*\[Lambda]*\[Omega] - 
    (88*I)*a^6*rh^7*\[Lambda]*\[Omega] + (328*I)*a^6*k*rh^7*\[Lambda]*
     \[Omega] - (1728*I)*a^6*(1 - k)*k*rh^7*\[Lambda]*\[Omega] + 
    (64*I)*a^6*m^2*rh^7*\[Lambda]*\[Omega] - 296*a^5*m*M*rh^7*\[Lambda]*
     \[Omega] - (168*I)*a^4*M^2*rh^7*\[Lambda]*\[Omega] + 
    (512*I)*a^4*k*M^2*rh^7*\[Lambda]*\[Omega] - (2304*I)*a^4*(1 - k)*k*M^2*
     rh^7*\[Lambda]*\[Omega] + 264*a^5*m*rh^8*\[Lambda]*\[Omega] + 
    (132*I)*a^4*M*rh^8*\[Lambda]*\[Omega] - (592*I)*a^4*k*M*rh^8*\[Lambda]*
     \[Omega] + (4320*I)*a^4*(1 - k)*k*M*rh^8*\[Lambda]*\[Omega] - 
    (16*I)*a^4*rh^9*\[Lambda]*\[Omega] + (176*I)*a^4*k*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^4*(1 - k)*k*rh^9*\[Lambda]*\[Omega] + 
    (56*I)*a^4*m^2*rh^9*\[Lambda]*\[Omega] - 208*a^3*m*M*rh^9*\[Lambda]*
     \[Omega] - (1760*I)*a^2*(1 - k)*k*M^2*rh^9*\[Lambda]*\[Omega] + 
    136*a^3*m*rh^10*\[Lambda]*\[Omega] - (28*I)*a^2*M*rh^10*\[Lambda]*
     \[Omega] - (16*I)*a^2*k*M*rh^10*\[Lambda]*\[Omega] + 
    (2112*I)*a^2*(1 - k)*k*M*rh^10*\[Lambda]*\[Omega] + 
    (8*I)*a^2*rh^11*\[Lambda]*\[Omega] + (8*I)*a^2*k*rh^11*\[Lambda]*
     \[Omega] - (624*I)*a^2*(1 - k)*k*rh^11*\[Lambda]*\[Omega] + 
    (16*I)*a^2*m^2*rh^11*\[Lambda]*\[Omega] - 40*a*m*M*rh^11*\[Lambda]*
     \[Omega] + (24*I)*M^2*rh^11*\[Lambda]*\[Omega] + 
    24*a*m*rh^12*\[Lambda]*\[Omega] - (12*I)*M*rh^12*\[Lambda]*\[Omega] + 
    8*a^10*rh^4*\[Kappa]*\[Lambda]*\[Omega] + 48*a^10*k*rh^4*\[Kappa]*
     \[Lambda]*\[Omega] - 128*a^8*k*M*rh^5*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^8*rh^6*\[Kappa]*\[Lambda]*\[Omega] + 384*a^8*k*rh^6*\[Kappa]*
     \[Lambda]*\[Omega] - 704*a^6*k*M*rh^7*\[Kappa]*\[Lambda]*\[Omega] + 
    24*a^6*rh^8*\[Kappa]*\[Lambda]*\[Omega] + 832*a^6*k*rh^8*\[Kappa]*
     \[Lambda]*\[Omega] - 960*a^4*k*M*rh^9*\[Kappa]*\[Lambda]*\[Omega] + 
    8*a^4*rh^10*\[Kappa]*\[Lambda]*\[Omega] + 704*a^4*k*rh^10*\[Kappa]*
     \[Lambda]*\[Omega] - 384*a^2*k*M*rh^11*\[Kappa]*\[Lambda]*\[Omega] + 
    208*a^2*k*rh^12*\[Kappa]*\[Lambda]*\[Omega] - (8*I)*a^10*rh^5*\[Kappa]^2*
     \[Lambda]*\[Omega] - (32*I)*a^8*rh^7*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (48*I)*a^6*rh^9*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (32*I)*a^4*rh^11*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^2*rh^13*\[Kappa]^2*\[Lambda]*\[Omega] - 
    (8*I)*a^8*rh^5*\[Lambda]^2*\[Omega] + 2*a^7*m*rh^6*\[Lambda]^2*\[Omega] + 
    (16*I)*a^6*M*rh^6*\[Lambda]^2*\[Omega] - (24*I)*a^6*rh^7*\[Lambda]^2*
     \[Omega] + 6*a^5*m*rh^8*\[Lambda]^2*\[Omega] + 
    (32*I)*a^4*M*rh^8*\[Lambda]^2*\[Omega] - (24*I)*a^4*rh^9*\[Lambda]^2*
     \[Omega] + 6*a^3*m*rh^10*\[Lambda]^2*\[Omega] + 
    (16*I)*a^2*M*rh^10*\[Lambda]^2*\[Omega] - (8*I)*a^2*rh^11*\[Lambda]^2*
     \[Omega] + 2*a*m*rh^12*\[Lambda]^2*\[Omega] + 
    (8*I)*a^8*M*rh^4*(-51 + 11*\[Lambda])*\[Omega] - 
    (24*I)*a^10*M*rh^4*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^8*M*rh^6*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (72*I)*a^6*M*rh^8*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] - 
    (24*I)*a^4*M*rh^10*\[Kappa]*(\[Kappa] - 4*\[Omega])*\[Omega] + 
    96*a^10*k*M*rh^3*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    288*a^8*k*M^2*rh^4*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    384*a^8*k*M*rh^5*(\[Kappa] - 3*\[Omega])*\[Omega] - 
    480*a^6*k*M^2*rh^6*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(\[Kappa] - 3*\[Omega])*\[Omega] + 
    96*a^8*k*M*rh^5*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    240*a^6*k*M^2*rh^6*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    288*a^6*k*M*rh^7*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    336*a^4*k*M^2*rh^8*(5*\[Kappa] - 3*\[Omega])*\[Omega] + 
    192*a^4*k*M*rh^9*(5*\[Kappa] - 3*\[Omega])*\[Omega] - 
    36*a^12*rh^2*\[Omega]^2 + 144*a^12*k*rh^2*\[Omega]^2 - 
    144*a^12*(1 - k)*k*rh^2*\[Omega]^2 - (72*I)*a^11*m*rh^3*\[Omega]^2 + 
    24*a^10*M*rh^3*\[Omega]^2 - 672*a^10*k*M*rh^3*\[Omega]^2 + 
    960*a^10*(1 - k)*k*M*rh^3*\[Omega]^2 + 960*a^10*k*rh^4*\[Omega]^2 - 
    1620*a^10*(1 - k)*k*rh^4*\[Omega]^2 + 144*a^10*m^2*rh^4*\[Omega]^2 + 
    (216*I)*a^9*m*M*rh^4*\[Omega]^2 + 96*a^8*M^2*rh^4*\[Omega]^2 + 
    576*a^8*k*M^2*rh^4*\[Omega]^2 - 1440*a^8*(1 - k)*k*M^2*rh^4*\[Omega]^2 - 
    (240*I)*a^9*m*rh^5*\[Omega]^2 - 2448*a^8*k*M*rh^5*\[Omega]^2 + 
    7056*a^8*(1 - k)*k*M*rh^5*\[Omega]^2 - 252*a^8*rh^6*\[Omega]^2 + 
    2016*a^8*k*rh^6*\[Omega]^2 - 5376*a^8*(1 - k)*k*rh^6*\[Omega]^2 + 
    468*a^8*m^2*rh^6*\[Omega]^2 + (504*I)*a^7*m*M*rh^6*\[Omega]^2 - 
    288*a^6*M^2*rh^6*\[Omega]^2 + 864*a^6*k*M^2*rh^6*\[Omega]^2 - 
    6720*a^6*(1 - k)*k*M^2*rh^6*\[Omega]^2 - (288*I)*a^7*m*rh^7*\[Omega]^2 + 
    552*a^6*M*rh^7*\[Omega]^2 - 2832*a^6*k*M*rh^7*\[Omega]^2 + 
    15552*a^6*(1 - k)*k*M*rh^7*\[Omega]^2 - 192*a^6*rh^8*\[Omega]^2 + 
    1728*a^6*k*rh^8*\[Omega]^2 - 7560*a^6*(1 - k)*k*rh^8*\[Omega]^2 + 
    552*a^6*m^2*rh^8*\[Omega]^2 + (312*I)*a^5*m*M*rh^8*\[Omega]^2 - 
    288*a^4*M^2*rh^8*\[Omega]^2 + 192*a^4*k*M^2*rh^8*\[Omega]^2 - 
    8640*a^4*(1 - k)*k*M^2*rh^8*\[Omega]^2 - (144*I)*a^5*m*rh^9*\[Omega]^2 + 
    288*a^4*M*rh^9*\[Omega]^2 - 1008*a^4*k*M*rh^9*\[Omega]^2 + 
    13200*a^4*(1 - k)*k*M*rh^9*\[Omega]^2 - 72*a^4*rh^10*\[Omega]^2 + 
    528*a^4*k*rh^10*\[Omega]^2 - 4752*a^4*(1 - k)*k*rh^10*\[Omega]^2 + 
    276*a^4*m^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*M*rh^10*\[Omega]^2 - 
    48*a^2*M^2*rh^10*\[Omega]^2 - 96*a^2*k*M^2*rh^10*\[Omega]^2 - 
    3168*a^2*(1 - k)*k*M^2*rh^10*\[Omega]^2 - (24*I)*a^3*m*rh^11*\[Omega]^2 + 
    48*a^2*M*rh^11*\[Omega]^2 + 48*a^2*k*M*rh^11*\[Omega]^2 + 
    3744*a^2*(1 - k)*k*M*rh^11*\[Omega]^2 - 12*a^2*rh^12*\[Omega]^2 - 
    1092*a^2*(1 - k)*k*rh^12*\[Omega]^2 + 48*a^2*m^2*rh^12*\[Omega]^2 - 
    (48*I)*a*m*M*rh^12*\[Omega]^2 - (48*I)*a^12*rh^3*\[Kappa]*\[Omega]^2 - 
    (192*I)*a^12*k*rh^3*\[Kappa]*\[Omega]^2 + (480*I)*a^10*k*M*rh^4*\[Kappa]*
     \[Omega]^2 - (192*I)*a^10*rh^5*\[Kappa]*\[Omega]^2 - 
    (1296*I)*a^10*k*rh^5*\[Kappa]*\[Omega]^2 + (2352*I)*a^8*k*M*rh^6*\[Kappa]*
     \[Omega]^2 - (288*I)*a^8*rh^7*\[Kappa]*\[Omega]^2 - 
    (3072*I)*a^8*k*rh^7*\[Kappa]*\[Omega]^2 + (3888*I)*a^6*k*M*rh^8*\[Kappa]*
     \[Omega]^2 - (192*I)*a^6*rh^9*\[Kappa]*\[Omega]^2 - 
    (3360*I)*a^6*k*rh^9*\[Kappa]*\[Omega]^2 + (2640*I)*a^4*k*M*rh^10*\[Kappa]*
     \[Omega]^2 - (48*I)*a^4*rh^11*\[Kappa]*\[Omega]^2 - 
    (1728*I)*a^4*k*rh^11*\[Kappa]*\[Omega]^2 + (624*I)*a^2*k*M*rh^12*\[Kappa]*
     \[Omega]^2 - (336*I)*a^2*k*rh^13*\[Kappa]*\[Omega]^2 - 
    24*a^12*rh^4*\[Kappa]^2*\[Omega]^2 - 108*a^10*rh^6*\[Kappa]^2*
     \[Omega]^2 - 192*a^8*rh^8*\[Kappa]^2*\[Omega]^2 - 
    168*a^6*rh^10*\[Kappa]^2*\[Omega]^2 - 72*a^4*rh^12*\[Kappa]^2*
     \[Omega]^2 - 12*a^2*rh^14*\[Kappa]^2*\[Omega]^2 + 
    4*a^10*rh^4*(-39 - 8*\[Lambda])*\[Omega]^2 - (24*I)*a^9*m*rh^5*\[Lambda]*
     \[Omega]^2 - 118*a^8*rh^6*\[Lambda]*\[Omega]^2 - 
    (80*I)*a^7*m*rh^7*\[Lambda]*\[Omega]^2 + 168*a^6*M*rh^7*\[Lambda]*
     \[Omega]^2 - 164*a^6*rh^8*\[Lambda]*\[Omega]^2 - 
    (96*I)*a^5*m*rh^9*\[Lambda]*\[Omega]^2 + 144*a^4*M*rh^9*\[Lambda]*
     \[Omega]^2 - 104*a^4*rh^10*\[Lambda]*\[Omega]^2 - 
    (48*I)*a^3*m*rh^11*\[Lambda]*\[Omega]^2 + 40*a^2*M*rh^11*\[Lambda]*
     \[Omega]^2 - 28*a^2*rh^12*\[Lambda]*\[Omega]^2 - 
    (8*I)*a*m*rh^13*\[Lambda]*\[Omega]^2 - 2*rh^14*\[Lambda]*\[Omega]^2 - 
    a^8*rh^6*\[Lambda]^2*\[Omega]^2 - 4*a^6*rh^8*\[Lambda]^2*\[Omega]^2 - 
    6*a^4*rh^10*\[Lambda]^2*\[Omega]^2 - 4*a^2*rh^12*\[Lambda]^2*\[Omega]^2 - 
    rh^14*\[Lambda]^2*\[Omega]^2 + 16*a^8*M*rh^5*(21 + 4*\[Lambda])*
     \[Omega]^2 + (24*I)*a^12*rh^3*\[Omega]^3 - 96*a^11*m*rh^4*\[Omega]^3 - 
    (72*I)*a^10*M*rh^4*\[Omega]^3 - 372*a^9*m*rh^6*\[Omega]^3 - 
    (180*I)*a^8*M*rh^6*\[Omega]^3 + (48*I)*a^8*rh^7*\[Omega]^3 - 
    552*a^7*m*rh^8*\[Omega]^3 - (96*I)*a^6*M*rh^8*\[Omega]^3 - 
    (48*I)*a^6*rh^9*\[Omega]^3 - 384*a^5*m*rh^10*\[Omega]^3 + 
    (72*I)*a^4*M*rh^10*\[Omega]^3 - (72*I)*a^4*rh^11*\[Omega]^3 - 
    120*a^3*m*rh^12*\[Omega]^3 + (72*I)*a^2*M*rh^12*\[Omega]^3 - 
    (24*I)*a^2*rh^13*\[Omega]^3 - 12*a*m*rh^14*\[Omega]^3 + 
    (12*I)*M*rh^14*\[Omega]^3 + (32*I)*a^8*rh^7*\[Lambda]*\[Omega]^3 + 
    (48*I)*a^6*rh^9*\[Lambda]*\[Omega]^3 + (32*I)*a^4*rh^11*\[Lambda]*
     \[Omega]^3 + (8*I)*a^2*rh^13*\[Lambda]*\[Omega]^3 + 
    (8*I)*a^10*rh^5*(9 + \[Lambda])*\[Omega]^3 + 24*a^12*rh^4*\[Omega]^4 + 
    108*a^10*rh^6*\[Omega]^4 + 192*a^8*rh^8*\[Omega]^4 + 
    168*a^6*rh^10*\[Omega]^4 + 72*a^4*rh^12*\[Omega]^4 + 
    12*a^2*rh^14*\[Omega]^4 - 168*a^5*k*m*M*rh^7*(2*\[Kappa] + \[Omega]) + 
    384*a^3*k*m*M^2*rh^8*(2*\[Kappa] + \[Omega]) - 
    432*a^3*k*m*M*rh^9*(2*\[Kappa] + \[Omega]) + 480*a*k*m*M^2*rh^10*
     (2*\[Kappa] + \[Omega]) - 264*a*k*m*M*rh^11*(2*\[Kappa] + \[Omega]) + 
    144*a^6*k*M*rh^7*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    336*a^4*k*M^2*rh^8*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    384*a^4*k*M*rh^9*\[Omega]*(4*\[Kappa] + \[Omega]) - 
    432*a^2*k*M^2*rh^10*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    240*a^2*k*M*rh^11*\[Omega]*(4*\[Kappa] + \[Omega]) + 
    15*a^8*(1 - k)*k*rh^4*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    84*a^6*(1 - k)*k*M*rh^5*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^6*(1 - k)*k*rh^6*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    112*a^4*(1 - k)*k*M^2*rh^6*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 432*a^4*(1 - k)*k*M*rh^7*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    270*a^4*(1 - k)*k*rh^8*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    360*a^2*(1 - k)*k*M^2*rh^8*(2*\[Lambda] + \[Lambda]^2 - 
      (12*I)*M*\[Omega]) - 660*a^2*(1 - k)*k*M*rh^9*
     (2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*a^2*(1 - k)*k*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    264*(1 - k)*k*M^2*rh^10*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) - 
    312*(1 - k)*k*M*rh^11*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    91*(1 - k)*k*rh^12*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    a^6*rh^8*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^4*rh^10*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    3*a^2*rh^12*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    rh^14*\[Kappa]^2*(2*\[Lambda] + \[Lambda]^2 - (12*I)*M*\[Omega]) + 
    (16*I)*a^4*k*M*rh^7*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (36*I)*a^2*k*M^2*rh^8*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (40*I)*a^2*k*M*rh^9*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    (44*I)*k*M^2*rh^10*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    (24*I)*k*M*rh^11*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    20*a^4*k*rh^9*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 
      12*M*\[Omega]) - 44*a^2*k*M*rh^10*\[Kappa]*((2*I)*\[Lambda] + 
      I*\[Lambda]^2 + 12*M*\[Omega]) + 48*a^2*k*rh^11*\[Kappa]*
     ((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) - 
    52*k*M*rh^12*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]) + 
    28*k*rh^13*\[Kappa]*((2*I)*\[Lambda] + I*\[Lambda]^2 + 12*M*\[Omega]))
