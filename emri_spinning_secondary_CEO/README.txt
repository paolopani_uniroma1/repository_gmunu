If you make use of any part of the code, please acknowledge by citing arXiv:2003.08448 , arXiv:2004.02654
The code use routines of the Black Hole Perturbation Toolkit http://bhptoolkit.org/, so please acknowledge by citing the Toolkit.
The notebook "Teukolsky source terms spinning particle CEO.nb" use the Mathematica xAct package http://www.xact.es/, so please acknowledge by citing the website.

Brief description of the files 
- to compute the asymptotic gravitational fluxes, see "Fluxes spinning particle CEO.nb"
- to compute the Teukolsky source, the exact orbital angular momentum and energy for a spin aligned secondary in a CEO, see "Teukolsky source terms spinning particle CEO.nb"
- the notebooks contained in the folder "BCs_Sasaki_Nakamura_and_coeff"
 1) Derivatives for BCs at horizon -SN equation
 2) Derivatives for BCs at infinity -SN equation
 3) Simplified SN equations and test BCs
  give the coefficients and their derivatives for the BCs of the Sasaki-Nakamura equation
  
Finally, the folder "Aux_files_ceo_spin" contains all of the necessary auxiliary files: 
- the coefficients of the Sasaki-Nakamura equations and their derivatives
- the exact orbital frequency, energy and angular momentum
- the source term
