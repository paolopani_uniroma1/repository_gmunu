(* Created with the Wolfram Language : www.wolfram.com *)
{-(M*q*r*(r*(-2*M + r) + Rot^2)*swsh*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*
     (Jz - epar*(Rot + M*q*\[Chi]))*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
      epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
     (((-4*I)*\[Omega])/(-2*M*r + r^2 + Rot^2) + 
      ((2*I)*(-2*M + 2*r)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
       (r*(-2*M*r + r^2 + Rot^2)^2) + 
      ((2*I)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
       (r^2*(-2*M*r + r^2 + Rot^2)) + 
      (4*r*\[Omega]*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
       (-2*M*r + r^2 + Rot^2)^2 - 
      (2*(-2*M + 2*r)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega])^2)/
       (-2*M*r + r^2 + Rot^2)^3 + I*((-4*r*(-2*M + 2*r)*\[Omega])/
         (-2*M*r + r^2 + Rot^2)^2 + (2*\[Omega])/(-2*M*r + r^2 + Rot^2) + 
        (2*(-2*M + 2*r)^2*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
         (-2*M*r + r^2 + Rot^2)^3 - (2*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
         (-2*M*r + r^2 + Rot^2)^2)))/(2*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) + 
  (swsh*(M*q*r*Rot*\[Chi]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2 + (r*(-2*M + r) + Rot^2)*
      (r^3 + 2*M^3*q^2*\[Chi]^2)*(Jz - epar*(Rot + M*q*\[Chi]))*
      (r^2*(Jz - epar*(Rot + M*q*\[Chi])) + M*q*\[Chi]*
        (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
           (M*q*(M + r)*Rot*\[Chi])/r) + 
         2*Rot*(Jz - epar*(Rot + M*q*\[Chi])))))*
    (((-2*I)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
      (r*(-2*M*r + r^2 + Rot^2)) + (-(m*Rot) + (r^2 + Rot^2)*\[Omega])^2/
      (-2*M*r + r^2 + Rot^2)^2 + I*((2*r*\[Omega])/(-2*M*r + r^2 + Rot^2) - 
       ((-2*M + 2*r)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
        (-2*M*r + r^2 + Rot^2)^2)))/(2*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*((2*M - r)*r - Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) - r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) + 
  ((I/4)*M*q*r*Sqrt[r*(-2*M + r) + Rot^2]*\[Chi]*
    ((r*(-2*M + r) + Rot^2)*(r^3 + 2*M^3*q^2*\[Chi]^2)*
      (Jz - epar*(Rot + M*q*\[Chi]))^2 + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2)*
    ((2*Sqrt[2]*(dswsh + swsh*(-m + Rot*\[Omega]))*
       (-2/r^2 + ((2*I)*r*\[Omega])/(-2*M*r + r^2 + Rot^2) - 
        (I*(-2*M + 2*r)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
         (-2*M*r + r^2 + Rot^2)^2))/Sqrt[-2*M*r + r^2 + Rot^2] - 
     (Sqrt[2]*(-2*M + 2*r)*(dswsh + swsh*(-m + Rot*\[Omega]))*
       (2/r + (I*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/(-2*M*r + r^2 + 
          Rot^2)))/(-2*M*r + r^2 + Rot^2)^(3/2)))/
   (Sqrt[2]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) + 
  (M*q*r^2*((2*M - r)*r - Rot^2)*\[Chi]*(Jz - epar*(Rot + M*q*\[Chi]))*
    (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
     epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
    (((-4*I)*Rot*(dswsh + swsh*(-m + Rot*\[Omega])))/
      (r^2*(-2*M*r + r^2 + Rot^2)) + 
     (4*(-2*M + 2*r)*(-(swsh*\[Lambda])/2 + (-m - (I*Rot)/r + Rot*\[Omega])*
         (dswsh + swsh*(-m + Rot*\[Omega]))))/(-2*M*r + r^2 + Rot^2)^2))/
   (12*M^3*q^2*Rot*((2*M - r)*r - Rot^2)*\[Chi]^2*
     (Jz - epar*(Rot + M*q*\[Chi])) - 4*r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
     (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
      (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
        epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r)))) - 
  ((-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
     epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
    (-(swsh*\[Lambda])/2 + (-m - (I*Rot)/r + Rot*\[Omega])*
      (dswsh + swsh*(-m + Rot*\[Omega])))*
    (M*q*(r*(-2*M + r) + Rot^2)*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (M*q*\[Chi]*(Jz - epar*(Rot + M*q*\[Chi]))*(-2*M*r - I*m*r*Rot + 
         I*r^3*\[Omega] + Rot^2*(2 + I*r*\[Omega])) + 
       (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
           (M*q*(M + r)*Rot*\[Chi])/r))*(-r^2 + M*q*\[Chi]*
          ((-I)*m*r + Rot + I*r*Rot*\[Omega])))))/
   ((-2*M*r + r^2 + Rot^2)*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*((2*M - r)*r - Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) - r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) + 
  (Sqrt[r*(-2*M + r) + Rot^2]*(dswsh + swsh*(-m + Rot*\[Omega]))*
    (2/r + (I*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/(-2*M*r + r^2 + Rot^2))*
    (I*M*q*r*\[Chi]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2 + 
     M*q*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*(Jz - epar*(Rot + M*q*\[Chi]))^2*
      (I*M*r - m*r*Rot + r^3*\[Omega] + Rot^2*(-I + r*\[Omega])) + 
     (Jz - epar*(Rot + M*q*\[Chi]))*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
       epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
      (I*(r^3 + 3*M*q*r*Rot*\[Chi])*(r^2 - (M^3*q^2*\[Chi]^2)/r) + 
       (r^3 + 2*M^3*q^2*\[Chi]^2)*(I*r^2 + M*q*\[Chi]*(-(m*r) + I*Rot + 
           r*Rot*\[Omega])))))/(2*Sqrt[-2*M*r + r^2 + Rot^2]*
    (r^2 - (M^3*q^2*\[Chi]^2)/r)*(3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*
      \[Chi]^2*(Jz - epar*(Rot + M*q*\[Chi])) + 
     r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*(Rot*(-2*M*r + r^2 + Rot^2)*
        (Jz - epar*(Rot + M*q*\[Chi])) + (r^2 + Rot^2)*
        (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
           (M*q*(M + r)*Rot*\[Chi])/r))))), 
 ((-I/4)*M*q*r*(-2*M + 2*r)*Sqrt[r*(-2*M + r) + Rot^2]*\[Chi]*
    ((r*(-2*M + r) + Rot^2)*(r^3 + 2*M^3*q^2*\[Chi]^2)*
      (Jz - epar*(Rot + M*q*\[Chi]))^2 + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2)*(dswsh + swsh*(-m + Rot*\[Omega])))/
   ((-2*M*r + r^2 + Rot^2)^(3/2)*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) + 
  (M*q*r*(r*(-2*M + r) + Rot^2)*swsh*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*
    (Jz - epar*(Rot + M*q*\[Chi]))*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
     epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
    (-r^(-2) + ((2*I)*r*\[Omega])/(-2*M*r + r^2 + Rot^2) - 
     (I*(-2*M + 2*r)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
      (-2*M*r + r^2 + Rot^2)^2))/((r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) - 
  (swsh*(M*q*r*Rot*\[Chi]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2 + (r*(-2*M + r) + Rot^2)*
      (r^3 + 2*M^3*q^2*\[Chi]^2)*(Jz - epar*(Rot + M*q*\[Chi]))*
      (r^2*(Jz - epar*(Rot + M*q*\[Chi])) + M*q*\[Chi]*
        (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
           (M*q*(M + r)*Rot*\[Chi])/r) + 
         2*Rot*(Jz - epar*(Rot + M*q*\[Chi])))))*
    (r^(-1) + (I*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
      (-2*M*r + r^2 + Rot^2)))/((r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*((2*M - r)*r - Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) - r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) + 
  (Sqrt[r*(-2*M + r) + Rot^2]*(dswsh + swsh*(-m + Rot*\[Omega]))*
    (I*M*q*r*\[Chi]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2 + 
     M*q*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*(Jz - epar*(Rot + M*q*\[Chi]))^2*
      (I*M*r - m*r*Rot + r^3*\[Omega] + Rot^2*(-I + r*\[Omega])) + 
     (Jz - epar*(Rot + M*q*\[Chi]))*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
       epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
      (I*(r^3 + 3*M*q*r*Rot*\[Chi])*(r^2 - (M^3*q^2*\[Chi]^2)/r) + 
       (r^3 + 2*M^3*q^2*\[Chi]^2)*(I*r^2 + M*q*\[Chi]*(-(m*r) + I*Rot + 
           r*Rot*\[Omega])))))/(2*Sqrt[-2*M*r + r^2 + Rot^2]*
    (r^2 - (M^3*q^2*\[Chi]^2)/r)*(3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*
      \[Chi]^2*(Jz - epar*(Rot + M*q*\[Chi])) + 
     r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*(Rot*(-2*M*r + r^2 + Rot^2)*
        (Jz - epar*(Rot + M*q*\[Chi])) + (r^2 + Rot^2)*
        (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
           (M*q*(M + r)*Rot*\[Chi])/r))))), 
 -(swsh*(M*q*r*Rot*\[Chi]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2 + (r*(-2*M + r) + Rot^2)*
      (r^3 + 2*M^3*q^2*\[Chi]^2)*(Jz - epar*(Rot + M*q*\[Chi]))*
      (r^2*(Jz - epar*(Rot + M*q*\[Chi])) + M*q*\[Chi]*
        (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
           (M*q*(M + r)*Rot*\[Chi])/r) + 
         2*Rot*(Jz - epar*(Rot + M*q*\[Chi]))))))/
  (2*(r^2 - (M^3*q^2*\[Chi]^2)/r)*(3*M^3*q^2*Rot*((2*M - r)*r - Rot^2)*
     \[Chi]^2*(Jz - epar*(Rot + M*q*\[Chi])) - r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
     (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
      (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
        epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))), 
 ((-I/2)*M*q*r*Sqrt[r*(-2*M + r) + Rot^2]*\[Chi]*
    ((r*(-2*M + r) + Rot^2)*(r^3 + 2*M^3*q^2*\[Chi]^2)*
      (Jz - epar*(Rot + M*q*\[Chi]))^2 + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2)*(dswsh + swsh*(-m + Rot*\[Omega]))*
    (2/r + (I*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/(-2*M*r + r^2 + Rot^2)))/
   (Sqrt[-2*M*r + r^2 + Rot^2]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) - 
  (M*q*r^2*\[Chi]*(Jz - epar*(Rot + M*q*\[Chi]))*
    (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
     epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
    (-(swsh*\[Lambda])/2 + (-m - (I*Rot)/r + Rot*\[Omega])*
      (dswsh + swsh*(-m + Rot*\[Omega]))))/
   (3*M^3*q^2*Rot*((2*M - r)*r - Rot^2)*\[Chi]^2*
     (Jz - epar*(Rot + M*q*\[Chi])) - r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
     (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
      (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
        epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r)))) + 
  (M*q*r*(r*(-2*M + r) + Rot^2)*swsh*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*
    (Jz - epar*(Rot + M*q*\[Chi]))*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
     epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
    (((-2*I)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
      (r*(-2*M*r + r^2 + Rot^2)) + (-(m*Rot) + (r^2 + Rot^2)*\[Omega])^2/
      (-2*M*r + r^2 + Rot^2)^2 + I*((2*r*\[Omega])/(-2*M*r + r^2 + Rot^2) - 
       ((-2*M + 2*r)*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
        (-2*M*r + r^2 + Rot^2)^2)))/(2*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))), 
 ((-I/2)*M*q*r*Sqrt[r*(-2*M + r) + Rot^2]*\[Chi]*
    ((r*(-2*M + r) + Rot^2)*(r^3 + 2*M^3*q^2*\[Chi]^2)*
      (Jz - epar*(Rot + M*q*\[Chi]))^2 + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (-(Jz*(Rot + (M^2*q*\[Chi])/r)) + epar*(r^2 + Rot^2 + 
          (M*q*(M + r)*Rot*\[Chi])/r))^2)*(dswsh + swsh*(-m + Rot*\[Omega])))/
   (Sqrt[-2*M*r + r^2 + Rot^2]*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))) - 
  (M*q*r*(r*(-2*M + r) + Rot^2)*swsh*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*
    (Jz - epar*(Rot + M*q*\[Chi]))*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
     epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))*
    (r^(-1) + (I*(-(m*Rot) + (r^2 + Rot^2)*\[Omega]))/
      (-2*M*r + r^2 + Rot^2)))/((r^2 - (M^3*q^2*\[Chi]^2)/r)*
    (3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*\[Chi]^2*
      (Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
      (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
       (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
         epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r))))), 
 -(M*q*r*(r*(-2*M + r) + Rot^2)*swsh*\[Chi]*(r^3 + 2*M^3*q^2*\[Chi]^2)*
    (Jz - epar*(Rot + M*q*\[Chi]))*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
     epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r)))/
  (2*(r^2 - (M^3*q^2*\[Chi]^2)/r)*(3*M^3*q^2*Rot*(r*(-2*M + r) + Rot^2)*
     \[Chi]^2*(Jz - epar*(Rot + M*q*\[Chi])) + r*(r^2 - (M^3*q^2*\[Chi]^2)/r)*
     (Rot*(-2*M*r + r^2 + Rot^2)*(Jz - epar*(Rot + M*q*\[Chi])) + 
      (r^2 + Rot^2)*(-(Jz*(Rot + (M^2*q*\[Chi])/r)) + 
        epar*(r^2 + Rot^2 + (M*q*(M + r)*Rot*\[Chi])/r)))))}
