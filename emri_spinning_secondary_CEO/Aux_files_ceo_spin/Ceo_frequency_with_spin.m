(* Created with the Wolfram Language : www.wolfram.com *)
{(2*M*r^3*Rot + 3*M^2*q*r*(r^2 + 2*Rot^2)*\[Chi] + 4*M^4*q^2*Rot*\[Chi]^2 + 
   3*M^3*q^2*r*Rot*\[Chi]^2 - Sqrt[M]*r*Sqrt[4*r^7 + 12*M*q*r^5*Rot*\[Chi] + 
      13*M^3*q^2*r^4*\[Chi]^2 + 6*M^4*q^3*r^2*Rot*\[Chi]^3 - 
      8*M^6*q^4*r*\[Chi]^4 + 9*M^5*q^4*Rot^2*\[Chi]^4])/
  (-2*r^6 + 4*M^4*q^2*Rot^2*\[Chi]^2 + 6*M^2*q*r*Rot^2*\[Chi]*
    (Rot + M*q*\[Chi]) + 2*M*r^3*(Rot^2 + 3*M*q*Rot*\[Chi] + 
     M^2*q^2*\[Chi]^2)), (2*M*r^3*Rot + 3*M^2*q*r*(r^2 + 2*Rot^2)*\[Chi] + 
   4*M^4*q^2*Rot*\[Chi]^2 + 3*M^3*q^2*r*Rot*\[Chi]^2 + 
   Sqrt[M]*r*Sqrt[4*r^7 + 12*M*q*r^5*Rot*\[Chi] + 13*M^3*q^2*r^4*\[Chi]^2 + 
      6*M^4*q^3*r^2*Rot*\[Chi]^3 - 8*M^6*q^4*r*\[Chi]^4 + 
      9*M^5*q^4*Rot^2*\[Chi]^4])/(-2*r^6 + 4*M^4*q^2*Rot^2*\[Chi]^2 + 
   6*M^2*q*r*Rot^2*\[Chi]*(Rot + M*q*\[Chi]) + 
   2*M*r^3*(Rot^2 + 3*M*q*Rot*\[Chi] + M^2*q^2*\[Chi]^2))}
