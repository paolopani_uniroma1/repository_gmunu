(* ::Package:: *)

BeginPackage["ringdown`"];

spheroidalKerr::usage =
 	"spheroidalKerr[s,l,m,a] solves the spheroidal equation for the Kerr metric";

pattern::usage=
  "pattern[\[Theta],\[Phi],\[Psi]] returns the GW pattern functions (fx,fp) as a function of the polar, azimuthal and polarization angle";

ringwave::usage =
 	"ringwave[f,Ap,\[Phi]p,\[Tau],\[Omega],spher] template for the post merger singal as a function of the frequency and damping time of a given mode";
 	
dringwave::usage =
 	"dringwave[f,Ap,\[Phi]p,\[Tau],\[Omega],spher] derivative of the ringdown template";


Begin["`Private`"];


(* ::Subsubsection::Closed:: *)
(*spheroidalKerr*)


spheroidalKerr[s_,l_,m_,a_]:=Block[{\[Chi],\[Alpha]\[Theta],\[Beta]\[Theta],\[Gamma]\[Theta],\[Alpha]r,\[Beta]r,\[Gamma]r,A,A0,\[Omega],\[Omega]0,N=100,n,f\[Theta],fr,y,x,c,slm,ap,ap0,j,z,Eq\[Theta],Eqr,\[Delta]z,
zmax,c0,c1,c2,c3,c4,b,nit,jump,na,\[Delta]a,k,km,kp},

\[Omega]0 = 2*(0.37 -0.08 I);
c = \[Chi] \[Omega];

{km,kp} = {Abs[m-s],Abs[m+s]}/2;

\[Alpha]\[Theta] = Table[-2(p+1)(p+2 km+1),{p,0,N}];
\[Beta]\[Theta] = Table[p(p-1)+2p(km+kp+1-2c)-(2c(2 km+s+1)-(km+kp)(km+kp+1))-(c^2+s(s+1)+A),{p,0,N}];
\[Gamma]\[Theta] = Table[2c(p+km+kp+s),{p,0,N}];

b = Sqrt[1-4 \[Chi]^2];

c0 = 1-s-I \[Omega] -2 I/b(\[Omega]/2 - \[Chi] m);
c1 = -4+2I \[Omega](2+b)+4I/b(\[Omega]/2-\[Chi] m);
c2 = s+3-3 I \[Omega]-2I/b(\[Omega]/2-\[Chi] m);
c3 = \[Omega]^2 (4.+2b-\[Chi]^2)-2\[Chi] m \[Omega]-s-1+(2+b)I \[Omega]- A+(4\[Omega]+2I)/b(\[Omega]/2-\[Chi] m);
c4 = s+1-2\[Omega]^2-(2s+3)I \[Omega]-(4\[Omega]+2I)/b(\[Omega]/2-\[Chi] m);

\[Alpha]r = Table[p^2+(c0+1)p+c0,{p,0,N}];

\[Beta]r = Table[-2 p^2+(c1+2)p+c3,{p,0,N}];

\[Gamma]r = Table[p^2+(c2-3)p+c4-c2+2,{p,0,N}];


f\[Theta][y_,{\[Alpha]\[Theta]_,\[Gamma]\[Theta]_,\[Beta]\[Theta]_}]:=\[Gamma]\[Theta]/(\[Beta]\[Theta]-\[Alpha]\[Theta] y);
Eq\[Theta][\[Chi]_,A_,\[Omega]_]=\[Beta]\[Theta][[1]]/\[Alpha]\[Theta][[1]]-Fold[f\[Theta],Last@\[Gamma]\[Theta]/Last@\[Beta]\[Theta],Reverse@Most@Transpose@{\[Alpha]\[Theta][[2;;-1]],\[Gamma]\[Theta][[2;;-1]],\[Beta]\[Theta][[2;;-1]]}];

fr[y_,{\[Alpha]r_,\[Gamma]r_,\[Beta]r_}]:= \[Gamma]r/(\[Beta]r-\[Alpha]r y);
Eqr[\[Chi]_,A_,\[Omega]_]=\[Beta]r[[1]]/\[Alpha]r[[1]]-Fold[fr,Last@\[Gamma]r/Last@\[Beta]r,Reverse@Most@Transpose@{\[Alpha]r[[2;;-1]],\[Gamma]r[[2;;-1]],\[Beta]r[[2;;-1]]}];

For[j=2,j<=l,j++,
 
 A0 = j(j+1)-s(s+1);
 \[Omega]0 = FindRoot[Eqr[0,A0,\[Omega]],{\[Omega],\[Omega]0/2,2\[Omega]0}][[1,2]];

];

If[a==0,

 {A,\[Omega]} = x = {A0,\[Omega]0};

 Goto[jump]

];

na = 40;
\[Delta]a = a/2/na;

For[j=1,j<=na,j++;	
  \[Omega]0 = FindRoot[Eqr[j \[Delta]a,A0,\[Omega]],{\[Omega],\[Omega]0,2\[Omega]0}][[1,2]];
  A0 = FindRoot[Eq\[Theta][j \[Delta]a,A,\[Omega]0],{A,A0,2A0}][[1,2]];
];

nit = 10;
x = Table[{0,0},{j,1,nit}];

x[[1,1]] = A0;
x[[1,2]] = \[Omega]0;

For[j=2,j<=nit,j++,
	x[[j,2]] = FindRoot[Eqr[a/2,A0,\[Omega]],{\[Omega],\[Omega]0,2\[Omega]0},PrecisionGoal->8,AccuracyGoal->8][[1,2]];
	\[Omega]0 = x[[j,2]];
	
	x[[j,1]] = FindRoot[Eq\[Theta][a/2,A,\[Omega]0],{A,A0,2A0},PrecisionGoal->8,AccuracyGoal->8][[1,2]];
	A0 = x[[j,1]];
	
	\[Omega]0 = x[[j,2]];
	A0 = x[[j,1]];

];

A = x[[nit,1]];
\[Omega] = x[[nit,2]];

Label[jump];

If[Abs[Eqr[a/2,A,\[Omega]]] > 10^-8 \[Or] Abs[Eq\[Theta][a/2,A,\[Omega]]] > 10^-8, Print["Root search failed"]];

Print["\!\(\*SubscriptBox[\(\[Omega]\), \(lm\)]\) = ",\[Omega]/2," - \!\(\*SubscriptBox[\(A\), \(lm\)]\) = ",A];

\[Chi] = a/2;

ap = Table[0,{j,1,N}];

ap0 = 1;
ap[[1]]=-((ap0 \[Beta]\[Theta][[1]])/\[Alpha]\[Theta][[1]]);
ap[[2]]=-((\[Beta]\[Theta][[2]]ap[[1]])/\[Alpha]\[Theta][[2]])-\[Gamma]\[Theta][[2]]/\[Alpha]\[Theta][[2]] ap0;

For[j=3,j<=N,j++,
	ap[[j]]=-((\[Beta]\[Theta][[j]]ap[[j-1]])/\[Alpha]\[Theta][[j]])-\[Gamma]\[Theta][[j]]/\[Alpha]\[Theta][[j]] ap[[j-2]];
];

{zmax,\[Delta]z} = {1,0.001};

slm = Interpolation[Join[{{-1,0}},Table[{z,Exp[\[Chi] \[Omega] z](1+z)^km (1-z)^kp (ap0+\!\(
\*SubsuperscriptBox[\(\[Sum]\), \(p = 1\), \(p = N\)]\(
\*SuperscriptBox[\((1 + z)\), \(p\)] ap[\([p]\)]\)\))},{z,-zmax+\[Delta]z,zmax-\[Delta]z,\[Delta]z}],{{1,0}}],InterpolationOrder->1];

Return[{\[Omega]/2,A,slm}];

]


(* ::Subsubsection::Closed:: *)
(*GW pattern function*)


pattern[\[Theta]_,\[Phi]_,\[Psi]_]:=Block[{fp,fx},

fp = 1/2 (1+Cos[\[Theta]]^2)Cos[2\[Phi]]Cos[2\[Psi]]-Cos[\[Theta]]Sin[2\[Phi]]Sin[2\[Psi]];

fx = 1/2 (1+Cos[\[Theta]]^2)Cos[2\[Phi]]Sin[2\[Psi]]+Cos[\[Theta]]Sin[2\[Phi]]Cos[2\[Psi]];

Return[{fp,fx}]

]


(* ::Subsubsection::Closed:: *)
(*Post-merger template*)


ringwave[f_,Ap_,\[Phi]p_,\[Tau]_,\[Omega]_,spher_]:=Block[{i,bp,bm,\[Phi]0=1,Nx=1,hc,hp},

{bp,bm} = {\[Tau]/(1+\[Tau]^2 (2\[Pi] f+\[Omega])^2),\[Tau]/(1+\[Tau]^2 (2\[Pi] f-\[Omega])^2)};

hp = Ap/Sqrt[2] (Exp[I \[Phi]p]bp*spher+Exp[-I \[Phi]p]bm*spher\[Conjugate]);

hc = -Ap (I Nx)/Sqrt[2] (Exp[I (\[Phi]p+\[Phi]0)]bp*spher+Exp[-I (\[Phi]p+\[Phi]0)]bm*spher\[Conjugate]);

Return[{hp,hc}]

]


(* ::Subsubsection::Closed:: *)
(*Derivative of post-merger template*)


dringwave[f_,amp_,phase_,damp_,freq_,spher_]:=Block[{i,dh,\[Tau],\[Omega],Ap,\[Phi]p},

dh = {Ap D[ringwave[f,Ap,\[Phi]p,\[Tau],\[Omega],spher],Ap],D[ringwave[f,Ap,\[Phi]p,\[Tau],\[Omega],spher],\[Phi]p],
	D[ringwave[f,Ap,\[Phi]p,\[Tau],\[Omega],spher],\[Omega]],D[ringwave[f,Ap,\[Phi]p,\[Tau],\[Omega],spher],\[Tau]]};

{Ap,\[Phi]p,\[Tau],\[Omega]} = {amp,phase,damp,freq};

Return[dh]
]


(* ::Subsection::Closed:: *)
(*End*)


End[];

EndPackage[];

