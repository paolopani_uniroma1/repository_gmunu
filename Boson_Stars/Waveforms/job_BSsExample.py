import sys
import numpy as np
import bilby
import time
#from bilby.gw.eos import TabularEOS, EOSFamily
import multiprocessing as mp
import astropy
import TaylorBS 


#----------------- Parameters conversion functions------------------------

def convert_binarybosonstars(parameters):
    parameters['beta_1'] = 1/parameters['M_B']*parameters['chirp_mass']/(2*parameters['eta']**(3/5))*(1+np.sqrt(1-4*parameters['eta']))
    parameters['beta_2'] =1/parameters['M_B']*parameters['chirp_mass']/(2*parameters['eta']**(3/5))*(1-np.sqrt(1-4*parameters['eta']))
    return parameters
                                                                         
def injection_params(parameters):
    parameters['mass_1'] = parameters['chirp_mass']/(2*parameters['eta']**(3/5))*(1+np.sqrt(1-4*parameters['eta']))      
    parameters['mass_2'] = parameters['chirp_mass']/(2*parameters['eta']**(3/5))*(1-np.sqrt(1-4*parameters['eta']))   
    return parameters

#-------------------------------------------------------------------------
                            
## setup random seed for reproducibility
np.random.seed(10201238)

fmin = 20. ## Hz

## set injection parameters
#m1, m2, dL = 36., 25., 200.
#l1 = obj_fam.lambda_from_mass(m1)
#l2 = obj_fam.lambda_from_mass(m2)
#l1, l2 = 0., 0.

injection_parameters = dict(chirp_mass=10, eta=0.247, M_B=230,\
        chi_1=0.2, chi_2=0.1, phase=1.3, luminosity_distance=1000,\
        theta_jn=0.4,\
        #kappa_1=1., kappa_2=1.,\
        #lambda_1=l1, lambda_2=l2,\
        tilt_1=0.,tilt_2=0.,phi_12=0.,phi_jl=0.,\
        psi=2.659, geocent_time=1187008882.43,\
        ra=1.097*np.pi, dec=-0.13*np.pi)

## set waveform length and sampling frequency
duration = 1.
sampling_frequency = 2*1024 ## Nydqvist frequency = sampling_frequency/2
start_time = injection_parameters['geocent_time'] + 2 - duration

## set waveform hyperparameters
waveform_arguments = dict(waveform_approximant='TaylorF2',\
        #IMRPhenomPv2_NRTidal',\
        reference_frequency=50., minimum_frequency=fmin)

waveform_generator = bilby.gw.WaveformGenerator(\
        duration=duration, sampling_frequency=sampling_frequency,\
        frequency_domain_source_model=TaylorBS.bs_taylor_f2,\
        #parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,\
        waveform_arguments=waveform_arguments)

## define interferometers
interferometers = bilby.gw.detector.InterferometerList(['ET'])
## change PSD
interferometers[0].power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_file='ET_D_psd.txt')
interferometers[1].power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_file='ET_D_psd.txt')
interferometers[2].power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_file='ET_D_psd.txt')

for ifo in interferometers:
    ifo.minimum_frequency = fmin
    ifo.maximum_frequency = 100

## set noisy ifos
'''
interferometers.set_strain_data_from_power_spectral_densities(\
        sampling_frequency=sampling_frequency, duration=duration,\
        start_time=start_time)
'''
## set zero-noise ifos

interferometers.set_strain_data_from_zero_noise(\
        sampling_frequency=sampling_frequency, duration=duration,\
        start_time=start_time)

## debug print
'''
for ifo in interferometers:
    print(ifo)
    print('\n')
'''

## inject signal 
interferometers.inject_signal(parameters=injection_params(injection_parameters.copy()),\
        waveform_generator=waveform_generator)

## SNR
'''
for ifo in interferometers:
    print(ifo.matched_filter_snr())
    print('\n')
'''

## debug print
'''
with open('outdir/outputjob0cythonarray.txt', 'w') as f:
    for ifo in interferometers:
       # for i in range(0,len(ifo.strain_data.time_domain_strain)):
        print(ifo.strain_data.time_domain_strain, file=f)
        print('\n')
'''

## set priors
# priors = bilby.gw.prior.BNSPriorDict()
priors = bilby.core.prior.PriorDict(conversion_function=convert_binarybosonstars)

for key in ['tilt_1','tilt_2','phi_12','phi_jl','psi', 'ra', 'dec', 'theta_jn']:
    priors[key] = injection_parameters[key]                                              
priors['chirp_mass'] = bilby.core.prior.Uniform(name='chirp_mass', latex_label='$M_{chirp}$', minimum=5, maximum=15,unit='$M_{\\odot}$')
priors['eta'] = bilby.core.prior.Uniform(name= 'eta', latex_label='$\\eta$', minimum=0., maximum=0.25)
priors['M_B'] = bilby.core.prior.Uniform(name='M_B', latex_label='$M_B$', minimum=10, maximum=500, unit='$M_{\\odot}$')

#The following constrains beta between a maximum (limit for static configurations) and a minumum 
#(arbitrarily set to 0.02 corresponding to an almost Newtonian configuration) during the samping. 
priors['beta_1'] = bilby.core.prior.Constraint(minimum=0.02, maximum=0.06) 
priors['beta_2'] = bilby.core.prior.Constraint(minimum=0.02, maximum=0.06)

'''
priors['geocent_time'] = bilby.core.prior.Uniform(
minimum=injection_parameters['geocent_time'] - 0.1,
maximum=injection_parameters['geocent_time'] + 0.1,
name='geocent_time', latex_label='$t_c$', unit='$s$')
'''

priors['chi_1'] = bilby.core.prior.Uniform(name='chi_1', latex_label='$\\chi_1$', minimum=-0.99, maximum=0.99)              
priors['chi_2'] = bilby.core.prior.Uniform(name='chi_2', latex_label='$\\chi_2$', minimum=-0.99, maximum=0.99)


priors['phase'] = bilby.gw.prior.Uniform(name='phase', minimum=0, maximum=2 * np.pi, boundary='periodic')
priors['luminosity_distance'] = bilby.gw.prior.UniformSourceFrame(name='luminosity_distance', minimum=100., maximum=2000.)

#Initialize the likelihood by providing the interferometer data (ifos), the waveform generator, and the priors.
likelihood = bilby.gw.GravitationalWaveTransient(
interferometers=interferometers, waveform_generator=waveform_generator, priors=priors,
distance_marginalization=True, phase_marginalization=True, time_marginalization=True)

# Run sampler. 
start_time = time.time()

result = bilby.run_sampler(npool=10,\
        likelihood=likelihood, priors=priors, sampler='dynesty', npoints=1500,
injection_parameters=injection_parameters, outdir='outdir/job_BSsExample',
label='job_BSs-ET-chirp10_q08_fmin20_MB230_spin=2-1')

# Make a corner plot.
result.plot_corner()

print("--- %s seconds ---" % (time.time() - start_time))  

