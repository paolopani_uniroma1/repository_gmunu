#Python libraries
import joblib
import numpy as np
from scipy import interpolate

#------------------ Constants ------------------------------------------

##Euler constant 
gamma_e = 0.5772156649
## solar mass
solar_m = 1.9884099021470415E30 #Kg
## Megaparsec
m_parsec = 3.085677581491367E22 #m
## speed of light
light_s = 299792458. #m/s
## Newton's constant
g_newton = 6.67430E-11 #m^3/kg/s


########################### FUNCTION DEFINITIONS ###############################


def compute_lambda_tilde(eta,lambda_s,lambda_a):
    #5pN leading tidal coefficient from http://www.arxiv.org/abs/0911.3535 
    delta = np.sqrt(1-4*eta)
    return 16/13*((1+7*eta-31*eta**2)*lambda_s+\
                  delta*(1+9*eta-11*eta**2)*lambda_a)

def compute_delta_lambda(eta,lambda_s,lambda_a):
    #6pN tidal coefficient from https://arxiv.org/abs/1410.8866
    delta = np.sqrt(1-4*eta)
    return delta*(1-13272/1319*eta+8944/1319*eta**2)*lambda_s+\
        (1-15910/1319*eta+32850/1319*eta**2+3380/1319*eta**3)*lambda_a

def mass_components(chirp_mass, eta):
    delta = np.sqrt(1-4*eta)
    mass_1=chirp_mass/(2*eta**(3/5))*(1+delta)
    mass_2=chirp_mass/(2*eta**(3/5))*(1-delta)
    return mass_1, mass_2

def tidal_from_beta(beta):
    #Inverted from relation (47) in https://arxiv.org/abs/1704.08651 
    #Numerical coefficients from https://arxiv.org/abs/2302.13954 (12) 
    if 0.02 <= beta <= 0.06:
        alpha0, alpha1, alpha2, alpha3, alpha4 = 7.52380096,  -9.03269026, - 4417.76753903, 100455.45237176, - 766441.06846530 
        tidal = 10**(alpha0 + alpha1* beta +  alpha2* beta**2 + alpha3* beta**3 +alpha4* beta**4)
        return tidal
    else:
        print("error: mass not in the correct range")
        exit(1)

def quadrupole_from_pars(beta, chi, func={'interpolant':None}):
    '''Computes the quadrupole moment from a bspline interpolation from the data in 
       https://arxiv.org/abs/2203.07442 contained in 'data_interp.pkl' '''
       
    if 0 <= abs(chi) < 0.0025:
        chi = 0.0025 
    if 0.02 <= beta <= 0.06 and 0.0025 <= abs(chi) <= 3.:
        if not func['interpolant']:
            func['interpolant'] = joblib.load('./data_interp.pkl')
        k2=func['interpolant'](beta, chi).item()
        return k2
    else:
        print("error: mass or spin not in the correct range")
        exit(1)
        

########################### TAYLOR-F2 PHASE #############################

'''Computes the Taylor_F2 phase at the post-Newtonian order equal to pn_phase_order/2,
   (max 3.5pN), including tidal corrections at 5pN and 6pN.'''

def pn_phase(frequency_array, total_mass, eta, chi_s, chi_a, lambda_s, lambda_a, kappa_s, kappa_a ,pn_phase_order=-1):

    delta = np.sqrt(1-4*eta)
    v = (g_newton/(light_s**3)*np.pi*frequency_array*total_mass)**(1/3.)
    
    # Initialization of coeffecients arrays    
    pp_phase_terms = np.zeros((8, len(v)))
    spin_phase_terms = np.zeros((8, len(v)))
    tidal_phase_terms = np.zeros((2, len(v)))
    
    np.seterr(divide='ignore')
    np.seterr(invalid='ignore')
        
    #Non-zero point particle phase coefficients 
    pp_phase_terms[0] = 1.
    pp_phase_terms[2] = 3715/756.+55*eta/9.
    pp_phase_terms[3] = -16*np.pi     
    pp_phase_terms[4] = 15293365/508032.+27145*eta/504.+3085*eta**2/72. 
    pp_phase_terms[5] = (1.+np.log(v**3)) *\
             ((38645*np.pi/756.)-(65*np.pi*eta/9.)) 
    pp_phase_terms[6] = 11583231236531/4694215680.-6848*gamma_e/21.-640*np.pi**2/3.+(-15737765635/3048192. +
             2255*np.pi**2/12.)*eta+76055*eta**2/1728.-127825*eta**3/1296.-6848/63. *\
             np.log(64*v**3)
    pp_phase_terms[7] = 77096675*np.pi/254016.+378515*np.pi*eta/1512.-74045*np.pi*eta**2/756.
    
    #Non-zero spin phase coefficients 
    spin_phase_terms[3] = 113/3.*delta*chi_a+(113/3.-76*eta/3.)*chi_s
    spin_phase_terms[4] = -5/8.*chi_s**2*(1+156*eta+80*delta*kappa_a +
             80*(1-2*eta)*kappa_s)+chi_a**2*(-5/8.-50*delta*kappa_a-50*kappa_s+100*eta*(1+kappa_s)) -\
             5/4.*chi_a*chi_s*(delta+80*(1-2*eta)*kappa_a+80*delta*kappa_s)
    spin_phase_terms[5] = (1.+np.log(v**3))*\
             (delta*(-732985/2268.-(140*eta/9.))*chi_a + (-732985/2268.+24260*eta/81.+340*eta**2/9.)*chi_s)
    spin_phase_terms[6] = np.pi*(2270/3.*delta*chi_a+(2270/3.-520*eta)*chi_s)+chi_s**2*((-1344475/2016.) +
             829705/504*eta+3415/9.*eta**2+delta*(26015/28.-1495/6.*eta)*kappa_a +
             (26015/28.-44255/21.*eta-240*eta**2)*kappa_s)+chi_a**2*(-1344475/2016.+267815/252.*eta -
             240*eta**2+delta*(26015/28.-1495/6.*eta)*kappa_a+(26015/28.-44255/21.*eta-240*eta**2)*kappa_s)\
             + chi_a*chi_s*((26015/14.-88510/21.*eta-480*eta**2)*kappa_a+delta*(-1344475/1008. +
             745/18.*eta+(26015/14.-1495/3.*eta)*kappa_s))
    spin_phase_terms[7] = (-25150083775/3048192.+26804935/6048.*eta-1985/48.*eta**2)*delta*chi_a\
             + (-25150083775/3048192.+10566655595/762048.*eta-1042165/3024.*eta**2+5345/36.*eta**3)*chi_s
    
    #Cubic spin terms (not implemented)
    '''spin_phase_terms[7]+= +(14585/24.-2380*eta)*delta*chi_a**3\
     +(14585/24.-475/6.*eta+100/3.*eta**2)*chi_s**3+(14585/8.-215/2.*eta)*delta*chi_a*chi_s**2 +
     (14585/8.-7270*eta+80*eta**2)*chi_a**2*chi_s'''
    
    #Non-zero tidal phase coefficients 
    tidal_phase_terms[0] = -39/2.*compute_lambda_tilde(eta,lambda_s,lambda_a)
    tidal_phase_terms[1] = -3115/64.*compute_lambda_tilde(eta,lambda_s,lambda_a)\
         +6595/364.*delta*compute_delta_lambda(eta,lambda_s,lambda_a)
    
    #Build phase
    phi=0
    if pn_phase_order==-1:
        dim = len(pp_phase_terms)
    else:
        dim = int(pn_phase_order)+1
    for i in range(dim):
        phi += (pp_phase_terms[i]+spin_phase_terms[i])*v**i
        
    # Add tidal terms
    phi = -(-np.pi/4.+3/(128.*eta)*v**(-5)*(phi+tidal_phase_terms[0]*v**10+tidal_phase_terms[1]*v**12))
    
    return phi


########################### WAVEFORM ###############################


def bs_taylor_f2(frequency_array, chirp_mass, eta, M_B, chi_1, chi_2,\
                 phase, luminosity_distance, theta_jn, tilt_1=0., tilt_2=0., phi_12=0., phi_jl=0., **kwargs):
    
    '''Waveform model for boson star binaries, with tidal deformability and quadrupole moments computed from 
       the binary component masses and spins and M_B as defined in https://arxiv.org/abs/2007.05264''' 

    # Reference frequency, minimum frequency, pN phase order
    waveform_kwargs = dict(reference_frequency=50.0, minimum_frequency=20.0, pn_phase_order=-1)
    waveform_kwargs.update(kwargs)
    reference_frequency = waveform_kwargs['reference_frequency']
    minimum_frequency = waveform_kwargs['minimum_frequency']
    pn_phase_order = waveform_kwargs['pn_phase_order']

    # Mass, beta, quadrupole and tidal def.
    mass_1, mass_2 = mass_components(chirp_mass,eta)
    beta_1, beta_2 = mass_1/M_B, mass_2/M_B
    lambda_1 = tidal_from_beta(beta_1)
    lambda_2 = tidal_from_beta(beta_2)
    kappa_1 = quadrupole_from_pars(beta_1,chi_1)
    kappa_2 = quadrupole_from_pars(beta_2,chi_2)
    
    # Conversion to SI units                                      
    l_dist = luminosity_distance*m_parsec                           
    total_mass = (mass_1 + mass_2)*solar_m

    # Symmetric and antisymmetric parameters
    eta = mass_1*mass_2/(mass_1+mass_2)**2
    delta = (mass_1-mass_2)/(mass_1+mass_2)
    chi_a = (chi_1-chi_2)/2.
    chi_s = (chi_1+chi_2)/2.
    kappa_a = (kappa_1-kappa_2)/2.
    kappa_s = (kappa_1+kappa_2)/2.
    lambda_a = (lambda_1-lambda_2)/2.
    lambda_s = (lambda_1+lambda_2)/2.
    
    # Rescaled frequency
    v = (g_newton/(light_s**3)*np.pi*frequency_array*total_mass)**(1/3.)

    # Phase
    phi = pn_phase(frequency_array, total_mass, eta, chi_s, chi_a, lambda_s, lambda_a, \
                         kappa_s, kappa_a,pn_phase_order=pn_phase_order)
    # Add phases shifts
    t_c=0
    phase_c = -pn_phase(np.array([reference_frequency]), total_mass, eta, chi_s, chi_a, lambda_s, lambda_a, \
                        kappa_s, kappa_a,pn_phase_order=pn_phase_order).astype(np.float)-3/4.*np.pi+2*phase
    phi-= (2*np.pi*frequency_array*t_c-phase_c)
    
    # Amplitude
    amplitude = g_newton/(light_s**3)*total_mass*np.sqrt(5*np.pi/96.) *\
                (g_newton/(light_s**2)*total_mass/l_dist)*np.sqrt(eta)*(v)**(-7/2.)
    
    # Construct frequency-domain plus and cross strain
    ampl_plus = amplitude*(1+np.cos(theta_jn)**2)
    ampl_cross = amplitude*(2*np.cos(theta_jn))

    wave_plus = ampl_plus*(np.cos(phi)+1j*np.sin(phi))
    wave_cross = ampl_cross*(np.sin(phi)-1j*np.cos(phi))
    
    # Implement minimum frequency
    idx = np.where(frequency_array < minimum_frequency)
    wave_plus[idx] = 0. + 1j * 0.
    wave_cross[idx] = 0. + 1j * 0.

    # Returns:A dictionary with plus and cross polarization strain

    waveform = {'plus': wave_plus,
                'cross': wave_cross}

    return waveform
