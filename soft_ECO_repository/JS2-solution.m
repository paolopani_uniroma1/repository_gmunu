(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{-1 + (2*MASS)/r + (\[Epsilon]p^2*(384*MASS^2*Cos[\[Theta]]*
       (2*MASS*(21*M3*MASS*r^3*(4*MASS^5 + 18*MASS^4*r - 140*MASS^3*r^2 + 
            185*MASS^2*r^3 - 90*MASS*r^4 + 15*r^5) + 
          2*J*(-24*MASS^8 + 8*MASS^7*r + 40*MASS^6*r^2 + 104*MASS^5*r^3 + 
            418*MASS^4*r^4 - 3030*MASS^3*r^5 + 3915*MASS^2*r^6 - 
            1890*MASS*r^7 + 315*r^8)*S2) + 15*r^5*(-2*MASS + r)^2*
         (21*M3*MASS*(2*MASS^2 - 3*MASS*r + r^2) + 
          2*J*(44*MASS^2 - 63*MASS*r + 21*r^2)*S2)*Log[1 - (2*MASS)/r]) - 
      1920*MASS^2*(2*MASS - r)*Cos[\[Theta]]^3*
       (2*MASS*(7*M3*MASS*r^3*(2*MASS^4 + 10*MASS^3*r - 65*MASS^2*r^2 + 
            60*MASS*r^3 - 15*r^4) + 2*J*(-4*MASS^7 - 2*MASS^6*r + 
            6*MASS^5*r^2 + 23*MASS^4*r^3 + 61*MASS^3*r^4 - 455*MASS^2*r^5 + 
            420*MASS*r^6 - 105*r^7)*S2) + 3*(2*MASS - r)*r^4*
         (35*M3*MASS*r*(2*MASS^2 - 3*MASS*r + r^2) + 
          2*J*(3*MASS^3 + 70*MASS^2*r - 105*MASS*r^2 + 35*r^3)*S2)*
         Log[1 - (2*MASS)/r]) - 6*(2*MASS - r)*Cos[\[Theta]]^2*
       (128*J^2*MASS^5*(48*MASS^6 - 8*MASS^5*r - 24*MASS^4*r^2 - 
          30*MASS^3*r^3 - 60*MASS^2*r^4 + 135*MASS*r^5 - 45*r^6) - 
        30*MASS*(MASS - r)*r^2*(64*M2*MASS^5*r*(2*MASS^2 + 6*MASS*r - 
            3*r^2) - 168*M4*MASS*r*(4*MASS^4 + 40*MASS^3*r - 440*MASS^2*r^2 + 
            420*MASS*r^3 - 105*r^4) - 5*(32*MASS^5 + 364*MASS^4*r + 
            2160*MASS^3*r^2 - 17652*MASS^2*r^3 + 16452*MASS*r^4 - 4095*r^5)*
           S2^2) + 45*r^3*(-256*M2*MASS^7*r^2 + 256*M2*MASS^6*r^3 - 
          64*M2*MASS^5*r^4 - 64*J^2*MASS^4*r^2*(-2*MASS + r)^2 + 
          840*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) - 
          160*MASS^6*S2^2 - 160*MASS^5*r*S2^2 + 27400*MASS^4*r^2*S2^2 - 
          83040*MASS^3*r^3*S2^2 + 89770*MASS^2*r^4*S2^2 - 
          41190*MASS*r^5*S2^2 + 6825*r^6*S2^2)*Log[1 - (2*MASS)/r] - 
        2700*r^5*(-2*MASS + r)^2*(3*MASS^2 - 5*MASS*r + r^2)*S2^2*
         Log[1 - (2*MASS)/r]^2) + 3*(2*MASS - r)*
       (2*MASS*(-640*M2*MASS^5*r^3*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 
            3*r^3) + 504*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 
            480*MASS^3*r^2 + 860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) + 
          128*J^2*MASS^4*(16*MASS^6 - 8*MASS^5*r - 8*MASS^4*r^2 - 
            10*MASS^3*r^3 - 20*MASS^2*r^4 + 45*MASS*r^5 - 15*r^6) + 
          5*(64*MASS^8 + 32*MASS^7*r + 1452*MASS^5*r^3 + 4844*MASS^4*r^4 - 
            61080*MASS^3*r^5 + 103164*MASS^2*r^6 - 61785*MASS*r^7 + 
            12285*r^8)*S2^2) + 15*r^3*(-512*M2*MASS^7*r^2 + 
          512*M2*MASS^6*r^3 - 128*M2*MASS^5*r^4 - 128*J^2*MASS^4*r^2*
           (-2*MASS + r)^2 + 504*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) - 288*MASS^6*S2^2 + 
          96*MASS^5*r*S2^2 + 17096*MASS^4*r^2*S2^2 - 50896*MASS^3*r^3*S2^2 + 
          54386*MASS^2*r^4*S2^2 - 24810*MASS*r^5*S2^2 + 4095*r^6*S2^2)*
         Log[1 - (2*MASS)/r] - 180*r^5*(60*MASS^4 - 120*MASS^3*r + 
          99*MASS^2*r^2 - 37*MASS*r^3 + 5*r^4)*S2^2*Log[1 - (2*MASS)/r]^2) + 
      5*(2*MASS - r)*Cos[\[Theta]]^4*
       (2*MASS*(3528*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 480*MASS^3*r^2 + 
            860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) - 
          5*(192*MASS^8 + 224*MASS^7*r - 832*MASS^6*r^2 - 5988*MASS^5*r^3 - 
            33396*MASS^4*r^4 + 410832*MASS^3*r^5 - 715932*MASS^2*r^6 + 
            431919*MASS*r^7 - 85995*r^8)*S2^2) + 
        15*(3528*M4*MASS*r^5*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
          r^3*(-96*MASS^6 + 416*MASS^5*r + 110552*MASS^4*r^2 - 
            346704*MASS^3*r^3 + 377574*MASS^2*r^4 - 173286*MASS*r^5 + 
            28665*r^6)*S2^2)*Log[1 - (2*MASS)/r] - 
        180*r^5*(180*MASS^4 - 600*MASS^3*r + 577*MASS^2*r^2 - 215*MASS*r^3 + 
          27*r^4)*S2^2*Log[1 - (2*MASS)/r]^2)))/(6144*MASS^10*r^5*
     (-2*MASS + r)), 0, 0, 
  (\[Epsilon]p*(-16*J*MASS^5 + 5*S2*Cos[\[Theta]]*
      (2*MASS*(2*MASS^3 + 2*MASS^2*r + 3*MASS*r^2 - 3*r^3) + 
       3*(2*MASS - r)*r^3*Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/
   (8*MASS^5*r)}, {0, -(r/(2*MASS - r)) + 
   (\[Epsilon]p^2*(1152*MASS^2*Cos[\[Theta]]*
       (2*MASS*(7*M3*MASS*r^3*(4*MASS^5 + 18*MASS^4*r - 140*MASS^3*r^2 + 
            185*MASS^2*r^3 - 90*MASS*r^4 + 15*r^5) + 
          2*J*(40*MASS^8 - 40*MASS^7*r + 8*MASS^6*r^2 + 28*MASS^5*r^3 + 
            126*MASS^4*r^4 - 980*MASS^3*r^5 + 1295*MASS^2*r^6 - 
            630*MASS*r^7 + 105*r^8)*S2) + 105*(MASS - r)*(2*MASS - r)^3*r^5*
         (M3*MASS + 2*J*S2)*Log[1 - (2*MASS)/r]) - 1920*MASS^2*(2*MASS - r)*
       Cos[\[Theta]]^3*(2*MASS*(7*M3*MASS*r^3*(2*MASS^4 + 10*MASS^3*r - 
            65*MASS^2*r^2 + 60*MASS*r^3 - 15*r^4) + 
          2*J*(20*MASS^7 - 6*MASS^6*r + 2*MASS^5*r^2 + 17*MASS^4*r^3 + 
            67*MASS^3*r^4 - 455*MASS^2*r^5 + 420*MASS*r^6 - 105*r^7)*S2) + 
        3*(2*MASS - r)*r^4*(35*M3*MASS*r*(2*MASS^2 - 3*MASS*r + r^2) + 
          2*J*(MASS^3 + 70*MASS^2*r - 105*MASS*r^2 + 35*r^3)*S2)*
         Log[1 - (2*MASS)/r]) + 18*(2*MASS - r)*Cos[\[Theta]]^2*
       (2*MASS*(64*J^2*MASS^4*(80*MASS^6 - 56*MASS^5*r + 8*MASS^4*r^2 + 
            10*MASS^3*r^3 + 20*MASS^2*r^4 - 45*MASS*r^5 + 15*r^6) + 
          5*(MASS - r)*r^2*(64*M2*MASS^5*r*(2*MASS^2 + 6*MASS*r - 3*r^2) - 
            168*M4*MASS*r*(4*MASS^4 + 40*MASS^3*r - 440*MASS^2*r^2 + 
              420*MASS*r^3 - 105*r^4) - 5*(32*MASS^5 + 364*MASS^4*r + 
              2160*MASS^3*r^2 - 17652*MASS^2*r^3 + 16452*MASS*r^4 - 4095*r^5)*
             S2^2)) - 15*r^3*(-256*M2*MASS^7*r^2 + 256*M2*MASS^6*r^3 - 
          64*M2*MASS^5*r^4 - 64*J^2*MASS^4*r^2*(-2*MASS + r)^2 + 
          840*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) - 
          160*MASS^6*S2^2 - 160*MASS^5*r*S2^2 + 27400*MASS^4*r^2*S2^2 - 
          83040*MASS^3*r^3*S2^2 + 89770*MASS^2*r^4*S2^2 - 
          41190*MASS*r^5*S2^2 + 6825*r^6*S2^2)*Log[1 - (2*MASS)/r] + 
        900*r^5*(-2*MASS + r)^2*(3*MASS^2 - 5*MASS*r + r^2)*S2^2*
         Log[1 - (2*MASS)/r]^2) - 3*(2*MASS - r)*
       (2*MASS*(640*M2*MASS^5*r^3*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 
            3*r^3) - 504*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 
            480*MASS^3*r^2 + 860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) + 
          128*J^2*MASS^4*(80*MASS^6 - 72*MASS^5*r + 8*MASS^4*r^2 + 
            10*MASS^3*r^3 + 20*MASS^2*r^4 - 45*MASS*r^5 + 15*r^6) + 
          5*(320*MASS^8 - 96*MASS^7*r - 384*MASS^6*r^2 - 2156*MASS^5*r^3 - 
            6860*MASS^4*r^4 + 61320*MASS^3*r^5 - 101436*MASS^2*r^6 + 
            61065*MASS*r^7 - 12285*r^8)*S2^2) - 
        15*r^3*(-512*M2*MASS^7*r^2 + 512*M2*MASS^6*r^3 - 128*M2*MASS^5*r^4 - 
          128*J^2*MASS^4*r^2*(-2*MASS + r)^2 + 504*M4*MASS*r^2*
           (-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) - 288*MASS^6*S2^2 - 
          32*MASS^5*r*S2^2 + 18184*MASS^4*r^2*S2^2 - 50064*MASS^3*r^3*S2^2 + 
          52754*MASS^2*r^4*S2^2 - 24330*MASS*r^5*S2^2 + 4095*r^6*S2^2)*
         Log[1 - (2*MASS)/r] + 180*r^5*(60*MASS^4 - 120*MASS^3*r + 
          51*MASS^2*r^2 + 7*MASS*r^3 - 5*r^4)*S2^2*Log[1 - (2*MASS)/r]^2) + 
      5*(2*MASS - r)*Cos[\[Theta]]^4*
       (2*MASS*(3528*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 480*MASS^3*r^2 + 
            860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) + 
          5*(960*MASS^8 + 96*MASS^7*r + 448*MASS^6*r^2 + 4516*MASS^5*r^3 + 
            33492*MASS^4*r^4 - 410304*MASS^3*r^5 + 718812*MASS^2*r^6 - 
            433503*MASS*r^7 + 85995*r^8)*S2^2) + 
        15*(3528*M4*MASS*r^5*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
          r^3*(-96*MASS^6 - 480*MASS^5*r + 109976*MASS^4*r^2 - 
            347920*MASS^3*r^3 + 380550*MASS^2*r^4 - 174342*MASS*r^5 + 
            28665*r^6)*S2^2)*Log[1 - (2*MASS)/r] - 
        180*r^5*(180*MASS^4 - 600*MASS^3*r + 657*MASS^2*r^2 - 299*MASS*r^3 + 
          49*r^4)*S2^2*Log[1 - (2*MASS)/r]^2)))/(6144*MASS^10*r^3*
     (-2*MASS + r)^3), 0, 0}, 
 {0, 0, r^2 + (\[Epsilon]p^2*(2688*MASS^2*Cos[\[Theta]]*
       (-3 + 5*Cos[\[Theta]]^2)*
       (2*MASS*(-7*M3*MASS*r^3*(2*MASS^3 - 10*MASS^2*r - 15*MASS*r^2 + 
            15*r^3) + 2*J*(4*MASS^6 + 8*MASS^5*r + 6*MASS^4*r^2 - 
            8*MASS^3*r^3 + 70*MASS^2*r^4 + 105*MASS*r^5 - 105*r^6)*S2) - 
        3*r^4*(7*M3*MASS*(4*MASS^3 - 10*MASS*r^2 + 5*r^3) + 
          2*J*(26*MASS^3 - 70*MASS*r^2 + 35*r^3)*S2)*Log[1 - (2*MASS)/r]) + 
      12*(-1 + 3*Cos[\[Theta]]^2)*(-448*J^2*MASS^5*(16*MASS^5 + 8*MASS^4*r - 
          10*MASS^2*r^3 + 15*MASS*r^4 + 15*r^5) + 
        20*MASS^2*(112*M2*MASS^4*r^3*(2*MASS^2 - 3*MASS*r - 3*r^2) - 
          5*(16*MASS^6 + 24*MASS^5*r + 8*MASS^4*r^2 + 130*MASS^3*r^3 - 
            84*MASS^2*r^4 - 159*MASS*r^5 - 63*r^6)*S2^2) + 
        30*MASS*r^3*(112*J^2*(2*MASS^5*r - MASS^3*r^3) + 
          112*M2*(2*MASS^6*r - MASS^4*r^3) + 5*(48*MASS^4 - 122*MASS^3*r - 
            64*MASS^2*r^2 + 29*MASS*r^3 + 42*r^4)*S2^2)*Log[1 - (2*MASS)/r] + 
        225*r^4*(24*MASS^4 - 12*MASS^2*r^2 - 8*MASS*r^3 + 7*r^4)*S2^2*
         Log[1 - (2*MASS)/r]^2) + (3 - 30*Cos[\[Theta]]^2 + 
        35*Cos[\[Theta]]^4)*(-2*MASS*(-1176*M4*MASS*r^3*(12*MASS^4 - 
            130*MASS^3*r - 270*MASS^2*r^2 + 735*MASS*r^3 - 315*r^4) + 
          5*(192*MASS^7 + 736*MASS^6*r + 880*MASS^5*r^2 - 2724*MASS^4*r^3 + 
            36666*MASS^3*r^4 + 79110*MASS^2*r^5 - 204183*MASS*r^6 + 
            85995*r^7)*S2^2) + 15*r^3*(1176*M4*MASS*r*(8*MASS^4 - 
            60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
          (-96*MASS^5 + 11752*MASS^4*r + 2032*MASS^3*r^2 - 87852*MASS^2*r^3 + 
            97902*MASS*r^4 - 28665*r^5)*S2^2)*Log[1 - (2*MASS)/r] + 
        180*r^4*(-20*MASS^4 + 150*MASS^2*r^2 - 173*MASS*r^3 + 49*r^4)*S2^2*
         Log[1 - (2*MASS)/r]^2)))/(43008*MASS^10*r^2), 0}, 
 {(\[Epsilon]p*(-16*J*MASS^5 + 5*S2*Cos[\[Theta]]*
      (2*MASS*(2*MASS^3 + 2*MASS^2*r + 3*MASS*r^2 - 3*r^3) + 
       3*(2*MASS - r)*r^3*Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/
   (8*MASS^5*r), 0, 0, r^2*Sin[\[Theta]]^2 + 
   (\[Epsilon]p^2*(2688*MASS^2*Cos[\[Theta]]*(-3 + 5*Cos[\[Theta]]^2)*
       (2*MASS*(-7*M3*MASS*r^3*(2*MASS^3 - 10*MASS^2*r - 15*MASS*r^2 + 
            15*r^3) + 2*J*(4*MASS^6 + 8*MASS^5*r + 6*MASS^4*r^2 - 
            8*MASS^3*r^3 + 70*MASS^2*r^4 + 105*MASS*r^5 - 105*r^6)*S2) - 
        3*r^4*(7*M3*MASS*(4*MASS^3 - 10*MASS*r^2 + 5*r^3) + 
          2*J*(26*MASS^3 - 70*MASS*r^2 + 35*r^3)*S2)*Log[1 - (2*MASS)/r]) + 
      12*(-1 + 3*Cos[\[Theta]]^2)*(-448*J^2*MASS^5*(16*MASS^5 + 8*MASS^4*r - 
          10*MASS^2*r^3 + 15*MASS*r^4 + 15*r^5) + 
        20*MASS^2*(112*M2*MASS^4*r^3*(2*MASS^2 - 3*MASS*r - 3*r^2) - 
          5*(16*MASS^6 + 24*MASS^5*r + 8*MASS^4*r^2 + 130*MASS^3*r^3 - 
            84*MASS^2*r^4 - 159*MASS*r^5 - 63*r^6)*S2^2) + 
        30*MASS*r^3*(112*J^2*(2*MASS^5*r - MASS^3*r^3) + 
          112*M2*(2*MASS^6*r - MASS^4*r^3) + 5*(48*MASS^4 - 122*MASS^3*r - 
            64*MASS^2*r^2 + 29*MASS*r^3 + 42*r^4)*S2^2)*Log[1 - (2*MASS)/r] + 
        225*r^4*(24*MASS^4 - 12*MASS^2*r^2 - 8*MASS*r^3 + 7*r^4)*S2^2*
         Log[1 - (2*MASS)/r]^2) + (3 - 30*Cos[\[Theta]]^2 + 
        35*Cos[\[Theta]]^4)*(-2*MASS*(-1176*M4*MASS*r^3*(12*MASS^4 - 
            130*MASS^3*r - 270*MASS^2*r^2 + 735*MASS*r^3 - 315*r^4) + 
          5*(192*MASS^7 + 736*MASS^6*r + 880*MASS^5*r^2 - 2724*MASS^4*r^3 + 
            36666*MASS^3*r^4 + 79110*MASS^2*r^5 - 204183*MASS*r^6 + 
            85995*r^7)*S2^2) + 15*r^3*(1176*M4*MASS*r*(8*MASS^4 - 
            60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
          (-96*MASS^5 + 11752*MASS^4*r + 2032*MASS^3*r^2 - 87852*MASS^2*r^3 + 
            97902*MASS*r^4 - 28665*r^5)*S2^2)*Log[1 - (2*MASS)/r] + 
        180*r^4*(-20*MASS^4 + 150*MASS^2*r^2 - 173*MASS*r^3 + 49*r^4)*S2^2*
         Log[1 - (2*MASS)/r]^2))*Sin[\[Theta]]^2)/(43008*MASS^10*r^2)}}
