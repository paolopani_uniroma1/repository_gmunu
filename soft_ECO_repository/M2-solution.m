(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{-1 + (2*MASS)/r - (5*M2*\[Epsilon]p*(-1 + 3*Cos[\[Theta]]^2)*
     (2*MASS*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + 
      3*r^2*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/(16*MASS^5*r^2) + 
   (\[Epsilon]p^2*(-28672*MASS^10*(2*MASS - r)*r^2*\[Delta]M20 - 
      4480*M22*MASS^5*(2*MASS - r)*r*(1 - 3*Cos[\[Theta]]^2)*
       (2*MASS*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + 
        3*r^2*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]) + 
      112*(4*MASS^2*(5*M2^2*(12*MASS^6 - 20*MASS^5*r - 20*MASS^4*r^2 + 
            90*MASS^3*r^3 - 3*MASS^2*r^4 - 36*MASS*r^5 + 9*r^6) + 
          64*MASS^8*(2*MASS - r)*r^2*\[Delta]M20) + 60*M2^2*MASS*r*
         (8*MASS^6 + 8*MASS^5*r - 62*MASS^4*r^2 + 44*MASS^3*r^3 + 
          10*MASS^2*r^4 - 15*MASS*r^5 + 3*r^6)*Log[1 - (2*MASS)/r] + 
        45*M2^2*r^3*(-2*MASS + r)^4*(2*MASS + r)*Log[1 - (2*MASS)/r]^2) + 
      40*(1 - 3*Cos[\[Theta]]^2)*(2*MASS^2*(MASS - r)*
         (112*M22*MASS^4*r*(4*MASS^3 + 10*MASS^2*r - 12*MASS*r^2 + 3*r^3) - 
          5*M2^2*(8*MASS^5 + 380*MASS^4*r + 390*MASS^3*r^2 - 780*MASS^2*r^3 + 
            315*MASS*r^4 - 36*r^5)) + 3*MASS*(2*MASS - r)*r*
         (112*M22*MASS^4*r^2*(-2*MASS + r)^2 + 5*M2^2*(32*MASS^5 - 
            112*MASS^4*r + 52*MASS^3*r^2 + 116*MASS^2*r^3 - 105*MASS*r^4 + 
            24*r^5))*Log[1 - (2*MASS)/r] - 90*M2^2*(2*MASS - r)^3*r^3*
         (5*MASS^2 - 3*MASS*r - r^2)*Log[1 - (2*MASS)/r]^2) + 
      (3 - 30*Cos[\[Theta]]^2 + 35*Cos[\[Theta]]^4)*
       (6*MASS*(MASS - r)*(-392*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 
            920*MASS^3*r^2 + 1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
          5*M2^2*(160*MASS^6 + 2056*MASS^5*r + 12476*MASS^4*r^2 - 
            114664*MASS^3*r^3 + 154336*MASS^2*r^4 - 75186*MASS*r^5 + 
            12495*r^6)) - 15*(2*MASS - r)*r*(1176*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(352*MASS^6 + 672*MASS^5*r - 
            51816*MASS^4*r^2 + 153456*MASS^3*r^3 - 164682*MASS^2*r^4 + 
            75402*MASS*r^5 - 12495*r^6))*Log[1 - (2*MASS)/r] - 
        540*M2^2*(2*MASS - r)^3*r^3*(13*MASS^2 - 19*MASS*r + 3*r^2)*
         Log[1 - (2*MASS)/r]^2)))/(14336*MASS^10*(2*MASS - r)*r^3) + 
   (\[Epsilon]p^3*(2*MASS*(30030*M6*MASS^2*r^2*(-2*MASS + r)^2*
         (16*MASS^7 + 320*MASS^6*r - 11424*MASS^5*r^2 + 49728*MASS^4*r^3 - 
          82950*MASS^3*r^4 + 65100*MASS^2*r^5 - 24255*MASS*r^6 + 3465*r^7) - 
        42*M2*M4*MASS*r*(35840*MASS^10 - 97984*MASS^9*r + 
          5482368*MASS^8*r^2 - 132409040*MASS^7*r^3 + 683399776*MASS^6*r^4 - 
          1519332360*MASS^5*r^5 + 1803833640*MASS^4*r^6 - 
          1237905270*MASS^3*r^7 + 494104380*MASS^2*r^8 - 106796025*MASS*r^9 + 
          9684675*r^10) + 5*M2^3*(132480*MASS^11 - 454400*MASS^10*r - 
          261696*MASS^9*r^2 + 49063952*MASS^8*r^3 - 955305000*MASS^7*r^4 + 
          4908589904*MASS^6*r^5 - 10969052680*MASS^5*r^6 + 
          13061786380*MASS^4*r^7 - 8970448500*MASS^3*r^8 + 
          3578066660*MASS^2*r^9 - 772216725*MASS*r^10 + 69894825*r^11)) - 
      15*(2*MASS - r)*r*(-210210*M6*MASS^2*(2*MASS - r)^3*r^3*
         (16*MASS^4 - 96*MASS^3*r + 180*MASS^2*r^2 - 132*MASS*r^3 + 33*r^4) + 
        294*M2*M4*MASS*r*(1024*MASS^9 - 8448*MASS^8*r + 443136*MASS^7*r^2 - 
          3019072*MASS^6*r^3 + 8080224*MASS^5*r^4 - 11131312*MASS^4*r^5 + 
          8677488*MASS^3*r^6 - 3876816*MASS^2*r^7 + 927390*MASS*r^8 - 
          92235*r^9) - 5*M2^3*(27136*MASS^10 - 6912*MASS^9*r - 
          707584*MASS^8*r^2 + 25631936*MASS^7*r^3 - 165135312*MASS^6*r^4 + 
          429585792*MASS^5*r^5 - 580835416*MASS^4*r^6 + 446962800*MASS^3*
           r^7 - 197893836*MASS^2*r^8 + 47046370*MASS*r^9 - 4659655*r^10))*
       Log[1 - (2*MASS)/r] + 225*M2*r^2*(-2*MASS + r)^2*
       (1176*M4*MASS*r^2*(-2*MASS + r)^2*(40*MASS^4 - 272*MASS^3*r + 
          396*MASS^2*r^2 - 182*MASS*r^3 + 21*r^4) + 
        M2^2*(1856*MASS^8 - 320*MASS^7*r - 314784*MASS^6*r^2 + 
          2317936*MASS^5*r^3 - 4941376*MASS^4*r^4 + 4657384*MASS^3*r^5 - 
          2161848*MASS^2*r^6 + 474138*MASS*r^7 - 37485*r^8))*
       Log[1 - (2*MASS)/r]^2 - 900*M2^3*r^4*(-2*MASS + r)^4*
       (-280*MASS^4 + 2108*MASS^3*r - 2292*MASS^2*r^2 + 491*MASS*r^3 + 
        28*r^4)*Log[1 - (2*MASS)/r]^3 - 105*(2*MASS - r)*Cos[\[Theta]]^2*
       (2*MASS*(6006*M6*MASS^2*r^2*(32*MASS^8 + 624*MASS^7*r - 
            23168*MASS^6*r^2 + 110880*MASS^5*r^3 - 215628*MASS^4*r^4 + 
            213150*MASS^3*r^5 - 113610*MASS^2*r^6 + 31185*MASS*r^7 - 
            3465*r^8) - 42*M2*M4*MASS*r*(1664*MASS^9 + 30688*MASS^8*r + 
            502128*MASS^7*r^2 - 14305600*MASS^6*r^3 + 65017792*MASS^5*r^4 - 
            123563892*MASS^4*r^5 + 120659850*MASS^3*r^6 - 63870870*MASS^2*
             r^7 + 17465175*MASS*r^8 - 1936935*r^9) + 
          5*M2^3*(2816*MASS^10 + 18784*MASS^9*r + 281952*MASS^8*r^2 + 
            4573352*MASS^7*r^3 - 110733904*MASS^6*r^4 + 485686872*MASS^5*
             r^5 - 908103476*MASS^4*r^6 + 878969680*MASS^3*r^7 - 
            462937294*MASS^2*r^8 + 126228375*MASS*r^9 - 13978965*r^10)) + 
        3*(2*MASS - r)*r*(210210*M6*MASS^2*r^3*(-2*MASS + r)^2*
           (16*MASS^4 - 96*MASS^3*r + 180*MASS^2*r^2 - 132*MASS*r^3 + 
            33*r^4) + 42*M2*M4*MASS*r*(1280*MASS^8 + 19840*MASS^7*r - 
            1767168*MASS^6*r^2 + 10422464*MASS^5*r^3 - 23927200*MASS^4*r^4 + 
            27408320*MASS^3*r^5 - 16706760*MASS^2*r^6 + 5187000*MASS*r^7 - 
            645645*r^8) - 5*M2^3*(128*MASS^9 + 21504*MASS^8*r + 
            313632*MASS^7*r^2 - 15864656*MASS^6*r^3 + 84631280*MASS^5*r^4 - 
            183856648*MASS^4*r^5 + 203923496*MASS^3*r^6 - 122067604*MASS^2*
             r^7 + 37555700*MASS*r^8 - 4659655*r^9))*Log[1 - (2*MASS)/r] + 
        45*M2*(2*MASS - r)*r^2*(168*M4*MASS*r^2*(-2*MASS + r)^2*
           (448*MASS^4 - 2032*MASS^3*r + 2584*MASS^2*r^2 - 1078*MASS*r^3 + 
            91*r^4) + M2^2*(320*MASS^8 + 3840*MASS^7*r - 529152*MASS^6*r^2 + 
            2691824*MASS^5*r^3 - 4952368*MASS^4*r^4 + 4294096*MASS^3*r^5 - 
            1854180*MASS^2*r^6 + 367254*MASS*r^7 - 23205*r^8))*
         Log[1 - (2*MASS)/r]^2 + 180*M2^3*(2*MASS - r)^3*r^4*
         (720*MASS^4 - 2320*MASS^3*r + 1690*MASS^2*r^2 - 227*MASS*r^3 - 
          19*r^4)*Log[1 - (2*MASS)/r]^3) - 7*Cos[\[Theta]]^6*
       (2*MASS*(MASS - r)*(198198*M6*MASS^2*r^2*(-2*MASS + r)^2*
           (16*MASS^6 + 336*MASS^5*r - 11088*MASS^4*r^2 + 38640*MASS^3*r^3 - 
            44310*MASS^2*r^4 + 20790*MASS*r^5 - 3465*r^6) - 
          42*M2*M4*MASS*r*(116480*MASS^9 + 3147328*MASS^8*r + 
            35329600*MASS^7*r^2 - 983793520*MASS^6*r^3 + 3951098256*MASS^5*
             r^4 - 6554037336*MASS^4*r^5 + 5610311280*MASS^3*r^6 - 
            2625548310*MASS^2*r^7 + 640511550*MASS*r^8 - 63918855*r^9) + 
          5*M2^3*(111360*MASS^10 + 2568000*MASS^9*r + 38829152*MASS^8*r^2 + 
            341501200*MASS^7*r^3 - 7932223480*MASS^6*r^4 + 30075484584*MASS^5*
             r^5 - 48576047304*MASS^4*r^6 + 41010063420*MASS^3*r^7 - 
            19050620640*MASS^2*r^8 + 4629926700*MASS*r^9 - 461305845*r^10)) + 
        15*(2*MASS - r)*r*(1387386*M6*MASS^2*(2*MASS - r)^3*r^3*
           (16*MASS^4 - 96*MASS^3*r + 180*MASS^2*r^2 - 132*MASS*r^3 + 
            33*r^4) + 294*M2*M4*MASS*r*(4608*MASS^9 + 21504*MASS^8*r - 
            3430784*MASS^7*r^2 + 21729792*MASS^6*r^3 - 55630656*MASS^5*r^4 + 
            74804416*MASS^4*r^5 - 57589488*MASS^3*r^6 + 25586616*MASS^2*r^7 - 
            6112710*MASS*r^8 + 608751*r^9) - 5*M2^3*(35072*MASS^10 + 
            650880*MASS^9*r + 2618304*MASS^8*r^2 - 221781952*MASS^7*r^3 + 
            1264831536*MASS^6*r^4 - 3051743808*MASS^5*r^5 + 
            3957475928*MASS^4*r^6 - 2979420624*MASS^3*r^7 + 
            1306199628*MASS^2*r^8 - 309786330*MASS*r^9 + 30753723*r^10))*
         Log[1 - (2*MASS)/r] + 225*M2*r^2*(-2*MASS + r)^2*
         (1176*M4*MASS*r^2*(-2*MASS + r)^2*(368*MASS^4 - 1584*MASS^3*r + 
            2064*MASS^2*r^2 - 938*MASS*r^3 + 105*r^4) + 
          M2^2*(7744*MASS^8 + 12416*MASS^7*r - 3119040*MASS^6*r^2 + 
            15080880*MASS^5*r^3 - 27628080*MASS^4*r^4 + 24550080*MASS^3*r^5 - 
            11150700*MASS^2*r^6 + 2420790*MASS*r^7 - 187425*r^8))*
         Log[1 - (2*MASS)/r]^2 - 4500*M2^3*r^4*(-2*MASS + r)^4*
         (-752*MASS^4 + 2232*MASS^3*r - 1710*MASS^2*r^2 + 243*MASS*r^3 + 
          27*r^4)*Log[1 - (2*MASS)/r]^3) + 35*Cos[\[Theta]]^4*
       (2*MASS*(MASS - r)*(54054*M6*MASS^2*r^2*(-2*MASS + r)^2*
           (16*MASS^6 + 336*MASS^5*r - 11088*MASS^4*r^2 + 38640*MASS^3*r^3 - 
            44310*MASS^2*r^4 + 20790*MASS*r^5 - 3465*r^6) - 
          42*M2*M4*MASS*r*(28160*MASS^9 + 773696*MASS^8*r + 
            9455808*MASS^7*r^2 - 262784272*MASS^6*r^3 + 1066277648*MASS^5*
             r^4 - 1778136408*MASS^4*r^5 + 1526355120*MASS^3*r^6 - 
            715360590*MASS^2*r^7 + 174639150*MASS*r^8 - 17432415*r^9) + 
          5*M2^3*(28800*MASS^10 + 514560*MASS^9*r + 8737664*MASS^8*r^2 + 
            89426832*MASS^7*r^3 - 2087250808*MASS^6*r^4 + 8052370872*MASS^5*
             r^5 - 13125938272*MASS^4*r^6 + 11136163020*MASS^3*r^7 - 
            5186628840*MASS^2*r^8 + 1262123100*MASS*r^9 - 125810685*r^10)) + 
        15*(2*MASS - r)*r*(378378*M6*MASS^2*(2*MASS - r)^3*r^3*
           (16*MASS^4 - 96*MASS^3*r + 180*MASS^2*r^2 - 132*MASS*r^3 + 
            33*r^4) + 42*M2*M4*MASS*r*(8192*MASS^9 + 51456*MASS^8*r - 
            6541824*MASS^7*r^2 + 41322816*MASS^6*r^3 - 105827296*MASS^5*r^4 + 
            142416944*MASS^4*r^5 - 109733840*MASS^3*r^6 + 48790560*MASS^2*
             r^7 - 11663610*MASS*r^8 + 1162161*r^9) - 
          5*M2^3*(6656*MASS^10 + 152576*MASS^9*r + 878208*MASS^8*r^2 - 
            60143808*MASS^7*r^3 + 342308816*MASS^6*r^4 - 826952128*MASS^5*
             r^5 + 1074069912*MASS^4*r^6 - 809834608*MASS^3*r^7 + 
            355508156*MASS^2*r^8 - 84409290*MASS*r^9 + 8387379*r^10))*
         Log[1 - (2*MASS)/r] + 225*M2*r^2*(-2*MASS + r)^2*
         (168*M4*MASS*r^2*(-2*MASS + r)^2*(776*MASS^4 - 3344*MASS^3*r + 
            4252*MASS^2*r^2 - 1834*MASS*r^3 + 175*r^4) + 
          M2^2*(1600*MASS^8 + 4928*MASS^7*r - 931808*MASS^6*r^2 + 
            4519600*MASS^5*r^3 - 8196480*MASS^4*r^4 + 7136568*MASS^3*r^5 - 
            3138192*MASS^2*r^6 + 645306*MASS*r^7 - 44625*r^8))*
         Log[1 - (2*MASS)/r]^2 - 900*M2^3*r^4*(-2*MASS + r)^4*
         (-1176*MASS^4 + 3572*MASS^3*r - 2632*MASS^2*r^2 + 351*MASS*r^3 + 
          36*r^4)*Log[1 - (2*MASS)/r]^3)))/(229376*MASS^15*r^4*
     (-2*MASS + r)^2), 0, 0, 0}, 
 {0, -(r/(2*MASS - r)) - (5*M2*\[Epsilon]p*(-1 + 3*Cos[\[Theta]]^2)*
     (2*MASS*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + 
      3*r^2*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/
    (16*MASS^5*(-2*MASS + r)^2) + 
   (\[Epsilon]p^2*(2*MASS*(MASS - r)*(504*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 
          920*MASS^3*r^2 + 1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
        5*M2^2*(224*MASS^6 + 3112*MASS^5*r - 8436*MASS^4*r^2 + 
          135096*MASS^3*r^3 - 195192*MASS^2*r^4 + 96750*MASS*r^5 - 
          16065*r^6)) - 15*(2*MASS - r)*r*(-504*M4*MASS*r^2*(-2*MASS + r)^2*
         (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(288*MASS^6 - 1312*MASS^5*r + 
          18024*MASS^4*r^2 - 59984*MASS^3*r^3 + 69018*MASS^2*r^4 - 
          32370*MASS*r^5 + 5355*r^6))*Log[1 - (2*MASS)/r] + 
      180*M2^2*(2*MASS - r)^3*r^3*(15*MASS^2 - MASS*r + 5*r^2)*
       Log[1 - (2*MASS)/r]^2 - 15*Cos[\[Theta]]^4*
       (2*MASS*(MASS - r)*(-392*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 
            920*MASS^3*r^2 + 1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
          5*M2^2*(32*MASS^6 + 1416*MASS^5*r + 12476*MASS^4*r^2 - 
            112744*MASS^3*r^3 + 152896*MASS^2*r^4 - 74898*MASS*r^5 + 
            12495*r^6)) - 5*(2*MASS - r)*r*(1176*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(352*MASS^6 + 
            1440*MASS^5*r - 50664*MASS^4*r^2 + 149232*MASS^3*r^3 - 
            161802*MASS^2*r^4 + 74826*MASS*r^5 - 12495*r^6))*
         Log[1 - (2*MASS)/r] - 180*M2^2*(2*MASS - r)^3*r^3*
         (13*MASS^2 - 11*MASS*r - r^2)*Log[1 - (2*MASS)/r]^2) - 
      30*r*Cos[\[Theta]]^2*(2*MASS*(MASS - r)*
         (168*M4*MASS*(8*MASS^5 + 76*MASS^4*r - 920*MASS^3*r^2 + 
            1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) - 
          5*M2^2*(344*MASS^5 + 5124*MASS^4*r - 47736*MASS^3*r^2 + 
            65244*MASS^2*r^3 - 32058*MASS*r^4 + 5355*r^5)) + 
        15*(2*MASS - r)*(168*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(32*MASS^6 + 288*MASS^5*r - 
            7240*MASS^4*r^2 + 21152*MASS^3*r^3 - 22986*MASS^2*r^4 + 
            10662*MASS*r^5 - 1785*r^6))*Log[1 - (2*MASS)/r] + 
        180*M2^2*(2*MASS - r)^3*r^2*(7*MASS^2 - 5*MASS*r - r^2)*
         Log[1 - (2*MASS)/r]^2)))/(2048*MASS^10*r*(-2*MASS + r)^3) + 
   (\[Epsilon]p^3*(2*MASS*(30030*M6*MASS^2*r^2*(-2*MASS + r)^2*
         (16*MASS^7 + 320*MASS^6*r - 11424*MASS^5*r^2 + 49728*MASS^4*r^3 - 
          82950*MASS^3*r^4 + 65100*MASS^2*r^5 - 24255*MASS*r^6 + 3465*r^7) + 
        42*M2*M4*MASS*r*(8960*MASS^10 + 568384*MASS^9*r - 
          10499968*MASS^8*r^2 + 131927440*MASS^7*r^3 - 643785376*MASS^6*r^4 + 
          1438249960*MASS^5*r^5 - 1729577640*MASS^4*r^6 + 
          1202163270*MASS^3*r^7 - 485284380*MASS^2*r^8 + 105914025*MASS*r^9 - 
          9684675*r^10) - 5*M2^3*(12160*MASS^11 - 284480*MASS^10*r + 
          3363616*MASS^9*r^2 - 109730832*MASS^8*r^3 + 955425240*MASS^7*r^4 - 
          4408544384*MASS^6*r^5 + 9928511440*MASS^5*r^6 - 
          12106429900*MASS^4*r^7 + 8511532560*MASS^3*r^8 - 
          3465218540*MASS^2*r^9 + 760971225*MASS*r^10 - 69894825*r^11)) - 
      15*(2*MASS - r)*r*(-210210*M6*MASS^2*(2*MASS - r)^3*r^3*
         (16*MASS^4 - 96*MASS^3*r + 180*MASS^2*r^2 - 132*MASS*r^3 + 33*r^4) + 
        294*M2*M4*MASS*r*(1024*MASS^9 - 28928*MASS^8*r + 458496*MASS^7*r^2 - 
          2711872*MASS^6*r^3 + 7221344*MASS^5*r^4 - 10168112*MASS^4*r^5 + 
          8136688*MASS^3*r^6 - 3725616*MASS^2*r^7 + 910590*MASS*r^8 - 
          92235*r^9) + 5*M2^3*(11008*MASS^10 + 18688*MASS^9*r + 
          2196480*MASS^8*r^2 - 26810816*MASS^7*r^3 + 137449648*MASS^6*r^4 - 
          351688992*MASS^5*r^5 + 493500088*MASS^4*r^6 - 398133936*MASS^3*
           r^7 + 184320612*MASS^2*r^8 - 45546970*MASS*r^9 + 4659655*r^10))*
       Log[1 - (2*MASS)/r] + 225*M2*r^2*(-2*MASS + r)^2*
       (1176*M4*MASS*r^2*(-2*MASS + r)^2*(40*MASS^4 - 152*MASS^3*r + 
          56*MASS^2*r^2 + 98*MASS*r^3 - 49*r^4) + 
        M2^2*(1856*MASS^8 - 2304*MASS^7*r - 272384*MASS^6*r^2 + 
          1340752*MASS^5*r^3 - 1458336*MASS^4*r^4 - 166880*MASS^3*r^5 + 
          1024692*MASS^2*r^6 - 538566*MASS*r^7 + 87465*r^8))*
       Log[1 - (2*MASS)/r]^2 - 900*M2^3*r^4*(-2*MASS + r)^4*
       (-280*MASS^4 + 56*MASS^3*r + 570*MASS^2*r^2 - 1231*MASS*r^3 + 574*r^4)*
       Log[1 - (2*MASS)/r]^3 + 35*Cos[\[Theta]]^4*
       (2*MASS*(MASS - r)*(54054*M6*MASS^2*r^2*(-2*MASS + r)^2*
           (16*MASS^6 + 336*MASS^5*r - 11088*MASS^4*r^2 + 38640*MASS^3*r^3 - 
            44310*MASS^2*r^4 + 20790*MASS*r^5 - 3465*r^6) - 
          42*M2*M4*MASS*r*(3840*MASS^9 + 494016*MASS^8*r + 
            11899968*MASS^7*r^2 - 260078672*MASS^6*r^3 + 1047478288*MASS^5*
             r^4 - 1752919608*MASS^4*r^5 + 1511261520*MASS^3*r^6 - 
            711051390*MASS^2*r^7 + 174160350*MASS*r^8 - 17432415*r^9) + 
          5*M2^3*(1920*MASS^10 + 10560*MASS^9*r + 4001504*MASS^8*r^2 + 
            120287472*MASS^7*r^3 - 2046468808*MASS^6*r^4 + 7805468952*MASS^5*
             r^5 - 12801313672*MASS^4*r^6 + 10943214180*MASS^3*r^7 - 
            5131673580*MASS^2*r^8 + 1256018400*MASS*r^9 - 125810685*r^10)) + 
        15*(2*MASS - r)*r*(378378*M6*MASS^2*(2*MASS - r)^3*r^3*
           (16*MASS^4 - 96*MASS^3*r + 180*MASS^2*r^2 - 132*MASS*r^3 + 
            33*r^4) + 42*M2*M4*MASS*r*(8192*MASS^9 + 129280*MASS^8*r - 
            6600192*MASS^7*r^2 + 40155456*MASS^6*r^3 - 102563552*MASS^5*r^4 + 
            138756784*MASS^4*r^5 - 107678800*MASS^3*r^6 + 48216000*MASS^2*
             r^7 - 11599770*MASS*r^8 + 1162161*r^9) - 
          5*M2^3*(768*MASS^10 + 116224*MASS^9*r + 2040960*MASS^8*r^2 - 
            60405696*MASS^7*r^3 + 325735856*MASS^6*r^4 - 783683680*MASS^5*
             r^5 + 1026718584*MASS^4*r^6 - 783523120*MASS^3*r^7 + 
            348179924*MASS^2*r^8 - 83595330*MASS*r^9 + 8387379*r^10))*
         Log[1 - (2*MASS)/r] + 225*M2*r^2*(-2*MASS + r)^2*
         (168*M4*MASS*r^2*(-2*MASS + r)^2*(776*MASS^4 - 2888*MASS^3*r + 
            2960*MASS^2*r^2 - 770*MASS*r^3 - 91*r^4) + 
          M2^2*(1600*MASS^8 + 15744*MASS^7*r - 914752*MASS^6*r^2 + 
            3899536*MASS^5*r^3 - 6205152*MASS^4*r^4 + 4512048*MASS^3*r^5 - 
            1432836*MASS^2*r^6 + 102234*MASS*r^7 + 23205*r^8))*
         Log[1 - (2*MASS)/r]^2 + 900*M2^3*r^4*(-2*MASS + r)^4*
         (1176*MASS^4 - 2096*MASS^3*r + 346*MASS^2*r^2 + 459*MASS*r^3 - 
          54*r^4)*Log[1 - (2*MASS)/r]^3) - 105*Cos[\[Theta]]^2*
       (2*MASS*(6006*M6*MASS^2*r^2*(-2*MASS + r)^2*(16*MASS^7 + 
            320*MASS^6*r - 11424*MASS^5*r^2 + 49728*MASS^4*r^3 - 
            82950*MASS^3*r^4 + 65100*MASS^2*r^5 - 24255*MASS*r^6 + 
            3465*r^7) - 42*M2*M4*MASS*r^2*(24768*MASS^9 + 1346304*MASS^8*r - 
            29077552*MASS^7*r^2 + 141398400*MASS^6*r^3 - 306122312*MASS^5*
             r^4 + 359367432*MASS^4*r^5 - 245746470*MASS^3*r^6 + 
            98146020*MASS^2*r^7 - 21273525*MASS*r^8 + 1936935*r^9) + 
          5*M2^3*(1792*MASS^11 + 2176*MASS^10*r + 46016*MASS^9*r^2 + 
            13494768*MASS^8*r^3 - 224849752*MASS^7*r^4 + 1043445520*MASS^6*
             r^5 - 2224251352*MASS^5*r^6 + 2595384340*MASS^4*r^7 - 
            1770929280*MASS^3*r^8 + 707035924*MASS^2*r^9 - 
            153350925*MASS*r^10 + 13978965*r^11)) + 3*(2*MASS - r)*r^2*
         (210210*M6*MASS^2*(2*MASS - r)^3*r^2*(16*MASS^4 - 96*MASS^3*r + 
            180*MASS^2*r^2 - 132*MASS*r^3 + 33*r^4) + 42*M2*M4*MASS*
           (2560*MASS^9 + 91648*MASS^8*r - 3594112*MASS^7*r^2 + 
            21813376*MASS^6*r^3 - 56043776*MASS^5*r^4 + 76239520*MASS^4*r^5 - 
            59415760*MASS^3*r^6 + 26687640*MASS^2*r^7 - 6434610*MASS*r^8 + 
            645645*r^9) - 5*M2^3*(20864*MASS^9 + 1321024*MASS^8*r - 
            32210112*MASS^7*r^2 + 174039888*MASS^6*r^3 - 423017824*MASS^5*
             r^4 + 559400312*MASS^4*r^5 - 430058896*MASS^3*r^6 + 
            192162404*MASS^2*r^7 - 46318090*MASS*r^8 + 4659655*r^9))*
         Log[1 - (2*MASS)/r] + 45*M2*r^2*(-2*MASS + r)^2*
         (168*M4*MASS*r^2*(-2*MASS + r)^2*(448*MASS^4 - 1720*MASS^3*r + 
            1700*MASS^2*r^2 - 350*MASS*r^3 - 91*r^4) + 
          M2^2*(320*MASS^8 + 9408*MASS^7*r - 513952*MASS^6*r^2 + 
            2276624*MASS^5*r^3 - 3606096*MASS^4*r^4 + 2504536*MASS^3*r^5 - 
            686832*MASS^2*r^6 - 4746*MASS*r^7 + 23205*r^8))*
         Log[1 - (2*MASS)/r]^2 + 180*M2^3*r^4*(-2*MASS + r)^4*
         (720*MASS^4 - 1300*MASS^3*r + 136*MASS^2*r^2 + 355*MASS*r^3 - 
          49*r^4)*Log[1 - (2*MASS)/r]^3) + 7*Cos[\[Theta]]^6*
       (2*MASS*(MASS - r)*(-198198*M6*MASS^2*r^2*(-2*MASS + r)^2*
           (16*MASS^6 + 336*MASS^5*r - 11088*MASS^4*r^2 + 38640*MASS^3*r^3 - 
            44310*MASS^2*r^4 + 20790*MASS*r^5 - 3465*r^6) + 
          42*M2*M4*MASS*r*(17920*MASS^9 + 2013888*MASS^8*r + 
            45234880*MASS^7*r^2 - 972828720*MASS^6*r^3 + 3874911376*MASS^5*
             r^4 - 6451842936*MASS^4*r^5 + 5549142480*MASS^3*r^6 - 
            2608084710*MASS^2*r^7 + 638571150*MASS*r^8 - 63918855*r^9) + 
          5*M2^3*(46080*MASS^10 + 69120*MASS^9*r - 18678272*MASS^8*r^2 - 
            469328560*MASS^7*r^3 + 7765530280*MASS^6*r^4 - 29069304264*MASS^5*
             r^5 + 47256350304*MASS^4*r^6 - 40226856420*MASS^3*r^7 + 
            18827765340*MASS^2*r^8 - 4605186600*MASS*r^9 + 461305845*r^10)) + 
        15*(2*MASS - r)*r*(-1387386*M6*MASS^2*(2*MASS - r)^3*r^3*
           (16*MASS^4 - 96*MASS^3*r + 180*MASS^2*r^2 - 132*MASS*r^3 + 
            33*r^4) - 294*M2*M4*MASS*r*(4608*MASS^9 + 66560*MASS^8*r - 
            3464576*MASS^7*r^2 + 21053952*MASS^6*r^3 - 53741120*MASS^5*r^4 + 
            72685376*MASS^4*r^5 - 56399728*MASS^3*r^6 + 25253976*MASS^2*r^7 - 
            6075750*MASS*r^8 + 608751*r^9) + 5*M2^3*(7168*MASS^10 + 
            545920*MASS^9*r + 7478208*MASS^8*r^2 - 223257280*MASS^7*r^3 + 
            1197665616*MASS^6*r^4 - 2875754400*MASS^5*r^5 + 
            3764982008*MASS^4*r^6 - 2872573584*MASS^3*r^7 + 
            1276472628*MASS^2*r^8 - 306487650*MASS*r^9 + 30753723*r^10))*
         Log[1 - (2*MASS)/r] - 225*M2*r^2*(-2*MASS + r)^2*
         (1176*M4*MASS*r^2*(-2*MASS + r)^2*(368*MASS^4 - 1320*MASS^3*r + 
            1316*MASS^2*r^2 - 322*MASS*r^3 - 49*r^4) + 
          M2^2*(7744*MASS^8 + 52288*MASS^7*r - 3078496*MASS^6*r^2 + 
            12634320*MASS^5*r^3 - 19575120*MASS^4*r^4 + 13877640*MASS^3*r^5 - 
            4214040*MASS^2*r^6 + 215190*MASS*r^7 + 87465*r^8))*
         Log[1 - (2*MASS)/r]^2 + 4500*M2^3*r^4*(-2*MASS + r)^4*
         (-752*MASS^4 + 1260*MASS^3*r - 36*MASS^2*r^2 - 459*MASS*r^3 + 
          81*r^4)*Log[1 - (2*MASS)/r]^3)))/(229376*MASS^15*r^2*
     (-2*MASS + r)^4), 0, 0}, 
 {0, 0, r^2 + (5*M2*r*\[Epsilon]p*(-1 + 3*Cos[\[Theta]]^2)*
     (4*MASS^3 - 6*MASS^2*r - 6*MASS*r^2 + (6*MASS^2*r - 3*r^3)*
       Log[1 - (2*MASS)/r]))/(16*MASS^5) + 
   (\[Epsilon]p^2*(4480*M22*MASS^5*(2*MASS - r)*r*(1 - 3*Cos[\[Theta]]^2)*
       (4*MASS^3 - 6*MASS^2*r - 6*MASS*r^2 + (6*MASS^2*r - 3*r^3)*
         Log[1 - (2*MASS)/r]) - 20*(1 - 3*Cos[\[Theta]]^2)*
       (448*M22*MASS^6*r*(4*MASS^3 - 8*MASS^2*r - 3*MASS*r^2 + 3*r^3) + 
        20*M2^2*MASS^2*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
          276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) + 6*MASS*(2*MASS - r)*r*
         (112*M22*MASS^4*r*(2*MASS^2 - r^2) + 5*M2^2*(64*MASS^4 - 
            198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 6*r^4))*
         Log[1 - (2*MASS)/r] + 45*M2^2*r^2*(32*MASS^5 - 16*MASS^4*r + 
          8*MASS^3*r^2 - 4*MASS^2*r^3 - 2*MASS*r^4 + r^5)*
         Log[1 - (2*MASS)/r]^2) + (3 - 30*Cos[\[Theta]]^2 + 
        35*Cos[\[Theta]]^4)*
       (2*MASS*(392*M4*MASS*r*(24*MASS^5 - 272*MASS^4*r - 410*MASS^3*r^2 + 
            1740*MASS^2*r^3 - 1365*MASS*r^4 + 315*r^5) + 
          5*M2^2*(384*MASS^6 - 7368*MASS^5*r + 34024*MASS^4*r^2 + 
            58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 162219*MASS*r^5 - 
            37485*r^6)) + 15*(2*MASS - r)*r*
         (392*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
          M2^2*(928*MASS^5 - 7064*MASS^4*r - 3408*MASS^3*r^2 + 
            38868*MASS^2*r^3 - 41506*MASS*r^4 + 12495*r^5))*
         Log[1 - (2*MASS)/r] - 60*M2^2*r^2*(-440*MASS^5 + 220*MASS^4*r + 
          828*MASS^3*r^2 - 792*MASS^2*r^3 + 171*MASS*r^4 + 9*r^5)*
         Log[1 - (2*MASS)/r]^2)))/(14336*MASS^10*(2*MASS - r)) - 
   (\[Epsilon]p^3*(2200*(1 - 3*Cos[\[Theta]]^2)*
       (4*MASS^2*(2688*M32*MASS^9*r^2*(-2*MASS + r)^2*(2*MASS^2 - 3*MASS*r - 
            3*r^2) + 5*M2^3*(384*MASS^9 - 18048*MASS^8*r + 57464*MASS^7*r^2 + 
            927796*MASS^6*r^3 - 1483036*MASS^5*r^4 - 1217388*MASS^4*r^5 + 
            3667452*MASS^3*r^6 - 2761875*MASS^2*r^7 + 904122*MASS*r^8 - 
            112455*r^9) + 24*M2*MASS*(2*MASS - r)*r*
           (10*M22*MASS^4*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
              276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) + 
            7*M4*(8*MASS^7 + 424*MASS^6*r - 10658*MASS^5*r^2 + 
              9634*MASS^4*r^3 + 16905*MASS^3*r^4 - 27210*MASS^2*r^5 + 
              13230*MASS*r^6 - 2205*r^7) - 112*MASS^7*r*(8*MASS^3 - 
              28*MASS^2*r - 15*MASS*r^2 + 15*r^3)*\[Delta]M20)) + 
        12*MASS*(2*MASS - r)*r*(1344*M32*MASS^9*r^2*(4*MASS^3 - 2*MASS^2*r - 
            2*MASS*r^2 + r^3) + 5*M2^3*(976*MASS^8 - 8256*MASS^7*r - 
            63124*MASS^6*r^2 + 141974*MASS^5*r^3 + 219458*MASS^4*r^4 - 
            749281*MASS^3*r^5 + 685434*MASS^2*r^6 - 264636*MASS*r^7 + 
            37485*r^8) - 12*M2*MASS*(2*MASS - r)*r*
           (-10*M22*MASS^4*(64*MASS^4 - 198*MASS^3*r + 16*MASS^2*r^2 + 
              99*MASS*r^3 - 6*r^4) + 7*M4*(32*MASS^6 - 1618*MASS^5*r + 
              2248*MASS^4*r^2 + 5205*MASS^3*r^3 - 11770*MASS^2*r^4 + 
              7350*MASS*r^5 - 1470*r^6) + 112*MASS^7*(6*MASS^2*r - 5*r^3)*
             \[Delta]M20))*Log[1 - (2*MASS)/r] + 45*M2*r^2*(-2*MASS + r)^2*
         (M2^2*(768*MASS^7 + 4936*MASS^6*r + 1152*MASS^5*r^2 - 
            59832*MASS^4*r^3 + 152388*MASS^3*r^4 - 164052*MASS^2*r^5 + 
            76464*MASS*r^6 - 12495*r^7) + 24*MASS*r*
           (2*M22*MASS^4*(16*MASS^4 + 4*MASS^2*r^2 - r^4) - 
            7*M4*(24*MASS^6 - 204*MASS^4*r^2 + 560*MASS^3*r^3 - 
              621*MASS^2*r^4 + 294*MASS*r^5 - 49*r^6)))*Log[1 - (2*MASS)/r]^
          2 + 45*M2^3*r^3*(-2*MASS + r)^2*(-160*MASS^6 + 1544*MASS^4*r^2 - 
          2784*MASS^3*r^3 + 2502*MASS^2*r^4 - 1236*MASS*r^5 + 249*r^6)*
         Log[1 - (2*MASS)/r]^3) + (5 - 105*Cos[\[Theta]]^2 + 
        315*Cos[\[Theta]]^4 - 231*Cos[\[Theta]]^6)*
       (2*MASS*(198198*M6*MASS^2*r^2*(-2*MASS + r)^2*(80*MASS^6 - 
            2436*MASS^5*r - 8400*MASS^4*r^2 + 72660*MASS^3*r^3 - 
            124530*MASS^2*r^4 + 79695*MASS*r^5 - 17325*r^6) + 
          42*M2*M4*MASS*r*(425600*MASS^9 - 22669440*MASS^8*r + 
            218063408*MASS^7*r^2 + 691895072*MASS^6*r^3 - 6655069988*MASS^5*
             r^4 + 15374522520*MASS^4*r^5 - 16661356320*MASS^3*r^6 + 
            9481694670*MASS^2*r^7 - 2745423765*MASS*r^8 + 319594275*r^9) + 
          5*M2^3*(124800*MASS^10 - 13701600*MASS^9*r + 297478960*MASS^8*r^2 - 
            1720454512*MASS^7*r^3 - 6572825408*MASS^6*r^4 + 
            52115322432*MASS^5*r^5 - 114657526680*MASS^4*r^6 + 
            121624988130*MASS^3*r^7 - 68569968630*MASS^2*r^8 + 
            19796792085*MASS*r^9 - 2306529225*r^10)) + 
        15*(2*MASS - r)*r*(1387386*M6*MASS^2*r^2*(32*MASS^7 - 16*MASS^6*r - 
            1120*MASS^5*r^2 + 3920*MASS^4*r^3 - 5460*MASS^3*r^4 + 
            3738*MASS^2*r^5 - 1254*MASS*r^6 + 165*r^7) + 
          294*M2*M4*MASS*r*(64640*MASS^8 - 1117664*MASS^7*r - 
            933168*MASS^6*r^2 + 27763360*MASS^5*r^3 - 81256240*MASS^4*r^4 + 
            104928620*MASS^3*r^5 - 69500886*MASS^2*r^6 + 23073738*MASS*r^7 - 
            3043755*r^8) + 5*M2^3*(256640*MASS^9 - 9566880*MASS^8*r + 
            79216192*MASS^7*r^2 + 113476984*MASS^6*r^3 - 1696740320*MASS^5*
             r^4 + 4452685120*MASS^4*r^5 - 5461548540*MASS^3*r^6 + 
            3531996978*MASS^2*r^7 - 1163393574*MASS*r^8 + 153768615*r^9))*
         Log[1 - (2*MASS)/r] + 225*M2*r^2*(-2*MASS + r)^2*
         (1176*M4*MASS*r*(1008*MASS^6 - 13200*MASS^4*r^2 + 24260*MASS^3*r^3 - 
            14850*MASS^2*r^4 + 2520*MASS*r^5 + 245*r^6) + 
          5*M2^2*(35904*MASS^7 - 483152*MASS^6*r - 243888*MASS^5*r^2 + 
            5203728*MASS^4*r^3 - 8765364*MASS^3*r^4 + 5232546*MASS^2*r^5 - 
            889920*MASS*r^6 - 87465*r^7))*Log[1 - (2*MASS)/r]^2 + 
        4500*M2^3*r^3*(-2*MASS + r)^2*(6776*MASS^6 - 28480*MASS^4*r^2 + 
          25650*MASS^3*r^3 - 1620*MASS^2*r^4 - 3276*MASS*r^5 + 405*r^6)*
         Log[1 - (2*MASS)/r]^3) - 10*(6 - 60*Cos[\[Theta]]^2 + 
        70*Cos[\[Theta]]^4)*(4*MASS^2*(25872*M34*MASS^5*r^2*(-2*MASS + r)^2*
           (12*MASS^4 - 130*MASS^3*r - 270*MASS^2*r^2 + 735*MASS*r^3 - 
            315*r^4) + 5*M2^3*(28080*MASS^9 - 954360*MASS^8*r + 
            12327732*MASS^7*r^2 - 43877392*MASS^6*r^3 - 46151372*MASS^5*r^4 + 
            252289910*MASS^4*r^5 - 268359225*MASS^3*r^6 + 109267800*MASS^2*
             r^7 - 12761055*MASS*r^8 - 1124550*r^9) + 6*M2*MASS*(2*MASS - r)*
           r*(110*M22*MASS^3*(384*MASS^6 - 7368*MASS^5*r + 34024*MASS^4*r^2 + 
              58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 162219*MASS*r^5 - 
              37485*r^6) + 7*M4*(6960*MASS^7 - 260504*MASS^6*r + 
              1378712*MASS^5*r^2 + 2113570*MASS^4*r^3 - 8295840*MASS^3*r^4 + 
              6121365*MASS^2*r^5 - 1153215*MASS*r^6 - 88200*r^7))) + 
        60*MASS*(2*MASS - r)*r*(12936*M34*MASS^5*r^2*(16*MASS^5 - 
            8*MASS^4*r - 120*MASS^3*r^2 + 200*MASS^2*r^3 - 112*MASS*r^4 + 
            21*r^5) + 5*M2^3*(11712*MASS^8 - 219528*MASS^7*r + 
            943316*MASS^6*r^2 - 165172*MASS^5*r^3 - 2395206*MASS^4*r^4 + 
            2952280*MASS^3*r^5 - 1110419*MASS^2*r^6 - 49026*MASS*r^7 + 
            74970*r^8) + 3*M2*MASS*(2*MASS - r)*r*
           (22*M22*MASS^3*(928*MASS^5 - 7064*MASS^4*r - 3408*MASS^3*r^2 + 
              38868*MASS^2*r^3 - 41506*MASS*r^4 + 12495*r^5) + 
            7*M4*(5408*MASS^6 - 46600*MASS^5*r + 4656*MASS^4*r^2 + 
              151820*MASS^3*r^3 - 141550*MASS^2*r^4 + 15141*MASS*r^5 + 
              11760*r^6)))*Log[1 - (2*MASS)/r] + 45*M2*r^2*(-2*MASS + r)^2*
         (352*M22*MASS^5*r*(220*MASS^4 - 414*MASS^2*r^2 + 189*MASS*r^3 + 
            9*r^4) + 840*M4*MASS*r*(104*MASS^6 + 1804*MASS^4*r^2 - 
            4970*MASS^3*r^3 + 4092*MASS^2*r^4 - 1225*MASS*r^5 + 98*r^6) + 
          5*M2^2*(20416*MASS^7 - 100480*MASS^6*r + 1560*MASS^5*r^2 - 
            400632*MASS^4*r^3 + 1266360*MASS^3*r^4 - 1071378*MASS^2*r^5 + 
            318207*MASS*r^6 - 24990*r^7))*Log[1 - (2*MASS)/r]^2 + 
        450*M2^3*r^3*(-2*MASS + r)^2*(3520*MASS^6 + 4896*MASS^4*r^2 - 
          11020*MASS^3*r^3 + 7032*MASS^2*r^4 - 2565*MASS*r^5 + 486*r^6)*
         Log[1 - (2*MASS)/r]^3) - 11827200*MASS^9*(2*MASS - r)*r^2*
       (1 - 3*Cos[\[Theta]]^2)*(12*M2*MASS^2*\[Delta]M20*
         (2*MASS*(MASS - r) + (2*MASS - r)*r*Log[1 - (2*MASS)/r]) + 
        (2*MASS - r)*(M32*MASS - 5*M2*\[Delta]M20)*(4*MASS^3 - 6*MASS^2*r - 
          6*MASS*r^2 + (6*MASS^2*r - 3*r^3)*Log[1 - (2*MASS)/r])) + 
      2640*MASS^5*(2*MASS - r)*r*(200*M2*M22*(1 - 3*Cos[\[Theta]]^2)*
         (-4*MASS^2*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
            276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) - 6*MASS*(2*MASS - r)*r*
           (64*MASS^4 - 198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 6*r^4)*
           Log[1 - (2*MASS)/r] - 9*r^2*(32*MASS^5 - 16*MASS^4*r + 
            8*MASS^3*r^2 - 4*MASS^2*r^3 - 2*MASS*r^4 + r^5)*
           Log[1 - (2*MASS)/r]^2) - (3 - 30*Cos[\[Theta]]^2 + 
          35*Cos[\[Theta]]^4)*(-4*MASS*(196*M34*MASS*r*(24*MASS^5 - 
              272*MASS^4*r - 410*MASS^3*r^2 + 1740*MASS^2*r^3 - 
              1365*MASS*r^4 + 315*r^5) + 5*M2*M22*(384*MASS^6 - 
              7368*MASS^5*r + 34024*MASS^4*r^2 + 58798*MASS^3*r^3 - 
              211596*MASS^2*r^4 + 162219*MASS*r^5 - 37485*r^6)) - 
          30*(2*MASS - r)*r*(196*M34*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 
              70*MASS*r^3 - 21*r^4) + M2*M22*(928*MASS^5 - 7064*MASS^4*r - 
              3408*MASS^3*r^2 + 38868*MASS^2*r^3 - 41506*MASS*r^4 + 
              12495*r^5))*Log[1 - (2*MASS)/r] + 120*M2*M22*r^2*
           (-440*MASS^5 + 220*MASS^4*r + 828*MASS^3*r^2 - 792*MASS^2*r^3 + 
            171*MASS*r^4 + 9*r^5)*Log[1 - (2*MASS)/r]^2))))/
    (37847040*MASS^15*r*(-2*MASS + r)^2), 0}, 
 {0, 0, 0, r^2*Sin[\[Theta]]^2 - (5*M2*r*\[Epsilon]p*(1 + 3*Cos[2*\[Theta]])*
     (-4*MASS^3 + 6*MASS^2*r + 6*MASS*r^2 + (-6*MASS^2*r + 3*r^3)*
       Log[1 - (2*MASS)/r])*Sin[\[Theta]]^2)/(32*MASS^5) + 
   (\[Epsilon]p^2*(4480*M22*MASS^5*(2*MASS - r)*r*(1 - 3*Cos[\[Theta]]^2)*
       (4*MASS^3 - 6*MASS^2*r - 6*MASS*r^2 + (6*MASS^2*r - 3*r^3)*
         Log[1 - (2*MASS)/r]) - 20*(1 - 3*Cos[\[Theta]]^2)*
       (448*M22*MASS^6*r*(4*MASS^3 - 8*MASS^2*r - 3*MASS*r^2 + 3*r^3) + 
        20*M2^2*MASS^2*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
          276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) + 6*MASS*(2*MASS - r)*r*
         (112*M22*MASS^4*r*(2*MASS^2 - r^2) + 5*M2^2*(64*MASS^4 - 
            198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 6*r^4))*
         Log[1 - (2*MASS)/r] + 45*M2^2*r^2*(32*MASS^5 - 16*MASS^4*r + 
          8*MASS^3*r^2 - 4*MASS^2*r^3 - 2*MASS*r^4 + r^5)*
         Log[1 - (2*MASS)/r]^2) + (3 - 30*Cos[\[Theta]]^2 + 
        35*Cos[\[Theta]]^4)*
       (2*MASS*(392*M4*MASS*r*(24*MASS^5 - 272*MASS^4*r - 410*MASS^3*r^2 + 
            1740*MASS^2*r^3 - 1365*MASS*r^4 + 315*r^5) + 
          5*M2^2*(384*MASS^6 - 7368*MASS^5*r + 34024*MASS^4*r^2 + 
            58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 162219*MASS*r^5 - 
            37485*r^6)) + 15*(2*MASS - r)*r*
         (392*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
          M2^2*(928*MASS^5 - 7064*MASS^4*r - 3408*MASS^3*r^2 + 
            38868*MASS^2*r^3 - 41506*MASS*r^4 + 12495*r^5))*
         Log[1 - (2*MASS)/r] - 60*M2^2*r^2*(-440*MASS^5 + 220*MASS^4*r + 
          828*MASS^3*r^2 - 792*MASS^2*r^3 + 171*MASS*r^4 + 9*r^5)*
         Log[1 - (2*MASS)/r]^2))*Sin[\[Theta]]^2)/
    (14336*MASS^10*(2*MASS - r)) - 
   (\[Epsilon]p^3*(2200*(1 - 3*Cos[\[Theta]]^2)*
       (4*MASS^2*(2688*M32*MASS^9*r^2*(-2*MASS + r)^2*(2*MASS^2 - 3*MASS*r - 
            3*r^2) + 5*M2^3*(384*MASS^9 - 18048*MASS^8*r + 57464*MASS^7*r^2 + 
            927796*MASS^6*r^3 - 1483036*MASS^5*r^4 - 1217388*MASS^4*r^5 + 
            3667452*MASS^3*r^6 - 2761875*MASS^2*r^7 + 904122*MASS*r^8 - 
            112455*r^9) + 24*M2*MASS*(2*MASS - r)*r*
           (10*M22*MASS^4*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
              276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) + 
            7*M4*(8*MASS^7 + 424*MASS^6*r - 10658*MASS^5*r^2 + 
              9634*MASS^4*r^3 + 16905*MASS^3*r^4 - 27210*MASS^2*r^5 + 
              13230*MASS*r^6 - 2205*r^7) - 112*MASS^7*r*(8*MASS^3 - 
              28*MASS^2*r - 15*MASS*r^2 + 15*r^3)*\[Delta]M20)) + 
        12*MASS*(2*MASS - r)*r*(1344*M32*MASS^9*r^2*(4*MASS^3 - 2*MASS^2*r - 
            2*MASS*r^2 + r^3) + 5*M2^3*(976*MASS^8 - 8256*MASS^7*r - 
            63124*MASS^6*r^2 + 141974*MASS^5*r^3 + 219458*MASS^4*r^4 - 
            749281*MASS^3*r^5 + 685434*MASS^2*r^6 - 264636*MASS*r^7 + 
            37485*r^8) - 12*M2*MASS*(2*MASS - r)*r*
           (-10*M22*MASS^4*(64*MASS^4 - 198*MASS^3*r + 16*MASS^2*r^2 + 
              99*MASS*r^3 - 6*r^4) + 7*M4*(32*MASS^6 - 1618*MASS^5*r + 
              2248*MASS^4*r^2 + 5205*MASS^3*r^3 - 11770*MASS^2*r^4 + 
              7350*MASS*r^5 - 1470*r^6) + 112*MASS^7*(6*MASS^2*r - 5*r^3)*
             \[Delta]M20))*Log[1 - (2*MASS)/r] + 45*M2*r^2*(-2*MASS + r)^2*
         (M2^2*(768*MASS^7 + 4936*MASS^6*r + 1152*MASS^5*r^2 - 
            59832*MASS^4*r^3 + 152388*MASS^3*r^4 - 164052*MASS^2*r^5 + 
            76464*MASS*r^6 - 12495*r^7) + 24*MASS*r*
           (2*M22*MASS^4*(16*MASS^4 + 4*MASS^2*r^2 - r^4) - 
            7*M4*(24*MASS^6 - 204*MASS^4*r^2 + 560*MASS^3*r^3 - 
              621*MASS^2*r^4 + 294*MASS*r^5 - 49*r^6)))*Log[1 - (2*MASS)/r]^
          2 + 45*M2^3*r^3*(-2*MASS + r)^2*(-160*MASS^6 + 1544*MASS^4*r^2 - 
          2784*MASS^3*r^3 + 2502*MASS^2*r^4 - 1236*MASS*r^5 + 249*r^6)*
         Log[1 - (2*MASS)/r]^3) + (5 - 105*Cos[\[Theta]]^2 + 
        315*Cos[\[Theta]]^4 - 231*Cos[\[Theta]]^6)*
       (2*MASS*(198198*M6*MASS^2*r^2*(-2*MASS + r)^2*(80*MASS^6 - 
            2436*MASS^5*r - 8400*MASS^4*r^2 + 72660*MASS^3*r^3 - 
            124530*MASS^2*r^4 + 79695*MASS*r^5 - 17325*r^6) + 
          42*M2*M4*MASS*r*(425600*MASS^9 - 22669440*MASS^8*r + 
            218063408*MASS^7*r^2 + 691895072*MASS^6*r^3 - 6655069988*MASS^5*
             r^4 + 15374522520*MASS^4*r^5 - 16661356320*MASS^3*r^6 + 
            9481694670*MASS^2*r^7 - 2745423765*MASS*r^8 + 319594275*r^9) + 
          5*M2^3*(124800*MASS^10 - 13701600*MASS^9*r + 297478960*MASS^8*r^2 - 
            1720454512*MASS^7*r^3 - 6572825408*MASS^6*r^4 + 
            52115322432*MASS^5*r^5 - 114657526680*MASS^4*r^6 + 
            121624988130*MASS^3*r^7 - 68569968630*MASS^2*r^8 + 
            19796792085*MASS*r^9 - 2306529225*r^10)) + 
        15*(2*MASS - r)*r*(1387386*M6*MASS^2*r^2*(32*MASS^7 - 16*MASS^6*r - 
            1120*MASS^5*r^2 + 3920*MASS^4*r^3 - 5460*MASS^3*r^4 + 
            3738*MASS^2*r^5 - 1254*MASS*r^6 + 165*r^7) + 
          294*M2*M4*MASS*r*(64640*MASS^8 - 1117664*MASS^7*r - 
            933168*MASS^6*r^2 + 27763360*MASS^5*r^3 - 81256240*MASS^4*r^4 + 
            104928620*MASS^3*r^5 - 69500886*MASS^2*r^6 + 23073738*MASS*r^7 - 
            3043755*r^8) + 5*M2^3*(256640*MASS^9 - 9566880*MASS^8*r + 
            79216192*MASS^7*r^2 + 113476984*MASS^6*r^3 - 1696740320*MASS^5*
             r^4 + 4452685120*MASS^4*r^5 - 5461548540*MASS^3*r^6 + 
            3531996978*MASS^2*r^7 - 1163393574*MASS*r^8 + 153768615*r^9))*
         Log[1 - (2*MASS)/r] + 225*M2*r^2*(-2*MASS + r)^2*
         (1176*M4*MASS*r*(1008*MASS^6 - 13200*MASS^4*r^2 + 24260*MASS^3*r^3 - 
            14850*MASS^2*r^4 + 2520*MASS*r^5 + 245*r^6) + 
          5*M2^2*(35904*MASS^7 - 483152*MASS^6*r - 243888*MASS^5*r^2 + 
            5203728*MASS^4*r^3 - 8765364*MASS^3*r^4 + 5232546*MASS^2*r^5 - 
            889920*MASS*r^6 - 87465*r^7))*Log[1 - (2*MASS)/r]^2 + 
        4500*M2^3*r^3*(-2*MASS + r)^2*(6776*MASS^6 - 28480*MASS^4*r^2 + 
          25650*MASS^3*r^3 - 1620*MASS^2*r^4 - 3276*MASS*r^5 + 405*r^6)*
         Log[1 - (2*MASS)/r]^3) - 10*(6 - 60*Cos[\[Theta]]^2 + 
        70*Cos[\[Theta]]^4)*(4*MASS^2*(25872*M34*MASS^5*r^2*(-2*MASS + r)^2*
           (12*MASS^4 - 130*MASS^3*r - 270*MASS^2*r^2 + 735*MASS*r^3 - 
            315*r^4) + 5*M2^3*(28080*MASS^9 - 954360*MASS^8*r + 
            12327732*MASS^7*r^2 - 43877392*MASS^6*r^3 - 46151372*MASS^5*r^4 + 
            252289910*MASS^4*r^5 - 268359225*MASS^3*r^6 + 109267800*MASS^2*
             r^7 - 12761055*MASS*r^8 - 1124550*r^9) + 6*M2*MASS*(2*MASS - r)*
           r*(110*M22*MASS^3*(384*MASS^6 - 7368*MASS^5*r + 34024*MASS^4*r^2 + 
              58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 162219*MASS*r^5 - 
              37485*r^6) + 7*M4*(6960*MASS^7 - 260504*MASS^6*r + 
              1378712*MASS^5*r^2 + 2113570*MASS^4*r^3 - 8295840*MASS^3*r^4 + 
              6121365*MASS^2*r^5 - 1153215*MASS*r^6 - 88200*r^7))) + 
        60*MASS*(2*MASS - r)*r*(12936*M34*MASS^5*r^2*(16*MASS^5 - 
            8*MASS^4*r - 120*MASS^3*r^2 + 200*MASS^2*r^3 - 112*MASS*r^4 + 
            21*r^5) + 5*M2^3*(11712*MASS^8 - 219528*MASS^7*r + 
            943316*MASS^6*r^2 - 165172*MASS^5*r^3 - 2395206*MASS^4*r^4 + 
            2952280*MASS^3*r^5 - 1110419*MASS^2*r^6 - 49026*MASS*r^7 + 
            74970*r^8) + 3*M2*MASS*(2*MASS - r)*r*
           (22*M22*MASS^3*(928*MASS^5 - 7064*MASS^4*r - 3408*MASS^3*r^2 + 
              38868*MASS^2*r^3 - 41506*MASS*r^4 + 12495*r^5) + 
            7*M4*(5408*MASS^6 - 46600*MASS^5*r + 4656*MASS^4*r^2 + 
              151820*MASS^3*r^3 - 141550*MASS^2*r^4 + 15141*MASS*r^5 + 
              11760*r^6)))*Log[1 - (2*MASS)/r] + 45*M2*r^2*(-2*MASS + r)^2*
         (352*M22*MASS^5*r*(220*MASS^4 - 414*MASS^2*r^2 + 189*MASS*r^3 + 
            9*r^4) + 840*M4*MASS*r*(104*MASS^6 + 1804*MASS^4*r^2 - 
            4970*MASS^3*r^3 + 4092*MASS^2*r^4 - 1225*MASS*r^5 + 98*r^6) + 
          5*M2^2*(20416*MASS^7 - 100480*MASS^6*r + 1560*MASS^5*r^2 - 
            400632*MASS^4*r^3 + 1266360*MASS^3*r^4 - 1071378*MASS^2*r^5 + 
            318207*MASS*r^6 - 24990*r^7))*Log[1 - (2*MASS)/r]^2 + 
        450*M2^3*r^3*(-2*MASS + r)^2*(3520*MASS^6 + 4896*MASS^4*r^2 - 
          11020*MASS^3*r^3 + 7032*MASS^2*r^4 - 2565*MASS*r^5 + 486*r^6)*
         Log[1 - (2*MASS)/r]^3) - 11827200*MASS^9*(2*MASS - r)*r^2*
       (1 - 3*Cos[\[Theta]]^2)*(12*M2*MASS^2*\[Delta]M20*
         (2*MASS*(MASS - r) + (2*MASS - r)*r*Log[1 - (2*MASS)/r]) + 
        (2*MASS - r)*(M32*MASS - 5*M2*\[Delta]M20)*(4*MASS^3 - 6*MASS^2*r - 
          6*MASS*r^2 + (6*MASS^2*r - 3*r^3)*Log[1 - (2*MASS)/r])) + 
      2640*MASS^5*(2*MASS - r)*r*(200*M2*M22*(1 - 3*Cos[\[Theta]]^2)*
         (-4*MASS^2*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
            276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) - 6*MASS*(2*MASS - r)*r*
           (64*MASS^4 - 198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 6*r^4)*
           Log[1 - (2*MASS)/r] - 9*r^2*(32*MASS^5 - 16*MASS^4*r + 
            8*MASS^3*r^2 - 4*MASS^2*r^3 - 2*MASS*r^4 + r^5)*
           Log[1 - (2*MASS)/r]^2) - (3 - 30*Cos[\[Theta]]^2 + 
          35*Cos[\[Theta]]^4)*(-4*MASS*(196*M34*MASS*r*(24*MASS^5 - 
              272*MASS^4*r - 410*MASS^3*r^2 + 1740*MASS^2*r^3 - 
              1365*MASS*r^4 + 315*r^5) + 5*M2*M22*(384*MASS^6 - 
              7368*MASS^5*r + 34024*MASS^4*r^2 + 58798*MASS^3*r^3 - 
              211596*MASS^2*r^4 + 162219*MASS*r^5 - 37485*r^6)) - 
          30*(2*MASS - r)*r*(196*M34*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 
              70*MASS*r^3 - 21*r^4) + M2*M22*(928*MASS^5 - 7064*MASS^4*r - 
              3408*MASS^3*r^2 + 38868*MASS^2*r^3 - 41506*MASS*r^4 + 
              12495*r^5))*Log[1 - (2*MASS)/r] + 120*M2*M22*r^2*
           (-440*MASS^5 + 220*MASS^4*r + 828*MASS^3*r^2 - 792*MASS^2*r^3 + 
            171*MASS*r^4 + 9*r^5)*Log[1 - (2*MASS)/r]^2)))*Sin[\[Theta]]^2)/
    (37847040*MASS^15*r*(-2*MASS + r)^2)}}
