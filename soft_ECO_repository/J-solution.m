(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{-1 + (2*MASS)/r + (\[Epsilon]p^2*(32*J^2*r + 
      ((-1 + 3*Cos[\[Theta]]^2)*
        (2*MASS*(-15*M2*MASS*r^3*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 
             3*r^3) + J^2*(48*MASS^6 - 8*MASS^5*r - 24*MASS^4*r^2 - 
             30*MASS^3*r^3 - 60*MASS^2*r^4 + 135*MASS*r^5 - 45*r^6)) - 
         45*(J^2 + M2*MASS)*r^5*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/
       MASS^6))/(48*r^5) + 
   (\[Epsilon]p^4*(-2*MASS*(-10*J^2*M2*MASS*r^3*(7040*MASS^10 - 
          9920*MASS^9*r + 448*MASS^8*r^2 + 1152*MASS^7*r^3 + 
          8584*MASS^6*r^4 - 212108*MASS^5*r^5 + 1224796*MASS^4*r^6 - 
          2319120*MASS^3*r^7 + 1913070*MASS^2*r^8 - 722835*MASS*r^9 + 
          103005*r^10) + J^4*(77824*MASS^13 - 73728*MASS^12*r - 
          10240*MASS^11*r^2 - 38656*MASS^10*r^3 + 106880*MASS^9*r^4 + 
          640*MASS^8*r^5 - 18400*MASS^7*r^6 - 48520*MASS^6*r^7 + 
          1010620*MASS^5*r^8 - 4787340*MASS^4*r^9 + 8487600*MASS^3*r^10 - 
          6826950*MASS^2*r^11 + 2555775*MASS*r^12 - 363825*r^13) + 
        5*MASS^2*r^6*(-504*M4*MASS*r*(8*MASS^6 + 68*MASS^5*r - 
            996*MASS^4*r^2 + 2200*MASS^3*r^3 - 1910*MASS^2*r^4 + 
            735*MASS*r^5 - 105*r^6) + 5*M2^2*(544*MASS^7 - 328*MASS^6*r + 
            12700*MASS^5*r^2 - 153900*MASS^4*r^3 + 339504*MASS^3*r^4 - 
            294246*MASS^2*r^5 + 112815*MASS*r^6 - 16065*r^7)) + 
        560*J*MASS^2*r^4*(16*MASS^9 + 16*MASS^8*r + 12*MASS^7*r^2 + 
          16*MASS^6*r^3 - 1722*MASS^5*r^4 + 10934*MASS^4*r^5 - 
          21000*MASS^3*r^6 + 17430*MASS^2*r^7 - 6615*MASS*r^8 + 945*r^9)*
         S3) + 15*(2*MASS - r)*r^4*(2*J^2*M2*MASS*(2304*MASS^9 - 
          768*MASS^8*r - 768*MASS^7*r^2 - 1152*MASS^6*r^3 + 
          19360*MASS^5*r^4 - 177000*MASS^4*r^5 + 448400*MASS^3*r^6 - 
          459730*MASS^2*r^7 + 207210*MASS*r^8 - 34335*r^9) + 
        J^4*(4608*MASS^9 - 1536*MASS^8*r - 1536*MASS^7*r^2 - 864*MASS^6*r^3 + 
          20320*MASS^5*r^4 - 146280*MASS^4*r^5 + 336400*MASS^3*r^6 - 
          330930*MASS^2*r^7 + 146730*MASS*r^8 - 24255*r^9) - 
        5*MASS^2*r^3*(-504*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 
            14*MASS*r + 7*r^2) + M2^2*(288*MASS^6 - 544*MASS^5*r + 
            20712*MASS^4*r^2 - 64592*MASS^3*r^3 + 70554*MASS^2*r^4 - 
            32370*MASS*r^5 + 5355*r^6)) - 3920*J*MASS^2*r^4*
         (4*MASS^5 - 42*MASS^4*r + 112*MASS^3*r^2 - 118*MASS^2*r^3 + 
          54*MASS*r^4 - 9*r^5)*S3)*Log[1 - (2*MASS)/r] + 
      900*(J^2 + M2*MASS)^2*(2*MASS - r)^3*r^9*(15*MASS^2 - 33*MASS*r + 
        5*r^2)*Log[1 - (2*MASS)/r]^2 + 10*(2*MASS - r)*Cos[\[Theta]]^2*
       (2*MASS*(-2*J^2*M2*MASS*r^3*(6720*MASS^9 + 5888*MASS^8*r - 
            2528*MASS^7*r^2 - 9984*MASS^6*r^3 - 41460*MASS^5*r^4 - 
            226140*MASS^4*r^5 + 2538540*MASS^3*r^6 - 4308120*MASS^2*r^7 + 
            2580525*MASS*r^8 - 515025*r^9) + J^4*(15360*MASS^12 - 
            9728*MASS^11*r - 11264*MASS^10*r^2 - 6528*MASS^9*r^3 - 
            5248*MASS^8*r^4 + 9088*MASS^7*r^5 + 16224*MASS^6*r^6 + 
            39060*MASS^5*r^7 + 179100*MASS^4*r^8 - 1858140*MASS^3*r^9 + 
            3073320*MASS^2*r^10 - 1824525*MASS*r^11 + 363825*r^12) + 
          15*MASS^2*(MASS - r)*r^6*(-168*M4*MASS*r*(4*MASS^4 + 40*MASS^3*r - 
              440*MASS^2*r^2 + 420*MASS*r^3 - 105*r^4) + 
            5*M2^2*(32*MASS^5 + 348*MASS^4*r + 2736*MASS^3*r^2 - 
              22980*MASS^2*r^3 + 21492*MASS*r^4 - 5355*r^5)) + 
          336*J*MASS^2*r^4*(8*MASS^8 + 12*MASS^7*r - 4*MASS^6*r^2 - 
            90*MASS^5*r^3 - 550*MASS^4*r^4 + 7440*MASS^3*r^5 - 
            13020*MASS^2*r^6 + 7875*MASS*r^7 - 1575*r^8)*S3) - 
        15*r^5*(2*J^2*M2*MASS*(384*MASS^8 + 256*MASS^6*r^2 + 992*MASS^5*r^3 - 
            143064*MASS^4*r^4 + 423264*MASS^3*r^5 - 452118*MASS^2*r^6 + 
            206730*MASS*r^7 - 34335*r^8) + J^4*(768*MASS^8 + 32*MASS^6*r^2 + 
            928*MASS^5*r^3 - 109464*MASS^4*r^4 + 309024*MASS^3*r^5 - 
            322758*MASS^2*r^6 + 146250*MASS*r^7 - 24255*r^8) + 
          15*MASS^2*r^2*(168*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 
              14*MASS*r + 7*r^2) + M2^2*(32*MASS^6 + 160*MASS^5*r - 
              7432*MASS^4*r^2 + 21856*MASS^3*r^3 - 23466*MASS^2*r^4 + 
              10758*MASS*r^5 - 1785*r^6)) + 336*J*MASS^2*r^3*
           (4*MASS^5 + 374*MASS^4*r - 1224*MASS^3*r^2 + 1358*MASS^2*r^3 - 
            630*MASS*r^4 + 105*r^5)*S3)*Log[1 - (2*MASS)/r] - 
        2700*(J^2 + M2*MASS)^2*r^9*(-2*MASS + r)^2*(7*MASS^2 - 9*MASS*r + 
          r^2)*Log[1 - (2*MASS)/r]^2) - 5*Cos[\[Theta]]^4*
       (2*MASS*(-10*J^2*M2*MASS*r^3*(4992*MASS^10 + 9536*MASS^9*r - 
            5440*MASS^8*r^2 - 18112*MASS^7*r^3 - 45944*MASS^6*r^4 - 
            65740*MASS^5*r^5 + 2298204*MASS^4*r^6 - 5116920*MASS^3*r^7 + 
            4405470*MASS^2*r^8 - 1685655*MASS*r^9 + 240345*r^10) + 
          J^4*(49152*MASS^13 - 61440*MASS^12*r - 46080*MASS^11*r^2 + 
            6400*MASS^10*r^3 - 80512*MASS^9*r^4 + 54400*MASS^8*r^5 + 
            140000*MASS^7*r^6 + 158200*MASS^6*r^7 + 108860*MASS^5*r^8 - 
            8144460*MASS^4*r^9 + 18192600*MASS^3*r^10 - 15609750*MASS^2*
             r^11 + 5958675*MASS*r^12 - 848925*r^13) + 15*MASS^2*(MASS - r)*
           r^6*(-392*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 920*MASS^3*r^2 + 
              1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
            5*M2^2*(160*MASS^6 + 2056*MASS^5*r + 12476*MASS^4*r^2 - 
              114664*MASS^3*r^3 + 154336*MASS^2*r^4 - 75186*MASS*r^5 + 
              12495*r^6)) + 560*J*MASS^2*r^4*(16*MASS^9 + 16*MASS^8*r - 
            52*MASS^7*r^2 - 368*MASS^6*r^3 - 298*MASS^5*r^4 + 
            19926*MASS^4*r^5 - 45960*MASS^3*r^6 + 40110*MASS^2*r^7 - 
            15435*MASS*r^8 + 2205*r^9)*S3) + 15*(2*MASS - r)*r^4*
         (J^4*(2560*MASS^9 - 3072*MASS^8*r - 2560*MASS^7*r^2 + 
            3040*MASS^6*r^3 + 15648*MASS^5*r^4 + 210920*MASS^4*r^5 - 
            691120*MASS^3*r^6 + 747810*MASS^2*r^7 - 341730*MASS*r^8 + 
            56595*r^9) + 2*J^2*M2*MASS*(1280*MASS^9 - 1536*MASS^8*r - 
            1280*MASS^7*r^2 + 640*MASS^6*r^3 + 16224*MASS^5*r^4 + 
            291560*MASS^4*r^5 - 959920*MASS^3*r^6 + 1050210*MASS^2*r^7 - 
            482850*MASS*r^8 + 80115*r^9) - 5*MASS^2*r^3*
           (1176*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
            M2^2*(352*MASS^6 + 672*MASS^5*r - 51816*MASS^4*r^2 + 
              153456*MASS^3*r^3 - 164682*MASS^2*r^4 + 75402*MASS*r^5 - 
              12495*r^6)) - 560*J*MASS^2*r^4*(36*MASS^5 + 454*MASS^4*r - 
            1664*MASS^3*r^2 + 1890*MASS^2*r^3 - 882*MASS*r^4 + 147*r^5)*S3)*
         Log[1 - (2*MASS)/r] - 2700*(J^2 + M2*MASS)^2*(2*MASS - r)^3*r^9*
         (13*MASS^2 - 19*MASS*r + 3*r^2)*Log[1 - (2*MASS)/r]^2)))/
    (10240*MASS^12*r^9*(-2*MASS + r)), 0, 0, 
  (-2*J*\[Epsilon]p*Sin[\[Theta]]^2)/r - 
   (\[Epsilon]p^3*(-640*r^4*S31 + 
      (-16*MASS*(5*J*M2*MASS*r^3*(-2*MASS^3 + 10*MASS^2*r + 15*MASS*r^2 - 
            15*r^3) + J^3*(16*MASS^6 - 10*MASS^3*r^3 + 50*MASS^2*r^4 + 
            75*MASS*r^5 - 75*r^6) - 40*MASS^6*r^4*S31) + 
        120*J*(J^2 + M2*MASS)*r^4*(4*MASS^3 - 10*MASS*r^2 + 5*r^3)*
         Log[1 - (2*MASS)/r])/MASS^7 + ((-1 + 5*Cos[\[Theta]]^2)*
        (2*MASS*(-10*J*M2*MASS*r^3*(12*MASS^5 + 88*MASS^4*r + 70*MASS^3*r^2 + 
             210*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) + 
           J^3*(192*MASS^8 - 160*MASS^7*r - 160*MASS^6*r^2 - 120*MASS^5*r^3 - 
             740*MASS^4*r^4 - 350*MASS^3*r^5 - 1050*MASS^2*r^6 + 
             3675*MASS*r^7 - 1575*r^8) + 35*MASS^2*r^4*(4*MASS^4 + 
             10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 45*r^4)*S3) + 
         15*r^4*(J^3*(32*MASS^5 - 40*MASS^4*r - 280*MASS^2*r^3 + 
             350*MASS*r^4 - 105*r^5) + 2*J*M2*MASS*(16*MASS^5 - 20*MASS^4*r - 
             280*MASS^2*r^3 + 350*MASS*r^4 - 105*r^5) + 35*MASS^2*r^3*
            (8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*Log[1 - (2*MASS)/r]))/MASS^9)*
     Sin[\[Theta]]^2)/(320*r^5) + 
   (\[Epsilon]p^5*(6*MASS*(-20*J^3*M2*MASS*r^3*(49280*MASS^12 - 
          65856*MASS^11*r - 13888*MASS^10*r^2 - 93632*MASS^9*r^3 + 
          1028104*MASS^8*r^4 - 2773412*MASS^7*r^5 + 7790292*MASS^6*r^6 - 
          21427266*MASS^5*r^7 + 59797290*MASS^4*r^8 - 101427690*MASS^3*r^9 + 
          85818180*MASS^2*r^10 - 34255305*MASS*r^11 + 5180175*r^12) + 
        J^5*(1089536*MASS^15 - 1032192*MASS^14*r - 143360*MASS^13*r^2 - 
          541184*MASS^12*r^3 + 1550080*MASS^11*r^4 + 224000*MASS^10*r^5 + 
          804160*MASS^9*r^6 - 10645040*MASS^8*r^7 + 26613000*MASS^7*r^8 - 
          61655920*MASS^6*r^9 + 151570300*MASS^5*r^10 - 364683900*MASS^4*
           r^11 + 572578650*MASS^3*r^12 - 470785350*MASS^2*r^13 + 
          186251625*MASS*r^14 - 28153125*r^15) + 5*J*MASS^2*r^6*
         (-336*M4*MASS*r*(168*MASS^8 + 1436*MASS^7*r - 17868*MASS^6*r^2 + 
            57694*MASS^5*r^3 - 181650*MASS^4*r^4 + 327600*MASS^3*r^5 - 
            283920*MASS^2*r^6 + 114345*MASS*r^7 - 17325*r^8) + 
          M2^2*(38080*MASS^9 - 14000*MASS^8*r + 879336*MASS^7*r^2 - 
            9056864*MASS^6*r^3 + 37571548*MASS^5*r^4 - 153104700*MASS^4*r^5 + 
            297604230*MASS^3*r^6 - 262792530*MASS^2*r^7 + 106174215*MASS*
             r^8 - 16060275*r^9)) + 70*J^2*MASS^2*r^4*(3584*MASS^11 - 
          12544*MASS^9*r^2 + 135520*MASS^8*r^3 - 386272*MASS^7*r^4 + 
          1105768*MASS^6*r^5 - 3028648*MASS^5*r^6 + 8987160*MASS^4*r^7 - 
          15707790*MASS^3*r^8 + 13430550*MASS^2*r^9 - 5380515*MASS*r^10 + 
          814275*r^11)*S3 - 70*MASS^3*(2*MASS - r)*r^7*
         (M2*(560*MASS^7 + 2536*MASS^6*r + 18704*MASS^5*r^2 + 
            246540*MASS^4*r^3 - 2497110*MASS^3*r^4 + 4718700*MASS^2*r^5 - 
            3114405*MASS*r^6 + 675675*r^7)*S3 - 132*MASS*r*
           (8*MASS^6 + 56*MASS^5*r + 420*MASS^4*r^2 - 5670*MASS^3*r^3 + 
            10920*MASS^2*r^4 - 7245*MASS*r^5 + 1575*r^6)*S5)) - 
      315*(2*MASS - r)*r^4*(J^5*(9216*MASS^11 - 3072*MASS^10*r - 
          3072*MASS^9*r^2 - 40128*MASS^8*r^3 + 124480*MASS^7*r^4 - 
          288640*MASS^6*r^5 + 728480*MASS^5*r^6 - 1893620*MASS^4*r^7 + 
          3364660*MASS^3*r^8 - 3236520*MASS^2*r^9 + 1509900*MASS*r^10 - 
          268125*r^11) + 4*J^3*M2*MASS*(2304*MASS^11 - 768*MASS^10*r - 
          768*MASS^9*r^2 - 19712*MASS^8*r^3 + 61600*MASS^7*r^4 - 
          172320*MASS^6*r^5 + 490800*MASS^5*r^6 - 1475310*MASS^4*r^7 + 
          2903760*MASS^3*r^8 - 2926350*MASS^2*r^9 + 1387680*MASS*r^10 - 
          246675*r^11) - 5*J*MASS^2*r^3*(-336*M4*MASS*r^2*
           (64*MASS^6 - 240*MASS^5*r + 770*MASS^4*r^2 - 1736*MASS^3*r^3 + 
            1890*MASS^2*r^4 - 924*MASS*r^5 + 165*r^6) + 
          M2^2*(576*MASS^8 - 1088*MASS^7*r + 34432*MASS^6*r^2 - 
            156000*MASS^5*r^3 + 700948*MASS^4*r^4 - 1654780*MASS^3*r^5 + 
            1778448*MASS^2*r^6 - 859908*MASS*r^7 + 152955*r^8)) + 
        70*J^2*MASS^2*r^3*(512*MASS^8 - 1664*MASS^7*r + 4736*MASS^6*r^2 - 
          13088*MASS^5*r^3 + 41624*MASS^4*r^4 - 87268*MASS^3*r^5 + 
          90732*MASS^2*r^6 - 43548*MASS*r^7 + 7755*r^8)*S3 - 
        70*MASS^3*r^5*(M2*(64*MASS^6 + 832*MASS^5*r - 21240*MASS^4*r^2 + 
            64420*MASS^3*r^3 - 73596*MASS^2*r^4 + 36156*MASS*r^5 - 6435*r^6)*
           S3 + 132*MASS*r^2*(40*MASS^4 - 140*MASS^3*r + 168*MASS^2*r^2 - 
            84*MASS*r^3 + 15*r^4)*S5))*Log[1 - (2*MASS)/r] - 
      18900*(J^2 + M2*MASS)*(2*MASS - r)*r^9*
       (2*J*M2*MASS*(40*MASS^6 - 192*MASS^5*r + 1647*MASS^4*r^2 - 
          3603*MASS^3*r^3 + 2670*MASS^2*r^4 - 700*MASS*r^5 + 35*r^6) + 
        J^3*(80*MASS^6 - 384*MASS^5*r + 1894*MASS^4*r^2 - 3706*MASS^3*r^3 + 
          2680*MASS^2*r^4 - 700*MASS*r^5 + 35*r^6) - 
        35*MASS^2*r^2*(40*MASS^4 - 100*MASS^3*r + 76*MASS^2*r^2 - 
          20*MASS*r^3 + r^4)*S3)*Log[1 - (2*MASS)/r]^2 - 
      210*Cos[\[Theta]]^2*(2*MASS*(-4*J^3*M2*MASS*r^3*(13440*MASS^12 + 
            7104*MASS^11*r - 46272*MASS^10*r^2 - 106720*MASS^9*r^3 + 
            356120*MASS^8*r^4 - 1075044*MASS^7*r^5 + 3667884*MASS^6*r^6 - 
            11577576*MASS^5*r^7 + 49965690*MASS^4*r^8 - 97322115*MASS^3*r^9 + 
            85263465*MASS^2*r^10 - 34283655*MASS*r^11 + 5180175*r^12) + 
          J^5*(61440*MASS^15 - 102400*MASS^14*r - 5120*MASS^13*r^2 + 
            16896*MASS^12*r^3 - 6912*MASS^11*r^4 + 143872*MASS^10*r^5 + 
            220160*MASS^9*r^6 - 743920*MASS^8*r^7 + 1793000*MASS^7*r^8 - 
            5423200*MASS^6*r^9 + 15579500*MASS^5*r^10 - 58072500*MASS^4*
             r^11 + 108036450*MASS^3*r^12 - 93209550*MASS^2*r^13 + 
            37288125*MASS*r^14 - 5630625*r^15) + J*MASS^2*r^6*
           (-336*M4*MASS*r*(120*MASS^8 + 1836*MASS^7*r - 10916*MASS^6*r^2 + 
              31024*MASS^5*r^3 - 149310*MASS^4*r^4 + 311535*MASS^3*r^5 - 
              281085*MASS^2*r^6 + 114345*MASS*r^7 - 17325*r^8) + 
            M2^2*(9600*MASS^9 + 107920*MASS^8*r + 1242472*MASS^7*r^2 - 
              5033552*MASS^6*r^3 + 23491348*MASS^5*r^4 - 141390420*MASS^4*r^
                5 + 294434970*MASS^3*r^6 - 262791270*MASS^2*r^7 + 
              106249815*MASS*r^8 - 16060275*r^9)) + 14*J^2*MASS^2*r^4*
           (1792*MASS^11 - 2176*MASS^10*r - 14080*MASS^9*r^2 + 
            52800*MASS^8*r^3 - 143392*MASS^7*r^4 + 522632*MASS^6*r^5 - 
            1626028*MASS^5*r^6 + 7492320*MASS^4*r^7 - 15031620*MASS^3*r^8 + 
            13326420*MASS^2*r^9 - 5383215*MASS*r^10 + 814275*r^11)*S3 - 
          14*MASS^3*(2*MASS - r)*r^7*(M2*(320*MASS^7 + 5712*MASS^6*r + 
              29924*MASS^5*r^2 + 248880*MASS^4*r^3 - 2591880*MASS^3*r^4 + 
              4767930*MASS^2*r^5 - 3117105*MASS*r^6 + 675675*r^7)*S3 - 
            132*MASS*r*(8*MASS^6 + 56*MASS^5*r + 420*MASS^4*r^2 - 
              5670*MASS^3*r^3 + 10920*MASS^2*r^4 - 7245*MASS*r^5 + 1575*r^6)*
             S5)) + 15*(2*MASS - r)*r^4*(4*J^3*M2*MASS*(256*MASS^11 - 
            768*MASS^10*r + 128*MASS^9*r^2 + 13696*MASS^8*r^3 - 
            30192*MASS^7*r^4 + 117392*MASS^6*r^5 - 334920*MASS^5*r^6 + 
            1586850*MASS^4*r^7 - 3812190*MASS^3*r^8 + 4058649*MASS^2*r^9 - 
            1946532*MASS*r^10 + 345345*r^11) + J^5*(1024*MASS^11 - 
            3072*MASS^10*r + 512*MASS^9*r^2 + 27712*MASS^8*r^3 - 
            56992*MASS^7*r^4 + 177888*MASS^6*r^5 - 477200*MASS^5*r^6 + 
            1933020*MASS^4*r^7 - 4319540*MASS^3*r^8 + 4466280*MASS^2*r^9 - 
            2118900*MASS*r^10 + 375375*r^11) + J*MASS^2*r^3*
           (336*M4*MASS*r*(16*MASS^7 - 328*MASS^6*r + 840*MASS^5*r^2 - 
              3990*MASS^4*r^3 + 11270*MASS^3*r^4 - 13041*MASS^2*r^5 + 
              6468*MASS*r^6 - 1155*r^7) + M2^2*(-1600*MASS^8 - 
              23456*MASS^7*r + 170720*MASS^6*r^2 - 580240*MASS^5*r^3 + 
              4249740*MASS^4*r^4 - 11329060*MASS^3*r^5 + 12445692*MASS^2*r^
                6 - 6029436*MASS*r^7 + 1070685*r^8)) - 14*J^2*MASS^2*r^3*
           (2048*MASS^8 - 3456*MASS^7*r + 15872*MASS^6*r^2 - 
            44800*MASS^5*r^3 + 222840*MASS^4*r^4 - 571240*MASS^3*r^5 + 
            628062*MASS^2*r^6 - 305196*MASS*r^7 + 54285*r^8)*S3 + 
          14*MASS^3*r^4*(M2*(192*MASS^7 + 640*MASS^6*r + 4480*MASS^5*r^2 - 
              152280*MASS^4*r^3 + 458680*MASS^3*r^4 - 518694*MASS^2*r^5 + 
              253452*MASS*r^6 - 45045*r^7)*S3 + 924*MASS*r^3*
             (40*MASS^4 - 140*MASS^3*r + 168*MASS^2*r^2 - 84*MASS*r^3 + 
              15*r^4)*S5))*Log[1 - (2*MASS)/r] + 1800*(J^2 + M2*MASS)*
         (2*MASS - r)*r^8*(J*M2*MASS*(16*MASS^7 - 80*MASS^6*r + 
            100*MASS^5*r^2 - 1945*MASS^4*r^3 + 4534*MASS^3*r^4 - 
            3523*MASS^2*r^5 + 1008*MASS*r^6 - 70*r^7) + 
          J^3*(16*MASS^7 - 80*MASS^6*r + 100*MASS^5*r^2 - 1021*MASS^4*r^3 + 
            2294*MASS^3*r^4 - 1766*MASS^2*r^5 + 504*MASS*r^6 - 35*r^7) + 
          7*MASS^2*r^3*(132*MASS^4 - 320*MASS^3*r + 251*MASS^2*r^2 - 
            72*MASS*r^3 + 5*r^4)*S3)*Log[1 - (2*MASS)/r]^2) + 
      35*Cos[\[Theta]]^4*(2*MASS*(-4*J^3*M2*MASS*r^3*(74880*MASS^12 + 
            212160*MASS^11*r - 572480*MASS^10*r^2 - 1239680*MASS^9*r^3 + 
            4043880*MASS^8*r^4 - 10263396*MASS^7*r^5 + 20579556*MASS^6*r^6 - 
            58769334*MASS^5*r^7 + 397757610*MASS^4*r^8 - 852095160*MASS^3*
             r^9 + 763841610*MASS^2*r^10 - 308694645*MASS*r^11 + 
            46621575*r^12) + J^5*(294912*MASS^15 - 811008*MASS^14*r - 
            55296*MASS^13*r^2 + 321024*MASS^12*r^3 - 653568*MASS^11*r^4 + 
            1852672*MASS^10*r^5 + 2718400*MASS^9*r^6 - 8584560*MASS^8*r^7 + 
            16447080*MASS^7*r^8 - 30518160*MASS^6*r^9 + 74282700*MASS^5*
             r^10 - 445031100*MASS^4*r^11 + 935496450*MASS^3*r^12 - 
            833140350*MASS^2*r^13 + 335782125*MASS*r^14 - 50675625*r^15) + 
          3*J*MASS^2*r^6*(-112*M4*MASS*r*(840*MASS^8 + 18524*MASS^7*r - 
              63484*MASS^6*r^2 + 145866*MASS^5*r^3 - 1182090*MASS^4*r^4 + 
              2723490*MASS^3*r^5 - 2515590*MASS^2*r^6 + 1029105*MASS*r^7 - 
              155925*r^8) + M2^2*(24000*MASS^9 + 399600*MASS^8*r + 
              4676856*MASS^7*r^2 - 8844736*MASS^6*r^3 + 47503844*MASS^5*r^4 - 
              401733060*MASS^4*r^5 + 875133210*MASS^3*r^6 - 787699710*MASS^
                2*r^7 + 318875445*MASS*r^8 - 48180825*r^9)) + 
          14*J^2*MASS^2*r^4*(15360*MASS^11 - 24320*MASS^10*r - 
            154880*MASS^9*r^2 + 600480*MASS^8*r^3 - 1299168*MASS^7*r^4 + 
            2948328*MASS^6*r^5 - 8182752*MASS^5*r^6 + 59750280*MASS^4*r^7 - 
            131601330*MASS^3*r^8 + 119345130*MASS^2*r^9 - 48462435*MASS*
             r^10 + 7328475*r^11)*S3 - 42*MASS^3*(2*MASS - r)*r^7*
           (M2*(1200*MASS^7 + 20296*MASS^6*r + 94072*MASS^5*r^2 + 
              764940*MASS^4*r^3 - 7880790*MASS^3*r^4 + 14361840*MASS^2*r^5 - 
              9355815*MASS*r^6 + 2027025*r^7)*S3 - 396*MASS*r*
             (8*MASS^6 + 56*MASS^5*r + 420*MASS^4*r^2 - 5670*MASS^3*r^3 + 
              10920*MASS^2*r^4 - 7245*MASS*r^5 + 1575*r^6)*S5)) + 
        15*(2*MASS - r)*r^4*(4*J^3*M2*MASS*(8448*MASS^11 - 12032*MASS^10*r + 
            1536*MASS^9*r^2 + 159744*MASS^8*r^3 - 310528*MASS^7*r^4 + 
            815424*MASS^6*r^5 - 1477440*MASS^5*r^6 + 11681610*MASS^4*r^7 - 
            32708340*MASS^3*r^8 + 36235836*MASS^2*r^9 - 17537688*MASS*r^10 + 
            3108105*r^11) + J^5*(33792*MASS^11 - 48128*MASS^10*r + 
            6144*MASS^9*r^2 + 338496*MASS^8*r^3 - 564608*MASS^7*r^4 + 
            1212864*MASS^6*r^5 - 2047680*MASS^5*r^6 + 13562940*MASS^4*r^7 - 
            36463980*MASS^3*r^8 + 39737880*MASS^2*r^9 - 19095300*MASS*r^10 + 
            3378375*r^11) + 3*J*MASS^2*r^3*(784*M4*MASS*r*(32*MASS^7 - 
              336*MASS^6*r + 480*MASS^5*r^2 - 4130*MASS^4*r^3 + 
              13860*MASS^3*r^4 - 16632*MASS^2*r^5 + 8316*MASS*r^6 - 
              1485*r^7) + M2^2*(-7360*MASS^8 - 99200*MASS^7*r + 
              415040*MASS^6*r^2 - 917760*MASS^5*r^3 + 11438660*MASS^4*r^4 - 
              33267500*MASS^3*r^5 + 37241736*MASS^2*r^6 - 18105108*MASS*r^7 + 
              3212055*r^8)) - 14*J^2*MASS^2*r^3*(23040*MASS^8 - 
            34432*MASS^7*r + 109056*MASS^6*r^2 - 197280*MASS^5*r^3 + 
            1646280*MASS^4*r^4 - 4912260*MASS^3*r^5 + 5607648*MASS^2*r^6 - 
            2748564*MASS*r^7 + 488565*r^8)*S3 + 42*MASS^3*r^4*
           (M2*(640*MASS^7 + 1600*MASS^6*r + 12480*MASS^5*r^2 - 
              455800*MASS^4*r^3 + 1378700*MASS^3*r^4 - 1558752*MASS^2*r^5 + 
              760956*MASS*r^6 - 135135*r^7)*S3 + 2772*MASS*r^3*
             (40*MASS^4 - 140*MASS^3*r + 168*MASS^2*r^2 - 84*MASS*r^3 + 
              15*r^4)*S5))*Log[1 - (2*MASS)/r] + 900*(J^2 + M2*MASS)*
         (2*MASS - r)*r^8*(J^3*(448*MASS^7 - 1296*MASS^6*r + 720*MASS^5*r^2 - 
            14226*MASS^4*r^3 + 34842*MASS^3*r^4 - 28224*MASS^2*r^5 + 
            8652*MASS*r^6 - 735*r^7) + 2*J*M2*MASS*(224*MASS^7 - 
            648*MASS^6*r + 360*MASS^5*r^2 - 14253*MASS^4*r^3 + 
            34851*MASS^3*r^4 - 28224*MASS^2*r^5 + 8652*MASS*r^6 - 735*r^7) + 
          21*MASS^2*r^3*(680*MASS^4 - 1660*MASS^3*r + 1344*MASS^2*r^2 - 
            412*MASS*r^3 + 35*r^4)*S3)*Log[1 - (2*MASS)/r]^2))*
     Sin[\[Theta]]^2)/(430080*MASS^15*r^9*(-2*MASS + r))}, 
 {0, -(r/(2*MASS - r)) + 
   (\[Epsilon]p^4*(2*MASS*(-10*J^2*M2*MASS*r^3*(23680*MASS^10 - 
          63296*MASS^9*r + 58944*MASS^8*r^2 - 25216*MASS^7*r^3 - 
          28808*MASS^6*r^4 + 287948*MASS^5*r^5 - 1251676*MASS^4*r^6 + 
          2293200*MASS^3*r^7 - 1901550*MASS^2*r^8 + 722835*MASS*r^9 - 
          103005*r^10) + J^4*(806912*MASS^13 - 1064960*MASS^12*r + 
          382976*MASS^11*r^2 - 209152*MASS^10*r^3 + 648320*MASS^9*r^4 - 
          594560*MASS^8*r^5 + 226400*MASS^7*r^6 + 119560*MASS^6*r^7 - 
          1361020*MASS^5*r^8 + 4912140*MASS^4*r^9 - 8358000*MASS^3*r^10 + 
          6769350*MASS^2*r^11 - 2555775*MASS*r^12 + 363825*r^13) + 
        5*MASS^2*(MASS - r)*r^6*(504*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 
            920*MASS^3*r^2 + 1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
          5*M2^2*(224*MASS^6 + 3112*MASS^5*r - 8436*MASS^4*r^2 + 
            135096*MASS^3*r^3 - 195192*MASS^2*r^4 + 96750*MASS*r^5 - 
            16065*r^6)) + 560*J*MASS^2*r^4*(80*MASS^9 - 16*MASS^8*r - 
          36*MASS^7*r^2 - 136*MASS^6*r^3 + 2502*MASS^5*r^4 - 
          11654*MASS^4*r^5 + 21180*MASS^3*r^6 - 17430*MASS^2*r^7 + 
          6615*MASS*r^8 - 945*r^9)*S3) - 15*(2*MASS - r)*r^4*
       (J^4*(23040*MASS^9 - 28672*MASS^8*r + 11776*MASS^7*r^2 - 
          1440*MASS^6*r^3 - 35040*MASS^5*r^4 + 151720*MASS^4*r^5 - 
          320080*MASS^3*r^6 + 323250*MASS^2*r^7 - 146730*MASS*r^8 + 
          24255*r^9) + 2*J^2*M2*MASS*(11520*MASS^9 - 14336*MASS^8*r + 
          5888*MASS^7*r^2 - 35360*MASS^5*r^4 + 183080*MASS^4*r^5 - 
          432080*MASS^3*r^6 + 452050*MASS^2*r^7 - 207210*MASS*r^8 + 
          34335*r^9) + 5*MASS^2*r^3*(-504*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(288*MASS^6 - 
            1312*MASS^5*r + 18024*MASS^4*r^2 - 59984*MASS^3*r^3 + 
            69018*MASS^2*r^4 - 32370*MASS*r^5 + 5355*r^6)) + 
        560*J*MASS^2*r^4*(52*MASS^5 - 330*MASS^4*r + 796*MASS^3*r^2 - 
          826*MASS^2*r^3 + 378*MASS*r^4 - 63*r^5)*S3)*Log[1 - (2*MASS)/r] + 
      900*(J^2 + M2*MASS)^2*(2*MASS - r)^3*r^9*(15*MASS^2 - MASS*r + 5*r^2)*
       Log[1 - (2*MASS)/r]^2 + 5*Cos[\[Theta]]^4*
       (2*MASS*(10*J^2*M2*MASS*r^3*(11136*MASS^10 - 27328*MASS^9*r - 
            20800*MASS^8*r^2 + 29888*MASS^7*r^3 - 44408*MASS^6*r^4 - 
            160396*MASS^5*r^5 + 2350044*MASS^4*r^6 - 5086680*MASS^3*r^7 + 
            4379550*MASS^2*r^8 - 1681335*MASS*r^9 + 240345*r^10) + 
          J^4*(270336*MASS^13 - 577536*MASS^12*r + 334848*MASS^11*r^2 + 
            110336*MASS^10*r^3 - 274304*MASS^9*r^4 - 208000*MASS^8*r^5 + 
            316960*MASS^7*r^6 - 248440*MASS^6*r^7 - 570620*MASS^5*r^8 + 
            8403660*MASS^4*r^9 - 18041400*MASS^3*r^10 + 15480150*MASS^2*
             r^11 - 5937075*MASS*r^12 + 848925*r^13) + 15*MASS^2*(MASS - r)*
           r^6*(392*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 920*MASS^3*r^2 + 
              1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) - 
            5*M2^2*(32*MASS^6 + 1416*MASS^5*r + 12476*MASS^4*r^2 - 
              112744*MASS^3*r^3 + 152896*MASS^2*r^4 - 74898*MASS*r^5 + 
              12495*r^6)) + 560*J*MASS^2*r^4*(80*MASS^9 - 16*MASS^8*r + 
            28*MASS^7*r^2 + 248*MASS^6*r^3 + 1078*MASS^5*r^4 - 
            20646*MASS^4*r^5 + 46140*MASS^3*r^6 - 40110*MASS^2*r^7 + 
            15435*MASS*r^8 - 2205*r^9)*S3) + 15*(2*MASS - r)*r^4*
         (2*J^2*M2*MASS*(6400*MASS^9 + 768*MASS^8*r - 7936*MASS^7*r^2 + 
            5120*MASS^6*r^3 + 288*MASS^5*r^4 - 305960*MASS^4*r^5 + 
            945520*MASS^3*r^6 - 1035810*MASS^2*r^7 + 479970*MASS*r^8 - 
            80115*r^9) + J^4*(12800*MASS^9 + 1536*MASS^8*r - 
            15872*MASS^7*r^2 + 8480*MASS^6*r^3 + 96*MASS^5*r^4 - 
            225320*MASS^4*r^5 + 676720*MASS^3*r^6 - 733410*MASS^2*r^7 + 
            338850*MASS*r^8 - 56595*r^9) + 5*MASS^2*r^3*
           (1176*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
            M2^2*(352*MASS^6 + 1440*MASS^5*r - 50664*MASS^4*r^2 + 
              149232*MASS^3*r^3 - 161802*MASS^2*r^4 + 74826*MASS*r^5 - 
              12495*r^6)) + 560*J*MASS^2*r^4*(12*MASS^5 + 490*MASS^4*r - 
            1676*MASS^3*r^2 + 1890*MASS^2*r^3 - 882*MASS*r^4 + 147*r^5)*S3)*
         Log[1 - (2*MASS)/r] + 2700*(J^2 + M2*MASS)^2*(2*MASS - r)^3*r^9*
         (13*MASS^2 - 11*MASS*r - r^2)*Log[1 - (2*MASS)/r]^2) - 
      30*Cos[\[Theta]]^2*(2*MASS*(-2*J^2*M2*MASS*r^3*(5760*MASS^10 - 
            1600*MASS^9*r + 21568*MASS^8*r^2 - 21728*MASS^7*r^3 + 
            13432*MASS^6*r^4 + 231660*MASS^5*r^5 - 1824380*MASS^4*r^6 + 
            3696420*MASS^3*r^7 - 3134790*MASS^2*r^8 + 1199925*MASS*r^9 - 
            171675*r^10) + J^4*(92160*MASS^13 - 137216*MASS^12*r + 
            51712*MASS^11*r^2 - 2816*MASS^10*r^3 + 3712*MASS^9*r^4 - 
            43648*MASS^8*r^5 + 43008*MASS^7*r^6 - 18712*MASS^6*r^7 - 
            200460*MASS^5*r^8 + 1355100*MASS^4*r^9 - 2646420*MASS^3*r^10 + 
            2219190*MASS^2*r^11 - 847125*MASS*r^12 + 121275*r^13) + 
          5*MASS^2*(MASS - r)*r^7*(168*M4*MASS*(8*MASS^5 + 76*MASS^4*r - 
              920*MASS^3*r^2 + 1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) - 
            5*M2^2*(344*MASS^5 + 5124*MASS^4*r - 47736*MASS^3*r^2 + 
              65244*MASS^2*r^3 - 32058*MASS*r^4 + 5355*r^5)) + 
          112*J*MASS^2*r^4*(80*MASS^9 - 16*MASS^8*r - 4*MASS^7*r^2 + 
            56*MASS^6*r^3 + 1790*MASS^5*r^4 - 16150*MASS^4*r^5 + 
            33660*MASS^3*r^6 - 28770*MASS^2*r^7 + 11025*MASS*r^8 - 1575*r^9)*
           S3) + 15*(2*MASS - r)*r^5*(2*J^2*M2*MASS*(640*MASS^8 - 
            1024*MASS^7*r + 640*MASS^6*r^2 + 3616*MASS^5*r^3 - 
            50760*MASS^4*r^4 + 138912*MASS^3*r^5 - 148306*MASS^2*r^6 + 
            68430*MASS*r^7 - 11445*r^8) + J^4*(1280*MASS^8 - 2048*MASS^7*r + 
            1120*MASS^6*r^2 + 3552*MASS^5*r^3 - 39560*MASS^4*r^4 + 
            100832*MASS^3*r^5 - 105186*MASS^2*r^6 + 48270*MASS*r^7 - 
            8085*r^8) + 5*MASS^2*r^2*(168*M4*MASS*r^2*(-2*MASS + r)^2*
             (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(32*MASS^6 + 
              288*MASS^5*r - 7240*MASS^4*r^2 + 21152*MASS^3*r^3 - 
              22986*MASS^2*r^4 + 10662*MASS*r^5 - 1785*r^6)) - 
          112*J*MASS^2*r^3*(20*MASS^5 - 410*MASS^4*r + 1236*MASS^3*r^2 - 
            1358*MASS^2*r^3 + 630*MASS*r^4 - 105*r^5)*S3)*
         Log[1 - (2*MASS)/r] + 900*(J^2 + M2*MASS)^2*(2*MASS - r)^3*r^9*
         (7*MASS^2 - 5*MASS*r - r^2)*Log[1 - (2*MASS)/r]^2)))/
    (10240*MASS^12*r^7*(-2*MASS + r)^3) + 
   (r*\[Epsilon]p^2*(-128*M20 - ((16*MASS - 8*r)*(-128*r*(-J^2 + M20*r^3) + 
         (2*(1 + 3*Cos[2*\[Theta]])*(2*MASS*(5*M2*MASS*r^3*(2*MASS^3 + 
                4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + J^2*(80*MASS^6 - 
                56*MASS^5*r + 8*MASS^4*r^2 + 10*MASS^3*r^3 + 20*MASS^2*r^4 - 
                45*MASS*r^5 + 15*r^6)) + 15*(J^2 + M2*MASS)*r^5*
             (-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/MASS^6))/
       (8*(2*MASS - r)*r^4)))/(16*MASS - 8*r)^2, 0, 0}, 
 {0, 0, r^2 + (\[Epsilon]p^2*(-1 + 3*Cos[\[Theta]]^2)*
     (-8*MASS*(5*M2*MASS*r^3*(-2*MASS^2 + 3*MASS*r + 3*r^2) + 
        J^2*(16*MASS^5 + 8*MASS^4*r - 10*MASS^2*r^3 + 15*MASS*r^4 + 
          15*r^5)) + 60*(J^2 + M2*MASS)*r^4*(2*MASS^2 - r^2)*
       Log[1 - (2*MASS)/r]))/(64*MASS^6*r^2) + 
   (\[Epsilon]p^4*(6*MASS*(-10*J^2*M2*MASS*r^3*(18560*MASS^9 - 
          66112*MASS^8*r - 38336*MASS^7*r^2 + 15392*MASS^6*r^3 + 
          157176*MASS^5*r^4 - 749272*MASS^4*r^5 - 88650*MASS^3*r^6 + 
          1654980*MASS^2*r^7 - 1338705*MASS*r^8 + 309015*r^9) + 
        J^4*(206848*MASS^12 - 19456*MASS^11*r + 23040*MASS^10*r^2 - 
          195072*MASS^9*r^3 + 658560*MASS^8*r^4 + 392320*MASS^7*r^5 - 
          128640*MASS^6*r^6 - 738680*MASS^5*r^7 + 3226680*MASS^4*r^8 - 
          195150*MASS^3*r^9 - 5721300*MASS^2*r^10 + 4727925*MASS*r^11 - 
          1091475*r^12) + 5*MASS^2*r^6*(504*M4*MASS*r*(24*MASS^5 - 
            272*MASS^4*r - 410*MASS^3*r^2 + 1740*MASS^2*r^3 - 1365*MASS*r^4 + 
            315*r^5) + 5*M2^2*(1408*MASS^6 - 19256*MASS^5*r + 
            62808*MASS^4*r^2 + 81906*MASS^3*r^3 - 279252*MASS^2*r^4 + 
            208773*MASS*r^5 - 48195*r^6)) + 560*J*MASS^2*r^4*
         (16*MASS^8 + 16*MASS^7*r + 108*MASS^6*r^2 + 736*MASS^5*r^3 - 
          6038*MASS^4*r^4 - 120*MASS^3*r^5 + 14700*MASS^2*r^6 - 
          12285*MASS*r^7 + 2835*r^8)*S3) - 45*(2*MASS - r)*r^4*
       (2*J^2*M2*MASS*(14848*MASS^8 + 1664*MASS^7*r - 8576*MASS^6*r^2 - 
          16704*MASS^5*r^3 + 130760*MASS^4*r^4 - 77360*MASS^3*r^5 - 
          286940*MASS^2*r^6 + 343110*MASS*r^7 - 103005*r^8) + 
        J^4*(29696*MASS^8 + 3328*MASS^7*r - 17152*MASS^6*r^2 - 
          20128*MASS^5*r^3 + 112840*MASS^4*r^4 - 77360*MASS^3*r^5 - 
          197340*MASS^2*r^6 + 242310*MASS*r^7 - 72765*r^8) - 
        5*MASS^2*r^3*(504*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 70*MASS*r^3 - 
            21*r^4) + M2^2*(2656*MASS^5 - 13608*MASS^4*r - 4016*MASS^3*r^2 + 
            52236*MASS^2*r^3 - 53502*MASS*r^4 + 16065*r^5)) - 
        560*J*MASS^2*r^4*(180*MASS^4 - 174*MASS^3*r - 476*MASS^2*r^2 + 
          630*MASS*r^3 - 189*r^4)*S3)*Log[1 - (2*MASS)/r] + 
      2700*(J^2 + M2*MASS)^2*r^8*(280*MASS^5 - 140*MASS^4*r - 
        332*MASS^3*r^2 + 328*MASS^2*r^3 - 79*MASS*r^4 - r^5)*
       Log[1 - (2*MASS)/r]^2 + 20*Cos[2*\[Theta]]*
       (2*MASS*(-2*J^2*M2*MASS*r^3*(40320*MASS^9 - 133824*MASS^8*r - 
            72640*MASS^7*r^2 + 38816*MASS^6*r^3 + 338280*MASS^5*r^4 - 
            1570200*MASS^4*r^5 + 90330*MASS^3*r^6 + 2710980*MASS^2*r^7 - 
            2231775*MASS*r^8 + 515025*r^9) + J^4*(92160*MASS^12 - 
            9216*MASS^11*r + 4608*MASS^10*r^2 - 84480*MASS^9*r^3 + 
            269184*MASS^8*r^4 + 154240*MASS^7*r^5 - 57536*MASS^6*r^6 - 
            319560*MASS^5*r^7 + 1382040*MASS^4*r^8 - 308730*MASS^3*r^9 - 
            1854180*MASS^2*r^10 + 1576575*MASS*r^11 - 363825*r^12) + 
          15*MASS^2*r^6*(56*M4*MASS*r*(24*MASS^5 - 272*MASS^4*r - 
              410*MASS^3*r^2 + 1740*MASS^2*r^3 - 1365*MASS*r^4 + 315*r^5) + 
            5*M2^2*(192*MASS^6 - 2520*MASS^5*r + 7720*MASS^4*r^2 + 
              9346*MASS^3*r^3 - 31308*MASS^2*r^4 + 23205*MASS*r^5 - 
              5355*r^6)) + 112*J*MASS^2*r^4*(48*MASS^8 + 80*MASS^7*r + 
            308*MASS^6*r^2 + 1680*MASS^5*r^3 - 12570*MASS^4*r^4 + 
            2040*MASS^3*r^5 + 23940*MASS^2*r^6 - 20475*MASS*r^7 + 4725*r^8)*
           S3) - 15*(2*MASS - r)*r^4*(2*J^2*M2*MASS*(6144*MASS^8 + 
            640*MASS^7*r - 3456*MASS^6*r^2 - 6720*MASS^5*r^3 + 
            54440*MASS^4*r^4 - 38544*MASS^3*r^5 - 92412*MASS^2*r^6 + 
            114450*MASS*r^7 - 34335*r^8) + J^4*(12288*MASS^8 + 
            1280*MASS^7*r - 6912*MASS^6*r^2 - 8160*MASS^5*r^3 + 
            47720*MASS^4*r^4 - 38544*MASS^3*r^5 - 62172*MASS^2*r^6 + 
            80850*MASS*r^7 - 24255*r^8) - 15*MASS^2*r^3*
           (56*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
            M2^2*(352*MASS^5 - 1688*MASS^4*r - 432*MASS^3*r^2 + 
              5892*MASS^2*r^3 - 5950*MASS*r^4 + 1785*r^5)) - 
          112*J*MASS^2*r^4*(380*MASS^4 - 402*MASS^3*r - 756*MASS^2*r^2 + 
            1050*MASS*r^3 - 315*r^4)*S3)*Log[1 - (2*MASS)/r] + 
        900*MASS*(J^2 + M2*MASS)^2*r^8*(104*MASS^4 - 52*MASS^3*r - 
          108*MASS^2*r^2 + 108*MASS*r^3 - 27*r^4)*Log[1 - (2*MASS)/r]^2) + 
      5*Cos[4*\[Theta]]*(2*MASS*(-10*J^2*M2*MASS*r^3*(5760*MASS^9 - 
            59712*MASS^8*r - 54464*MASS^7*r^2 - 16736*MASS^6*r^3 + 
            61464*MASS^5*r^4 - 462648*MASS^4*r^5 - 1159170*MASS^3*r^6 + 
            4050900*MASS^2*r^7 - 3121245*MASS*r^8 + 721035*r^9) + 
          J^4*(18432*MASS^12 + 9216*MASS^11*r + 115200*MASS^10*r^2 - 
            66048*MASS^9*r^3 + 543360*MASS^8*r^4 + 446080*MASS^7*r^5 - 
            7040*MASS^6*r^6 - 256920*MASS^5*r^7 + 1399320*MASS^4*r^8 + 
            4418250*MASS^3*r^9 - 14408100*MASS^2*r^10 + 11019825*MASS*r^11 - 
            2546775*r^12) + 15*MASS^2*r^6*(392*M4*MASS*r*(24*MASS^5 - 
              272*MASS^4*r - 410*MASS^3*r^2 + 1740*MASS^2*r^3 - 
              1365*MASS*r^4 + 315*r^5) + 5*M2^2*(384*MASS^6 - 7368*MASS^5*r + 
              34024*MASS^4*r^2 + 58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 
              162219*MASS*r^5 - 37485*r^6)) - 560*J*MASS^2*r^4*
           (48*MASS^8 + 176*MASS^7*r + 260*MASS^6*r^2 + 96*MASS^5*r^3 + 
            4062*MASS^4*r^4 + 9240*MASS^3*r^5 - 36540*MASS^2*r^6 + 
            28665*MASS*r^7 - 6615*r^8)*S3) - 15*(2*MASS - r)*r^4*
         (2*J^2*M2*MASS*(10752*MASS^8 + 2176*MASS^7*r - 8064*MASS^6*r^2 - 
            15936*MASS^5*r^3 + 88040*MASS^4*r^4 + 74640*MASS^3*r^5 - 
            734220*MASS^2*r^6 + 798990*MASS*r^7 - 240345*r^8) + 
          J^4*(21504*MASS^8 + 4352*MASS^7*r - 16128*MASS^6*r^2 - 
            17952*MASS^5*r^3 + 61160*MASS^4*r^4 + 74640*MASS^3*r^5 - 
            532620*MASS^2*r^6 + 563790*MASS*r^7 - 169785*r^8) - 
          15*MASS^2*r^3*(392*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 
              70*MASS*r^3 - 21*r^4) + M2^2*(928*MASS^5 - 7064*MASS^4*r - 
              3408*MASS^3*r^2 + 38868*MASS^2*r^3 - 41506*MASS*r^4 + 
              12495*r^5)) - 560*J*MASS^2*r^4*(100*MASS^4 + 42*MASS^3*r - 
            1260*MASS^2*r^2 + 1470*MASS*r^3 - 441*r^4)*S3)*
         Log[1 - (2*MASS)/r] + 900*(J^2 + M2*MASS)^2*r^8*
         (440*MASS^5 - 220*MASS^4*r - 828*MASS^3*r^2 + 792*MASS^2*r^3 - 
          171*MASS*r^4 - 9*r^5)*Log[1 - (2*MASS)/r]^2)))/
    (245760*MASS^12*(2*MASS - r)*r^6), 0}, 
 {(-2*J*\[Epsilon]p*Sin[\[Theta]]^2)/r - 
   (\[Epsilon]p^3*(-640*r^4*S31 + 
      (-16*MASS*(5*J*M2*MASS*r^3*(-2*MASS^3 + 10*MASS^2*r + 15*MASS*r^2 - 
            15*r^3) + J^3*(16*MASS^6 - 10*MASS^3*r^3 + 50*MASS^2*r^4 + 
            75*MASS*r^5 - 75*r^6) - 40*MASS^6*r^4*S31) + 
        120*J*(J^2 + M2*MASS)*r^4*(4*MASS^3 - 10*MASS*r^2 + 5*r^3)*
         Log[1 - (2*MASS)/r])/MASS^7 + ((-1 + 5*Cos[\[Theta]]^2)*
        (2*MASS*(-10*J*M2*MASS*r^3*(12*MASS^5 + 88*MASS^4*r + 70*MASS^3*r^2 + 
             210*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) + 
           J^3*(192*MASS^8 - 160*MASS^7*r - 160*MASS^6*r^2 - 120*MASS^5*r^3 - 
             740*MASS^4*r^4 - 350*MASS^3*r^5 - 1050*MASS^2*r^6 + 
             3675*MASS*r^7 - 1575*r^8) + 35*MASS^2*r^4*(4*MASS^4 + 
             10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 45*r^4)*S3) + 
         15*r^4*(J^3*(32*MASS^5 - 40*MASS^4*r - 280*MASS^2*r^3 + 
             350*MASS*r^4 - 105*r^5) + 2*J*M2*MASS*(16*MASS^5 - 20*MASS^4*r - 
             280*MASS^2*r^3 + 350*MASS*r^4 - 105*r^5) + 35*MASS^2*r^3*
            (8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*Log[1 - (2*MASS)/r]))/MASS^9)*
     Sin[\[Theta]]^2)/(320*r^5) + 
   (\[Epsilon]p^5*(6*MASS*(-20*J^3*M2*MASS*r^3*(49280*MASS^12 - 
          65856*MASS^11*r - 13888*MASS^10*r^2 - 93632*MASS^9*r^3 + 
          1028104*MASS^8*r^4 - 2773412*MASS^7*r^5 + 7790292*MASS^6*r^6 - 
          21427266*MASS^5*r^7 + 59797290*MASS^4*r^8 - 101427690*MASS^3*r^9 + 
          85818180*MASS^2*r^10 - 34255305*MASS*r^11 + 5180175*r^12) + 
        J^5*(1089536*MASS^15 - 1032192*MASS^14*r - 143360*MASS^13*r^2 - 
          541184*MASS^12*r^3 + 1550080*MASS^11*r^4 + 224000*MASS^10*r^5 + 
          804160*MASS^9*r^6 - 10645040*MASS^8*r^7 + 26613000*MASS^7*r^8 - 
          61655920*MASS^6*r^9 + 151570300*MASS^5*r^10 - 364683900*MASS^4*
           r^11 + 572578650*MASS^3*r^12 - 470785350*MASS^2*r^13 + 
          186251625*MASS*r^14 - 28153125*r^15) + 5*J*MASS^2*r^6*
         (-336*M4*MASS*r*(168*MASS^8 + 1436*MASS^7*r - 17868*MASS^6*r^2 + 
            57694*MASS^5*r^3 - 181650*MASS^4*r^4 + 327600*MASS^3*r^5 - 
            283920*MASS^2*r^6 + 114345*MASS*r^7 - 17325*r^8) + 
          M2^2*(38080*MASS^9 - 14000*MASS^8*r + 879336*MASS^7*r^2 - 
            9056864*MASS^6*r^3 + 37571548*MASS^5*r^4 - 153104700*MASS^4*r^5 + 
            297604230*MASS^3*r^6 - 262792530*MASS^2*r^7 + 106174215*MASS*
             r^8 - 16060275*r^9)) + 70*J^2*MASS^2*r^4*(3584*MASS^11 - 
          12544*MASS^9*r^2 + 135520*MASS^8*r^3 - 386272*MASS^7*r^4 + 
          1105768*MASS^6*r^5 - 3028648*MASS^5*r^6 + 8987160*MASS^4*r^7 - 
          15707790*MASS^3*r^8 + 13430550*MASS^2*r^9 - 5380515*MASS*r^10 + 
          814275*r^11)*S3 - 70*MASS^3*(2*MASS - r)*r^7*
         (M2*(560*MASS^7 + 2536*MASS^6*r + 18704*MASS^5*r^2 + 
            246540*MASS^4*r^3 - 2497110*MASS^3*r^4 + 4718700*MASS^2*r^5 - 
            3114405*MASS*r^6 + 675675*r^7)*S3 - 132*MASS*r*
           (8*MASS^6 + 56*MASS^5*r + 420*MASS^4*r^2 - 5670*MASS^3*r^3 + 
            10920*MASS^2*r^4 - 7245*MASS*r^5 + 1575*r^6)*S5)) - 
      315*(2*MASS - r)*r^4*(J^5*(9216*MASS^11 - 3072*MASS^10*r - 
          3072*MASS^9*r^2 - 40128*MASS^8*r^3 + 124480*MASS^7*r^4 - 
          288640*MASS^6*r^5 + 728480*MASS^5*r^6 - 1893620*MASS^4*r^7 + 
          3364660*MASS^3*r^8 - 3236520*MASS^2*r^9 + 1509900*MASS*r^10 - 
          268125*r^11) + 4*J^3*M2*MASS*(2304*MASS^11 - 768*MASS^10*r - 
          768*MASS^9*r^2 - 19712*MASS^8*r^3 + 61600*MASS^7*r^4 - 
          172320*MASS^6*r^5 + 490800*MASS^5*r^6 - 1475310*MASS^4*r^7 + 
          2903760*MASS^3*r^8 - 2926350*MASS^2*r^9 + 1387680*MASS*r^10 - 
          246675*r^11) - 5*J*MASS^2*r^3*(-336*M4*MASS*r^2*
           (64*MASS^6 - 240*MASS^5*r + 770*MASS^4*r^2 - 1736*MASS^3*r^3 + 
            1890*MASS^2*r^4 - 924*MASS*r^5 + 165*r^6) + 
          M2^2*(576*MASS^8 - 1088*MASS^7*r + 34432*MASS^6*r^2 - 
            156000*MASS^5*r^3 + 700948*MASS^4*r^4 - 1654780*MASS^3*r^5 + 
            1778448*MASS^2*r^6 - 859908*MASS*r^7 + 152955*r^8)) + 
        70*J^2*MASS^2*r^3*(512*MASS^8 - 1664*MASS^7*r + 4736*MASS^6*r^2 - 
          13088*MASS^5*r^3 + 41624*MASS^4*r^4 - 87268*MASS^3*r^5 + 
          90732*MASS^2*r^6 - 43548*MASS*r^7 + 7755*r^8)*S3 - 
        70*MASS^3*r^5*(M2*(64*MASS^6 + 832*MASS^5*r - 21240*MASS^4*r^2 + 
            64420*MASS^3*r^3 - 73596*MASS^2*r^4 + 36156*MASS*r^5 - 6435*r^6)*
           S3 + 132*MASS*r^2*(40*MASS^4 - 140*MASS^3*r + 168*MASS^2*r^2 - 
            84*MASS*r^3 + 15*r^4)*S5))*Log[1 - (2*MASS)/r] - 
      18900*(J^2 + M2*MASS)*(2*MASS - r)*r^9*
       (2*J*M2*MASS*(40*MASS^6 - 192*MASS^5*r + 1647*MASS^4*r^2 - 
          3603*MASS^3*r^3 + 2670*MASS^2*r^4 - 700*MASS*r^5 + 35*r^6) + 
        J^3*(80*MASS^6 - 384*MASS^5*r + 1894*MASS^4*r^2 - 3706*MASS^3*r^3 + 
          2680*MASS^2*r^4 - 700*MASS*r^5 + 35*r^6) - 
        35*MASS^2*r^2*(40*MASS^4 - 100*MASS^3*r + 76*MASS^2*r^2 - 
          20*MASS*r^3 + r^4)*S3)*Log[1 - (2*MASS)/r]^2 - 
      210*Cos[\[Theta]]^2*(2*MASS*(-4*J^3*M2*MASS*r^3*(13440*MASS^12 + 
            7104*MASS^11*r - 46272*MASS^10*r^2 - 106720*MASS^9*r^3 + 
            356120*MASS^8*r^4 - 1075044*MASS^7*r^5 + 3667884*MASS^6*r^6 - 
            11577576*MASS^5*r^7 + 49965690*MASS^4*r^8 - 97322115*MASS^3*r^9 + 
            85263465*MASS^2*r^10 - 34283655*MASS*r^11 + 5180175*r^12) + 
          J^5*(61440*MASS^15 - 102400*MASS^14*r - 5120*MASS^13*r^2 + 
            16896*MASS^12*r^3 - 6912*MASS^11*r^4 + 143872*MASS^10*r^5 + 
            220160*MASS^9*r^6 - 743920*MASS^8*r^7 + 1793000*MASS^7*r^8 - 
            5423200*MASS^6*r^9 + 15579500*MASS^5*r^10 - 58072500*MASS^4*
             r^11 + 108036450*MASS^3*r^12 - 93209550*MASS^2*r^13 + 
            37288125*MASS*r^14 - 5630625*r^15) + J*MASS^2*r^6*
           (-336*M4*MASS*r*(120*MASS^8 + 1836*MASS^7*r - 10916*MASS^6*r^2 + 
              31024*MASS^5*r^3 - 149310*MASS^4*r^4 + 311535*MASS^3*r^5 - 
              281085*MASS^2*r^6 + 114345*MASS*r^7 - 17325*r^8) + 
            M2^2*(9600*MASS^9 + 107920*MASS^8*r + 1242472*MASS^7*r^2 - 
              5033552*MASS^6*r^3 + 23491348*MASS^5*r^4 - 141390420*MASS^4*r^
                5 + 294434970*MASS^3*r^6 - 262791270*MASS^2*r^7 + 
              106249815*MASS*r^8 - 16060275*r^9)) + 14*J^2*MASS^2*r^4*
           (1792*MASS^11 - 2176*MASS^10*r - 14080*MASS^9*r^2 + 
            52800*MASS^8*r^3 - 143392*MASS^7*r^4 + 522632*MASS^6*r^5 - 
            1626028*MASS^5*r^6 + 7492320*MASS^4*r^7 - 15031620*MASS^3*r^8 + 
            13326420*MASS^2*r^9 - 5383215*MASS*r^10 + 814275*r^11)*S3 - 
          14*MASS^3*(2*MASS - r)*r^7*(M2*(320*MASS^7 + 5712*MASS^6*r + 
              29924*MASS^5*r^2 + 248880*MASS^4*r^3 - 2591880*MASS^3*r^4 + 
              4767930*MASS^2*r^5 - 3117105*MASS*r^6 + 675675*r^7)*S3 - 
            132*MASS*r*(8*MASS^6 + 56*MASS^5*r + 420*MASS^4*r^2 - 
              5670*MASS^3*r^3 + 10920*MASS^2*r^4 - 7245*MASS*r^5 + 1575*r^6)*
             S5)) + 15*(2*MASS - r)*r^4*(4*J^3*M2*MASS*(256*MASS^11 - 
            768*MASS^10*r + 128*MASS^9*r^2 + 13696*MASS^8*r^3 - 
            30192*MASS^7*r^4 + 117392*MASS^6*r^5 - 334920*MASS^5*r^6 + 
            1586850*MASS^4*r^7 - 3812190*MASS^3*r^8 + 4058649*MASS^2*r^9 - 
            1946532*MASS*r^10 + 345345*r^11) + J^5*(1024*MASS^11 - 
            3072*MASS^10*r + 512*MASS^9*r^2 + 27712*MASS^8*r^3 - 
            56992*MASS^7*r^4 + 177888*MASS^6*r^5 - 477200*MASS^5*r^6 + 
            1933020*MASS^4*r^7 - 4319540*MASS^3*r^8 + 4466280*MASS^2*r^9 - 
            2118900*MASS*r^10 + 375375*r^11) + J*MASS^2*r^3*
           (336*M4*MASS*r*(16*MASS^7 - 328*MASS^6*r + 840*MASS^5*r^2 - 
              3990*MASS^4*r^3 + 11270*MASS^3*r^4 - 13041*MASS^2*r^5 + 
              6468*MASS*r^6 - 1155*r^7) + M2^2*(-1600*MASS^8 - 
              23456*MASS^7*r + 170720*MASS^6*r^2 - 580240*MASS^5*r^3 + 
              4249740*MASS^4*r^4 - 11329060*MASS^3*r^5 + 12445692*MASS^2*r^
                6 - 6029436*MASS*r^7 + 1070685*r^8)) - 14*J^2*MASS^2*r^3*
           (2048*MASS^8 - 3456*MASS^7*r + 15872*MASS^6*r^2 - 
            44800*MASS^5*r^3 + 222840*MASS^4*r^4 - 571240*MASS^3*r^5 + 
            628062*MASS^2*r^6 - 305196*MASS*r^7 + 54285*r^8)*S3 + 
          14*MASS^3*r^4*(M2*(192*MASS^7 + 640*MASS^6*r + 4480*MASS^5*r^2 - 
              152280*MASS^4*r^3 + 458680*MASS^3*r^4 - 518694*MASS^2*r^5 + 
              253452*MASS*r^6 - 45045*r^7)*S3 + 924*MASS*r^3*
             (40*MASS^4 - 140*MASS^3*r + 168*MASS^2*r^2 - 84*MASS*r^3 + 
              15*r^4)*S5))*Log[1 - (2*MASS)/r] + 1800*(J^2 + M2*MASS)*
         (2*MASS - r)*r^8*(J*M2*MASS*(16*MASS^7 - 80*MASS^6*r + 
            100*MASS^5*r^2 - 1945*MASS^4*r^3 + 4534*MASS^3*r^4 - 
            3523*MASS^2*r^5 + 1008*MASS*r^6 - 70*r^7) + 
          J^3*(16*MASS^7 - 80*MASS^6*r + 100*MASS^5*r^2 - 1021*MASS^4*r^3 + 
            2294*MASS^3*r^4 - 1766*MASS^2*r^5 + 504*MASS*r^6 - 35*r^7) + 
          7*MASS^2*r^3*(132*MASS^4 - 320*MASS^3*r + 251*MASS^2*r^2 - 
            72*MASS*r^3 + 5*r^4)*S3)*Log[1 - (2*MASS)/r]^2) + 
      35*Cos[\[Theta]]^4*(2*MASS*(-4*J^3*M2*MASS*r^3*(74880*MASS^12 + 
            212160*MASS^11*r - 572480*MASS^10*r^2 - 1239680*MASS^9*r^3 + 
            4043880*MASS^8*r^4 - 10263396*MASS^7*r^5 + 20579556*MASS^6*r^6 - 
            58769334*MASS^5*r^7 + 397757610*MASS^4*r^8 - 852095160*MASS^3*
             r^9 + 763841610*MASS^2*r^10 - 308694645*MASS*r^11 + 
            46621575*r^12) + J^5*(294912*MASS^15 - 811008*MASS^14*r - 
            55296*MASS^13*r^2 + 321024*MASS^12*r^3 - 653568*MASS^11*r^4 + 
            1852672*MASS^10*r^5 + 2718400*MASS^9*r^6 - 8584560*MASS^8*r^7 + 
            16447080*MASS^7*r^8 - 30518160*MASS^6*r^9 + 74282700*MASS^5*
             r^10 - 445031100*MASS^4*r^11 + 935496450*MASS^3*r^12 - 
            833140350*MASS^2*r^13 + 335782125*MASS*r^14 - 50675625*r^15) + 
          3*J*MASS^2*r^6*(-112*M4*MASS*r*(840*MASS^8 + 18524*MASS^7*r - 
              63484*MASS^6*r^2 + 145866*MASS^5*r^3 - 1182090*MASS^4*r^4 + 
              2723490*MASS^3*r^5 - 2515590*MASS^2*r^6 + 1029105*MASS*r^7 - 
              155925*r^8) + M2^2*(24000*MASS^9 + 399600*MASS^8*r + 
              4676856*MASS^7*r^2 - 8844736*MASS^6*r^3 + 47503844*MASS^5*r^4 - 
              401733060*MASS^4*r^5 + 875133210*MASS^3*r^6 - 787699710*MASS^
                2*r^7 + 318875445*MASS*r^8 - 48180825*r^9)) + 
          14*J^2*MASS^2*r^4*(15360*MASS^11 - 24320*MASS^10*r - 
            154880*MASS^9*r^2 + 600480*MASS^8*r^3 - 1299168*MASS^7*r^4 + 
            2948328*MASS^6*r^5 - 8182752*MASS^5*r^6 + 59750280*MASS^4*r^7 - 
            131601330*MASS^3*r^8 + 119345130*MASS^2*r^9 - 48462435*MASS*
             r^10 + 7328475*r^11)*S3 - 42*MASS^3*(2*MASS - r)*r^7*
           (M2*(1200*MASS^7 + 20296*MASS^6*r + 94072*MASS^5*r^2 + 
              764940*MASS^4*r^3 - 7880790*MASS^3*r^4 + 14361840*MASS^2*r^5 - 
              9355815*MASS*r^6 + 2027025*r^7)*S3 - 396*MASS*r*
             (8*MASS^6 + 56*MASS^5*r + 420*MASS^4*r^2 - 5670*MASS^3*r^3 + 
              10920*MASS^2*r^4 - 7245*MASS*r^5 + 1575*r^6)*S5)) + 
        15*(2*MASS - r)*r^4*(4*J^3*M2*MASS*(8448*MASS^11 - 12032*MASS^10*r + 
            1536*MASS^9*r^2 + 159744*MASS^8*r^3 - 310528*MASS^7*r^4 + 
            815424*MASS^6*r^5 - 1477440*MASS^5*r^6 + 11681610*MASS^4*r^7 - 
            32708340*MASS^3*r^8 + 36235836*MASS^2*r^9 - 17537688*MASS*r^10 + 
            3108105*r^11) + J^5*(33792*MASS^11 - 48128*MASS^10*r + 
            6144*MASS^9*r^2 + 338496*MASS^8*r^3 - 564608*MASS^7*r^4 + 
            1212864*MASS^6*r^5 - 2047680*MASS^5*r^6 + 13562940*MASS^4*r^7 - 
            36463980*MASS^3*r^8 + 39737880*MASS^2*r^9 - 19095300*MASS*r^10 + 
            3378375*r^11) + 3*J*MASS^2*r^3*(784*M4*MASS*r*(32*MASS^7 - 
              336*MASS^6*r + 480*MASS^5*r^2 - 4130*MASS^4*r^3 + 
              13860*MASS^3*r^4 - 16632*MASS^2*r^5 + 8316*MASS*r^6 - 
              1485*r^7) + M2^2*(-7360*MASS^8 - 99200*MASS^7*r + 
              415040*MASS^6*r^2 - 917760*MASS^5*r^3 + 11438660*MASS^4*r^4 - 
              33267500*MASS^3*r^5 + 37241736*MASS^2*r^6 - 18105108*MASS*r^7 + 
              3212055*r^8)) - 14*J^2*MASS^2*r^3*(23040*MASS^8 - 
            34432*MASS^7*r + 109056*MASS^6*r^2 - 197280*MASS^5*r^3 + 
            1646280*MASS^4*r^4 - 4912260*MASS^3*r^5 + 5607648*MASS^2*r^6 - 
            2748564*MASS*r^7 + 488565*r^8)*S3 + 42*MASS^3*r^4*
           (M2*(640*MASS^7 + 1600*MASS^6*r + 12480*MASS^5*r^2 - 
              455800*MASS^4*r^3 + 1378700*MASS^3*r^4 - 1558752*MASS^2*r^5 + 
              760956*MASS*r^6 - 135135*r^7)*S3 + 2772*MASS*r^3*
             (40*MASS^4 - 140*MASS^3*r + 168*MASS^2*r^2 - 84*MASS*r^3 + 
              15*r^4)*S5))*Log[1 - (2*MASS)/r] + 900*(J^2 + M2*MASS)*
         (2*MASS - r)*r^8*(J^3*(448*MASS^7 - 1296*MASS^6*r + 720*MASS^5*r^2 - 
            14226*MASS^4*r^3 + 34842*MASS^3*r^4 - 28224*MASS^2*r^5 + 
            8652*MASS*r^6 - 735*r^7) + 2*J*M2*MASS*(224*MASS^7 - 
            648*MASS^6*r + 360*MASS^5*r^2 - 14253*MASS^4*r^3 + 
            34851*MASS^3*r^4 - 28224*MASS^2*r^5 + 8652*MASS*r^6 - 735*r^7) + 
          21*MASS^2*r^3*(680*MASS^4 - 1660*MASS^3*r + 1344*MASS^2*r^2 - 
            412*MASS*r^3 + 35*r^4)*S3)*Log[1 - (2*MASS)/r]^2))*
     Sin[\[Theta]]^2)/(430080*MASS^15*r^9*(-2*MASS + r)), 0, 0, 
  r^2*Sin[\[Theta]]^2 - (\[Epsilon]p^2*(1 + 3*Cos[2*\[Theta]])*
     (2*MASS*(5*M2*MASS*r^3*(-2*MASS^2 + 3*MASS*r + 3*r^2) + 
        J^2*(16*MASS^5 + 8*MASS^4*r - 10*MASS^2*r^3 + 15*MASS*r^4 + 
          15*r^5)) - 15*(J^2 + M2*MASS)*r^4*(2*MASS^2 - r^2)*
       Log[1 - (2*MASS)/r])*Sin[\[Theta]]^2)/(32*MASS^6*r^2) + 
   (\[Epsilon]p^4*(-6720*MASS^5*r^4*(1 + 3*Cos[2*\[Theta]])*
       (2*MASS*(2*J^2*M20*r*(8*MASS^5 - 4*MASS^4*r - 30*MASS^3*r^2 + 
            90*MASS^2*r^3 + 45*MASS*r^4 - 45*r^5) + 5*MASS*r^3*
           (M2*M20*(-8*MASS^3 + 28*MASS^2*r + 15*MASS*r^2 - 15*r^3) + 
            M42*MASS*(4*MASS^3 - 8*MASS^2*r - 3*MASS*r^2 + 3*r^3)) + 
          2*J*MASS*(-32*MASS^6 + 8*MASS^4*r^2 + 20*MASS^3*r^3 - 
            40*MASS^2*r^4 - 15*MASS*r^5 + 15*r^6)*S31) - 
        15*(2*MASS - r)*r^4*(J^2*M20*(8*MASS^2 - 6*r^2) + 
          MASS*(M2*M20*(6*MASS^2 - 5*r^2) + M42*MASS*(-2*MASS^2 + r^2)) + 
          2*J*MASS*(-2*MASS^2 + r^2)*S31)*Log[1 - (2*MASS)/r]) + 
      60*(1 - 3*Cos[\[Theta]]^2)*
       (4*MASS^2*(J^4*(-8704*MASS^11 + 1024*MASS^10*r + 1152*MASS^9*r^2 + 
            7296*MASS^8*r^3 - 18624*MASS^7*r^4 - 8800*MASS^6*r^5 + 
            5496*MASS^5*r^6 + 27500*MASS^4*r^7 - 114930*MASS^3*r^8 + 
            91380*MASS^2*r^9 - 19845*MASS*r^10 - 225*r^11) - 
          5*MASS^2*r^6*(112*M42*MASS^4*r*(4*MASS^3 - 8*MASS^2*r - 
              3*MASS*r^2 + 3*r^3) - 112*M2*M20*MASS^3*r*(8*MASS^3 - 
              28*MASS^2*r - 15*MASS*r^2 + 15*r^3) + 
            5*M2^2*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
              276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5)) + 
          2*J^2*(-112*M20*MASS^4*r^5*(8*MASS^5 - 4*MASS^4*r - 30*MASS^3*r^2 + 
              90*MASS^2*r^3 + 45*MASS*r^4 - 45*r^5) + M2*MASS*r^3*
             (3520*MASS^8 - 8864*MASS^7*r - 3280*MASS^6*r^2 + 
              4936*MASS^5*r^3 + 28620*MASS^4*r^4 - 120530*MASS^3*r^5 + 
              89280*MASS^2*r^6 - 17745*MASS*r^7 - 225*r^8)) + 
          224*J*MASS^3*(2*MASS - r)*r^4*(-2*MASS^5*S3 + 210*MASS*r^4*S3 - 
            105*r^5*S3 + 16*MASS^7*S31 + 8*MASS^6*r*S31 - 
            2*MASS^4*(3*r*S3 + 5*r^3*S31) + 15*MASS^3*(-(r^2*S3) + r^4*S31) + 
            5*MASS^2*(-10*r^3*S3 + 3*r^5*S31))) - 30*MASS*(2*MASS - r)*r^4*
         (-(J^4*(896*MASS^7 + 64*MASS^6*r - 448*MASS^5*r^2 - 544*MASS^4*r^3 + 
             3790*MASS^3*r^4 - 4784*MASS^2*r^5 + 1353*MASS*r^6 + 30*r^7)) + 
          MASS^2*r^3*(112*M42*MASS^4*r*(2*MASS^2 - r^2) - 112*M2*M20*MASS^3*
             (6*MASS^2*r - 5*r^3) + 5*M2^2*(64*MASS^4 - 198*MASS^3*r + 
              16*MASS^2*r^2 + 99*MASS*r^3 - 6*r^4)) - 
          2*J^2*(112*M20*MASS^4*r^4*(4*MASS^2 - 3*r^2) + 
            M2*MASS*(448*MASS^7 + 32*MASS^6*r - 224*MASS^5*r^2 - 
              432*MASS^4*r^3 + 4070*MASS^3*r^4 - 4784*MASS^2*r^5 + 
              1213*MASS*r^6 + 30*r^7)) + 224*J*MASS^3*r^4*(-21*MASS*r*S3 + 
            7*r^2*S3 + 2*MASS^4*S31 + MASS^2*(15*S3 - r^2*S31)))*
         Log[1 - (2*MASS)/r] - 225*(J^2 + M2*MASS)^2*r^8*
         (32*MASS^5 - 16*MASS^4*r + 8*MASS^3*r^2 - 4*MASS^2*r^3 - 
          2*MASS*r^4 + r^5)*Log[1 - (2*MASS)/r]^2) + 
      (3 - 30*Cos[\[Theta]]^2 + 35*Cos[\[Theta]]^4)*
       (2*MASS*(-10*J^2*M2*MASS*r^3*(5760*MASS^9 - 59712*MASS^8*r - 
            54464*MASS^7*r^2 - 16736*MASS^6*r^3 + 61464*MASS^5*r^4 - 
            462648*MASS^4*r^5 - 1159170*MASS^3*r^6 + 4050900*MASS^2*r^7 - 
            3121245*MASS*r^8 + 721035*r^9) + J^4*(18432*MASS^12 + 
            9216*MASS^11*r + 115200*MASS^10*r^2 - 66048*MASS^9*r^3 + 
            543360*MASS^8*r^4 + 446080*MASS^7*r^5 - 7040*MASS^6*r^6 - 
            256920*MASS^5*r^7 + 1399320*MASS^4*r^8 + 4418250*MASS^3*r^9 - 
            14408100*MASS^2*r^10 + 11019825*MASS*r^11 - 2546775*r^12) + 
          15*MASS^2*r^6*(392*M4*MASS*r*(24*MASS^5 - 272*MASS^4*r - 
              410*MASS^3*r^2 + 1740*MASS^2*r^3 - 1365*MASS*r^4 + 315*r^5) + 
            5*M2^2*(384*MASS^6 - 7368*MASS^5*r + 34024*MASS^4*r^2 + 
              58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 162219*MASS*r^5 - 
              37485*r^6)) - 560*J*MASS^2*r^4*(48*MASS^8 + 176*MASS^7*r + 
            260*MASS^6*r^2 + 96*MASS^5*r^3 + 4062*MASS^4*r^4 + 
            9240*MASS^3*r^5 - 36540*MASS^2*r^6 + 28665*MASS*r^7 - 6615*r^8)*
           S3) - 15*(2*MASS - r)*r^4*(2*J^2*M2*MASS*(10752*MASS^8 + 
            2176*MASS^7*r - 8064*MASS^6*r^2 - 15936*MASS^5*r^3 + 
            88040*MASS^4*r^4 + 74640*MASS^3*r^5 - 734220*MASS^2*r^6 + 
            798990*MASS*r^7 - 240345*r^8) + J^4*(21504*MASS^8 + 
            4352*MASS^7*r - 16128*MASS^6*r^2 - 17952*MASS^5*r^3 + 
            61160*MASS^4*r^4 + 74640*MASS^3*r^5 - 532620*MASS^2*r^6 + 
            563790*MASS*r^7 - 169785*r^8) - 15*MASS^2*r^3*
           (392*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
            M2^2*(928*MASS^5 - 7064*MASS^4*r - 3408*MASS^3*r^2 + 
              38868*MASS^2*r^3 - 41506*MASS*r^4 + 12495*r^5)) - 
          560*J*MASS^2*r^4*(100*MASS^4 + 42*MASS^3*r - 1260*MASS^2*r^2 + 
            1470*MASS*r^3 - 441*r^4)*S3)*Log[1 - (2*MASS)/r] - 
        900*(J^2 + M2*MASS)^2*r^8*(-440*MASS^5 + 220*MASS^4*r + 
          828*MASS^3*r^2 - 792*MASS^2*r^3 + 171*MASS*r^4 + 9*r^5)*
         Log[1 - (2*MASS)/r]^2))*Sin[\[Theta]]^2)/
    (215040*MASS^12*(2*MASS - r)*r^6)}}
