(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{-1 + (2*MASS)/r - (5*M2*\[Epsilon]p*(1 + 3*Cos[2*\[Theta]])*
     (2*MASS*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + 
      3*r^2*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/(32*MASS^5*r^2) + 
   (\[Epsilon]p^2*(256*J^2*MASS^5*(32*MASS^7 - 32*MASS^6*r - 8*MASS^5*r^2 - 
        12*MASS^4*r^3 - 30*MASS^3*r^4 + 110*MASS^2*r^5 - 75*MASS*r^6 + 
        15*r^7) + 2*MASS*r^2*(504*M4*MASS*r*(8*MASS^6 + 68*MASS^5*r - 
          996*MASS^4*r^2 + 2200*MASS^3*r^3 - 1910*MASS^2*r^4 + 735*MASS*r^5 - 
          105*r^6) - 5*M2^2*(544*MASS^7 - 328*MASS^6*r + 12700*MASS^5*r^2 - 
          153900*MASS^4*r^3 + 339504*MASS^3*r^4 - 294246*MASS^2*r^5 + 
          112815*MASS*r^6 - 16065*r^7)) - 15*(2*MASS - r)*r^3*
       (M2^2*(288*MASS^6 - 544*MASS^5*r + 20712*MASS^4*r^2 - 
          64592*MASS^3*r^3 + 70554*MASS^2*r^4 - 32370*MASS*r^5 + 5355*r^6) + 
        8*MASS*r^2*(-2*MASS + r)^2*(16*J^2*MASS^3 - 
          63*M4*(6*MASS^2 - 14*MASS*r + 7*r^2)))*Log[1 - (2*MASS)/r] + 
      180*M2^2*(2*MASS - r)^3*r^5*(15*MASS^2 - 33*MASS*r + 5*r^2)*
       Log[1 - (2*MASS)/r]^2 - 2*(2*MASS - r)*Cos[\[Theta]]^2*
       (128*J^2*MASS^5*(48*MASS^6 - 8*MASS^5*r - 24*MASS^4*r^2 - 
          30*MASS^3*r^3 - 60*MASS^2*r^4 + 135*MASS*r^5 - 45*r^6) + 
        30*MASS*(MASS - r)*r^2*(168*M4*MASS*r*(4*MASS^4 + 40*MASS^3*r - 
            440*MASS^2*r^2 + 420*MASS*r^3 - 105*r^4) - 
          5*M2^2*(32*MASS^5 + 348*MASS^4*r + 2736*MASS^3*r^2 - 
            22980*MASS^2*r^3 + 21492*MASS*r^4 - 5355*r^5)) + 
        45*r^3*(5*M2^2*(32*MASS^6 + 160*MASS^5*r - 7432*MASS^4*r^2 + 
            21856*MASS^3*r^3 - 23466*MASS^2*r^4 + 10758*MASS*r^5 - 
            1785*r^6) - 8*MASS*r^2*(-2*MASS + r)^2*(8*J^2*MASS^3 - 
            105*M4*(6*MASS^2 - 14*MASS*r + 7*r^2)))*Log[1 - (2*MASS)/r] + 
        2700*M2^2*r^5*(-2*MASS + r)^2*(7*MASS^2 - 9*MASS*r + r^2)*
         Log[1 - (2*MASS)/r]^2) - 15*r^2*Cos[\[Theta]]^4*
       (2*MASS*(MASS - r)*(-392*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 
            920*MASS^3*r^2 + 1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
          5*M2^2*(160*MASS^6 + 2056*MASS^5*r + 12476*MASS^4*r^2 - 
            114664*MASS^3*r^3 + 154336*MASS^2*r^4 - 75186*MASS*r^5 + 
            12495*r^6)) - 5*(2*MASS - r)*r*(1176*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(352*MASS^6 + 672*MASS^5*r - 
            51816*MASS^4*r^2 + 153456*MASS^3*r^3 - 164682*MASS^2*r^4 + 
            75402*MASS*r^5 - 12495*r^6))*Log[1 - (2*MASS)/r] - 
        180*M2^2*(2*MASS - r)^3*r^3*(13*MASS^2 - 19*MASS*r + 3*r^2)*
         Log[1 - (2*MASS)/r]^2)))/(2048*MASS^10*r^5*(-2*MASS + r)), 0, 0, 
  (-2*J*\[Epsilon]p*Sin[\[Theta]]^2)/r - 
   (\[Epsilon]p^2*(2*MASS*(2*J*M2*(20*MASS^5 + 48*MASS^4*r + 10*MASS^3*r^2 + 
          270*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
        7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
          45*r^4)*S3) + 15*r^2*(2*J*M2*(4*MASS^4 - 8*MASS^3*r + 
          60*MASS^2*r^2 - 70*MASS*r^3 + 21*r^4) - 
        7*MASS*r^2*(8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*Log[1 - (2*MASS)/r] - 
      5*Cos[\[Theta]]^2*(2*MASS*(2*J*M2*(12*MASS^5 + 88*MASS^4*r + 
            70*MASS^3*r^2 + 210*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
          7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
            45*r^4)*S3) + 3*r*(2*J*M2*(-16*MASS^5 + 20*MASS^4*r + 
            280*MASS^2*r^3 - 350*MASS*r^4 + 105*r^5) - 
          35*MASS*r^3*(8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*
         Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/(64*MASS^8*r^2)}, 
 {0, -(r/(2*MASS - r)) - (5*M2*\[Epsilon]p*(-1 + 3*Cos[\[Theta]]^2)*
     (2*MASS*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + 
      3*r^2*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/
    (16*MASS^5*(-2*MASS + r)^2) + 
   (\[Epsilon]p^2*(-256*J^2*MASS^5*(160*MASS^7 - 224*MASS^6*r + 
        88*MASS^5*r^2 + 12*MASS^4*r^3 + 30*MASS^3*r^4 - 110*MASS^2*r^5 + 
        75*MASS*r^6 - 15*r^7) + 2*MASS*(MASS - r)*r^2*
       (504*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 920*MASS^3*r^2 + 
          1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
        5*M2^2*(224*MASS^6 + 3112*MASS^5*r - 8436*MASS^4*r^2 + 
          135096*MASS^3*r^3 - 195192*MASS^2*r^4 + 96750*MASS*r^5 - 
          16065*r^6)) - 15*(2*MASS - r)*r^3*
       (M2^2*(288*MASS^6 - 1312*MASS^5*r + 18024*MASS^4*r^2 - 
          59984*MASS^3*r^3 + 69018*MASS^2*r^4 - 32370*MASS*r^5 + 5355*r^6) + 
        8*MASS*r^2*(-2*MASS + r)^2*(16*J^2*MASS^3 - 
          63*M4*(6*MASS^2 - 14*MASS*r + 7*r^2)))*Log[1 - (2*MASS)/r] + 
      180*M2^2*(2*MASS - r)^3*r^5*(15*MASS^2 - MASS*r + 5*r^2)*
       Log[1 - (2*MASS)/r]^2 + 15*r^2*Cos[\[Theta]]^4*
       (2*MASS*(MASS - r)*(392*M4*MASS*r*(8*MASS^5 + 76*MASS^4*r - 
            920*MASS^3*r^2 + 1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) - 
          5*M2^2*(32*MASS^6 + 1416*MASS^5*r + 12476*MASS^4*r^2 - 
            112744*MASS^3*r^3 + 152896*MASS^2*r^4 - 74898*MASS*r^5 + 
            12495*r^6)) + 5*(2*MASS - r)*r*(1176*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) + M2^2*(352*MASS^6 + 
            1440*MASS^5*r - 50664*MASS^4*r^2 + 149232*MASS^3*r^3 - 
            161802*MASS^2*r^4 + 74826*MASS*r^5 - 12495*r^6))*
         Log[1 - (2*MASS)/r] + 180*M2^2*(2*MASS - r)^3*r^3*
         (13*MASS^2 - 11*MASS*r - r^2)*Log[1 - (2*MASS)/r]^2) + 
      6*Cos[\[Theta]]^2*(2*MASS*(64*J^2*MASS^4*(160*MASS^7 - 192*MASS^6*r + 
            72*MASS^5*r^2 + 12*MASS^4*r^3 + 30*MASS^3*r^4 - 110*MASS^2*r^5 + 
            75*MASS*r^6 - 15*r^7) + 5*(MASS - r)*r^3*
           (-168*M4*MASS*(8*MASS^5 + 76*MASS^4*r - 920*MASS^3*r^2 + 
              1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 
            5*M2^2*(344*MASS^5 + 5124*MASS^4*r - 47736*MASS^3*r^2 + 
              65244*MASS^2*r^3 - 32058*MASS*r^4 + 5355*r^5))) + 
        15*(2*MASS - r)*r^3*(-5*M2^2*(32*MASS^6 + 288*MASS^5*r - 
            7240*MASS^4*r^2 + 21152*MASS^3*r^3 - 22986*MASS^2*r^4 + 
            10662*MASS*r^5 - 1785*r^6) + 8*MASS*r^2*(-2*MASS + r)^2*
           (8*J^2*MASS^3 - 105*M4*(6*MASS^2 - 14*MASS*r + 7*r^2)))*
         Log[1 - (2*MASS)/r] - 900*M2^2*(2*MASS - r)^3*r^5*
         (7*MASS^2 - 5*MASS*r - r^2)*Log[1 - (2*MASS)/r]^2)))/
    (2048*MASS^10*r^3*(-2*MASS + r)^3), 0, 0}, 
 {0, 0, r^2 - (5*M2*r*\[Epsilon]p*(-1 + 3*Cos[\[Theta]]^2)*
     (-4*MASS^3 + 6*MASS^2*r + 6*MASS*r^2 + (-6*MASS^2*r + 3*r^3)*
       Log[1 - (2*MASS)/r]))/(16*MASS^5) + 
   (\[Epsilon]p^2*(-2240*M22*MASS^5*r*(1 + 3*Cos[2*\[Theta]])*
       (2*MASS*(2*MASS^2 - 3*MASS*r - 3*r^2) + (6*MASS^2*r - 3*r^3)*
         Log[1 - (2*MASS)/r]) - (2*(1 + 3*Cos[2*\[Theta]])*
        (448*J^2*MASS^5*(32*MASS^6 - 8*MASS^4*r^2 - 20*MASS^3*r^3 + 
           40*MASS^2*r^4 + 15*MASS*r^5 - 15*r^6) - 20*MASS^2*r^2*
          (112*M22*MASS^4*r*(4*MASS^3 - 8*MASS^2*r - 3*MASS*r^2 + 3*r^3) + 
           5*M2^2*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
             276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5)) - 30*MASS*(2*MASS - r)*
          r^3*(112*MASS^3*(J^2 + M22*MASS)*r*(2*MASS^2 - r^2) + 
           5*M2^2*(64*MASS^4 - 198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 
             6*r^4))*Log[1 - (2*MASS)/r] - 225*M2^2*r^4*
          (32*MASS^5 - 16*MASS^4*r + 8*MASS^3*r^2 - 4*MASS^2*r^3 - 
           2*MASS*r^4 + r^5)*Log[1 - (2*MASS)/r]^2))/((2*MASS - r)*r^2) + 
      ((3 - 30*Cos[\[Theta]]^2 + 35*Cos[\[Theta]]^4)*
        (2*MASS*(392*M4*MASS*r*(24*MASS^5 - 272*MASS^4*r - 410*MASS^3*r^2 + 
             1740*MASS^2*r^3 - 1365*MASS*r^4 + 315*r^5) + 
           5*M2^2*(384*MASS^6 - 7368*MASS^5*r + 34024*MASS^4*r^2 + 
             58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 162219*MASS*r^5 - 
             37485*r^6)) + 15*(2*MASS - r)*r*
          (392*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
           M2^2*(928*MASS^5 - 7064*MASS^4*r - 3408*MASS^3*r^2 + 
             38868*MASS^2*r^3 - 41506*MASS*r^4 + 12495*r^5))*
          Log[1 - (2*MASS)/r] - 60*M2^2*r^2*(-440*MASS^5 + 220*MASS^4*r + 
           828*MASS^3*r^2 - 792*MASS^2*r^3 + 171*MASS*r^4 + 9*r^5)*
          Log[1 - (2*MASS)/r]^2))/(2*MASS - r)))/(14336*MASS^10), 0}, 
 {(-2*J*\[Epsilon]p*Sin[\[Theta]]^2)/r - 
   (\[Epsilon]p^2*(2*MASS*(2*J*M2*(20*MASS^5 + 48*MASS^4*r + 10*MASS^3*r^2 + 
          270*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
        7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
          45*r^4)*S3) + 15*r^2*(2*J*M2*(4*MASS^4 - 8*MASS^3*r + 
          60*MASS^2*r^2 - 70*MASS*r^3 + 21*r^4) - 
        7*MASS*r^2*(8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*Log[1 - (2*MASS)/r] - 
      5*Cos[\[Theta]]^2*(2*MASS*(2*J*M2*(12*MASS^5 + 88*MASS^4*r + 
            70*MASS^3*r^2 + 210*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
          7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
            45*r^4)*S3) + 3*r*(2*J*M2*(-16*MASS^5 + 20*MASS^4*r + 
            280*MASS^2*r^3 - 350*MASS*r^4 + 105*r^5) - 
          35*MASS*r^3*(8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*
         Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/(64*MASS^8*r^2), 0, 0, 
  r^2*Sin[\[Theta]]^2 - (5*M2*r*\[Epsilon]p*(1 + 3*Cos[2*\[Theta]])*
     (-4*MASS^3 + 6*MASS^2*r + 6*MASS*r^2 + (-6*MASS^2*r + 3*r^3)*
       Log[1 - (2*MASS)/r])*Sin[\[Theta]]^2)/(32*MASS^5) - 
   (\[Epsilon]p^2*(2240*M22*MASS^5*(2*MASS - r)*r^3*(1 + 3*Cos[2*\[Theta]])*
       (2*MASS*(2*MASS^2 - 3*MASS*r - 3*r^2) + (6*MASS^2*r - 3*r^3)*
         Log[1 - (2*MASS)/r]) + 4*(1 - 3*Cos[\[Theta]]^2)*
       (-448*J^2*MASS^5*(32*MASS^6 - 8*MASS^4*r^2 - 20*MASS^3*r^3 + 
          40*MASS^2*r^4 + 15*MASS*r^5 - 15*r^6) + 20*MASS^2*r^2*
         (112*M22*MASS^4*r*(4*MASS^3 - 8*MASS^2*r - 3*MASS*r^2 + 3*r^3) + 
          5*M2^2*(40*MASS^5 - 428*MASS^4*r + 834*MASS^3*r^2 + 
            276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5)) + 30*MASS*(2*MASS - r)*
         r^3*(112*MASS^3*(J^2 + M22*MASS)*r*(2*MASS^2 - r^2) + 
          5*M2^2*(64*MASS^4 - 198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 
            6*r^4))*Log[1 - (2*MASS)/r] + 225*M2^2*r^4*
         (32*MASS^5 - 16*MASS^4*r + 8*MASS^3*r^2 - 4*MASS^2*r^3 - 
          2*MASS*r^4 + r^5)*Log[1 - (2*MASS)/r]^2) - 
      r^2*(3 - 30*Cos[\[Theta]]^2 + 35*Cos[\[Theta]]^4)*
       (2*MASS*(392*M4*MASS*r*(24*MASS^5 - 272*MASS^4*r - 410*MASS^3*r^2 + 
            1740*MASS^2*r^3 - 1365*MASS*r^4 + 315*r^5) + 
          5*M2^2*(384*MASS^6 - 7368*MASS^5*r + 34024*MASS^4*r^2 + 
            58798*MASS^3*r^3 - 211596*MASS^2*r^4 + 162219*MASS*r^5 - 
            37485*r^6)) + 15*(2*MASS - r)*r*
         (392*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 70*MASS*r^3 - 21*r^4) + 
          M2^2*(928*MASS^5 - 7064*MASS^4*r - 3408*MASS^3*r^2 + 
            38868*MASS^2*r^3 - 41506*MASS*r^4 + 12495*r^5))*
         Log[1 - (2*MASS)/r] - 60*M2^2*r^2*(-440*MASS^5 + 220*MASS^4*r + 
          828*MASS^3*r^2 - 792*MASS^2*r^3 + 171*MASS*r^4 + 9*r^5)*
         Log[1 - (2*MASS)/r]^2))*Sin[\[Theta]]^2)/(14336*MASS^10*(2*MASS - r)*
     r^2)}}
