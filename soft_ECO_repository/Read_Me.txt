
Particular solutions discussed in "Exotic compact objects with soft hair", Guilherme Raposo, Paolo Pani, Roberto Emparan, arxiv:XXXXXXX;

Description of the files:
The prefix in the file name denotes the solution according to the adopted nomenclature. For example, file JS2M2-solution contains the solution [JS_2M_2]
presented in the paper. 





Each file contains a Mathematica table containing the form of the covariant metric of the respective solution up to a given order. The coordinates are 
respectively (t,r,\theta, \phi)




Nomenclature:

"Mass"      Physical Mass of the object;
"J"         Physical Angular momentum of the object;
"M2"        Physical mass quadrupole of the object;
"S2"        Physical current quadrupole of the object;
"M\ell"     Physical mass \ell-pole of the object;
"S\ell"     Physical current \ell-pole of the object;
"\epsilonp" Bookeeping parameter that denotes the order in order in perturbation theory;  


