(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{-1 + (2*MASS)/r - (5*M2*\[Epsilon]p*(1 + 3*Cos[2*\[Theta]])*
     (2*MASS*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + 
      3*r^2*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/(32*MASS^5*r^2) + 
   (\[Epsilon]p^2*(6*MASS*(128*J^2*MASS^4*(32*MASS^7 - 32*MASS^6*r - 
          8*MASS^5*r^2 - 12*MASS^4*r^3 - 30*MASS^3*r^4 + 110*MASS^2*r^5 - 
          75*MASS*r^6 + 15*r^7) + 5*M2^2*r^2*(-544*MASS^7 + 328*MASS^6*r - 
          12700*MASS^5*r^2 + 153900*MASS^4*r^3 - 339504*MASS^3*r^4 + 
          294246*MASS^2*r^5 - 112815*MASS*r^6 + 16065*r^7) + 
        (2*MASS - r)*(504*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 
            480*MASS^3*r^2 + 860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) + 
          5*(64*MASS^8 + 32*MASS^7*r + 1452*MASS^5*r^3 + 4844*MASS^4*r^4 - 
            61080*MASS^3*r^5 + 103164*MASS^2*r^6 - 61785*MASS*r^7 + 
            12285*r^8)*S2^2)) - 45*(2*MASS - r)*r^3*(512*J^2*MASS^6*r^2 - 
        512*J^2*MASS^5*r^3 + 128*J^2*MASS^4*r^4 - 504*M4*MASS*r^2*
         (-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
        M2^2*(288*MASS^6 - 544*MASS^5*r + 20712*MASS^4*r^2 - 
          64592*MASS^3*r^3 + 70554*MASS^2*r^4 - 32370*MASS*r^5 + 5355*r^6) + 
        288*MASS^6*S2^2 - 96*MASS^5*r*S2^2 - 17096*MASS^4*r^2*S2^2 + 
        50896*MASS^3*r^3*S2^2 - 54386*MASS^2*r^4*S2^2 + 24810*MASS*r^5*S2^2 - 
        4095*r^6*S2^2)*Log[1 - (2*MASS)/r] + 540*r^5*(-2*MASS + r)^2*
       (M2^2*(30*MASS^3 - 81*MASS^2*r + 43*MASS*r^2 - 5*r^3) + 
        (-30*MASS^3 + 45*MASS^2*r - 27*MASS*r^2 + 5*r^3)*S2^2)*
       Log[1 - (2*MASS)/r]^2 + 384*MASS^2*Cos[\[Theta]]*
       (2*MASS*(21*M3*MASS*r^3*(4*MASS^5 + 18*MASS^4*r - 140*MASS^3*r^2 + 
            185*MASS^2*r^3 - 90*MASS*r^4 + 15*r^5) + 
          2*J*(-24*MASS^8 + 8*MASS^7*r + 40*MASS^6*r^2 + 104*MASS^5*r^3 + 
            418*MASS^4*r^4 - 3030*MASS^3*r^5 + 3915*MASS^2*r^6 - 
            1890*MASS*r^7 + 315*r^8)*S2) + 15*r^5*(-2*MASS + r)^2*
         (21*M3*MASS*(2*MASS^2 - 3*MASS*r + r^2) + 
          2*J*(44*MASS^2 - 63*MASS*r + 21*r^2)*S2)*Log[1 - (2*MASS)/r]) - 
      1920*MASS^2*(2*MASS - r)*Cos[\[Theta]]^3*
       (2*MASS*(7*M3*MASS*r^3*(2*MASS^4 + 10*MASS^3*r - 65*MASS^2*r^2 + 
            60*MASS*r^3 - 15*r^4) + 2*J*(-4*MASS^7 - 2*MASS^6*r + 
            6*MASS^5*r^2 + 23*MASS^4*r^3 + 61*MASS^3*r^4 - 455*MASS^2*r^5 + 
            420*MASS*r^6 - 105*r^7)*S2) + 3*(2*MASS - r)*r^4*
         (35*M3*MASS*r*(2*MASS^2 - 3*MASS*r + r^2) + 
          2*J*(3*MASS^3 + 70*MASS^2*r - 105*MASS*r^2 + 35*r^3)*S2)*
         Log[1 - (2*MASS)/r]) - 6*(2*MASS - r)*Cos[\[Theta]]^2*
       (128*J^2*MASS^5*(48*MASS^6 - 8*MASS^5*r - 24*MASS^4*r^2 - 
          30*MASS^3*r^3 - 60*MASS^2*r^4 + 135*MASS*r^5 - 45*r^6) + 
        30*MASS*(MASS - r)*r^2*(168*M4*MASS*r*(4*MASS^4 + 40*MASS^3*r - 
            440*MASS^2*r^2 + 420*MASS*r^3 - 105*r^4) - 
          5*M2^2*(32*MASS^5 + 348*MASS^4*r + 2736*MASS^3*r^2 - 
            22980*MASS^2*r^3 + 21492*MASS*r^4 - 5355*r^5) + 
          5*(32*MASS^5 + 364*MASS^4*r + 2160*MASS^3*r^2 - 17652*MASS^2*r^3 + 
            16452*MASS*r^4 - 4095*r^5)*S2^2) + 
        45*r^3*(-256*J^2*MASS^6*r^2 + 256*J^2*MASS^5*r^3 - 
          64*J^2*MASS^4*r^4 + 840*M4*MASS*r^2*(-2*MASS + r)^2*
           (6*MASS^2 - 14*MASS*r + 7*r^2) + 5*M2^2*(32*MASS^6 + 
            160*MASS^5*r - 7432*MASS^4*r^2 + 21856*MASS^3*r^3 - 
            23466*MASS^2*r^4 + 10758*MASS*r^5 - 1785*r^6) - 160*MASS^6*S2^2 - 
          160*MASS^5*r*S2^2 + 27400*MASS^4*r^2*S2^2 - 83040*MASS^3*r^3*S2^2 + 
          89770*MASS^2*r^4*S2^2 - 41190*MASS*r^5*S2^2 + 6825*r^6*S2^2)*
         Log[1 - (2*MASS)/r] + 2700*r^5*(-2*MASS + r)^2*
         (M2^2*(7*MASS^2 - 9*MASS*r + r^2) - (3*MASS^2 - 5*MASS*r + r^2)*
           S2^2)*Log[1 - (2*MASS)/r]^2) - 5*Cos[\[Theta]]^4*
       (90*M2^2*MASS*r^2*(160*MASS^7 + 1896*MASS^6*r + 10420*MASS^5*r^2 - 
          127140*MASS^4*r^3 + 269000*MASS^3*r^4 - 229522*MASS^2*r^5 + 
          87681*MASS*r^6 - 12495*r^7) - 2*MASS*(2*MASS - r)*
         (3528*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 480*MASS^3*r^2 + 
            860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) - 
          5*(192*MASS^8 + 224*MASS^7*r - 832*MASS^6*r^2 - 5988*MASS^5*r^3 - 
            33396*MASS^4*r^4 + 410832*MASS^3*r^5 - 715932*MASS^2*r^6 + 
            431919*MASS*r^7 - 85995*r^8)*S2^2) - 15*(2*MASS - r)*r^3*
         (3528*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
          3*M2^2*(352*MASS^6 + 672*MASS^5*r - 51816*MASS^4*r^2 + 
            153456*MASS^3*r^3 - 164682*MASS^2*r^4 + 75402*MASS*r^5 - 
            12495*r^6) + (-96*MASS^6 + 416*MASS^5*r + 110552*MASS^4*r^2 - 
            346704*MASS^3*r^3 + 377574*MASS^2*r^4 - 173286*MASS*r^5 + 
            28665*r^6)*S2^2)*Log[1 - (2*MASS)/r] + 180*r^5*(-2*MASS + r)^2*
         (-9*M2^2*(26*MASS^3 - 51*MASS^2*r + 25*MASS*r^2 - 3*r^3) + 
          (90*MASS^3 - 255*MASS^2*r + 161*MASS*r^2 - 27*r^3)*S2^2)*
         Log[1 - (2*MASS)/r]^2)))/(6144*MASS^10*r^5*(-2*MASS + r)), 0, 0, 
  (\[Epsilon]p*(-16*J*MASS^5 + 5*S2*Cos[\[Theta]]*
       (2*MASS*(2*MASS^3 + 2*MASS^2*r + 3*MASS*r^2 - 3*r^3) + 
        3*(2*MASS - r)*r^3*Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/
    (8*MASS^5*r) - (\[Epsilon]p^2*(-40*MASS^2*Cos[\[Theta]]^2*
       (2*MASS*(2*J*M2*(12*MASS^5 + 88*MASS^4*r + 70*MASS^3*r^2 + 
            210*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
          7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
            45*r^4)*S3) + 3*r*(2*J*M2*(-16*MASS^5 + 20*MASS^4*r + 
            280*MASS^2*r^3 - 350*MASS*r^4 + 105*r^5) - 
          35*MASS*r^3*(8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*
         Log[1 - (2*MASS)/r]) + 8*MASS^2*
       (2*MASS*(2*J*M2*(20*MASS^5 + 48*MASS^4*r + 10*MASS^3*r^2 + 
            270*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
          7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
            45*r^4)*S3) + 15*(2*J*M2*r^2*(4*MASS^4 - 8*MASS^3*r + 
            60*MASS^2*r^2 - 70*MASS*r^3 + 21*r^4) - 7*MASS*r^4*
           (8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*Log[1 - (2*MASS)/r]) + 
      Cos[\[Theta]]*(-50*M2*MASS*(16*MASS^6 + 180*MASS^5*r + 586*MASS^4*r^2 + 
          3006*MASS^3*r^3 - 19059*MASS^2*r^4 + 19917*MASS*r^5 - 5670*r^6)*
         S2 + 504*MASS^2*r*(4*MASS^5 + 18*MASS^4*r + 90*MASS^3*r^2 - 
          685*MASS^2*r^3 + 735*MASS*r^4 - 210*r^5)*S4 + 
        15*(5*M2*r*(32*MASS^6 + 48*MASS^5*r + 208*MASS^4*r^2 - 
            6224*MASS^3*r^3 + 12614*MASS^2*r^4 - 8553*MASS*r^5 + 1890*r^6)*
           S2 + 252*MASS*r^4*(40*MASS^3 - 90*MASS^2*r + 63*MASS*r^2 - 14*r^3)*
           S4)*Log[1 - (2*MASS)/r] + 450*M2*r^4*(40*MASS^3 - 58*MASS^2*r + 
          23*MASS*r^2 - 2*r^3)*S2*Log[1 - (2*MASS)/r]^2) + 
      3*Cos[\[Theta]]^3*(2*MASS*(25*M2*(16*MASS^6 + 188*MASS^5*r + 
            486*MASS^4*r^2 + 2298*MASS^3*r^3 - 14853*MASS^2*r^4 + 
            15507*MASS*r^5 - 4410*r^6)*S2 - 196*MASS*r*(4*MASS^5 + 
            18*MASS^4*r + 90*MASS^3*r^2 - 685*MASS^2*r^3 + 735*MASS*r^4 - 
            210*r^5)*S4) - 15*(5*M2*r*(32*MASS^6 + 16*MASS^5*r + 
            112*MASS^4*r^2 - 4768*MASS^3*r^3 + 9810*MASS^2*r^4 - 
            6663*MASS*r^5 + 1470*r^6)*S2 + 196*MASS*r^4*(40*MASS^3 - 
            90*MASS^2*r + 63*MASS*r^2 - 14*r^3)*S4)*Log[1 - (2*MASS)/r] + 
        450*M2*r^4*(-24*MASS^3 + 38*MASS^2*r - 17*MASS*r^2 + 2*r^3)*S2*
         Log[1 - (2*MASS)/r]^2))*Sin[\[Theta]]^2)/(512*MASS^10*r^2)}, 
 {0, -(r/(2*MASS - r)) - (5*M2*\[Epsilon]p*(-1 + 3*Cos[\[Theta]]^2)*
     (2*MASS*(2*MASS^3 + 4*MASS^2*r - 9*MASS*r^2 + 3*r^3) + 
      3*r^2*(-2*MASS + r)^2*Log[1 - (2*MASS)/r]))/
    (16*MASS^5*(-2*MASS + r)^2) + 
   (\[Epsilon]p^2*(1152*MASS^2*Cos[\[Theta]]*
       (2*MASS*(7*M3*MASS*r^3*(4*MASS^5 + 18*MASS^4*r - 140*MASS^3*r^2 + 
            185*MASS^2*r^3 - 90*MASS*r^4 + 15*r^5) + 
          2*J*(40*MASS^8 - 40*MASS^7*r + 8*MASS^6*r^2 + 28*MASS^5*r^3 + 
            126*MASS^4*r^4 - 980*MASS^3*r^5 + 1295*MASS^2*r^6 - 
            630*MASS*r^7 + 105*r^8)*S2) + 105*(MASS - r)*(2*MASS - r)^3*r^5*
         (M3*MASS + 2*J*S2)*Log[1 - (2*MASS)/r]) - 1920*MASS^2*(2*MASS - r)*
       Cos[\[Theta]]^3*(2*MASS*(7*M3*MASS*r^3*(2*MASS^4 + 10*MASS^3*r - 
            65*MASS^2*r^2 + 60*MASS*r^3 - 15*r^4) + 
          2*J*(20*MASS^7 - 6*MASS^6*r + 2*MASS^5*r^2 + 17*MASS^4*r^3 + 
            67*MASS^3*r^4 - 455*MASS^2*r^5 + 420*MASS*r^6 - 105*r^7)*S2) + 
        3*(2*MASS - r)*r^4*(35*M3*MASS*r*(2*MASS^2 - 3*MASS*r + r^2) + 
          2*J*(MASS^3 + 70*MASS^2*r - 105*MASS*r^2 + 35*r^3)*S2)*
         Log[1 - (2*MASS)/r]) + 18*Cos[\[Theta]]^2*
       (2*MASS*(64*J^2*MASS^4*(160*MASS^7 - 192*MASS^6*r + 72*MASS^5*r^2 + 
            12*MASS^4*r^3 + 30*MASS^3*r^4 - 110*MASS^2*r^5 + 75*MASS*r^6 - 
            15*r^7) - 5*(MASS - r)*r^2*(-5*M2^2*r*(344*MASS^5 + 
              5124*MASS^4*r - 47736*MASS^3*r^2 + 65244*MASS^2*r^3 - 
              32058*MASS*r^4 + 5355*r^5) + (2*MASS - r)*
             (168*M4*MASS*r*(4*MASS^4 + 40*MASS^3*r - 440*MASS^2*r^2 + 
                420*MASS*r^3 - 105*r^4) + 5*(32*MASS^5 + 364*MASS^4*r + 
                2160*MASS^3*r^2 - 17652*MASS^2*r^3 + 16452*MASS*r^4 - 
                4095*r^5)*S2^2))) + 15*(2*MASS - r)*r^3*(256*J^2*MASS^6*r^2 - 
          256*J^2*MASS^5*r^3 + 64*J^2*MASS^4*r^4 - 840*M4*MASS*r^2*
           (-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) - 
          5*M2^2*(32*MASS^6 + 288*MASS^5*r - 7240*MASS^4*r^2 + 
            21152*MASS^3*r^3 - 22986*MASS^2*r^4 + 10662*MASS*r^5 - 
            1785*r^6) + 160*MASS^6*S2^2 + 160*MASS^5*r*S2^2 - 
          27400*MASS^4*r^2*S2^2 + 83040*MASS^3*r^3*S2^2 - 
          89770*MASS^2*r^4*S2^2 + 41190*MASS*r^5*S2^2 - 6825*r^6*S2^2)*
         Log[1 - (2*MASS)/r] - 900*(2*MASS - r)^3*r^5*
         (M2^2*(7*MASS^2 - 5*MASS*r - r^2) - (3*MASS^2 - 5*MASS*r + r^2)*
           S2^2)*Log[1 - (2*MASS)/r]^2) - 
      3*(2*MASS*(128*J^2*MASS^4*(160*MASS^7 - 224*MASS^6*r + 88*MASS^5*r^2 + 
            12*MASS^4*r^3 + 30*MASS^3*r^4 - 110*MASS^2*r^5 + 75*MASS*r^6 - 
            15*r^7) - 5*M2^2*r^2*(224*MASS^7 + 2888*MASS^6*r - 
            11548*MASS^5*r^2 + 143532*MASS^4*r^3 - 330288*MASS^3*r^4 + 
            291942*MASS^2*r^5 - 112815*MASS*r^6 + 16065*r^7) - 
          (2*MASS - r)*(504*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 
              480*MASS^3*r^2 + 860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) - 
            5*(320*MASS^8 - 96*MASS^7*r - 384*MASS^6*r^2 - 2156*MASS^5*r^3 - 
              6860*MASS^4*r^4 + 61320*MASS^3*r^5 - 101436*MASS^2*r^6 + 
              61065*MASS*r^7 - 12285*r^8)*S2^2)) + 15*(2*MASS - r)*r^3*
         (512*J^2*MASS^6*r^2 - 512*J^2*MASS^5*r^3 + 128*J^2*MASS^4*r^4 - 
          504*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
          M2^2*(288*MASS^6 - 1312*MASS^5*r + 18024*MASS^4*r^2 - 
            59984*MASS^3*r^3 + 69018*MASS^2*r^4 - 32370*MASS*r^5 + 
            5355*r^6) + 288*MASS^6*S2^2 + 32*MASS^5*r*S2^2 - 
          18184*MASS^4*r^2*S2^2 + 50064*MASS^3*r^3*S2^2 - 
          52754*MASS^2*r^4*S2^2 + 24330*MASS*r^5*S2^2 - 4095*r^6*S2^2)*
         Log[1 - (2*MASS)/r] + 180*r^5*(-2*MASS + r)^2*
         (M2^2*(-30*MASS^3 + 17*MASS^2*r - 11*MASS*r^2 + 5*r^3) + 
          (30*MASS^3 - 45*MASS^2*r + 3*MASS*r^2 + 5*r^3)*S2^2)*
         Log[1 - (2*MASS)/r]^2) - 5*Cos[\[Theta]]^4*
       (90*M2^2*MASS*r^2*(32*MASS^7 + 1384*MASS^6*r + 11060*MASS^5*r^2 - 
          125220*MASS^4*r^3 + 265640*MASS^3*r^4 - 227794*MASS^2*r^5 + 
          87393*MASS*r^6 - 12495*r^7) - 2*MASS*(2*MASS - r)*
         (3528*M4*MASS*r^3*(4*MASS^5 + 36*MASS^4*r - 480*MASS^3*r^2 + 
            860*MASS^2*r^3 - 525*MASS*r^4 + 105*r^5) + 
          5*(960*MASS^8 + 96*MASS^7*r + 448*MASS^6*r^2 + 4516*MASS^5*r^3 + 
            33492*MASS^4*r^4 - 410304*MASS^3*r^5 + 718812*MASS^2*r^6 - 
            433503*MASS*r^7 + 85995*r^8)*S2^2) - 15*(2*MASS - r)*r^3*
         (3528*M4*MASS*r^2*(-2*MASS + r)^2*(6*MASS^2 - 14*MASS*r + 7*r^2) + 
          3*M2^2*(352*MASS^6 + 1440*MASS^5*r - 50664*MASS^4*r^2 + 
            149232*MASS^3*r^3 - 161802*MASS^2*r^4 + 74826*MASS*r^5 - 
            12495*r^6) + (-96*MASS^6 - 480*MASS^5*r + 109976*MASS^4*r^2 - 
            347920*MASS^3*r^3 + 380550*MASS^2*r^4 - 174342*MASS*r^5 + 
            28665*r^6)*S2^2)*Log[1 - (2*MASS)/r] - 180*r^5*(-2*MASS + r)^2*
         (9*M2^2*(26*MASS^3 - 35*MASS^2*r + 9*MASS*r^2 + r^3) + 
          (-90*MASS^3 + 255*MASS^2*r - 201*MASS*r^2 + 49*r^3)*S2^2)*
         Log[1 - (2*MASS)/r]^2)))/(6144*MASS^10*r^3*(-2*MASS + r)^3), 0, 0}, 
 {0, 0, r^2 - (5*M2*r*\[Epsilon]p*(-1 + 3*Cos[\[Theta]]^2)*
     (-4*MASS^3 + 6*MASS^2*r + 6*MASS*r^2 + (-6*MASS^2*r + 3*r^3)*
       Log[1 - (2*MASS)/r]))/(16*MASS^5) + 
   (\[Epsilon]p^2*(-6720*M22*MASS^5*r^3*(1 + 3*Cos[2*\[Theta]])*
       (2*MASS*(2*MASS^2 - 3*MASS*r - 3*r^2) + (6*MASS^2*r - 3*r^3)*
         Log[1 - (2*MASS)/r]) + 2688*MASS^2*Cos[\[Theta]]*
       (-3 + 5*Cos[\[Theta]]^2)*
       (2*MASS*(-7*M3*MASS*r^3*(2*MASS^3 - 10*MASS^2*r - 15*MASS*r^2 + 
            15*r^3) + 2*J*(4*MASS^6 + 8*MASS^5*r + 6*MASS^4*r^2 - 
            8*MASS^3*r^3 + 70*MASS^2*r^4 + 105*MASS*r^5 - 105*r^6)*S2) - 
        3*r^4*(7*M3*MASS*(4*MASS^3 - 10*MASS*r^2 + 5*r^3) + 
          2*J*(26*MASS^3 - 70*MASS*r^2 + 35*r^3)*S2)*Log[1 - (2*MASS)/r]) + 
      (12*(-1 + 3*Cos[\[Theta]]^2)*(-448*J^2*MASS^5*(32*MASS^6 - 
           8*MASS^4*r^2 - 20*MASS^3*r^3 + 40*MASS^2*r^4 + 15*MASS*r^5 - 
           15*r^6) + 20*MASS^2*(5*M2^2*r^2*(40*MASS^5 - 428*MASS^4*r + 
             834*MASS^3*r^2 + 276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) + 
           (2*MASS - r)*(112*M22*MASS^4*r^3*(2*MASS^2 - 3*MASS*r - 3*r^2) - 
             5*(16*MASS^6 + 24*MASS^5*r + 8*MASS^4*r^2 + 130*MASS^3*r^3 - 84*
                MASS^2*r^4 - 159*MASS*r^5 - 63*r^6)*S2^2)) + 
         30*MASS*(2*MASS - r)*r^3*(224*M22*MASS^6*r - 112*M22*MASS^4*r^3 + 
           112*J^2*(2*MASS^5*r - MASS^3*r^3) + 5*M2^2*(64*MASS^4 - 
             198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 6*r^4) + 
           240*MASS^4*S2^2 - 610*MASS^3*r*S2^2 - 320*MASS^2*r^2*S2^2 + 
           145*MASS*r^3*S2^2 + 210*r^4*S2^2)*Log[1 - (2*MASS)/r] + 
         225*(2*MASS - r)*r^4*(M2^2*(16*MASS^4 + 4*MASS^2*r^2 - r^4) + 
           (24*MASS^4 - 12*MASS^2*r^2 - 8*MASS*r^3 + 7*r^4)*S2^2)*
          Log[1 - (2*MASS)/r]^2))/(2*MASS - r) - 
      ((3 - 30*Cos[\[Theta]]^2 + 35*Cos[\[Theta]]^4)*
        (-2*MASS*(15*M2^2*r^2*(-384*MASS^6 + 7368*MASS^5*r - 
             34024*MASS^4*r^2 - 58798*MASS^3*r^3 + 211596*MASS^2*r^4 - 
             162219*MASS*r^5 + 37485*r^6) + (2*MASS - r)*
            (-1176*M4*MASS*r^3*(12*MASS^4 - 130*MASS^3*r - 270*MASS^2*r^2 + 
               735*MASS*r^3 - 315*r^4) + 5*(192*MASS^7 + 736*MASS^6*r + 880*
                MASS^5*r^2 - 2724*MASS^4*r^3 + 36666*MASS^3*r^4 + 79110*
                MASS^2*r^5 - 204183*MASS*r^6 + 85995*r^7)*S2^2)) + 
         15*(2*MASS - r)*r^3*(1176*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 
             70*MASS*r^3 - 21*r^4) + 3*M2^2*(928*MASS^5 - 7064*MASS^4*r - 
             3408*MASS^3*r^2 + 38868*MASS^2*r^3 - 41506*MASS*r^4 + 
             12495*r^5) + (-96*MASS^5 + 11752*MASS^4*r + 2032*MASS^3*r^2 - 
             87852*MASS^2*r^3 + 97902*MASS*r^4 - 28665*r^5)*S2^2)*
          Log[1 - (2*MASS)/r] + 180*(2*MASS - r)*r^4*
          (M2^2*(220*MASS^4 - 414*MASS^2*r^2 + 189*MASS*r^3 + 9*r^4) + 
           (-20*MASS^4 + 150*MASS^2*r^2 - 173*MASS*r^3 + 49*r^4)*S2^2)*
          Log[1 - (2*MASS)/r]^2))/(-2*MASS + r)))/(43008*MASS^10*r^2), 0}, 
 {(\[Epsilon]p*(-16*J*MASS^5 + 5*S2*Cos[\[Theta]]*
       (2*MASS*(2*MASS^3 + 2*MASS^2*r + 3*MASS*r^2 - 3*r^3) + 
        3*(2*MASS - r)*r^3*Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/
    (8*MASS^5*r) - (\[Epsilon]p^2*(-40*MASS^2*Cos[\[Theta]]^2*
       (2*MASS*(2*J*M2*(12*MASS^5 + 88*MASS^4*r + 70*MASS^3*r^2 + 
            210*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
          7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
            45*r^4)*S3) + 3*r*(2*J*M2*(-16*MASS^5 + 20*MASS^4*r + 
            280*MASS^2*r^3 - 350*MASS*r^4 + 105*r^5) - 
          35*MASS*r^3*(8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*
         Log[1 - (2*MASS)/r]) + 8*MASS^2*
       (2*MASS*(2*J*M2*(20*MASS^5 + 48*MASS^4*r + 10*MASS^3*r^2 + 
            270*MASS^2*r^3 - 735*MASS*r^4 + 315*r^5) - 
          7*MASS*r*(4*MASS^4 + 10*MASS^3*r + 30*MASS^2*r^2 - 105*MASS*r^3 + 
            45*r^4)*S3) + 15*(2*J*M2*r^2*(4*MASS^4 - 8*MASS^3*r + 
            60*MASS^2*r^2 - 70*MASS*r^3 + 21*r^4) - 7*MASS*r^4*
           (8*MASS^2 - 10*MASS*r + 3*r^2)*S3)*Log[1 - (2*MASS)/r]) + 
      Cos[\[Theta]]*(-50*M2*MASS*(16*MASS^6 + 180*MASS^5*r + 586*MASS^4*r^2 + 
          3006*MASS^3*r^3 - 19059*MASS^2*r^4 + 19917*MASS*r^5 - 5670*r^6)*
         S2 + 504*MASS^2*r*(4*MASS^5 + 18*MASS^4*r + 90*MASS^3*r^2 - 
          685*MASS^2*r^3 + 735*MASS*r^4 - 210*r^5)*S4 + 
        15*(5*M2*r*(32*MASS^6 + 48*MASS^5*r + 208*MASS^4*r^2 - 
            6224*MASS^3*r^3 + 12614*MASS^2*r^4 - 8553*MASS*r^5 + 1890*r^6)*
           S2 + 252*MASS*r^4*(40*MASS^3 - 90*MASS^2*r + 63*MASS*r^2 - 14*r^3)*
           S4)*Log[1 - (2*MASS)/r] + 450*M2*r^4*(40*MASS^3 - 58*MASS^2*r + 
          23*MASS*r^2 - 2*r^3)*S2*Log[1 - (2*MASS)/r]^2) + 
      3*Cos[\[Theta]]^3*(2*MASS*(25*M2*(16*MASS^6 + 188*MASS^5*r + 
            486*MASS^4*r^2 + 2298*MASS^3*r^3 - 14853*MASS^2*r^4 + 
            15507*MASS*r^5 - 4410*r^6)*S2 - 196*MASS*r*(4*MASS^5 + 
            18*MASS^4*r + 90*MASS^3*r^2 - 685*MASS^2*r^3 + 735*MASS*r^4 - 
            210*r^5)*S4) - 15*(5*M2*r*(32*MASS^6 + 16*MASS^5*r + 
            112*MASS^4*r^2 - 4768*MASS^3*r^3 + 9810*MASS^2*r^4 - 
            6663*MASS*r^5 + 1470*r^6)*S2 + 196*MASS*r^4*(40*MASS^3 - 
            90*MASS^2*r + 63*MASS*r^2 - 14*r^3)*S4)*Log[1 - (2*MASS)/r] + 
        450*M2*r^4*(-24*MASS^3 + 38*MASS^2*r - 17*MASS*r^2 + 2*r^3)*S2*
         Log[1 - (2*MASS)/r]^2))*Sin[\[Theta]]^2)/(512*MASS^10*r^2), 0, 0, 
  r^2*Sin[\[Theta]]^2 - (5*M2*r*\[Epsilon]p*(1 + 3*Cos[2*\[Theta]])*
     (-4*MASS^3 + 6*MASS^2*r + 6*MASS*r^2 + (-6*MASS^2*r + 3*r^3)*
       Log[1 - (2*MASS)/r])*Sin[\[Theta]]^2)/(32*MASS^5) - 
   (\[Epsilon]p^2*(6720*M22*MASS^5*r^3*(1 + 3*Cos[2*\[Theta]])*
       (2*MASS*(2*MASS^2 - 3*MASS*r - 3*r^2) + (6*MASS^2*r - 3*r^3)*
         Log[1 - (2*MASS)/r]) - 1344*MASS^2*Cos[\[Theta]]*
       (-1 + 5*Cos[2*\[Theta]])*
       (2*MASS*(-7*M3*MASS*r^3*(2*MASS^3 - 10*MASS^2*r - 15*MASS*r^2 + 
            15*r^3) + 2*J*(4*MASS^6 + 8*MASS^5*r + 6*MASS^4*r^2 - 
            8*MASS^3*r^3 + 70*MASS^2*r^4 + 105*MASS*r^5 - 105*r^6)*S2) - 
        3*r^4*(7*M3*MASS*(4*MASS^3 - 10*MASS*r^2 + 5*r^3) + 
          2*J*(26*MASS^3 - 70*MASS*r^2 + 35*r^3)*S2)*Log[1 - (2*MASS)/r]) - 
      (12*(-1 + 3*Cos[\[Theta]]^2)*(-448*J^2*MASS^5*(32*MASS^6 - 
           8*MASS^4*r^2 - 20*MASS^3*r^3 + 40*MASS^2*r^4 + 15*MASS*r^5 - 
           15*r^6) + 20*MASS^2*(5*M2^2*r^2*(40*MASS^5 - 428*MASS^4*r + 
             834*MASS^3*r^2 + 276*MASS^2*r^3 - 315*MASS*r^4 + 9*r^5) + 
           (2*MASS - r)*(112*M22*MASS^4*r^3*(2*MASS^2 - 3*MASS*r - 3*r^2) - 
             5*(16*MASS^6 + 24*MASS^5*r + 8*MASS^4*r^2 + 130*MASS^3*r^3 - 84*
                MASS^2*r^4 - 159*MASS*r^5 - 63*r^6)*S2^2)) + 
         30*MASS*(2*MASS - r)*r^3*(224*M22*MASS^6*r - 112*M22*MASS^4*r^3 + 
           112*J^2*(2*MASS^5*r - MASS^3*r^3) + 5*M2^2*(64*MASS^4 - 
             198*MASS^3*r + 16*MASS^2*r^2 + 99*MASS*r^3 - 6*r^4) + 
           240*MASS^4*S2^2 - 610*MASS^3*r*S2^2 - 320*MASS^2*r^2*S2^2 + 
           145*MASS*r^3*S2^2 + 210*r^4*S2^2)*Log[1 - (2*MASS)/r] + 
         225*(2*MASS - r)*r^4*(M2^2*(16*MASS^4 + 4*MASS^2*r^2 - r^4) + 
           (24*MASS^4 - 12*MASS^2*r^2 - 8*MASS*r^3 + 7*r^4)*S2^2)*
          Log[1 - (2*MASS)/r]^2))/(2*MASS - r) + 
      ((3 - 30*Cos[\[Theta]]^2 + 35*Cos[\[Theta]]^4)*
        (2*MASS*(15*M2^2*r^2*(-384*MASS^6 + 7368*MASS^5*r - 
             34024*MASS^4*r^2 - 58798*MASS^3*r^3 + 211596*MASS^2*r^4 - 
             162219*MASS*r^5 + 37485*r^6) + (2*MASS - r)*
            (-1176*M4*MASS*r^3*(12*MASS^4 - 130*MASS^3*r - 270*MASS^2*r^2 + 
               735*MASS*r^3 - 315*r^4) + 5*(192*MASS^7 + 736*MASS^6*r + 880*
                MASS^5*r^2 - 2724*MASS^4*r^3 + 36666*MASS^3*r^4 + 79110*
                MASS^2*r^5 - 204183*MASS*r^6 + 85995*r^7)*S2^2)) - 
         15*(2*MASS - r)*r^3*(1176*M4*MASS*r*(8*MASS^4 - 60*MASS^2*r^2 + 
             70*MASS*r^3 - 21*r^4) + 3*M2^2*(928*MASS^5 - 7064*MASS^4*r - 
             3408*MASS^3*r^2 + 38868*MASS^2*r^3 - 41506*MASS*r^4 + 
             12495*r^5) + (-96*MASS^5 + 11752*MASS^4*r + 2032*MASS^3*r^2 - 
             87852*MASS^2*r^3 + 97902*MASS*r^4 - 28665*r^5)*S2^2)*
          Log[1 - (2*MASS)/r] - 180*(2*MASS - r)*r^4*
          (M2^2*(220*MASS^4 - 414*MASS^2*r^2 + 189*MASS*r^3 + 9*r^4) + 
           (-20*MASS^4 + 150*MASS^2*r^2 - 173*MASS*r^3 + 49*r^4)*S2^2)*
          Log[1 - (2*MASS)/r]^2))/(2*MASS - r))*Sin[\[Theta]]^2)/
    (43008*MASS^10*r^2)}}
