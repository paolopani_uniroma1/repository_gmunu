(* Created with the Wolfram Language for Students - Personal Use Only : www.wolfram.com *)
{{-1 + (2*MASS)/r + (\[Epsilon]p^2*(80*M2*MASS^4*(1 - 3*Cos[\[Theta]]^2)*
       (2*MASS*(4*MASS^4 + 6*MASS^3*r - 22*MASS^2*r^2 + 15*MASS*r^3 - 
          3*r^4) + 3*(2*MASS - r)^3*r^2*Log[1 - (2*MASS)/r]) - 
      21*M4*(3 - 30*Cos[\[Theta]]^2 + 35*Cos[\[Theta]]^4)*
       (2*MASS*(MASS - r)*(8*MASS^5 + 76*MASS^4*r - 920*MASS^3*r^2 + 
          1280*MASS^2*r^3 - 630*MASS*r^4 + 105*r^5) + 15*(2*MASS - r)^3*r^2*
         (6*MASS^2 - 14*MASS*r + 7*r^2)*Log[1 - (2*MASS)/r])))/
    (256*MASS^9*(2*MASS - r)*r^2), 0, 0, 
  (5*S2*\[Epsilon]p^2*Cos[\[Theta]]*
     (2*MASS*(2*MASS^3 + 2*MASS^2*r + 3*MASS*r^2 - 3*r^3) + 
      3*(2*MASS - r)*r^3*Log[1 - (2*MASS)/r])*Sin[\[Theta]]^2)/(8*MASS^5*r) - 
   (3*\[Epsilon]p^3*Cos[\[Theta]]*(-1792*MASS^4*S4*(1 + 7*Cos[2*\[Theta]])*
       (2*MASS*(4*MASS^5 + 18*MASS^4*r + 90*MASS^3*r^2 - 685*MASS^2*r^3 + 
          735*MASS*r^4 - 210*r^5) + 15*r^3*(40*MASS^3 - 90*MASS^2*r + 
          63*MASS*r^2 - 14*r^3)*Log[1 - (2*MASS)/r]) - 
      143*S6*(19 + 12*Cos[2*\[Theta]] + 33*Cos[4*\[Theta]])*
       (2*MASS*(16*MASS^7 + 160*MASS^6*r + 1680*MASS^5*r^2 - 
          35784*MASS^4*r^3 + 106890*MASS^3*r^4 - 120960*MASS^2*r^5 + 
          58905*MASS*r^6 - 10395*r^7) + 105*r^3*(224*MASS^5 - 1120*MASS^4*r + 
          2016*MASS^3*r^2 - 1680*MASS^2*r^3 + 660*MASS*r^4 - 99*r^5)*
         Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/(65536*MASS^13*r)}, 
 {0, -(r/(2*MASS - r)) + 
   (\[Epsilon]p^2*(2*MASS*(MASS - r)*(80*M2*MASS^4*(2*MASS^2 + 6*MASS*r - 
          3*r^2) - 63*M4*(4*MASS^4 + 40*MASS^3*r - 440*MASS^2*r^2 + 
          420*MASS*r^3 - 105*r^4)) - 15*r^2*(-2*MASS + r)^2*
       (-16*M2*MASS^4 + 63*M4*(6*MASS^2 - 14*MASS*r + 7*r^2))*
       Log[1 - (2*MASS)/r] - 735*M4*Cos[\[Theta]]^4*
       (2*MASS*(4*MASS^5 + 36*MASS^4*r - 480*MASS^3*r^2 + 860*MASS^2*r^3 - 
          525*MASS*r^4 + 105*r^5) + 15*r^2*(-2*MASS + r)^2*
         (6*MASS^2 - 14*MASS*r + 7*r^2)*Log[1 - (2*MASS)/r]) + 
      30*Cos[\[Theta]]^2*(2*MASS*(MASS - r)*
         (-8*M2*MASS^4*(2*MASS^2 + 6*MASS*r - 3*r^2) + 
          21*M4*(4*MASS^4 + 40*MASS^3*r - 440*MASS^2*r^2 + 420*MASS*r^3 - 
            105*r^4)) + 3*r^2*(-2*MASS + r)^2*(-8*M2*MASS^4 + 
          105*M4*(6*MASS^2 - 14*MASS*r + 7*r^2))*Log[1 - (2*MASS)/r])))/
    (256*MASS^9*(-2*MASS + r)^2), 0, 0}, 
 {0, 0, r^2 + (r*\[Epsilon]p^2*(-320*M2*MASS^4*(1 + 3*Cos[2*\[Theta]])*
       (-4*MASS^3 + 6*MASS^2*r + 6*MASS*r^2 + (-6*MASS^2*r + 3*r^3)*
         Log[1 - (2*MASS)/r]) - 7*M4*(9 + 20*Cos[2*\[Theta]] + 
        35*Cos[4*\[Theta]])*(2*MASS*(-12*MASS^4 + 130*MASS^3*r + 
          270*MASS^2*r^2 - 735*MASS*r^3 + 315*r^4) - 
        15*(8*MASS^4*r - 60*MASS^2*r^3 + 70*MASS*r^4 - 21*r^5)*
         Log[1 - (2*MASS)/r])))/(2048*MASS^9), 0}, 
 {(5*S2*\[Epsilon]p^2*Cos[\[Theta]]*
     (2*MASS*(2*MASS^3 + 2*MASS^2*r + 3*MASS*r^2 - 3*r^3) + 
      3*(2*MASS - r)*r^3*Log[1 - (2*MASS)/r])*Sin[\[Theta]]^2)/(8*MASS^5*r) - 
   (3*\[Epsilon]p^3*Cos[\[Theta]]*(-1792*MASS^4*S4*(1 + 7*Cos[2*\[Theta]])*
       (2*MASS*(4*MASS^5 + 18*MASS^4*r + 90*MASS^3*r^2 - 685*MASS^2*r^3 + 
          735*MASS*r^4 - 210*r^5) + 15*r^3*(40*MASS^3 - 90*MASS^2*r + 
          63*MASS*r^2 - 14*r^3)*Log[1 - (2*MASS)/r]) - 
      143*S6*(19 + 12*Cos[2*\[Theta]] + 33*Cos[4*\[Theta]])*
       (2*MASS*(16*MASS^7 + 160*MASS^6*r + 1680*MASS^5*r^2 - 
          35784*MASS^4*r^3 + 106890*MASS^3*r^4 - 120960*MASS^2*r^5 + 
          58905*MASS*r^6 - 10395*r^7) + 105*r^3*(224*MASS^5 - 1120*MASS^4*r + 
          2016*MASS^3*r^2 - 1680*MASS^2*r^3 + 660*MASS*r^4 - 99*r^5)*
         Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/(65536*MASS^13*r), 0, 0, 
  r^2*Sin[\[Theta]]^2 + 
   (r*\[Epsilon]p^2*(320*M2*MASS^4*(1 + 3*Cos[2*\[Theta]])*
       (2*MASS*(2*MASS^2 - 3*MASS*r - 3*r^2) + (6*MASS^2*r - 3*r^3)*
         Log[1 - (2*MASS)/r]) + 7*M4*(9 + 20*Cos[2*\[Theta]] + 
        35*Cos[4*\[Theta]])*(2*MASS*(12*MASS^4 - 130*MASS^3*r - 
          270*MASS^2*r^2 + 735*MASS*r^3 - 315*r^4) + 
        15*(8*MASS^4*r - 60*MASS^2*r^3 + 70*MASS*r^4 - 21*r^5)*
         Log[1 - (2*MASS)/r]))*Sin[\[Theta]]^2)/(2048*MASS^9)}}
